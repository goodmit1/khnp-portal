package com.clovirsm;

import java.util.List;
import java.util.Map;

import com.clovirsm.hv.IAfterProcess;

public interface ISite {

	IAfterProcess onAfterProcess(String svcCd, String cudCd,  Map info) throws Exception;
	boolean expire(String svcCd, String pkVal, boolean bySchedule ) throws Exception;
	void deleteAfterExpire(String svcCd, String pkVal) throws Exception;
	void reuse(String svcCd, String pkVal, int mm ) throws Exception;
	void onAfterCollectCatalog(List<Map> list) throws Exception;
}
