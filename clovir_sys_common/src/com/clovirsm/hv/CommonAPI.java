package com.clovirsm.hv;

import java.util.Map;

 

 

public    class CommonAPI implements IAPI {

	
	protected String getConnectionName() {
		return AnonymousConnection.class.getName();
	}
	public IConnection connect(Map param ) throws Exception
	{
		return connect(param, 5, 0);
	}
	public IConnection connect(Map param , int maxSize, int initialSize) throws Exception
	{
		 
		return ConnectionPool.getInstance().getConnection(getConnectionName(), (String)param.get("CONN_URL"), (String)param.get("CONN_USERID"), (String)param.get("CONN_PWD"), param, maxSize, initialSize);
		 
	}
 
	public static boolean disconnect(IConnection conn) {
		
		try {
			if(conn != null) {
				return ConnectionPool.getInstance().release(conn);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
