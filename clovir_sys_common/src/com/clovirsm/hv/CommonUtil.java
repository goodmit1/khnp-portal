package com.clovirsm.hv;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

public class CommonUtil {
	public static JSONArray getJSONArray(JSONObject json, String key) {
		Object o = json.get(key);
		JSONArray result = null;
		if(o instanceof JSONObject) {
			result = new JSONArray();
			result.put(o);
		}
		else {
			result = (JSONArray)o;
		}
		return result;
	}
	public static List<File> listSortedFiles(File file, final boolean isAsc) {
		File[] files = file.listFiles();
		List<File> result  = new ArrayList();
		for(File f:files) {
			result.add(f);
		}
		Collections.sort(result, new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				if(isAsc) {
					return o1.getName().compareTo(o2.getName());
				}
				else {
					return o2.getName().compareTo(o1.getName());
				}
				 
			}
			
		}
		);
		return result;
	}
	public static Date convertToCurrentTimeZone(String Date) {
        
        try {

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utcFormat.parse(Date);

            return date;
        }
        catch (Exception e){ e.printStackTrace();}

        return null;
	}
	public static Date convertUTC(String Date, String format) {
        
        try {

            DateFormat utcFormat = new SimpleDateFormat(format);
            //utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utcFormat.parse(Date);
            long tt= date.getTime();
            return new Date(tt - TimeZone.getDefault().getOffset(tt));
            
        }
        catch (Exception e){ e.printStackTrace();}

        return null;
	}

	//get the current time zone
	
	public static String getCurrentTimeZone(){
	        TimeZone tz = Calendar.getInstance().getTimeZone();
	       
	        return tz.getID();
	}
	
	private static void printPixelARGB(int pixel) {
	    int alpha = (pixel >> 24) & 0xff;
	    int red = (pixel >> 16) & 0xff;
	    int green = (pixel >> 8) & 0xff;
	    int blue = (pixel) & 0xff;
	    System.out.println("argb: " + alpha + ", " + red + ", " + green + ", " + blue);
	  }
	 public static float[] convertGrayImgToFloat(  BufferedImage img)
	 {
		 

		 
		 
		 float[] result = new float[img.getWidth() * img.getHeight()];
		   // float [] bitfloat = null;
		   // bitfloat = img.getData().getPixels(0, 0, img.getWidth(), img.getHeight(), bitfloat);
		    int idx=0;
		    for(int y=0 ; y < img.getHeight(); y++)
		    {
		    	for(int x=0; x < img.getWidth(); x++)
		    	{
		    		 
		    		int i = img.getRGB(x, y);
		    		Color color = new Color(i);
		    		result[idx++] = 1- (float) ( 1.0 * color.getBlue()/255);
		    			
		    	}
		    	 
		    }

		 
		    return result;
	 }
	 
	 public static String getDataUrl(byte[] fileArray, String fileExtName) {
		 
	                    String imageString  = new String( Base64.getEncoder().encode(  fileArray ) );
	 
	                    return "data:image/"+ fileExtName +";base64, "+ imageString;
	                    
	 }
	 
	 public static String join(List<String>list, String delimeter) {
		 StringBuffer sb = new StringBuffer();
		 for(String s:list) {
			 if(sb.length()>0) {
				 sb.append(delimeter);
			 }
			 sb.append(s);
		 }
		 return sb.toString();
	 }
	 
	 public static int indexOf(String[] arr, String a) {
		 int idx= 0; 
		 for(String s:arr) {
			 if(s.equals(a)) {
				 return idx;
			 }
			 idx++;
		 }
		 return -1;
	 }
	 public static double[][][] convertColorImgToFloat(  BufferedImage img)
	 {
		 

		 
		 
		 double[][][] result = new double[img.getWidth()][img.getHeight()][4];
		   // float [] bitfloat = null;
		   // bitfloat = img.getData().getPixels(0, 0, img.getWidth(), img.getHeight(), bitfloat);
		    int idx=0;
		    for(int y=0 ; y < img.getHeight(); y++)
		    {
		    	for(int x=0; x < img.getWidth(); x++)
		    	{
		    		 
		    		int i = img.getRGB(x, y);
		    		Color color = new Color(i);
		    		result[x][y][0] =  (1.0 * color.getRed()/255);
		    		result[x][y][1] =  (1.0 * color.getGreen()/255);
		    		result[x][y][2] =  (1.0 * color.getBlue()/255);
		    		result[x][y][3] =  (1.0 * color.getAlpha()/255);		    			
		    	}
		    	 
		    }

		 
		    return result;
	 }
	 public static void readStream2Stream(InputStream from, OutputStream to ) throws IOException
	  {
	    byte[] buf = new byte[4096];
	    int len = 0;
	    while ((len = from.read(buf)) > 0)
	    {
	      to.write(buf, 0, len);
	    }
	  }
	 public static String readStream2String(InputStream from  , String encoding) throws IOException
	  {
	    byte[] buf = new byte[4096];
	    int len = 0;
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    while ((len = from.read(buf)) > 0)
	    {
	    	out.write(buf, 0, len);
	    }
	    return new String(out.toByteArray(),encoding);
	  }
	public static int getInt(Object o)
	{
		if(o==null)
		{
			return -1;
		}
		else
		{
			return Integer.parseInt(o.toString());
		}
	}
	public static String convertDateStr(Date date, String format)
	{
		SimpleDateFormat transFormat = new SimpleDateFormat(format);
		return transFormat.format(date);
	}
	public static Map<String, String> getQueryMap(String query)
	{
	    String[] params = query.split("&");
	    Map<String, String> map = new HashMap<String, String>();
	    for (String param : params)
	    {
	        String name = param.split("=")[0];
	        String value = param.split("=")[1];
	        map.put(name, value);
	    }
	    return map;
	}
	public static long getSize(String o)
	{
		StringBuffer sb = new StringBuffer();
		int i=0;
		for(; i < o.length(); i++) {
			if((o.charAt(i)>='0' && o.charAt(i)<='9')  ) {
				continue;
			}
			else {
				break;
			}
		}
		return Long.parseLong(o.substring(0, i));
	}
	public static String getSizeUnit(String o)
	{
		int i=0;
		for(; i < o.length(); i++) {
			if((o.charAt(i)>='0' && o.charAt(i)<='9') || o.charAt(i) == '.') {
				continue;
			}
			else {
				break;
			}
		}
		return o.substring(  i);
	}
	public static void main(String[] args) {
		String size = "10.00Gi";
		System.out.println(CommonUtil.getSize(size));
		System.out.println(CommonUtil.getSizeUnit(size));
	}
	public static List<Map> convertList(JSONArray arr, String... field) {
		List result = new ArrayList();
		for(int i=0; i < arr.length(); i++) {
			result.add(converMap(arr.getJSONObject(i) , field));
		}
		return result;
	}
	
	public static void putAll(JSONObject target, JSONObject src) {
		Iterator it = src.keys();
		while(it.hasNext()) {
			String key = (String)it.next();
			target.put(key, src.get(key));
		}
	}
	public static Map converMap(JSONObject map, String... field) {
		Map result = new HashMap();
		if(field==null || field.length==0) {
			for(Object f:map.keySet()) {
				result.put(f, map.get((String)f));
			}
		}
		else {
			for(String f:field) {
				result.put(f, map.get(f));
			}
		}
		
		return result;
	}
	public static String getInnerStr(String str, String start, String end) {
		int pos = str.indexOf(start);
		if(pos>=0) {
			int pos1 = str.indexOf( end,pos+start.length()+1);
			if(pos1>pos) {
				return str.substring(pos+start.length() , pos1);
			}
		}
		return null;
	}
	public static String getReplaceInnerStr(String str, String start, String end, String replaceStr) {
		int pos = str.indexOf(start);
		if(pos>=0) {
			int pos1 = str.indexOf( end,pos+start.length()+1);
			if(pos1>pos) {
				return str.substring(0,pos+start.length()+1) + replaceStr + str.substring(pos1);
			}
		}
		return null;
	}
	
	public static Object nvl(Object obj, Object def) {
		if(obj == null) {
			return def;
		}
		else {
			return obj;
		}
	}
}
