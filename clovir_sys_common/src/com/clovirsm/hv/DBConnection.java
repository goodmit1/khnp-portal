package com.clovirsm.hv;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

public class DBConnection implements IConnection {
	String url;
	Connection conn ;
	String type;
	String userId;
	String pwd;
	String driver ;
	public String getDriver() {
		return driver;
	}


	 
	 
	public String getUserId() {
		return userId;
	}

	 

	public String getPwd() {
		return pwd;
	}

	 
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getURL() {
		 
		return url;
	}

	public Connection getConn() {
		return conn;
	}
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		type =  getDBType(url);
		this.url = url;
		this.userId = userId;
		this.pwd = pwd;
				
		driver = getDriver(type, prop);
		
		if(driver != null) {
			conn =  getDBConnection(driver , url, userId, pwd);
		}
		else {
			conn =  getDBConnection(url, userId, pwd);
		}
		
		
	}
	public static String getDriver(String type, Map prop) {
		String driver = prop == null ? null : (String) prop.get("driverClassName");
		if(driver == null) {
			switch (type){
            case ("oracle"):
                return "oracle.jdbc.driver.OracleDriver";
              
            case ("mssql"):
                return "com.microsoft.sqlserver.jdbc.SQLServerDriver" ;
                
            case ("mysql"):
                return "com.mysql.jdbc.Driver" ;
                

			}
		}
		return driver;
	}
	public static String getDBType(String url) {
		String type = null;
    	int pos = url.indexOf(":", 6);
    	type = url.substring("jdbc:".length(), pos);
    	return type;
	}
	public static Connection getDBConnection( String driverName, String url, String id, String pwd ) throws  Exception{
		   Class.forName(driverName);
		   return DriverManager.getConnection(url, id, pwd);
	}
	public static Connection getDBConnection( String url, String id, String pwd ) throws  Exception{
    	String type = getDBType(url);
        
    	String driverName = getDriver(type, null);
        return getDBConnection(driverName, url, id, pwd);
    }
	@Override
	public void disconnect() {
		try {
			boolean isOk = CommonAPI.disconnect(this);
			 
			if(!isOk && conn != null) {
				conn.close();
			}
		}
		catch(Exception ignore) {}
	}

	@Override
	public boolean isConnected() {
		 
		try {
			if(conn == null) {
				return false;
			}
			return !conn.isClosed();
		} catch (SQLException e) {
			 
			e.printStackTrace();
			return false;
		}
	}

}
