package com.clovirsm.hv;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.yaml.snakeyaml.Yaml;

public class YamlUtil {
	public static boolean isJSON(String str)
	{
		str = str.trim();
		if(str.startsWith("{") || str.startsWith("[")) {
			return true;
		}
		return false;
	}
	public static void putAllJSONArray(JSONArray target, JSONArray src) {
		if(src == null) return;
		for(int i=0; i < src.length(); i++) {
			target.put(src.get(i));
		}
		 
	}
	public static JSONObject getJSONObjectByPath(boolean notExistCreate, JSONObject json, String... path)
	{
		JSONObject json1= json;
		for(String p:path)
		{
			if(json1.has(p))
			{
				if(json1.get(p) instanceof JSONArray) {
					json1 = json1.getJSONArray(p).getJSONObject(0);
				}
				else {
					json1 = json1.getJSONObject(p);
				}
					
			}
			else if(notExistCreate)
			{
				json1.put(p, new JSONObject());
				json1 = json1.getJSONObject(p);
			}
			else {
				return null;
			}
		}
		return json1;
	}
	public static JSONObject yamlToJson(String yamlString) {
		
		if(isJSON(yamlString))
		{
			return new JSONObject(yamlString);
		}
	    Yaml yaml= new Yaml();
	    Map map = (Map) yaml.load(yamlString);
	    return new JSONObject(map);

	    
	    
	}
	public static String jsonEscape(String string) {
		int i;
		int len = string.length();
		char b;
	    char c = 0;
		StringBuffer w = new StringBuffer();
		  for (i = 0; i < len; i += 1) {
	            b = c;
	            c = string.charAt(i);
	            switch (c) {
	            case '\\':
	            case '"':
	                w.append("\\\\");
	                w.append(c);
	                break;
	            case '/':
	                if (b == '<') {
	                    w.append("\\\\");
	                }
	                w.append(c);
	                break;
	            case '\b':
	                w.append("\\\\b");
	                break;
	            case '\t':
	                w.append("\\\\t");
	                break;
	            case '\n':
	                w.append("\\\\n");
	                break;
	            case '\f':
	                w.append("\\\\f");
	                break;
	            case '\r':
	                
	                break;
	            default:
	               
	                    w.append(c);
	                 
	            }
	        }
		return w.toString();
	}
	public static float getGi(String size)
	{
		if(size.endsWith("m")) {
			return Integer.parseInt(size.substring(0, size.length()-1))/100;
		}
		int iSize = Integer.parseInt(size.substring(0, size.length()-2));
		if(size.endsWith("Mi"))
		{
			return iSize/1024;
		}
		else if(size.endsWith("Ki"))
		{
			return iSize/1024/1024;
		}
		 
		return iSize;
	}
	public static Map yamlToMap(String yamlString)
	{
		Yaml yaml= new Yaml();
		return yaml.loadAs(yamlString, Map.class);
	}
}
