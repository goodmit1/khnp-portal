package com.clovirsm.cm.sys.git;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.clovirsm.hv.RestClient;

public class GitHubAPI implements IGitAPI{

	RestClient client;
	String token;
	public String getToken() {
		return token;
	}


	String user;
	String gitUrl; 
	public void setToken(String token)
	{
		this.token = token;
		Map header = new HashMap();
		header.put("Authorization", "token " + token);
		client.setMimeType("application/json");
		client.setHeader(header);
	}

	 
	public String createUser(String id , String email, String pwd) throws Exception
	{
		return null;
	}
	public void importPrj(String prjId, String id, File file ) throws Exception{
		Map<String, Object> params = new HashMap();
		params.put("path", id);
		params.put("namespace", prjId);
		client.postFiles("/projects/import", params , new File[] { file });
	}
	
	public void createDirectory(String prjId, String id, String dir) throws Exception
	{
		JSONObject json = new JSONObject();
		json.put("message", "init");
		json.put("content", "");
		
		client.put("/repos/" + user +"/" + id + "/contents/" + dir + "/README.md", json.toString());
	}
	public void createRepository(String prjId, String id) throws Exception
	{
		JSONObject json = new JSONObject();
		json.put("name", id);
		json.put("private", false);
		json.put("name", id);
		try
		{
			client.getJSON("/repos/" + user + "/" + id);
		}
		catch(Exception e)
		{
			client.post("/user/repos", json.toString());
		}
		 
		
		
	}
	
	public String getGitURL(String prjId, String id, String reqToken)
	{
		String url = gitUrl.replace("api.", reqToken==null?token:reqToken + ":x-oauth-basic@");
		return url +  "/" + id + ".git";
	}

	@Override
	public void setUrl(String url) {
		this.gitUrl = url;
		int pos = url.lastIndexOf("/");
		user = url.substring(pos+1);
		client = new RestClient( url.substring(0,pos)  );
		
	}


	@Override
	public   void saveFile(String id,  String repoName, String branch, String fileName,  String content, String commitMsg) throws Exception{
		
	}


	@Override
	public boolean movePrjToGroup(String groupName, String prjName) throws Exception {
		 
		return false;
	}


	@Override
	public JSONObject getPrj(String id, String repoName)   {
		try {
		return client.getJSON("/repos/" + user + "/" + repoName);
		}catch(Exception e) {
			return null;
		}
	}


	@Override
	public void setUserId(String string) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void setRepo(String string) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String getUserId() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getRepo() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void addPrjMember(String group, String prjId, String userId, String level) throws Exception {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void deletePipeline(String group, String name, String pipeline) throws Exception {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void newFile(String group, String id, String branch, String fileName, String content, String commitMsg)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void deleteFile(String group,  String repoName, String branch, String fileName, String commitMsg) throws Exception{
		
	}


	@Override
	public boolean existFile(String gitGroup, String repo, String fileName) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public String getJobLog(String group, String repoName, String jobId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void deletePrj(String group, String name) throws Exception {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void deleteFiles(String src_group, String src_repoName, String path, String commitMsg)
			throws JSONException, Exception {
		// TODO Auto-generated method stub
		
	}

	 
}

