package com.clovirsm.cm.sys.git;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.yaml.snakeyaml.util.UriEncoder;

import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.RestClient;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public class GitLabAPI implements IGitAPI{

	RestClient client;
	String gitUrl;
	public String getGitUrl() {
		return gitUrl;
	}
	public void setGitUrl(String gitUrl) {
		this.gitUrl = gitUrl;
	}

	String token;
	public String getToken() {
		return token;
	}

	String userId;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRepo() {
		return repo;
	}
	public void setRepo(String repo) {
		this.repo = repo;
	}

	String repo;
	
	
	
	
	 
	public RestClient getClient() {
		return client;
	}
	public void setToken(String token){
		Map header = new HashMap();
		header.put("Private-Token", token);
		client.setMimeType("application/json");
		client.setHeader(header);
		this.token = token;
	}
	public boolean movePrjToGroup(String groupName, String prjName) throws Exception{
		JSONArray prjs = (JSONArray)client.get("/projects?search=" + prjName);
		Object prjId = null;
		Object groupId = null;
		for(int i=0; i < prjs.length(); i++) {
			if(prjs.getJSONObject(i).getString("name").equals(prjName)){
				prjId = prjs.getJSONObject(0).get("id");
				break;
			}
		}
		JSONObject parentInfo = client.getJSON("/groups/" + groupName);
		if(parentInfo != null) {
			 groupId = parentInfo.get("id");
		}
		
		if(groupId != null && prjId != null  ) {
			client.post("/groups/" + groupId + "/projects/" + prjId, "{}");
			return true;
		}
		return false;
	}
	public void deletePrj(String group, String name) throws Exception{
		try {
			client.delete("/projects/" + group + "%2F" +   name.toLowerCase());
		}
		catch(Exception e) {
			if(e.getMessage().indexOf("404")<0) {
				throw e;
			}
		}
	}
	public void addPrjMember(String group, String name, String userId, String level) throws Exception{
		JSONArray user = (JSONArray)client.get("/users?username=" + userId);
		if(user==null || user.length() < 1) {
			throw new NotFoundException("user:" + userId);
		}
		client.setMimeType("application/x-www-form-urlencoded");
		try {
			client.post("/projects/" + group + "%2F" +   name.toLowerCase() + "/members", "user_id=" + user.getJSONObject(0).getInt("id") + "&access_level=" + level);
		}
		catch(Exception e) {
			if(e.getMessage().indexOf("exists")<0) {
				throw e;
			}
		}
	}
	
	public void deletePipeline(String group, String name, String pipeline) throws Exception{
		
		 
		client.delete("/projects/" + group + "%2F" +   name.toLowerCase() +"/pipelines/" + pipeline);
	}
		
	public void createDirectory(String group, String name, String dir) throws Exception{
		 
	 
		JSONObject json = new JSONObject();
		json.put("branch", "master");
		 
		json.put("commit_message", "init");
		
		JSONObject action = new JSONObject();
		action.put("action","create");
		action.put("file_path", dir + "/README.md");
		action.put("content","");
		json.put("actions", new JSONArray().put(action));
		client.post("/projects/" +  group + "%2F" +   name.toLowerCase() +"/repository/commits", json.toString());
		
		/*POST /projects/:id/repository/commits
		 * {
  "branch": "master",
  "commit_message": "some commit message",
  "actions": [
    {
      "action": "create",
      "file_path": "foo/bar",
      "content": "some content"
    },
		 */
	}
	public Object createGroup(String id , String parent) throws Exception {
		//{"path": "<subgroup_path>", "name": "<subgroup_name>", "parent_id": <parent_group_id> }
		JSONObject group = new JSONObject();
		group.put("name", id);
		group.put("path", id);
		if(parent != null) {
			JSONObject parentInfo = client.getJSON("/groups/" + parent);
			if(parentInfo != null) {
				group.put("parent_id", parentInfo.get("id"));
			}
			else {
				throw new NotFoundException("parent:" + parent  );
			}
		}
		JSONObject user = (JSONObject) client.post("/groups", group.toString());
		return user.get("id");
	}
	public String createUser(String id , String email, String pwd) throws Exception {
		JSONObject json = new JSONObject();
		json.put("username", id);
		json.put("password", pwd);
		json.put("name", id);
		json.put("email", email);
		json.put("skip_confirmation", "true");
		JSONObject user = (JSONObject) client.post("/users", json.toString());
		String token =  createToken(user.getInt("id"));
		
		return token;
	}
	
	 
	public void createRepository(String group, String id) throws Exception
	{
		JSONObject json = new JSONObject();
		json.put("name", id);
		try { 
			
			
			client.post(  "/projects", json.toString());
			if(group != null) {
				movePrjToGroup(group, id);
			}
			else {
				group="root";
			}
		}
		catch(Exception e) {
			if(e.getMessage().indexOf("already")<0) {
				throw e;
			}
		}
		 
		
	}

	private String createToken(int id) throws Exception
	{
		JSONObject json = new JSONObject();
		json.put("name", "portal");
		JSONArray arr = new JSONArray();
		arr.put("read_repository");
		arr.put("write_repository");
		arr.put("api");
		json.put("scopes", arr);
		JSONObject token = (JSONObject) client.post("/users/" + id + "/impersonation_tokens", json.toString());
		return token.getString("token");
	}
	public String getGitURL(String group, String id , String usertoken)
	{
		int pos = gitUrl.indexOf(":");
		String tokenStr =  "";
		if(token != null || usertoken != null) {
			tokenStr = "gitlab-ci-token:" + (usertoken==null?token:usertoken) + "@";
		}
		return gitUrl.substring(0,pos+3) + tokenStr +  gitUrl.substring(pos+3) + "/" + group + "/" + id.toLowerCase() + ".git";
	}
	@Override
	public void setUrl(String url) {
		this.gitUrl = url;
		client = new RestClient( url + "/api/v4");
		
	}
	public JSONObject getPrj(String group, String repoName)  {
		try {
			JSONObject project = client.getJSON("/projects/" + group + "%2F" +   repoName.toLowerCase());
			return project;
		}catch(Exception e) {
			return null;
		}
	}
	public String getFileContent(String group, String repoName,  String branch,  String fileName) throws Exception{
	 
		return (String)client.get("/projects/" + group + "%2F" +   repoName.toLowerCase() + "/repository/files/" + replacePath(fileName) + "/raw?ref=" + branch);
	}
	 
	 
	public   void saveFile(String group,  String repoName, String branch, String fileName,  String content, String commitMsg) throws Exception{

		 
		JSONObject json = new JSONObject();
		json.put("file_path", fileName);
		json.put("branch", "master");
		json.put("content", content);
		json.put("commit_message", commitMsg);
		if(fileName.startsWith("/")) {
			fileName = fileName.substring(1);
		}
		try {
			getFileContent(group, repoName, branch, fileName);
			client.put("/projects/" + group + "%2F" +   repoName.toLowerCase() + "/repository/files/" + replacePath(fileName), json.toString());
		}
		catch(Exception e) {
			client.post("/projects/" + group + "%2F" +   repoName.toLowerCase()+ "/repository/files/" + replacePath(fileName), json.toString());
		}
		
		
	}
	public   void deleteFile(String group,  String repoName, String branch, String fileName, String commitMsg) throws Exception{
		JSONObject json = new JSONObject();
		json.put("file_path", fileName);
		json.put("branch", branch);
	 
		json.put("commit_message", commitMsg);
		if(fileName.startsWith("/")) {
			fileName = fileName.substring(1);
		}
		  
		client.delete("/projects/" + group + "%2F" +   repoName.toLowerCase() + "/repository/files/" + replacePath(fileName), json.toString() );
	}
	public   void newFile(String group,  String repoName, String branch, String fileName,  String content, String commitMsg) throws Exception{

		 
		JSONObject json = new JSONObject();
		json.put("file_path", fileName);
		json.put("branch", branch);
		json.put("content", content);
		json.put("commit_message", commitMsg);
		if(fileName.startsWith("/")) {
			fileName = fileName.substring(1);
		}
		 
		//getFileContent(group, repoName, branch, fileName);
		client.post("/projects/" + group + "%2F" +   repoName.toLowerCase() + "/repository/files/" + replacePath(fileName), json.toString());
		 
		
	}
	public   void newFile(String group,  String repoName, String branch, String fileName,  InputStream in, String commitMsg) throws Exception{

		 
		JSONObject json = new JSONObject();
		json.put("file_path", fileName);
		json.put("branch", branch);
		json.put("encoding", "base64");
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int size = 0;
		while((size = in.read(buf))>0) {
			out.write(buf, 0, size);
		}
		json.put("content", Base64.encode(out.toByteArray()));
		json.put("commit_message", commitMsg);
		if(fileName.startsWith("/")) {
			fileName = fileName.substring(1);
		}
		 
		//getFileContent(group, repoName, branch, fileName);
		client.post("/projects/" + group + "%2F" +   repoName.toLowerCase() + "/repository/files/" + replacePath(fileName), json.toString());
		 
		
	}
	/*public ZipInputStream getArchiveZip(String group, String repoName) throws Exception{
		//GET /projects/:id/repository/archive[.format]
		// <a rel="nofollow" download="" class="btn btn-xs btn-primary" href="/clovircm/apitest/-/archive/master/apitest-master.zip">zip</a>
		String baseUrl = client.getBaseUrl();
		int pos = baseUrl.indexOf("/", 10);
		byte[] zipByte = (byte[]) client.get("/../../" + group + "/" + repoName.toLowerCase() + "/-/archive/master/" + repoName.toLowerCase() + "-master.zip" );
		ByteArrayInputStream in = new ByteArrayInputStream(zipByte);
		return new ZipInputStream(in);
	}*/
	
	public void saveAs(String src_group, String src_repoName,String target_group, String target_repoName) throws Exception {
		copyFiles( src_group,   src_repoName,  target_group,   target_repoName, null);
	}
	
	protected void copyFiles(String src_group, String src_repoName,String target_group, String target_repoName, String path) throws Exception {
		JSONArray list = (JSONArray)client.get("/projects/" +   src_group + "%2F" +   src_repoName.toLowerCase()   + "/repository/tree" + ( path != null ? "?path=" + replacePath(path) : ""));
		 for(int i=0; i < list.length(); i++) {
			 JSONObject fileInfo = list.getJSONObject(i);
			 if("tree".equals(fileInfo.getString("type"))) {
				 copyFiles(  src_group,   src_repoName,  target_group,   target_repoName, fileInfo.getString("path"));
			 }
			 else {
				 Object content = client.get("/projects/" + src_group + "%2F" +   src_repoName.toLowerCase()  +"/repository/files/" + replacePath(fileInfo.getString("path")) +"/raw?ref=master");
				 if(content instanceof String) {
					 this.newFile(target_group, target_repoName, "master", fileInfo.getString("path"), (String)content, "copy from " + src_repoName );
				 }
				 else {
					InputStream in = new ByteArrayInputStream((byte[])content);
					this.newFile(target_group, target_repoName, "master", fileInfo.getString("path"), in , "copy from " + src_repoName );
				 }
			 }
		 }
	}
	public void deleteFiles(String src_group, String src_repoName, String path, String commitMsg) throws JSONException, Exception {
		JSONArray list = (JSONArray)client.get("/projects/" +   src_group + "%2F" +   src_repoName.toLowerCase()   + "/repository/tree" + ( path != null ? "?path=" + replacePath(path) : ""));
		 for(int i=0; i < list.length(); i++) {
			 JSONObject fileInfo = list.getJSONObject(i);
			 if(!"tree".equals(fileInfo.getString("type"))) {
				 deleteFile(src_group, src_repoName,  "master",fileInfo.getString("path"), commitMsg);
			 }
		 }
	}
	private String replacePath(String fileName) {
		fileName = fileName.replace("/","%2F");
		fileName = fileName.replace(".","%2E");
		return fileName;
	}
	public void newFileFromZip(String group,  String repoName, ZipInputStream zin) throws Exception {
		ZipEntry zipEntry;
		while((zipEntry=zin.getNextEntry())!=null) {
            

	        
	        if(zipEntry.isDirectory()) continue;
	       
	        String fileName =  zipEntry.getName();
	        int pos = fileName.indexOf("/");
	        newFile(group,repoName, "master", fileName.substring(pos), zin, "copy from " + fileName.substring(0, pos));
	         
	        
	         
		         
		 }
	}
	public String getJobLog(String group,  String repoName,  String jobId) throws Exception{
		return (String)client.get("/projects/"+  group + "%2F" +   repoName.toLowerCase() + "/jobs/" + jobId + "/trace");
	}
	public void importPrj(String prjId, String id, File file ) throws Exception{
		Map<String, Object> params = new HashMap();
		params.put("path", id);
		params.put("namespace", prjId);
		client.postFiles("/projects/import", params , new File[] { file });
	}
	@Override
	public boolean existFile(String gitGroup, String repoName,   String fileName) {
		try {
			JSONObject fileInfo = client.getJSON("/projects/" + gitGroup + "%2F" +   repoName.toLowerCase() + "/repository/files/" + replacePath(fileName) + "?ref=master");
			return true;
		}
		catch(Exception e) {
			System.out.print(e.getMessage());
			return false;
		}
	}
}

