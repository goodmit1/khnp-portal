
package com.clovirsm.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.fliconz.fm.mvc.DefaultService;

@Service
public class TaskService extends DefaultService {

	@Override
	protected String getTableName() {
		return "NC_TASK_ING";
	}

	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return new String[] {"TASK_ID"};
	}

	@Override
	protected String getNameSpace() {

		return "com.clovirsm.ingTask";
	}

	
	/**
	 * Hypervisor�۾��� ���� �Ǿ� NC_TASK_ING ������ ����
	 * @param taskId
	 */
	public void finishTask(String taskId )  {
		Map param = new HashMap();
		param.put("TASK_ID", taskId);
		
		try {
			this.deleteDBTable("NC_TASK_ING", param);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
		
		
	}
	public void updateRecheckTask(String taskId, String svcCd, String svcId, String tableNm, String pkKey  ) throws Exception  {
		updateStartTask(taskId, svcCd, svcId, tableNm, null);
		Map param = new HashMap();
		param.put("TABLE_NM", tableNm);
		param.put("PK_KEY", pkKey);
		param.put("SVC_ID", svcId);
		this.updateByQueryKey("update_TASK_ING", param);
	}
	/**
	 * Hypervisor�۾��� Async�� ���� �� ��� NC_TASK_ING�� ������ �߰�
	 * @param taskId
	 * @param svcCd
	 * @param svcId
	 * @param tableNm
	 * @param oldParam
	 */
	public void updateStartTask(String taskId, String svcCd, String svcId, String tableNm, Map oldParam )  {
		Map param = new HashMap();
		param.put("TASK_ID", taskId);
		param.put("SVC_ID", svcId);
		param.put("TASK_CD", svcCd);
		param.put("TABLE_NM", tableNm);
		try
		{
		
			this.insertDBTable("NC_TASK_ING", param);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	/**
	 * �۾� �������� TASK
	 * @return
	 * @throws Exception
	 */
	public List<Map> listRunningTask() throws Exception{
		return this.selectList("NC_TASK_ING", new HashMap());
	}
	
}
