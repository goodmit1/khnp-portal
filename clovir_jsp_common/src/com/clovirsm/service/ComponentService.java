package com.clovirsm.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.RestClient;
import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class ComponentService extends DefaultService {

	@Autowired CodeHandler codeHandler;
	protected String getNameSpace() {
		return "com.clovirsm.common.Component";
	}
	String currentSvr = null;
	@PostConstruct
	public void init()
	{
		currentSvr = System.getProperty("serverName");
		
		  
	}
	public String getDDExt(String ddId, String ddval) throws Exception {
		Map param = new HashMap();
		param.put("DD_ID", ddId);
		param.put("DD_VALUE", ddval);
		return (String) this.selectOneObjectByQueryKey("selectDDExt", param );
	}
	public String search(String indexNm, String filter) throws Exception{
		RestClient client = new RestClient(PropertyManager.getString("search.url","http://10.150.0.4:9200"));
		client.setMimeType("application/json");
		JSONObject json = new JSONObject();
		json.put("size", 10000);
		JSONObject query = new JSONObject();
		if(filter != null) {
			query.put("bool", ( new JSONObject()).put("must",  (new JSONObject()).put("match_all", new JSONObject())).put("filter", new JSONObject(filter)));
			json.put("query", query);
		}
		JSONArray list = new JSONArray();
		 
		JSONObject result = (JSONObject)client.post("/" + indexNm + "/_search", json.toString());
		JSONArray hits = result.getJSONObject("hits").getJSONArray("hits");
		for(int i=0; i < hits.length(); i++) {
			JSONObject source = hits.getJSONObject(i).getJSONObject("_source");
			 
			list.put(source);
		}
		return list.toString();
		 
	}
	boolean isScheduleServerOld = false;
	public   boolean isScheduleService() {
		String runSvr = (String)getEnv(codeHandler, "schedule_svr", null); 
		boolean isScheduleServer=false;
		if(runSvr == null)	{
			isScheduleServer= true;
			
		}
		else {
			isScheduleServer= runSvr.equals(currentSvr);
		}
		/*if(isScheduleServerOld && !isScheduleServer) { 
			
		}*/
		return isScheduleServer;
	}
	public static  Object getEnv(String name, Object defaultVal) {
		CodeHandler codeHandler = (CodeHandler)SpringBeanUtil.getBean("codeHandler");
		return getEnv(codeHandler, name, defaultVal);
	}
	protected static  Object getEnv(CodeHandler codeHandler, String name, Object defaultVal)
	{
		
		Map env  = codeHandler.getCodeList("ENV", "ko");
		if(env==null) return defaultVal;
		Object v =  env.get(name);
		if(v == null)
		{
			return defaultVal;
		}
		else
		{
			return v;
		}
	}


	Map teamMap;
	public Map getTeamList() throws Exception {
		if(teamMap == null)
		{
			Map<String, Object> paramMap =  new HashMap<String, Object>();
			List<Map> teamList = selectListByQueryKey("list_all_team", paramMap);

			//selectChildrenList(teamList, paramMap);
			teamMap = new LinkedHashMap<String, Object>();
			for(Map team: teamList){
				teamMap.put(team.get("TEAM_CD").toString(),team.get("TEAM_NM").toString());
			}
		}
		return teamMap;
	}

	Map teamMap2;
	public Map getTeamList(String type) throws Exception {
		if(teamMap2 == null)
		{
			Map<String, Object> paramMap =  new HashMap<String, Object>();

			List<Map> teamList = selectListByQueryKey("list_old_all_team", paramMap);

			teamMap2= new LinkedHashMap<String, Object>();
			for(Map team: teamList){
				teamMap2.put(team.get("TEAM_CD").toString(),team.get("TEAM_NM").toString());
			}
		}
		return teamMap2;
	}

	List<Map> specMapList;
	public List<Map> getSpecList() throws Exception {
		if(specMapList == null)
		{
			Map<String, Object> paramMap =  new HashMap<String, Object>();
			specMapList = selectListByQueryKey("list_all_spec", paramMap);
		}
		return specMapList;
	}


	private void selectChildrenList(List parentList, Map searchParam) throws Exception{
		List returnList = new ArrayList();
		for(Object obj: parentList){
			Map grid = (Map)obj;
			searchParam.put("PARENT_CD", grid.get("TEAM_CD"));
			List l = selectListByQueryKey("selectChildrenList", searchParam);
			if(l != null){
				if(l.size() > 0){
					selectChildrenList(l, searchParam);
				}
				returnList.addAll(l);
			}
		}
		parentList.addAll(returnList);
	}
	Map centers;
	public Map getDataCenterList() throws Exception{
		if(	centers == null)
		{
			centers = new HashMap();
			List<Map> centerList = (List<Map>)selectList("NC_DC", null);
			for(Map center: centerList){
				centers.put(center.get("DC_NM"), center.get("DC_ID"));
			}
		}
		return centers;
	}

	public Map getVMList(String DC_ID) throws Exception{
		Map vms = new HashMap();
		if(DC_ID != null && !"".equals(DC_ID)){
			Map param = new HashMap();
			param.put("DC_ID", DC_ID);
			List<Map> vmList = (List<Map>)selectList("NC_VM", param);
			for(Map vm: vmList){
				vms.put(vm.get("VM_NM"), vm.get("VM_ID"));
			}
		}
		return vms;
	}
	List<Map> compUserList;
	public List<Map> getcompUserList() throws Exception
	{
		if(compUserList == null)
		{
			compUserList = selectListByQueryKey("list_company_user", new HashMap());
		}
		return compUserList;
	}
	List<Map> compTeamUserList;
	public List<Map> getcompTeamUserList() throws Exception
	{
		if(compTeamUserList == null)
		{
			Map param = new HashMap();
			param.put("SAME_TEAM", "Y");
			compTeamUserList = selectListByQueryKey("list_company_user", param);
		}
		return compTeamUserList;
	}
	 
	public Map getNewSVCEtcFee(String SVC_CD) throws Exception {
		Map<String, String> feeTypeWithFeeMap = new LinkedHashMap<String, String>();
		Map param = new HashMap();
		param.put("SVC_CD", SVC_CD);
		List<Map> feeList = selectListByQueryKey("list_NEW_SVC_ETC_FEE", param);
		for(Map fee: feeList){
			feeTypeWithFeeMap.put((String)fee.get("FEE_TYPE_DESC"), (String)fee.get("FEE_TYPE_CD"));
		}
		return feeTypeWithFeeMap;
	}

	public Map getSVCFee(String tableNm, String key, String value) throws Exception {
		Map<String, String> feeTypeWithFeeMap = new LinkedHashMap<String, String>();
		Map param = new HashMap();
		param.put("TABLE_NM", tableNm);
		param.put("KEY", key);
		param.put("VALUE", value);
		List<Map> feeList = selectListByQueryKey("list_SVC_FEE", param);
		for(Map fee: feeList){
			feeTypeWithFeeMap.put((String)fee.get("FEE_TYPE_DESC"), (String)fee.get("FEE_TYPE_CD"));
		}
		return feeTypeWithFeeMap;
	}

	public Map getDiskTypeList() throws Exception {
		Map<String, String> diskMap = new LinkedHashMap<String, String>();
		List<Map> diskList = selectListByQueryKey("selectDiskTypeList", null);
		for(Map disk: diskList){
			diskMap.put(disk.get("DISK_TYPE_NM").toString(), disk.get("DISK_TYPE_ID").toString());
		}
		return diskMap;
	}

	public String getNowYear() {
		Calendar cal = Calendar.getInstance();

		return new Integer(cal.get(Calendar.YEAR)).toString();
	}

	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}

}