
package com.clovirsm.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.service.workflow.DefaultNextApprover;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class AlarmService extends DefaultService {

	
	@Override
	protected String getTableName() {
		return "NC_ALARM";
	}

	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return new String[] {"ALARM_ID"};
	}

	@Override
	protected String getNameSpace() {

		return "com.clovirsm.alarm";
	 
	}
	public void logAlarm(String msg, String type, Object svcId  )   {
		try {
			insertAlarm(msg, type, svcId, null );
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
	public void insertAlarm(String msg, String type, Object svcId, Object... rcvs  ) {	
		Map paramA = new HashMap();
		DefaultNextApprover approver = (DefaultNextApprover)SpringBeanUtil.getBean("nextApprover"); 
		Long id = Long.valueOf(System.nanoTime());
		paramA.put("ALARM_ID", id);
		paramA.put("ALARM_MSG", msg);
		paramA.put("ALARM_TYPE", type);
		paramA.put("ALARM_SVC_ID", svcId);
		try {
			this.insert(paramA);
			
			if(rcvs != null) {
				for(Object obj : rcvs) {
					Map paramU = new HashMap();
					paramU.put("ALARM_ID", id);
					paramU.put("USER_ID", obj);
					
					this.insertByQueryKey("insert_NC_ALARM_RCV", paramU);
				}
			}
			 
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
}
