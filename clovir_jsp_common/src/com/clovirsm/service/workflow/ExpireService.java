package com.clovirsm.service.workflow;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.ClassHelper;
import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.common.NCReqService;
import com.clovirsm.service.AlarmService;
import com.clovirsm.service.ComponentService;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class ExpireService extends NCDefaultService{
	@Autowired	AlarmService alarmService ;
	public void sendWarnMail(int day) throws Exception {
		Map param = new HashMap();
		param.put("DAY",day);
		List<Map> list  = this.selectListByQueryKey("list_NOT_EXPIRE", param);
		DefaultNextApprover approver = (DefaultNextApprover)SpringBeanUtil.getBean("nextApprover");
		for(Map m : list) {
			 
		 
			
			List targets =  approver.getExpireWarnMailTarget(m, param, false);
			for(Object target:targets) {
				sendMail(target, "title_expire_warn", "expire_warn", param);
			}
			
		}
	}
	public int update_reuse(  String tableNm, String pkKey, String pkVal,  int useMM) throws Exception {
		Map param = new HashMap();
		param.put("TABLE_NM", tableNm);
		param.put("PK_KEY", pkKey);
		param.put("PK_VAL", pkVal);
		param.put("DEL_YN", "N"); 
		param.put("ADD_USE_MM", useMM);
		 
		return updateByQueryKey("update_EXPIRED", param);	
	}
	public int update_expire(boolean isRealDelete, String tableNm, String pkKey, String pkVal,  String failMsg) throws Exception {
		Map param = new HashMap();
		param.put("TABLE_NM", tableNm);
		param.put("PK_KEY", pkKey);
		param.put("PK_VAL", pkVal);
		param.put("DEL_YN", "Y"); 
		if(failMsg != null) param.put("FAIL_MSG", failMsg);
		if(!isRealDelete) {
			param.put("EXPIRED_TMS", new Date()); 
		}
		return updateByQueryKey("update_EXPIRED", param);	
	}
	public void update_expire( ) throws Exception {
		Map param = new HashMap();
		param.put("DAY",0);
		List<Map> list  = this.selectListByQueryKey("list_NOT_EXPIRE", param);
		DefaultNextApprover approver = (DefaultNextApprover)SpringBeanUtil.getBean("nextApprover"); 
		String delete_day = (String)ComponentService.getEnv("exire.delete_day", "30");
		
		for(Map m : list) {
			boolean isExpire = this.expire((String)m.get("SVC_CD"), (String)m.get("SVC_ID"), true);
			
			List targets =  approver.getExpireWarnMailTarget(m, param, true);
			if(isExpire) {
				param.put("DELETE_DAY", delete_day);
			}
			else {
				param.remove("DELETE_DAY");
			}
			for(Object target:targets) {
				
				sendMail(target, "title_expire", "expire", param);
				alarmService.insertAlarm("expire " + m.get("TITLE") , (String)m.get("SVC_CD"), m.get("SVC_ID"), target);
			}
		
			
		}
		
		 
		
	}
	public void delete(int day ) throws Exception {
		Map param = new HashMap();
		
		param.put("DAY",day);
		List<Map> list  = this.selectListByQueryKey("list_DELETE_TARGET", param);
		for(Map m : list) {
			this.delete((String)m.get("SVC_CD"), (String)m.get("SVC_ID"));
			alarmService.insertAlarm("delete " + m.get("TITLE") , (String)m.get("SVC_CD"),  m.get("SVC_ID"),  m.get("INS_ID"));
		}
	}
	
	public void updateReUse(String svcCd, String pkVal, int mm ) throws Exception{
		ClassHelper.getSite( ).reuse(svcCd, pkVal, mm );
		
	}
	 

	public boolean expire(String svcCd, String pkVal, boolean bySchedule) throws Exception{
		return ClassHelper.getSite( ).expire(svcCd, pkVal, bySchedule );
	}
	public void delete(String svcCd, String pkVal) throws Exception{		
		ClassHelper.getSite( ).deleteAfterExpire(svcCd, pkVal );
 
	}

	@Override
	protected String getTableName() {
	 
		return "EXPIRED";
	}

	@Override
	public String[] getPks() {
		 
		return new String[] {"SVC_ID"};
	}

	@Override
	protected String getNameSpace() {
	 
		return "com.clovirsm.resource.expire";
	}
	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("P_KUBUN", "P_KUBUN");
		return config;
	}
}
