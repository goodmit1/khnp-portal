package com.clovirsm.service.workflow;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.security.UserVO;

@Service
public class WorkService extends DefaultService {

	@Autowired
	transient NextApproverService nextApproverService;
	@Autowired
	transient ApprovalService approvalService;

	protected String getNameSpace() {
		return "com.clovirsm.workflow.work.Work";
	}

	/**�옉�뾽�븿�뿉�꽌 �슂泥��꽌 �옉�꽦**/

	public void insertAuthRequest(List<Map> checkedMap, Map selRow) throws Exception{
	 

		long reqId = IDGenHelper.getId();
		selRow.put("REQ_ID", reqId);
		selRow.put("APPR_STATUS_CD", NCConstant.APP_KIND.R.toString());
		selRow.put("TEAM_CD", UserVO.getUser().getTeam().getTeamCd());
	
		

		for(Map reqSelRow: checkedMap){
			 
			NCReqService reqService = NCReqService.getService((String)reqSelRow.get("SVC_CD"));
			reqService.updateStatusReq((String)reqSelRow.get("SVC_ID"), (String)reqSelRow.get("INS_DT"));
			reqSelRow.put("REQ_ID", reqId);
			this.insertByQueryKey("insert_NC_REQ_DETAIL", reqSelRow);
			selRow.put("SVC_CD", (String)reqSelRow.get("SVC_CD"));
		}
		putContent(selRow );
		selRow.put("STEP", 0);
		StepInfo nextStep = nextApproverService.insertNextStepInfo(selRow);
		
		
		this.insertByQueryKey("insert_NC_REQ", selRow);
		 
		if(nextStep == null)
		{
			approvalService.updateDeploy(selRow, checkedMap);
		}
	}
	
	protected void putContent(Map selRow) throws Exception {
		String cmt = nextApproverService.getNextApproverAPI().getDetailMailContent( selRow.get("REQ_ID").toString(), null  );
		if(!TextHelper.isEmpty(cmt)){
			
		 
			selRow.put("CMT",  cmt);
		}
	}
	/**諛붾줈 �쟻�슜*/
	public void insertAuthRequest(Map selRow) throws Exception{

		long reqId = IDGenHelper.getId();
		selRow.put("REQ_ID", reqId);
		selRow.put("APPR_STATUS_CD", NCConstant.APP_KIND.R.toString());
		selRow.put("TEAM_CD", UserVO.getUser().getTeam().getTeamCd());
		

		NCReqService reqService = NCReqService.getService((String)selRow.get("SVC_CD"));
		reqService.updateStatusReq((String)selRow.get("SVC_ID"), (String)selRow.get("INS_DT"));
		this.insertByQueryKey("insert_NC_REQ_DETAIL", selRow);
	 
		putContent(selRow );
		this.insertByQueryKey("insert_NC_REQ", selRow);
	}

	/**�옉�뾽痍⑥냼**/
	public void updateWorkCancel(Set<Map> checkedMap) throws Exception{
		Iterator<Map> it = checkedMap.iterator();
		while(it.hasNext()){
			Map selRow = it.next();
			NCReqService service = NCReqService.getService((String)selRow.get("SVC_CD"));
			service.updateStatusCancel((String)selRow.get("SVC_ID"), (String)selRow.get("INS_DT"));
		}
	}

	@Override
	public String[] getPks() {
		return new String[]{"REQ_ID"};
	}

	@Override
	protected String getTableName() {

		return "NC_REQ";
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("CUD_CD", "CUD");
		config.put("SVC_CD", "SVC");
		config.put("FEE_TYPE_CD", "FEE");
		config.put("APPR_STATUS_CD", "APP_KIND");
		config.put("FEE", "FEE_FREE");
		return config;
	}

}