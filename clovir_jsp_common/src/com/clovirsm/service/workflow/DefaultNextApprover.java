package com.clovirsm.service.workflow;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.yaml.snakeyaml.util.UriEncoder;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.RestClient; 
import com.fliconz.fm.admin.schedule.ScheduleService;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.dao.MapResultHandler;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.QueryKeyHelper;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
 

public class DefaultNextApprover implements INextApprover{

	public static final String PARAM_NEXT_USER_APPROVER = "_NEXT_USER_APPROVER_";
	public static final String ROLE_POTAL_MANAGER ="ROLE_PORTAL_MANAGER";
	@Autowired SqlSessionTemplate sqlSession;
	
	String[] step;
	boolean isSameSkip=false;
	List stepNames;
	public DefaultNextApprover()
	{
		step = PropertyManager.getStringArray("workflow.line" );
		if(step == null) {
			step = new String[0];
		}
		isSameSkip = PropertyManager.getBoolean("workflow.isSameSkip", true);
	}
	public String[] getSteps() {
		return step;
	}
	public boolean isApprover()
	{
		for(String s:step)
		{
			if(UserVO.getUser().getRoleNames().contains(s))
			{
				return true;
			}
		}
		return false;
	}
	public List getExpireWarnMailTarget(  Map m, Map param, boolean isExpire) throws Exception {
		Map<String, Map> info =  getDetailList((String)m.get("SVC_CD"), (String)m.get("SVC_ID"), null);
		param.putAll(info);
		param.put("CMT", getDetailMailContent(null, info));
		
		List result = new ArrayList();
		result.add(m.get("INS_ID"));
		return result;
	}
	public List<String> getAllStepNames()
	{
		if(stepNames == null)
		{	
			stepNames = new ArrayList();
			if(step.length >=0  ) {
				Map param = new HashMap();
				param.put("ROLE_NAMES", step);
				param.put("_LANG_", UserVO.getUser().getLangKnd());
				MapResultHandler resultHandler = new MapResultHandler("ROLE_NM", "ROLE_DESC");
				sqlSession.select("com.clovirsm.workflow.approver.selectRoleNames", param, resultHandler);
				Map roleNames = resultHandler.getResult();
				
				for(String s:step)
				{
				 
					stepNames.add(roleNames.get(s)==null?s:roleNames.get(s));
				}
			}
		}
		return stepNames;
	}
	
	private int getNextStep(int startStep) //
	{
		for(int i=startStep; i < step.length; i++)
		{
			String next = step[i];
			if(UserVO.getUser().hasRole(next))
			{
				return i+1;
			}
		}
		return startStep;
	}
	
	protected void onBeforeGetNextApprover(int stepIdx, Map param) throws Exception {
		String nextSelRole = PropertyManager.getString("workflow.line.NEXT_APPR_ID");
		if(nextSelRole != null)
		{
			if(stepIdx < step.length && nextSelRole.equals(step[stepIdx]))
			{
				param.put(PARAM_NEXT_USER_APPROVER, param.get("NEXT_APPR_ID"));
			}
			 
		}
	
	}
	
	@Override
	public StepInfo getNextApprover(int stepIdx, Map param) throws Exception {
		
		 onBeforeGetNextApprover(  stepIdx,   param);
		if(!"".equals(param.get(param.get(PARAM_NEXT_USER_APPROVER))) && param.get(PARAM_NEXT_USER_APPROVER) != null)
		{
			StepInfo info = new StepInfo(stepIdx+1);
			String t = (String)param.get(PARAM_NEXT_USER_APPROVER);
			info.addApprover(t.split(","));
			return info;
		}
		stepIdx = getNextStep(stepIdx);
		if(stepIdx+1 > step.length) return null;
		if(stepIdx ==0 && UserVO.getUser().hasRole(ROLE_POTAL_MANAGER)) return null ; //��Ż������ ������ο� ������ �ٷ� �ݿ�
		String next = step[stepIdx];
		 
		 
		//stepIdx =NumberUtil.getInt( param.get(StepInfo.PARAM_STEP),stepIdx);
		StepInfo info = new StepInfo(stepIdx+1);
		info.setStepName(next);
		info.setApproverList(getMemberByRole(next, param));
		return info;
		 
	}
	
	public List getMemberByRole(String roleName, Map param) throws Exception
	{
		param.put("ROLE",  roleName);
		String queryKey = PropertyManager.getString("workflow.line.queryKey", "com.clovirsm.workflow.approver.selectUserByRole");
		return  sqlSession.selectList(queryKey, param);
	}
	@Override
	public void onAfterDeny(Map param) throws Exception {
		 
		 
	 
		service.sendMail(param.get("INS_ID"), "mail_title_req_deny", "req_deny", param);
		
	}
	
	protected String[] getReqSvcList() {
		return new String[] {"S","F","G" ,NCConstant.SVC.E.toString(),NCConstant.SVC.V.toString()};
	}
	public Map getDetailList(String reqId) throws Exception {
		 
		Map resultData = new HashMap(); 
		Map selRow = new HashMap();
		selRow.put("REQ_ID", reqId);
		String[] svcCds =  getReqSvcList() ;
		for(String svcCd : svcCds) {
			resultData.put(svcCd + "_list", service.getDetail(selRow, svcCd));
		}
	 
		return resultData;
	}
	
	@Override
	public Map getDetailList(String svcCd, String svcId, String insDt) throws Exception {
		 
		Map resultData = new HashMap(); 
		Map selRow = new HashMap();
		selRow.put("SVC_ID", svcId);
		selRow.put("INS_DT", insDt);
	 
		resultData.put( svcCd + "_list", service.getDetail(selRow, svcCd));
	 
		return resultData;
	}
	
	 
	@Override	 
	public String getDetailMailContent(String reqId,   Map result)   {
		try {
			String reqMailUrl = PropertyManager.getString("req_mail_url");
			if(reqMailUrl != null) {
				 
				if(reqId != null) {
					result = getDetailList(reqId); 
				}
				 
				RestClient client = new RestClient(reqMailUrl );
				client.setMimeType("application/x-www-form-urlencoded");
				try {
					 
					if(result != null) {
						
						
						return (String) client.post("",  "DATA=" + URLEncoder.encode(new JSONObject(result).toString(),"utf-8"));
					}
					 
					
				} catch (Exception e) {
					 
					e.printStackTrace();
					return null;
				}
				
			}
		}
		catch(Exception ignore) {
			System.out.println(ignore.getMessage());
		}
		return null;
	}
	 
	@Override
	public void onAfterDeploy(Map param, List<Map> details) throws Exception {
		 
		 
		
	} 
	@Autowired protected ApprovalService service;
	@Override
	public void onAfterRequest(List approverList,Map param) throws Exception {
		 
		 
		System.out.println(param);
		for(Object app: approverList) {
			service.sendMail(app, "mail_title_req", "req", param);
		}
	}
	@Override
	public String chkBeforeDeploy(List<Map> details) throws Exception {
			 
		for(Map m : details) {
		 
			String msg = NCReqService.getService((String)m.get("SVC_CD")).chkOnBeforeDeploy((String)m.get("SVC_ID"), (String)m.get("INS_DT"));
			if(msg != null) {
				return msg;
			}
			
		}
		return null;
		 
	}
}
