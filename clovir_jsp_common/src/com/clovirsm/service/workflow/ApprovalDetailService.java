package com.clovirsm.service.workflow;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;

@Service
public class ApprovalDetailService extends NCDefaultService {


	@Autowired NextApproverService nextApprover;
	protected String getNameSpace() {
		return "com.clovirsm.workflow.approval.ApprovalDetail";
	}



	@Override
	public String[] getPks() {
		return new String[]{"REQ_ID", "SVC_ID"};
	}

	@Override
	protected String getTableName() {
		return "NC_REQ_DETAIL";
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("CUD_CD", "CUD");
		config.put("SVC_CD", "SVC");
		config.put("APPR_CMT_CD", "APPR_COMMENT");
		return config;
	}

}