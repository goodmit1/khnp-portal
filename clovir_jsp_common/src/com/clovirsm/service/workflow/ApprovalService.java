package com.clovirsm.service.workflow;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.common.NCReqService;
import com.fliconz.fm.common.util.NumberHelper;
import com.fliconz.fw.runtime.util.MapUtil;
 

@Service
public class ApprovalService extends NCDefaultService {


	@Autowired NextApproverService nextApprover;
	protected String getNameSpace() {
		return "com.clovirsm.workflow.approval.Approval";
	}
	
	public List getDetail(Map param, String kubun) throws Exception{
		param.put("SVC_CD", kubun);
		param.putAll( NCReqService.getService(kubun).addTableParam(null, false)) ;
		List<Map> list = selectListByQueryKey("list_NC_REQ_DETAIL_" + kubun, param);
		addCodeNames(list);
		return list;
	}

	@Override
	public List list(Map searchParam) throws Exception {
		List<String> allStep = nextApprover.getAllStepNames();
		
		List<Map> result = super.list(searchParam);
		for(Map m:result)
		{
			if(m.get("STEP") != null)
			{	
				int step = NumberHelper.getInt(m.get("STEP"));
				if(allStep.size()>=step)
				{	
					m.put("STEP_NM", allStep.get(step-1));
				}
			}
		}
		return result;
	}

	public List<Map> getRequestDetailList(Map param) throws Exception{
		return this.selectListByQueryKey("list_NC_REQ_DETAIL", param);
	}

	public String chkBeforeDeploy(Map paramMap) throws Exception{
		List<Map> details = getRequestDetailList(paramMap);
		return nextApprover.getNextApproverAPI().chkBeforeDeploy(details);
	}
	public void updateDeny(Map paramMap) throws Exception{
		if(paramMap.get("INS_ID")==null) {
			Map m = this.getInfo(paramMap);
			MapUtil.copyNotExist(m, paramMap);
		}
		paramMap.put("APPR_STATUS_CD", NCConstant.APP_KIND.D.toString());	
		this.updateByQueryKey("update_NC_REQ_APPROVER", paramMap);
		this.updateByQueryKey("update_NC_REQ", paramMap);
		
		List<Map> details = getRequestDetailList(paramMap);
		for(Map<String, Object> m : details){
			NCReqService service = NCReqService.getService((String)m.get("SVC_CD"));
			service.updateDeny((String)m.get("SVC_ID"), (String)m.get("INS_DT"));
		}
		nextApprover.getNextApproverAPI().onAfterDeny(paramMap);
	}


	public void updateDeploy(Map selRow, List<Map> details) throws Exception
	{
		
		for(Map m : details){
			 
			NCReqService service = NCReqService.getService((String)m.get("SVC_CD"));
			service.updateDeploy((String)m.get("SVC_ID"), (String)m.get("INS_DT"));
		}
		nextApprover.getNextApproverAPI().onAfterDeploy(selRow,details);
	}

	public void updateApprove(Map paramMap) throws Exception{
		if(paramMap.get("INS_ID")==null) {
			Map m = this.getInfo(paramMap);
			MapUtil.copyNotExist(m, paramMap);
		}
		paramMap.put("APPR_STATUS_CD", NCConstant.APP_KIND.A.toString());	
		this.updateByQueryKey("update_NC_REQ_APPROVER", paramMap);
		List<Map> details = getRequestDetailList(paramMap);
		if(paramMap.get("SVC_CD") == null && details != null && details.size()>0) {
			paramMap.put("SVC_CD", details.get(0).get("SVC_CD"));
		}
		StepInfo nextStepInfo = nextApprover.insertNextStepInfo(paramMap);
		
		this.updateByQueryKey("update_NC_REQ", paramMap);
		
		if(nextStepInfo == null || nextStepInfo.getApproverList()==null || nextStepInfo.getApproverList().isEmpty())
		{
			
			updateDeploy(paramMap, details );

		}
		 
	}

	@Override
	public String[] getPks() {
		return new String[]{"REQ_ID"};
	}

	@Override
	protected String getTableName() {

		return "NC_REQ";
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("CUD_CD", "CUD");
		config.put("SVC_CD", "SVC");
		 
		config.put("FEE_TYPE_CD", "FEE");
		config.put("APPR_STATUS_CD", "APP_KIND");
		config.put("FEE", "FEE_FREE");
		
		
		return config;
	}

	

}