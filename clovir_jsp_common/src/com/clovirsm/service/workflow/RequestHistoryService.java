package com.clovirsm.service.workflow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.common.NCReqService;
import com.fliconz.fm.common.util.NumberHelper;
import com.fliconz.fm.common.util.TextHelper;
 

@Service
public class RequestHistoryService extends NCDefaultService {
	@Autowired NextApproverService nextApprover;
	protected String getNameSpace() {
		return "com.clovirsm.workflow.work.RequestHistory";
	}

	@Override
	public List list(Map searchParam) throws Exception {
		List<String> allStep = nextApprover.getAllStepNames();
		
		List<Map> result = super.list(searchParam);
		for(Map m:result)
		{
			if(m.get("STEP") != null)
			{	
				int step = NumberHelper.getInt(m.get("STEP"));
				if(step>0 && allStep.size()>0 && allStep.size()>step)
				{	
					m.put("STEP_NM", TextHelper.nvl( allStep.get(step-1), "" + step));
				}
			}
		}
		return result;
	}
	public List<Map<String, Object>> getRequestDetailList(Map param) throws Exception{
		return this.selectListByQueryKey("list_NC_REQ_DETAIL", param);
	}

	public void updateRework(Map paramMap) throws Exception{
		this.updateByQueryKey("update_NC_REQ", paramMap);
		List<Map<String, Object>> details = getRequestDetailList(paramMap);
		for(Map<String, Object> m : details){
			NCReqService service = NCReqService.getService((String)m.get("SVC_CD"));
			service.updateRework((String)m.get("SVC_ID"), (String)m.get("INS_DT"));
		}
	}

	public void updateCancel(Map paramMap) throws Exception{
		paramMap.put("APPR_STATUS_CD", NCConstant.APP_KIND.C.toString());
		this.updateByQueryKey("update_NC_REQ", paramMap);
		List<Map<String, Object>> details = getRequestDetailList(paramMap);
		for(Map<String, Object> m : details){
			NCReqService service = NCReqService.getService((String)m.get("SVC_CD"));
			service.updateStatusCancel((String)m.get("SVC_ID"), (String)m.get("INS_DT"));
		}
	}


	@Override
	public String[] getPks() {
		return new String[]{"REQ_ID"};
	}

	@Override
	protected String getTableName() {

		return "NC_REQ";
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("CUD_CD", "CUD");
		config.put("SVC_CD", "SVC");
		config.put("FEE_TYPE_CD", "FEE");
		config.put("APPR_STATUS_CD", "APP_KIND");
		config.put("FEE", "FEE_FREE");
		return config;
	}

}