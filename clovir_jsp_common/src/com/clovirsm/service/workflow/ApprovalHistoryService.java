package com.clovirsm.service.workflow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;
import com.fliconz.fm.common.util.NumberHelper;

@Service
public class ApprovalHistoryService extends NCDefaultService {
	@Autowired NextApproverService nextApprover;
	protected String getNameSpace() {
		return "com.clovirsm.workflow.approval.ApprovalHistory";
	}

	@Override
	public String[] getPks() {
		return new String[]{"REQ_ID"};
	}

	@Override
	protected String getTableName() {

		return "NC_REQ";
	}

	@Override
	public List list(Map searchParam) throws Exception {
		List<String> allStep = nextApprover.getAllStepNames();
		
		List<Map> result = super.list(searchParam);
		for(Map m:result)
		{
			if(m.get("STEP") != null )
			{	
				int step = NumberHelper.getInt(m.get("STEP"));
				
				if(allStep.size()>=step && step > 0)
				{	
					m.put("STEP_NM", allStep.get(step-1));
				}
			}
		}
		return result;
	}
	
	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("CUD_CD", "CUD");
		config.put("SVC_CD", "SVC");
		config.put("FEE_TYPE_CD", "FEE");
		config.put("APPR_STATUS_CD", "APP_KIND");
		config.put("FEE", "FEE_FREE");
		config.put("APPR_CMT_CD", "APPR_COMMENT");
		return config;
	}

}