package com.clovirsm.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;


@Service
public class QnaAnswerService extends NCDefaultService {

	protected String getNameSpace()
	  {
	    return "com.fliconz.fm.admin.qna";
	  }
	  
	  protected String getTableName()
	  {
	    return "FM_INQUIRY";
	  }
	  
	  protected int insertDBTable(String tableNm, Map param)
	    throws Exception
	  {
	    if (param.get("INQ_ID") == null) {
	      param.put("INQ_ID", Long.valueOf(System.nanoTime()));
	    }
	    return super.insertDBTable(tableNm, param);
	  }
	  
	  public String[] getPks()
	  {
	    return new String[] { "INQ_ID" };
	  }
	  
	  protected Map<String, String> getCodeConfig()
	  {
	    Map conf = new HashMap();
	    conf.put("SVC_CD", "SVC");
	    conf.put("INQ_CD", "INQ");
	    return conf;
	  }


	

}