package com.clovirsm.service.admin;


import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;

@Service
public class ServerSettingService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.setting.serverSetting";
	}

	@Override
	protected String getTableName() {
		return "FM_DDIC";
	}

	@Override
	public String[] getPks() {
		return new String[]{"DC_ID", "ID", "KUBUN"};
	}
}