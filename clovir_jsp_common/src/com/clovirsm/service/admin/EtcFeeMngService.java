package com.clovirsm.service.admin;
 

import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;

@Service
public class EtcFeeMngService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.etcfee.EtcFeeMng";
	}

	@Override
	protected String getTableName() {
		 
		return "NC_ETC_FEE";
	}

	@Override
	public String[] getPks() {
		return new String[]{"SVC_CD"};
	}

}