package com.clovirsm.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.hv.RestClient;
import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.ibm.icu.impl.Row;

@Service
public class DdicUserService extends NCDefaultService {
	
	@Autowired EtcConnMngService connService; 
	
	@Override
	protected String getNameSpace() {
		 
		return "com.clovirsm.admin.DdicUser";
	}

	public Map getInfo (String ddId, String ddvalue) throws Exception{
		Map param = new HashMap();
		param.put("DD_ID", ddId);
		param.put("DD_VALUE", ddvalue);
		return super.getInfo(param);
	}
	@Override
	public String[] getPks() {
		 
		return new String[]{"DD_ID","DD_VALUE"};
	}
	

	@Override
	protected String getTableName() {
		 
		return "FM_DDIC";
	}
	@Override
	public int insertOrUpdateMulti(List<Map> params) throws Exception{
		int row = super.insertOrUpdateMulti(params);
		reload(); 
		return row;
	}
	
	public class ReloadThread extends Thread{
		List<Map> cmList;
		public ReloadThread(List list) {
			this.cmList = list;
		}
		
		public void run() {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				 
			}
			for(Map m: cmList) {
				RestClient client = new RestClient((String)m.get("CONN_URL"));
				try {
					client.get("/api/guest/codeReload");
				}
				catch(Exception ignore) {
					System.out.println("Code Reload Error " + m.get("CONN_URL") + ":" + ignore.getMessage());
				}
			}
		}
	}
	public void reload() {
		Map param = new HashMap();
		param.put("CONN_TYPE", "Portal");
		try {
			List<Map> cmList = connService.selectListByQueryKey("list_LIVE_CONN", param);
			if(cmList.size()==0) {
				CodeHandler code = (CodeHandler)SpringBeanUtil.getBean("codeHandler");
				code.reload();
				
			}
			else {
				
				ReloadThread t = new ReloadThread(cmList);
				t.start();
			}
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
	@Override
	public int insertOrUpdate(Map param) throws Exception
	{
		int row = super.insertOrUpdate(param);
		reload(); 
		return row;
	  
	}
	@Override
	public int delete(Map param) throws Exception
	{
		int row = super.delete(param);
		reload(); 
		return row;
	}
}
