package com.clovirsm.service.admin;
 

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;

@Service
public class InquiryCmtService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.inquiryCmt";
	}

	@Override
	protected String getTableName() {
		 
		return "FM_INQUIRY_CMT";
	}

	@Override
	public String[] getPks() {
		return new String[]{"INQ_CMT_ID","INQ_ID"};
	}
	
	@Override
	public int insert(Map param) {
		int result = 0;
		if(param.get("INQ_CMT_ID") == null) {
			param.put("INQ_CMT_ID", "" + System.nanoTime());
		}
		int len = 0;
		
		if(param.get("INQ_SEQ") == null) {
			try {
				len = this.list_count(param);
				
				if(len == 0) {
					param.put("INQ_SEQ", ""+ 1);
				} else {
					param.put("INQ_SEQ", "" + (len+ 1));
				}
				
				result = super.insert(param);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			
			try {
				this.updateByQueryKey("update_commonet",param);
				result = super.insert(param);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	
	


}