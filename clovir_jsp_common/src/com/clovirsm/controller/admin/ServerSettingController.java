package com.clovirsm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.ServerSettingService;
import com.clovirsm.service.admin.SpecSettingService;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/server_setting" )
public class ServerSettingController extends DefaultController {

	@Autowired ServerSettingService service;
	@Override
	protected ServerSettingService getService() {
		return service;
	}

}
