package com.clovirsm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.EtcFeeMngService; 
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/etc_fee_mng" )
public class EtcFeeMngController extends DefaultController {

	@Autowired EtcFeeMngService service;
	@Override
	protected EtcFeeMngService getService() {
		 
		return service;
	}

}
