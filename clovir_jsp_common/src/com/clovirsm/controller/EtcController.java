package com.clovirsm.controller;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.ComponentService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/etc" )
public class EtcController extends DefaultController{

	@Autowired ComponentService service;
	@Override
	protected DefaultService getService() {
		return service;
	}
	@RequestMapping(value =  "/team_list" )
	public Object teamList(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return service.getTeamList();

			}

		}).run(request);

	}
	@RequestMapping(value =  "/team_list2" )
	public Object teamList2(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return service.getTeamList("old");
			}
		}).run(request);
	}

	@RequestMapping(value =  "/chatbot" )
	public void chatbot(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		response.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession();
		ChatBotBean bot = (ChatBotBean)session.getAttribute("ChatBotBean");
		if(bot == null) {
			bot = new ChatBotBean(session.getId());
			session.setAttribute("ChatBotBean", bot);
		}
		String q = request.getParameter("q");
		if(q != null) {
			bot.setCurrentIdx( Integer.parseInt(q));
		}
		PrintWriter out = response.getWriter();
		out.write(bot.answer( request.getParameter("a")));  
		 
		out.close();
		 
	}
	
	@RequestMapping(value =  "/get_fee" )
	public Object getFee(final HttpServletRequest request, HttpServletResponse response)
	{
		return  (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return service.selectOneObjectByQueryKey("getFee", param);

			}

		}).run(request);

	}
	@RequestMapping(value =  "/es_search" )
	public String getSearch(final HttpServletRequest request, HttpServletResponse response)
	{
		 try {
				return service.search((String)request.getParameter("table"),request.getParameter("filter"));

			}
		 catch(Exception e) {
			 e.printStackTrace();
			 return(new JSONObject(ControllerUtil.getErrMap(e))).toString();
		 }
	 

	}
	@RequestMapping(value =  "/spec_list" )
	public List specList(final HttpServletRequest request, HttpServletResponse response)
	{
		return (List)(new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return service.getSpecList();

			}

		}).run(request);

	}
}
