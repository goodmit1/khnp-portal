package com.clovirsm.controller.resource;
 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.controller.ReqController;
import com.clovirsm.service.resource.ExtendPeriodService;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/extendPeriod" )
public class ExtendPeriodController extends ReqController{

	@Autowired @Qualifier("extendPeriodService") ExtendPeriodService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

}
