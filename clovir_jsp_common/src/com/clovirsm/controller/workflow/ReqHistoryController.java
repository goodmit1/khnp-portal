package com.clovirsm.controller.workflow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.controller.ReqController;
 
import com.clovirsm.service.workflow.ReqHistoryService;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/resource/req_history" )
public class ReqHistoryController extends ReqController{

	@Autowired ReqHistoryService service;
	@Override
	protected DefaultService getService() {
		return service;
	}


}
