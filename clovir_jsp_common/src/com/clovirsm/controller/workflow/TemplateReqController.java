package com.clovirsm.controller.workflow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.controller.ReqController;
 
import com.clovirsm.service.workflow.TemplateReqService;
 
import com.clovirsm.util.JSONUtil;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/template_req" )
public class TemplateReqController extends DefaultController{

	@Autowired TemplateReqService service;
	@Override
	protected TemplateReqService getService() {
		return service;
	}
	
	

}
