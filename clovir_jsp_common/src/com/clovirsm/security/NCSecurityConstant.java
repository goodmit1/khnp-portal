package com.clovirsm.security;

import com.fliconz.fm.security.SecurityConstant;


public class NCSecurityConstant extends SecurityConstant {

	public static final String ROLE_ORG_MANAGER = "ROLE_ORG_MANAGER";
	public static final String ROLE_GRP_MANAGER = "ROLE_GRP_MANAGER";
	public static final String ROLE_GRP_MEMBER = "ROLE_GRP_MEMBER";
	public static final String ROLE_NO_GRP_MEMBER = "ROLE_NO_GRP_MEMBER";


}
