package com.clovirsm.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.quartz.CronExpression;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.web.context.ContextLoaderListener;

import com.clovirsm.service.ComponentService;
import com.clovirsm.service.ScheduleJspService;
import com.fliconz.fm.admin.schedule.ScheduleService;
import com.fliconz.fw.runtime.util.PropertyManager;

public class NCScheduleFactoryBean extends SchedulerFactoryBean {

	public NCScheduleFactoryBean() {
		super();
	}

 
	
 
	
	public void setTriggers(List<Map> list) throws Exception {
		
			removeAllTriggers() ;
			List<Trigger> triggers = new ArrayList();
			
			 
			for (Map<String, String> row : list) {
				try {
					CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
					MethodInvokingJobDetailFactoryBean job = new MethodInvokingJobDetailFactoryBean();
					trigger.setCronExpression(row.get("CRON"));
					CronExpression cronTrigger = new CronExpression((String)row.get("CRON"));
				    Date next = cronTrigger.getNextValidTimeAfter(new Date());
				    System.out.println(row + "=>Next Execution Time: " + next);
				    if(next == null) {
				    	continue;
				    }
				  
				    
					IJob j = ((IJob)Class.forName(PropertyManager.getString("Schedule." + row.get("KUBUN"))).newInstance());
					j.setId(row.get("ID"));
					job.setTargetObject(j);
					job.setTargetMethod(row.get("ACTION"));
					job.setName(row.get("ID"));
					job.afterPropertiesSet();
					trigger.setJobDetail(job.getObject());
					trigger.setName("trigger_" + row.get("ID"));
				 
					trigger.afterPropertiesSet();
					triggers.add(trigger.getObject());	
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
			setTriggers(triggers.toArray(new Trigger[] {}));
			if(super.getScheduler() != null) {
				this.setJobDetails(new JobDetail[] {});
				this.registerJobsAndTriggers();
			}
			/*else {
				JobListener globalJobListener = new JobListener() {
					
					@Override
					public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
						Map map = (Map) context.getResult();
						try {
							map.put("ID",  ((JobDetailImpl)context.getJobDetail()).getName());
							service.update(map);
						} catch (Exception e) {
							 
							e.printStackTrace();
						}
						
					}
					
					@Override
					public void jobToBeExecuted(JobExecutionContext context) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void jobExecutionVetoed(JobExecutionContext context) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public String getName() {
						 
						return "NCScheduleResultSaver";
					}
				};
				super.setGlobalJobListeners(globalJobListener);
			}*/
		 
		
	}
	public void removeAllTriggers() throws Exception{
		if(super.getScheduler() != null) {
			super.getScheduler().clear();
		}
	}
}
