package com.clovirsm.common;
 
import com.fliconz.fm.common.util.NumberHelper;

public class IPAddressUtil {
	/**
	 * IP주소의 첫 번째 IP(네트웍 IP)
	 * @param ip
	 * @param max
	 * @return
	 * @throws Exception
	 */
	public static String getStartIp(String ip, int max) throws Exception
	{

		int pos = ip.lastIndexOf(".");
		if(pos>0)
		{
			int last = Integer.parseInt( ip.substring(pos+1));
			int last1 = last % max ;
			
			return ip.substring(0, pos+1) + (last - last1  );

		}
		else
		{
			throw new Exception("invalid ip");
		}
	}
	
	/**
	 * 사용할 수 있는 첫번째 IP , getStartIp() 다음 아이피 
	 * @param ip
	 * @param max
	 * @return
	 * @throws Exception
	 */
	public static String getFirstIp(String ip, int max) throws Exception
	{

		int pos = ip.lastIndexOf(".");
		if(pos>0)
		{
			int last = Integer.parseInt( ip.substring(pos+1));
			int last1 = last % max ;
			
			return ip.substring(0, pos+1) + (last - last1 + 1);

		}
		else
		{
			throw new Exception("invalid ip");
		}
	}
	
	/**
	 * 마지막 IP
	 * @param ip
	 * @param max
	 * @return
	 * @throws Exception
	 */
	public static String getLastIp(String ip, int max) throws Exception
	{

		String firstIp = getFirstIp(ip, max);
		return getNextIp(firstIp, max, 0, max-2);
			}
	
	/**
	 * ip를 숫자로
	 * @param ipAddress
	 * @return
	 */
	public static long ipToLong(String ipAddress) {

		long result = 0;

		String[] ipAddressInArray = ipAddress.split("\\.");

		for (int i = 3; i >= 0; i--) {

			long ip = Long.parseLong(ipAddressInArray[3 - i]);

			//left shifting 24,16,8,0 and bitwise OR

			//1. 192 << 24
			//1. 168 << 16
			//1. 1   << 8
			//1. 2   << 0
			result |= ip << (i * 8);

		}

		return result;
	}
	
	/**
	 * 숫자를 IP로
	 * @param ip
	 * @return
	 */
	public static String longToIp(long ip) {

		return ((ip >> 24) & 0xFF) + "." 
				+ ((ip >> 16) & 0xFF) + "." 
				+ ((ip >> 8) & 0xFF) + "." 
				+ (ip & 0xFF);

	}
	/**
	 * 다음 Network 아이피
	 * @param ip
	 * @param max
	 * @return
	 * @throws Exception
	 */
	public static String getNextStartIp(String ip, int max) throws Exception
	{
		String result = getNextIp(ip, max, 0, max+1);
		int pos = result.lastIndexOf(".");
		return result.substring(0,pos) + "." + (NumberHelper.getInt(result.substring(pos+1))-1);
	}
	
	/**
	 * 서브넷 마스크
	 * @param cnt
	 * @return
	 * @throws Exception
	 */
	public static String getSubnetMask(int cnt) throws Exception
	{
		if(cnt>255*255)
		{
			return "255." + (255-(cnt/255/255)+1) + ".0.0";
		}
		else if(cnt>255)
		{
			return "255.255." + (255-(cnt/255)+1) + ".0";
		}
		return "255.255.255." + (256-cnt);
	}
	
	/**
	 * ip 다음 아이피, 해당 네트웍의 아이피가 없다면  null
	 * @param ip
	 * @param max
	 * @param excludeLast 마지막 제외여부
	 * @return
	 * @throws Exception
	 */
	public static String getNextIp(String ip, int max , boolean excludeLast) throws Exception
	{
		return getNextIp(ip, max, excludeLast?2:1, 1);
	}
	
	
	
	/**
	 * ip 다음 아이피, 해당 네트웍의 아이피가 없다면  null
	 * @param ip
	 * @param max
	 * @param excludeLast 마지막 제외여부
	 * @param plus 다음 몇 번째
	 * @return
	 * @throws Exception
	 */
	public static String getNextIp(String ip, int max , int excludeCnt, int plus) throws Exception
	{
		int pos = ip.lastIndexOf(".");
		if(pos>0)
		{
			int minus = excludeCnt;
			int last = Integer.parseInt( ip.substring(pos+1));
			int next = last + plus;
			
			if(  next % max == max-minus || next % max == 0)
			{
				return null;
			}
			else 
			{
				if(next>=255)
				{
					String[] arr = ip.split("\\.");
					return arr[0] + "." + arr[1] + "." + (Integer.parseInt(arr[2])+ (next/255)) + "." + (next%255);
				}
				return ip.substring(0, pos+1) + next;
			}
		}
		else
		{
			throw new Exception("invalid ip");
		}
	}

	private static boolean isAvailIpPart(long ip, int idx)
	{
		long ipPart = (ip >> idx) & 0xFF;
		if(ipPart>=255) return false;
		return true;
	}
	public static boolean isAvailableIp(long ip)
	{
		if(isAvailIpPart(ip, 24))
		{
			if(isAvailIpPart(ip, 16))
			{
				if(isAvailIpPart(ip, 8))
				{
					if(isAvailIpPart(ip, 0))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	public static void main(String[] args)
	{
		try {
			/*System.out.println(IPAddressUtil.getNextIp("10.0.0.0", 16, true));
			System.out.println(IPAddressUtil.getNextStartIp("10.0.0.0", 255*2 ));
			System.out.println(getLastIp("10.2.1.254", 255));
			System.out.println(getFirstIp("10.2.1.254", 255*10));
			System.out.println(getLastIp("10.2.1.254", 255*10));
			System.out.println(getNextIp("10.2.1.254", 255*10, 0, 1));*/
			long start_ipNum = IPAddressUtil.ipToLong("10.2.1.0");
			long end_ipNum = IPAddressUtil.ipToLong("10.3.1.0");
			for(long i=start_ipNum; i < end_ipNum ; i++)
			{
				if(!isAvailableIp(i)) continue;
				System.out.println(IPAddressUtil.longToIp(i));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
