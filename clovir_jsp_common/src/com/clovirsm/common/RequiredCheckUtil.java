package com.clovirsm.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RequiredCheckUtil {

	public static String[] requiredCheck(Map <String, ?> target, String... keys){
		if(keys != null && keys.length != 0 && target != null && !target.isEmpty()){
			List<String> notFound = new ArrayList<String>();
			for(String key : keys){
				if(!target.containsKey(key) || target.get(key) == null){
					notFound.add(key);
				}
			}
			if(notFound.size() != 0){
				return notFound.toArray(new String[]{});
			}
		}
		return null;
	}

	public static String[] requiredCheck(List<Map <String, ?>> targets, String... keys){
		for(Map target : targets){
			String[] tmp = requiredCheck(target, keys);
			if(tmp != null){
				return tmp;
			}
		}
		return null;
	}
}
