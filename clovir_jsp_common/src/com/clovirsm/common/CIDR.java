package com.clovirsm.common;

 
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.PropertyManager;

public class CIDR {

	String cidr;
	String subnetMask ;
	long networkIpNum ;
	int hostCount ;
	int startIdx = 0;
	int allHostCount;
	String networkip;
	public CIDR(String cidr)
	{
		this.cidr = cidr;
		int pos = cidr.lastIndexOf("/");
		int cnt1 = Integer.parseInt(cidr.substring(pos+1));
		String t = TextHelper.rpad(1, cnt1, '1');
		t = TextHelper.rpad(t, 32,'0');
		
		subnetMask = Integer.parseInt(t.substring(0,8), 2) +"."+ Integer.parseInt(t.substring(8,16), 2)+"."+ Integer.parseInt(t.substring(16,24), 2)+"."+ Integer.parseInt(t.substring(24), 2);
		networkip = cidr.substring(0, pos);
		
		int pos1 = cidr.indexOf("+");
		hostCount = (int) (Math.pow(2,32-cnt1)-2);
		allHostCount = hostCount;
		if(pos1>0)
		{
			networkip = cidr.substring(0, pos1);
			startIdx = Integer.parseInt(cidr.substring(pos1+1, pos));
			hostCount =  (int) (Math.pow(2,32-24)-2);
			if(allHostCount< startIdx*266)
			{
				throw new RuntimeException("Invalid CIDR");
			}
		}
		 
		
		networkIpNum = IPAddressUtil.ipToLong(networkip) ;
	}
	public String getSubnetMask()
	{
		return subnetMask;
	}
	public String getNWIp()
	{
		return networkip;
	}
	public String getGateWay(boolean isLast) throws Exception
	{
		if(isLast)
		{
			return IPAddressUtil.longToIp(networkIpNum+allHostCount);
		}
		else
		{
			return IPAddressUtil.longToIp(networkIpNum+1);
			
		}
	}
	public String getGateWay() throws Exception
	{
		String gateway = PropertyManager.getString("gatewayIp." + networkip);
		if(gateway == null) {
			return getGateWay(isGatewayLastIp());
		}
		else {
			return gateway;
		}
		
	}
	public static boolean isGatewayLastIp() throws Exception
	{
		
		return !"Y".equals(PropertyManager.getString("gatewayIp.firstYn","N"));
	}
	public int getAllIpCnt()
	{
		return hostCount;
	}
	public long getNthIpNum(int i)
	{
		return networkIpNum+i + (startIdx * 256);
	}
	
	
	public long lastHostIpNum()
	{
		return networkIpNum + hostCount+ (startIdx * 256);
	}
	
	public static void main(String[] args)
	{
		CIDR cidr = new CIDR("10.112.112.0+7/21");
		try {
			System.out.println(cidr.getGateWay(false));
			System.out.println(cidr.getSubnetMask( ));
			System.out.println(IPAddressUtil.longToIp(cidr.getNthIpNum(1)));
			System.out.println(IPAddressUtil.longToIp(cidr.lastHostIpNum()));
		} catch (Exception e) {
		 
			e.printStackTrace();
		}
		
	}
}
