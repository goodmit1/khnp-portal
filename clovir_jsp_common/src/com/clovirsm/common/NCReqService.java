package com.clovirsm.common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.fliconz.fm.admin.service.TeamService;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.security.NCSecurityConstant;
import com.clovirsm.service.workflow.ApprovalService;
import com.clovirsm.service.workflow.NextApproverService;
import com.clovirsm.service.workflow.WorkService;

public abstract class NCReqService extends NCDefaultService{


	protected abstract String getDefaultTitle(Map param);
	protected final String OWNER_FIELD = "INS_ID";
	protected String getStatusField(){
		return "TASK_STATUS_CD";
	}
	protected String getAppStatusField(){
		return "APP_STATUS_CD";
	}
	protected String getReqTitle(Map param)
	{
		Object o = this.getCodeMap("SVCCUD",  getLang()).get(this.getSVC_CD()+param.get("CUD_CD"));
		if(o == null) {
			o = (String)this.getCodeMap("SVC", getLang()).get(this.getSVC_CD()) +  this.getCodeMap("CUD", getLang()).get(param.get("CUD_CD")) ;
		}
		return  o + "(" + this.getDefaultTitle(param) + ")";

	}

	@Autowired
	transient WorkService workService;

	@Autowired
	transient ApprovalService approvalService;

	@Autowired
	transient NextApproverService nextApproverService;

	@Autowired
	transient TeamService teamService;

	public int insertReq1(Map selRow, boolean isDeploy) throws Exception
	{

		selRow.put("CUD_CD", NCConstant.CUD_CD.C.toString());
		selRow.put("APPR_STATUS_CD", NCConstant.APP_KIND.W.toString());
		int row =   saveReq(selRow);
		if(row>0 && isDeploy)
		{
			 update_deployReq(selRow);
		}
		return row;
	}

	public int updateReq1(Map selRow, boolean isDeploy) throws Exception
	{


		selRow.put("CUD_CD", NCConstant.CUD_CD.U.toString());
		selRow.put("APPR_STATUS_CD", NCConstant.APP_KIND.W.toString());
		int row =  saveReq(selRow);
		if(row>0 && isDeploy)
		{
			 update_deployReq(selRow);
		}
		return row;


	}
	public int deleteReq1(Map selRow, boolean isDeploy) throws Exception
	{


		selRow.put("CUD_CD", NCConstant.CUD_CD.D.toString());
		selRow.put("APPR_STATUS_CD", NCConstant.APP_KIND.W.toString());
		int row =   saveReq(selRow);
		if(row>0 && isDeploy)
		{
			update_deployReq(selRow);
		}
		return row;


	}
	public int deleteReqs(List<Map> selRows, boolean isDeploy) throws Exception
	{

		 return saveReqs( selRows, "D" ,   isDeploy) ;


	}
	public int saveReqs(List<Map> selRows, String cudCd , boolean isDeploy) throws Exception{
		int row = 0;
		for(Map selRow:selRows) {
			selRow.put("CUD_CD", cudCd);
			selRow.put("APPR_STATUS_CD", NCConstant.APP_KIND.W.toString());
			  row +=   saveReq(selRow);
		}
		if(row>0 && isDeploy)
		{
			 update_deployReq(selRows);
		}
		return row;
	}
	public void update_deployReq(  Map selRow) throws Exception
	{
		selRow.put("SVC_CD", this.getSVC_CD());
		boolean isDeploy = this.checkAutoApproval(this.getTableName(), selRow);
		if(!isDeploy)
		{
			List<Map> selected = new ArrayList();
			
			selRow.put("SVC_ID", selRow.get(this.getPks()[0]));
			selRow.put("REQ_NM", this.getReqTitle(selRow));

			selected.add(selRow);
			workService.insertAuthRequest(selected , selRow);
		}

	}

	protected void onAfterInsertReq(String tableNm, Map param) throws Exception {

	}
	public void update_deployReq(List<Map> selected) throws Exception {
		if(selected==null || selected.size()==0) return;
		Map reqMap = null;
		boolean isDeploy = false;
		StringBuffer svcNames = new StringBuffer();
		for(Map selRow:selected)
		{
			selRow.put("SVC_CD", this.getSVC_CD());
			selRow.put("SVC_ID", selRow.get(this.getPks()[0]));
			if(reqMap == null){
				reqMap = new HashMap();
				reqMap.putAll(selRow);
			}
			svcNames.append(selRow.get("SVC_NM")).append(",");
			isDeploy = this.checkAutoApproval(this.getTableName(), selRow);
		}
		if(!isDeploy)
		{
			svcNames.setLength(svcNames.length()-1);
			reqMap.put("SVC_NM", svcNames.toString());
			reqMap.put("REQ_NM", this.getReqTitle(reqMap) + (selected.size()>1 ? "(+" + (selected.size()-1) + ")":""));
			workService.insertAuthRequest(selected , reqMap);
		}
	}
	public Map addTableParam( String pkVal) {
		return addTableParam(pkVal, true);
	}
	public Map addTableParam( String pkVal, boolean req)
	{
		Map param = new HashMap();
		param.put("TABLE_NM", getTableName() +  (req ? "_REQ": "" ));
		param.put("PK_KEY", this.getPks()[0]);
		param.put("PK_VAL", pkVal);
		return param;
	}
	
	private Map addTableParamPKMulti(List pkVal)
	{
		Map param = new HashMap();
		Map pkParam = null;
		List pk = new ArrayList();
		param.put("TABLE_NM", getTableName()+ "_REQ");
		for(int i = 0 ; i < this.getPks().length ; i++) {
			pkParam = new HashMap();
			pkParam.put("PK_KEY", this.getPks()[i]);
			if(pkVal.get(i) != null)
				pkParam.put("PK_VAL", pkVal.get(i));
			else
				pkParam.put("PK_VAL", "");
			
			pk.add(pkParam);
		}
		param.put("PK", pk);
		return param;
	}
	
	public int updateOwner(   String pkVal,   String owner) throws Exception
	{
		Map param =addTableParam(pkVal);
		param.put("TABLE_NM", getTableName());
		param.put("OWNER", owner);
		//param.put("TEAM_CD", teamCd);
		int row = super.updateByQueryKey("com.clovirsm.workflow.work.Work", "updateOwner", param);
		String[] children = getChildTable();
		if(children != null)
		{
			for(String c:children)
			{
				param.put("TABLE_NM", c);
				super.updateByQueryKey("com.clovirsm.workflow.work.Work", "updateOwner", param);
			}
		}
		return row;

	}

	public int updateOwnerPKMulti(List pkVal,   String owner) throws Exception
	{
		Map param =addTableParamPKMulti(pkVal);
		param.put("TABLE_NM", getTableName());
		param.put("OWNER", owner);
		//param.put("TEAM_CD", teamCd);
		int row = super.updateByQueryKey("com.clovirsm.workflow.work.Work", "updateOwnerPKMulti", param);
		String[] children = getChildTable();
		if(children != null)
		{
			for(String c:children)
			{
				param.put("TABLE_NM", c);
				super.updateByQueryKey("com.clovirsm.workflow.work.Work", "updateOwnerPKMulti", param);
			}
		}
		return row;

	}

	protected String[] getChildTable()
	{
		return null;
	}
	public static NCReqService getService(String svcCd) throws Exception
	{
		String svcName = PropertyManager.getString("clovirsm.req.service." + svcCd);
		if(svcName == null) throw new NCException("Service is not found. property name=[clovirsm.req.service." + svcCd+"]");
		return (NCReqService)SpringBeanUtil.getBean(svcName);
	}
	public int deleteInCreate(String pkVal ) throws Exception
	{
		Map param = addTableParam(   pkVal);

		return super.updateByQueryKey("com.clovirsm.workflow.work.Work", "deleteInCreate", param);
	}
	public int updateStatusCancel(String pkVal, String date ) throws Exception
	{
		return this.updateStatus(pkVal, date,  NCConstant.APP_KIND.C.toString());
	}
	public int updateStatusReq(String pkVal, String date ) throws Exception
	{
		return this.updateStatus(pkVal, date,  NCConstant.APP_KIND.R.toString());
	}
	protected int updateStatusOK(String pkVal, String date ) throws Exception
	{
		return this.updateStatus(pkVal, date,  NCConstant.APP_KIND.A.toString());
	}
	protected int updateStatusDeny(String pkVal, String date ) throws Exception
	{
		return this.updateStatus(pkVal, date,  NCConstant.APP_KIND.D.toString());
	}
	protected int updateStatusRework(String pkVal, String date ) throws Exception
	{
		return this.updateStatus(pkVal, date,  NCConstant.APP_KIND.W.toString());
	}
	public void cancel(String pkVal, String date ) throws Exception
	{
		updateStatusCancel(pkVal, date);
	}
	protected int updateStatus(String pkVal, String date, String status ) throws Exception
	{
		Map param = addTableParam(   pkVal);
		param.put("APPR_STATUS_CD",status);
		param.put("INS_DT",date);
		return super.updateByQueryKey("com.clovirsm.workflow.work.Work", "updateApprStatus", param);
	}



	public void updateRework(String pkVal, String date ) throws Exception
	{
		updateStatusRework(pkVal, date);
	}
	public void updateDeny(String pkVal, String date) throws Exception
	{
		updateStatusDeny(pkVal, date);
	}

	protected String getWorkInstDt(String pkVal) throws Exception
	{
		Map param = addTableParam(   pkVal);
		return (String) super.selectOneObjectByQueryKey("com.clovirsm.common.Component", "select_W_INS_DT", param);
	}
	public boolean updateReDeploy( String pkVal, String date, String cudCd) throws Exception{
		Map info = this.getAllDetail(pkVal);
		
		Map param = new HashMap();
		param.put(this.getPks()[0], pkVal);
		param.put("TASK_STATUS_CD", "W");
		param.put("RUN_CD", "W");
		param.put("FAIL_MSG", "");
		this.update(param);
		boolean isOK = onReDeploy(pkVal,cudCd, info);
		if(isOK) {
			updateStatusOK(pkVal, date);
		}
		return isOK;
	}
	protected boolean onReDeploy( String pkVal, String cudCd, Map info) throws Exception {
		return onDeploy(pkVal,cudCd, info);
	}
	public boolean updateDeploy(String pkVal, String date) throws Exception
	{
		Map param = addTableParam(   pkVal);
		param.put("INS_DT",date);
		Map info = super.selectOneByQueryKey("com.clovirsm.common.Component", "selectByPrimaryKey", param);
		 
		onBeforeDeploy(info);
		
		int row = updateDBTable(getTableName(), info);
		if(row == 0)
		{
			insertDBTable(getTableName(), info);
		}
		updateStatusOK(pkVal, date);
		return onDeploy(pkVal,(String)info.get("CUD_CD"), info);
			 
		
	}
	protected void onBeforeDeploy(Map info) throws Exception{
		if(NCConstant.CUD_CD.D.toString().equals(info.get("CUD_CD")))
		{
			info.put("DEL_YN", "Y");
		}
		else
		{
			info.put("DEL_YN", "N");
		}
		if(info.get("TEAM_CD") != null && info.get("COMP_ID") == null)
		{
			Map teamInfo = teamService.selectInfo("FM_TEAM", info);
			if(teamInfo != null)
			{
				info.put("COMP_ID", teamInfo.get("COMP_ID"));
			}
		}

	}
	protected abstract boolean onDeploy(String pkVal, String cudCode, Map info) throws Exception;


	public abstract String getSVC_CD();

	public final Map setSelectedReq(String SVC_ID, String INS_DT) throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(this.getPks()[0], SVC_ID);
		param.put("INS_DT", INS_DT);
		Map result =  this.selectOneByQueryKey("selectByPrimaryKey_" + getTableName()+ "_REQ", param);
		Map old = this.selectOneByQueryKey("selectByPrimaryKey_" + getTableName(), param);
		if(old != null)
		{
			result.put("old", old);
		}
		return result;
	}

	protected final boolean checkAutoApproval(String tableNm, Map param) throws Exception{
		//�궗�슜�옄媛� 洹몃９ 愿�由ъ옄(寃곗옱沅뚯옄)硫�, 寃곗옱�슂泥��꽌瑜� �옉�꽦(insertAuthRequest)�썑, 寃곗옱(updateApprove)瑜� �옄�룞�쑝濡� �닔�뻾�븳�떎.

		if(nextApproverService.isAutoDeploy(param)){
			param.put("APPR_STATUS_CD", NCConstant.APP_KIND.A.toString());
			//super.updateDBTable(tableNm, param);

			param.put("SVC_ID", param.get(this.getPks()[0]));
			param.put("SVC_CD", this.getSVC_CD());
			param.put("REQ_NM", getReqTitle(param));
			workService.insertAuthRequest(param);
			approvalService.updateApprove(param);

			return true;
		}
		return false;
	}


	protected boolean hasEditableRight(Object teamCode, Object owner, Object taskstatusCd, Object appStatusCd, Map param) {
		Map categoryMap = (Map)UserVO.getUser().getMemberData().get("CATEGORY_MAP");
		if(categoryMap == null) {
			if(owner != null && owner.toString().equals(this.getUserVO().getUserId().toString()))
			{
				return true;
			}
			if(teamCode != null && UserVO.getUser().getRoleNames().contains(NCSecurityConstant.ROLE_GRP_MANAGER))
			{
				if(teamCode.toString().equals( UserVO.getUser().getTeam().getTeamCd().toString()))
				{
					return true;
				}
				if(UserVO.getUser().getChildrenTeamListMap().values().contains(teamCode.toString()))
				{
					return true;
				}
			}
			return false;
		}
		else {
			if(param.get("CATEGORY") != null) {
				return categoryMap.get(param.get("CATEGORY")) != null;
			}
			return true;
		}
	}
	public boolean isSavable(Object teamCode, Object owner, Object taskstatusCd, Object appStatusCd, Map param)
	{
		if(isAdmin()) return true;
		if("W".equals(taskstatusCd)){
			return false;
		}
		if("R".equals(appStatusCd)){
			return false;
		}
		if("W".equals(appStatusCd) && !owner.toString().equals(this.getUserVO().getUserId().toString())){
			return false;
		}
		
		return hasEditableRight(teamCode, owner, taskstatusCd, appStatusCd, param);
		
	}

	@Override
	public int update(Map param) throws Exception{
		if(isSavable(param)) {
			return super.update(param);
		}
		throw new Exception("Invaild right");
	}
	
	protected boolean isSavable(Map selRow) throws Exception
	{
		return isSavable(selRow.get("TEAM_CD"), selRow.get(OWNER_FIELD), selRow.get(getStatusField()),   selRow.get(getAppStatusField()), selRow);
	}
	protected int insertReqDBTable(String tableNm, Map param) throws Exception
	{

		int row = 0;
		if(param.get(this.getPks()[0]) != null)
		{
			row = super.updateDBTable(tableNm, param);
		}
		if(row == 0){
			//insert�븷 �븣 Key �깮�꽦
			if(param.get(this.getPks()[0]) == null || "C".equals(param.get("CUD_CD"))){

				genPK(tableNm, param);
			}
			row = super.insertDBTable(tableNm, param);
			param.put("INS_DT",param.get("_SYSDATE_"));
		}
		else
		{
			param.put("INS_DT",getWorkInstDt((String)param.get(this.getPks()[0])));
		}


		return row;
	}
	protected void genPK(String tableNm, Map param) throws Exception
	{
		this.chkDuplicate(tableNm.substring(0, tableNm.length()-"_REQ".length()), param, this.getDefaultTitle(param) );
		param.put(this.getPks()[0], IDGenHelper.getId(this.getSVC_CD()));
	}
	public int saveReq( Map param) throws Exception
	{
		return this.saveReq(this.getTableName()+"_REQ", param);
	}
	public int saveReq(String tableNm,Map param) throws Exception
	{

		int row = 0;
		if(!isSavable(param) && ("D".equals(param.get("CUD_CD")) || "U".equals(param.get("CUD_CD")))) throw new NCException(NCException.INVALID_OWNER_STATUS);
		if("D".equals(param.get("CUD_CD")))
		{

			row = this.deleteInCreate((String)param.get(this.getPks()[0]));
			if(row ==0)
			{
				row = this.insertReqDBTable(tableNm, param);
			}

		}
		else
		{
			row = this.insertReqDBTable(tableNm, param);

		}
		if(row>0)
		{
			if("C".equals(param.get("CUD_CD")))
			{
				this.onAfterInsertReq(tableNm, param);
			}
			else if("D".equals(param.get("CUD_CD")))
			{
				this.onAfterDeleteReq(tableNm, param);
			}
			else
			{
				this.onAfterUpdateReq(tableNm, param);
			}
		}
		return row;
	}

	@Override
	public Map getInfo(Map  param) throws Exception {

		Map info =  null;
		if(param.get("INS_DT") != null) {
			info = super.selectInfo(this.getTableName()+"_REQ", param);
			if(info != null)
			{	
				addCodeNames(info, getCodeConfig());
			}
		} else {
			info = super.getInfo(param);
			if(info != null)
			{	
				try {
					info.put("isSavable", this.isSavable(info));
					info.put("isOwner", this.isOwner(info));
				}
				catch(Exception ignore) {
					
				}
			}
		}
		return info;
	}
	protected void onAfterDeleteReq(String tableNm, Map param)  throws Exception{


	}
	protected void onAfterUpdateReq(String tableNm, Map param)   throws Exception{


	}
	@Override
	protected void addCommonParam(Map param) {
		super.addCommonParam(param);
		//SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMddHHmmss");
		//param.put("_SYSDATE_", sdf.format(  new java.util.Date() ));
	}
	
	public void delete4DeployFail(Map param) throws Exception{
		updateStatusCancel((String)param.get("SVC_ID"), (String)param.get("INS_DT"));
	}
	public Map getAllDetail(String id)  throws Exception{
		Map param = new HashMap();
		param.put(this.getPks()[0], id);
		return this.getInfo(param);
	}
	
	public String chkOnBeforeDeploy(String id, String instDt) throws Exception{
		return null;
	}
	
}
