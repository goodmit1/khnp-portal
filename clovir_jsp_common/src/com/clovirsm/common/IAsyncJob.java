package com.clovirsm.common;

import com.clovirsm.hv.IAfterProcess;

public interface IAsyncJob {

	IAfterProcess getAfter(  String id ) throws Exception;
}
