package com.clovirsm.common.chatbot;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import com.fliconz.fw.runtime.util.PropertyManager;

public class ChatBotConfigFactory {
	static JSONObject config;
	static ChatBotConfigFactory me;
	public static ChatBotConfigFactory getInstance() throws Exception {
		synchronized (ChatBotConfigFactory.class) {
			if(me==null) {
				me = new ChatBotConfigFactory();
			}
		}
		return me;
	}
	private  ChatBotConfigFactory() throws Exception {
		InputStream in = PropertyManager.class.getClassLoader().getResourceAsStream("config/chatbot.json");
		String source= IOUtils.toString(in, "utf-8");
		config = new JSONObject(source);
	}
	public String getBaseUrl() {
		return config.getString("baseUrl");
	}
	public JSONObject getQuestion(int idx) {
		return config.getJSONObject("question").getJSONObject("" + idx);
	}
	public int getQuestionCount() {
		return config.getJSONObject("question").length();
	}
	
}
