package com.vmware.sso.client.utils;

import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

 
/**
 * 속도 개선을 위해 추가
 * @author 윤경
 *
 */
public class Single {
	private static Single me1;
	JAXBContext jaxbContext;
	private Single() throws JAXBException
	{
		jaxbContext = JAXBContext
				.newInstance(Constants.WS_1_3_TRUST_JAXB_PACKAGE + ":"
						+ Constants.WSSE_JAXB_PACKAGE + ":"
						+ Constants.WSSU_JAXB_PACKAGE);
	}
	public static synchronized Single getInstance() throws JAXBException {
		 if(me1==null) me1 = new Single();
		 return me1;
	}
	public JAXBContext getJAXBContext()
	{
		return jaxbContext;
	}
}
