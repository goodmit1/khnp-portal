import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openstack4j.api.OSClient;
import org.openstack4j.model.compute.Flavor;
import org.openstack4j.model.compute.Image;
import org.openstack4j.model.compute.SecGroupExtension;
import org.openstack4j.model.compute.Server;
import org.openstack4j.model.network.Network;
import org.openstack4j.model.network.SecurityGroup;
import org.openstack4j.openstack.OSFactory;

public class Common {

	OSClient os;
	Map<String, String> imageMap;
	Map<String, String> flavorMap;
	private HashMap scGroupMap;
	public void connect()
	{
		os = OSFactory.builder()
                .endpoint("http://x86.trystack.org:5000/v2.0/")
                .credentials("facebook1101060473292215","25JXaCRuUF6Tj185")
                .tenantName("facebook1101060473292215")
                .authenticate();
	}
	protected Map getImageMap()
	{
		if(imageMap == null)
		{
			List<? extends Image> images = os.compute().images().list();
			imageMap = new HashMap();
			for(Image img : images)
			{
				imageMap.put(img.getName(), img.getId());
			}
		}
		return imageMap;
	}
	protected Map getFlavorMap()
	{
		if(flavorMap == null)
		{
			List<? extends Flavor> flavors = os.compute().flavors().list();
			flavorMap = new HashMap();
			for(Flavor f : flavors)
			{
				flavorMap.put(f.getName(), f.getId());
			}
		}
		return flavorMap;
	}
	protected String getSvrId(String name)
	{
		Map<String, String> filteringParams = new HashMap();
		filteringParams.put("name", name);
		List<? extends Server> list = os.compute().servers().list(filteringParams );
		if(list.size()>0)
		{
			return list.get(0).getId();
		}
		return null;
	}
	protected String getImageId(String imgName)
	{
		
		Map<String, String> map = this.getImageMap();
		return map.get(imgName);
	}
	protected String getSecurityGroupId(String name)
	{
		
		Map<String, String> map = this.getSCGroupMap();
		System.out.println(name + "=" + map.get(name));
		return map.get(name);
	}
	protected Map  getSCGroupMap() {
		if(scGroupMap == null)
		{
			List<? extends SecGroupExtension> scgroups = os.compute().securityGroups().list();
			scGroupMap = new HashMap();
			for(SecGroupExtension f : scgroups)
			{
				scGroupMap.put(f.getName(), f.getId());
			}
		}
		System.out.println(scGroupMap);
		return scGroupMap;
	}
	protected String getFlavorId(String name)
	{
		
		Map<String, String> map = this.getFlavorMap();
		return map.get(name);
	}
	protected String getNWId(String name)
	{
		
		Map<String, String> map = this.getNWMap();
		return map.get(name);
	}
	Map nwMap;
	protected Map<String, String> getNWMap() {
		if(nwMap == null)
		{
			List<? extends Network> nws = os.networking().network().list();
			nwMap = new HashMap();
			for(Network n : nws)
			{
				nwMap.put(n.getName(), n.getId());
			}
		}
		return nwMap;
	}
}
