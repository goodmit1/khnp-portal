package com.clovirsm.hv.vmware;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.DBPoolAPI;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.impl.VCheckTemplatePath;
import com.clovirsm.hv.vmware.impl.VChkTask;
import com.clovirsm.hv.vmware.impl.VCreateDVPortGroup;
import com.clovirsm.hv.vmware.impl.VCreateDisk;
import com.clovirsm.hv.vmware.impl.VCreateFolder;
import com.clovirsm.hv.vmware.impl.VCreateImage;
import com.clovirsm.hv.vmware.impl.VCreateNic;
import com.clovirsm.hv.vmware.impl.VCreateResourcePool;
import com.clovirsm.hv.vmware.impl.VCreateSnapshot;
import com.clovirsm.hv.vmware.impl.VCreateVM;
import com.clovirsm.hv.vmware.impl.VDeleteDisk;
import com.clovirsm.hv.vmware.impl.VDeleteDiskFromVM;
import com.clovirsm.hv.vmware.impl.VDeleteNic;
import com.clovirsm.hv.vmware.impl.VDeleteObject;
import com.clovirsm.hv.vmware.impl.VDeleteSnapShot;
import com.clovirsm.hv.vmware.impl.VGetAllPerfMetricInfo;
import com.clovirsm.hv.vmware.impl.VGetAllVMMaxPerf;
import com.clovirsm.hv.vmware.impl.VGetPerf;
import com.clovirsm.hv.vmware.impl.VGuestFileDownload;
import com.clovirsm.hv.vmware.impl.VGuestFileUpload;
import com.clovirsm.hv.vmware.impl.VGuestRun;
import com.clovirsm.hv.vmware.impl.VInfoSnapshotSize;
import com.clovirsm.hv.vmware.impl.VInfoVM;
import com.clovirsm.hv.vmware.impl.VListAlarm;
import com.clovirsm.hv.vmware.impl.VListDSAttribute;
import com.clovirsm.hv.vmware.impl.VListEvent;
import com.clovirsm.hv.vmware.impl.VListHostAttribute;
import com.clovirsm.hv.vmware.impl.VListLicense;
import com.clovirsm.hv.vmware.impl.VListObject;
import com.clovirsm.hv.vmware.impl.VListPerfHistory;
import com.clovirsm.hv.vmware.impl.VListTriggeredAlarm;
import com.clovirsm.hv.vmware.impl.VListVMDisk;
import com.clovirsm.hv.vmware.impl.VListVMInDC;
import com.clovirsm.hv.vmware.impl.VListVMIpInDC;
import com.clovirsm.hv.vmware.impl.VListVMPerfInDC;
import com.clovirsm.hv.vmware.impl.VMountDisk;
import com.clovirsm.hv.vmware.impl.VMoveVMIntoFolder;
import com.clovirsm.hv.vmware.impl.VOnStartUpProcess;
import com.clovirsm.hv.vmware.impl.VOpenConsole;
import com.clovirsm.hv.vmware.impl.VPowerOffVM;
import com.clovirsm.hv.vmware.impl.VPowerOnVM;
import com.clovirsm.hv.vmware.impl.VRebootVM;
import com.clovirsm.hv.vmware.impl.VReconfigCPUResourcePool;
import com.clovirsm.hv.vmware.impl.VReconfigVM;
import com.clovirsm.hv.vmware.impl.VRename;
import com.clovirsm.hv.vmware.impl.VRenameSnapshot;
import com.clovirsm.hv.vmware.impl.VRevertVM;
import com.clovirsm.hv.vmware.impl.VSizeUpVMDisk;
import com.clovirsm.hv.vmware.impl.VUnMountDisk;
import com.clovirsm.hv.vmware.log.LogInsightAPI;
import com.clovirsm.hv.vmware.nsx.NSXAPI;
import com.clovirsm.hv.vmware.rest.GetVMTags;
import com.clovirsm.hv.vmware.vni.VNIAPI;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.hv.vmware.vrops.VROpsAPI;
import com.vmware.connection.ConnectionFactory;
import com.vmware.connection.ConnectionMng; 

public class VMWareAPI extends VMWareCommon implements HypervisorAPI{
	public static String GUEST_USER_ID_PREFIX="user_";
	
	
	public IAPI getAPI(String kubun) throws Exception {
		//VRNI,		VROPS,		VRLI
		if(kubun.equals("VRA")) {
			return getVRA();
		}
		if(kubun.equals("VRNI")) {
			return getVNI();
		}
		if(kubun.equals("VROPS")) {
			return getVROps();
		}
		if(kubun.equals("NSX")) {
			return getNSX();
		}
		if(kubun.equals("VRLI")) {
			return getVLI();
		}
		if(kubun.equals("VRLI")) {
			return getVLI();
		}
		if(kubun.indexOf("DB")>=0) {
			return new DBPoolAPI();
		}
		return new CommonAPI();
		 
	}
	public LogInsightAPI getVLI() throws Exception {
		return (LogInsightAPI)HVProperty.getInstance().getOtherAPI(LogInsightAPI.getHVType(), LogInsightAPI.class);
	}
	public VRAAPI getVRA() throws Exception
	{
		 
		return (VRAAPI)HVProperty.getInstance().getOtherAPI(VRAAPI.getHVType(), VRAAPI.class);
	}
	public VNIAPI getVNI() throws Exception
	{
		 
		return (VNIAPI)HVProperty.getInstance().getOtherAPI(VNIAPI.getHVType(), VNIAPI.class);
	}
	public VROpsAPI getVROps() throws Exception
	{
		 
		return (VROpsAPI)HVProperty.getInstance().getOtherAPI(VROpsAPI.getHVType(),VROpsAPI.class);
	}
	public NSXAPI getNSX() throws Exception
	{
		return (NSXAPI)HVProperty.getInstance().getOtherAPI(NSXAPI.getHVType(), NSXAPI.class);
	}
 	public Map onFirstVM(ConnectionMng connectionMgr, Map param) throws Exception
	{
		
		Map result = new HashMap();
		if(param.get(VMWareAPI.PARAM_PORT_GROUP_NM) != null && !"".equals(param.get(VMWareAPI.PARAM_PORT_GROUP_NM)))
		{	
			try
			{
				VCreateDVPortGroup addVlan = new VCreateDVPortGroup(connectionMgr);
				
				addVlan.run(param, result);
			}
			catch(Exception ignore)
			{
				System.out.println(ignore.getMessage());
			}
		}
		if(param.get(VMWareAPI.PARAM_RESOURCEPOOL_NM) != null && !"".equals(param.get(VMWareAPI.PARAM_RESOURCEPOOL_NM)))
		{
			try
			{
				VCreateResourcePool resourcePool=new VCreateResourcePool(connectionMgr);
				resourcePool.run(param, result);
			}
			catch(Exception ignore)
			{
				System.out.println(ignore.getMessage());
			}
		}
		try
		{
			VCreateFolder folder=new VCreateFolder(connectionMgr);
			folder.run(param, result);
		}
	 
		catch(Exception ignore)
		{
			System.out.println(ignore.getMessage());
		}
		return result;
		 
	}
 	@Override
 	public Map getVMTags(Map param) throws Exception
 	{
 		VCRestConnection conn = null;
 		try	{
 			conn = connectRest(param);
 			Map result = new HashMap();
 			GetVMTags tags = new GetVMTags(conn);
 			tags.run(param, result);
 			return result;
 		}
 		finally {
 			disconnect(conn);
 		}
 		
 	}
 	@Override
	public Map chkTask(Map param) throws Exception
	{
 		VMWareConnection connectionMgr = null;
		try {
			connectionMgr = connect(param);
			return this.doProcess(param, null, VChkTask.class);
		}
		finally
		{
			disconnect(connectionMgr);
		}
	}
 	@Override
	public Map onFirstVM(Map param) throws Exception
	{
 		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param);
			return this.onFirstVM(connectionMgr, param);
		}
		finally
		{
			disconnect(connectionMgr);
		}
	}
	@Override
	public Map deleteOrg(Map param) throws Exception
	{
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param);
			Map result = new HashMap();
			VDeleteObject action = new VDeleteObject(connectionMgr);
			param.put(PARAM_OBJECT_TYPE, OBJ_TYPE_DVP);
			param.put(PARAM_OBJECT_NM, param.get(PARAM_PORT_GROUP_NM));
			
			action.run(param, result);
			 
			param.put(PARAM_OBJECT_TYPE, VMWareCommon.OBJ_TYPE_RP);
			param.put(PARAM_OBJECT_NM, param.get(PARAM_RESOURCEPOOL_NM));
			
			action.run(param, result);
			
			param.put(PARAM_OBJECT_TYPE,OBJ_TYPE_FOLDER);
			param.put(PARAM_OBJECT_NM, param.get(PARAM_FOLDER_NM));
			
			action.run(param, result);
			return result;
		}
		finally
		{
			disconnect(connectionMgr);
		}
	}
	@Override
	public Map deleteVM(Map param, VMInfo oldInfo) throws Exception
	{
		 
		param.put(PARAM_OBJECT_TYPE, VMWareCommon.OBJ_TYPE_VM);
		param.put(PARAM_OBJECT_NM, oldInfo.hostName);
		if(oldInfo != null)
		{
			param.put(PARAM_PLUS_CPU, oldInfo.cpuCnt*-1);
			return doProcess( param, oldInfo, VPowerOffVM.class,  VDeleteObject.class, VReconfigCPUResourcePool.class);
		}
		else
		{
			return doProcess( param, oldInfo, VPowerOffVM.class,VDeleteObject.class);
		}
	}
	@Override
	public Map deleteImage(Map param) throws Exception
	{
		param.put(HypervisorAPI.PARAM_VM_NM, (String)param.get(HypervisorAPI.PARAM_IMAGE_NM));
		return deleteObject(VMWareCommon.OBJ_TYPE_VM, null ,param);
		 
	}
	@Override
	public Map getSnapshotSize(Map param ) throws Exception
	{
		return doProcess( param, null, VInfoSnapshotSize.class);
		 
	}
	
	@Override
	public Map deleteSnapshot(Map param, VMInfo info) throws Exception
	{
		return doProcess( param, info, VDeleteSnapShot.class);
		 
	}
	
	protected Map deleteObject(String type, String name, Map param) throws Exception
	{
		param.put(PARAM_OBJECT_TYPE, type);
		if(name != null) param.put(PARAM_OBJECT_NM, name);
		return doProcess( param, null, VDeleteObject.class);
		 
	}
	@Override
	public Map powerOpVM(Map param, VMInfo info, String op) throws Exception
	{
		 Class cls = null;
		if(op.equals(POWER_ON))
		{
			cls = VPowerOnVM.class;
		}
		else if(op.equals(POWER_OFF))
		{
			cls = VPowerOffVM.class;
		} 
		else if(op.equals(POWER_REBOOT))
		{
			cls = VRebootVM.class;
		} 
		return doProcess( param,info, cls);
		
	}
	@Override
	public Map openConsole( Map param, VMInfo info) throws Exception
	{
		ConnectionMng connectionMgr = null;
		try
		{
			connectionMgr = connectNewGuest(param);
			Map result = new HashMap();
			CommonAction action = new VOpenConsole(connectionMgr);
		 	 
			action.run(param, result);
			return result;
		}
		catch(Exception e)
		{
			disconnectNew(connectionMgr);
			throw e;
		}
	}
	protected Map doProcess( Map param, VMInfo info , Class... classList) throws Exception
	{
		return doProcess(null, param, info, classList);
	}
	protected Map doProcess(IAfterProcess after, Map param, VMInfo info , Class... classList) throws Exception
		{	
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param);
			if(info != null)
			{
				param.put(PARAM_VM_NM, info.hostName);
			}
			Map result = new HashMap();
			for(Class cls : classList)
			{
				CommonAction action = (CommonAction)cls.getConstructor(ConnectionMng.class).newInstance(connectionMgr);
			 	if(after == null)
			 	{
			 		action.run(param, result);
			 	}
			 	else
			 	{
			 		action.run(param, result, after);
			 	}
			}
			return result;
		}
		finally
		{
			if(after == null) disconnect(connectionMgr);
		}
	}
	@Override
	public void onStartUpProcess(Map param)throws Exception
	{
		doProcess(param, null, VOnStartUpProcess.class);
	}
	@Override
	public Map reconfigVM(Map param, VMInfo oldInfo) throws Exception
	{
		param.put(PARAM_VM_NM, oldInfo.hostName);
		int cpu = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_CPU));
		int addOnCpu = 0;
		if(oldInfo != null)
		{
			addOnCpu = cpu - oldInfo.cpuCnt ;
		}
		if(addOnCpu != 0)
		{
			param.put(PARAM_PLUS_CPU, addOnCpu);
			return doProcess(param, oldInfo,VReconfigVM.class,   VReconfigCPUResourcePool.class);
		}
		else
		{
			return doProcess(param,oldInfo, VReconfigVM.class);
		}
	 
	}
	@Override
	public Map sizeUpVMDisk(Map param, VMInfo oldInfo ) throws Exception
	{
		return doProcess(param, oldInfo, VSizeUpVMDisk.class);
		
	 
	}
	@Override
	public List listVMDisk(Map param ) throws Exception
	{
		Map result = doProcess(param, null, VListVMDisk.class);
		return (List)result.get(PARAM_LIST);
	 
	}
	public List listAllPerfMetricInfo(Map param ) throws Exception
	{
		Map result = doProcess(param, null, VGetAllPerfMetricInfo.class);
		return (List)result.get(PARAM_LIST);
	 
	}
	@Override
	public Map createVM(IBeforeAfter before, boolean isDCFirst, Map param) throws Exception
	{
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param);			
			Map result = new HashMap();
			if(isDCFirst)
			{
				 
				this.onFirstVM(connectionMgr, param);
				
			}
			// resource pool 
			String resourcePool = (String)param.get(VMWareAPI.PARAM_RESOURCEPOOL_NM);
			if(resourcePool != null && !"".equals(resourcePool))
			{	
				VReconfigCPUResourcePool pool = new VReconfigCPUResourcePool(connectionMgr);
				pool.run(param, result);
			}
			
			CommonAction action = new VCreateVM(connectionMgr);
			if(before != null )
			{
				action.run(param, result, before.onAfterProcess("S","C", param));
			}
			else
			{
				action.run(param, result);
			}
			
		
			
			return result;
		}
		finally
		{
			if(before == null ) disconnect(connectionMgr);
		}
		
		
		 
	}
	/*@Override
	public Map createVMFromTemplate(IBeforeAfter before, boolean isDCFirst, Map param) throws Exception
	{
		param.put(PARAM_FROM, "//" + param.get(PARAM_REAL_DC_NM) + "//vm//" + param.get(PARAM_FOLDER_NM) + "//" + param.get(PARAM_IMAGE_NM)) ;
		return createVM(before, isDCFirst,  param);
	}*/
	@Override
	public Map createImage(IAfterProcess after,Map param, VMInfo info) throws Exception
	{
		param.put(PARAM_FROM,   info.hostName) ;
		return doProcess(after,  param,null,VCreateImage.class);
		 
	}
	@Override
	public Map addDisk(IAfterProcess after, Map param, VMInfo info) throws Exception
	{
		
		return doProcess(after, param,info, VCreateDisk.class);
		 
	}
	@Override
	public Map mountDisk(Map param, VMInfo info) throws Exception
	{
		return doProcess( param,info,VMountDisk.class);
		 
	}
	@Override
	public Map renameVM(Map param, VMInfo info) throws Exception
	{
		param.put(PARAM_OBJECT_TYPE, VMWareCommon.OBJ_TYPE_VM);
		return doProcess( param,info, VRename.class);
		 
	}
	@Override
	public Map revertVM(IAfterProcess after,Map param, VMInfo info) throws Exception
	{  
		return doProcess(after,  param,info,VRevertVM.class);
		 
	}
	@Override
	public Map renameSnapshot(Map param, VMInfo info) throws Exception
	{
		return doProcess(  param,info,VRenameSnapshot.class);
	}
	@Override
	public Map vmState(Map param, String... vms) throws Exception
	{
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(param);	
			
			Map result = new HashMap();
			 
			for(String vm : vms)
			{
				Map result1 = new HashMap();
				VInfoVM vmaction = new VInfoVM(connectionMgr);
				param.put(PARAM_VM_NM, vm);
				vmaction.run(param, result1);
				result.put(vm, result1);
			}
			 
			
			return result;
		}
		finally
		{
			disconnect(connectionMgr);
		}
	}
	@Override
	public Map deleteDisk(Map param, VMInfo info) throws Exception
	{
		Class cls = null;
		if(info == null)
		{
			cls = VDeleteDisk.class;
		}
		else
		{
			cls = VDeleteDiskFromVM.class;
			 
		}
		return doProcess(  param,info, cls);
	}
	@Override
	public Map createSnapshot( IAfterProcess after, Map param, VMInfo info) throws Exception {
		return doProcess(after, param,info, VCreateSnapshot.class);
		
	}
	@Override
	public Map addNic(Map param, VMInfo info) throws Exception {
		 
		return doProcess( param,info, VCreateNic.class);
	}
	@Override
	public Map deleteNic(Map param, VMInfo info) throws Exception {
		 
		return doProcess( param,info, VDeleteNic.class);
	}
	@Override
	public Map renameImg(Map param) throws Exception {
		param.put(PARAM_OBJECT_TYPE, VMWareCommon.OBJ_TYPE_VM);
		param.put(PARAM_VM_NM, param.get(PARAM_IMAGE_NM));
		return doProcess( param,null, VRename.class);
	}
	 
	/*public Map copyVM(Map param ) throws Exception {
		param.put(PARAM_FROM, "//" + param.get(PARAM_REAL_DC_NM) + "//vm//" + param.get(PARAM_FOLDER_NM) + "//" + param.get(PARAM_IMAGE_NM)) ;
		return createVM(null, false, param);
		
	}*/
	@Override
	public Map unmount(Map param, VMInfo info) throws Exception {
		return this.doProcess(param, info, VUnMountDisk.class);
	}
	@Override
	public Map getVMPerf(  Map param) throws Exception
	{
		return getPerf(VMWareCommon.OBJ_TYPE_VM, param);
	}
	@Override
	public Map getPerf(String type, Map param) throws Exception
	{
		String className = "com.clovirsm.hv.vmware.impl.VGetPerf" + type;
		
		return this.doProcess(param, null, Class.forName(className));
	}
	  
	@Override
	public Map listObject(  Map param) throws Exception
	{
		 
		return this.doProcess(param, null, VListObject.class);
	}
	@Override
	public Map listAllVMMaxPerf(  Map param) throws Exception
	{
		 
		return this.doProcess(param, null, VGetAllVMMaxPerf.class);
	}
	@Override
	public Map listPerfHistory( Map param) throws Exception
	{
		String type = getHVObjType((String)param.get(HypervisorAPI.PARAM_OBJECT_TYPE));
		param.put(HypervisorAPI.PARAM_OBJECT_TYPE, type);
		return this.doProcess(param, null, VListPerfHistory.class);
	}
	@Override
	public int getFirstNicId() throws Exception {
		return HVProperty.getInstance().getInt("vmware_firstNic_id", 4000 );
	}
	@Override
	public Map listEvent(  Map param) throws Exception {
	 
		String type =  getHVObjType((String)param.get(HypervisorAPI.PARAM_OBJECT_TYPE));
		param.put(HypervisorAPI.PARAM_OBJECT_TYPE, type);
		return this.doProcess(param, null, VListEvent.class);
	}
	@Override
	public Map listAlarm(  Map param) throws Exception {
	 
		 
		String type =  getHVObjType((String)param.get(HypervisorAPI.PARAM_OBJECT_TYPE));
		param.put(HypervisorAPI.PARAM_OBJECT_TYPE, type);
		return this.doProcess(param, null, VListTriggeredAlarm.class);
	}
	
	@Override
	public Map listAllAlarm(  Map param) throws Exception {
		String type =  VMWareCommon.OBJ_TYPE_DC;
		param.put(HypervisorAPI.PARAM_OBJECT_TYPE, type);
		param.put(HypervisorAPI.PARAM_OBJECT_NM, param.get("REAL_DC_NM"));
		return this.doProcess(param, null, VListTriggeredAlarm.class);
	}
	@Override
	public String[] getPerfHistoryTypes(String objType) throws Exception
	{
		String type =  getHVObjType(objType);
		return   HVProperty.getInstance().getStrings("vmware.perf.obj.history." + type);
	}
	@Override
	public String getHVObjType(String standardType) {
		switch(standardType)
		{
			case HypervisorAPI.OBJ_TYPE_DC:
				return VMWareCommon.OBJ_TYPE_DC;
			case HypervisorAPI.OBJ_TYPE_DS:
				return VMWareCommon.OBJ_TYPE_DS;
			case HypervisorAPI.OBJ_TYPE_CLUSTER:
				return VMWareCommon.OBJ_TYPE_CLUSTER;
			case HypervisorAPI.OBJ_TYPE_HOST:
				return VMWareCommon.OBJ_TYPE_HOST;
			case HypervisorAPI.OBJ_TYPE_VM:
				return VMWareCommon.OBJ_TYPE_VM;
			case HypervisorAPI.OBJ_TYPE_RP:
				return VMWareCommon.OBJ_TYPE_RP;
		}
		return standardType;
	}
	@Override
	public String getStandardObjType(String objType) {
		switch(objType)
		{
			case VMWareCommon.OBJ_TYPE_DC:
				return HypervisorAPI.OBJ_TYPE_DC;
			case VMWareCommon.OBJ_TYPE_DS:
				return HypervisorAPI.OBJ_TYPE_DS;
			case VMWareCommon.OBJ_TYPE_CLUSTER:
				return HypervisorAPI.OBJ_TYPE_CLUSTER;
			case VMWareCommon.OBJ_TYPE_HOST:
				return HypervisorAPI.OBJ_TYPE_HOST;
			case VMWareCommon.OBJ_TYPE_VM:
				return HypervisorAPI.OBJ_TYPE_VM;
			case VMWareCommon.OBJ_TYPE_RP:
				return HypervisorAPI.OBJ_TYPE_RP;
		}
		return objType;
	}
	@Override
	public Map checkTemplatePath(Map param) throws Exception {
		return this.doProcess(param, null, VCheckTemplatePath.class);
		
	}
	@Override
	public Map getDSAttribute(Map param) throws Exception  
	{
		return this.doProcess(param, null, VListDSAttribute.class);
		
	}
	@Override
	public Map getVMPerfListInDC(Map param) throws Exception  
	{
		return this.doProcess(param, null, VListVMPerfInDC.class);
		
	}
	@Override
	public Map getVMIpListInDC(Map param) throws Exception  
	{
		return this.doProcess(param, null, VListVMIpInDC.class);
		
	}
	@Override
	public Map getHostAttribute(Map param) throws Exception  
	{
		return this.doProcess(param, null, VListHostAttribute.class);
		
	}
	@Override
	public Map listLicense(Map param) throws Exception  
	{
		return this.doProcess(param, null, VListLicense.class);
		
	}

	@Override
	public Map getVMListInDC(Map param) throws Exception {
		return this.doProcess(param, null, VListVMInDC.class);
	}
	@Override
	public Map guestRun(Map vmInfo, String cmd, String param) throws Exception {
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(vmInfo);
			VGuestRun vm = new VGuestRun(connectionMgr) ;
			vmInfo.put("PROGRAM_PATH",cmd);
			vmInfo.put("PROGRAM_ARG",param == null?"":param);
			Map result = new HashMap();
			vm.run(vmInfo, result);
			return result;
		}
		finally
		{
			disconnect(connectionMgr);
		}
		
	}
	@Override
	public void guestUpload(Map vmInfo, String svrPath, String localPath) throws Exception {
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(vmInfo);
			VGuestFileUpload vm = new VGuestFileUpload(connectionMgr) ;
			vmInfo.put("FROM_PATH",localPath);
			vmInfo.put("TO_PATH",svrPath);
			Map result = new HashMap();
			vm.run(vmInfo, result);
		}
		finally
		{
			disconnect(connectionMgr);
		}
		
	}
	@Override
	public void guestDownload(Map vmInfo, String svrPath, String localPath) throws Exception {
		VMWareConnection connectionMgr = null;
		try
		{
			connectionMgr = connect(vmInfo);
			VGuestFileDownload vm = new VGuestFileDownload(connectionMgr) ;
			vmInfo.put("FROM_PATH",svrPath);
			vmInfo.put("TO_PATH",localPath);
			Map result = new HashMap();
			vm.run(vmInfo, result);
		}
		finally
		{
			disconnect(connectionMgr);
		}
		
	}
	@Override
	public void connectTest(Map param) throws Exception {
		VMWareConnection connectionMgr   = new VMWareConnection();
		connectionMgr.connect((String)param.get(HypervisorAPI.PARAM_URL), (String)param.get(HypervisorAPI.PARAM_USERID), (String)param.get(HypervisorAPI.PARAM_PWD));
		disconnect(connectionMgr);
	}
	@Override
	public Map moveVMinfoFolder(Map param, VMInfo info) throws Exception {
		VMWareConnection connectionMgr = null;
		try {
			connectionMgr = connect(param);
			return this.doProcess(param, info, VMoveVMIntoFolder.class);
		}
		finally
		{
			disconnect(connectionMgr);
		}
	}
}
