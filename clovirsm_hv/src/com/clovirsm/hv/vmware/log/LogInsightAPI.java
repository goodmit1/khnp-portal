package com.clovirsm.hv.vmware.log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.IAPI;
import com.clovirsm.hv.IConnection;
 

public class LogInsightAPI extends CommonAPI {
	public static String getHVType() {
		 
		return "V.VNI";
	}
	protected  String getConnectionName() {
		return VLogConnection.class.getName();
	}
	public List<Object[]> getLog(Map param,String field, String value, long start_timestamp, long end_timestamp) throws Exception{
		VLogConnection conn = (VLogConnection) connect(param);
		String path = "/api/v1/events/" + field + "/CONTAINS%20" + value +  (start_timestamp >0 ? "/timestamp/%3E%3D" + start_timestamp : "" ) + ( end_timestamp >0 ? "/timestamp/%3C%3D" + end_timestamp : "") +"?order-by-direction=ASC&view=SIMPLE";
		JSONObject res = (JSONObject)conn.getRestClient().get(path);
		List result = new ArrayList();
		org.json.JSONArray list =  res.getJSONArray("results");
		for(int i=0 ; i < list.length(); i++) {
			JSONObject result1 = list.getJSONObject(i);
			result.add(new Object[] { new Date(result1.getLong("timestamp")) ,result1.getString("text")});
		}
		disconnect(conn);
		return result;
	}
	
	 
}
