package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.GuestAuthentication;
import com.vmware.vim25.GuestOperationsFaultFaultMsg;
import com.vmware.vim25.GuestProcessInfo;
import com.vmware.vim25.GuestProgramSpec;
import com.vmware.vim25.InvalidStateFaultMsg;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.NamePasswordAuthentication;
import com.vmware.vim25.RuntimeFaultFaultMsg;
import com.vmware.vim25.TaskInProgressFaultMsg;
import com.vmware.vim25.TicketedSessionAuthentication;
import com.vmware.vim25.VirtualMachineTicket;

public class VGuestRun extends com.clovirsm.hv.vmware.CommonAction{

	public VGuestRun(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		ManagedObjectReference vm = super.getVM(param);
	 
		NamePasswordAuthentication auth = new NamePasswordAuthentication();
		 
		this.runCmd(vm, (String)param.get("GUEST_ID"), (String)param.get("GUEST_PWD"), (String)param.get("PROGRAM_PATH"), (String)param.get("PROGRAM_ARG"), result);
		return null; 
	}
	
	protected Integer runCmd (ManagedObjectReference vm, String id, String pwd, String path, String args, Map result) throws Exception{
		NamePasswordAuthentication auth = new NamePasswordAuthentication();
		 
		auth.setUsername(id);
		auth.setPassword(pwd);
		GuestProgramSpec prog = new GuestProgramSpec();
		prog.setProgramPath(path);
		prog.setArguments(args);
		ManagedObjectReference mor = (ManagedObjectReference)super.getProp(super.connectionMng.getServiceContent().getGuestOperationsManager(), "processManager");
		
		long pid = super.vimPort.startProgramInGuest(mor, vm, auth, prog );
		List<Long> pids = new ArrayList();
		pids.add(pid);
		
		Integer exitCode = getExitCode(0 , mor, vm, auth, pids);
		if(result != null) {
			result.put("PID", pid);
			result.put("EXIT_CODE", exitCode);
		}
	
		return exitCode;
		
	}
	protected Integer getExitCode( int idx, ManagedObjectReference mor,  ManagedObjectReference vm, GuestAuthentication auth,List<Long> pids) throws Exception
	{
		if(idx>5) return null;
		Thread.sleep(1000); 
		List<GuestProcessInfo> pInfoList = super.vimPort.listProcessesInGuest(mor, vm, auth, pids );
		if(pInfoList.size()>0)
		{
			 Integer exitCode = pInfoList.get(0).getExitCode();
			 if(exitCode == null)
			 {
				 return getExitCode(++idx, mor, vm, auth, pids);
			 }
			 else
			 {
				 return exitCode;
			 }
		}
		return null;
	}

}
