package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfManagedObjectReference;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.VirtualMachineConfigInfo;

public class VListTemplate extends com.clovirsm.hv.vmware.CommonAction{

	public VListTemplate(ConnectionMng m) {
		super(m);
		 
	}
	private void addVMInFolder(List<String>templateList, String folderName, ManagedObjectReference vmFolderRef) throws Exception
    {
    	ArrayOfManagedObjectReference children1 = (ArrayOfManagedObjectReference) super.getProp(vmFolderRef, "childEntity");
    	List<ManagedObjectReference> list = children1.getManagedObjectReference();
    	for(ManagedObjectReference m : list)
    	{
    		if(m.getType().equals(VMWareCommon.OBJ_TYPE_FOLDER))
    		{
    			addVMInFolder(templateList, folderName + "/" + super.getProp(m, "name"), m);
    		}
    		else if(m.getType().equals(VMWareCommon.OBJ_TYPE_VM))
    		{
    			VirtualMachineConfigInfo config = (VirtualMachineConfigInfo)super.getProp(m, "config");
    			if(config.isTemplate())
    			{
    				templateList.add(folderName + "/" + config.getName());
    			}
    		}
    	}
    }
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		String folderName = (String)param.get(VMWareCommon.PARAM_FOLDER_NM);
		ManagedObjectReference vmFolderRef = super.findObjectFromRoot(VMWareCommon.OBJ_TYPE_FOLDER, folderName);
    	List<String> templateList = new ArrayList();
		addVMInFolder(templateList , folderName, vmFolderRef);
		System.out.println(templateList);
		result.put(HypervisorAPI.PARAM_LIST, templateList);
		return null;
	}
}
