package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

/**
 * 스냅샷에서 복구
 * @author 윤경
 *
 */
public class VRevertVM extends VDeleteSnapShot{

	public VRevertVM(ConnectionMng m) {
		super(m);
		 
	}
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		 
		return revertSnapshot(super.getVM(param),(String)param.get(HypervisorAPI.PARAM_SNAPSHOT_NM));
		
	}
	
	protected ManagedObjectReference revertSnapshot(ManagedObjectReference vmRef, String snapshotname) throws Exception
	{
		 
	 
		ManagedObjectReference snapmor = getSnapshotReference(vmRef,  snapshotname);
	    if (snapmor != null) {
	    	ManagedObjectReference taskMor =
                    vimPort.revertToSnapshotTask(snapmor, null, true);
	        return taskMor;
	  		
	    }
	    return null;
	}
}
