package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.AlreadyExistsFaultMsg;
import com.vmware.vim25.DVPortgroupConfigSpec;
import com.vmware.vim25.HostConfigFaultFaultMsg;
import com.vmware.vim25.HostConfigManager;
import com.vmware.vim25.HostNetworkPolicy;
import com.vmware.vim25.HostPortGroupSpec;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.RuntimeFaultFaultMsg;
import com.vmware.vim25.VMwareDVSPortSetting;
import com.vmware.vim25.VMwareDVSPortgroupPolicy;
import com.vmware.vim25.VmwareDistributedVirtualSwitchVlanIdSpec;

/**
 * 분산 포트 그룹 추가
 * @author 윤경
 *
 */
public class VCreateDVPortGroup extends com.clovirsm.hv.vmware.CommonAction{

	public VCreateDVPortGroup(ConnectionMng m) {
		super(m);

	}
	

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map  param,Map result) throws Exception {

		int vlanId = (int)param.get(VMWareAPI.PARAM_VLAN_ID);
		String portName = (String)param.get(VMWareAPI.PARAM_PORT_GROUP_NM);
		
		addDVPortGroup(dc,cluster,(String)param.get(VMWareAPI.PARAM_VIRTUALSWITCH_ID),portName, 
				vlanId,
				result);
		return null;
	}



	protected void addDVPortGroup(String dcName, String cluster, String virtualswitchid, String portgroupname , int vlanId, Map result ) throws Exception
	{
		ManagedObjectReference dvsMor = findObjectFromRoot(VMWareAPI.OBJ_TYPE_DVS,virtualswitchid);

		if (dvsMor != null) {
			DVPortgroupConfigSpec portGroupConfigSpec =
					new DVPortgroupConfigSpec();
			VMwareDVSPortgroupPolicy policy = new VMwareDVSPortgroupPolicy();
			policy.setVlanOverrideAllowed(true);
			portGroupConfigSpec.setPolicy(policy);
			portGroupConfigSpec.setName(portgroupname);
			portGroupConfigSpec.setNumPorts(16);
			VmwareDistributedVirtualSwitchVlanIdSpec vlanspec = new VmwareDistributedVirtualSwitchVlanIdSpec();
			vlanspec.setVlanId(vlanId);
			vlanspec.setInherited(false);
			VMwareDVSPortSetting portSetting = new VMwareDVSPortSetting();
			portSetting.setVlan(vlanspec);
			portGroupConfigSpec.setDefaultPortConfig(portSetting);
			portGroupConfigSpec.setType("earlyBinding");

			List<DVPortgroupConfigSpec> listDVSPortConfigSpec =
					new ArrayList<DVPortgroupConfigSpec>();
			listDVSPortConfigSpec.add(portGroupConfigSpec);

			ManagedObjectReference taskmor =
					vimPort.addDVPortgroupTask(dvsMor, listDVSPortConfigSpec);

			getTaskResultAfterDone(taskmor, "CreateDVPortGroup:" + portgroupname);
		}

	}


}
