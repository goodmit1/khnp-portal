package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

/**
 * VM정지
 * @author 윤경
 *
 */
public class VPowerOffVM extends VPowerOnVM{

	public VPowerOffVM(ConnectionMng m) {
		super(m);
		 
	}
	protected void onAfter(ManagedObjectReference vmMor,Map result) throws Exception
	{
		
	}
	protected ManagedObjectReference process(ManagedObjectReference vmMor)
			throws Exception {
		return vimPort.powerOffVMTask(vmMor );
	}
}
