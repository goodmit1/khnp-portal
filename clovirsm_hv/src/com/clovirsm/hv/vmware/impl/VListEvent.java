package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfEvent;
import com.vmware.vim25.ArrayOfEventDescriptionEventDetail;
import com.vmware.vim25.Event;
import com.vmware.vim25.EventDescriptionEventDetail;
import com.vmware.vim25.EventFilterSpec;
import com.vmware.vim25.EventFilterSpecByEntity;
import com.vmware.vim25.EventFilterSpecByTime;
import com.vmware.vim25.EventFilterSpecRecursionOption;
import com.vmware.vim25.InvalidPropertyFaultMsg;
import com.vmware.vim25.InvalidStateFaultMsg;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RetrieveOptions;
import com.vmware.vim25.RetrieveResult;
import com.vmware.vim25.RuntimeFaultFaultMsg;

/**
 * 이벤트 목록
 * @author 윤경
 *
 */
public class VListEvent extends com.clovirsm.hv.vmware.CommonAction {

	public VListEvent(ConnectionMng m) {
		super(m);
		 
	}



	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		ManagedObjectReference vmRef = getObject(dc, param);
		 
		String category = (String)param.get(VMWareAPI.PARAM_CATEGORY);
		Date startTime = (Date)param.get(VMWareAPI.PARAM_START_TIME);
		Date finishTime = (Date)param.get(VMWareAPI.PARAM_END_TIME);
		int pageNo = CommonUtil.getInt( param.get(VMWareAPI.PARAM_PAGE_NO));
		int pageSize = CommonUtil.getInt( param.get(VMWareAPI.PARAM_PAGE_SIZE));
		List<Map> list = createEventHistoryCollector(vmRef, category, startTime, finishTime, pageNo, pageSize);
		result.put(VMWareAPI.PARAM_LIST, list);
		return null;

	}
	
	List<Map> createEventHistoryCollector(ManagedObjectReference vmRef, String category, Date startTime, Date finishTime, int pageNo, int pageSize) throws Exception 
	{
		if(pageNo<0) pageNo=1;
		if(pageSize<0) pageSize=100;

		ManagedObjectReference eventManagerRef = super.connectionMng.getServiceContent().getEventManager();
		EventFilterSpecByEntity entitySpec = new EventFilterSpecByEntity();
		entitySpec.setEntity(vmRef);
		entitySpec.setRecursion(EventFilterSpecRecursionOption.ALL);
		EventFilterSpec eventFilter = new EventFilterSpec();
		eventFilter.setEntity(entitySpec);
		if(category != null)
		{
			eventFilter.getCategory().add(category);
		}
		
		if(startTime != null || finishTime != null)
		{
			EventFilterSpecByTime timeFilter = new EventFilterSpecByTime();
			if(startTime != null) timeFilter.setBeginTime(toXmlCal(startTime));
			if(finishTime != null) timeFilter.setEndTime(toXmlCal(finishTime));
			eventFilter.setTime(timeFilter );
		}
		
		ManagedObjectReference eventHistoryCollectorRef = vimPort.createCollectorForEvents(eventManagerRef, eventFilter);
		vimPort.setCollectorPageSize(eventHistoryCollectorRef, pageSize*pageNo);
		ArrayOfEvent o = (ArrayOfEvent)getProp(eventHistoryCollectorRef,"latestPage");
		List<Map> result = new ArrayList();
		if(o == null) return result;
		List<Event>list1 = o.getEvent();
		int startNo=(pageNo-1)*pageSize;
		
		ArrayOfEventDescriptionEventDetail eventDesc = (ArrayOfEventDescriptionEventDetail)getProp(super.connectionMng.getServiceContent().getEventManager(),"description.eventInfo");
		List<EventDescriptionEventDetail> eventInfo = eventDesc.getEventDescriptionEventDetail();

		for(int i = startNo; i < list1.size(); i++)
		{
			Event e = list1.get(i);
			Map m = new HashMap();
			m.put(VMWareAPI.PARAM_COMMENT, e.getFullFormattedMessage());
			XMLGregorianCalendar t = e.getCreatedTime();

			m.put(VMWareAPI.PARAM_CRE_TIME, t.toGregorianCalendar().getTime());
			m.put(VMWareAPI.PARAM_CATEGORY,this.getSeverity(e.getClass().getName(), eventInfo));
			m.put(VMWareAPI.PARAM_TARGET, getTarget(e));
			result.add(m);
		}
		return result;

	}


	String getTarget(Event e)
	{
		if(e.getVm() != null)
		{
			return "VM:" + e.getVm().getName();
		}
		else if(e.getDs() != null)
		{
			return "DS:" + e.getDs().getName();
		}
		else if(e.getHost() != null)
		{
			return "Host:" + e.getHost().getName();
		}
		return "";
	}
	private String getSeverity(String eventKey, List<EventDescriptionEventDetail>eventInfo) {
		// strip com.vmware.vim25. from eventKey
		int beginIndex = 17; // skip 17 chars
		String eventName = eventKey.substring(beginIndex);
		for (EventDescriptionEventDetail info: eventInfo) {
			if (info.getKey().equals(eventName))
				return info.getCategory();
		}
		return "Unknown";
	}

}
