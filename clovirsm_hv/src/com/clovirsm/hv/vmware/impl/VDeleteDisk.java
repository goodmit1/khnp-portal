package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
/**
 * VM에 종속되지 않는 디스크 삭제
 * @author 윤경
 *
 */
public class VDeleteDisk  extends com.clovirsm.hv.vmware.CommonAction{

	public VDeleteDisk(ConnectionMng m) {
		super(m);
	 
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		String path = (String)param.get(VMWareAPI.PARAM_DISK_PATH);
		deleteDisk(dc, path);
		return null;
	}
	
	protected void deleteDisk(String dc, String path) throws Exception
	{
		ManagedObjectReference dcRef = super.getDataCenter(dc);
		super.connectionMng.getVimPort().deleteVirtualDiskTask(this.connectionMng.getServiceContent().getVirtualDiskManager(), path, dcRef);
	}

}
