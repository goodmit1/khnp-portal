package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfCustomizationSpecInfo;
import com.vmware.vim25.CustomizationSpecInfo;
import com.vmware.vim25.CustomizationSpecItem;
import com.vmware.vim25.ManagedObjectReference;

/**
 * DC, Cluster, Host, DVS 정보 가져오기 
 * @author 윤경
 *
 */
public class VListObject extends com.clovirsm.hv.vmware.CommonAction {

	public VListObject(ConnectionMng m) {
		super(m);
		 
	}

	private void add(List result,String id, String name, String type, String parent)
	{
		Map map = new HashMap();
		map.put("OBJ_ID", id);
		map.put("OBJ_NM", name);
		map.put("OBJ_TYPE_NM", type);
		map.put("PARENT_OBJ_NM", parent);
		result.add(map);
	}
	private void addChild(List list, ManagedObjectReference parent,String parentName, Map objTypeTree) throws Exception
	{
		String[] objType = (String[])objTypeTree.get(parent.getType());
		if(objType == null) return;
		for(String objType1 : objType)
		{
			Map<String, ManagedObjectReference>  childList = super.findObjectList(parent, objType1 );
			for(String child : childList.keySet())
			{
				ManagedObjectReference childObj = childList.get(child);
				add(list, childObj.getValue(),   child, objType1, parentName);
				addChild(list, childObj, child, objTypeTree);
			}
		}
	}
	 
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param,
			Map result) throws Exception {
		 
		Map objTypeTree = new HashMap();
		objTypeTree.put(VMWareCommon.OBJ_TYPE_DC,  new String[]{VMWareCommon.OBJ_TYPE_CLUSTER, VMWareCommon.OBJ_TYPE_DS,  VMWareAPI.OBJ_TYPE_NETWORK  });
		objTypeTree.put(VMWareCommon.OBJ_TYPE_CLUSTER,new String[]{VMWareCommon.OBJ_TYPE_HOST});
		Map<String, ManagedObjectReference> dcList = null;
		if(dc == null)
		{
			dcList = super.findObjectListFromRoot(VMWareCommon.OBJ_TYPE_DC);
		}
		else
		{
			dcList = new HashMap();
			dcList.put(dc,  super.getDataCenter(dc));
		}
		List list = new ArrayList();
		
		for(String dc1 : dcList.keySet())
		{
			ManagedObjectReference obj = dcList.get(dc1);
			add(list,obj.getValue(), dc1, VMWareCommon.OBJ_TYPE_DC, "");
			addChild(list, obj, dc1, objTypeTree);
			Map<ManagedObjectReference,Map<String,Object>> vmList = super.findObjectList(obj, VMWareCommon.OBJ_TYPE_VM, new String[]{"name","config.template"});
			for(ManagedObjectReference vm : vmList.keySet())
			{
				Map m = vmList.get(vm);
				if(Boolean.TRUE==m.get("config.template"))
				{
					 add(list,vm.getValue(),  (String)m.get("name"), "Template", dc1);
				}
			}
			
		}
		 ManagedObjectReference cpm = super.connectionMng.getServiceContent().getCustomizationSpecManager();
		 List<CustomizationSpecInfo> infoList =  ((ArrayOfCustomizationSpecInfo)getProp(cpm, "info")).getCustomizationSpecInfo();
		 if(infoList != null)
		 {
			 for(CustomizationSpecInfo m : infoList)
			 {
				 add(list,"",  m.getName(), VMWareAPI.OBJ_TYPE_CSI, "");
				 
			 }
		 }
		result.put(VMWareAPI.PARAM_LIST, list);
		return null;
	}

}
