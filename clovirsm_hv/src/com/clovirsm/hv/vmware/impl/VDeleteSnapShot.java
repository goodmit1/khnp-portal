package com.clovirsm.hv.vmware.impl;

import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.InvalidPropertyFaultMsg;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.RuntimeFaultFaultMsg;
import com.vmware.vim25.VirtualMachineSnapshotInfo;
import com.vmware.vim25.VirtualMachineSnapshotTree;

/**
 * 스냅샷 삭제
 * @author 윤경
 *
 */
public class VDeleteSnapShot  extends VCreateSnapshot{

	public VDeleteSnapShot(ConnectionMng m) {
		super(m);
		 
	}
	 
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		 
		deleteSnapshot(super.getVM(param),(String)param.get(HypervisorAPI.PARAM_SNAPSHOT_NM));
		return null;
	}
	
	protected void deleteSnapshot(ManagedObjectReference vmRef, String snapshotname)  
	{
		 
		try {
			 
			ManagedObjectReference snapmor = getSnapshotReference(vmRef,  snapshotname);
		    if (snapmor != null) {
		        ManagedObjectReference taskMor =
		                  vimPort.removeSnapshotTask(snapmor, true, true);
		        //getTaskResultAfterDone(taskMor, "DeleteSnapshot:" + vmName + "," + snapshotname);
		    }
		} catch (Exception e) {
		 
			e.printStackTrace();
		}
	 
		
	}

}
