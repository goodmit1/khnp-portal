package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.datatype.XMLGregorianCalendar;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.HypervisorException;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.PerfCounterInfo;
import com.vmware.vim25.PerfEntityMetric;
import com.vmware.vim25.PerfEntityMetricBase;
import com.vmware.vim25.PerfInterval;
import com.vmware.vim25.PerfMetricId;
import com.vmware.vim25.PerfMetricIntSeries;
import com.vmware.vim25.PerfMetricSeries;
import com.vmware.vim25.PerfQuerySpec;
import com.vmware.vim25.PerfSampleInfo;
import com.vmware.vim25.PerfStatsType;
import com.vmware.vim25.PerfSummaryType;

/**
 * 성능 이력
 * @author 윤경
 *
 */
public class VListPerfHistory extends CommonAction{

	Set divideTarget;
	public VListPerfHistory(ConnectionMng m) {
		super(m);
		divideTarget = new HashSet();
		try {
			String[] arr = HVProperty.getInstance().getStrings( "vmware.perf.divide100");
			for(String a:arr)
			{
				divideTarget.add(a);
			}
		} catch (Exception e) {
			 
			e.printStackTrace();
		}	 
	}
	protected String getObjType(Map param)
	{
		return (String) param.get(VMWareAPI.PARAM_OBJECT_TYPE);
	}

	protected ManagedObjectReference getRef(String dc, Map param) throws Exception
	{
		return super.getObject(dc, param);
	}
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		ManagedObjectReference vmRef = getRef(dc, param);
	 
		result.put(VMWareAPI.PARAM_LIST, counterInfo(vmRef,  (String)param.get(VMWareAPI.PARAM_PERIOD), param));
		return null;
		
	}
	protected PerfMetricId getPerfMetricId(String perfName) throws Exception
	{
		PerfMetricId ourCounter = new PerfMetricId();

		ourCounter.setInstance("");
		int id = HVProperty.getInstance().getInt("vmware.perf.type."+perfName ,0);
		if(id >0) 
		{
			ourCounter.setCounterId(id);
		}
		else
		{
			System.out.println(perfName);
			throw new NotFoundException( perfName);
		}
		return ourCounter;
	}
	protected String[] getPerfTypes(String objType) throws Exception 
	{
		String[] ids = HVProperty.getInstance().getStrings("vmware.perf.obj.history." + objType);
		if(ids==null)
		{
			throw new Exception(objType + "'s perf not found");
		}
		return ids;
	}
	Map<Integer, String> getPerfIds(PerfQuerySpec qSpec,String objType, String[] perfIds) throws Exception
	{
		
		String[] ids = perfIds;
		if(ids==null)
		{
			ids = getPerfTypes(objType);
		}
		List<PerfMetricId> list = qSpec.getMetricId();
		
		int idx=0;
		Map<Integer, String> result = new HashMap();
		for(String id:ids)
		{
			if(id.equals("")) continue;
			PerfMetricId m = getPerfMetricId(id);
			result.put(m.getCounterId(),id);
			list.add(m);
		}
		return result;
	}
	void setTime(PerfQuerySpec qSpec, Date startDt, Date finishDt, Object intervalId) throws Exception	{
		Date startDt1 = new Date(startDt.getTime()-1);
		
		 
		 
		//System.out.println(startDt + "~" + finishDt);
		qSpec.setStartTime(toXmlCal(startDt1));
		if( finishDt == null) {
			finishDt = new Date();
		}
		qSpec.setEndTime( toXmlCal(finishDt));
		
		if(intervalId == null) {
			int dd = (int) ((finishDt.getTime()-startDt.getTime())/1000/60/60/24);
			if(dd>10) {
				intervalId = "86400";
			}	
			else {
				intervalId = "7200";
			} 
		}
		
		qSpec.setIntervalId( new Integer( intervalId.toString()));
	}
	void setTimeInterval(PerfQuerySpec qSpec, String periodKubun, Map param) throws Exception
	{
		if(periodKubun.equals(VMWareAPI.PERF_PERIOD_1D))
		{
			/*XMLGregorianCalendar serverstarttime = toXmlCal(new Date());
			serverstarttime.setDay(serverstarttime.getDay()-1);
			qSpec.setStartTime(serverstarttime );
			qSpec.setEndTime( toXmlCal(new Date()));*/
			int interval =  HVProperty.getInstance().getInt("vmware.perf.interval." + periodKubun + "." + param.get("DC_ID"),-1);
			if(interval == -1)
			{
				interval =  HVProperty.getInstance().getInt("vmware.perf.interval." + periodKubun  ,300);
			}
			System.out.println(interval);
			qSpec.setIntervalId(new Integer(interval));
		}
		else if(periodKubun.equals(VMWareAPI.PERF_PERIOD_1W))
		{
			/*XMLGregorianCalendar serverstarttime = toXmlCal(new Date());
			serverstarttime.setDay(serverstarttime.getDay()-7);
			qSpec.setStartTime(serverstarttime );
			qSpec.setEndTime( toXmlCal(new Date()));*/
			qSpec.setIntervalId(new Integer(1800));
		}
		else if(periodKubun.equals(VMWareAPI.PERF_PERIOD_1M))
		{
			/*XMLGregorianCalendar serverstarttime = toXmlCal(new Date());
			serverstarttime.setMonth(serverstarttime.getMonth()-1);
			qSpec.setStartTime(serverstarttime );
			qSpec.setEndTime( toXmlCal(new Date()));*/
			qSpec.setIntervalId(new Integer(7200));
		}
		else if(periodKubun.equals(VMWareAPI.PERF_PERIOD_1Y))
		{
			/*XMLGregorianCalendar serverstarttime = toXmlCal(new Date());
			serverstarttime.setYear(serverstarttime.getYear()-1);
			qSpec.setStartTime(serverstarttime );
			qSpec.setEndTime( toXmlCal(new Date()));*/
			qSpec.setIntervalId(new Integer(86400));
		}
		else if(periodKubun.equals(VMWareAPI.PERF_PERIOD_1H))
		{
			
			 
			qSpec.setIntervalId(new Integer(20));
		}
		else
		{
			throw new Exception("Perf kubun not found  " + periodKubun + "in (1d, 1w, 1m, 1y) ");
		}
	}
	 
	protected Object counterInfo(ManagedObjectReference pmRef , String periodKubun, Map param) throws Exception {
		PerfQuerySpec qSpec = new PerfQuerySpec();
		qSpec.setEntity(pmRef);

		if((Date)param.get(VMWareCommon.PARAM_START_DT) != null)
		{
			this.setTime(qSpec, (Date)param.get(VMWareCommon.PARAM_START_DT), (Date)param.get(VMWareCommon.PARAM_FINISH_DT),  param.get(VMWareCommon.PARAM_INTERVAL));
			
		}
		else
		{
			setTimeInterval(qSpec,periodKubun,param );
		}
		 
		setQSpeckExtraInfo(qSpec);
		Map<Integer, String> perfMap = getPerfIds(qSpec, pmRef.getType(), (String[])param.get("PERF_IDS"));
		
		if(perfMap==null || perfMap.size()==0) return new HashMap();


		ManagedObjectReference perfManager = super.connectionMng.getServiceContent().getPerfManager();
		List<PerfQuerySpec> alpqs = new ArrayList<PerfQuerySpec>(1);
		alpqs.add(qSpec);
		List<PerfEntityMetricBase> listpemb = vimPort.queryPerf(
				perfManager, alpqs);
		return displayValues(periodKubun, listpemb, perfMap);
	}
	 
	protected void setQSpeckExtraInfo(PerfQuerySpec qSpec) {

		
	}
	protected Object displayValues(String periodKubun, List<PerfEntityMetricBase> values,Map<Integer, String> perfMap  ) {
		List<Map> result = new ArrayList();
		 
		for (int i = 0; i < values.size(); ++i) {
			List<PerfMetricSeries> listperfmetser = ((PerfEntityMetric) values
					.get(i)).getValue();
			List<PerfSampleInfo> listperfsinfo = ((PerfEntityMetric) values
					.get(i)).getSampleInfo();
			if (listperfsinfo == null || listperfsinfo.size() == 0) {
				 
				continue;
			}

			for (int vi = 0; vi < listperfmetser.size(); ++vi) {

				if (listperfmetser.get(vi) instanceof PerfMetricIntSeries) {
					PerfMetricIntSeries val = (PerfMetricIntSeries) listperfmetser.get(vi);
					String metricName = perfMap.get(val.getId().getCounterId());
					 
					List list1 = new ArrayList();
					
					List<Long> listlongs = val.getValue();
					for (int j = 0; j < listlongs.size(); j++) {
						Map m1 = new HashMap();
						m1.put(VMWareAPI.PARAM_CRE_TIME,  listperfsinfo.get(j).getTimestamp().toGregorianCalendar().getTimeInMillis());
						if(divideTarget.contains(metricName))
						{
							m1.put(VMWareAPI.PARAM_VAL, 1.0 * listlongs.get(j)/100);
						}
						else
						{
							m1.put(VMWareAPI.PARAM_VAL, listlongs.get(j));
						}
						
						 
						list1.add(m1);
						
					}
					Map m = new HashMap();
					m.put(metricName, list1);
					result.add(m); 
				}
			}
		}
		return result;
	}


}
