package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfLicenseAssignmentManagerLicenseAssignment;
import com.vmware.vim25.ArrayOfLicenseManagerLicenseInfo;
import com.vmware.vim25.HostConnectInfo;
import com.vmware.vim25.HostLicenseConnectInfo;
import com.vmware.vim25.LicenseAssignmentManagerLicenseAssignment;
import com.vmware.vim25.LicenseManagerLicenseInfo;
import com.vmware.vim25.ManagedObjectReference;

public class VListLicense extends com.clovirsm.hv.vmware.CommonAction {

	public VListLicense(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		List<LicenseManagerLicenseInfo> list = ((ArrayOfLicenseManagerLicenseInfo) super.getProp(this.connectionMng.getServiceContent().getLicenseManager(), "licenses")).getLicenseManagerLicenseInfo();
		Map<String, ManagedObjectReference>  childList = super.findObjectList(getDataCenter(dc), VMWareCommon.OBJ_TYPE_HOST );
		List hostList = new ArrayList();
		ManagedObjectReference licenseAssignmentManager = (ManagedObjectReference)super.getProp(this.connectionMng.getServiceContent().getLicenseManager(),"licenseAssignmentManager");
		Set liceseSet = new HashSet();
		for(String k:childList.keySet())
		{
			ManagedObjectReference host = childList.get(k);
			List<LicenseAssignmentManagerLicenseAssignment> llist =  this.connectionMng.getVimPort().queryAssignedLicenses(licenseAssignmentManager, host.getValue());
			for(LicenseAssignmentManagerLicenseAssignment l :llist)
			{
				Map m = new HashMap();
				m.put("LICENSE_KEY",	l.getAssignedLicense().getLicenseKey());
				liceseSet.add(m.get("LICENSE_KEY"));
				m.put("HOST_HV_ID",	host.getValue());
				hostList.add(m);
			}
		}
		
		List resultList = new ArrayList();
		for(LicenseManagerLicenseInfo l :list)
		{
			//if(!liceseSet.contains(l.getLicenseKey())) continue;
			Map m = new HashMap();
			m.put("editorKey",l.getEditionKey());
			m.put("LICENSE_KEY", l.getLicenseKey());
			m.put("LICENSE_TOTAL", l.getTotal());
			m.put("LICENSE_USED", l.getUsed());
			m.put("COST_UNIT", l.getCostUnit());
			m.put("LICENSE_NAME", l.getName());
			resultList.add(m);
		}
		
		result.put(HypervisorAPI.PARAM_LIST, resultList);
		result.put("HOST_LIST", hostList);
		return null;
	}

}
