package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ResourceAllocationInfo;
import com.vmware.vim25.ResourceConfigSpec;

/**
 * 리소스풀 셋팅 정보 수정, VM추가.삭제시 실행됨.
 * @author 윤경
 *
 */
public class VReconfigCPUResourcePool extends com.clovirsm.hv.vmware.CommonAction{

	 

	public VReconfigCPUResourcePool(ConnectionMng m) {
		super(m);
		 
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		int cpu = 0;
		if(param.get(VMWareAPI.PARAM_PLUS_CPU) == null)
		{
			cpu = CommonUtil.getInt(param.get(VMWareAPI.PARAM_CPU));
		}
		else
		{
			cpu = CommonUtil.getInt(param.get(VMWareAPI.PARAM_PLUS_CPU));
		}
		
		reconfig(dc, (String)param.get(VMWareAPI.PARAM_RESOURCEPOOL_NM),  cpu *1024 );
		return null;
	}
	protected void reconfig(String dc, String resourceName, int limit) throws Exception
	{
		 ManagedObjectReference resourcepool = super.findObjectFromRoot("ResourcePool", resourceName);
		 ResourceConfigSpec spec =  (ResourceConfigSpec) super.getProp(resourcepool, "config");
		 ResourceAllocationInfo cpu = spec.getCpuAllocation();
		 cpu.setLimit(cpu.getLimit()+limit);
		 vimPort.updateConfig(resourcepool, resourceName, spec);
				 
		
	}
}
