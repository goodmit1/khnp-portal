package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfAuthorizationRole;
import com.vmware.vim25.AuthorizationRole;
import com.vmware.vim25.InvalidPropertyFaultMsg;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.Permission;
import com.vmware.vim25.RuntimeFaultFaultMsg;

public class VAddPermission extends CommonAction{

	public VAddPermission(ConnectionMng m) {
		super(m);
		 
	}
	private int getRoleId(String roleName) throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg
	{
		ManagedObjectReference mng = connectionMng.getServiceContent().getAuthorizationManager();
		ArrayOfAuthorizationRole list1 = (ArrayOfAuthorizationRole) connectionMng.getMOREF().entityProps(mng,new String[]{"roleList"}).get("roleList");
		List<AuthorizationRole> list = list1.getAuthorizationRole();
		for(AuthorizationRole r : list)
		{
			if(r.getName().equals(roleName))
			{
				return r.getRoleId();
			}
			
		}
		return -2;
		
	}
	  public void printInventory() throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg {
	        Map<String, ManagedObjectReference> inventory = inventory();

	        for (String entityName : inventory.keySet()) {
	            System.out.printf("> " + inventory.get(entityName).getType() + ":"
	                    + inventory.get(entityName).getValue() + "{" + entityName + "}%n");
	        }
	    }

	    public Map<String, ManagedObjectReference> inventory() throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg {
	        return connectionMng.getMOREF().inFolderByType(super.connectionMng.getServiceContent().getRootFolder(), "ManagedEntity");
	    }
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		
		ManagedObjectReference hostAuthorizationManager = super.connectionMng.getServiceContent().getAuthorizationManager();
		vimPort.currentTime(super.connectionMng.getServiceInstanceReference());
		 
		ManagedObjectReference resourcepoolmor   = null;
		ManagedObjectReference root   = null;
		
			
	 
		
	

		//getRoleId("Portal_Console");
		/*
		 * For demonstration purposes only, the account is granted the
		 * 'administrator' role (-1) on the rootFolder of the inventory. Never
		 * give users more privileges than absolutely necessary.
		 */
		String userName = VMWareAPI.GUEST_USER_ID_PREFIX +  param.get(VMWareAPI.PARAM_GUEST_USERID);
		List<Permission> permissions = new ArrayList<Permission>();
		if(cluster == null || "".equals(cluster))
		{
			Map<String, ManagedObjectReference> t = connectionMng.getMOREF().inContainerByType(connectionMng.getServiceContent().getRootFolder(), "ComputeResource");
			root = t.values().iterator().next();
			
			resourcepoolmor = super.findObjectFromRoot( VMWareCommon.OBJ_TYPE_RP , (String) param.get(VMWareAPI.PARAM_RESOURCEPOOL_NM));
			//root = (ManagedObjectReference) super.getProp(resourcepoolmor, "owner");
			
			Permission per = new Permission();
			per.setGroup(false);
			per.setPrincipal(  userName);
			per.setRoleId(getRoleId(HVProperty.getInstance().getProperty("console_role", "console"))); //TODO properties
			//per.setRoleId(-2);
			per.setPropagate(true);
			per.setEntity(resourcepoolmor);
			System.out.println(HVProperty.getInstance().getProperty("console_role", "console") + "=" + per.getRoleId());	 
			Permission per1 = new Permission();
			per1.setGroup(false);
			per1.setPrincipal(  userName);
			per1.setRoleId(-2); //TODO properties
			//per.setRoleId(-2);
			per1.setPropagate(false);
			per1.setEntity(root);
			permissions.add(per);
			
			List<Permission> permissions1 = new ArrayList<Permission>();
			permissions1.add(per1);
			
		
			vimPort.setEntityPermissions(hostAuthorizationManager, root,  permissions1);
			vimPort.setEntityPermissions(hostAuthorizationManager, resourcepoolmor,  permissions);
			
		}
		else
		{
			root = connectionMng.getServiceContent().getRootFolder();
			Permission per = new Permission();
			per.setGroup(false);
			per.setPrincipal(  userName);
			per.setRoleId(getRoleId(HVProperty.getInstance().getProperty("console_role", "console"))); //TODO properties
			//per.setRoleId(-2);
			per.setPropagate(true);
			per.setEntity(root);
			vimPort.setEntityPermissions(hostAuthorizationManager, root,  permissions);
		}
		
		return null;
	}
	 
}
