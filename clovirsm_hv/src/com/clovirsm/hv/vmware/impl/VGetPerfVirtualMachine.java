package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.NumberUtil;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfGuestDiskInfo;
import com.vmware.vim25.ArrayOfVirtualDevice;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.PerfMetricId;
import com.vmware.vim25.PerfQuerySpec;

/**
 * VM의 성능 정보
 * @author 윤경
 *
 */
public class VGetPerfVirtualMachine extends VGetPerf{
	 
	int prevDay = 0;
	public VGetPerfVirtualMachine(ConnectionMng m) {
		super(m);
		 
		
	}
	 
	@Override
	protected ManagedObjectReference getRef(String dc, Map param) throws Exception {
		if(param.get("PREV_DAY") != null) prevDay = NumberUtil.getInt(param.get("PREV_DAY") );
		return super.getVM( param);
	}

	protected void addCpuMemMax(ManagedObjectReference vm, Map result) throws Exception
	{
		PerfQuerySpec qSpec = new PerfQuerySpec();
		qSpec.setEntity(vm);
		PerfMetricId metric = new PerfMetricId();
		metric.setInstance("");
		metric.setCounterId(2);
		
		PerfMetricId metric1 = new PerfMetricId();
		metric1.setInstance("");
		metric1.setCounterId(24);
		qSpec.getMetricId().add(metric);
		qSpec.getMetricId().add(metric1);
		Calendar fromDate = Calendar.getInstance();
		fromDate.add(Calendar.DATE, -1 * prevDay);
		 
	 
		qSpec.setStartTime(toXmlCal(fromDate.getTime()));
		qSpec.setEndTime(toXmlCal(new Date()));
		qSpec.setMaxSample(1);
		//qSpec.setIntervalId(86400);
		//setTime(qSpec, fromDate, new Date(),  3200);
		ManagedObjectReference perfManager = super.connectionMng.getServiceContent().getPerfManager();
		 
		List<PerfQuerySpec> alpqs = new ArrayList<PerfQuerySpec>(1);
		alpqs.add(qSpec);
		List  listpemb = vimPort.queryPerf(
				perfManager, alpqs);
		Map perfMap = new HashMap();
		perfMap.put(2, "prev_cpu");
		perfMap.put(24, "prev_mem");
		Map m = (Map)displayValues(null, listpemb, perfMap);
		result.putAll(m);
	}
	 
	protected void addDiskInfo(ManagedObjectReference vm, Map result) throws Exception
	{
		 VInfoVM infoVM = new VInfoVM(this.connectionMng);
		 Map props =  super.getProps(vm, "config.hardware.device","guest.disk", "runtime.host");
		 long total = infoVM.getDiskSize((ArrayOfVirtualDevice)props.get("config.hardware.device" ));
         ArrayOfGuestDiskInfo diskInfos = (ArrayOfGuestDiskInfo)props.get( "guest.disk");
         long used = infoVM.getUsedDiskSize(  diskInfos);
		 result.put("VM_HV_ID",vm.getValue());
		 ManagedObjectReference host = (ManagedObjectReference)props.get("runtime.host");
		 if(host!=null)
		 {
				 
				result.put("HOST_HV_ID", host.getValue());
		 }
		 result.put("disk_totalgb", 1.0f * total/1024/1024);
		 result.put("disk_usedgb", 1.0f * used/1024/1024/1024);
		 result.put("disk_usage_per", 1.0f * used / (total*1024) * 100);
		 
		 if(prevDay > 0)
		 {
			 this.addCpuMemMax(vm, result);
		 }
		 
	}
	 
	protected void addSpecInfo(ManagedObjectReference vm, Map result) throws Exception
	{
		 
	}
}
