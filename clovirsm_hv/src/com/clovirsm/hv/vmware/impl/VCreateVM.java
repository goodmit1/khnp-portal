package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.CustomizationAdapterMapping;
import com.vmware.vim25.CustomizationDhcpIpGenerator;
import com.vmware.vim25.CustomizationFixedIp;
import com.vmware.vim25.CustomizationFixedName;
import com.vmware.vim25.CustomizationGlobalIPSettings;
import com.vmware.vim25.CustomizationIPSettings;
import com.vmware.vim25.CustomizationLinuxPrep;
import com.vmware.vim25.CustomizationSpec;
import com.vmware.vim25.CustomizationSpecItem;
import com.vmware.vim25.DVPortgroupConfigInfo;
import com.vmware.vim25.DistributedVirtualSwitchPortConnection;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.VMwareDVSConfigInfo;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDeviceConfigSpec;
import com.vmware.vim25.VirtualDeviceConfigSpecOperation;
import com.vmware.vim25.VirtualDeviceConnectInfo;
import com.vmware.vim25.VirtualEthernetCard;
import com.vmware.vim25.VirtualEthernetCardDistributedVirtualPortBackingInfo;
import com.vmware.vim25.VirtualEthernetCardNetworkBackingInfo;
import com.vmware.vim25.VirtualEthernetCardOpaqueNetworkBackingInfo;
import com.vmware.vim25.VirtualMachineCloneSpec;
import com.vmware.vim25.VirtualMachineConfigSpec;
import com.vmware.vim25.VirtualMachineRelocateSpec;
import com.vmware.vim25.VirtualVmxnet3;
/**
 * VM 생성
 * @author 윤경
 *
 */
public class VCreateVM extends com.clovirsm.hv.vmware.CommonAction{


	public VCreateVM(ConnectionMng m) {
		super(m);

	}

	Map param;

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		this.param = param;

		int cpu = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_CPU));
		int mem = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_MEM));
		int disk = CommonUtil.getInt(param.get(HypervisorAPI.PARAM_DISK));
		return this.createVM(false,result, dc, (String)param.get(VMWareAPI.PARAM_FOLDER_NM), (String)param.get(VMWareAPI.PARAM_RESOURCEPOOL_NM),
				(String)param.get(HypervisorAPI.PARAM_VM_NM), cpu, mem, disk, (String[])param.get(VMWareAPI.PARAM_DATASTORE_NM_LIST),
				(String)param.get(VMWareAPI.PARAM_PORT_GROUP_NM),
				  (String)param.get(HypervisorAPI.PARAM_FROM),
				CommonUtil.getInt(param.get(HypervisorAPI.PARAM_NIC_ID)),
				(String)param.get(VMWareAPI.PARAM_WINDOWS_CSI),(String)param.get(VMWareAPI.PARAM_LINUX_CSI),param
				);

	}
	protected VirtualDeviceConfigSpec setNetwork(  String portgroupName, int nicKey ) throws Exception
	{
		ManagedObjectReference dvspgMor =	findObjectFromRoot(
				VMWareAPI.OBJ_TYPE_DVP,portgroupName);
		return setDVPNetwork(dvspgMor, nicKey);
	}
	protected VirtualDeviceConfigSpec setStandardNetwork( ManagedObjectReference  vmRef, ManagedObjectReference dvspgMor, int nicKey ) throws Exception
	{
		VirtualDeviceConfigSpec nicSpec = new VirtualDeviceConfigSpec();
		VirtualEthernetCard nic = null;
		List<VirtualDevice> hardwares = super.getHardwareDevice(vmRef);
		for (VirtualDevice device : hardwares) {
			if (device instanceof VirtualEthernetCard) {
					nic = (VirtualEthernetCard) device;
					break;
			}
		}
		
		if(nic == null)
		{
			nic = new VirtualVmxnet3();
			nic.setKey(nicKey);
			nic.setAddressType("assigned");
		}
		else
		{
			nicSpec.setOperation(VirtualDeviceConfigSpecOperation.EDIT);	
		}

		
		// VirtualEthernetCard nic = new VirtualPCNet32();
		//
		String netName = (String) getProp(dvspgMor,"name");
		VirtualEthernetCardNetworkBackingInfo nicBacking = new VirtualEthernetCardNetworkBackingInfo();
	    VirtualDeviceConnectInfo info = new VirtualDeviceConnectInfo();
	    info.setConnected(true);
	    info.setStartConnected(true);
	    info.setAllowGuestControl(true);
	    nic.setConnectable(info);
	    nicBacking.setDeviceName(netName);
	    nicBacking.setUseAutoDetect(true);
	    nicBacking.setNetwork(dvspgMor);
	 
		nic.setBacking(nicBacking);

		nicSpec.setDevice(nic);
		return nicSpec;

		 
	}
	
	protected VirtualDeviceConfigSpec setDVPNetwork( ManagedObjectReference dvspgMor, int nicKey ) throws Exception
	{

		VirtualDeviceConfigSpec nicSpec = new VirtualDeviceConfigSpec();

		nicSpec.setOperation(VirtualDeviceConfigSpecOperation.EDIT);
		// VirtualEthernetCard nic = new VirtualPCNet32();
		VirtualEthernetCard nic = new VirtualVmxnet3();

		


		DVPortgroupConfigInfo portgroupInfo = (DVPortgroupConfigInfo) getProp(dvspgMor,"config");
		ManagedObjectReference dvs = portgroupInfo.getDistributedVirtualSwitch();
		VMwareDVSConfigInfo switchInfo = (VMwareDVSConfigInfo) getProp(dvs,"config");
		if (switchInfo != null && portgroupInfo != null) {


			DistributedVirtualSwitchPortConnection vspc= new DistributedVirtualSwitchPortConnection();
			vspc.setSwitchUuid(switchInfo.getUuid());
			vspc.setPortgroupKey(portgroupInfo.getKey());
			VirtualEthernetCardDistributedVirtualPortBackingInfo nicBacking = new VirtualEthernetCardDistributedVirtualPortBackingInfo();
			nicBacking.setPort(vspc);

			// nicBacking.setDeviceName(networkName);
			nic.setKey(nicKey);
			nic.setAddressType("assigned");
			nic.setBacking(nicBacking);

			nicSpec.setDevice(nic);
			return nicSpec;

		}
		return null;

	}
	protected VirtualDeviceConfigSpec createVirtualDisk(ManagedObjectReference vmRef, Map m, String[] dsNames,
             int diskSizeGB, Set dsInHost) throws Exception {

		 
			 
			 
			 String diskName = createDisk.getDiskName(dsInHost, dsNames, (String)param.get("VM_NM"),  diskSizeGB);
			 String diskmode = "persistent";
			 VirtualDeviceConfigSpec vdcs = createDisk.getDiskDeviceConfigSpec(vmRef, diskName, diskSizeGB, diskmode);
			 m.put("DISK_PATH", diskName);
			 
		 	 return vdcs;
		 
	 
	}
	public static void setLinuxNetwork(int idx, VirtualMachineCloneSpec cloneSpec, String hostName, String gateway, String ip, String subnetMask) throws Exception
	{
		CustomizationSpec spinfo = new CustomizationSpec();
		customizeNetwork(idx, spinfo, gateway, ip, subnetMask);
		CustomizationLinuxPrep idsettings  = new CustomizationLinuxPrep();
		CustomizationFixedName fixedName = new CustomizationFixedName();
	    fixedName.setName(hostName);

	    idsettings.setDomain("local.com");
		idsettings.setHostName(fixedName );
		spinfo.setIdentity(idsettings);
		CustomizationGlobalIPSettings ipset = new CustomizationGlobalIPSettings();

		spinfo.setGlobalIPSettings(ipset );
		cloneSpec.setCustomization(spinfo );
	}
	protected void setCustomizationSpecInfo(int idx, boolean isWin, VirtualMachineCloneSpec cloneSpec, CustomizationSpecItem cspec  ,String hostName,  String ip, String gateway, String subnetMask ) throws Exception
	{
		 
			customizeNetwork(idx, cspec.getSpec(), gateway, ip, subnetMask);
			if(!isWin && hostName != null)
			{
				CustomizationFixedName fixedName = new CustomizationFixedName();
			    fixedName.setName(hostName);
			    CustomizationLinuxPrep customizationLinuxPrep =(CustomizationLinuxPrep) cspec.getSpec().getIdentity();
			    customizationLinuxPrep.setHostName(fixedName);
				
			}
			/*
			 * CustomizationWinOptions options = new CustomizationWinOptions();
			 * options.setChangeSID(true); options.setDeleteAccounts(true);
			 * cspec.getSpec().setOptions(options);
			 */
			
			cloneSpec.setCustomization(cspec.getSpec());

		 


	}


	private static void customizeNetwork(int idx , CustomizationSpec cspec, String gateway, String ip, String subnetMask)
	{
		CustomizationIPSettings cip = new CustomizationIPSettings();
		if(ip==null || "".equals(ip))
		{
			CustomizationDhcpIpGenerator dhcp = new CustomizationDhcpIpGenerator();
			cip.setIp(dhcp);
		}
		else
		{
			CustomizationFixedIp fip = new CustomizationFixedIp();
			fip.setIpAddress(ip);
			cip.setIp(fip);
			cip.setSubnetMask(subnetMask);
			cip.getGateway().add(gateway);
		}
		
		List<CustomizationAdapterMapping> list = cspec.getNicSettingMap();
		if(list.size()<=idx)
		{
			List dnsServerList =    list.get(list.size()-1).getAdapter().getDnsServerList();
			if(dnsServerList !=null && dnsServerList.size()>0) {
				cip.getDnsServerList().addAll(dnsServerList);
			}
			CustomizationAdapterMapping mapping = new CustomizationAdapterMapping();
			mapping.setAdapter(cip);
			list.add(mapping);
		}
		else
		{
			List dnsServerList =    list.get(idx).getAdapter().getDnsServerList();
			if(dnsServerList !=null && dnsServerList.size()>0) {
				cip.getDnsServerList().addAll(dnsServerList);
			}
			list.get(idx).setAdapter(cip);
		}

	}

	VCreateDisk createDisk ;

	protected ManagedObjectReference createVM(boolean isTemplate, Map result, String dc, String folderName, String resourcePool, String vmName,   int cpu, int memSize, int diskSizeGB,
			String[] dsNames,  String portgroupNm, String templateNm, int nicKey,
			String window_csi,String linux_csi, Map param) throws Exception
	{
		ManagedObjectReference vmRef = super.getVM(  templateNm);
		if (vmRef == null) {
			throw new NotFoundException( "Template", templateNm);

		}

		ManagedObjectReference dcM = super.getDataCenter(dc);
		ManagedObjectReference vmFolderRef;
		if(folderName != null && !"".equals(folderName))
		{	
			VCreateFolder folder = new VCreateFolder(this.connectionMng);
	
			vmFolderRef = folder.makeFolder(dcM, folderName);
		}
		else
		{
			vmFolderRef = (ManagedObjectReference) super.getProp(vmRef, "parent");
		}

		VirtualMachineCloneSpec cloneSpec = new VirtualMachineCloneSpec();

		cloneSpec.setTemplate(isTemplate);

		VirtualMachineRelocateSpec relocSpec = new VirtualMachineRelocateSpec();
		ManagedObjectReference resourcepoolmor = null;
		ManagedObjectReference host = null;
		if(param.get("HOST_HV_ID") != null && !"".equals(param.get("HOST_HV_ID"))) // host 셋팅
		{
			host = makeManagedObjectReference(VMWareCommon.OBJ_TYPE_HOST, (String)param.get("HOST_HV_ID"));
			relocSpec.setHost(host);
			
			resourcepoolmor = super.findObject((ManagedObjectReference)super.getProp(host, "parent"), "ResourcePool", "Resources");
			
			
		}
		else
		{
			
			if(resourcePool == null || "".equals(resourcePool))
			{
				resourcePool="Resources";

			}
			resourcepoolmor = super.findObject(dcM, "ResourcePool", resourcePool);
			 
		}
		relocSpec.setPool(resourcepoolmor);
		if(host == null)
		{
			host = (ManagedObjectReference)getProp(vmRef,"runtime.host");
		}
		Set dsInHost = super.getDsInHost(host);
		if(dsNames != null && dsNames.length>0)
		{
			String dsNm = getAvailableDs(dsInHost, dsNames,diskSizeGB);
			result.put(VMWareAPI.PARAM_DATASTORE_NM, dsNm);
			if(dsNm != null)
			{
				ManagedObjectReference datastoreRef = findObjectFromRoot(VMWareCommon.OBJ_TYPE_DS, dsNm);
				relocSpec.setDatastore(datastoreRef);
			}
		}
		cloneSpec.setLocation(relocSpec);
		
		 
		if(!isTemplate)
		{
			List<IPInfo> ipList = (List<IPInfo>)param.get("IP");
			String hostName = (String) param.get(VMWareAPI.PARAM_HOST_NM);
			if(hostName == null)
			{
				hostName = vmName;
			}
			boolean isWin  = isWindow(vmRef);
			int idx=0;
			CustomizationSpecItem cspec = null;
			ManagedObjectReference cpm = super.connectionMng.getServiceContent()
					.getCustomizationSpecManager();
			if(isWin)
			{
				cspec = vimPort.getCustomizationSpec(cpm,
						window_csi);
				
			}
			else
			{
				cspec = vimPort.getCustomizationSpec(cpm,
						linux_csi);
			}
			ArrayList deviceConfigSpec = new ArrayList<VirtualDeviceConfigSpec>();
			for(IPInfo ipInfo : ipList)
			{
				String ip = ipInfo.ip;
				String gateway = ipInfo.gateway;
				String subnet= ipInfo.subnetMask;
				
				
				System.out.println(ip + ":" + gateway + ":" + subnet + ":" + ipInfo.network);
				
				
				if(cspec != null)
				{
					setCustomizationSpecInfo(idx,isWin,cloneSpec, cspec, hostName, ip, gateway, subnet );
				}
				 
				else
				{
					this.setLinuxNetwork(idx, cloneSpec,hostName, ip,   gateway,   subnet);
				}
				if(ipInfo.network != null)
				{
					//cspec.getSpec().getNicSettingMap().remove(cspec.getSpec().getNicSettingMap().size()-1);
					 
					if(ipInfo.network.startsWith("network"))
					{
						ManagedObjectReference nt = super.makeManagedObjectReference(VMWareAPI.OBJ_TYPE_NETWORK, ipInfo.network);
						deviceConfigSpec.add(this.setStandardNetwork(vmRef, nt, ipInfo.nicId));
					}
					else
					{
						ManagedObjectReference nt = super.makeManagedObjectReference(VMWareAPI.OBJ_TYPE_DVP, ipInfo.network);
						deviceConfigSpec.add(this.setDVPNetwork(nt, ipInfo.nicId));
					}
					 
				}
				idx++;
			
			}
			if(cspec != null)
			{	
				int diff = cspec.getSpec().getNicSettingMap().size() - ipList.size();
				for(int i=0; i < diff; i++)
				{	
					cspec.getSpec().getNicSettingMap().remove(cspec.getSpec().getNicSettingMap().size()-1);
				}
			}
			
			VirtualMachineConfigSpec spec = new VirtualMachineConfigSpec();
			

		
			
			cloneSpec.setPowerOn(false);
			spec.setNumCPUs(cpu);
			spec.setMemoryMB( 1024l * memSize  );

			if(portgroupNm != null && !"".equals(portgroupNm))
			{
				VirtualDeviceConfigSpec nicSpec = setNetwork(  portgroupNm, nicKey );
				deviceConfigSpec.add(nicSpec);
			}
			//String diskPath = getVMDiskFileName(dsNm, vmName);
			//List<VirtualDevice> hardwares = super.getHardwareDevice(vmRef);
			List<Map> dataDisk = (List<Map>) param.get(HypervisorAPI.PARAM_DATA_DISK_LIST);
			createDisk = new VCreateDisk(this.connectionMng);
			
			for(Map m : dataDisk)
			{
				System.out.println(m.get(HypervisorAPI.PARAM_DATASTORE_NM));
				VirtualDeviceConfigSpec diskSpec = createVirtualDisk(vmRef, m, dsNames,  NumberUtil.getInt(m.get("DISK_SIZE")), dsInHost);
				if(diskSpec != null) deviceConfigSpec.add(diskSpec);
			}
			if(deviceConfigSpec.size()>0)
			{
				spec.getDeviceChange().addAll(deviceConfigSpec);
			}
			if(param.get("PURPOSE") != null) spec.setAnnotation((String)param.get("PURPOSE"));
			cloneSpec.setConfig(spec );
		}


		ManagedObjectReference cloneTask =
				vimPort.cloneVMTask(vmRef, vmFolderRef, vmName, cloneSpec);

		return cloneTask;

	}

}

