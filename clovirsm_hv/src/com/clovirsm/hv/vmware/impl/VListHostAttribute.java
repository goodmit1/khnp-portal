package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfHostGraphicsInfo;
import com.vmware.vim25.HostGraphicsInfo;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.RetrieveOptions;

/**
 * 데이터 센터 내 호스트 속성 목록
 * @author 윤경
 *
 */
public class VListHostAttribute extends com.clovirsm.hv.vmware.CommonAction {

	public VListHostAttribute(ConnectionMng m) {
		super(m);
		// TODO Auto-generated constructor stub
	}
	
	protected ManagedObjectReference getParent(String dc, String cluster) throws Exception
	{
		return super.getDataCenter(dc);
	}
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param,
			Map result) throws Exception {
		ManagedObjectReference clusterMof = getParent(dc, cluster);
		 
		Map<ManagedObjectReference, Map<String, Object>> hosts =
				 inContainerByType(clusterMof,
						getObjType(),
						attributes() );
		List list = print(param,hosts);
		
		result.put(HypervisorAPI.PARAM_LIST, list);
		return null;

	}
	protected List print(Map param, Map<ManagedObjectReference, Map<String, Object>> hosts)
	{
		List result = new ArrayList();
		for (ManagedObjectReference host : hosts.keySet()) {
			
			Map<String, Object> attribute = hosts.get(host);
			if(!isTarget(attribute)) continue;
			Map newAttr = new HashMap();
			newAttr.put("OBJ_ID", host.getValue());
			for(String key : attribute.keySet())
			{
				Object o = attribute.get(key);
				if(key.equals("vm"))
				{
					newAttr.put("VM_CNT",((com.vmware.vim25.ArrayOfManagedObjectReference)o).getManagedObjectReference().size());
				}
				else if(key.equals("config.graphicsInfo")) {
					List<HostGraphicsInfo> ginfos = ((ArrayOfHostGraphicsInfo)o).getHostGraphicsInfo();
					int gTotal = 0;
					for(HostGraphicsInfo g:ginfos) {
						if(g.getDeviceName().startsWith("NVIDIA")) {
							if(gTotal ==0) {
								String[] arr = g.getDeviceName().split(" ");
								if(arr.length==2) {
									newAttr.put("GPU_MODEL", arr[1].toLowerCase());
									newAttr.put("GPU_MODEL_MAX",  (int)Math.ceil(  1.0*g.getMemorySizeInKB()/1024/1024));
								}
							}
							gTotal += g.getMemorySizeInKB();
						}
					}
					if(gTotal>0) newAttr.put("GPU_SIZE", (int)Math.ceil(  1.0*gTotal/1024/1024)); //G
				}
				else
				{
					int pos = key.lastIndexOf(".");
					newAttr.put(key.substring(pos+1).toUpperCase(), o);
				}
			}
			addPerfInfo(param, host, newAttr);
			result.add(newAttr);

		}
		return result;
	}
	
	protected boolean isTarget(Map<String, Object> attribute) {
		 
		return true;
	}

	protected void addPerfInfo(Map param, ManagedObjectReference host, Map newAttr) 
	{
		 
		VGetPerf perf = new VGetPerf(this.connectionMng);
		Map perfInfo;
		try {
			perfInfo = (Map)perf.counterInfo(host, param.get(VMWareAPI.PARAM_START_DT) == null ?  VMWareAPI.PERF_PERIOD_1H:null, param);
			if(perfInfo != null)
			{
				newAttr.putAll(perfInfo);
			}
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
		
	}

	protected String getObjType()
	{
		return VMWareCommon.OBJ_TYPE_HOST;
	}
	protected String[] attributes()
	{
		return new String[] {
				"config.graphicsInfo",
				"summary.config.name",
				"summary.hardware.memorySize",
				"summary.hardware.cpuModel",
				"summary.hardware.numCpuCores",
				"summary.hardware.numCpuPkgs",
				"summary.config.product.fullName",
				"summary.overallStatus", "vm"
		};
	}
}
