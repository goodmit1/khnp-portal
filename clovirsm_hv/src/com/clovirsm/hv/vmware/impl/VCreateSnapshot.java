package com.clovirsm.hv.vmware.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.HypervisorException;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.InvalidCollectorVersionFaultMsg;
import com.vmware.vim25.InvalidPropertyFaultMsg;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.RuntimeFaultFaultMsg;
import com.vmware.vim25.VirtualMachineFileLayoutEx;
import com.vmware.vim25.VirtualMachineFileLayoutExDiskLayout;
import com.vmware.vim25.VirtualMachineFileLayoutExDiskUnit;
import com.vmware.vim25.VirtualMachineFileLayoutExFileInfo;
import com.vmware.vim25.VirtualMachineFileLayoutExSnapshotLayout;
import com.vmware.vim25.VirtualMachineSnapshotInfo;
import com.vmware.vim25.VirtualMachineSnapshotTree;
/**
 * 스냅샷 추가
 * @author 윤경
 *
 */
public class VCreateSnapshot extends com.clovirsm.hv.vmware.CommonAction{

	public VCreateSnapshot(ConnectionMng m) {
		super(m);

	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {

		return createSnapshot(result, super.getVM(param), (String)param.get(HypervisorAPI.PARAM_SNAPSHOT_NM), "");
		
	}
	ManagedObjectReference traverseSnapshotInTree(
			List<VirtualMachineSnapshotTree> snapTree, String findName ) {
		ManagedObjectReference snapmor = null;
		if (snapTree == null) {
			return snapmor;
		}
		for (VirtualMachineSnapshotTree node : snapTree) {

			if (findName != null && node.getName().equalsIgnoreCase(findName)) {
				return node.getSnapshot();
			} else {
				List<VirtualMachineSnapshotTree> listvmst =
						node.getChildSnapshotList();
				List<VirtualMachineSnapshotTree> childTree = listvmst;
				snapmor = traverseSnapshotInTree(childTree, findName );
			}
		}
		return snapmor;
	}
	ManagedObjectReference getSnapshotReference(
			ManagedObjectReference vmmor,   String snapName) throws Exception{
		VirtualMachineSnapshotInfo snapInfo =
				(VirtualMachineSnapshotInfo) getProp(vmmor, "snapshot");
		ManagedObjectReference snapmor = null;
		if (snapInfo != null) {
			List<VirtualMachineSnapshotTree> listvmst =
					snapInfo.getRootSnapshotList();
			snapmor = traverseSnapshotInTree(listvmst, snapName );
			if (snapmor == null) {
				throw new NotFoundException( "snapshot:" + snapName);
			}
		} else {
			throw new NotFoundException( "snapshot:" + snapName);
		}
		return snapmor;
	}
	protected ManagedObjectReference createSnapshot(Map result, ManagedObjectReference vmRef , String snapshotname, String description) throws Exception
	{
	 
	
		ManagedObjectReference taskMor =
				vimPort.createSnapshotTask(vmRef, snapshotname, description, false,
						false);
		return taskMor;
		 

	}
	
}
