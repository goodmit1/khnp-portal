package com.clovirsm.hv.vmware.impl;

import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;

/**
 * 스냅 샷 이름 변경
 * @author 윤경
 *
 */
public class VRenameSnapshot extends VDeleteSnapShot{

	public VRenameSnapshot(ConnectionMng m) {
		super(m);
		 
	}
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		 
		renameSnapshot(super.getVM(param),(String)param.get(HypervisorAPI.PARAM_SNAPSHOT_NM),(String)param.get(HypervisorAPI.PARAM_OBJECT_NEW_NM));
		return null;
	}
	
	protected void renameSnapshot(ManagedObjectReference vmRef, String snapshotname, String newName) throws Exception
	{
		 
	 
		ManagedObjectReference snapmor = getSnapshotReference(vmRef,  snapshotname);
	    if (snapmor != null) {
	    	vimPort.renameSnapshot(snapmor, newName, "");
	        
	  		
	    }
	}
}
