package com.clovirsm.hv.vmware.impl;

 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.AlarmInfo;
import com.vmware.vim25.AlarmState;
import com.vmware.vim25.ArrayOfAlarmState;
import com.vmware.vim25.ArrayOfEvent;
import com.vmware.vim25.Event;
import com.vmware.vim25.ManagedObjectReference;

/**
 * 트리거된 알람 목록
 * @author 윤경
 *
 */
public class VListTriggeredAlarm extends CommonAction {

	public VListTriggeredAlarm(ConnectionMng m) {
		super(m);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result)
			throws Exception {
		ManagedObjectReference vmRef = getObject(dc, param);
		
		result.put(VMWareAPI.PARAM_LIST, list(vmRef));
		return null;
	}
	protected  ArrayOfAlarmState listAlarmState(ManagedObjectReference mor) throws Exception
	{
		return (ArrayOfAlarmState) super.getProp(mor, "triggeredAlarmState");
	}
	protected void listConfigIssue(List<Map> result, ManagedObjectReference mor) throws Exception
	{
		List<Event> list = ((ArrayOfEvent)super.getProp(mor, "configIssue")).getEvent();
		for(Event e : list)
		{
			 Map m = new HashMap();
			 m.put(VMWareAPI.PARAM_CATEGORY,  "YELLOW");
			 m.put(VMWareAPI.PARAM_CRE_TIME, e.getCreatedTime().toGregorianCalendar().getTime().getTime());
			 
			 m.put(VMWareAPI.PARAM_COMMENT,e.getFullFormattedMessage());
			 m.put(VMWareAPI.PARAM_TARGET, "");
			 result.add(m);
		}
	}
	protected List<Map> list(ManagedObjectReference mor) throws Exception
	{
		 ArrayOfAlarmState array = listAlarmState(mor);
		 List<AlarmState> list = array.getAlarmState();
		 List<Map> result = new ArrayList();
		 for(AlarmState a : list)
		 {
			 Map m = new HashMap();
			 m.put(VMWareAPI.PARAM_CATEGORY,a.getOverallStatus());
			 m.put(VMWareAPI.PARAM_CRE_TIME, a.getTime().toGregorianCalendar().getTime().getTime());
			 AlarmInfo  info = (AlarmInfo)getProp(a.getAlarm(), "info");
			 m.put("ALARM_NM",info.getName());
			 m.put(VMWareAPI.PARAM_COMMENT,info.getDescription());
			 m.put("TARGET_TYPE", a.getEntity().getType());
			 m.put("TARGET_ID", a.getEntity().getValue());
			 m.put(VMWareAPI.PARAM_TARGET, getProp(a.getEntity(), "name"));
			 result.add(m);
		 }
		 listConfigIssue(result, mor);
		 return result;
	}
}
