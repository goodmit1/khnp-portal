package com.clovirsm.hv.vmware.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.vmware.CommonAction;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfPerfCounterInfo;
import com.vmware.vim25.DynamicProperty;
import com.vmware.vim25.InvalidPropertyFaultMsg;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.ObjectContent;
import com.vmware.vim25.ObjectSpec;
import com.vmware.vim25.PerfCounterInfo;
import com.vmware.vim25.PropertyFilterSpec;
import com.vmware.vim25.PropertySpec;
import com.vmware.vim25.RetrieveOptions;
import com.vmware.vim25.RetrieveResult;
import com.vmware.vim25.RuntimeFaultFaultMsg;
import com.vmware.vim25.VimPortType;

public class VGetAllPerfMetricInfo extends CommonAction{

	public VGetAllPerfMetricInfo(ConnectionMng m) {
		super(m);
	 
	}
	List<ObjectContent> retrievePropertiesAllObjects(
			VimPortType  vimPort, ManagedObjectReference propCollectorRef ,List<PropertyFilterSpec> listpfs) throws RuntimeFaultFaultMsg, InvalidPropertyFaultMsg {

	    RetrieveOptions propObjectRetrieveOpts = new RetrieveOptions();

	    List<ObjectContent> listobjcontent = new ArrayList<ObjectContent>();

	    RetrieveResult rslts =
	            vimPort.retrievePropertiesEx(propCollectorRef, listpfs,
	                    propObjectRetrieveOpts);
	    if (rslts != null && rslts.getObjects() != null
	            && !rslts.getObjects().isEmpty()) {
	        listobjcontent.addAll(rslts.getObjects());
	    }
	    String token = null;
	    if (rslts != null && rslts.getToken() != null) {
	        token = rslts.getToken();
	    }
	    while (token != null && !token.isEmpty()) {
	        rslts =
	                vimPort.continueRetrievePropertiesEx(propCollectorRef, token);
	        token = null;
	        if (rslts != null) {
	            token = rslts.getToken();
	            if (rslts.getObjects() != null && !rslts.getObjects().isEmpty()) {
	                listobjcontent.addAll(rslts.getObjects());
	            }
	        }
	    }

	    return listobjcontent;
	}
	List<PerfCounterInfo> getPerfCounters( VimPortType  vimPort, ManagedObjectReference perfManager, ManagedObjectReference propCollectorRef) throws RuntimeFaultFaultMsg, InvalidPropertyFaultMsg {
	    
		
		List<PerfCounterInfo> pciArr = null;
	    // Create Property Spec
	    PropertySpec propertySpec = new PropertySpec();
	    propertySpec.setAll(Boolean.FALSE);
	    propertySpec.getPathSet().add("perfCounter");
	    propertySpec.setType("PerformanceManager");
	    List<PropertySpec> propertySpecs = new ArrayList<PropertySpec>();
	    propertySpecs.add(propertySpec);

	    // Now create Object Spec
	    ObjectSpec objectSpec = new ObjectSpec();
	    objectSpec.setObj(perfManager);
	    List<ObjectSpec> objectSpecs = new ArrayList<ObjectSpec>();
	    objectSpecs.add(objectSpec);

	    // Create PropertyFilterSpec using the PropertySpec and ObjectPec
	    // created above.
	    PropertyFilterSpec propertyFilterSpec = new PropertyFilterSpec();
	    propertyFilterSpec.getPropSet().add(propertySpec);
	    propertyFilterSpec.getObjectSet().add(objectSpec);
	    List<PropertyFilterSpec> propertyFilterSpecs =
	            new ArrayList<PropertyFilterSpec>();
	    propertyFilterSpecs.add(propertyFilterSpec);
	    List<PropertyFilterSpec> listpfs =
	            new ArrayList<PropertyFilterSpec>(1);
	    listpfs.add(propertyFilterSpec);
	    List<ObjectContent> listobjcont =
	            retrievePropertiesAllObjects(vimPort, propCollectorRef, listpfs);

	    if (listobjcont != null) {
	        for (ObjectContent oc : listobjcont) {
	            List<DynamicProperty> dps = oc.getPropSet();
	            if (dps != null) {
	                for (DynamicProperty dp : dps) {
	                    List<PerfCounterInfo> pcinfolist =
	                            ((ArrayOfPerfCounterInfo) dp.getVal())
	                                    .getPerfCounterInfo();
	                    pciArr = pcinfolist;
	                }
	            }
	        }
	    }
	    return pciArr;
	}
	@Override
	protected ManagedObjectReference run1(String dc, String cluster, Map param, Map result) throws Exception {
		List<PerfCounterInfo> list = getPerfCounters(super.connectionMng.getVimPort(), super.connectionMng.getServiceContent().getPerfManager(),super.connectionMng.getServiceContent().getPropertyCollector());
		result.put("LIST", list);
		
		
		return null;
	}

}
