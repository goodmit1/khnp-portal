package com.clovirsm.hv.vmware.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ArrayOfVirtualDevice;
import com.vmware.vim25.GuestDiskInfo;
import com.vmware.vim25.GuestInfo;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualDisk;
import com.vmware.vim25.VirtualMachinePowerState;
import com.vmware.vim25.VirtualMachineRuntimeInfo;
import com.vmware.vim25.VirtualPCIPassthrough;
import com.vmware.vim25.VirtualPCIPassthroughVmiopBackingInfo;

/**
 * 데이터 센터 내 호스트 속성 목록
 * @author 윤경
 *
 */
public class VListVMPerfInDC extends VListHostAttribute {
	Set<String> excludePath ;
	public VListVMPerfInDC(ConnectionMng m) {
		super(m);
		try {
			String[] path= HVProperty.getInstance().getStrings("disk_exclude_path");
			if(path != null)
			{
				excludePath = new HashSet();
				for(String p:path)
				{
					excludePath.add(p);
				}
			}
		} catch (Exception e) {
		 
			e.printStackTrace();
		}
	}
	@Override
	protected boolean isTarget(Map<String, Object> attribute) {
		Object role = attribute.get("config.ftInfo.role");
		if(role == null || NumberUtil.getInt(role)==1) return true; 
		return false;
	}

	protected void addDiskInfo(ManagedObjectReference vm, Map newAttr ) throws Exception
	{
		 newAttr.put("VM_HV_ID", vm.getValue());
		 VirtualMachinePowerState powerState = (VirtualMachinePowerState)newAttr.remove("POWERSTATE");
		 if(powerState != null)
		 {
			 newAttr.put(VMWareAPI.PARAM_RUN_CD,powerState.value().equals("poweredOn") ? VMWareAPI.RUN_CD_RUN : VMWareAPI.RUN_CD_STOP);
		 }
		 ArrayOfVirtualDevice hdeviceList = (ArrayOfVirtualDevice)newAttr.remove("DEVICE");
		 long total = 0; 
		 int gpuSize = 0;
		 if(hdeviceList != null)
		 {	 
			 List<VirtualDevice> deviceList =  hdeviceList.getVirtualDevice();
			
	         for (VirtualDevice device : deviceList) {
	         
	        	 if(device instanceof VirtualDisk)
	        	 {
	        		 
	                total +=((VirtualDisk)device).getCapacityInKB();
	                 
	        	 }
	        	 else if(device instanceof VirtualPCIPassthrough ) {
	        		VirtualPCIPassthroughVmiopBackingInfo backing = (VirtualPCIPassthroughVmiopBackingInfo)((VirtualPCIPassthrough)device).getBacking();
	 				String summary = backing.getVgpu();
	 				//grid_p40-12q
	 				int pos = summary.lastIndexOf("grid_");
	 				if(pos>=0) {
	 					int pos1 = summary.lastIndexOf("-");
	 					String gmodel = summary.substring(pos+5, pos1);
	 					String q = summary.substring(pos1+1, summary.length()-1);
	 					newAttr.put("GPU_MODEL", gmodel);
	 					gpuSize += Integer.parseInt( q) ;
	 				}
	 			}
	         }
		 }
		 newAttr.put("GPU_SIZE", gpuSize);
         GuestInfo guest = (GuestInfo)newAttr.remove("GUEST");
		  List<GuestDiskInfo> diskList = guest.getDisk();
		  long gtotal = 0;
		  long free = 0;
		  for(GuestDiskInfo info : diskList)
		  {
			  //System.out.println(newAttr.get("NAME") + "'s disk="+ info.getDiskPath() + ":"  + info.getFreeSpace() + "/" + info.getCapacity());
			  if(excludePath != null  && excludePath.contains(info.getDiskPath())) continue;	
			  gtotal += info.getCapacity();
			  free += info.getFreeSpace();
		  }
		  long used = gtotal - free;
		  //newAttr.put("CPUMHZ", 1.0 * NumberUtil.getInt(newAttr.get("cpu_usagemhz")) / NumberUtil.getInt(newAttr.get("cpu_usage")) * 100 );
		  
		  
		  ManagedObjectReference host = (ManagedObjectReference)newAttr.remove("HOST");
		  if(host != null) {
			  newAttr.put("HOST_NM",super.getProp(host, "name"));
		  }
		  newAttr.put("disk_totalgb", 1.0f * total/1024/1024);
		  newAttr.put("disk_usedgb", 1.0f * used/1024/1024/1024);
		  newAttr.put("ipAddress", guest.getIpAddress());
		  newAttr.put("GUESTFULLNAME", guest.getGuestFullName());
		 
		  if(free>0)
		  {	  
			  newAttr.put("disk_usage_per", 1.0f * used / (total*1024) * 100 );
		  }
          
		 
	}
	
	protected void addPerfInfo(Map param, ManagedObjectReference host, Map newAttr) {
		super.addPerfInfo(param, host, newAttr);
		 
		try {
			addDiskInfo(host, newAttr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		  
	  }
	protected ManagedObjectReference getParent(String dc, String cluster) throws Exception
	{
		if(cluster == null)
		{
			return super.getDataCenter(dc);
		}
		return super.getCluster(dc, cluster);
	}
	 
	protected String getObjType()
	{
		return VMWareCommon.OBJ_TYPE_VM;
	}
	protected String[] attributes()
	{
		return new String[] {
				"summary.config.name" ,"summary.config.template", // template 여부
				"summary.config.memorySizeMB",
				"summary.config.numCpu" , "guest", "runtime.powerState", "runtime.host", "config.hardware.device", "config.ftInfo.role"
		};
	}
}
