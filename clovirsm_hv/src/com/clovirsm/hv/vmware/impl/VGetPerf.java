package com.clovirsm.hv.vmware.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.vmware.connection.ConnectionMng;
import com.vmware.vim25.ManagedObjectReference;
import com.vmware.vim25.PerfEntityMetric;
import com.vmware.vim25.PerfEntityMetricBase;
import com.vmware.vim25.PerfMetricIntSeries;
import com.vmware.vim25.PerfMetricSeries;
import com.vmware.vim25.PerfQuerySpec;
import com.vmware.vim25.PerfSampleInfo;

/**
 * 성능 정보 가져오기
 * @author 윤경
 *
 */
public class VGetPerf extends VListPerfHistory{
	Date startDate =  null;
	Date finishDate = null;
	boolean innerTime = false;
	public VGetPerf(ConnectionMng m) {
		super(m);
		 
	}
	protected Object counterInfo(ManagedObjectReference vmRef , String periodKubun, Map param) throws Exception {
		try {
			startDate = ((Date)param.get(VMWareCommon.PARAM_START_DT));
			finishDate = ((Date)param.get(VMWareCommon.PARAM_FINISH_DT));
			if(param.get("INNER_HOUR") != null)
			{
				innerTime = NumberUtil.getBoolean(param.get("INNER_HOUR"),false);
			}
			Map result = (Map) super.counterInfo(vmRef, periodKubun, param);
			addDiskInfo(vmRef, result);
			addSpecInfo(vmRef, result);
			return result;
		}
		catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
				
	}
 
	protected void addSpecInfo(ManagedObjectReference vmRef, Map result) throws Exception{
		 
		
	}
	protected void addDiskInfo(ManagedObjectReference vmRef, Map result) throws Exception {
		 
		
	}
	protected String[] getPerfTypes(String objType) throws Exception 
	{
		String[] ids = HVProperty.getInstance().getStrings("vmware.perf.obj." + objType);
		if(ids==null)
		{
			throw new Exception(objType + "'s perf not found");
		}
		return ids;
	}
	@Override
	protected void setQSpeckExtraInfo(PerfQuerySpec qSpec) {
	 
		qSpec.setMaxSample(1);
	}

	/*
	@Override
	void setTimeInterval(PerfQuerySpec qSpec, String periodKubun) throws Exception
	{
		ManagedObjectReference perfManager = super.connectionMng.getServiceContent().getPerfManager();
		PerfProviderSummary perfProviderSummary = vimPort
				.queryPerfProviderSummary(perfManager, qSpec.getEntity());
		
		if (!perfProviderSummary.isCurrentSupported()) {
			qSpec.setIntervalId(300);
		} else {
			qSpec.setIntervalId(perfProviderSummary.getRefreshRate());
		}
		qSpec.setMaxSample(1);
	}
	*/
	private boolean isHourBetween(Date date, Date fromDate, Date toDate)
	{
		return (date.getHours()>fromDate.getHours() || (date.getHours()==fromDate.getHours() && date.getMinutes()>=fromDate.getMinutes() )) && (date.getHours()<toDate.getHours() || ((date.getHours()==toDate.getHours() && date.getMinutes()<=toDate.getMinutes() )));
	}
	protected boolean isTargetTime(long date)
	{
		
		if(!innerTime || startDate==null || finishDate==null)
		{
			return true;
		}
		else
		{
			return isHourBetween(new Date(date), startDate, finishDate);
		}
	}
	@Override
	protected Object displayValues(String periodKubun,List<PerfEntityMetricBase> values,Map<Integer, String> perfMap  ) {
		Map result = new HashMap();
		for(PerfEntityMetricBase v:values) {
			result.putAll((Map)displayValues(periodKubun, v, perfMap));
		}
		return result;
	}
	
	protected Object displayValues(String periodKubun, PerfEntityMetricBase metric,Map<Integer, String> perfMap  ) {
		Map result = new HashMap();
		 
			List<PerfMetricSeries> listperfmetser = ((PerfEntityMetric) metric
					 ).getValue();
			List<PerfSampleInfo> listperfsinfo = ((PerfEntityMetric) metric
					 ).getSampleInfo();
			if (listperfsinfo == null || listperfsinfo.size() == 0) {
				 
				return result;
			}
			result.put(VMWareAPI.PARAM_CRE_TIME,  listperfsinfo.get(0).getTimestamp().toGregorianCalendar().getTimeInMillis());
			for (int vi = 0; vi < listperfmetser.size(); ++vi) {

				if (listperfmetser.get(vi) instanceof PerfMetricIntSeries) {
					PerfMetricIntSeries val = (PerfMetricIntSeries) listperfmetser.get(vi);
					List<Long> listlongs = val.getValue();
					long total = 0;
					double max = 0;
					int count = 0;
					for(int j=0; j<listlongs.size(); j++)
					{
						long date = listperfsinfo.get(j).getTimestamp().toGregorianCalendar().getTimeInMillis();
						if(isTargetTime(date))
						{	
							//System.out.println(new Date(date));
							long v = listlongs.get(j);
							if(v>max) max = v;
							total += v;
							count++;
						}
					}
					if(count==0) continue;		
					String metricName = perfMap.get(val.getId().getCounterId());
					
					double lval = 1.0 * total / count;
					
					if(divideTarget.contains(metricName))
					{
						lval= lval / 100 ;
						max = 1.0 * max / 100;
					}
					System.out.println(metricName  + "=" + listlongs.size()+ ":"+ total + "/" + count + "," + max);
					result.put(metricName, lval); 
					result.put(metricName + "_max", max); 
				}
			}
		 
		return result;
	}
}
