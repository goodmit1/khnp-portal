package com.clovirsm.hv.vmware;

import java.util.Map;

import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public abstract class VCRestCommon {
	protected RestClient client;
	public VCRestCommon(VCRestConnection conn)
	{
		if(conn != null) {
			this.client = conn.getRestClient();
		}
	}
	public abstract void run(Map param, Map result) throws Exception ;
	 
	 
}
