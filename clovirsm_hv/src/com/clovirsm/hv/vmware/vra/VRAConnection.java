package com.clovirsm.hv.vmware.vra;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;

public class VRAConnection implements IConnection{
	protected String url;
	String userId;
	String pwd;
	 
	protected RestClient client;
	protected Date expire;
	protected String token;
	protected boolean connect = false;
	public RestClient getRestClient()
	{
		return client;
	}
	 
	@Override
	public String getURL() {
		return url;
	}

 
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		this.url = url;
		 
		client = new RestClient(url);
		client.setMimeType("application/json");
		client.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		JSONObject json = new JSONObject();
		json.put("username",userId);
		json.put("password", pwd);
		json.put("tenant", prop.get("tenant"));  
		JSONObject result = (JSONObject)client.post("/identity/api/tokens", json.toString());
		expire = client.parseDate(result.getString("expires"));
		if(expire.compareTo(new Date())<=0)
		{
			expire = new Date();
			expire.setMinutes(expire.getMinutes()+30);
		}
		token = result.getString("id");
		client.setToken(token);
		connect = true;
	}

	 

	@Override
	public boolean isConnected() {
		return client == null || expire.compareTo(new Date())>0;
		
	}

	public static void main(String[] args)
	{
		VRAConnection conn = new VRAConnection();
		Map prop = new HashMap();
		prop.put("tenant", "vsphere.local");
		try {
			conn.connect("https://172.16.33.210", "configurationadmin", "VMware1!", prop);
			Map param = new HashMap();
			VListCatalog list = new VListCatalog(conn);
			list.run(param, param);
			//System.out.println(conn.getRestClient().getJSON(  "/forms-service/api/forms/aa9ecece-1e7e-4c81-92d9-a9565cdc0046" ));
			System.out.println(param);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}
	@Override
	public void disconnect() {
		client = null;
		
	}
}
