package com.clovirsm.hv.vmware.vra;

import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.RestClient;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class VListCatalog extends VRACommon{

	public VListCatalog(VRAConnection conn) {
		super(conn);
		 
	}

	private String[] getDCIp(String  sourceMachineId) throws Exception
	{
		 
		JSONObject sourceMachine = client.getJSON("/iaas-proxy-provider/api/source-machines/" + sourceMachineId + "?platformTypeId=*");
		String endPointName = sourceMachine.getString("endpointName");
		JSONObject endPoint = client.getJSON("/endpoint-configuration-service/api/endpoints/name/" + endPointName);
		JSONArray entries = endPoint.getJSONObject("extensionData").getJSONArray("entries");
		for(int k=0; k<entries.length(); k++)
		{
			if("address".equals(entries.getJSONObject(k).getString("key")))
			{
				return new String[]{ entries.getJSONObject(k).getJSONObject("value").getString("value"), sourceMachine.getString("hostName")} ;
			}
		}
		return null;
	}
	@Override
	public void run(Map param, Map result ) throws Exception {
		JSONObject json = client.getJSON(  "/catalog-service/api/consumer/entitledCatalogItemViews" );
		System.out.println(json);
		JSONArray arr = json.getJSONArray("content");
		List list = new ArrayList();
		for(int i=0; i < arr.length(); i++)
		{
			JSONObject json1 = arr.getJSONObject(i);
			Map m = new HashMap();
			 
			m.put("CATALOG_NM", json1.getString("name"));
			m.put("CATALOG_ID", json1.getString("catalogItemId"));
			m.put("CATALOG_CMT", json1.getString("description"));
			m.put("INS_TMS", client.parseDate(json1.getString("dateCreated")));
			
			JSONObject icon = client.getJSON("/catalog-service/api/icons/" +  json1.getString("iconId") );
			m.put("ICON_TYPE", icon.getString("contentType"));
			m.put("ICON",  icon.getString("image") );
			
			JSONObject catalogItem = client.getJSON("/catalog-service/api/catalogItems/" + json1.getString("catalogItemId") );
			System.out.println(catalogItem);
			try
			{
				/*
				*/
				JSONObject template = client.getJSON(  "/catalog-service/api/consumer/entitledCatalogItems/" + json1.getString("catalogItemId") + "/requests/template" );
				JSONObject schema = client.getJSON("/catalog-service/api/consumer/entitledCatalogItems/" + json1.getString("catalogItemId")  + "/requests/schema");
				
				FormGen formGen  = VRAAPI.getFormGet();
				JSONObject formInfo = new JSONObject();
				
				if(catalogItem != null && "external".equals(catalogItem.getJSONObject("forms").getJSONObject("requestDetails").getString("type")))
				{
				
					addXaasForm(formGen, formInfo,  json1.getString("catalogItemId") );
					// xaas form https://vra.goodmit.tech.kr/content-management-service/api/contents/0d069e79-fbf6-46e7-96b3-309bd88a89ff/data
				}
				else
				{
					
					formInfo.put("html", formGen.formGen(template, schema ));
					formInfo.put("template", formGen.getJspTemplateArr(  ));
					m.put("IP_CNT", formGen.getIpCnt());
					
					List<String> dcIds = formGen.getDCIds();
					List<String[]> dcIps = new ArrayList();
					for(String dcId : dcIds)
					{	
						
						  String[] ip = getDCIp(dcId);
						  if(ip != null)
						  {
							  dcIps.add(ip);
						  }
						  else
						  {
							  dcIps.add(new String[]{"",""});
						  }
						
					}
					m.put("DC_IPS", dcIps);
				}
				m.put("FORM_INFO", formInfo.toString());
				
			}
			catch( Exception ignore)
			{
				 ignore.printStackTrace();
			}		
			list.add(m);
			
		}
		result.put(HypervisorAPI.PARAM_LIST, list);
		
		// form 정보 https://vra.goodmit.tech.kr/forms-service/api/forms/parent/vsphere.local!%3A%3A!Win2012R2NoTools/type/requestform
	}
	private void addXaasForm(FormGen formGen, JSONObject form, String templateId) throws Exception
	{
		JSONObject content = client.getJSON("/catalog-service/api/consumer/entitledCatalogItems/" + templateId + "/requests/schema");
		JSONArray fields = content.getJSONArray("fields");
	 
		 
		form.put("extra_html", formGen.formGenCustom(fields));
		 

		
	}

	public static void main(String[] args)
	{
		VRAConnection conn = new VRAConnection();
		Map prop = new HashMap();
		prop.put("tenant", "vsphere.local");
		try {
			conn.connect("https://172.16.33.116", "configurationadmin", "VMware1!", prop);
			String requestId = "6c4ad903-4f5a-45fa-ad56-fcd3d129a7a5";
			prop.put("VRA_REQUEST_ID", requestId);
			//VCatalogRequestDelete list = new VCatalogRequestDelete(conn);

			VListCatalog list = new VListCatalog(conn );
			list.run(prop, prop, null);
			
			System.out.println(prop.get(HypervisorAPI.PARAM_LIST));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
			
	}
}
