package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VListIPRange extends VRACommon8{

	public VListIPRange(VRAConnection conn) {
		super(conn);
		 
	}

	@Override
	public void run(Map param, Map result) throws Exception {
		JSONObject about = conn.getRestClient().getJSON("/iaas/api/about");
		String ver = about.getString("latestApiVersion");
		JSONObject ipRanges = conn.getRestClient().getJSON("/iaas/api/network-ip-ranges?apiVersion=" + ver);
		JSONArray content = ipRanges.getJSONArray("content");
		List list = new ArrayList();
		for(int i=0; i < content.length(); i++) {
			Map m = new HashMap();
			m.put("startIPAddress", content.getJSONObject(i).getString("startIPAddress"));
			m.put("endIPAddress", content.getJSONObject(i).getString("endIPAddress"));
			list.add(m);
		}
		result.put("LIST", list);
	}

}
