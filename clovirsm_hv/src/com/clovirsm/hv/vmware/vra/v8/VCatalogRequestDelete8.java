package com.clovirsm.hv.vmware.vra.v8;

import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VCatalogRequestDelete8  extends VRACommon8{

	boolean isWait = false;
	
	
	public boolean isWait() {
		return isWait;
	}

	public void setWait(boolean isWait) {
		this.isWait = isWait;
	}
	
	int tryCnt = 0;
	public VCatalogRequestDelete8(VRAConnection conn) {
		super(conn);
		tryCnt = 0;
	}
/*
 * https://api.mgmt.cloud.vmware.com/deployment/api/deployments/28e0e409-75f3-4222-a18e-5634e256ccf4/requests
 * POST {"actionId":"Deployment.Delete","targetId":"28e0e409-75f3-4222-a18e-5634e256ccf4"}
 * response : {"id":"4f278e26-3feb-4235-b6fd-d0512874203b","name":"Delete","deploymentId":"28e0e409-75f3-4222-a18e-5634e256ccf4","createdAt":"2019-08-23T08:14:08.530Z","updatedAt":"2019-08-23T08:14:08.530Z","requestedBy":"syk@goodmit.co.kr","requester":"syk@goodmit.co.kr","blueprintId":"inline-blueprint","completedTasks":0,"totalTasks":0,"status":"PENDING","inputs":{}}
 */

	protected boolean isExist(String requestId ) {
		try {
		  super.client.getJSON("/deployment/api/deployments/" + requestId + "?expandProject=true&expandResources=true&expandLastRequest=true");
		  return true;
		}
		catch(Exception e) {
			return false;
		}
	}
	@Override
	public void run(Map param, Map result) throws Exception {
		JSONObject json = new JSONObject();
		json.put("actionId", "Deployment.Delete").put("targetId",  (String)param.get("VRA_REQUEST_ID"));
		///deployment/api/deployments/6db56100-3d7f-4052-8d4c-56303d6606fa/requests
		try {
			JSONObject response = (JSONObject) super.client.post("/deployment/api/deployments/" + param.get("VRA_REQUEST_ID") +"/requests" , json.toString());
			System.out.println(response);
			if(isWait) {
				while(true) {
					if(tryCnt>20) {
						throw new Exception("삭제하는데 시간이 초과하였습니다. 수동으로 삭제하시고 재배포 하세요.");
					}
					Thread.sleep(5000);
					if(!isExist((String)param.get("VRA_REQUEST_ID"))) {
						break;
					}
					tryCnt++;
				}
			}
		}catch(Exception e) {
			if(e.getMessage().indexOf("404")<0) {
				throw e;
			}
		}
	}
}
