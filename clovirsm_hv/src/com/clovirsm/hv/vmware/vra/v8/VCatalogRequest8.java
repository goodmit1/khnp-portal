package com.clovirsm.hv.vmware.vra.v8;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VCatalogRequest8 extends VRACommon8{

	public VCatalogRequest8(VRAConnection conn) {
		super(conn);
		 
	}
/*
 * https://api.mgmt.cloud.vmware.com/blueprint/api/blueprint-requests?apiVersion=2019-01-15
 * POST
 * {"deploymentId":null,"deploymentName":"test2","description":"test","plan":false,"blueprintId":"dd7907dd-a0ca-4631-a3ec-34789fcdd566","content":null,"inputs":{"count":1,"ssh_key":"11111111111111111","flavor":"medium","image":"centos7new","cloud":"vmware","bitnami_bundle_url":"https://s3.amazonaws.com/bitnami-autobuild-ondemand/temporary/s3temp-bitnami-20190814095748-86377/provisioner-jenkins-2.164.2-5-bundle-vmware.tar.gz?AWSAccessKeyId=AKIAIZ44VOFCHNG7C2NQ&Expires=1566374268&Signature=TyK44%2BTdnkYXfcxDKC4kf7S1%2BnI=","password":"ssss"},"simulate":false}
 * ﻿response : {"type":"blueprint-request","id":"d54abf9b-776e-4106-8b26-5c50a7cf577d","createdAt":"2019-08-23T08:05:05.225Z","createdBy":"syk@goodmit.co.kr","updatedAt":"2019-08-23T08:05:05.225Z","updatedBy":"syk@goodmit.co.kr","orgId":"a2d81171-bdda-444e-8269-dcf6f0d5c3f9","projectId":"7709713a-9d5c-48ef-9599-33d471205c15","projectName":"clovircm","selfLink":"/blueprint/api/blueprint-requests/d54abf9b-776e-4106-8b26-5c50a7cf577d","deploymentId":"28e0e409-75f3-4222-a18e-5634e256ccf4","requestTrackerId":"d54abf9b-776e-4106-8b26-5c50a7cf577d","deploymentName":"test2","reason":"Create","description":"test","plan":false,"destroy":false,"ignoreDeleteFailures":false,"simulate":false,"blueprintId":"dd7907dd-a0ca-4631-a3ec-34789fcdd566","inputs":{"count":1,"ssh_key":"11111111111111111","flavor":"medium","image":"centos7new","cloud":"vmware","bitnami_bundle_url":"https://s3.amazonaws.com/bitnami-autobuild-ondemand/temporary/s3temp-bitnami-20190814095748-86377/provisioner-jenkins-2.164.2-5-bundle-vmware.tar.gz?AWSAccessKeyId=AKIAIZ44VOFCHNG7C2NQ&Expires=1566374268&Signature=TyK44%2BTdnkYXfcxDKC4kf7S1%2BnI=","password":"((secret:v1:AAFKJ+Kj26AnZ93tdhXwEiZYQAXUHS4ySnmQYRNomiMvThRD))"},"status":"STARTED","flowId":"d54abf9b-776e-4106-8b26-5c50a7cf577d","flowExecutionId":"a3055e25-ea6b-46fe-a17d-a722b12b3488"}
 * 
 * deploymentId
﻿ */

	@Override
	public void run(Map param, Map result) throws Exception {
		JSONObject json = new JSONObject();
		JSONObject dataJSON= new JSONObject((String)param.get("DATA_JSON"));
		if(param.get("FIXED_JSON") != null && !"".equals(param.get("FIXED_JSON"))) {
			JSONObject fixed= new JSONObject((String)param.get("FIXED_JSON"));
			CommonUtil.putAll(dataJSON, fixed);
		}
		json.put("deploymentName", param.get("REQ_TITLE")).put("inputs",dataJSON); //.put("reason", param.get("CMT"))
		if(param.get("VRA_REQUEST_ID") != null) {
			VCatalogRequestDelete8 vraDelete = new VCatalogRequestDelete8(this.conn);
			vraDelete.setWait(true);
			try {
					vraDelete.run(param, result); //삭제 후 신규 배포
			}
			catch(Exception ignore) {
				ignore.printStackTrace();
			}
			//json.put("deploymentId", param.get("VRA_REQUEST_ID"));
		}
		if(param.get("VER") != null) {
			json.put("version", param.get("VER"));
		}
		JSONObject formInfo = new JSONObject((String)param.get("FORM_INFO"));
		json.put("projectId", formInfo.getString("projectId"));
		Object o = super.client.post("/catalog/api/items/"+ param.get("CATALOG_ID") + "/request", json.toString());
		String deploymentId = null;
		if(o instanceof JSONArray) {
			deploymentId = ((JSONArray)o).getJSONObject(0).getString("deploymentId");
		}
		else   {
			deploymentId = ((JSONObject)o).getString("deploymentId");
		}
		param.put("VRA_REQUEST_ID", deploymentId);
	}
}
