package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VListStorageProfile  extends VRACommon8{

	public VListStorageProfile(VRAConnection conn) {
		super(conn);
		 
	}

	@Override
	public void run(Map param, Map result) throws Exception {
		JSONArray list = (JSONArray)super.client.get("/provisioning/mgmt/instance-names?view=list");
		List nameList = new ArrayList();
		for(int i=0; i < list.length(); i++) {
			nameList.add(list.getJSONObject(i).getString("name"));
		}
		result.put("LIST", nameList);
		
	}

}
