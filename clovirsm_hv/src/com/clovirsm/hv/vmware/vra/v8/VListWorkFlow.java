package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
 
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VListWorkFlow extends VRACommon8{
	Map fieldMap;
	public VListWorkFlow(VRAConnection conn) {
		super(conn);
		fieldMap = VRA8Util.getFieldNameMap("O");
	}

	protected void getIcon(JSONObject json, Map m) throws  Exception {
		if(json.has("iconId")) {
			client.setMimeType("image/png");
			 
			m.put("ICON",CommonUtil.getDataUrl((byte[])client.get("/icon/api/icons/" + json.getString("iconId")), "png"));
			client.setMimeType("application/json");
		}
		else {
			
			//m.put("ICON","/res/img/deployment-default.svg");
		}
	}
	public List getStep(String catalogId, JSONObject formInfo) throws Exception {
		JSONObject json = new JSONObject( client.getJSON("/catalog/api/items/" + catalogId).toString());
		System.out.println(json);
		String projectId = json.getJSONArray("projectIds").getString(0);
		
		//{"type":"object","properties":{"deploymentName":{"type":"string","description":"Deployment Name","maxLength":"80"},"description":{"type":"string","description":"Description","maxLength":"256"},"project":{"type":"string","description":"Project","enum":[],"default":"6386337f-cd32-4800-a34a-7ebead30106a"},"dc":{"type":"string","title":"dc"},"vm_nm":{"type":"string","title":"vm_nm"}},"required":["deploymentName","project"]}
		JSONObject model = new JSONObject(  ((org.json.JSONObject)client.post("/form-service/api/forms/renderer/model?sourceType=com.vmw.vro.workflow&sourceId=" + catalogId + "&formType=requestForm", json.getJSONObject("schema").toString())).toString());
		System.out.println(model);
		JSONArray pages = model.getJSONObject("model").getJSONObject("layout").getJSONArray("pages");
		List step = new ArrayList();
		String[] skipId = new String[] {"deploymentName", "project", "description"};
		Map defaultMap = new HashMap();
		//[{"type":"string","oneOf":[{"title":"VMKLOUD","const":"vmkloud"}],"default":"vmkloud","title":"Region","name":"env","fieldName":"constraints"},{"type":"string","encrypted":true,"default":"password","title":"Login Password","name":"password","fieldName":"cloudConfig"},{"type":"string","oneOf":[{"title":"Public","const":"public"},{"title":"Static","const":"static"},{"title":"Stage","const":"stage"},{"title":"Department","const":"routed"},{"title":"Development","const":"outbound"}],"default":"stage","title":"Purpose","name":"net","fieldName":"networkType"}]
		org.codehaus.jettison.json.JSONObject inputs = new org.codehaus.jettison.json.JSONObject();
		for(int i=0; i < pages.length(); i++) {
			JSONObject page = pages.getJSONObject(i);
			String title = page.getString("title");
			JSONArray fieldList = new JSONArray();
			JSONArray sections = page.getJSONArray("sections");
			for(int j=0; j < sections.length(); j++) {
				JSONArray  fields = sections.getJSONObject(j).getJSONArray("fields");
				for(int k=0; k < fields.length(); k++) {
					JSONObject field = fields.getJSONObject(k);
					String id = field.getString("id");
					if( CommonUtil.indexOf( skipId, field.getString("id"))>=0 ) {
						continue;
					}
					Object visibleO = field.getJSONObject("state").get("visible");
					if(visibleO instanceof Boolean) {
						if(!field.getJSONObject("state").getBoolean("visible")) {
							defaultMap.put(id, model.getJSONObject("model").getJSONObject("schema").getJSONObject(id).getString("default"));
							inputs.put(id, new JSONObject().put("name", id).put("default", model.getJSONObject("model").getJSONObject("schema").getJSONObject(id).getString("default")));
							continue;
						}
					}
					 
					
					JSONObject stepField = getFieldInfo(id, projectId, model.getJSONObject("model").getJSONObject("schema").getJSONObject(id), defaultMap);
					if(visibleO instanceof JSONArray) {
						stepField.put("visible", visibleO);
					}
					if(field.has("size")) {
						stepField.put("maxItems", field.get("size"));
					}
					inputs.put(id, stepField);
					if(!id.startsWith("_")) {
						fieldList.put( stepField);
					}
				}
			}
			Map m = new HashMap();
			m.put("STEP_NM", title);
			m.put("TAGS", "");
			m.put("INPUTS",fieldList);
			 
			
			step.add(  m);
		}
		formInfo.put("inputs", inputs);
		System.out.println(step);
		return step;
	}
	
	private void putScript(Map defaultMap, String prjId, JSONObject json, JSONObject result) throws Exception {
		if("scriptAction".equals(json.getString("type"))){
			JSONObject api = new JSONObject();
			api.put("url","/form-service/api/forms/renderer/external-value?projectId=" + prjId);
			JSONArray params1 = new JSONArray();
			JSONArray dependOn = new JSONArray();
			JSONArray parameters = json.getJSONArray("parameters");
			for(int i=0; i  < parameters.length(); i++) {
				JSONObject param = parameters.getJSONObject(i);
				Iterator it = param.keys();
				while(it.hasNext()) {
					String key = (String)it.next();
					if(!key.equals("$type")) {
						JSONObject param1 = new JSONObject();
						param1.put("name", key);
						Object d = defaultMap.get(param.get(key));
						if(d == null) {
							param1.put("value", "${" + param.get(key) + "}");
							dependOn.put(param.get(key));
						}
						else {
							param1.put("value", d);
						}
						param1.put("useResultFromRequestId",-1);
						params1.put(param1);
					
						
					}
				}
			}
			JSONObject param = new JSONObject();
			param.put("uri", json.getString("id"));
			param.put("dataSource", "scriptAction");		
			param.put("parameters", params1);		 
			param.put("requestId", 1);
			api.put("param", param);
			api.put("dependOn",dependOn);
			result.put("api", api);
		}
	}
	private JSONObject getFieldInfo(String name, String prjId, JSONObject schema, Map defaultMap) throws  Exception {
		
		JSONObject result = new JSONObject();
		if(schema.has("placeholder")) {
			result.put("title", schema.getString("placeholder"));
		}
		else {
			result.put("title", schema.getString("label"));
		}
		
		result.put("name", name);
		
		if(schema.getJSONObject("type").has("isMultiple") && schema.getJSONObject("type").getBoolean("isMultiple")) {
			result.put("type", "array");
		}
		else {
			result.put("type", schema.getJSONObject("type").getString("dataType"));
		}
		if(schema.getJSONObject("type").getString("dataType").equals("reference")) {
			result.put("fieldName", schema.getJSONObject("type").getString("referenceType"));
		}
		String field = (String)fieldMap.get(name);
		if(field != null) {
			result.put("fieldName", field);
		}
		if(schema.has("default")) {
			Object def = schema.get("default");
			if(def instanceof JSONObject) {
				putScript(  defaultMap,   prjId,   (JSONObject)def,   result);
			}
			else {
				result.put("default", schema.getString("default"));
			}
		}
		if(schema.has("valueList")) {
			Object valueListO = schema.get("valueList");
			if(valueListO instanceof JSONObject) {
				JSONObject valueList = schema.getJSONObject("valueList");
				
				putScript(  defaultMap,   prjId,   valueList,   result);
					 
				 
			}
			else if(valueListO instanceof JSONArray) {
				JSONArray valueList = schema.getJSONArray("valueList");
				JSONArray oneOf = new JSONArray();
				for(int i=0; i < valueList.length(); i++) {
					Object value1 = valueList.get(i);
					if(value1 instanceof JSONObject) {
						oneOf.put((new JSONObject()).put("const", ((JSONObject)value1).get("value")).put("title", ((JSONObject)value1).get("label")));
					}
					else {
						oneOf.put((new JSONObject()).put("const", value1).put("title",value1));
					}
				}
				result.put("oneOf", oneOf);
				
			}
		}
		if(schema.has("signpost")) {
			String signpost = schema.getString("signpost");
			if(!"".equals(signpost )) {
				try {
					putAll(result, new JSONObject(signpost));
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	public static void putAll(JSONObject target, JSONObject src) throws JSONException {
		Iterator it = src.keys();
		while(it.hasNext()) {
			String key = (String)it.next();
			target.put(key, src.get(key));
		}
		
	}
	@Override
	public void run(Map param, Map result) throws Exception {
		JSONObject root =   new JSONObject(super.client.getJSON("/catalog/api/items/?expandProjects=true&search=&page=0&size=200&sort=name,asc&types=com.vmw.vro.workflow").toString());
		JSONArray content = root.getJSONArray("content");
		List list = new ArrayList();
		for(int i=0; i < content.length(); i++) {
			JSONObject content1 = content.getJSONObject(i);
			System.out.println(content1.getString("name") + "======================");
			String projectId = content1.getJSONArray("projects").getJSONObject(0).getString("id");
			String id = content1.getString("id");
			Map m = new HashMap();
			getIcon(content1,m);	
			m.put("CATALOG_NM", content1.getString("name"));
			m.put("CATALOG_ID", id);
			if(content1.has("description")) {
				m.put("CATALOG_CMT", content1.getString("description"));
			}
			JSONObject formInfo = new JSONObject();
			m.put("STEP_INFO", this.getStep(id, formInfo));
			m.put("KUBUN","O");
			
			formInfo.put("projectId",projectId);
			m.put("FORM_INFO", formInfo.toString());
			list.add(m);
		}
		result.put("LIST", list);
	}

}
