package com.clovirsm.hv.vmware.vra.v8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VListFlavor  extends VRACommon8{

	public VListFlavor(VRAConnection conn) {
		super(conn);
		 
	}

	@Override
	public void run(Map param, Map result) throws Exception {
		JSONArray list = (JSONArray)super.client.get("/provisioning/mgmt/instance-names?view=list");
		List nameList = new ArrayList();
		for(int i=0; i < list.length(); i++) {
			Map m = new HashMap();
			m.put("SPEC_NM", list.getJSONObject(i).getString("name"));
			JSONObject instanceTypeMapping = list.getJSONObject(i).getJSONObject("instanceTypeMapping");
			JSONArray names = instanceTypeMapping.names();
			for(int j=0; j < names.length(); j++) {
				JSONObject spec = instanceTypeMapping.getJSONObject(names.getString(j));
				if(spec.has("cpuCount")) {
					m.put("CPU_CNT", spec.get("cpuCount"));
					m.put("RAM_SIZE", spec.getInt("memoryMb")/1024);
					m.put("DISK_SIZE", spec.getInt("diskSizeMb")/1024);
					m.put("DISK_UNIT", "G");
					break;
				}
			}
			nameList.add(m);
		}
		result.put("LIST", nameList);
		
	}

}
