package com.clovirsm.hv.vmware.vra.v8;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.jettison.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.YamlUtil;
import com.clovirsm.hv.vmware.vra.VRACommon;
import com.clovirsm.hv.vmware.vra.VRAConnection;

public class VListCatalog8  extends VRACommon8{

	public VListCatalog8(VRAConnection conn) {
		super(conn);
		 
	}

	 
	@Override
	public void run(Map param, Map result) throws Exception {
		 VListBluePrint bp = new VListBluePrint(super.conn);
		 bp.run(param, result);
		 List list = (List) result.get("LIST");
		 Map result1 = new HashMap();
		 VListWorkFlow wf = new VListWorkFlow(super.conn);
		 wf.run(param, result1);
		 List listwf = (List) result1.get("LIST");
		 if(listwf != null) {
			 list.addAll(listwf);
		 }
	}
	
	
}
