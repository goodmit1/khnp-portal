package com.clovirsm.hv.vmware.vra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
 
public class VGetRequestState extends VRACommon{

	public VGetRequestState(VRAConnection conn) {
		super(conn);
	}

	protected void getVMInfo( Map result,String requestId) throws Exception
	{
		
		/*
		 
      "@type": "CatalogResourceView",
      "resourceId": "b45b3a41-f071-447b-b6cd-2c9491501a07",
      "iconId": "Infrastructure.CatalogItem.Machine.Virtual.vSphere",
      "name": "vRA005",
      "description": null,
      "status": "On",
      "catalogItemId": null,
      "catalogItemLabel": null,
      "requestId": "6c4ad903-4f5a-45fa-ad56-fcd3d129a7a5",
      "requestState": "SUCCESSFUL",
      "resourceType": "Infrastructure.Virtual",
      "owners": [
        "admin admin"
      ],
      "businessGroupId": "550fc363-0259-4a2a-9cff-516e517da9f5",
      "tenantId": "vsphere.local",
      "dateCreated": "2018-11-07T05:16:29.207Z",
      "lastUpdated": "2018-11-07T05:20:06.003Z",
      "lease": {
        "start": "2018-11-07T05:12:25.737Z",
        "end": null
      },
      "costs": null,
      "costToDate": null,
      "totalCost": null,
      "parentResourceId": "d07dfd26-eab2-4a33-b6fa-aa508e7ff3ba",
      "hasChildren": false,
      "data": {
        "ChangeLease": true,
        "ChangeOwner": true,
        "Component": "Win2012R2NoTools_1~Win2012R2-NoTools_v2",
        "ConnectViaNativeVmrc": true,
        "ConnectViaRdp": true,
        "ConnectViaVmrc": true,
        "CreateSnapshot": true,
        "DISK_VOLUMES": [
          {
            "componentTypeId": "com.vmware.csp.component.iaas.proxy.provider",
            "componentId": null,
            "classId": "dynamicops.api.model.DiskInputModel",
            "typeFilter": null,
            "data": {
              "DISK_CAPACITY": 40,
              "DISK_INPUT_ID": "DISK_INPUT_ID1",
              "DISK_LABEL": "Hard disk 1"
            }
          }
        ],
        "Destroy": true,
        "EXTERNAL_REFERENCE_ID": "vm-671",
        "Expire": true,
        "IS_COMPONENT_MACHINE": false,
        "InstallTools": true,
        "MachineBlueprintName": "test",
        "MachineCPU": 1,
        "MachineDailyCost": 0,
        "MachineDestructionDate": null,
        "MachineExpirationDate": null,
        "MachineGroupName": "Configuration Administrators",
        "MachineGuestOperatingSystem": null,
        "MachineInterfaceDisplayName": "vSphere (vCenter)",
        "MachineInterfaceType": "vSphere",
        "MachineMemory": 4096,
        "MachineName": "vRA005",
        "MachineReservationName": "goodmit",
        "MachineStorage": 40,
        "MachineType": "Virtual",
        "NETWORK_LIST": [
          {
            "componentTypeId": "com.vmware.csp.component.iaas.proxy.provider",
            "componentId": null,
            "classId": "dynamicops.api.model.NetworkViewModel",
            "typeFilter": null,
            "data": {
              "NETWORK_MAC_ADDRESS": "00:50:56:9b:09:c8",
              "NETWORK_NAME": "Uplink01"
            }
          }
        ],
        "PowerOff": true,
        "Reboot": true,
        "Reconfigure": true,
        "Relocate": true,
        "Reprovision": true,
        "Reset": true,
        "SNAPSHOT_LIST": [],
        "Shutdown": true,
        "Suspend": true,
        "Unregister": true,
        "VirtualMachine.Admin.UUID": "501bbbc9-825d-abfb-9a0e-5f62c1e2b33e",
        "endpointExternalReferenceId": "b5023e4c-8390-4228-9d89-faa795866250",
        "ip_address": "",
        "machineId": "1243c5cd-9a8f-4dc3-95c2-f1149e3418ea"
      }
		 */
		JSONObject resource = super.client.getJSON("/catalog-service/api/consumer/requests/" + requestId + "/resourceViews");
		JSONArray arr = resource.getJSONArray("content");
		List list = new ArrayList();
		for(int i=0; i < arr.length(); i++)
		{
			JSONObject resource1 = arr.getJSONObject(i);
			JSONObject data = resource1.getJSONObject("data");
			if(data.length()>0 && data.has("EXTERNAL_REFERENCE_ID"))
			{
				Map m = new HashMap();
				m.put("VM_HV_ID", data.getString("EXTERNAL_REFERENCE_ID"));
				m.put("CPU_CNT", data.getInt("MachineCPU"));
				m.put("RAM_SIZE", data.getInt("MachineMemory"));
				m.put("VM_NM", data.getString("MachineName"));
				m.put("DISK_SIZE", data.getInt("MachineStorage"));
				m.put("PRIVATE_IP", data.getString("ip_address"));
				m.put("RESOURCE_ID", resource1.getString("resourceId"));
				if("vSphere".equals(data.getString("MachineInterfaceType")))
				{
					m.put("HV_CD", "V");
				}
				String[] ipNames = getDCIp( data.getString("MachineReservationName"));
				if(ipNames != null)
				{
					m.put("DC_IP",ipNames[0]);
					m.put("REAL_DC_NM",ipNames[1]);
				}
				
				list.add(m);
			}
		}
		result.put("VM_LIST", list);
		
	 
		
	}
	protected String[] getDCIp(String reservationName) throws Exception
	{
		
		JSONObject reservation = this.client.getJSON("/reservation-service/api/reservations?consistent=false&page=1&limit=20&$filter=name%20eq%20'" + reservationName + "'");
		
		JSONObject content = reservation.getJSONArray("content").getJSONObject(0);
		JSONArray entries = content.getJSONObject("extensionData").getJSONArray("entries");
		for(int i=0; i < entries.length(); i++)
		{
			JSONObject entry = entries.getJSONObject(i);
			if("computeResource".equals(entry.getString("key")))
			{
				JSONObject value = entry.getJSONObject("value");
				String clusterId = value.getString("id");
				JSONObject computeResources = this.client.getJSON("/iaas-proxy-provider/api/compute-resources");
				JSONArray contentList = computeResources.getJSONArray("content");
				for(int j=0; j < contentList.length(); j++)
				{
					JSONObject computeResource = contentList.getJSONObject(j);
					if(clusterId.equals(computeResource.getString("id")))
					{
						String hostUniqueId = computeResource.getString("hostUniqueId");
						int pos = hostUniqueId.indexOf("/");
						int pos1 = hostUniqueId.indexOf("/", pos+1);
						
						String ip = hostUniqueId.substring(0,  pos);
						String dcNm = hostUniqueId.substring(pos+1,  pos1);
						return new String[]{ip, dcNm};
					}
				}
				
						
			}
		}
		return null;
	}
	@Override
	public void run(Map param, Map result ) throws Exception {
		String requestId = (String)param.get("VRA_REQUEST_ID");
		JSONObject requestState = super.client.getJSON("/catalog-service/api/consumer/requests/" + requestId);
		if(requestState.getBoolean("final"))
		{
			if(requestState.getBoolean("successful"))
			{
				result.put("TASK_STATUS_CD" , "S");
				getVMInfo(  result,  requestId);
			}
			else
			{
				result.put("FAIL_MSG", requestState.getJSONObject("requestCompletion").getString("completionDetails"));
				result.put("TASK_STATUS_CD" , "F");
			}
		}
		
		 
				
				
				
 
		
	}
	
	public static void main(String[] args)
	{
		VRAConnection conn = new VRAConnection();
		Map prop = new HashMap();
		prop.put("tenant", "vsphere.local");
		try {
			conn.connect("https://172.16.33.210", "configurationadmin", "VMware1!", prop);
			Map param = new HashMap();
			//param.put("VRA_REQUEST_ID", "3741003c-8aab-401d-b27d-7d99c64ce3ed");
			param.put("VRA_REQUEST_ID","76b8c4e5-ff50-422d-9d8b-fba4995adcef");
			VGetRequestState list = new VGetRequestState(conn);
			list.run(param, param);
			System.out.println(param);
			//System.out.println(conn.getRestClient().getJSON(  "/forms-service/api/forms/aa9ecece-1e7e-4c81-92d9-a9565cdc0046" ));
			//System.out.println(conn.getRestClient().getJSON(  "/catalog-service/api/consumer/entitledCatalogItemViews"));
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
	}

}
