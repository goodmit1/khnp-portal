package com.clovirsm.hv.vmware.vra;

import java.util.Map;

import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.RestClient;

public abstract  class VRACommon {
	protected RestClient client;
	protected VRAConnection conn;
	public VRACommon(VRAConnection conn)
	{
		this.conn = conn;
		if(conn != null) {
			this.client = conn.getRestClient();
		}
	}
	public abstract void run(Map param, Map result) throws Exception ;
	public void run(Map param, Map result, IAfterProcess after) throws Exception 
	{
		this.run(param, result);
		if(after != null)
		{
			after.startTask((String)param.get("CATALOGREQ_ID"));
		}
	}
	 
}
