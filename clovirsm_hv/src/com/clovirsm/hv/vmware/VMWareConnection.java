package com.clovirsm.hv.vmware;

import java.util.Map;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IConnection;
import com.vmware.connection.ConnectionMng;

public class VMWareConnection extends ConnectionMng implements IConnection{

	String url;
	 
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		//super.setHostConnection(true);
		super.connect(url, userId, pwd);
		this.url = url;
		
	}

	  

	 

	@Override
	public String getURL() {
		 
		return this.url;
	}

	 

}
