package com.clovirsm.hv.vmware.vrops;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonUtil;

public class GetAllDCHealth extends VROpsCommon{

	public GetAllDCHealth(VROpsConnection conn) {
		super(conn);
		 
	}

	@Override
	public Object run(Map param ) throws Exception {
		JSONObject resources =  (JSONObject) client.get("/resources?resourceKind=datacenter");
		JSONArray resourceArr =CommonUtil.getJSONArray(resources.getJSONObject("ops:resources"), "ops:resource");
		JSONArray result = new JSONArray();
		for(int i=0; i < resourceArr.length(); i++) {
			JSONObject resource = resourceArr.getJSONObject(i);
			String name = resource.getJSONObject("ops:resourceKey").getString("ops:name") ;
			String id = resource.getString("identifier");
			JSONObject m = new JSONObject();
			m.put("DC_NM", name);
			 
			m.put("HEALTH", resource.getString("ops:resourceHealth"));
			JSONArray badges = resource.getJSONObject("ops:badges").getJSONArray("ops:badge");			 
			m.put("BADGES", badges);
			result.put(m);
			
		}
		return result;
		
	}

}
