package com.clovirsm.hv.vmware.vrops;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.YamlUtil;

public class GetUnderOverSizeVM extends VROpsCommon{

	public GetUnderOverSizeVM(VROpsConnection conn) {
		super(conn);
		 
	}

	@Override
	public Object run(Map param) throws Exception {
		int day = NumberUtil.getInt( param.get("DAY"));
		Map keyMap = new HashMap();
		keyMap.put("summary|undersized|vcpus", "UNDER_CPU");
		keyMap.put("summary|oversized|vcpus", "OVER_CPU");
		keyMap.put("summary|oversized|memory", "OVER_MEM");
		keyMap.put("summary|undersized|memory", "UNDER_MEM");
		JSONObject resources =  (JSONObject) client.get("/resources?resourceKind=virtualmachine");
		List result = new ArrayList();
		JSONArray resourceArr = resources.getJSONObject("ops:resources").getJSONArray("ops:resource");
		for(int i=0; i < resourceArr.length(); i++) {
			JSONObject resource = resourceArr.getJSONObject(i);
			JSONObject json = new JSONObject();
			Calendar begin = Calendar.getInstance();
			begin.add(Calendar.DATE, -1*(day-1));
			json.put("statKey", (new JSONArray(keyMap.keySet()))).put("rollUpType","MIN").put("intervalType","DAYS").put("intervalQuantifier", day).put("begin", begin.getTimeInMillis()).put("resourceId", (new JSONArray()).put(resource.getString("identifier")));
			JSONObject res = (JSONObject) client.post("/resources/stats/query",json.toString());
			//System.out.println(res);
			JSONObject stat = YamlUtil.getJSONObjectByPath(false, res, "ops:stats-of-resources","ops:stats-of-resource","ops:stat-list" );
					
			if(stat != null) {
				JSONArray statList = stat.getJSONArray("ops:stat");
				Map mem = new HashMap();
				int total =0;
				for(int j=0; j < statList.length(); j++)
				{
					int data = statList.getJSONObject(j).getInt("ops:data");
					total += data;
					if(data>0) {
						String key = statList.getJSONObject(j).getJSONObject("ops:statKey").getString("ops:key");
						if(key.endsWith("memory")) {
							data = data / 1024 /1024 ;
						}
						mem.put(keyMap.get(key), data);
					}
					
				}
			
				if(total>0) {
					mem.put("VM_HV_ID", getVMId(resource));
					mem.put("VM_NM", resource.getJSONObject("ops:resourceKey").getString("ops:name"));
					result.add(mem);
				}
			}
			
		}
		return result;
	}

}
