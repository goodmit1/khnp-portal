package com.clovirsm.hv.vmware.vrops;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.NumberUtil;

public class GetMetricData extends VROpsCommon{

	public GetMetricData(VROpsConnection conn) {
		super(conn);
		 
	}

	@Override
	public Object run(Map param) throws Exception {
		String resId = (String) param.get("resId");
		String key= (String) param.get("key");
		String intervalType = (String) param.get("intervalType");
		long begin= (long) param.get("begin");
		long end=  (long) param.get("end");
		JSONObject json = new JSONObject();
		json.put("statKey", key).put("begin", begin).put("end",end);
		JSONObject res = ((JSONObject) client.get("/resources/" + resId + "/stats?rollUpType=AVG&intervalType=" + intervalType + "&statKey=" + key + "&begin=" + begin + "&end=" + end )).getJSONObject("ops:stats-of-resources");
		System.out.println(res);
		Map result = new HashMap();
		if(res.has("ops:stats-of-resource")) {
			JSONArray statList = CommonUtil.getJSONArray(res.getJSONObject("ops:stats-of-resource").getJSONObject("ops:stat-list"), "ops:stat");
			List list = new ArrayList();
			for(int i=0; i < statList.length(); i++) {
				JSONObject stat =  statList.getJSONObject(i);
				 
				String[] ts = stat.getString("ops:timestamps").split(" ");
				long[] tsl = new long[ts.length];
				int idx=0;
				for(String t:ts) {
					tsl[idx++] = NumberUtil.getLong(t);
				}
					 
				 
				
				String[] data = stat.getString("ops:data").split(" ");
				idx=0;
				Object[][] dataf = new Object[data.length][2];
				for(String t:data) {
					dataf[idx][0] = tsl[idx];
					dataf[idx][1] = NumberUtil.getFloat(t);
					idx++;
				}
				 
				Map data1 = new HashMap();
				data1.put("name", stat.getJSONObject("ops:statKey").getString("ops:key"));
				data1.put("data", dataf);
				list.add(data1);
				 
			}
			result.put("y", list);
			
		}	
		return result;
	}

}
