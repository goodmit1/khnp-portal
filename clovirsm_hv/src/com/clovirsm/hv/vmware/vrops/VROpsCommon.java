package com.clovirsm.hv.vmware.vrops;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.SessionRestClient;
import com.clovirsm.hv.vmware.vrops.VROpsConnection;

public abstract  class VROpsCommon {
	protected RestClient client;
	
	public VROpsCommon(VROpsConnection conn)
	{
		if(conn != null) {
			this.client = conn.getRestClient();
		}
	}
	protected String getVMResourceId(RestClient client, String name, String vmId) throws Exception{
		JSONObject resources =  (JSONObject) client.get("/resources?name=" + name);
		JSONArray resourceArr = null;
		
		Object o = resources.getJSONObject("ops:resources").get("ops:resource");
		if(o instanceof JSONArray) {
			resourceArr = resources.getJSONObject("ops:resources").getJSONArray("ops:resource");
		}
		else {
			resourceArr = new JSONArray();
			resourceArr.put(o);
		}
		for(int i=0; i < resourceArr.length(); i++) {
			JSONObject resource = resourceArr.getJSONObject(i);
			if(vmId == null) {
				return resource.getString("identifier");
			}
			if(vmId.equals(getVMId(resource))){
				return resource.getString("identifier");
			}
		}
		return null;
	}
	public abstract Object run(Map param ) throws Exception ;
	protected String getVMId( JSONObject resource ) {
		JSONArray resourceIdentifiers = resource.getJSONObject("ops:resourceKey").getJSONObject("ops:resourceIdentifiers").getJSONArray("ops:resourceIdentifier");
		for(int i=0; i < resourceIdentifiers.length(); i++)		{
			JSONObject res = resourceIdentifiers.getJSONObject(i);
			if("VMEntityObjectID".equals(res.getJSONObject("ops:identifierType").getString("name"))){
				return res.getString("ops:value");
			}
			
		}
		return null;
	}
	
	 
}
