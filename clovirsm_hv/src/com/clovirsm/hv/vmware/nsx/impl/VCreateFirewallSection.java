package com.clovirsm.hv.vmware.nsx.impl;

import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.ParamObj;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.nsx.NSXCommon;

public class VCreateFirewallSection extends NSXCommon {

	public VCreateFirewallSection(RestClient client) {
		super(client);
		 
	}

	@Override
	public void run(Map param, Map result, IAfterProcess after) throws Exception {
		ParamObj obj = new ParamObj("section");
		
		obj.addAttr("name", (String)param.get(HypervisorAPI.PARAM_OBJECT_NM));
		Object response = super.post("/4.0/firewall/globalroot-0/config/layer3sections", obj);
		
		
	}

}
