package com.clovirsm.hv.vmware.nsx.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.nsx.NSXCommon;

public class VListTransportzone extends NSXCommon{

	public VListTransportzone(RestClient client) {
		super(client);
		 
	}

	@Override
	public void run(Map param, Map result, IAfterProcess after) throws Exception {
		 JSONObject json = super.get("/2.0/vdn/scopes");
		 System.out.println(json);
		 JSONObject vdnScopes = json.getJSONObject("vdnScopes");
		 JSONArray vdnScopeList = vdnScopes.getJSONArray("vdnScope");
		 List list = new ArrayList();
		 for(int i=0; i < vdnScopeList.length(); i++)
		 {
			 JSONObject vdnScope = vdnScopeList.getJSONObject(i);
			 Map m = new HashMap();
			 m.put("id", vdnScope.getString("objectid"));
			 m.put("name", vdnScope.getString("name"));
			 list.add(m);
		 }
		result.put(HypervisorAPI.PARAM_LIST, list);
	}


}
