package com.clovirsm.hv.vmware.nsx.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.nsx.NSXCommon;

public class VListLicense extends NSXCommon{

	public VListLicense(RestClient client) {
		super(client);
		 
	}

	@Override
	public void run(Map param, Map result, IAfterProcess after) throws Exception {
		
		
		JSONObject results = (JSONObject) client.get("/api/v1/licenses");
		//JSONObject feature_usage_info = (JSONObject)client.get("/api/v1/licenses/licenses-usage");
		JSONArray resultList = results.getJSONArray("results");
		List list = new ArrayList();
		for(int i=0; i <resultList.length(); i++)
		{
			Map m = new HashMap();
			JSONObject r = resultList.getJSONObject(i);
			m.put("LICENSE_KEY", r.getString("license_key"));
			m.put("LICENSE_TOTAL", r.getInt("quantity"));
			m.put("LICENSE_USED", r.getInt("quantity"));
			m.put("COST_UNIT", r.getString("capacity_type").toLowerCase());
			m.put("LICENSE_NAME", r.getInt("description"));
		}
		result.put(HypervisorAPI.PARAM_LIST, list);
	}
	
	public static void main(String[] args)
	{
		try {
			RestClient client = new RestClient("https://172.16.33.140","admin","VMware1!");
			
			//VListEdge edge = new VListEdge(client);
			VListLicense edge = new VListLicense(client);
			Map param = new HashMap();
			Map result = new HashMap();
			
				edge.run(param, result, null);
			} catch (Exception e) {
				 
				e.printStackTrace();
			}
	}

}
