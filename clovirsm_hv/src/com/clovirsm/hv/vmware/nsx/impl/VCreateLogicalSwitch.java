package com.clovirsm.hv.vmware.nsx.impl;

import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.ParamObj;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.nsx.NSXCommon;

public class VCreateLogicalSwitch extends NSXCommon {

	public VCreateLogicalSwitch(RestClient client) {
		super(client);
		 
	}

	@Override
	public void run(Map param, Map result, IAfterProcess after) throws Exception {
		ParamObj obj = getParamObj((String)param.get(HypervisorAPI.PARAM_OBJECT_NM), (String)param.get(HypervisorAPI.PARAM_OBJECT_NM), (String)param.get(HypervisorAPI.PARAM_OBJECT_NM));
		String scopeId = (String)param.get("TRANSPORTZONE_ID");
		Object response = super.post("/2.0/vdn/scopes/" + scopeId + "/virtualwires", obj);
		result.put("id", response);
		
	}
	/*
	 <virtualWireCreateSpec>  <name>Web-Tier-01</name>  <description>Web tier network</description>  <tenantId>virtual wire tenant</tenantId>  <controlPlaneMode>UNICAST_MODE</controlPlaneMode>  <guestVlanAllowed>false</guestVlanAllowed> </virtualWireCreateSpec>
	 */
	private ParamObj getParamObj( String name, String description, String tenantId)
	{
		ParamObj virtualWireCreateSpec = new ParamObj("virtualWireCreateSpec");
		virtualWireCreateSpec.addChild(new ParamObj("name", name));
		virtualWireCreateSpec.addChild(new ParamObj("description", description));
		virtualWireCreateSpec.addChild(new ParamObj("tenantId", tenantId));
		 
		return virtualWireCreateSpec;
	}

}
