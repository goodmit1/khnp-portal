package com.clovirsm.hv.vmware.nsx;

import java.util.HashMap;
import java.util.Map;

import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IAPI;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.VMWareConnection;
import com.clovirsm.hv.vmware.log.VLogConnection;
import com.clovirsm.hv.vmware.nsx.impl.VCreateLogicalSwitch;
import com.clovirsm.hv.vmware.vrops.VROpsConnection;
import com.vmware.connection.ConnectionMng;

public class NSXAPI extends CommonAPI {

	public static String getHVType()
	{
		return "V.NSX";
	}
	protected  String getConnectionName() {
		return NSXConnection.class.getName();
	}
	 
	NSXConnection conn;
	 
	public Map createLogicalSwitch(Map param) throws Exception
	{
		RestClient client = conn.getRestClient();
		VCreateLogicalSwitch action = new VCreateLogicalSwitch(client);
		Map result = new HashMap();
		action.run(param, result, null);
		return result;
		
	}

	 
	 
}
