package com.clovirsm.hv.vmware.vni;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.AnonymousConnection;
import com.clovirsm.hv.CommonAPI;
import com.clovirsm.hv.ConnectionPool;
import com.clovirsm.hv.IAPI;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.IConnection;
import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.CommonAction;
import com.clovirsm.hv.vmware.VMWareConnection;
import com.clovirsm.hv.vmware.vra.VCatalogRequest;
import com.vmware.connection.ConnectionMng;

public class VNIAPI extends CommonAPI{
	
	
	
	protected String getConnectionName() {
		return VNIConnection.class.getName();
	}
	 
	public static String getHVType() {
	 
		return "V.VNI";
	}
	 
	protected Map doProcess(IAfterProcess after, Map param,  Class... classList) throws Exception
	{	
		VNIConnection connectionMgr = null;
		try
		{
			connectionMgr = (VNIConnection) connect(param);
			 
			Map result = new HashMap();
			for(Class cls : classList)
			{
				VNICommon action = (VNICommon)cls.getConstructor(VNIConnection.class).newInstance(connectionMgr);
			 	action.run(param, result, after);
			 	
			}
			return result;
		}
		finally
		{
			disconnect(connectionMgr);
		}
	}
	public Map getFlow(Map param) throws Exception{
		 
			return this.doProcess(null, param, VNIGetFlows.class);
			
		 
	}
	 

	 
}
