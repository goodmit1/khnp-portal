package com.clovirsm.hv.vmware;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.vra.VRAConnection;

// Vcenter rest client
public class VCRestConnection extends VRAConnection{
	@Override
	public void connect(String url, String userId, String pwd, Map prop) throws Exception {
		int pos = url.lastIndexOf("/");
		url = url.substring(0,  pos) + "/rest";
		client = new RestClient(url, userId,pwd);
		JSONObject res = (JSONObject) client.post("/com/vmware/cis/session", "{}");
		client.setAuthStr(null);
		client.setMimeType("application/json");
		Date today = new Date();
		today.setMinutes(today.getMinutes()+30);
		expire = today;
		Map  header = new HashMap();
		header.put("vmware-api-session-id", res.getString("value"));
		client.setHeader(header);
	}
}
