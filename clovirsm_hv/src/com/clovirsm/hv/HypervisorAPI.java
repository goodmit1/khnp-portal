package com.clovirsm.hv;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface HypervisorAPI {
	public static final String PARAM_NIC_ID = "NIC_ID";
	public static final Object PARAM_VAL = "VAL";
	public static final String PARAM_CUD_CD = "CUD_CD";
	public static final String PARAM_REAL_DC_NM = "REAL_DC_NM";
	
	public static final String PARAM_URL = "CONN_URL";

	public static final String PARAM_PWD = "CONN_PWD";

	public static final String PARAM_USERID = "CONN_USERID";
	
	public static final String PARAM_GUEST_PWD = "GUEST_CONN_PWD";

	public static final String PARAM_GUEST_USERID = "GUEST_CONN_USERID";
	
	 
	public static final String PARAM_USE_VLAN_YN = "USE_VLAN_YN"; // VLAN 사용 여부
	public static final String PARAM_USE_RP_YN = "USE_RP_YN";  // 리소스 풀 사용 여부

	public static final String PARAM_OBJECT_TYPE = "SVC_CD";

	public static final String PARAM_OBJECT_NM = "SVC_NM";
	
	public static final String PARAM_HOST_NM = "HOST_NM";

	public static final String PARAM_FROM = "TMPL_PATH";

	public static final String PARAM_CPU = "CPU_CNT";

	public static final String PARAM_MEM = "RAM_SIZE";

	public static final String PARAM_DISK = "DISK_SIZE";
	public static final String PARAM_COMMENT = "COMMENT";
	
	public static final String PARAM_DATA_DISK_LIST = "DATA_DISK_LIST";

	public static final String PARAM_VM_NM = "VM_NM";

	public static final String PARAM_IMAGE_NM = "IMG_NM";

	public static final String PARAM_SNAPSHOT_NM = "SNAPSHOT_NM";

	public static final String PARAM_OBJECT_NEW_NM = "NEW_SVC_NM";
	public static final String PARAM_DATASTORE_NM = "DS_NM";
	public static final String PARAM_DATASTORE_NM_LIST = "DS_NM_LIST";
	public static final String PARAM_IP_LIST = "IP_LIST";
	public static final String PARAM_DISK_PATH = "DISK_PATH";
	
	public static final String PARAM_RUN_CD="RUN_CD";
	
	
	public static final String PARAM_PRIVATE_IP="PRIVATE_IP"; 
	public static final String PARAM_CATEGORY = "CATEGORY";
	public static final Object PARAM_LIST = "LIST";
	public static final String PARAM_CRE_TIME = "CRE_TIME";
	public static final String PARAM_TARGET = "TARGET";
	public static final String PARAM_START_TIME = "START_TIME";
	public static final String PARAM_END_TIME = "END_TIME";
	public static final String PARAM_PAGE_NO = "PAGE_NO";
	public static final String PARAM_PAGE_SIZE = "PAGE_SIZE";
	public static final Object PARAM_PERIOD = "PERIOD";
	public static final Object PARAM_CONSOLE_URL = "CONSOLE_URL";
	public static final Object PARAM_PERF_NAME =  "PERF_NAME";
	public static final String POWER_ON="powerOn";
	public static final String POWER_OFF="powerOff";
	public static final String POWER_REBOOT="reboot";
	
	public static final String OBJ_TYPE_VM="VM";
	public static final String OBJ_TYPE_DS="DS";
	public static final String OBJ_TYPE_HOST="HOST";
	public static final String OBJ_TYPE_DC="DC";
	public static final String OBJ_TYPE_CLUSTER="CLUSTER";
	public static final String OBJ_TYPE_RP="RP";
	
	public static final String RUN_CD_RUN = "R";
	public static final String RUN_CD_STOP = "S";
	
	public static final String HVTYPE_VMWARE="V";
	
	public static final String PERF_CATEGORY_CPU="cpu";
	public static final String PERF_CATEGORY_MEM="mem";
	public static final String PERF_CATEGORY_NET="net";
	public static final String PERF_CATEGORY_DISK="disk";
	public static final String PERF_NAME_USAGE="usage";
	
	public static final String PERF_PERIOD_1D = "1d"; //당일
	public static final String PERF_PERIOD_1W = "1w"; //1주일
	
	public static final String PERF_PERIOD_1M = "1m"; //1달
	public static final String PERF_PERIOD_1Y = "1y"; //1년
	public static final String PERF_PERIOD_1H = "1h"; //실시간
	public static final String PARAM_TMPL_NM_LIST = "TMPL_NM_LIST";
	public static final Object PARAM_VM_HV_ID = "VM_HV_ID";
	
	/**
	 * 디스크 삭제
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , DISK_PATH(삭제할 디스크 경로)
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public  Map deleteDisk(Map param, VMInfo info) throws Exception;
	
	 
	
	/**
	 * 스냅샷명 변경
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , SNAPSHOT_NM(예전 명칭), NEW_SVC_NM(새 명칭)
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public  Map renameSnapshot(Map param, VMInfo info) throws Exception;
	
	/**
	 * 스냅샷에서 VM 복구
	 * @param after 복구 후 추가 작업 클래스
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , SNAPSHOT_NM(스냅샷명)
	 * @param info
	 * @return
	 * @throws Exception
	 */
	public  Map revertVM(IAfterProcess after,Map param, VMInfo info) throws Exception;
	
	/**
	 * VM명 변경
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, ,  NEW_SVC_NM(새 명칭)
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public  Map renameVM(Map param, VMInfo info) throws Exception;
	
	/**
	 * VM에 디스크 마운트
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , DISK_PATH(마운트할 디스크 경로)
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public  Map mountDisk(Map param, VMInfo info) throws Exception;
	
	/**
	 * VM 생성
	 * @param beforeAfter VM생성전, 생성 후 처리에 대한 클래스
	 * @param isDCFirst 첫 VM생성 여부(처음 생성시 분산포트그룹, 폴더, 리소스풀을 생성한다)
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , VM_NM,CPU_CNT, RAM_SIZE, DISK_SIZE, TMPL_PATH(이미지 경로), NIC_ID(랜카드 아이디),DS_NM_LIST[](데이터 스토어 목록, 이중 가장 VM개수가 적은 것 순으로 대상을 찾는다.)
	 * @return DS_NM(저장된 데이터 스토어명)
	 * @throws Exception
	 */
	public  Map createVM(IBeforeAfter beforeAfter, boolean isDCFirst, Map param) throws Exception;
	
	public  Map moveVMinfoFolder(Map param,  VMInfo oldInfo  ) throws Exception;
	
	/**
	 * VM 사양 변경 ( CPU개수 값이 변하면 리소스풀의 CPU 셋팅값을 변경한다.)
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, ,  CPU_CNT, RAM_SIZE, DISK_SIZE
	 * @param oldInfo 변경전 VM정보
	 * @return
	 * @throws Exception
	 */
	public  Map reconfigVM(Map param , VMInfo oldInfo) throws Exception;
	
	/**
	 * VM 시작, 정지, reboot
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, 
	 * @param info VM정보
	 * @param op
	 * @return
	 * @throws Exception
	 */
	public  Map powerOpVM(Map param, VMInfo info, String op)
			throws Exception;
	
	/**
	 * 스냅샷제거
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , SNAPSHOT_NM(스냅샷명)
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public  Map deleteSnapshot(Map param, VMInfo info) throws Exception;
	
	/**
	 * 이미지 삭제
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , IMG_NM(이미지명)
	 * @return
	 * @throws Exception
	 */
	public  Map deleteImage(Map param) throws Exception;
	
	/**
	 * VM제거
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, 
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public  Map deleteVM(Map param, VMInfo info) throws Exception;
	
	/**
	 * 조직이 제거 될 때 자동으로 추가된 것들 제거(VMWARE는 분산포트그룹, 폴더, 리소스풀)
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD,  
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public  Map deleteOrg(Map param) throws Exception;
	
	/**
	 * 콘솔 열기
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD,  
	 * @param info VM정보
	 * @return CONSOLE_URL(콘솔 URL)
	 * @throws Exception
	 */
	public  Map openConsole(Map param, VMInfo info) throws Exception;
	
	/**
	 * 스냅샷생성
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , SNAPSHOT_NM(스냅샷명) 
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public  Map createSnapshot(IAfterProcess after,Map param, VMInfo info) throws Exception;
	
	
	/**
	 * 이미지명 변경
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , IMG_NM(이미지명), NEW_SVC_NM(새 명칭)
	 * @return
	 * @throws Exception
	 */
	public  Map renameImg(Map param ) throws Exception;
	
 
	
	/**
	 * VM에 디스크 언마운트
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , DISK_PATH(언마운트할 디스크 경로)
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public  Map unmount(Map param, VMInfo info) throws Exception;
	
	/**
	 * VM 현재 성능 지표
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , VM_NM
	 * @return  disk_totalgb, disk_usedgb, mem_total,disk_usage,cpu_usagemhz,mem_active,cpu_usage,mem_usage
	 * @throws Exception
	 */
	public Map getVMPerf( Map param) throws Exception;
	
	/**
	 * 현재 성능 지표
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , VM_NM
	 * @return  hv.properties에 정의된 (vmware.perf.obj.xxx) 지표의 결과 값
	 * @throws Exception
	 */
	public Map getPerf(String type, Map param) throws Exception;
	
	
	/**
	 * VM에 랜카드 추가
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , 
	 * @param info VM정보
	 * @return NIC_ID(랜카드 ID)
	 * @throws Exception
	 */
	public Map addNic(Map param, VMInfo info) throws Exception;
	
	/**
	 * VM에서 랜카드 제거
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , NIC_ID
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public Map deleteNic(Map param, VMInfo info) throws Exception;
	
	/**
	 * 
	 * @return 첫번째 랜카드 KEY
	 * @throws Exception
	 */
	public int getFirstNicId() throws Exception;
	
	/**
	 * 성능이력
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, ,PERIOD(기간, 1h|1d|1w|1m|1y), SVC_CD(Object 타입, VM,DS,HOST,DC,CLUSTER,RP), SVC_NM(VM이외), VM_NM(VM인 경우)
	 * @return LIST=[{CRE_TIME=..,VAL=...}]
	 * @throws Exception
	 */
	public Map listPerfHistory(  Map param) throws Exception;
	
	/**
	 * 이벤트 목록
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , SVC_CD(VM,DS,HOST,DC,CLUSTER,RP), SVC_NM(VM이외), VM_NM(VM인 경우)
	 * @return LIST=[{CRE_TIME=..,VAL=..., COMMENT=..., CATEGORY=유형, TARGET=대상}]
	 * @throws Exception
	 */
	public  Map listEvent(  Map param ) throws Exception;
	
	/**
	 * 문제 목록
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, ,  SVC_CD(VM,DS,HOST,DC,CLUSTER,RP), SVC_NM(VM이외), VM_NM(VM인 경우)
	 * @return LIST=[{CRE_TIME=..,VAL=..., COMMENT=..., CATEGORY=유형, TARGET=대상}]
	 * @throws Exception
	 */
	public Map listAlarm(  Map param ) throws Exception;
	
	/**
	 * VM기본 디스크 사이즈 업, VM생성 후 템플릿의 디스크 사이즈 보다 VM 디스크 사이즈가 큰 경우 이용
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, ,  CPU_CNT, RAM_SIZE, DISK_SIZE
	 * @param info VM정보
	 * @return
	 * @throws Exception
	 */
	public Map sizeUpVMDisk(Map param, VMInfo info) throws Exception;
	
	/**
	 * 어드민 > 데이터 센터 관리에서 사용하기 위해 필요한 Object(데이터 센터, 클러스터, 데이터 스토어, 호스트 등)정보
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, 
	 * @return LIST=[{OBJ_NM=..,OBJ_TYPE_NM=..., PARENT_OBJ_NM=...},..]
	 * @throws Exception
	 */
	public Map listObject(Map param) throws Exception;
	
	/**
	 * 공통 오브젝트 코드에 대한 각 Hypervisor Object명 
	 * @param standardType VM,DS,HOST,DC,CLUSTER,RP
	 * @return Vmware인 경우 VM은 VirtualMachine, DS는 Datastore ...으로 return
	 */
	public String getHVObjType(String standardType);
	
	
	public String getStandardObjType(String objType);
	
	/**
	 * 이미지(VM템플릿)이 존재하는 체크, 데이터 센터 관리에서 OS별 템플릿 입력 한 것 체크하기 위해 이용, 존재하지 않는 다면 Exception발생
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD, , TMPL_NM_LIST[](템플릿 경로 목록)
	 * @return 
	 * @throws Exception 
	 */
	public  Map checkTemplatePath(Map param) throws Exception;
	
	/**
	 * 클러스터내  데이터 스토어 속성 목록
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD,  
	 * @return LIST=[{summary.capacity(전체 용량), summary.freeSpace(남은 용량),summary.name(이름),overallStatus(상태), vm(VM개수)}..]
	 * @throws Exception
	 */
	public Map getDSAttribute(Map param) throws Exception;
	
	/**
	 * 데이터 센터 내 호스트 속성 목록
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD,  
	 * @return LIST=[{summary.hardware.memorySize(메모리), summary.hardware.cpuModel(CPU종류),summary.hardware.numCpuCores(CPU개수),summary.config.product.fullName(설치된 제품명),summary.config.name(이름),overallStatus(상태), vm(VM개수)}..]
	 * @throws Exception
	 */
	public Map getHostAttribute(Map param) throws Exception;
	
	/**
	 * Object별 성능 이력 종류
	 * @param objType VM,DS,HOST,DC,CLUSTER,RP
	 * @return hv.properties에 정의(vmware.perf.obj.history.xxx)
	 * @throws Exception
	 */
	public String[] getPerfHistoryTypes(String objType) throws Exception;
	
	/**
	 * VM 디스크 목록, VM 복제 시 원본의 하위 디스크가 여러 개 일 때 DB와 싱크하기 위해 이용 
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD,  , VM_NM
	 * @return LIST=[{DISK_PATH=<디스크 파일 경로>,DISK_SIZE=<디스크 사이즈 GB>},...]
	 * @throws Exception
	 */
	public List<Map> listVMDisk(Map param) throws Exception;
	
	/**
	 * VM에 디스크 추가
	 * @param after 디스크 추가 후 사후 작업 클래스
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD,  ,DISK_SIZE, DS_NM_LIST[](데이터 스토어 목록, 이중 가장 VM개수가 적은 것 순으로 대상을 찾는다.)
	 * @param info VM정보
	 * @return DISK_PATH(디스크 파일 경로)
	 * @throws Exception
	 */
	public Map addDisk(IAfterProcess after, Map param, VMInfo info) throws Exception;
	
	/**
	 * 이미지 생성
	 * @param after 생성 후 사후 작업을 위한 클래스
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD,  , IMG_NM, img_ds_nm(데이터 센터 관리에서 정의한 이미지가 저장될 데이터 스토어명, NC_DC_PROP에 저장된 정보)
	 * @param info VM정보
	 * @return 
	 * @throws Exception
	 */
	public Map createImage(IAfterProcess after, Map param, VMInfo info)
			throws Exception;
	
	/**
	 * 스냅샷 사이즈
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD,  , SNAPSHOT_NM
	 * @return DISK_SIZE
	 * @throws Exception
	 */
	public Map getSnapshotSize(Map param) throws Exception;
	
	/**
	 * WAS시작하면서 이전에 실행 중에 죽은 TASK의 상태를 DB와 싱크 시킴
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD,  ,LIST (Map<String, IAfterProcess> 미완료 TASK 목록)
	 * @throws Exception
	 */
	public void onStartUpProcess(Map param) throws Exception;


	/**
	 * VM이 실행 여부와 셋팅된 IP목록
	 * @param param REAL_DC_NM, CONN_URL,CONN_USERID,CONN_PWD,  
	 * @param vmNames VM이름들
	 * @return VM명={RUN_CD=.., IP_LIST=[{<nicId>=<ip>},..]}
	 * @throws Exception
	 */
	public  Map vmState(Map param, String... vmNames) throws Exception;



	Map getVMPerfListInDC(Map param) throws Exception;

	Map getVMListInDC(Map param) throws Exception;

	Map listAllAlarm(Map param) throws Exception;



	Map onFirstVM(Map param) throws Exception;



	public Map guestRun(Map vmInfo, String cmd, String param) throws Exception;
	
	public void guestUpload(Map vmInfo,String svrPath, String localPath) throws Exception;
	
	public void guestDownload(Map vmInfo, String svrPath, String localPath) throws Exception;
	
	
	public  Map listLicense(Map param) throws Exception;



	Map getVMIpListInDC(Map param) throws Exception;



	public void connectTest(Map param) throws Exception;



	Map chkTask(Map param) throws Exception;



	Map listAllVMMaxPerf(Map param) throws Exception;



	Map getVMTags(Map param) throws Exception;
	
}
