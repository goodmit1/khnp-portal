package com.clovirsm.hv;

import java.util.List;
import java.util.Map;

public class DummyAPI implements HypervisorAPI{

	@Override
	public Map deleteDisk(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map renameSnapshot(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map revertVM(IAfterProcess after, Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map renameVM(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map mountDisk(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map createVM(IBeforeAfter beforeAfter, boolean isDCFirst, Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map moveVMinfoFolder(Map param, VMInfo oldInfo) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map reconfigVM(Map param, VMInfo oldInfo) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map powerOpVM(Map param, VMInfo info, String op) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map deleteSnapshot(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map deleteImage(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map deleteVM(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map deleteOrg(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map openConsole(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map createSnapshot(IAfterProcess after, Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map renameImg(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map unmount(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getVMPerf(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getPerf(String type, Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map addNic(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map deleteNic(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getFirstNicId() throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Map listPerfHistory(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map listEvent(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map listAlarm(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map sizeUpVMDisk(Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map listObject(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getHVObjType(String standardType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getStandardObjType(String objType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map checkTemplatePath(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getDSAttribute(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getHostAttribute(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getPerfHistoryTypes(String objType) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map> listVMDisk(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map addDisk(IAfterProcess after, Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map createImage(IAfterProcess after, Map param, VMInfo info) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getSnapshotSize(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onStartUpProcess(Map param) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map vmState(Map param, String... vmNames) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getVMPerfListInDC(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getVMListInDC(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map listAllAlarm(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map onFirstVM(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map guestRun(Map vmInfo, String cmd, String param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guestUpload(Map vmInfo, String svrPath, String localPath) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void guestDownload(Map vmInfo, String svrPath, String localPath) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map listLicense(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getVMIpListInDC(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void connectTest(Map param) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map chkTask(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map listAllVMMaxPerf(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map getVMTags(Map param) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
