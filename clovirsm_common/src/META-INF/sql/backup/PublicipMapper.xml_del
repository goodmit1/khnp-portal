<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.clovirsm.resources.publicip.Publicip">

	<select id="list_NC_PUBLIC_IP" resultType="hashmap" parameterType="hashmap">
	/*com.clovirsm.resources.publicip.Publicip.list_NC_PUBLIC_IP*/
		SELECT
			A.*
		FROM (
			SELECT
				NC_PUBLIC_IP.PUBLIC_IP_ID,
				NC_PUBLIC_IP.PUBLIC_IP_NM,
				NC_PUBLIC_IP.PUBLIC_IP_NM AS OLD_PUBLIC_IP_NM,
				NC_PUBLIC_IP.PUBLIC_IP,
				NC_PUBLIC_IP.CMT,
			    NC_PUBLIC_IP.INS_TMS,
				NC_PUBLIC_IP.PRIVATE_IP,
				B.VM_ID,B.VM_NM,
				NC_PUBLIC_IP.DC_ID,
				NC_DC.DC_NM,
				NC_PUBLIC_IP.TEAM_CD,
				FM_TEAM.COMP_ID,
				FM_TEAM.TEAM_NM,
				CASE WHEN NC_PUBLIC_IP.FEE_TYPE_CD = 'M'
				THEN CASE WHEN MM_FEE is null THEN 0 ELSE MM_FEE END
				ELSE
					CASE WHEN NC_PUBLIC_IP.FEE_TYPE_CD = 'D'
					THEN CASE WHEN DD_FEE is null THEN 0 ELSE DD_FEE END
					ELSE
						CASE WHEN NC_PUBLIC_IP.FEE_TYPE_CD = 'H'
						THEN CASE WHEN HH_FEE is null THEN 0 ELSE HH_FEE END
						ELSE 0 END
					END
				END AS FEE,
				NC_PUBLIC_IP.FEE_TYPE_CD,
				HH_FEE,
				DD_FEE,
				MM_FEE,
				NC_PUBLIC_IP.TASK_STATUS_CD,
				NC_PUBLIC_IP.INS_ID,
				USER_NAME as INS_ID_NM
			FROM NC_PUBLIC_IP
				LEFT OUTER JOIN FM_USER ON (FM_USER.USER_ID=NC_PUBLIC_IP.INS_ID)
				LEFT OUTER JOIN
					(SELECT NC_VM.VM_ID,NC_VM.DC_ID,PRIVATE_IP, VM_NM FROM NC_VM, NC_VM_NIC WHERE NC_VM.DEL_YN='N' and NC_VM.VM_ID=NC_VM_NIC.VM_ID) B
						ON B.PRIVATE_IP = NC_PUBLIC_IP.PRIVATE_IP AND B.DC_ID = NC_PUBLIC_IP.DC_ID
				LEFT OUTER JOIN NC_DC ON NC_DC.DC_ID = NC_PUBLIC_IP.DC_ID
				LEFT OUTER JOIN FM_TEAM ON FM_TEAM.TEAM_CD = NC_PUBLIC_IP.TEAM_CD
			WHERE (NC_PUBLIC_IP.TASK_STATUS_CD = 'F' or NC_PUBLIC_IP.DEL_YN='N')
		) A
		<trim prefix="WHERE" prefixOverrides="AND">
			<include refid="com.clovirsm.monitor.not_vm_team_where"></include>
		</trim>
	</select>



	<insert id="insert_NC_PUBLIC_IP" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.insert_NC_PUBLIC_IP*/
		INSERT INTO NC_PUBLIC_IP
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="TEAM_CD != null">
				TEAM_CD,
			</if>
			<if test="PRIVATE_IP != null">
				PRIVATE_IP,
			</if>

			<if test="PUBLIC_IP != null">
				PUBLIC_IP,
			</if>
			<if test="PUBLIC_IP_NM != null">
				PUBLIC_IP_NM,
			</if>

			<if test="CMT != null">
				CMT,
			</if>
			<if test="FEE_TYPE_CD != null">
				FEE_TYPE_CD,
			</if>
			<if test="DEL_YN != null">
				DEL_YN,
			</if>
			<if test="PUBLIC_IP_ID != null">
				PUBLIC_IP_ID,
			</if>
			<if test="DC_ID != null">
				DC_ID,
			</if>
			<if test="TASK_STATUS_CD != null">
				TASK_STATUS_CD,
			</if>
			HH_FEE,DD_FEE,MM_FEE,
			INS_TMS,
			INS_ID,
			INS_PGM,
			INS_IP,
			UPD_TMS,
			UPD_ID,
			UPD_PGM,
			UPD_IP,
		</trim>
		VALUES
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="TEAM_CD != null">
				#{TEAM_CD},
			</if>
			<if test="PRIVATE_IP != null">
				#{PRIVATE_IP},
			</if>
			<if test="PUBLIC_IP != null">
				#{PUBLIC_IP},
			</if>
			<if test="PUBLIC_IP_NM != null">
				#{PUBLIC_IP_NM},
			</if>

			<if test="CMT != null">
				#{CMT},
			</if>
			<if test="FEE_TYPE_CD != null">
				#{FEE_TYPE_CD},
			</if>
			<if test="DEL_YN != null">
				#{DEL_YN},
			</if>
			<if test="PUBLIC_IP_ID != null">
				#{PUBLIC_IP_ID},
			</if>
			<if test="DC_ID != null">
				#{DC_ID},
			</if>
			<if test="TASK_STATUS_CD != null">
				#{TASK_STATUS_CD},
			</if>
			#{HH_FEE},#{DD_FEE},#{MM_FEE},
			#{_NOW_},
			#{INS_ID},
			#{INS_PGM},
			#{INS_IP},
			#{_NOW_},
			#{INS_ID},
			#{INS_PGM},
			#{INS_IP},
		</trim>
	</insert>

	<select id="list_PUBLIC_IP_FEE" resultType="hashmap" parameterType="hashmap">
		SELECT FEE.FEE_TYPE_CD, 
			CONCAT(CONCAT(CONCAT(concat(FM_DDIC.${_LANG_}_DD_DESC, '('), FEE.FEE), FM_DDIC2.${_LANG_}_DD_DESC), ')') AS FEE_TYPE_DESC FROM (
			SELECT HH_FEE as FEE, 'H' as FEE_TYPE_CD FROM NC_PUBLIC_IP WHERE PUBLIC_IP_ID = #{PUBLIC_IP_ID}
			UNION ALL
			SELECT DD_FEE as FEE, 'D' as FEE_TYPE_CD FROM NC_PUBLIC_IP WHERE PUBLIC_IP_ID = #{PUBLIC_IP_ID}
			UNION ALL
			SELECT MM_FEE as FEE, 'M' as FEE_TYPE_CD FROM NC_PUBLIC_IP WHERE PUBLIC_IP_ID = #{PUBLIC_IP_ID}
		) FEE
		JOIN FM_DDIC ON FM_DDIC.DD_VALUE = FEE.FEE_TYPE_CD AND FM_DDIC.DD_ID = 'FEE_TYPE'
		JOIN FM_DDIC FM_DDIC2 ON FM_DDIC2.DD_VALUE = FEE.FEE_TYPE_CD AND FM_DDIC2.DD_ID = 'FEE'
	</select>

	<update id="update_NC_PUBLIC_IP_REQ_FEE" parameterType="hashmap">
		UPDATE NC_PUBLIC_IP_REQ
		SET
			PRIVATE_IP = #{PRIVATE_IP}
		WHERE
			NC_PUBLIC_IP_REQ.PUBLIC_IP_ID = #{PUBLIC_IP_ID}
			AND  NC_PUBLIC_IP_REQ.APPR_STATUS_CD = 'W'

	</update>
	<select id="selectByIp_NC_PUBLIC_IP" resultType="hashmap" parameterType="hashmap">
		select * from NC_PUBLIC_IP where PUBLIC_IP=#{PUBLIC_IP} and DEL_YN='N'
	</select>
	<select id="selectByPrimaryKey_NC_PUBLIC_IP" resultType="hashmap" parameterType="hashmap">
	/*com.clovirsm.resources.publicip.Publicip.selectByPrimaryKey_NC_PUBLIC_IP*/
		SELECT NC_PUBLIC_IP.PUBLIC_IP_ID,NC_PUBLIC_IP.PUBLIC_IP,NC_PUBLIC_IP.CMT,NC_PUBLIC_IP.INS_TMS,NC_PUBLIC_IP.INS_ID,
			NC_PUBLIC_IP.PUBLIC_IP_NM, NC_PUBLIC_IP.PUBLIC_IP_NM AS OLD_PUBLIC_IP_NM,
			NC_PUBLIC_IP.PRIVATE_IP,B.VM_NM,
			NC_PUBLIC_IP.DC_ID,NC_DC.DC_NM,
			NC_PUBLIC_IP.TEAM_CD,FM_TEAM.COMP_ID,FM_TEAM.TEAM_NM,
			CASE WHEN NC_PUBLIC_IP.FEE_TYPE_CD = 'M'
			THEN CASE WHEN MM_FEE is null THEN 0 ELSE MM_FEE END
			ELSE
				CASE WHEN NC_PUBLIC_IP.FEE_TYPE_CD = 'D'
				THEN CASE WHEN DD_FEE is null THEN 0 ELSE DD_FEE END
				ELSE
					CASE WHEN NC_PUBLIC_IP.FEE_TYPE_CD = 'H'
					THEN CASE WHEN HH_FEE is null THEN 0 ELSE HH_FEE END
					ELSE 0 END
				END
			END AS FEE, NC_PUBLIC_IP.FEE_TYPE_CD,
			HH_FEE,DD_FEE,MM_FEE,
			NC_PUBLIC_IP.FAIL_MSG,
			(SELECT COUNT(NC_PUBLIC_IP_REQ.PUBLIC_IP_ID) FROM NC_PUBLIC_IP_REQ WHERE NC_PUBLIC_IP_REQ.PUBLIC_IP_ID = NC_PUBLIC_IP.PUBLIC_IP_ID AND NC_PUBLIC_IP_REQ.APPR_STATUS_CD = 'W' AND NC_PUBLIC_IP_REQ.INS_ID != #{_USER_ID_}) AS REQ_CNT,
			(SELECT COUNT(NC_PUBLIC_IP_REQ.PUBLIC_IP_ID) FROM NC_PUBLIC_IP_REQ WHERE NC_PUBLIC_IP_REQ.PUBLIC_IP_ID = NC_PUBLIC_IP.PUBLIC_IP_ID AND NC_PUBLIC_IP_REQ.APPR_STATUS_CD = 'R') AS APPR_CNT
		FROM NC_PUBLIC_IP
		LEFT OUTER JOIN
			(SELECT NC_VM.VM_ID,NC_VM.DC_ID,PRIVATE_IP, VM_NM FROM NC_VM, NC_VM_NIC WHERE NC_VM.DEL_YN='N' and NC_VM.VM_ID=NC_VM_NIC.VM_ID) B
				ON B.PRIVATE_IP = NC_PUBLIC_IP.PRIVATE_IP AND B.DC_ID = NC_PUBLIC_IP.DC_ID
		LEFT OUTER JOIN NC_DC ON NC_DC.DC_ID = NC_PUBLIC_IP.DC_ID
		LEFT OUTER JOIN FM_TEAM ON FM_TEAM.TEAM_CD = NC_PUBLIC_IP.TEAM_CD
		WHERE NC_PUBLIC_IP.PUBLIC_IP_ID= #{PUBLIC_IP_ID}
	</select>

	<update id="update_NC_PUBLIC_IP" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.update_NC_PUBLIC_IP*/
		UPDATE NC_PUBLIC_IP
		<trim prefix="SET" suffixOverrides=",">
			<if test="CUD_CD == null">
				<if test="PUBLIC_IP_NM != null">
					PUBLIC_IP_NM= #{PUBLIC_IP_NM},
				</if>
				<if test="CMT != null">
					CMT= #{CMT},
				</if>
				<if test="FEE_TYPE_CD != null">
					FEE_TYPE_CD= #{FEE_TYPE_CD},
				</if>
			</if>
			<if test="CUD_CD != null">
				<if test="DEL_YN != null">
					DEL_YN= #{DEL_YN},
				</if>
				<if test='PRIVATE_IP != null'>
					PRIVATE_IP= #{PRIVATE_IP},
				</if>
				TASK_STATUS_CD = 'W' ,
			</if>
			<include refid="com.clovirsm.common.Component.updateCommonForReq"></include>
		</trim>
		where PUBLIC_IP_ID= #{PUBLIC_IP_ID}
	</update>



	<insert id="insert_NC_FW" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.insert_NC_FW*/
		INSERT INTO NC_FW
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				PROTOCOL_CD,
			</if>
			<if test="PORT != null">
				PORT,
			</if>
			 
			<if test="CMT != null">
				CMT,
			</if>
			<if test="CIDR != null">
				CIDR,
			</if>
			<if test="PUBLIC_IP_ID != null">
				PUBLIC_IP_ID,
			</if>
			<if test="VM_ID != null">
				VM_ID,
			</if>
			<if test="FW_ID != null">
				FW_ID,
			</if>
			<if test="DEL_YN != null">
				DEL_YN,
			</if>
			<if test="INS_TMS != null">
				INS_TMS,
			</if>
			<if test="UPD_TMS != null">
				UPD_TMS,
			</if>
			INS_TMS,
			INS_ID,
			INS_PGM,
			INS_IP,
			UPD_TMS,
			UPD_ID,
			UPD_PGM,
			UPD_IP,
		</trim>
		VALUES
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				#{PROTOCOL_CD},
			</if>
			<if test="PORT != null">
				#{PORT},
			</if>
			 
			<if test="CMT != null">
				#{CMT},
			</if>
			<if test="CIDR != null">
				#{CIDR},
			</if>
			<if test="PUBLIC_IP_ID != null">
				#{PUBLIC_IP_ID},
			</if>
			<if test="VM_ID != null">
				#{VM_ID},
			</if>
			<if test="FW_ID != null">
				#{FW_ID},
			</if>
			<if test="DEL_YN != null">
				#{DEL_YN},
			</if>
			<if test="INS_TMS != null">
				#{INS_TMS},
			</if>
			<if test="UPD_TMS != null">
				#{UPD_TMS},
			</if>
			#{_NOW_},
			#{INS_ID},
			#{INS_PGM},
			#{INS_IP},
			#{_NOW_},
			#{INS_ID},
			#{INS_PGM},
			#{INS_IP},
		</trim>
	</insert>

	<select id="list_NC_FW" resultType="hashmap" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.list_NC_FW*/
		SELECT A.* FROM (
			SELECT NC_FW.*, get_vm_ip(PUBLIC_IP_ID, VM_ID) PUBLIC_IP,
			(SELECT COUNT(NC_FW_REQ.FW_ID) FROM NC_FW_REQ WHERE NC_FW_REQ.FW_ID = NC_FW.FW_ID AND NC_FW_REQ.APPR_STATUS_CD = 'W' AND NC_FW_REQ.INS_ID != #{_USER_ID_}) AS REQ_CNT,
			(SELECT COUNT(NC_FW_REQ.FW_ID) FROM NC_FW_REQ WHERE NC_FW_REQ.FW_ID = NC_FW.FW_ID AND NC_FW_REQ.APPR_STATUS_CD = 'R') AS APPR_CNT
			FROM NC_FW 
			WHERE (NC_FW.TASK_STATUS_CD = 'F' or NC_FW.DEL_YN='N')
		) A
		<trim prefix="WHERE" prefixOverrides="AND">
			<choose>
			<when test="PUBLIC_IP_ID != null  and PUBLIC_IP_ID != '' ">
				AND PUBLIC_IP_ID=#{PUBLIC_IP_ID}
			</when>
			<when test="VM_ID != null  and VM_ID != '' ">
				AND VM_ID=#{VM_ID}
			</when>
			<otherwise>
				AND 1=2
			</otherwise>
			</choose>
		</trim>
	</select>

	<update id="update_NC_FW" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.update_NC_FW*/
		UPDATE NC_FW
		<trim prefix="SET" suffixOverrides=",">
			<if test="CUD_CD == null and CMT != null">
				CMT= #{CMT},
			</if>
			<if test="CUD_CD != null">
				<if test="PROTOCOL_CD != null">
					PROTOCOL_CD= #{PROTOCOL_CD},
				</if>
				<if test="PORT != null">
					PORT= #{PORT},
				</if>
				 

				<if test="CIDR != null">
					CIDR= #{CIDR},
				</if>
				DEL_YN=#{DEL_YN},
				TASK_STATUS_CD = 'W' ,
			</if>
			<include refid="com.clovirsm.common.Component.updateCommonForReq"></include>
		</trim>
		where FW_ID= #{FW_ID}
	</update>

	<insert id="delete_NC_PUBLIC_IP_REQ" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.delete_NC_PUBLIC_IP_REQ*/
		INSERT INTO NC_PUBLIC_IP_REQ
		<trim prefix="(" suffix=")" suffixOverrides=",">
			PUBLIC_IP_ID,
			PUBLIC_IP_NM,
			CMT,
			DC_ID,
			PRIVATE_IP,
			FEE_TYPE_CD,
			HH_FEE,DD_FEE,MM_FEE,
			CUD_CD,
			APPR_STATUS_CD,
			TEAM_CD,
			INS_DT, INS_ID, INS_PGM, INS_IP,
		</trim>
		VALUES
		<trim prefix="(" suffix=")" suffixOverrides=",">
			#{PUBLIC_IP_ID},
			#{PUBLIC_IP_NM},
			#{CMT},
			#{DC_ID},
			#{PRIVATE_IP},
			#{FEE_TYPE_CD},
			<if test="HH_FEE != null">#{HH_FEE},</if>
			<if test="HH_FEE == null">(SELECT HH_FEE FROM NC_ETC_FEE WHERE SVC_CD = 'I'),</if>
			<if test="DD_FEE != null">#{DD_FEE},</if>
			<if test="DD_FEE == null">(SELECT DD_FEE FROM NC_ETC_FEE WHERE SVC_CD = 'I'),</if>
			<if test="MM_FEE != null">#{MM_FEE},</if>
			<if test="MM_FEE == null">(SELECT MM_FEE FROM NC_ETC_FEE WHERE SVC_CD = 'I'),</if>
			#{CUD_CD},
			#{APPR_STATUS_CD},
			#{_TEAM_CD_},
			#{_SYSDATE_},
			#{_USER_ID_},
			#{_USER_PGM_},
			#{_USER_IP_},
		</trim>
	</insert>

	<insert id="insert_NC_PUBLIC_IP_REQ" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.insert_NC_PUBLIC_IP_REQ*/
		INSERT INTO NC_PUBLIC_IP_REQ
		<trim prefix="(" suffix=")" suffixOverrides=",">
			PUBLIC_IP_ID,
			PUBLIC_IP_NM,
			CMT,
			DC_ID,
			PRIVATE_IP,
			FEE_TYPE_CD,
			HH_FEE,DD_FEE,MM_FEE,
			CUD_CD,
			APPR_STATUS_CD,
			TEAM_CD,
			INS_DT, INS_ID, INS_PGM, INS_IP,
		</trim>
		VALUES
		<trim prefix="(" suffix=")" suffixOverrides=",">
			#{PUBLIC_IP_ID},
			#{PUBLIC_IP_NM},
			#{CMT},
			#{DC_ID},
			#{PRIVATE_IP},
			#{FEE_TYPE_CD},
			<if test="HH_FEE != null">#{HH_FEE},</if>
			<if test="HH_FEE == null">(SELECT HH_FEE FROM NC_ETC_FEE WHERE SVC_CD = 'I'),</if>
			<if test="DD_FEE != null">#{DD_FEE},</if>
			<if test="DD_FEE == null">(SELECT DD_FEE FROM NC_ETC_FEE WHERE SVC_CD = 'I'),</if>
			<if test="MM_FEE != null">#{MM_FEE},</if>
			<if test="MM_FEE == null">(SELECT MM_FEE FROM NC_ETC_FEE WHERE SVC_CD = 'I'),</if>
			#{CUD_CD},
			#{APPR_STATUS_CD},
			#{_TEAM_CD_},
			#{_SYSDATE_},
			#{_USER_ID_},
			#{_USER_PGM_},
			#{_USER_IP_},
		</trim>
		<selectKey keyColumn="INS_DT" keyProperty="INS_DT" resultType="java.lang.String" order="AFTER" >
			SELECT INS_DT FROM NC_PUBLIC_IP_REQ WHERE PUBLIC_IP_ID = #{PUBLIC_IP_ID} AND APPR_STATUS_CD = 'W'
		</selectKey>
	</insert>

	<select id="selectByPrimaryKey_NC_PUBLIC_IP_REQ" resultType="hashmap" parameterType="hashmap">
	/*com.clovirsm.resources.publicip.Publicip.selectByPrimaryKey_NC_PUBLIC_IP_REQ*/
		SELECT NC_PUBLIC_IP_REQ.PUBLIC_IP_ID,NC_PUBLIC_IP_REQ.PUBLIC_IP_NM,NC_PUBLIC_IP_REQ.CMT,NC_PUBLIC_IP_REQ.INS_DT,
			NC_PUBLIC_IP_REQ.CUD_CD,
			NC_PUBLIC_IP_REQ.PRIVATE_IP,B.VM_ID,B.VM_NM,
			NC_PUBLIC_IP_REQ.DC_ID,NC_DC.DC_NM,
			NC_PUBLIC_IP_REQ.TEAM_CD,FM_TEAM.COMP_ID,FM_TEAM.TEAM_NM,
			NC_PUBLIC_IP_REQ.INS_ID,
			CASE WHEN NC_PUBLIC_IP_REQ.FEE_TYPE_CD = 'M'
			THEN CASE WHEN MM_FEE is null THEN 0 ELSE MM_FEE END
			ELSE
				CASE WHEN NC_PUBLIC_IP_REQ.FEE_TYPE_CD = 'D'
				THEN CASE WHEN DD_FEE is null THEN 0 ELSE DD_FEE END
				ELSE
					CASE WHEN NC_PUBLIC_IP_REQ.FEE_TYPE_CD = 'H'
					THEN CASE WHEN HH_FEE is null THEN 0 ELSE HH_FEE END
					ELSE 0 END
				END
			END AS FEE, NC_PUBLIC_IP_REQ.FEE_TYPE_CD
		FROM NC_PUBLIC_IP_REQ
		LEFT OUTER JOIN 	(SELECT NC_VM.VM_ID, PRIVATE_IP, VM_NM FROM NC_VM, NC_VM_NIC WHERE NC_VM.DEL_YN='N' and NC_VM.VM_ID=NC_VM_NIC.VM_ID) B
					ON B.PRIVATE_IP = NC_PUBLIC_IP_REQ.PRIVATE_IP
		LEFT OUTER JOIN NC_DC ON NC_DC.DC_ID = NC_PUBLIC_IP_REQ.DC_ID
		LEFT OUTER JOIN FM_TEAM ON FM_TEAM.TEAM_CD = NC_PUBLIC_IP_REQ.TEAM_CD
		WHERE NC_PUBLIC_IP_REQ.PUBLIC_IP_ID= #{PUBLIC_IP_ID} AND INS_DT = #{INS_DT}
	</select>

	<update id="update_NC_PUBLIC_IP_REQ" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.update_NC_PUBLIC_IP_REQ*/
		UPDATE NC_PUBLIC_IP_REQ
		<trim prefix="SET" suffixOverrides=",">
			PUBLIC_IP_NM= #{PUBLIC_IP_NM},
			CMT= #{CMT},
			PRIVATE_IP= #{PRIVATE_IP},
			FEE_TYPE_CD= #{FEE_TYPE_CD},
			HH_FEE=<if test="HH_FEE != null">#{HH_FEE},</if>
			<if test="HH_FEE == null">(SELECT HH_FEE FROM NC_ETC_FEE WHERE SVC_CD = 'I'),</if>
			DD_FEE=<if test="DD_FEE != null">#{DD_FEE},</if>
			<if test="DD_FEE == null">(SELECT DD_FEE FROM NC_ETC_FEE WHERE SVC_CD = 'I'),</if>
			MM_FEE=<if test="MM_FEE != null">#{MM_FEE},</if>
			<if test="MM_FEE == null">(SELECT MM_FEE FROM NC_ETC_FEE WHERE SVC_CD = 'I'),</if>
			INS_PGM = #{_USER_PGM_},
			INS_ID = #{_USER_ID_},
			INS_IP = #{_USER_IP_},
			INS_DT = #{_SYSDATE_}
		</trim>
		where PUBLIC_IP_ID= #{PUBLIC_IP_ID}  AND APPR_STATUS_CD='W'
	</update>

	<select id="selectByPrimaryKey_NC_FW_REQ" resultType="hashmap" parameterType="hashmap">
	/*com.clovirsm.resources.publicip.Publicip.selectByPrimaryKey_NC_FW_REQ*/
		SELECT NC_FW_REQ.*,get_vm_ip(PUBLIC_IP_ID, VM_ID) PUBLIC_IP
		FROM NC_FW_REQ  
		WHERE NC_FW_REQ.FW_ID= #{FW_ID} 
		AND NC_FW_REQ.INS_DT = #{INS_DT}
		 
	</select>

	<insert id="insert_NC_FW_REQ" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.insert_NC_FW_REQ*/
		INSERT INTO NC_FW_REQ
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				PROTOCOL_CD,
			</if>
			<if test="PORT != null">
				PORT,
			</if>
			 
			<if test="CMT != null">
				CMT,
			</if>
			<if test="CIDR != null">
				CIDR,
			</if>
			<if test="PUBLIC_IP_ID != null">
				PUBLIC_IP_ID,
			</if>
			<if test="VM_ID != null">
				VM_ID,
			</if>
			<if test="FW_ID != null">
				FW_ID,
			</if>
			CUD_CD,
			APPR_STATUS_CD,
			INS_DT,
			INS_ID,
			INS_PGM,
			INS_IP,
		</trim>
		VALUES
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				#{PROTOCOL_CD},
			</if>
			<if test="PORT != null">
				#{PORT},
			</if>
			 
			<if test="CMT != null">
				#{CMT},
			</if>
			<if test="CIDR != null">
				#{CIDR},
			</if>
			<if test="PUBLIC_IP_ID != null">
				#{PUBLIC_IP_ID},
			</if>
			<if test="VM_ID != null">
				#{VM_ID},
			</if>
			<if test="FW_ID != null">
				#{FW_ID},
			</if>
			#{CUD_CD},
			#{APPR_STATUS_CD},
			#{_SYSDATE_},
			#{_USER_ID_},
			#{_USER_PGM_},
			#{_USER_IP_},
		</trim>
	</insert>

	<update id="update_NC_FW_REQ" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.update_NC_FW_REQ*/
		UPDATE NC_FW_REQ
		<trim prefix="SET" suffixOverrides=",">
			<if test="CIDR != null">
				CIDR=#{CIDR},
			</if>
			<if test="PROTOCOL_CD != null">
				PROTOCOL_CD=#{PROTOCOL_CD},
			</if>
			<if test="PORT != null">
				PORT=#{PORT},
			</if>
			 
			<if test="CMT != null">
				CMT=#{CMT},
			</if>
			INS_PGM = #{_USER_PGM_},
			INS_ID = #{_USER_ID_},
			INS_IP = #{_USER_IP_},
			INS_DT = #{_SYSDATE_},
		</trim>
		where FW_ID= #{FW_ID}  AND APPR_STATUS_CD='W'
	</update>
	
	<select id="selectDuplicateChk_NC_PUBLIC_IP" resultType="int" parameterType="hashmap">
	/*com.clovirsm.resources.publicIp.selectDuplicateChk*/
		SELECT
			count(1) CNT
		FROM
			NC_PUBLIC_IP A
		WHERE DEL_YN = 'N'
			AND (
				<if test="PUBLIC_IP != null">
					PUBLIC_IP=#{PUBLIC_IP} OR
				</if>
				 (
					TEAM_CD IN (SELECT TEAM_CD FROM FM_TEAM WHERE COMP_ID = #{_COMP_ID_})
					AND PUBLIC_IP_NM = #{PUBLIC_IP_NM}
				)
			)
			<if test="PUBLIC_IP_ID != null">
			AND PUBLIC_IP_ID != #{PUBLIC_IP_ID}
			</if>
	</select>
	
	<insert id="delete_NC_FW_REQ" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.delete_NC_FW_REQ*/
		INSERT INTO NC_FW_REQ
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				PROTOCOL_CD,
			</if>
			<if test="PORT != null">
				PORT,
			</if>
			 
			<if test="CMT != null">
				CMT,
			</if>
			<if test="CIDR != null">
				CIDR,
			</if>
			<if test="PUBLIC_IP_ID != null">
				PUBLIC_IP_ID,
			</if>
			<if test="FW_ID != null">
				FW_ID,
			</if>
			CUD_CD,
			APPR_STATUS_CD,
			INS_DT,
			INS_ID,
			INS_PGM,
			INS_IP,
		</trim>
		VALUES
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				#{PROTOCOL_CD},
			</if>
			<if test="PORT != null">
				#{PORT},
			</if>
			 
			<if test="CMT != null">
				#{CMT},
			</if>
			<if test="CIDR != null">
				#{CIDR},
			</if>
			<if test="PUBLIC_IP_ID != null">
				#{PUBLIC_IP_ID},
			</if>
			<if test="FW_ID != null">
				#{FW_ID},
			</if>
			#{CUD_CD},
			#{APPR_STATUS_CD},
			#{_SYSDATE_},
			#{_USER_ID_},
			#{_USER_PGM_},
			#{_USER_IP_},
		</trim>
	</insert>

	<insert id="insert_NC_NAT" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.insert_NC_NAT*/
		INSERT INTO NC_NAT
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				PROTOCOL_CD,
			</if>
			<if test="PRIVATE_IP != null">
				PRIVATE_IP,
			</if>
			<if test="START_PORT != null">
				START_PORT,
			</if>
			<if test="END_PORT != null">
				END_PORT,
			</if>
			<if test="CMT != null">
				CMT,
			</if>

			<if test="PUBLIC_IP_ID != null">
				PUBLIC_IP_ID,
			</if>
			<if test="NAT_ID != null">
				NAT_ID,
			</if>
			<if test="DEL_YN != null">
				DEL_YN,
			</if>
			<if test="INS_TMS != null">
				INS_TMS,
			</if>
			<if test="UPD_TMS != null">
				UPD_TMS,
			</if>
			INS_TMS,
			INS_ID,
			INS_PGM,
			INS_IP,
			UPD_TMS,
			UPD_ID,
			UPD_PGM,
			UPD_IP,
		</trim>
		VALUES
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				#{PROTOCOL_CD},
			</if>
			<if test="PRIVATE_IP != null">
				#{PRIVATE_IP},
			</if>
			<if test="START_PORT != null">
				#{START_PORT},
			</if>
			<if test="END_PORT != null">
				#{END_PORT},
			</if>
			<if test="CMT != null">
				#{CMT},
			</if>

			<if test="PUBLIC_IP_ID != null">
				#{PUBLIC_IP_ID},
			</if>
			<if test="NAT_ID != null">
				#{NAT_ID},
			</if>
			<if test="DEL_YN != null">
				#{DEL_YN},
			</if>
			<if test="INS_TMS != null">
				#{INS_TMS},
			</if>
			<if test="UPD_TMS != null">
				#{UPD_TMS},
			</if>
			#{_NOW_},
			#{INS_ID},
			#{INS_PGM},
			#{INS_IP},
			#{_NOW_},
			#{INS_ID},
			#{INS_PGM},
			#{INS_IP},
		</trim>
	</insert>

	<select id="list_NC_NAT" resultType="hashmap" parameterType="hashmap">
	/*com.clovirsm.resources.publicip.Publicip.list_NC_NAT*/
		SELECT A.* FROM (
			SELECT
				NC_NAT.*, B.VM_ID, B.DC_ID,
				(SELECT COUNT(NC_NAT_REQ.NAT_ID) FROM NC_NAT_REQ WHERE NC_NAT_REQ.NAT_ID = NC_NAT.NAT_ID AND NC_NAT_REQ.APPR_STATUS_CD = 'W' AND NC_NAT_REQ.INS_ID != #{_USER_ID_}) AS REQ_CNT,
				(SELECT COUNT(NC_NAT_REQ.NAT_ID) FROM NC_NAT_REQ WHERE NC_NAT_REQ.NAT_ID = NC_NAT.NAT_ID AND NC_NAT_REQ.APPR_STATUS_CD = 'R') AS APPR_CNT
			FROM NC_NAT
			LEFT OUTER JOIN NC_PUBLIC_IP ON NC_PUBLIC_IP.PUBLIC_IP_ID = NC_NAT.PUBLIC_IP_ID
			LEFT OUTER JOIN
				(SELECT NC_VM.VM_ID,NC_VM.DC_ID,PRIVATE_IP, VM_NM FROM NC_VM, NC_VM_NIC WHERE NC_VM.DEL_YN='N' and NC_VM.VM_ID=NC_VM_NIC.VM_ID) B
					ON B.PRIVATE_IP = NC_PUBLIC_IP.PRIVATE_IP AND B.DC_ID = NC_PUBLIC_IP.DC_ID
			LEFT OUTER JOIN NC_DC ON NC_DC.DC_ID = NC_PUBLIC_IP.DC_ID
			WHERE NC_NAT.TASK_STATUS_CD = 'F' OR NC_NAT.DEL_YN='N'
		) A
		<trim prefix="WHERE" prefixOverrides="AND">
			AND PUBLIC_IP_ID =#{PUBLIC_IP_ID}
		</trim>
	</select>

	<update id="update_NC_NAT" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.update_NC_NAT*/
		UPDATE NC_NAT
		<trim prefix="SET" suffixOverrides=",">
			<if test="CUD_CD == null and CMT != null">
				CMT= #{CMT},
			</if>
			<if test="CUD_CD != null">
				<if test="PROTOCOL_CD != null">
					PROTOCOL_CD= #{PROTOCOL_CD},
				</if>
				<if test="PRIVATE_IP != null">
					PRIVATE_IP= #{PRIVATE_IP},
				</if>
				<if test="START_PORT != null">
					START_PORT= #{START_PORT},
				</if>
				<if test="END_PORT != null">
					END_PORT= #{END_PORT},
				</if>

				INS_DT = #{INS_DT},
				DEL_YN=#{DEL_YN},
				TASK_STATUS_CD = 'W' ,
			</if>
			<include refid="com.clovirsm.common.Component.updateCommonForReq"></include>
		</trim>
		where NAT_ID= #{NAT_ID}
	</update>
  <select id="selectByPrimaryKey_NC_NAT_REQ" resultType="hashmap" parameterType="hashmap">
	/*com.clovirsm.resources.publicip.Publicip.selectByPrimaryKey_NC_NAT_REQ*/
		SELECT NC_NAT_REQ.*, NC_PUBLIC_IP.PUBLIC_IP, B.VM_ID, NC_DC.DC_ID
		FROM NC_NAT_REQ
		LEFT OUTER JOIN NC_PUBLIC_IP ON NC_PUBLIC_IP.PUBLIC_IP_ID = NC_NAT_REQ.PUBLIC_IP_ID
		LEFT OUTER JOIN
			(SELECT NC_VM.VM_ID,NC_VM.DC_ID,PRIVATE_IP, VM_NM FROM NC_VM, NC_VM_NIC WHERE NC_VM.DEL_YN='N' and NC_VM.VM_ID=NC_VM_NIC.VM_ID) B
				ON B.PRIVATE_IP = NC_NAT_REQ.PRIVATE_IP AND B.DC_ID = NC_PUBLIC_IP.DC_ID
		LEFT OUTER JOIN NC_DC ON NC_DC.DC_ID = NC_PUBLIC_IP.DC_ID
		WHERE NC_NAT_REQ.NAT_ID= #{NAT_ID} AND NC_NAT_REQ.INS_DT = #{INS_DT}
	</select>

	<insert id="insert_NC_NAT_REQ" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.insert_NC_NAT_REQ*/
		INSERT INTO NC_NAT_REQ
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				PROTOCOL_CD,
			</if>
			<if test="PRIVATE_IP != null">
				PRIVATE_IP,
			</if>
			<if test="START_PORT != null">
				START_PORT,
			</if>
			<if test="END_PORT != null">
				END_PORT,
			</if>
			<if test="CMT != null">
				CMT,
			</if>

			<if test="PUBLIC_IP_ID != null">
				PUBLIC_IP_ID,
			</if>
			<if test="NAT_ID != null">
				NAT_ID,
			</if>
			CUD_CD,
			APPR_STATUS_CD,
			INS_DT,
			INS_ID,
			INS_PGM,
			INS_IP,
		</trim>
		VALUES
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				#{PROTOCOL_CD},
			</if>
			<if test="PRIVATE_IP != null">
				#{PRIVATE_IP},
			</if>
			<if test="START_PORT != null">
				#{START_PORT},
			</if>
			<if test="END_PORT != null">
				#{END_PORT},
			</if>
			<if test="CMT != null">
				#{CMT},
			</if>

			<if test="PUBLIC_IP_ID != null">
				#{PUBLIC_IP_ID},
			</if>
			<if test="NAT_ID != null">
				#{NAT_ID},
			</if>
			#{CUD_CD},
			#{APPR_STATUS_CD},
			#{_SYSDATE_},
			#{_USER_ID_},
			#{_USER_PGM_},
			#{_USER_IP_},
		</trim>
	</insert>

	<update id="update_NC_NAT_REQ" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.update_NC_NAT_REQ*/
		UPDATE NC_NAT_REQ
		<trim prefix="SET" suffixOverrides=",">
	 		<if test="PROTOCOL_CD != null">
				PROTOCOL_CD= #{PROTOCOL_CD},
			</if>
			<if test="PRIVATE_IP != null">
				PRIVATE_IP=#{PRIVATE_IP},
			</if>
			<if test="START_PORT != null">
				START_PORT=#{START_PORT},
			</if>
			<if test="END_PORT != null">
				END_PORT=#{END_PORT},
			</if>
			<if test="CMT != null">
				CMT=#{CMT},
			</if>
			INS_PGM = #{_USER_PGM_},
			INS_ID = #{_USER_ID_},
			INS_IP = #{_USER_IP_},
			INS_DT = #{_SYSDATE_},
		</trim>
		where NAT_ID= #{NAT_ID}  AND APPR_STATUS_CD='W'
	</update>

	<select id="selectDuplicateChk_NC_NAT" resultType="int" parameterType="hashmap">
		/*com.clovirsm.resources.publicIp.selectDuplicateChk_NC_NAT*/
		SELECT
			count(1) CNT
		FROM
			NC_NAT A
		WHERE DEL_YN = 'N'
			AND PUBLIC_IP_ID = #{PUBLIC_IP_ID}
			AND START_PORT = #{START_PORT}
			<if test="NAT_ID != null">
			AND NAT_ID != #{NAT_ID}
			</if>
	</select>

	<insert id="delete_NC_NAT_REQ" parameterType="hashmap">
		/*com.clovirsm.resources.publicip.Publicip.delete_NC_NAT_REQ*/
		INSERT INTO NC_NAT_REQ
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				PROTOCOL_CD,
			</if>
			<if test="PRIVATE_IP != null">
				PRIVATE_IP,
			</if>
			<if test="START_PORT != null">
				START_PORT,
			</if>
			<if test="END_PORT != null">
				END_PORT,
			</if>
			<if test="CMT != null">
				CMT,
			</if>

			<if test="PUBLIC_IP_ID != null">
				PUBLIC_IP_ID,
			</if>
			<if test="NAT_ID != null">
				NAT_ID,
			</if>
			CUD_CD,
			APPR_STATUS_CD,
			INS_DT,
			INS_ID,
			INS_PGM,
			INS_IP,
		</trim>
		VALUES
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="PROTOCOL_CD != null">
				#{PROTOCOL_CD},
			</if>
			<if test="PRIVATE_IP != null">
				#{PRIVATE_IP},
			</if>
			<if test="START_PORT != null">
				#{START_PORT},
			</if>
			<if test="END_PORT != null">
				#{END_PORT},
			</if>
			<if test="CMT != null">
				#{CMT},
			</if>

			<if test="PUBLIC_IP_ID != null">
				#{PUBLIC_IP_ID},
			</if>
			<if test="NAT_ID != null">
				#{NAT_ID},
			</if>
			#{CUD_CD},
			#{APPR_STATUS_CD},
			#{_SYSDATE_},
			#{_USER_ID_},
			#{_USER_PGM_},
			#{_USER_IP_},
		</trim>
	</insert>
</mapper>