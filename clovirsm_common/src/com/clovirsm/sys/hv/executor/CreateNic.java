package com.clovirsm.sys.hv.executor;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * 랜 카드 추가
 * @author 윤경
 *
 */
@Component
public class CreateNic  extends ReconfigVM
{

	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		int nextNicId = dcService.getNextNicId(param);
		param.put(HypervisorAPI.PARAM_NIC_ID, nextNicId);
		List<IPInfo> ipInfo = dcInfo.getBefore(dcService).getNextIp( nextNicId, param);
		/*for(IPInfo ip : ipInfo)
		{
			dcService.insertNic(ip, param);
		}*/
		param.put("IP", ipInfo);
		return dcInfo.getAPI().addNic(param, info);
	}
 

}
