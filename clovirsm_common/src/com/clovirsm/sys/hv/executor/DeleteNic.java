package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.fliconz.fm.common.util.NumberHelper;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * 랜카드 삭제
 * @author 윤경
 *
 */
@Component
public class DeleteNic extends Common {
	public void run(String vmId, String nicId) throws Exception
	{
		Map param = dcService.getVMInfo(vmId);

		param.put(HypervisorAPI.PARAM_NIC_ID, nicId);
		this.run(param);

	}
	@Override
	protected Map run1(DC dcInfo,boolean isNew, Map param) throws Exception {

		VMInfo info = dcService.toVMInfo(param);
		int nicId = NumberHelper.getInt(param.get(HypervisorAPI.PARAM_NIC_ID));
		if(nicId == dcInfo.getAPI().getFirstNicId())
		{
			throw new Exception(MsgUtil.getMsg("msg_cant_del_first_lan", null));
		}
		Map result = dcInfo.getAPI().deleteNic(param, info);
		//dcService.deleteNic(param);
		return result;
	}
}
