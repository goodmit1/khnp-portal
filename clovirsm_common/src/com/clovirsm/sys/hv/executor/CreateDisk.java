package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * 디스크 추가
 * @author 윤경
 *
 */
@Component
public class CreateDisk extends  Common{
	
	
	public void run(  String diskId) throws Exception
	{
	 
		Map diskInfo = dcService.getDiskInfo(diskId);
		String[] dsNames = dcService.getDsName(diskInfo);
		// 디스크 종류에 해당하는 데이터 스토어 모두 보낸다. 이중  가장 VM이 적으면서 용량에 여유가 있는 것을 선택한다. 
		diskInfo.put(HypervisorAPI.PARAM_DATASTORE_NM_LIST, dsNames); 
		this.run(diskInfo);
	}
	@Override
	protected Map run1(DC dcInfo,boolean isNew, Map param) throws Exception {
		
		VMInfo info = dcService.toVMInfo(param);
		 
		return doProcess(dcInfo, param, info);
	}
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		IBeforeAfter before = dcInfo.getBefore( dcService  );
		Map result = dcInfo.getAPI().addDisk(before.onAfterProcess("D","C", param),param, info );
		 
		return result;
	}
}
