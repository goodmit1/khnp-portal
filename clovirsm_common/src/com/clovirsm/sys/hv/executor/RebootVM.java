package com.clovirsm.sys.hv.executor;

import org.springframework.stereotype.Component;

import com.clovirsm.hv.vmware.VMWareAPI;

/**
 * VM OS reboot
 * @author 윤경
 *
 */
@Component
public class RebootVM extends PowerOnVM{
	protected String getOp()
	{
		return VMWareAPI.POWER_REBOOT;
	}
}
