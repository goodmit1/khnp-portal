package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.service.monitor.MonitorService;
import com.clovirsm.sys.hv.DC;

/**
 * 문제 목록
 * @author 윤경
 *
 */
@Component
public class ListAlarm extends   ListEvent{

	@Autowired MonitorService monitorService;
	
	public Map run(String dc, String type, String name) throws Exception
	{
		Map param = new HashMap();
		param.put(PARAM_DC_ID, dc);
		if(type.equals(HypervisorAPI.OBJ_TYPE_VM))
		{ 
			param.put("VM_NM", name);
		}
		param.put(HypervisorAPI.PARAM_OBJECT_NM, name);
		
	 
		
		param.put(HypervisorAPI.PARAM_OBJECT_TYPE, type);
		param.put("REAL_YN","Y");
		
		return this.run(param);
		
	}
 
	@Override
	protected Map doProcess(DC dcInfo, Map param  ) throws Exception
	{
		if(!"Y".equals(param.get("REAL_YN")))
		{
			Map result = new HashMap();
			result.put(HypervisorAPI.PARAM_LIST,   monitorService.selectList("ALARM", param));
			return result;
		}
		return dcInfo.getAPI().listAlarm(param );
	}
	 
}
