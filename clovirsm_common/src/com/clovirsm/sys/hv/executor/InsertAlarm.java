package com.clovirsm.sys.hv.executor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.PropertyManager;

/**
 * 어드민 > 데이터 센터 관리에서 사용하기 위해 필요한 Object(데이터 센터, 클러스터, 데이터 스토어, 호스트 등)를 가지고 와서  NC_HV_OBJ테이블에 insert
 * 
 * @author 윤경
 *
 */
@Component("insertAlarm")
public class InsertAlarm extends Common implements ICollectByDC{
	
	public void runAll(boolean isWait) throws Exception
	{
		 List<Map> list = dcService.selectList("NC_DC", new HashMap());
			 
			for(Map m : list){
				
				CollectRunThread runThread = new CollectRunThread(this,(String)m.get("DC_ID"));
				runThread.start(); 
				if(isWait)
				{
					runThread.join();
				}
			} 
	}
	
	
	@Autowired BatchService batchService;
	public List run(String dcId ) throws Exception
	{
		Map param = new HashMap();
		param.put("DC_ID", dcId);
		 
		Map result = super.run(param);
		if(result == null) return null;
		return (List)result.get(HypervisorAPI.PARAM_LIST);
		
	}
	 
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		 return dcInfo.getAPI().listAllAlarm(param);
	}
	protected String getTableNm()
	{
		return "NC_HV_ALARM";
	}
	protected void setExtraInfo(Map m, Map param, DC dcInfo) throws Exception
	{
		
		m.put("TARGET_TYPE", dcInfo.getAPI().getStandardObjType((String)m.get("TARGET_TYPE")));
	}
	protected void onBeforeRun(Map param) throws Exception
	{
		batchService.deleteMonitorInfo(getTableNm(),param);
	}
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		 
		Map result = this.getMonitorInfo(dcInfo, param);
		if(result == null) return result;
 		List<Map> list = (List)result.get(HypervisorAPI.PARAM_LIST);
		
		List objList = new ArrayList();
		onBeforeRun(param);
		for(Map m : list)
		{
			m.put("DC_ID", param.get("DC_ID"));
			setExtraInfo(m, param, dcInfo);
			insertInfo(m);
		}
		result.put("DC_ID",  param.get("DC_ID"));
		onAfter(batchService, result); 
		Map result1 = new HashMap();
		 
		return result1; 
		
	}
	protected void insertInfo(Map m) throws Exception
	{
		batchService.insertMonitorInfo(getTableNm(), m);
	}
	protected void onAfter(BatchService batchService, Map result) throws Exception{
		batchService.updateOnAfter(getTableNm(), result);
		
	}
	
	/*
	 * alaramSend
	 *    VM 경고(RED)일 경우 해당 사용자에게 메일을 보내는 함수
	 */
	public void alaramSend() {
		String alaramSendYN = "N";
		if(PropertyManager.getString("clovirsm.alaram_send_yn") != null) {
			 alaramSendYN = PropertyManager.getString("clovirsm.alaram_send_yn");
		}
		
		String title = "ALARM_MAIL_SEND_TITLE";
		String template = "alarm";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(alaramSendYN.equals("Y")) {
			List<Map> alaramList = null;
			try {
				alaramList = batchService.list("list_ALARAM_SEND",null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(alaramList.size() > 0) {
				for(int i = 0 ; i < alaramList.size() ; i++) {
					try {
						long date =  Long.parseLong((String.valueOf(alaramList.get(i).get("CRE_TIME"))));
						String creTime = formatter.format(date);
						alaramList.get(i).put("CRE_TIME", creTime);
						batchService.sendMail(String.valueOf(alaramList.get(i).get("USER_ID")), title, template, alaramList.get(i));
						Map temp = new HashMap();
						temp.put("TARGET_ID", alaramList.get(i).get("TARGET_ID"));
						temp.put("DC_ID", alaramList.get(i).get("DC_ID"));
						try {
							batchService.updateByQueryKey("update_NC_HV_ALARAM_SEND", temp);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
				}
			}
		}
	}


}
