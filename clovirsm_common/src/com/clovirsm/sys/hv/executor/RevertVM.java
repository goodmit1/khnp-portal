package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;
 


import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * 스냅샷으로 부터 VM 복구
 * @author 윤경
 *
 */
@Component
public class RevertVM extends CreateSnapshot{

 
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		IBeforeAfter before = dcInfo.getBefore( dcService  );
		Map result =  dcInfo.getAPI().revertVM(before.onAfterProcess("R", "C", param),param, info);
		 
		return result;
	}

}