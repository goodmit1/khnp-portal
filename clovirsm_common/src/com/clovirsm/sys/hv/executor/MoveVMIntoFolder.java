package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.sys.hv.DC;

@Component
public class MoveVMIntoFolder extends Common {
	public Map run(String vmId, String folder) throws Exception{
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		param.putAll( dcService.getVMCreateInfo(vmId, false));
		param.put(VMWareAPI.PARAM_FOLDER_NM, folder);
		this.run(param);
		return param;
	}
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		VMInfo info = dcService.toVMInfo(param);
		 
		dcInfo.getAPI().moveVMinfoFolder(param, info);
		return null;
	}

	 
}
