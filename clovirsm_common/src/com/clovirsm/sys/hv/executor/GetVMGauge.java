package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.VMWareCommon;
import com.clovirsm.sys.hv.DC;

/**
 * VM 현재 상태 정보 
 * @author 윤경
 *
 */
@Component
public class GetVMGauge extends Common{

	/**
	 * 
	 * @param vmId
	 * @return disk_totalgb, disk_usedgb, mem_total,disk_usage,cpu_usagemhz,mem_active,cpu_usage,mem_usage
	 * @throws Exception
	 */
	public Map run(String vmId) throws Exception
	{
		Map param = dcService.getVMInfo(vmId);
	 
		return this.run(param);
	}
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		param.put(VMWareAPI.PARAM_PERIOD, VMWareAPI.PERF_PERIOD_1H);
		return dcInfo.getAPI().getVMPerf(param);
	}

}
