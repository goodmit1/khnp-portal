package com.clovirsm.sys.hv.executor;

 
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fliconz.fw.runtime.util.DateUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.sys.hv.DC;
@Component 
public class InsertDSPerf  extends InsertAlarm {

	@Override
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		param.put(HypervisorAPI.PARAM_CRE_TIME, new Date());
		 return  dcInfo.getAPI().getDSAttribute(param);
	}
	 
	@Override
	protected String getTableNm()
	{
		return "NC_MONITOR_DS";
	}
	
	@Override
	protected void setExtraInfo(Map m, Map param, DC dcInfo) throws Exception
	{
		 
		Date date1 = (Date)param.get(HypervisorAPI.PARAM_CRE_TIME);
		m.put("REG_DT", DateUtil.format(date1, "yyyyMMdd"));
		m.put("REG_TM", DateUtil.format(date1, "HHmmss"));
		 
	}
 
	 

}
