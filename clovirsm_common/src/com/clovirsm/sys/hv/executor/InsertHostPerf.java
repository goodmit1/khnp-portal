package com.clovirsm.sys.hv.executor;

 
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fliconz.fw.runtime.util.DateUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.sys.hv.DC;
@Component("insertHostPerf")
public class InsertHostPerf  extends InsertAlarm {

	@Override
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		 return  dcInfo.getAPI().getHostAttribute(param);
	}
	protected void onBeforeRun(Map param) throws Exception{
		
	}
	@Override
	protected String getTableNm()
	{
		return "NC_MONITOR_HOST";
	}
	
	@Override
	protected void setExtraInfo(Map m, Map param, DC dcInfo) throws Exception
	{
		long time = System.currentTimeMillis();
		if(m.get(HypervisorAPI.PARAM_CRE_TIME) != null)
		{
			time = (long) m.get(HypervisorAPI.PARAM_CRE_TIME);
		}
		 
		Date date1 = new Date(time);
		m.put("REG_DT", DateUtil.format(date1, "yyyyMMdd"));
		m.put("REG_TM", DateUtil.format(date1, "HHmmss"));
		 
	}
 
	 

}
