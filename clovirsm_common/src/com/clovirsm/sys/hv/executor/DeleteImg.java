package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.common.NCConstant;
import com.clovirsm.sys.hv.DC;

/**
 * 이미지 삭제
 * @author 윤경
 *
 */
@Component
public class DeleteImg extends CreateImg{

	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		Map result = null;
		try {
			result = dcInfo.getAPI().deleteImage(param);
		}
		catch(com.clovirsm.hv.NotFoundException e) {
			
		}
		usageLogService.useEnd(NCConstant.SVC.G.toString(), (String)param.get(PARAM_IMG_ID));
		return result;
	}

	 
}
