package com.clovirsm.sys.hv.executor;

import java.util.List;
/**
 * vcenter에서 주기적으로 정보 수집 
 * @author user01
 *
 */
public interface ICollectByDC {

	public List run(String dcId ) throws Exception;
}
