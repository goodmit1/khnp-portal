package com.clovirsm.sys.hv.executor;

 
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.vmware.connection.SsoConnection.SSOLoginException;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.service.ComponentService;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;

/**
 * VM 모니터링 정보 수집
 * @author user01
 *
 */
@Component("insertVMPerf")
public class InsertVMPerf  extends InsertAlarm {

	float min_cpu = 1;
	float min_net = 0;
	int oversize_term_day=0;
	@Autowired BatchService batchService;
	@Autowired CodeHandler codeHandler;
	public InsertVMPerf()
	{
		
		
	}
	@Override  
	protected Map getMonitorInfo( DC dcInfo,Map param) throws Exception
	{
		 try {
				min_cpu = Float.parseFloat((String)ComponentService.getEnv("use_vm.cpu_min","1"));
				min_net = Float.parseFloat((String)ComponentService.getEnv("use_vm.net_min","0"));
				oversize_term_day = Integer.parseInt((String)ComponentService.getEnv("size_term_day","0"));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		BatchService batchService = (BatchService)SpringBeanUtil.getBean("batchService");
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		List<Map> list = batchService.selectList("NC_VM", param);
		batchService.deleteMonitorInfo(getTableNm(),param);
		for(Map m : list )
		{
			try
			{
				 
				m.putAll(param);
				m.put(VMWareAPI.PARAM_PERIOD,"1h");
				
				
				if(oversize_term_day>0)
				{
					m.put("PREV_DAY", oversize_term_day);
				}
				Map result = dcInfo.getAPI().getVMPerf(m);
				Map m1 = (Map)result.get("LIST");
				if( m1== null || m1.get("VM_HV_ID") == null) continue;
				m.putAll(m1);
				m.put("DC_ID", param.get("DC_ID"));
				setExtraInfo(m, param, dcInfo);
				batchService.insertMonitorInfo(getTableNm(), m);
				 
			}
			catch(Exception ignore)
			{
				if(ignore instanceof com.clovirsm.hv.NotFoundException)	{
					
				}
				else if(ignore instanceof SSOLoginException){
					throw ignore;
				}
				else {
					ignore.printStackTrace();
				}
			}
		}
		return null;
	}
	 
	@Override
	protected String getTableNm()
	{
		return "NC_MONITOR_VM";
	}
	
	@Override
	protected void setExtraInfo(Map m, Map param, DC dcInfo) throws Exception
	{
		Date date1 = new Date();
		if(m.get(HypervisorAPI.PARAM_CRE_TIME) != null)
		{
			long time = (long) m.get(HypervisorAPI.PARAM_CRE_TIME);
			date1 = new Date(time);
			m.put("REG_DT", DateUtil.format(date1, "yyyyMMdd"));
			m.put("REG_TM", DateUtil.format(date1, "HHmmss"));
			if(isUseVM(m))
			{
				 
				m.put("CRE_TIME", date1);
				batchService.updateMonitorInfo("NC_VM", m);
			}
			
			 
			
		}
		else
		{
			m.put("REG_DT", DateUtil.format(date1, "yyyyMMdd"));
			m.put("REG_TM", DateUtil.format(date1, "HHmmss"));
		}
		
	}
	protected boolean isUseVM(Map m) {
		 
		if(m==null) return false;
		try
		{
			return NumberUtil.getFloat(m.get("cpu_usage")) > min_cpu || NumberUtil.getFloat(m.get("net_usage") ) > min_net ;
		}
		catch(Exception e)
		{
			return false;
		}
		
	}
	 

}

