package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.fliconz.fm.common.util.NumberHelper;
import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.VMWareAPI;

/**
 * VM 정지
 * @author 윤경
 *
 */
@Component
public class PowerOffVM extends PowerOnVM{
	protected String getOp()
	{
		return VMWareAPI.POWER_OFF;
	}
	protected void onAfterProcess(Map param ) throws Exception
	{
		
		dcService.updateVMStop(param);
	}
}
