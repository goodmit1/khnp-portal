package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;
/**
 * 콘솔 열기
 * @author 윤경
 *
 */
@Component
public class OpenConsole extends Common {
	public String run(String vmId) throws Exception
	{
		Map param = dcService.getVMInfo(vmId);
	 
		Map result = this.run(param);
		return (String)result.get(HypervisorAPI.PARAM_CONSOLE_URL);
	}
	@Override
	protected Map run1(DC dcInfo,boolean isNew, Map param) throws Exception {
		
		VMInfo info = dcService.toVMInfo(param);
		 
		return dcInfo.getAPI().openConsole(param, info);
	}
}
