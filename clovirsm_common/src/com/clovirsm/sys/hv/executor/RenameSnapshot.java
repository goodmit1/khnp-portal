package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;
 


import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * 스냅샷명 변경
 * @author 윤경
 *
 */
@Component
public class RenameSnapshot extends Common{

	public void run(String vmId, String snapshotId, String newName) throws Exception
	{
	 
		Map snapshotInfo = dcService.getSnapshotInfo(vmId, snapshotId);
		snapshotInfo.put(HypervisorAPI.PARAM_OBJECT_NEW_NM, newName);
		this.run(snapshotInfo);
	}
 
	@Override
	protected Map run1(DC dcInfo,boolean isNew, Map param) throws Exception {
		
		VMInfo info = dcService.toVMInfo(param);
		 
		return doProcess(dcInfo, param, info);
	}
	 
 
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		return dcInfo.getAPI().renameSnapshot(param, info);
	}

}