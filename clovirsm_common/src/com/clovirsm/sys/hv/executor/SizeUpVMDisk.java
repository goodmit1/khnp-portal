package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * VM생성할 때 디스크 사이즈를 셋팅대로 늘릴 수 없어서, 템플릿의 디스크 사이즈보다 더 큰 경우 사이즈를 늘려 줌
 * @author 윤경
 *
 */
@Component
public class SizeUpVMDisk extends ReconfigVM{
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		return dcInfo.getAPI().sizeUpVMDisk(param, info);
	}
}
