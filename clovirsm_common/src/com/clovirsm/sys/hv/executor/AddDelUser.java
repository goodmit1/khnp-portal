package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.ldap.LDAPHandler;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.PropertyManager;


/**
 * 서버 계정 추가/삭제 
 * @author DT042090
 *
 */

@Component
public class AddDelUser extends  Common{

	public Map run(String dcId, String vmId, String instDt) throws Exception
	{
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		param.put("DC_ID", dcId);
		param.put("INS_DT", instDt);
		return run(param);
	}
	public void runOne( Map param) throws Exception
	{
		Map vmInfo = dcService.getVMInfo((String)param.get("VM_ID"));
		if(vmInfo == null) throw new Exception("VM not found");
		DC dcInfo = dcService.getDC((String)vmInfo.get("DC_ID"));
		if(dcInfo == null) throw new Exception("DC not found");
		param.putAll(dcInfo.getProp());
		param.put("VM_NM", vmInfo.get("VM_NM"));
		param.put("GUEST_NM", vmInfo.get("GUEST_NM"));
		if(sendShellFile(  dcInfo,  vmInfo ,   param))
		{	
			
			String cmd = getCmd(dcInfo,vmInfo, param);
			Map userInfo = dcService.getUserInfo(param.get("FW_USER_ID"));
			MapUtil.copy(userInfo, param,new String[] {"LOGIN_ID","EMAIL","USER_NAME", "TEAM_NM"});
			
			adddeluser(dcInfo, param, param, cmd);
		}
	}
	
	protected boolean sendShellFile(DC dcInfo,Map vmInfo , Map param) throws Exception
	{
		String cmd = null;
		String shellLocalPath = PropertyManager.getString("local_shell_path","d:/temp");
		String fileName = "";
		Map connMap = null;
		String delim = "/";
		if(isWindow(vmInfo))
		{
			connMap= dcInfo.getPropMap("WIN_GUEST");
			delim = "\\";
		}
		else
		{
			connMap= dcInfo.getPropMap("LINUX_GUEST");
		}
		if(connMap == null) return false;
		param.put("GUEST_ID", connMap.get("CONN_USERID"));
		param.put("GUEST_PWD", connMap.get("CONN_PWD"));
		poweronVM.run(vmInfo);
		cmd = (String)connMap.get("CONN_URL");
		if(!"Y".equals(vmInfo.get("SEND_SHELL_YN")))
		{
 			int pos = cmd.lastIndexOf(delim);
			fileName = cmd.substring(pos+1);
			param.put("FILE_PERMISSION",0755L); 
			try
			{
				 
				uploadFile(dcInfo , cmd, shellLocalPath + "/" +  fileName, param);
				VMService vmService = (VMService) NCReqService.getService("S");
				vmService.updateByQueryKey("update_SEND_SHELL_Y", param);
			}
			catch(Exception e)
			{
				System.out.println("send shell fail :" + e.getMessage());
				return false;
			}
			 
		}
		return true;
	}
	protected String getCmd(DC dcInfo,Map vmInfo,Map param) throws Exception
	{
		Map connMap=null;
		if(isWindow(vmInfo))
		{
			connMap= dcInfo.getPropMap("WIN_GUEST");
			 
		}
		else
		{
			connMap= dcInfo.getPropMap("LINUX_GUEST");
		}
		param.put("GUEST_ID", connMap.get("CONN_USERID"));
		param.put("GUEST_PWD", connMap.get("CONN_PWD"));
		param.putAll(dcInfo.getProp());
		return (String)connMap.get("CONN_URL");
	}
	 
	 
	@Autowired @Qualifier("powerOnVM") PowerOnVM poweronVM;
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		
		VMService vmService = (VMService) NCReqService.getService("S");
		Map vmInfo = vmService.selectInfo("NC_VM", param);
		 
		if(!sendShellFile(  dcInfo,  vmInfo ,   param)) return null;
		String cmd = getCmd(dcInfo,vmInfo, param);
		param.put("VM_NM", vmInfo.get("VM_NM"));
		param.put("DEPLOY_AFTER_YN", "Y");
		List<Map> userList = vmService.selectList("NC_FW_4AddDelUser", param);
		 
		for(Map m : userList)
		{
			try
			{
				adddeluser(dcInfo, m, param, cmd);
			}
			catch(Exception e)
			{
				 e.printStackTrace();
				 
				
			}
		
		}
		System.out.println("################### end add del user ");
		return null;
	}
	/**
	 * LDAP 추가
	 * @param dcInfo
	 * @param m
	 * @throws Exception
	 */
	protected void addLDAPUser(DC dcInfo,Map m) throws Exception{
		if(isWindow(m))
		{
			Map ldapProp = dcInfo.getPropMap("LDAP");
			if(ldapProp != null) {
				LDAPHandler ldap = new LDAPHandler((String)ldapProp.get("CONN_URL"), (String)ldapProp.get("CONN_USERID"), (String)ldapProp.get("CONN_PWD"));
				String customProp = (String)ldapProp.get("CONN_PROP");
				if(customProp != null && !customProp.trim().equals("")) {
					m.putAll(CommonUtil.getQueryMap(customProp));
				}
				try {
					ldap.addFMUser(m);
				}
				finally {
					ldap.close();
				}
			}
		}
	}
	protected void adddeluser(DC dcInfo, Map m, Map param, String cmd) throws Exception
	{
		
		String paramStr = null;
		
		if("N".equals(m.get("DEL_YN")))
		{
			//addLDAPUser(dcInfo, m);
			paramStr = "adduser " + m.get("LOGIN_ID");
		}
		else  
		{
			paramStr = "deluser " + m.get("LOGIN_ID");
		}
		System.out.println(paramStr);
		if(paramStr != null)
		{
			Map result = dcInfo.getAPI().guestRun(param, cmd, paramStr);
			if(NumberUtil.getInt(result.get("EXIT_CODE"),-1)==0) //성공인 경우 
			{
				m.put("ACCT_AUTO_CREATE_YN", "Y");
				
			}
			else
			{
				if(!isWindow(m))
				{
					result = dcInfo.getAPI().guestRun(param, "/usr/bin/id",  (String) m.get("LOGIN_ID"));
					if(NumberUtil.getInt(result.get("EXIT_CODE"),-1)==0) //성공인 경우 
					{
						m.put("ACCT_AUTO_CREATE_YN", "Y");
						
					}
					else
					{
						m.put("ACCT_AUTO_CREATE_YN", "N");
					}
				}
				else
				{
					m.put("ACCT_AUTO_CREATE_YN", "N");
				}
				
			}
			VMService vmService = (VMService) NCReqService.getService("S");
			vmService.updateByQueryKey("update_ACCT_AUTO_CREATE_YN", m);
		}
	}
	protected void uploadFile(DC dcInfo,String cmd,String localPath, Map param) throws Exception {
		dcInfo.getAPI().guestUpload(param, cmd, localPath);
		
	}
	private boolean isWindow(Map vmInfo) throws Exception
	{
		if(vmInfo.get("LINUX_YN") != null)
		{
			return !"Y".equals(vmInfo.get("LINUX_YN"));
		}
		else if(vmInfo.get("GUEST_NM") != null)
		{
			return (((String)vmInfo.get("GUEST_NM")).indexOf("Windows")>=0);
		}
		else
		{
			throw new Exception("not found os Info");
		}
	}

}
