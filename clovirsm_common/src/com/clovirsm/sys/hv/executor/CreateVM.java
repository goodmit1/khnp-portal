package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.common.NCException;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.sys.hv.DC;

/**
 * VM 생성
 * @author 윤경
 *
 */
@Component
public class CreateVM extends Common{

	private static final Object PARAM_SPEC_ID = "SPEC_ID";
	private static final Object PARAM_OS_ID = "OS_ID";
	

	public void run(String vmId) throws Exception
	{
		Map param = new HashMap();
		param.put(PARAM_VM_ID, vmId);
		param.put("VM_HV_ID", "");
		int row = dcService.updateByQueryKey("com.clovirsm.resources.vm.VM", "update_NC_VM_recreate", param);
		if(row>0) {
			dcService.updateByQueryKey("com.clovirsm.resources.vm.VM",  "update_NC_FW_reset_acc", param);
		}
		 
		
		param.putAll(getCreateInfo(vmId));
		
		this.run(param);
	}
	
	@Override
	protected Map run1(DC dcInfo, boolean isNew,Map param) throws Exception {
	 
	
		
		IBeforeAfter before = null;
		
		before =  dcInfo.getBefore( dcService );
		
		before.onBeforeCreateVM(isNew, param);
		// 기본 랜카드 추가 NIC_ID는 hypervisor key와 같게 해서 나중에 수동으로 ip를 바꿨을 경우 sync시킬 수 있게 함.
		 
		List<IPInfo> ipInfo = dcInfo.getBefore(dcService).getNextIp( dcInfo.getAPI().getFirstNicId(),  param);
		/*for(IPInfo ip : ipInfo)
		{
			dcService.insertNic(ip, param);
		}*/
		param.put("IP", ipInfo);
		return dcInfo.getAPI().createVM(before, isNew, param); 
		 
	}
	
	/**
	 * 생성할 VM정보
	 * @param vmId
	 * @return
	 * @throws Exception
	 */
	protected Map getCreateInfo(String vmId) throws Exception
	{
		
		Map cinfo = dcService.getVMCreateInfo(vmId, true);
		if(cinfo == null)
		{
			throw new NCException(NCException.NOT_FOUND,"VM_ID:", vmId);
		}
		setCopyInfo(cinfo);
		String[] dsNames = dcService.getDsName(cinfo);
		cinfo.put(HypervisorAPI.PARAM_DATASTORE_NM_LIST, dsNames);		
		return cinfo;
	}

	protected void setCopyInfo(Map cinfo ) throws Exception
	{
		 
		String fromId = (String)cinfo.get(Common.PARAM_FROM_ID);
		if(  fromId != null && !"".equals(fromId))
		{
			Map info = dcService.getVMCopyInfo(fromId);
			 
			if(info == null)
			{
				throw new NCException(NCException.NOT_FOUND,"VM_ID or IMG_ID", fromId);
			}
			String fromNm = (String)info.get("IMG_NM");
			cinfo.put(HypervisorAPI.PARAM_FROM,  fromNm);
			
		}
	}
}
