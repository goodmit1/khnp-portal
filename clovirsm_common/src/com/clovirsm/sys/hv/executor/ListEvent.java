package com.clovirsm.sys.hv.executor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * 이벤트 목록
 * @author 윤경
 *
 */
@Component
public class ListEvent extends   Common{

	 
	public Map run(String dc, String type, String name) throws Exception
	{
		Map param = new HashMap();
		param.put(PARAM_DC_ID, dc);
		if(type.equals(HypervisorAPI.OBJ_TYPE_VM))
		{
			/*Map vmInfo = null;
			try	{
				vmInfo = dcService.getVMInfo(name);
			}catch(Exception ignore){
				param.put("VM_NM", name);
			}
			if(vmInfo != null) param.putAll(vmInfo);
			*/
			param.put("VM_NM", name);
		}
		param.put(HypervisorAPI.PARAM_OBJECT_NM, name);
		
	 
		
		param.put(HypervisorAPI.PARAM_OBJECT_TYPE, type);
		
		return this.run(param);
		
	}
 
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		
		return doProcess(dcInfo,param );
	}
	protected Map doProcess(DC dcInfo, Map param) throws Exception 
	{
		return dcInfo.getAPI().listEvent(param );
	}
}
