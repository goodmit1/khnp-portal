package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.common.NCConstant;
import com.clovirsm.hv.HypervisorException;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.sys.hv.DC;
/**
 * VM 삭제
 * @author 윤경
 *
 */
@Component
public class DeleteVM extends ReconfigVM{
	@Autowired IPService ipService;
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		try
		{
			Map result = dcInfo.getAPI().deleteVM(param, info);
		}
		catch(NotFoundException e)
		{
			
		}
		param.put("VM_NM", info.hostName);
		dcInfo.getBefore(dcService).onAfterDeleteVM(param);
		dcService.deleteDiskByVM(param); // 추가 디스크 제거
		ipService.cancelIp(param); // 랜카드 제거
		usageLogService.useEnd(NCConstant.SVC.S.toString(),(String) param.get(PARAM_VM_ID));
		
		return null;
		 
	}
}
