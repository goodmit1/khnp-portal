package com.clovirsm.sys.hv.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.common.IPAddressUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.NotFoundException;
import com.clovirsm.hv.VMInfo;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.service.resource.DiskService;
import com.clovirsm.service.resource.IPService;
import com.clovirsm.sys.hv.DC;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
 
@Component
public class GetVMInfos extends Common{
	 
	@Autowired DiskService diskService;
	@Autowired IPService ipService;
	
	
	public void runInThread(long delay, String vmId) 
	{
		Thread thread = new Thread()
				{
					public void run()
					{
						try {
							Thread.sleep(delay);
						
							GetVMInfos infos = (GetVMInfos)SpringBeanUtil.getBean("getVMInfos");
							infos.run(true,   vmId );
						} catch ( Exception e) {
							 
							e.printStackTrace();
						}
					}
				};
		thread.start();		
	}
	public void runInThread(long delay, List<Map> vmInfos) 
	{
		Thread thread = new Thread()
				{
					public void run()
					{
						try {
							
						
							
							if(vmInfos.size()>0) {
								Thread.sleep(delay);
								GetVMInfos infos = (GetVMInfos)SpringBeanUtil.getBean("getVMInfos");
								infos.run(true, (String)vmInfos.get(0).get("DC_ID"), (String)vmInfos.get(0).get("COMP_ID"),vmInfos.get(0).get("TEAM_CD"),   vmInfos) ;
							}
							
							
						} catch ( Exception e) {
							 
							e.printStackTrace();
						}
					}
				};
		thread.start();		
	}
	public Map run(boolean isAll, String vmId ) throws Exception
	{
		Map vmInfo = dcService.getVMInfo(vmId);
		return this.run( isAll, (String) vmInfo.get("DC_ID"),  (String)vmInfo.get("COMP_ID"), vmInfo.get("TEAM_CD"), vmInfo);
	}

	public Map run(boolean isAll, String dc, String compId, Object teamCd,  Map  vmInfo ) throws Exception
	{
		
		List<Map> vmInfos = new ArrayList();
		vmInfos.add(vmInfo);
		Map result =  this.run(isAll, dc, compId, teamCd, vmInfos );
		return (Map)result.get(vmInfo.get(HypervisorAPI.PARAM_VM_NM));
	}
	public Map run(boolean isAll,String dc, String compId, Object teamCd, List<Map> vmInfos) throws Exception
	{
		String[] hostNames = new String[vmInfos.size()];
		int idx=0;
		for(Map m : vmInfos)
		{
			hostNames[idx++] = (String) m.get(HypervisorAPI.PARAM_VM_NM);
		}
		Map param = new HashMap();
		param.put(PARAM_DC_ID, dc);
		param.put(PARAM_COMP_ID, compId);
		param.put(PARAM_TEAM_CD, teamCd);
		param.put("VM_NM_LIST", hostNames);
		if(isAll) param.put("ALL_YN", "Y");
		try
		{
			Map<String, Map> result = super.run(param);
			Set<String> hosts = result.keySet();
			for(Map m : vmInfos)
			{
				m.put("DC_ID", dc);
				String host = (String) m.get(HypervisorAPI.PARAM_VM_NM);
				Map newInfo = result.get(host);
				String newRunCd = (String)newInfo.get(HypervisorAPI.PARAM_RUN_CD);
				m.putAll(newInfo);
				List<Map> diskList = (List)newInfo.get("DISK_LIST");
				if(diskList != null)
				{
					diskService.syncDiskInfo( m, diskList);
				}
				if(!newRunCd.equals(m.get(HypervisorAPI.PARAM_RUN_CD))) 
				{
					m.put(HypervisorAPI.PARAM_RUN_CD, newRunCd);
					m.put("FAIL_MSG", "");
					if(newRunCd.equals(HypervisorAPI.RUN_CD_RUN))
					{
						dcService.updateVMStart(m);
					}
					else
					{
						dcService.updateVMStop(m);
					}
					
				}
				else
				{
					dcService.updateByQueryKey("update_NC_VM", m);
				}
				Map<Integer, String> ipList = (Map)newInfo.get(HypervisorAPI.PARAM_IP_LIST);
				if(ipList != null && ipList.size()>0)
				{	
					
					Map<Integer, String> macInfo = (Map)newInfo.get("MAC_LIST");
					int count = 0;
				 
					ipService.beforeVMCollect(m);
					
					for(Integer confId : ipList.keySet())
					{
						String ip = ipList.get(confId);
						
						if(ip != null   && confId>0 && !"".equals(ip))
						{
							m.put("MAC_ADDR", macInfo.get(confId));
							count += ipService.ipUsed(m, ip);
							
							
						}
					}
					ipService.afterVMCollect(count>0, m);
				}
				
				
				
			}
			dcService.getDC(dc).getBefore(dcService).onAfterIpCollect();
			return result;
		}
		catch(NotFoundException e)
		{
			if(vmInfos.size()==1)
			{
				Map m = vmInfos.get(0);
				m.put("RUN_CD", "F");
				m.put("FAIL_MSG", "vm not found");
				dcService.updateVMStop(m);
			}
			throw e;
		}
		
	}
	 
	 
	
	@Override
	protected Map run1(DC dcInfo, boolean isNew, Map param) throws Exception {
		return dcInfo.getAPI().vmState(param,(String[]) param.remove("VM_NM_LIST"));
	}
}
