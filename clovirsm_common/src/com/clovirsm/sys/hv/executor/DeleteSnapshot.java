package com.clovirsm.sys.hv.executor;

import java.util.Map;

import org.springframework.stereotype.Component;
 
import com.clovirsm.hv.VMInfo;
import com.clovirsm.sys.hv.DC;

/**
 * 스냅샷 삭제
 * @author 윤경
 *
 */
@Component
public class DeleteSnapshot extends CreateSnapshot{

 
	protected Map doProcess(DC dcInfo, Map param,VMInfo info ) throws Exception
	{
		return dcInfo.getAPI().deleteSnapshot(param, info);
	}

}