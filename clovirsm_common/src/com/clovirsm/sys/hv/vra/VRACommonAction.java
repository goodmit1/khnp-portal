package com.clovirsm.sys.hv.vra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.common.NCException;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorException;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.Common;

public abstract class VRACommonAction {
	@Autowired
	DCService dcService;
	Logger log = LoggerFactory.getLogger(Common.class);
	
	
	public static VRAAPI getAPI() throws Exception
	{
		VMWareAPI vmwareApi = new VMWareAPI();
		return vmwareApi.getVRA();
	}
	public static VRAAPI getAPI(Map param) throws Exception
	{
		if(param.get("CONN_ID") == null) {
			throw new Exception("연결정보가 없습니다.");
		}
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		VRAAPI vmwareApi = getAPI();
		Map p = dcService.getEtcConnInfo("VRA", param.get("CONN_ID"));
		if(p != null) {
			param.putAll(p);
		}
		return vmwareApi;
	}
	public Map runAll(Map param) throws Exception
	{
		List list = new ArrayList();
		List<Map> connList = dcService.getEtcConnInfo("VRA");
		for(Map m:connList) {
			VRAAPI vmwareApi = getAPI();
			param.putAll(m);
			Map r = run1(vmwareApi,   param);
			List<Map> rList = (List)r.get("LIST");
			if(rList != null) {
				for(Map m1:rList) {
					m1.put("CONN_ID", m.get("CONN_ID"));
					list.add(m1);
				}
			}
		}
		Map result = new HashMap();
		result.put("LIST", list);
		return result;
	}
	public Map run(Map param) throws Exception
	{
		try
		{
		 
			 
			
			VRAAPI api = getAPI(param);
			 
			 
			return run1(api,   param);
		}
		catch(Exception e)
		{
			log.error("hv api error :" + this.getClass().getName() + ":" + param + ":" + e.getMessage());
			e.printStackTrace();
			if(e instanceof HypervisorException)
			{
				throw new NCException(((HypervisorException)e).getMsgParams());
			}
			throw e;
			//logging
		}
	}

	public DCService getDCService()
	{
		return dcService;
	}
	protected abstract Map run1(VRAAPI api,   Map param) throws Exception;
}
