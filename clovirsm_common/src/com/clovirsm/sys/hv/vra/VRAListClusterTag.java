package com.clovirsm.sys.hv.vra;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.service.admin.DcKubunConfService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;

@Component
public class VRAListClusterTag extends VRACommonAction {

	@Override
	protected Map run1(VRAAPI api, Map param) throws Exception {
		Map result =  api.getListClusterTag(param);
		
		return result;
	}
	@Autowired DcKubunConfService mapService;
	@Autowired
	DCService dcService; 
	public void insertAll(Map result) throws Exception {
		List<Map> list = (List)result.get("LIST");
		Set<String> dcConnMap = new HashSet(); 
		Set<String> connSet = new HashSet();
		for(Map m : list) {
				DC dc = dcService.getDCByIp(m);
				if(dc == null) {
					System.out.println("DC IS NULL" + m);
					continue;
				}
				else {
					m.put("DC_ID", dc.getProp("DC_ID"));
					if(m.get("TAG") != null) {
						
						m.put("OBJ_TYPE_NM", "ClusterComputeResource");
						dcService.updateByQueryKey("update_TAG_NC_HV_OBJ", m);
					}
					dcConnMap.add(  (String)m.get("CONN_ID")   +"," + (String)m.get("DC_ID"));
					connSet.add((String)m.get("CONN_ID"));
					 
				}
		}
		for(String conn:connSet) {
			mapService.delete(null,conn,"DC_ETC_CONN", null);
		}
		for(String conn:dcConnMap) {
			String[] arr = conn.split(",");
			mapService.insert(arr[1],arr[0],"DC_ETC_CONN", null, null);
		 
		}
	}
	
}
