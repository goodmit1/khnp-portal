package com.clovirsm.sys.hv;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.common.util.CodeHelper;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.ISite;
import com.clovirsm.common.ClassHelper;
import com.clovirsm.hv.HVProperty;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.sys.hv.vmware.BeforeAfterVMWare;
/**
 * 데이터 센터 정보
 * @author 윤경
 *
 */
public class DC {

	Map etcProducts;
	public String hvType;
	Map<String,Object> prop;
	HypervisorAPI api;
	
	public Map getEtcGoods()
	{
		if(etcProducts==null)
		{
			CodeHandler hdr = (CodeHandler)SpringBeanUtil.getBean("codeHandler");
			etcProducts = hdr.getCodeList("HV_ETC_PRODUCT_" + hvType, "ko");
		}
		return etcProducts;
	}
	public DC(  String hvType ) throws Exception
	{
		prop = new HashMap();
		
		
		this.hvType = hvType;
		api = getAPI(  hvType);
	}
	
	/**
	 * Network 자동화 여부 
	 * @return
	 */
	public boolean isNwAuto()
	{
		return "Y".equals(prop.get("NW_AUTO_YN"));
	}
	
	/**
	 * VLAN 사용 여부
	 * @return
	 */
	public boolean useVLAN()
	{
		return "Y".equals(prop.get("USE_VLAN_YN"));
	}
	/**
	 * 리소스 풀 사용 여부
	 * @return
	 */
	public boolean useRP()
	{
		return "Y".equals(prop.get("USE_RP_YN"));
	}
	public void addProp(Map prop1)
	{
		prop.putAll(prop1);
	}
	public HypervisorAPI getAPI()
	{
		return api;
	}
	public static HypervisorAPI getAPI(String hvType) throws Exception
	{
		return  HVProperty.getInstance().getAPI(hvType);
	}
	public  static IBeforeAfter  getBefore(   ) throws Exception{
		BeforeAfterVMWare site = (BeforeAfterVMWare)ClassHelper.getSite();
	 
		 
		return site;
	}
	public IBeforeAfter getBefore(  DCService dcservice ) throws Exception
	{
		BeforeAfterVMWare site = (BeforeAfterVMWare)ClassHelper.getSite();
		site.setService(dcservice);
		site.setDcInfo(  this);
		return site;
		 
	}
	
	public Map getProp()
	{
		return prop;
	}
	public void putProp(String key, String val)
	{
		prop.put(key, val);
	}
	public void putProp(String key, Map val)
	{
		prop.put(key, val);
	}
	public String getProp(String key )
	{
		return (String)prop.get(key );
	}
	public Map getPropMap(String key )
	{
		return (Map)prop.get(key );
	}
}
