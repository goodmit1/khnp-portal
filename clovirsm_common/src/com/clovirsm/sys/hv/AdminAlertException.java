package com.clovirsm.sys.hv;

public class AdminAlertException extends Exception {

	public AdminAlertException(String msg) {
		super(msg);
	}

}
