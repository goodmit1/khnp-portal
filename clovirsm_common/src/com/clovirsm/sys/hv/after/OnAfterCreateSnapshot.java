package com.clovirsm.sys.hv.after;

import java.util.Map;

import com.clovirsm.common.NCConstant;
import com.clovirsm.service.TaskService;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class OnAfterCreateSnapshot extends OnAfterCreateSM{
	public OnAfterCreateSnapshot(  Map param)
	{
		super(param);
	}
	protected String getTableNm() {
		 
		return "NC_SNAPSHOT";
	}

	@Override
	public void onAfterSuccess(String taskId) throws Exception {
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		Map result = dcService.getDC((String)param.get("DC_ID")).getAPI().getSnapshotSize(param);
		param.putAll(result);
		super.onAfterSuccess(taskId);
	}
	protected String getSvcCd()
	{
		return NCConstant.TASK_CD.T.toString();
	}
	protected String getSvcId()
	{
		return (String)param.get("VM_ID");
	}
}
