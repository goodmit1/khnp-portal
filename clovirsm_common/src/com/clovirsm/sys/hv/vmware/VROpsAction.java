package com.clovirsm.sys.hv.vmware;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.hv.vmware.vra.VRAAPI;
import com.clovirsm.hv.vmware.vrops.VROpsAPI;
import com.clovirsm.service.ComponentService;
import com.clovirsm.service.batch.BatchService;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fm.common.util.ArrayUtil;
import com.fliconz.fw.runtime.util.DateUtil;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.ibm.icu.util.Calendar;

@Component
public class VROpsAction {
	
	Map metricAllMap;
	 
	public void init() {
		metricAllMap = new HashMap();
		 
		try {
			JSONParser jsonParser = new JSONParser();
			JSONArray jsonArray = (JSONArray) jsonParser.parse(new InputStreamReader(VROpsAction.class.getClassLoader().getResourceAsStream("config/vrops_statKeys.json") ,"UTF-8"));
			
			
			String[] keys = PropertyManager.getStringArray("VROPS.statKeys");
			for(int i=0; i < jsonArray.size(); i++) {
				JSONObject json = (JSONObject)jsonArray.get(i);
				metricAllMap.put(json.get("ops:key"), MapUtil.jsonToMap(json.toString()));
				 
			}
			 
		} catch (Exception e) {
		 
			e.printStackTrace();
		}
		
	}
	public static VROpsAPI getAPI(Map param) throws Exception
	{
		VMWareAPI vmwareApi = new VMWareAPI();
		VROpsAPI api =  vmwareApi.getVROps();
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		param.putAll(dcService.getEtcConnInfoByDC("VROPS", param.get("DC_ID")));
		 
		//param.put("SOURCE", PropertyManager.getString("VROPS.SOURCE"));
		
		 
		return api;
	}
	 
	public List getOverUnderSize() throws Exception{
		Map param = new HashMap();
		VROpsAPI api = getAPI(param);
		return api.getUnderOverSize(param, NumberUtil.getInt(ComponentService.getEnv("size_term_day","1")));
	}
	
	public Map getMetricMeta(Map param) throws Exception{
		VROpsAPI api = getAPI(param);
		if(metricAllMap == null) {
			init();
		}
		return api.getResourceMetricKeys(param, metricAllMap, (String)param.get("VM_NM"),  (String)param.get("VM_HV_ID"));
		
	}
	private String getIntervalType(String period) {
		String result = "DAYS";
		if("1h".equals(period)) {
			result = "MINUTES";
		} 
		else if("1d".equals(period)) {
			result = "HOURS";
		} 
		else if("1y".equals(period)) {
			result = "MONTHS";
		}
		return result;
	}
	private long getBeforeTime(String period) {
		Calendar cal = Calendar.getInstance();
		int kubun = Calendar.DATE;
		if("1h".equals(period)) {
			kubun = Calendar.HOUR;
			cal.add(kubun, -1*1);
		} 
		else if("1w".equals(period)) {
			kubun = Calendar.DATE;
			cal.add(kubun, -1*7);
		}
		else if("1m".equals(period)) {
			kubun = Calendar.MONTH;
			cal.add(kubun, -1*1);
		}
		else if("1y".equals(period)) {
			kubun = Calendar.YEAR;
			cal.add(kubun, -1*1);
		}
		else if("1d".equals(period)) {
			kubun = Calendar.DATE;
			cal.add(kubun, -1*1);
		}
		return cal.getTimeInMillis();
	}
	public Map getMetricData(Map param) throws Exception{
		VROpsAPI api = getAPI(param);
		String startDt = (String)param.get("START_DT");
		long begin = 0;
		long end = System.currentTimeMillis();
		String intervalType = "DAYS";
		if(startDt != null) {
			begin = DateUtil.toDate(startDt, "yyyy-MM-DD HH:mm").getTime();
		}
		else {
			begin =getBeforeTime((String)param.get("PERIOD"));
			intervalType = getIntervalType((String)param.get("PERIOD"));
		}
		String finishDt = (String)param.get("FINISH_DT");
		if(finishDt != null) {
			end = DateUtil.toDate(finishDt, "yyyy-MM-DD HH:mm").getTime();
		}
		Map result =  api.getMetricData(param, (String)param.get("resourceId"), (String)param.get("statKeys"), intervalType, begin, end);
		 
		return result;
		
	}
	
	@Autowired BatchService batchService;
	public void insertDCHealth() throws Exception{
		Map param = new HashMap();
		VROpsAPI api = getAPI(param);
		insertMonotirEtc("DC_HEALTH",api.getAllDCHeathInfo(param).toString());
	}
	
	protected void insertMonotirEtc(String id, String data) throws Exception {
		Map param = new HashMap();
		param.put("ID", id);
		batchService.deleteMonitorInfo("NC_MONITOR_ETC", param);
		param.put("DATA", data);
		batchService.insertMonitorInfo("NC_MONITOR_ETC", param);
	}
}
