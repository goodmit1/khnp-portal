package com.clovirsm.sys.hv.vmware;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.clovirsm.hv.RestClient;
import com.clovirsm.hv.vmware.log.LogInsightAPI;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.ibm.icu.util.Calendar;

public class VMLogHandler extends TextWebSocketHandler {
	boolean isClose = false;
	String target;
	String field;
	String dcId;
	long startTime = 0l;
	long lastTime = 0l;
	LogInsightAPI api;
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		String msgJson = message.getPayload();
		System.out.printf("%s로 부터 %s받음", session.getId(), msgJson);
		JSONObject json = new JSONObject(msgJson); 
		field = json.getString("field");
		target = json.getString("target");
		dcId = json.getString("dcId");
		if(json.getString("startTime")==null)
		{
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DATE, -1);
			startTime = cal.getTimeInMillis();
		}
		reciveMsg(session);
	}
	
	protected void reciveMsg(WebSocketSession session) throws Exception {
		
		
		
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		Map param = dcService.getEtcConnInfoByDC("VRLI", dcId );
		LogInsightAPI api = new LogInsightAPI();
		api.connect(param); 
		long now  = System.currentTimeMillis(); 
		while(session.isOpen()) {
			List<Object[]> list = api.getLog(param, field,target, startTime, lastTime);
			for(Object[] t:list) {
				session.sendMessage(new TextMessage(t[0] + " " + t[1]));
				startTime = ((Date) t[0]).getTime();
			}
			startTime += 1;
			if(now < startTime) {
				Thread.sleep(5000);
			}
			
		}
		
//		RestClient client = new RestClient("http://115.71.42.53:9200");
//		client.setMimeType("application/json");
//		JSONObject json = new JSONObject();
//		JSONObject timestamp = (new JSONObject()).put("gt", "2019-08-28T05:00:02.453Z");
//		json.put("query", ( new JSONObject()).put("range", ( new JSONObject()).put("@timestamp", timestamp)));
//		json.put("sort" , (new JSONArray()).put("@timestamp"));
//		while(session.isOpen()) {
//			JSONObject res = (JSONObject)client.get("/logstash-*/_search?pretty", json.toString());
//			JSONArray hits = res.getJSONObject("hits").getJSONArray("hits");
//			
//			for(int i=0; i < hits.length(); i++) {
//				JSONObject source = hits.getJSONObject(i).getJSONObject("_source");
//				session.sendMessage(new TextMessage(source.getString("message")));
//				timestamp.put("gt", source.getString("@timestamp"));
//						
//			}
//			Thread.sleep(1000);
//			
//		}
	}
	 
}
