package com.clovirsm.service.resource;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.CIDR;
import com.clovirsm.common.IPAddressUtil;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.sys.hv.NextIPHelper;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.NumberUtil;
 

@Service
public class IPService extends NCDefaultService {

	@Override
	protected String getTableName() {
 
		return "NC_IP";
		
	}

	@Override
	public String[] getPks() {
		 
		return new String[]{"IP"};
	}

	@Override
	protected String getNameSpace() {
		 
		return "com.clovirsm.resources.ip";
	}
	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("STATUS", "IP_STATUS");
		config.put("P_KUBUN", "P_KUBUN");
		config.put("PURPOSE", "PURPOSE");
		return config;
	}
	public void insertVMIp(Map param) throws Exception {
		String ip = (String)param.get("IP");
		
		param.put("IP_NUM", IPAddressUtil.ipToLong(ip));
		int row = this.update(param);
		if(row == 0) {
			this.insert(param);
		}
		
	}
	public void insertAllIp(String nwId, String cidrs) throws Exception {
		Map<String, Object> param = new HashMap();
		param.put("NW_HV_ID", nwId);
		if(cidrs == null || "".equals(cidrs.trim()))	{
			this.updateByQueryKey("clear_NW", param);
			return;
 
		}
		boolean isGateWayLastIp = CIDR.isGatewayLastIp();
		String[] cidr_arr = cidrs.split(",");
	
		param.put("STATUS", "N");
		for(String cidr1:cidr_arr)	{
			CIDR cidr = new CIDR(cidr1);
			
			param.put("START_IP", IPAddressUtil.longToIp(cidr.getNthIpNum(1)));
			param.put("FINISH_IP",IPAddressUtil.longToIp(cidr.lastHostIpNum()));
			Integer count_old = (Integer)this.selectOneObjectByQueryKey("count_bycidr", param);
			int count = cidr.getAllIpCnt();
			if(count_old == count) continue;
			String gateWay = cidr.getGateWay();
			for(int i=1; i<=count;i++)	{
				long ipNum = cidr.getNthIpNum(i);
				String ip = IPAddressUtil.longToIp(ipNum);
				
				param.put("IP", ip);
				param.put("IP_NUM", new Long(ipNum));
				param.put("STATUS", "N"); //사용 가능
				param.put("RSN", ""); 
				 
				if(ip.equals(gateWay)){
					param.put("STATUS", "V"); //사용 불가
					param.put("RSN", "gateway"); //사용 불가
				}
				try {  
					int row = this.insert(param);
				}catch(Exception e) {
					System.out.println(e.getMessage());
				}
				 
				 
			}
		}
	}
	public void beforeVMCollect(Map param) throws Exception {
		 // T로 바꿘다
		if(param.get("VM_HV_ID") == null) {
			throw new Exception("VM_HV_ID is null");
		}
		updateByQueryKey("update_STATUS_T", param);
	}
	public void afterVMCollect(boolean hasIp , Map param) throws Exception {
		if(hasIp) { // 예약 , T 모두 미사용으로
			 updateByQueryKey("update_STATUS_RT_Cancel", param);
		}
		else {
			 // T를 다시 U로 바꾼다.
			 param.put("STATUS", "U");
			 
			 updateByQueryKey("update_STATUS", param);
		}
		
	}
	public static boolean ping(String ip)
	{
		try
		{
			InetAddress pingcheck = InetAddress.getByName(ip);
			return pingcheck.isReachable(  1000);
		}
		catch(Exception ignore)
		{
			ignore.printStackTrace();
			return false;
		}
	}
	public int cancelReserveIp( String vmId ) throws Exception {
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		return this.updateByQueryKey("update_STATUS_R_Cancel", param);
	}
	public String getReserveNextIp(  Map vmInfo,  long startIpNum, long finishIpNum) throws Exception {
		Map param = new HashMap();		 
		param.put("VM_ID", vmInfo.get("VM_ID"));
		param.put("START_IP_NUM", startIpNum);
		param.put("FINISH_IP_NUM", finishIpNum);
		Map ipInfo = this.selectOneByQueryKey("getNextIp", param);
		if(ipInfo==null || ipInfo.size()==0) return null;
		String ip = (String)ipInfo.get("IP");
		boolean isPing = ping(ip);
		if(isPing) {
			setPing(ip);
			return getReserveNextIp( vmInfo, startIpNum, finishIpNum);
		}
		if(NumberUtil.getInt(ipInfo.get("IP_NUM"))==0) {
			return ip;
		}
		param.put("IP", ip);
		
		param.put("VM_NM", vmInfo.get("VM_NM"));
		param.put("DC_ID", vmInfo.get("DC_ID"));
		
		int row = this.updateByQueryKey("update_STATUS_R", param);
		if(row==0){
			return getReserveNextIp( vmInfo, startIpNum, finishIpNum);
		}
		else {
			return ip;
		}
	}
	public void setPing(String ip) throws Exception
	{
		Map param = new HashMap();
		param.put("IP", ip);
		param.put("RSN", "Ping");
		param.put("STATUS", "V");
		this.updateDBTable("NC_IP", param);
	}
	public int ipUsed(Map param, String ip) throws Exception {
		param.put("IP", ip);
		param.put("IP_NUM", IPAddressUtil.ipToLong(ip));
		param.put("STATUS", "U");
		
		return update(param);
		
	}
	@Override
	public List list(Map searchParam) throws Exception {
		if(!TextHelper.isEmpty((String)searchParam.get("START_IP"))){
			searchParam.put("START_IP_NUM", IPAddressUtil.ipToLong((String)searchParam.get("START_IP")));
		}
		if(!TextHelper.isEmpty((String)searchParam.get("FINISH_IP"))){
			searchParam.put("FINISH_IP_NUM", IPAddressUtil.ipToLong((String)searchParam.get("FINISH_IP")));
		}
		return super.list(searchParam);
	}

	@Override
	protected int insertDBTable(String tableNm, Map param) throws Exception {
		if(!param.containsKey("IP_NUM")) {
			String ip = param.get("IP").toString();
			long ipNum = IPAddressUtil.ipToLong(ip);
			param.put("IP_NUM", ipNum);
		}
		return super.insertDBTable(tableNm, param);
	}

	public int insertOrUpdate(Map param) throws Exception{
		String ip = (String)param.get("IP");
		int pos = ip.indexOf("-");
		if(pos<0)
		{		
			return super.insertOrUpdate(param);
		}
		{
			String startIp = ip.substring(0, pos);
			int pos1 = startIp.lastIndexOf(".");
			String lastIp = startIp.substring(0, pos1 + 1) + ip.substring(pos+1);
			long startIpNum = IPAddressUtil.ipToLong(startIp);
			long lastIpNum = IPAddressUtil.ipToLong(lastIp);
			int row = 0;
			for(long ip1 = startIpNum; ip1<=lastIpNum; ip1++)
			{
				param.put("IP_NUM", ip1); 
				param.put("IP", IPAddressUtil.longToIp(ip1));
				row+=this.insertOrUpdate(param);
			}
			return row;
		}
	}
	public void cancelIp(Map vmInfo) throws Exception {
		this.updateByQueryKey("update_STATUS_R_Cancel", vmInfo);
		
	}
	

}
