package com.clovirsm.service.resource;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.IAsyncJob;
import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.common.NCException;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.service.ComponentService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.CreateSnapshot;
import com.clovirsm.sys.hv.executor.DeleteSnapshot;
import com.clovirsm.sys.hv.executor.RenameSnapshot;
import com.clovirsm.sys.hv.executor.RevertVM;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class SnapshotService extends NCDefaultService implements IAsyncJob{

	@Autowired
	transient RenameSnapshot renameSnapshot;

	@Autowired
	transient CreateSnapshot createSnapshot;

	@Autowired
	transient DeleteSnapshot deleteSnapshot;

	@Autowired
	transient RevertVM revertVM;

	protected String getNameSpace() {
		return "com.clovirsm.resources.snapshot.Snapshot";
	}
	Map codeMap=null;
	
	
	@Override
	public IAfterProcess getAfter(  String id) throws Exception {
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		Map param = dcService.getSnapshotInfo(id, null);
		if(param == null) return null;
	 
		DC dcInfo = dcService.getDC((String)param.get("DC_ID"));
		param.putAll(dcInfo.getProp());
		return dcInfo.getBefore(dcService  ).onAfterProcess("T", "C", param);
		 
	}
	@Override
	protected Map<String,String> getCodeConfig()
	{
		if(codeMap == null)
		{
			codeMap = new HashMap();
			codeMap.put("RUN_CD", "RUN_CD");
			codeMap.put("TASK_STATUS_CD", "TASK_STATUS_CD");
		}
		return codeMap;
	}
	public void revertVMStart(String vm, String snapId) throws Exception
	{
		Map param = new HashMap();
		param.put("VM_ID", vm);
		param.put("SNAPSHOT_ID", snapId);
		param.put("TASK_STATUS_CD", NCConstant.TASK_STATUS_CD.W.toString());
		this.insertDBTable("NC_REVERT_LOG", param);
		revertVM.run(vm, snapId);
	}
	@Override
	protected int insertDBTable(String tableNm, Map param) throws Exception {
		if(!"NC_REVERT_LOG".equals(tableNm)){
			chkDuplicate(tableNm, param,"SNAPSHOT_NM");
			int max = Integer.parseInt((String)ComponentService.getEnv("snapshot.max","0"));
			if(max>0)
			{
				int num = super.selectCountByQueryKey("list_count_NC_SNAPSHOT", param);
				if(num>=max)
				{
					throw new Exception("The maximum number(" + max + ") has been exceeded.");
				}
			}
		}

		if("NC_SNAPSHOT".equals(tableNm)){
			param.put("SNAPSHOT_ID", IDGenHelper.getId(""));
		}
		int row = super.insertDBTable(tableNm, param);
		if(row == 1){
			if("NC_SNAPSHOT".equals(tableNm)){
				createSnapshot.run((String)param.get("VM_ID"), (String)param.get("SNAPSHOT_ID"));
			}
		}
		return row;
	}

	@Override
	protected int updateDBTable(String tableNm, Map param) throws Exception {

		if("NC_SNAPSHOT".equals(tableNm)){
			//isDuplicate 체크
			chkDuplicate(tableNm, param,"SNAPSHOT_NM");
			if(isChangeValue(param, "SNAPSHOT_NM")){
				renameSnapshot.run((String)param.get("VM_ID"), (String)param.get("SNAPSHOT_ID"), (String)param.get("SNAPSHOT_NM"));
			}
		}
		return super.updateDBTable(tableNm, param);
	}

	public void removeSnapshot(Map param) throws Exception
	{
		deleteDBTable("NC_SNAPSHOT", param) ;
	}
	@Override
	protected int deleteDBTable(String tableNm, Map param) throws Exception {
		if("NC_SNAPSHOT".equals(tableNm)){
			deleteSnapshot.run((String)param.get("VM_ID"), (String)param.get("SNAPSHOT_ID"));
		}
		return super.deleteDBTable(tableNm, param);
	}
	@Override
	public String[] getPks() {
		 
		return new String[]{"SNAPSHOT_ID"};
	}
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return "NC_SNAPSHOT";
	}
}