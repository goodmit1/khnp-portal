package com.clovirsm.service.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.IAsyncJob;
import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.CreateDisk;
import com.clovirsm.sys.hv.executor.DeleteDisk;
import com.clovirsm.sys.hv.executor.MountDisk;
import com.clovirsm.sys.hv.executor.UnmountDisk;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

@Service
public class DiskService extends NCReqService implements IAsyncJob{

	@Autowired
	transient CreateDisk createDisk;

	@Autowired
	transient MountDisk mountDisk;


	@Autowired
	transient UnmountDisk unmountDisk;


	@Autowired
	transient DeleteDisk deleteDisk;
	
	@Autowired
	transient DCService dcService;

	protected String getNameSpace() {
		return "com.clovirsm.resources.disk.Disk";
	}
	@Override
	public IAfterProcess getAfter( String id) throws Exception {
		Map param = dcService.getDiskInfo(id);
		if(param == null) return null;
	 
		DC dcInfo = dcService.getDC((String)param.get("DC_ID"));
		param.putAll(dcInfo.getProp());
		return dcInfo.getBefore(dcService  ).onAfterProcess("D","C", param);
		 
	}
	public void syncDiskInfo( Map vmInfo, List<Map> diskList) throws Exception
	{
		
		// UPDATE NC_DISK SET DEL_YN='Y' where VM_ID=#{VM_ID}
		this.updateByQueryKey("update_delYN", vmInfo);
		for(int i=diskList.size()-1; i>=0; i--)
		{
			Map m = diskList.get(i);
			m.put("VM_ID", vmInfo.get("VM_ID"));
			m.put("DS_NM", getDS( (String)m.get("DISK_PATH")));
			/*if(m.get("DISK_NM")==null) {
				m.put("DISK_NM", "HDD" + (i+1));
			}
			*/
			int row = this.updateByQueryKey("update_by_DISK_PATH", m);
			
			if(row==0)
			{
				row = this.updateByQueryKey("update_DISK_PATH_bySize", m);
				// UPDATE NC_DISK SET DISK_NM=#{DISK_NM}, DISK_PATH=#{DISK_PATH} where DISK_ID= (SELECT DISK_ID FROM NC_DISK where VM_ID=#{VM_ID} AND DISK_SIZE=#{DISK_SIZE} limit 1)
				if(row == 0)
				{
					this.insertDiskBySync(vmInfo, m);
				}
			}
		}
		this.updateByQueryKey("delete_NC_DISK_afterSync", vmInfo); 
	}
	 
	protected String getDS(String diskPath)
	{
		if(diskPath==null || "".equals(diskPath)) {
			return null;
		}
		int pos = diskPath.indexOf("]");
		if(pos>0)
		{
			return diskPath.substring(1, pos );
		}
		return null;
	}
	protected void insertDiskBySync( Map vmInfo, Map info) throws Exception
	{
		info.put("DISK_ID",   IDGenHelper.getId(this.getSVC_CD()));
		MapUtil.copy(vmInfo, info, new String[]{ "DC_ID", "TEAM_CD", "INS_ID"});
		info.put("TASK_STATUS_CD","S");
		Object diskTypeId = dcService.getDiskType((String)vmInfo.get("DC_ID") , (String)info.get("DS_NM"));
		if(diskTypeId == null)
		{
			diskTypeId = PropertyManager.getString("default.diskTypeId","1");
		}
		info.put("DISK_TYPE_ID", diskTypeId);
		vmInfo.put("DISK_TYPE_ID", diskTypeId);
		this.insert(info);
		
	}
	@Override
	protected String getDefaultTitle(Map param) {
		return (String)param.get("DISK_NM");
	}

	@Override
	public String getSVC_CD(){
		return NCConstant.SVC.D.toString();
	}

	@Override
	protected boolean onDeploy(String pkVal, String cudCode, Map info)
			throws Exception {
		/*if(cudCode.equals(NCConstant.CUD_CD.C.toString()))
		{
			createDisk.run(pkVal);
		}
		else if(cudCode.equals(NCConstant.CUD_CD.D.toString()))
		{
			deleteDisk.run(pkVal);
		}*/
		return true;
	}

	public void deployByVMId(String vmId, String date) throws Exception
	{
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		param.put("INS_DT", date);
		param.put("all_yn", "Y");
		this.updateByQueryKey("update_delYN", param);
		List<Map> list = this.selectListByQueryKey("list_NC_DISK_byVM", param);
		for(Map m : list)
		{
			this.updateDeploy((String)m.get("DISK_ID"), date);
		}
	}
	@Override
	protected int updateDBTable(String tableNm, Map param) throws Exception {
		//if(tableNm.equals("NC_DISK")) chkDuplicate(tableNm, param);
		return super.updateDBTable(tableNm, param);
	}

	public void updateMount(String vmId, String diskId) throws Exception
	{
		Map param = new HashMap();
		param.put("VM_ID", vmId);
		param.put("DISK_ID", diskId);
		int row = this.updateByQueryKey("update_VM_ID", param);
		if(row == 1)
		{
			mountDisk.run(diskId);
		}

	}
	public void updateUnmount(String diskId ) throws Exception
	{
		unmountDisk.run(diskId);
		Map param = new HashMap();

		param.put("DISK_ID", diskId);
		int row = this.updateByQueryKey("update_VM_ID", param);


	}

	@Override
	protected String getTableName() {
		 
		return "NC_DISK";
	}

	@Override
	public String[] getPks() {
		 
		return new String[]{"DISK_ID"};
	}



	public void updateDiskPath(Map param, List<Map> list) throws Exception {
		for(Map m : list)
		{
			this.updateByQueryKey("update_DISK_PATH", m);
		}
		
	}
}