package com.clovirsm.service.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.IAsyncJob;
import com.clovirsm.common.NCReqService;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.hv.YamlUtil;
import com.clovirsm.hv.vmware.vra.v8.VRA8Util;
import com.clovirsm.hv.vmware.vra.v8.YamlCalc;
 
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService; 
import com.clovirsm.sys.hv.vra.VRARequest;
import com.clovirsm.sys.hv.vra.VRARequestDelete;
import com.clovirsm.sys.hv.vra.VRARunExtern;
import com.fliconz.fm.common.util.ArrayUtil;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

import freemarker.template.utility.StringUtil;

@Service
public class VraCatalogService  extends NCReqService  implements IAsyncJob{

	 
	@Autowired VRARequest vraRequest;
	@Autowired VRARequestDelete catalogRequestDelete;
	
	@Override
	public void chkDuplicate(String tableNm, Map selRow, String chkField) throws Exception
	{
		
	}
	public void setSpecFee(Map info, Map<String,Map> specMap, JSONObject properties, org.json.JSONObject dataJSON, int disk ) throws Exception  {
		if(properties.has("flavor")) {
			Map m = specMap.get( properties.getString("flavor"));
			if(m != null) {
				info.put("FLAVOR",  (String)m.get("TITLE"));
				info.put("FEE", getFee( properties.getString("flavor") , NumberUtil.getInt(m.get( "CPU_CNT")) , NumberUtil.getInt(m.get( "RAM_SIZE")),  disk, 0) * NumberUtil.getInt(info.get("COUNT")));
			}
			else {
				info.put("FLAVOR",   properties.getString("flavor"));
				info.put("FEE","0");
			}
			return;
		}
		String cpuStr = properties.get("cpuCount").toString();
		String memoryStr = properties.get("totalMemoryMB").toString();
		YamlCalc calc;
		try {
			
			calc = new YamlCalc(dataJSON);
			int cpu = getSize(calc, properties.get("cpuCount"));
			int memory =  getSize(calc, properties.get("totalMemoryMB"));;
			int gpu = 0;
			if(properties.has("gpu_size")) { 
				gpu = getSize(calc, properties.get("gpu_size"));;
			}
			 
			memory =  memory /1024 ;
			long fee =  getFee( "0", cpu , memory,  disk, gpu) * NumberUtil.getInt(info.get("COUNT"));
				
			info.put("FLAVOR",  "(" + cpu + "vCore, " + memory + "GB RAM, " + (gpu>0? gpu+"Q GPU," : "") +  NumberUtil.format(fee) + "원/day)");
			info.put("FEE", fee);
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
		
	}
	int getSize(YamlCalc calc, Object str) throws Exception {
		if(str == null) return 0;
		String val = str.toString();
		if(val.startsWith("${")) {
			return NumberUtil.getInt(  calc.cal(val));
		}
		else {
			return Integer.parseInt(val);
		}
	}
	public void getICON(HttpServletResponse response , Map param) throws Exception {
		 
		Map info = this.selectOneByQueryKey("selectIcon",param);
		String icon = (String) info.get("ICON");
		if(icon.startsWith("data:image/")) {
			String type = CommonUtil.getInnerStr(icon, "data:", ";");
			int pos = icon.indexOf(",");
			byte[] buf = Base64.getDecoder().decode(icon.substring(pos+1));
			
			if(type.indexOf("svg")>0) {
				response.setHeader("ConentType","image/svg+xml");
				String prefix = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" + 
						"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";
				response.getWriter().write(prefix);
				response.getWriter().write(new String(buf,"utf-8"));
			}
			else {
				response.setHeader("ConentType", type);
				response.getOutputStream().write(buf);
			}
			
			 
		}
		 
	}
	private String getResourceId(String str) {
		String result = CommonUtil.getInnerStr(str, "resource[\"", "\"]");
		if(result == null) {
			result = CommonUtil.getInnerStr(str, "resource.", ".");
		}
		return result;
	}
	private  int getDiskSize(YamlCalc calc, JSONObject properties, JSONObject resources, org.json.JSONObject dataJson) throws Exception{
		Object o = properties.get("attachedDisks");
		int disk = 0;
		if(o instanceof JSONArray) {
			JSONArray attachedDisks = (JSONArray)o;
			
			for(int i=0; i < attachedDisks.length(); i++) {
				String source = attachedDisks.getJSONObject(i).getString("source");
				String targetName = getResourceId(source);
				 
				Object capacityGb = resources.getJSONObject(targetName).getJSONObject("properties").get("capacityGb");
				int count = this.getCount(calc, resources.getJSONObject(targetName).getJSONObject("properties") );
				disk += NumberUtil.getInt(capacityGb, 0) * count;
			}
		}
		else if(o instanceof String) {
			try {
				String arr[] = ((String)o).split("(\\.|\\[|\\+|\\(| )");
				 
				for(int i=0; i <arr.length; i++) {
					if(arr[i].equals("resource")) {
						String a = arr[++i];
						 
						Object capacityGb = resources.getJSONObject(a).getJSONObject("properties").get("capacityGb");
						if(capacityGb instanceof String && ((String)capacityGb).startsWith("${")) {
							
							capacityGb = calc.cal((String)capacityGb);
						}
						int count = this.getCount(calc, resources.getJSONObject(a).getJSONObject("properties") );
						disk += NumberUtil.getInt(capacityGb, 0) * count;
						
							
					}
				}
			}
			catch(Exception e) {
				e.printStackTrace();
				return 0;
			}
		}
		return disk;
	}
	public List getSummary(String formInfo , String dataJson, String fixedJSONStr , int deployTime ) throws Exception {
		formInfo = StringUtil.replace(formInfo, "&gt;", ">");
		formInfo = StringUtil.replace(formInfo, "&lt;", "<");
		org.json.JSONObject dataJSON = new  org.json.JSONObject( dataJson);
		if(fixedJSONStr != null && !"".equals(fixedJSONStr)) { 
			org.json.JSONObject fixedJSON = new  org.json.JSONObject( fixedJSONStr );
			CommonUtil.putAll(dataJSON, fixedJSON);
			dataJson = dataJSON.toString();
		}
		if(TextHelper.isEmpty(formInfo)) {
			return new ArrayList();
		}
		JSONObject formJSON  = getFormInfoWithInpus(formInfo, dataJSON);
		if(!formJSON.has("resources")){
			return new ArrayList();
		}
		Map<String, Map> specMap = new HashMap();
		List<Map> specList = this.selectListByQueryKey("com.clovirsm.common.Component", "selectSpec", new HashMap());
		for(Map m : specList) {
			specMap.put((String)m.get("ID"), m);
		}
		JSONObject resources = formJSON.getJSONObject("resources");
		List result = new ArrayList();
		
		Map<String, List<String>> swMap = VRA8Util.getSWInfo(new org.json.JSONObject(formJSON.toString()), new org.json.JSONObject(dataJson)); //파싱한 값
	 
		/*for(Object k: swJSON.keySet()) { //화면에서 입력 받은 값
			JSONArray swList = CommonUtil.getJSONArray(swJSON, (String)k);
			for(int i=0; i <swList.length(); i++) {
				String sw = swList.getString(i);
				int pos = sw.lastIndexOf("/");
				int pos1 = sw.lastIndexOf(".");
				if(pos>0 && pos1>pos) {
					List list = swMap.get(k);
					if(list == null) {
						list = new ArrayList();
						swMap.put((String)k, list);
					}
					list.add( sw.subSequence(pos+1, pos1));
				}
			}
		}
		*/
		int totalCount = 0;
		Iterator it = resources.keys();
		YamlCalc calc = new YamlCalc(dataJSON);
		while(it.hasNext()) {
			String key = (String)it.next();
			JSONObject resource = resources.getJSONObject((String)key);
			if(resource.getString("type").endsWith("Machine")) {
				JSONObject properties = resource.getJSONObject("properties");
				Map map = new HashMap();
				map.put("NAME", key);
				map.put("IMAGE", properties.getString("image"));
				 
				 
				map.put("COUNT", getCount(calc, properties ));
			 
				map.put("FEE", 0);
				int disk = 0;
				if(properties.has("attachedDisks")) {
					disk = getDiskSize(calc, properties, resources, dataJSON);
				}
				this.setSpecFee(map, specMap, properties, dataJSON, disk);
				map.put("DISK_SIZE", disk);
				Object sw =  swMap.get(key);
				map.put("SW", sw==null?"":sw);
				map.put("DEPLOY_TIME", NumberUtil.getInt(map.get("COUNT"), 1)*deployTime);
				result.add(map);
			}
			
		}
		 
		return result;
	}
	
	private long getFee(String spec, int cpu, int memory, int diskSize, int gpu)  {
		Map param = new HashMap();
		param.put("SPEC_ID", spec);
		param.put("DISK_SIZE", diskSize);
		param.put("DISK_TYPE_ID", "");
		param.put("ID", "");
		param.put("DC_ID", "");
		param.put("CPU_CNT", cpu);
		param.put("RAM_SIZE", memory);
		param.put("GPU_SIZE", gpu);
		Map info;
		try {
			info = selectOneByQueryKey("com.clovirsm.common.Component", "getFee", param);
		
			return NumberUtil.getLong(info.get("FEE"), 0);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
		
	}
	@Override
	public IAfterProcess getAfter( String id) throws Exception {
		DCService dcService = (DCService)SpringBeanUtil.getBean("DCService");
		Map param = dcService.getCatalogReqInfo(id);
		if(param == null) return null;
		 
		if("Y".equals(param.get("DEL_YN")))
		{
			return DC.getBefore().onAfterProcess("C", "D", param);
		}
		else
		{
			return DC.getBefore().onAfterProcess("C", "C", param);
		}
	}
	@Override
	protected String getDefaultTitle(Map param) {
		return (String)param.get("CATALOG_NM") + "/" + param.get("REQ_TITLE");
	}

	public static JSONObject getFormInfoWithInpus(String formInfo,  org.json.JSONObject dataJSON) throws Exception {
		 
		Iterator it = dataJSON.keys(); 
		while(it.hasNext()) {
			String key = (String) it.next();
			if(dataJSON.get((String)key) instanceof String) {
				formInfo = formInfo.replace("${input." + key + "}", dataJSON.get((String)key).toString());
			}
			else {
				formInfo = formInfo.replace("\"${input." + key + "}\"", dataJSON.get((String)key).toString());
			}
		}
		
		JSONObject formJSON = new JSONObject(formInfo);
		return formJSON;
	}
	public List listStep(Map param) throws Exception{
		if(param.get("ORG_CATALOG_ID") == null) {
			return this.list("list_NC_VRA_CATALOG_STEP", param);
		}
		else {
			Map param1 = new HashMap();
			param1.put("CATALOG_ID", param.get("ORG_CATALOG_ID") );
			List<Map> list = this.list("list_NC_VRA_CATALOG_STEP", param1);
			JSONObject fixedJSON = new JSONObject((String)param.get("FIXED_JSON"));
			Iterator<String> keys = fixedJSON.keys();
			Map removeTarget = new HashMap();
			while(keys.hasNext()) {
				String key = keys.next();
				removeTarget.put(key, fixedJSON.get(key));
			}
			List result = new ArrayList();
			for(Map step: list) {
				JSONArray inputs = new JSONArray((String)step.get("INPUTS"));
				for(int i=0; i < inputs.length(); ) {
					JSONObject o = inputs.getJSONObject(i);
					if(removeTarget.containsKey( o.getString("name"))) {
						inputs.remove(o);
						if(o.has("fieldName") &&  "application".equals(o.getString("fieldName"))) {
							if(!removeTarget.containsKey("_" + o.getString("name") + "_param")) {
								JSONArray apps = (JSONArray) removeTarget.get( o.getString("name"));
								String appStr = "";
								for(int j=0; j < apps.length(); j++) {
									if(j>0) {
										appStr += "|";
									}
									appStr += apps.getString(j);
								}
								step.put("TAGS", step.get("TAGS") + ",app:" + appStr + ",appName:" + o.getString("name"));
							}
						}
					}
					else {
						i++;
					}
				}
				if(inputs.length()>0) {
					step.put("INPUTS", inputs.toString());
					result.add(step);
				}
			}
			return result;
			
		}
	}
	private int getCount(YamlCalc cal, JSONObject properties ) throws Exception {
		if(!properties.has("count")) {
			return 1;
		}
		Object o = properties.get("count");
		if(o instanceof String && ((String)o).startsWith("${")) {
			 
			try {
			 
				return NumberUtil.getInt(cal.cal(((String)o)));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0;
		}
		else {
			return NumberUtil.getInt(o);
		}
	}
	protected boolean isAvailableGPU(int wantedSize, int maxSize) {
		if(wantedSize<=maxSize) {
			return true;
		}
		if(wantedSize%maxSize == 0) {
			return true;
		}
		else return false;
	}
	protected void putGPUServer(org.json.JSONObject dataJSON , Map param, int size) throws Exception{
		param.put("GPU_SIZE", size);
		List<String> ingDataJsons =  this.selectListByQueryKey("select_ing_data_json", param);
		Map<String, Integer> ingSize = new HashMap();
		for(String dataJsonStr: ingDataJsons) {
			JSONObject dataJson = new JSONObject(dataJsonStr);
			if(dataJson.has("_gpu_server")) {
				Integer gpuSize = (Integer)ingSize.get( dataJson.getString("_gpu_server"));
				if(gpuSize == null) {
					ingSize.put( dataJson.getString("_gpu_server"),  NumberUtil.getInt(dataJson.get("_totalGpu")));
				}
				else {
					gpuSize +=  NumberUtil.getInt( dataJson.get("_totalGpu") );
					ingSize.put( dataJson.getString("_gpu_server"), gpuSize);
				}
			}
			
		}
		List<Map> list =  this.selectListByQueryKey("com.clovirsm.monitor","select_target_gpu_server", param);
		if(list.size()==0) {
			throw new Exception("GPU Server 용량이 없습니다.(" + size + "Q)");
		}
		Map result = null; 
		if(ingSize.size()==0) {
			for(Map m : list) {
				if(isAvailableGPU(size, NumberUtil.getInt(m.get("GPU_MODEL_MAX")))) {
					result = m;
					break;
				}
			}
			 
		}
		else {
			Map<Integer,Map> newSizeMap = new HashMap();
			int max = 0;
			for(Map m: list) {
				Integer isize = ingSize.get( m.get("NAME"));
				int remain = NumberUtil.getInt( m.get("REMAIN_GPU_SIZE")) -  (isize != null?isize: 0 );
				if(remain>0) {
					if(max<remain) {
						if(isAvailableGPU(size, NumberUtil.getInt(m.get("GPU_MODEL_MAX")))) {
							max = remain;
							newSizeMap.put(remain,  m );
						}
						
					}
					
				}
				 
			}
			if(newSizeMap.size()==0) {
				throw new Exception("GPU Server 용량이 없습니다.(" + size + "Q)");
			}
			result = newSizeMap.get(max);
		}
		if(result != null) {
			dataJSON.put("_gpu_server", result.get("NAME"));
			dataJSON.put("_gpu_model", result.get("GPU_MODEL"));
			dataJSON.put("_gpu_model_max", result.get("GPU_MODEL_MAX"));
			dataJSON.put("_totalGpu", size);
		}
	}
	@Autowired DCService dcService;
	protected void putUserData4Create(Map param) throws Exception{
		
		org.json.JSONObject dataJSON = new org.json.JSONObject((String)param.get("DATA_JSON"));
		JSONObject formJSON  = getFormInfoWithInpus((String)param.get("FORM_INFO"), dataJSON );
		if(!formJSON.has("resources")) return;
		JSONObject resources = formJSON.getJSONObject("resources");
		Iterator keys = resources.keys(); 
		JSONObject inputs = formJSON.getJSONObject("inputs");
		int totalCount = 0;
		int totalGpu = 0;
		Map<String, Integer> stepCount = new LinkedHashMap<String, Integer>();
		YamlCalc calc = new YamlCalc(dataJSON);
		while(keys.hasNext()) {
			String key = (String) keys.next();
			JSONObject resource = (JSONObject) resources.get((String)key);
			if(((String)resource.get("type")).matches("Cloud.*.Machine")) {// VM 외부에서 VM에 관련 된 것 ...
				if(inputs.has("_" + key)){
					JSONObject properties = resource.getJSONObject("properties");
					int count = getCount(calc, properties );
					 
					stepCount.put(key, count);
					totalCount += count;
					if(properties.has("gpu_size")) { 
						totalGpu += this.getSize(calc, properties.get("gpu_size"));
					}
				}
		 
			}
		}
		if(inputs.has("_gpu_server") && totalGpu>0){
			putGPUServer(dataJSON, param, totalGpu);
			 
			
		}
		
		if(totalCount > 0) {
			DC dc = new DC("V");
			dc.getBefore(dcService).putVraDataJson(dataJSON, formJSON, param);
			String[] vmNames = dc.getBefore(dcService).getVMNames( json2Map( dataJSON), totalCount );
			int start = 0;
			for(String step : stepCount.keySet()) {
				int count = stepCount.get(step);
				dataJSON.put("_" + step, Arrays.copyOfRange(vmNames, start, start + count));
				start += count;
			}
			param.put("MAX_VM_NM", vmNames[vmNames.length-1]);
			param.put("VM_NMS", TextHelper.join(vmNames));
			param.put("DATA_JSON", dataJSON.toString() );
			this.updateByQueryKey("update_MAX_VM_NM", param);
			// update NC_VRA_CATALOGREQ SET DATA_JSON=#{DATA_JSON}, MAX_VM_NM=#{MAX_VM_NM} where CATALOGREQ_ID=#{CATALOGREQ_ID}
		}
		
		
		// 재배포인 경우
		Map vraInfo = this.selectOneByQueryKey("select_vra_request_id", param);
		if(vraInfo != null) {
			param.putAll(vraInfo);
		}
		
	}
	public static Map<String, Object> json2Map(org.json.JSONObject paramJson) {
		Map<String, Object> result = new HashMap<String, Object>();
		for (Object key : paramJson.keySet()) {
			result.put((String) key, paramJson.get((String)key));
		}
		return result;

	}
	 
	public Map getCustomInfo(String dataJsonStr, String formJsonStr)  throws Exception{
		String[] notfields = PropertyManager.getStringArray("vra.not_display.custom.input");
	 
		Map paramNames = new HashMap();
		List<Map> envList = this.selectListByQueryKey("com.clovirsm.admin.sw.env", "list_NC_SW_ENV",  paramNames);
		for(Map m : envList) {
			paramNames.put(  m.get("NAME"), m.get("TITLE"));
		}
		JSONObject dataJson = new JSONObject(dataJsonStr);
		JSONObject dataJson1 = new JSONObject(dataJsonStr);
		Iterator it = dataJson.keys();
		while(it.hasNext()) {
			String key = (String)it.next();
			if(dataJson.get(key) instanceof JSONArray) {
				JSONArray arr = dataJson.getJSONArray(key);
				for(int i=0; i < arr.length(); i++) {
					String str = arr.getString(i);
					if(str.indexOf("=")>0) {
						String[] arr1 = str.split("=");
						if(arr1.length==2 &&  arr1[1] != null) {
							dataJson1.put(arr1[0], arr1[1]);
						}
					}
					else {
						break;
					}
				}
			}
		}
		org.codehaus.jettison.json.JSONObject inputs = (new  org.codehaus.jettison.json.JSONObject(formJsonStr)).getJSONObject("inputs");
		Map dataMap = new LinkedHashMap();
		Iterator keys = inputs.keys();
		while(keys.hasNext()) {
			String f = (String)keys.next();
			if(f.startsWith("_")) continue;
			if(notfields != null && ArrayUtil.indexOf(notfields, f)>=0) {
				continue;
			}
			if(dataJson1.has(f) ) {
				if(inputs.has(f)) {
					dataMap.put( inputs.getJSONObject(f).getString("title"), dataJson1.get(f));
				}
				else if(paramNames.containsKey(f)) {
					dataMap.put( paramNames.get(f), dataJson1.get(f));
				}
				else {
					dataMap.put( f , dataJson1.get(f));
				}
			}
		}
		return dataMap;
	}
	@Override
	public Map getAllDetail(String id)  throws Exception{
		Map param = new HashMap();
		Map result = new HashMap();
		param.put("CATALOGREQ_ID", id);
		Map info = this.selectInfo("NC_VRA_CATALOGREQ",  param);
		result.put("INFO", info);
		Map dataMap = this.getCustomInfo((String)info.get("DATA_JSON"), (String)info.get("FORM_INFO")); 
		if(dataMap != null) result.put("CUST_DATA", dataMap);
		result.put("VM_LIST", this.selectListByQueryKey("list_NC_VM", param));
		return result;
	}
	protected boolean onReDeploy( String pkVal, String cudCd, Map info) throws Exception {
		vraRequest.run((Map)info.get("INFO"));
		return false;
	}
	public void updateRedeploy(Map param) throws Exception{
	 
		param.put("TASK_STATUS_CD", "W");
		this.update(param);
		Map req = this.selectOneByQueryKey("selectByPrimaryKey_NC_VRA_CATALOGREQ", param);
		vraRequest.run(req);
	}
	public void updateVraJson(Map param) throws Exception{
		
		this.updateByQueryKey("update_NC_VRA_CATALOGREQ_REQ",param);
	}
	@Override
	protected boolean onDeploy(String pkVal, String cudCode, Map info) throws Exception {
		Map catalog = this.selectOneByQueryKey("list_NC_VRA_CATALOG", info);
		MapUtil.copy(catalog, info, new String[]{"IP_CNT","CONN_ID", "DC_ID", "ORG_CATALOG_ID",  "FORM_INFO", "FIXED_JSON", "KUBUN"});
		if("C".equals(cudCode))
		{
			
			putUserData4Create(info);
			
			vraRequest.run(info);
		}
		else if("U".equals(cudCode))
		{
			info.putAll(selectOneByQueryKey("select_vra_request_id", info));
			 
			vraRequest.run(info);
		}
		else if("D".equals(cudCode))
		{
			info.putAll(selectOneByQueryKey("select_vra_request_id", info));
			catalogRequestDelete.run(info);
			this.updateByQueryKey("update_NC_VM_DEL_YN", info);
		}
		return true;
	}
	public boolean hasVM(String id) throws Exception {
		Map param = new HashMap();
		param.put("CATALOGREQ_ID", id);
		Map info = this.getInfo(param);
		if(info == null && !info.isEmpty() && NumberUtil.getInt(info.get("USE_VM"),0)>0) {
			return true;
		}
		else {
			return false;
		}
	}
	public void deleteIfNoVM(String id) throws Exception {
		Map param = new HashMap();
		param.put("CATALOGREQ_ID", id);
		Map info = this.getInfo(param);
		if(info == null || info.isEmpty() ||   NumberUtil.getInt(info.get("USE_VM"),0)>0) {
			return;
		}
		 //마지막 VM이면
		param.put("DEL_YN", "Y");
		this.update(param);
		catalogRequestDelete.run(info);
	}
	@Override
	public String getSVC_CD() {
		return "C";
	}

	@Override
	protected String getNameSpace() {
		 
		return "com.clovirsm.resources.vra.VraCatalog";
	}
	@Override
	protected String getTableName() {
		return "NC_VRA_CATALOGREQ";
	}
	@Override
	public String[] getPks() {
		return new String[]{"CATALOGREQ_ID"};
	}
	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("TASK_STATUS_CD", "TASK_STATUS_CD");
		config.put("APPR_STATUS_CD", "APP_KIND");
		config.put("CUD_CD", "CUD");
		config.put("P_KUBUN", "P_KUBUN");
		config.put("PURPOSE", "PURPOSE");
		return config;
	}
	@Override
	public void delete4DeployFail(Map param) throws Exception{
		super.delete4DeployFail(param);
		param.put("DEL_YN", "Y");
		param.put("CATALOGREQ_ID", (String)param.get("SVC_ID"));
		this.update(param);
		param.putAll(selectOneByQueryKey("select_vra_request_id", param));
		 
		catalogRequestDelete.run(param);
		this.updateByQueryKey("update_NC_VM_DEL_YN", param);
	}
	/*@Override
	public int saveReq( Map param) throws Exception
	{
		
	    JSONParser jsonParser = new JSONParser();
		 
	    JSONArray swList = null;
	    
	    if(param.get("SW_ARRAY_JSON") != null) {
	    	swList = (JSONArray) jsonParser.parse((String)param.get("SW_ARRAY_JSON"));
	    }

		 		
		  
		int result = this.saveReq(this.getTableName()+"_REQ", param);
		deleteDBTable("NC_VRA_CATALOGREQ_SW_REQ", param);
		if(swList != null) {
			for( int s = 0 ; s < swList.size() ; s++ ) {
				
				JSONObject swObject = (JSONObject) swList.get(s);
				swObject.put("CATALOGREQ_ID", param.get("CATALOGREQ_ID"));
				insertDBTable("NC_VRA_CATALOGREQ_SW_REQ", swObject);
				
			}
		}
		
		
		return result;
	}*/
	
	@Autowired VRARunExtern runExtern;
	public Map externAPI(Map param) throws Exception {
		 
		return runExtern.run(param)  ;
	}
	 
	@Override
	public int updateOwner(   String pkVal,   String owner) throws Exception
	{
		int row = super.updateOwner(pkVal, owner);
		Map param = new HashMap();
		param.put("CATALOGREQ_ID", pkVal);
		param.put("owner", owner);
		super.updateByQueryKey("update_NC_VM_OWNER", param);
		return row;

	}
	
	
}
