package com.clovirsm.service.popup;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fm.mvc.DefaultService;

@Service
public class VMSearchPopupService extends DefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.popup.VMSearch";
	}

	@Override
	public String[] getPks() {
		return new String[]{"VM_ID"};
	}

	@Override
	protected String getTableName() {
		return "NC_VM";
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		return config;
	}

}