package com.clovirsm.service.search;

import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;

@Service
public class SearchService extends NCDefaultService {

	@Override
	public String[] getPks() {
		return new String[] {"VM_ID"};
	}

	@Override
	protected String getTableName() {
		return "NC_VM";
	}

	@Override
	protected String getNameSpace() {
		return "com.clovirsm.search.Search";
	}
}