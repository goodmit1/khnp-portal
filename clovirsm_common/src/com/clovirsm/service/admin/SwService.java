package com.clovirsm.service.admin;


import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;

@Service
public class SwService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.sw";
	}

	@Override
	protected String getTableName() {
		return "NC_SW";
	}

	@Override
	public String[] getPks() {
		return new String[]{"SW_ID"};
	}
	
	@Override
	protected Map<String, String> getCodeConfig() {
		Map conf = new HashMap();
		conf.put("OS_TYPE", "OS_TYPE");
		return conf;
	}
}