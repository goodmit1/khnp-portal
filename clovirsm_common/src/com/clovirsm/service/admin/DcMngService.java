package com.clovirsm.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.fliconz.fm.security.util.DBParamMap;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.hv.IBeforeAfter;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService; 
import com.clovirsm.sys.hv.executor.ListHVObjects;

@Service
public class DcMngService extends NCDefaultService {

	
	@Autowired DCService dcService;
	@Autowired ListHVObjects listHVObjects;
	
	protected String getNameSpace() {
		return "com.clovirsm.admin.dcMng.DcMng";
	}
	
	
	
	public void updateAfterChangeDCInfo(Map dcInfo) throws Exception
	{
		DC dc = dcService.getDC((String)dcInfo.get("DC_ID"));
		if(dc != null)
		{	
			
			IBeforeAfter before  = dc.getBefore( dcService );
			before.onChangeDCInfo(dcInfo);
		}
	}
	
	public void collectHVObj(String dcId) throws Exception
	{
		listHVObjects.run(true,dcId, "Datacenter", "");
	}
	@Override
	protected int updateDBTable(String tableNm, Map param) throws Exception {
		int row = super.updateDBTable(tableNm, param);
		updateAfterChangeDCInfo(param);
		return row;
	}
	@Override
	protected int insertDBTable(String tableNm, Map param) throws Exception {
		if(tableNm.equals("NC_DC")) { 
			connect_test(param);
			param.put("DC_ID", "" + System.nanoTime());
		}
		int row = super.insertDBTable(tableNm, param);
		if(tableNm.equals("NC_DC")) {
			dcService.putTempDCInfo(  param );
			collectHVObj((String)param.get("DC_ID"));
		}
		return row;
	}
	protected void connect_test(Map param) throws Exception {
		 DC dc = new DC((String)param.get("HV_CD"));
		 dc.getAPI().connectTest(param);
		
	}



	@Override
	protected String getTableName() {
		 
		return "NC_DC";
	}
	@Override
	public String[] getPks() {
		 
		return new String[]{"DC_ID"};
	}
	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("DEL_YN", "sys.yn");
		config.put("HV_CD", "HYPERVISOR");
		config.put("NW_AUTO_YN", "sys.yn");
		return config;
	}


	@Override
	public int insertOrUpdate(Map param) throws Exception {
		
		
		//List<Map> osTypes = getJSON2List(param, "NC_DC_OS_TYPE", "DC_ID");
		
		
		int row = super.insertOrUpdate(param);
		if(param.get("NC_DC_DISK_TYPE") != null)
		{
			List<Map> diskTypes = getJSON2List(param, "NC_DC_DISK_TYPE", "DC_ID");
			insertOrUpdateMulti("NC_DC_DISK_TYPE", diskTypes);
		}
		if(param.get("NC_DC_PROP") != null)
		{
			List<Map> props = getJSON2List(param, "NC_DC_PROP", "DC_ID");
			insertOrUpdateMulti("NC_DC_PROP", props);
			DC dc = dcService.getDC((String)param.get("DC_ID"));

			if(dc != null)
			{
				for(Map m : props)
				{
					 
						dc.putProp((String)m.get("PROP_NM"), (String)m.get("PROP_VAL"));
					 
				}
			}
		}
		if(param.get("NC_DC_ETC_CONN") != null)
		{
			List<Map> etcConns = getJSON2List(param, "NC_DC_ETC_CONN", "DC_ID");
			insertOrUpdateMulti("NC_DC_ETC_CONN", etcConns);
			
			DC dc = dcService.getDC((String)param.get("DC_ID"));
			if(dc != null)
			{	
				for(Map m:etcConns)
				{	
					dc.putProp((String)m.get("CONN_TYPE"), m);
				}
			}
		}
		//insertOrUpdateMulti("NC_DC_OS_TYPE", osTypes);
		
		
		return row;
	}



	public void deleteDiskType(Map param) throws Exception {
		List<Map> diskTypes = getJSON2List(param, "deleteRows" );
		for(Map m : diskTypes)
		{
			this.deleteDBTable("NC_DC_DISK_TYPE", m);
		}
		
	}	 
}