package com.clovirsm.service.admin;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.sys.hv.vra.VRAListFavor;

@Service
public class VmSpecMngService extends NCDefaultService {
	@Autowired VRAListFavor listFavor;
	
	protected String getNameSpace() {
		return "com.clovirsm.admin.vmspec.VmSpecMng";
	}

	@Override
	protected int insertDBTable(String tableNm, Map param) throws Exception {
		param.put("SPEC_ID", IDGenHelper.getId(""));
		return super.insertDBTable(tableNm, param);
	}

	@Override
	protected String getTableName() {
		 
		return "NC_VM_SPEC";
	}

	@Override
	public String[] getPks() {
		 
		return new String[]{"SPEC_ID"};
	}
	/**
	 * 수집
	 * @throws Exception
	 */
	public void insertCollect() throws Exception {
		Map param = new HashMap();
		 
		 
		this.updateByQueryKey("update_NC_VM_SPEC_DEL", param);
		Map result = listFavor.runAll(param);
		List<Map> list = (List)result.get(HypervisorAPI.PARAM_LIST);
		for(Map m : list){
			m.put("DEL_YN", "N");
			 
			m.put("SPEC_ID", m.get("SPEC_NM"));
			int row = this.updateByQueryKey("update_NC_VM_SPEC", m);
			if(row==0){
				row = this.updateByQueryKey("insert_NC_VM_SPEC", m);
			}
		}
		
	}

}