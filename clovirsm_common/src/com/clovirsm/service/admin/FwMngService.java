package com.clovirsm.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCConstant;
import com.clovirsm.common.NCDefaultService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.vmware.BeforeAfterVMWare;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fw.runtime.util.ListUtil;

@Service
public class FwMngService extends NCDefaultService {

	@Autowired DCService dcService;

	protected String getNameSpace() {
		return "com.clovirsm.admin.fw.FwMng";
	}
	/**
	 * 1.작업완료 여부 변경
	 * 2.변경된 정보 사용자에게 메일 발송
	 *
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public int updateApproval( Map param) throws Exception {
		if (NCConstant.TASK_STATUS_CD.S.toString().equals(param.get("TASK_STATUS_CD"))) {
			param.put("FAIL_MSG", "");
		}
		if(param.get("DEL_YN").equals("Y")){
			param.put("CUD_CD_NM", MsgUtil.getMsg("title_del", null));
		}else {
			param.put("CUD_CD_NM",MsgUtil.getMsg("request", null));
		}
		int row =  this.updateByQueryKey("updateApprovalByAdmin", param);
		try	{
			super.sendMail(param.get("FW_USER_ID"), "title_fw_result_mail","fwResult", param);
		}
		catch(Exception ignore)
		{
			ignore.printStackTrace();
		}
		return row;
	}
	public void deploy(Map param)  throws Exception {
		 
	 
		Map info = this.selectOneByQueryKey("select4Deploy", param);
		BeforeAfterVMWare beforeAfter = (BeforeAfterVMWare) dcService.getDC((String)param.get("DC_ID")).getBefore(dcService);
		beforeAfter.setDcInfo(dcService.getDC((String)param.get("DC_ID")));
		info.put("FW_DEPLOY", "Y");
		beforeAfter.onAfterFWDeploy(info);
		
	}
	public int updateApprovalList( Map param) throws Exception {
		int row = 0;
		List<Map> list = ListUtil.makeJson2List((String)param.get("selected_json"));
		for(Map tmp : list){
			row += updateApproval(tmp);
		}
		return row;
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map conf = new HashMap();
		conf.put("TASK_STATUS_CD", "TASK_STATUS_CD");
		conf.put("VM_USER_YN", "sys.yn");
		conf.put("ACCT_AUTO_CREATE_YN", "sys.yn");
		return conf;
	}

	@Override
	protected String getTableName() {

		return "NC_FW";
	}
	@Override
	public String[] getPks() {

		return new String[]{"FW_ID"};
	}
}