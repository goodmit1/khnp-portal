package com.clovirsm.service.admin;


import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;

@Service
public class FabSettingService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.fabSetting";
	}

	@Override
	protected String getTableName() {
		return "NC_DC_KUBUN_CONF";
	}

	@Override
	public String[] getPks() {
		return new String[]{"DC_ID", "ID", "KUBUN"};
	}

}