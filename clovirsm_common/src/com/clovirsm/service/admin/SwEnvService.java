package com.clovirsm.service.admin;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;

@Service
public class SwEnvService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.sw.env";
	}

	@Override
	protected String getTableName() {
		return "NC_SW_ENV";
	}

	@Override
	public String[] getPks() {
		return new String[]{"NAME","SW_ID"};
	}
	
	@Override
	public List list(Map searchParam) throws Exception {
		 
		List<Map> list =  super.list(searchParam);
		List<Map> tempList = new ArrayList<Map>();
		
		for(int i = 0 ; i < list.size(); i++) {
			tempList.add(changeToLowerMapKey(list.get(i)));
		}
		
		return tempList;
	}
	
	public  Map changeToLowerMapKey(Map map){
		   Map<String, Object> origin = map;
		   Map<String, Object> temp = new HashMap<String, Object>();   
		  
		   Set<String> set = origin.keySet();
		   Iterator<String> e = set.iterator();

		   while(e.hasNext()){
		     String key = e.next();
		     Object value = (Object) origin.get(key);
		     if(key.equals("FIELDNAME")) {
		    	 temp.put("fieldName", value);
		     }else {
		    	 temp.put(key.toLowerCase(), value);
		     }
		   }

		   origin = null;
		  return temp;
		 }
}