package com.clovirsm.service.admin;


import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;

@Service
public class ScriptService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.script";
	}

	@Override
	protected String getTableName() {
		return "NC_SCRIPT";
	}

	@Override
	public String[] getPks() {
		return new String[]{"SCRIPT_ID"};
	}
}