package com.clovirsm.service.admin;


import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import com.clovirsm.common.NCDefaultService;

@Service
public class DcKubunConfService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.dcKubunConf";
	}

	@Override
	protected String getTableName() {
		return "NC_DC_KUBUN_CONF";
	}

	public int insert(String dcId, String id, String kubun ,String val1, String val2) throws Exception {
		Map  param = new HashMap();
		param.put("DC_ID", dcId);
		param.put("ID", id);
		param.put("KUBUN", kubun);
		param.put("VAL1", val1);
		param.put("VAL2", val2);
		return this.insert(param);
	}
	public int update(String dcId, String id, String kubun ,String val1, String val2) throws Exception {
		Map  param = new HashMap();
		param.put("DC_ID", dcId);
		param.put("ID", id);
		param.put("KUBUN", kubun);
		param.put("VAL1", val1);
		param.put("VAL2", val2);
		return this.update(param);
	}
	public int delete(String dcId, String id, String kubun ,String val1 ) throws Exception {
		Map  param = new HashMap();
		param.put("DC_ID", dcId);
		param.put("ID", id);
		param.put("KUBUN", kubun);
		param.put("VAL1", val1);
		return this.delete(param);
	}
	@Override
	public String[] getPks() {
		return new String[]{"DC_ID", "ID", "KUBUN"};
	}

}