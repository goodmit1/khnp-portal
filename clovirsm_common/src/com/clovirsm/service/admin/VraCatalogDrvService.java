package com.clovirsm.service.admin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;
import com.clovirsm.hv.CommonUtil;
import com.clovirsm.hv.HypervisorAPI;
import com.clovirsm.service.resource.VraCatalogService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.vra.VRAListCatalog;

@Service
public class VraCatalogDrvService  extends NCDefaultService {

	@Autowired VraCatalogService  vraCatalogService;
	 
	@Override
	protected String getNameSpace() {
		
		return "com.clovirsm.admin.vra.CatalogDrv";
		
	}

	public static Map converMap(JSONObject map, String... field) throws JSONException {
		Map result = new HashMap();
		if(field==null || field.length==0) {
			Iterator it = map.keys();
			while(it.hasNext()) {
				String f = (String) it.next();
				result.put(f, map.get((String)f));
			}
		}
		else {
			for(String f:field) {
				result.put(f, map.get(f));
			}
		}
		
		return result;
	}

	@Override
	protected String getTableName() {
		 
		return "NC_VRA_CATALOG_DRV";
	}

	@Override
	public String[] getPks() {
		return new String[]{"DRV_ID"};
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map config = new HashMap();
		config.put("ALL_USE_YN", "sys.yn");
		return config;
	}
	 
	public String getFieldList(Map param) throws Exception {
		Map info = vraCatalogService.selectOneByQueryKey("list_NC_VRA_CATALOG", param);
		JSONObject formInfo = new JSONObject((String)info.get("FORM_INFO"));
		
		JSONArray field_list  = new JSONArray();
		List<Map> stepList = vraCatalogService.list("list_NC_VRA_CATALOG_STEP", info);
		Map<String, JSONObject> fieldNameMap = new HashMap();
		for(Map m:stepList) {
			JSONArray inputs = new JSONArray((String)m.get("INPUTS"));
			for(int i=0; i < inputs.length(); i++) {
				 
					fieldNameMap.put(inputs.getJSONObject(i).getString("name"), inputs.getJSONObject(i) );
				 
				
				
			}
		}
		JSONObject inputs = formInfo.getJSONObject("inputs");
		Iterator it = inputs.keys();
		while(it.hasNext()) {
			String key = (String)it.next();
			JSONObject json1 = inputs.getJSONObject(key);
			if(!json1.has("title") ) {
				json1.put("title", key);
			}
			json1.put("name", key);
			if(fieldNameMap.containsKey(key)) {
			
				putAll(json1, fieldNameMap.get(key));
				 
			}
			field_list.put(json1);
		}
		return field_list.toString() ;
	}
	 

	public static void putAll(JSONObject target, JSONObject src) throws JSONException {
		Iterator it = src.keys();
		while(it.hasNext()) {
			String key = (String)it.next();
			target.put(key, src.get(key));
		}
	}

	@Override
	public List list(Map searchParam) throws Exception {
		 
		List<Map> list =  super.list(searchParam);
		if((String)searchParam.get("JSON_PARAMS") != null){
		String[] jsonParams = ((String)searchParam.get("JSON_PARAMS")).split(",");
			for(int i = 0 ; i < list.size(); i++) {
				if(list.get(i).get("FIXED_JSON") != null ) {
				 
					JSONObject jsonObj = new JSONObject((String)list.get(i).get("FIXED_JSON"));
					for(String p:jsonParams) {
						if(jsonObj.has(p)) {
							list.get(i).put(p, jsonObj.get(p).toString());
						}
					}
					
				 
				}
			}
		}
		return list;
	}
	@Autowired CategoryMapService categoryMapService;
	
	protected void setPKVal(Map param) throws Exception
	{
		String DRV_ID = ""+System.nanoTime();
		param.put("DRV_ID", DRV_ID);
	}
	@Override
	public int insertOrUpdate(Map param) throws Exception {
		int row =  super.insertOrUpdate(param);
		categoryMapService.insert((String)param.get("DRV_ID"), param.get("CATEGORY_IDS"), "C", (String)param.get("ICON_YN"));
		
		return row;
	}

}
