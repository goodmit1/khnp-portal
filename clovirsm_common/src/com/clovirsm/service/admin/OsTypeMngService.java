package com.clovirsm.service.admin;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.common.IDGenHelper;
import com.clovirsm.common.NCConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;
import com.clovirsm.hv.HypervisorAPI; 
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.executor.CheckTemplatePath;
import com.clovirsm.sys.hv.vra.VRAListBaseImage;
import com.clovirsm.sys.hv.vra.VRAListClusterTag;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class OsTypeMngService extends NCDefaultService {

	@Autowired
	CheckTemplatePath checkTemplatePath;
	
	
	protected String getNameSpace() {
		return "com.clovirsm.admin.ostype.OsTypeMng";
	}

	@Override
	protected String getTableName() {
		return "NC_OS_TYPE";
	}

	@Override
	public String[] getPks() {
		return new String[]{"OS_ID"};
	}

	public String getSVC_CD(){
		return NCConstant.SVC.G.toString();
	}
	
	/*public void insertAll(Map param, List<Map> list) throws Exception {
		this.updateByQueryKey("delete_NC_DC_OS_TYPE_ALL", param);
		this.updateByQueryKey("update_NC_OS_TYPE_DEL", param);
		 
		for(Map m : list){
			 
			m.put("DEL_YN", "N");
			 
			m.put("OS_ID", m.get("OS_NM") );
			Map template = (Map) m.remove("template");
			int row = this.updateByQueryKey("update_NC_OS_TYPE", m);
			if(row==0){
				row = this.updateByQueryKey("insert_NC_OS_TYPE", m);
			}
			insertDCTemplate(m, template);
			
		}
	}*/
	@Autowired
	VRAListBaseImage listBaseImage;
	public void insertCollect() throws Exception {
		Map param = new HashMap();
		 
		 
		Map result = listBaseImage.runAll(param);
		List<Map> list = (List)result.get(HypervisorAPI.PARAM_LIST);
		for(Map m : list){
			 
			m.put("DEL_YN", "N");
			 
			m.put("OS_ID", m.get("OS_NM") );
			Map template = (Map) m.remove("template");
			int row = this.updateByQueryKey("update_NC_OS_TYPE", m);
			if(row==0){
				row = this.updateByQueryKey("insert_NC_OS_TYPE", m);
			}
			insertDCTemplate(m, template);
			
		}
		 
	}
	protected void insertDCTemplate(Map param, Map<String, Integer> template) throws Exception {
		Set<String> keys = template.keySet();
		for(String key:keys) {
			param.put("TMPL_PATH", key);
			param.put("DISK_SIZE", template.get(key));
			try {
				this.updateByQueryKey("insert_NC_DC_OS_TYPE_ByTemplate", param);
			}catch(Exception ignore) {
				
			}
		}
	}
	@Override
	protected Map<String, String> getCodeConfig() {
		Map<String, String> conf = new HashMap<String, String>();
		conf.put("PURPOSE", "PURPOSE");
		conf.put("P_KUBUN", "P_KUBUN");
		conf.put("LINUX_YN", "sys.yn");
		conf.put("DEL_YN", "sys.yn");
		return conf;
	}

	protected void chkTemplatePath(Map param) throws Exception
	{
		if(TextHelper.isEmpty((String)param.get("TMPL_PATH"))) return;
		Map info = checkTemplatePath.run((String)param.get("DC_ID"), (String)param.get("TMPL_PATH"));
		if(info != null && info.size()>0)
		{
			param.putAll(info);
			if(((String)info.get("GUEST_NM")).indexOf("Microsoft")>=0)
			{
				param.put("LINUX_YN", "N");
			}
			else
			{
				param.put("LINUX_YN", "Y");
			}
		}
		 
	}
	public int insertOrUpdate4DC(Map param) throws Exception {
		
		chkTemplatePath(  param); 
		int row = super.updateDBTable("NC_DC_OS_TYPE",param);
		if(row==0)
		{
			row = super.insertDBTable("NC_DC_OS_TYPE",param);
		}
		List<String> network = (List)param.get("NETWORK");
		this.deleteByQueryKey("delete_NC_DC_OS_TYPE_NW", param);
		if(network != null)
		{	
			int idx = 1;
			for(String n:network)
			{
				param.put("NW_HV_ID", n);
				param.put("SEQ", idx++);
				this.insertByQueryKey("insert_NC_DC_OS_TYPE_NW", param);
			}
		}
		return row;
	}

	public void chgCategory(Object old, Object newCategory) throws Exception {
		Map param = new HashMap();
		param.put("OLD_CATEGORY", old);
		param.put("CATEGORY", newCategory);
		this.updateByQueryKey("update_category", param);
		// UPDATE NC_OS_TYPE SET CATEGORY=#{CATEGORY} WHERE CATEGORY=#{OLD_CATEGORY}
		
	}
	@Override
	public int delete(Map param) throws Exception {
		this.deleteByQueryKey("delete_NC_DC_OS_TYPE", param);
		return super.delete(param);
	}
	@Override
	public int update(Map param) throws Exception {
		if(!TextHelper.isEmpty((String)param.get("TMPL_PATH")))
		{
			chkTemplatePath(param);
			insertOrUpdate4DC(param);
		}
		return super.update(param);
	}

	@Override
	public int insert(Map param) throws Exception {
		if(!TextHelper.isEmpty((String)param.get("TMPL_PATH")))
		{
			chkTemplatePath(param);
		}
		param.put("DEL_YN", "N");
		int row = this.updateByQueryKey("restore_by_TMPL_PATH", param);
		if(row == 0)
		{
			param.put("OS_ID", IDGenHelper.getId(getSVC_CD()));
			row =  super.insert(param);

		}
		if(!TextHelper.isEmpty((String)param.get("TMPL_PATH")))
		{
			insertOrUpdate4DC(param);
		}
		return row;
		
	}


}