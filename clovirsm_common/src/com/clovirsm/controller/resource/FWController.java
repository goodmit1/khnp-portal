package com.clovirsm.controller.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.controller.ReqController;
import com.clovirsm.service.resource.FWService;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/fw" )
public class FWController extends ReqController{

	@Autowired FWService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

}
