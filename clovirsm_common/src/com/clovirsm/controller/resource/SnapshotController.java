package com.clovirsm.controller.resource;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.resource.SnapshotService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;

@RestController
@RequestMapping(value =  "/snapshot" )
public class SnapshotController extends DefaultController{

	@Autowired SnapshotService service;
	@Override
	protected DefaultService getService() {
		return service;
	}

	@RequestMapping(value =  "/revert" )
	public Object revert(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				
				String vm=(String)param.get("VM_ID");
				String snapId=(String)param.get("SNAPSHOT_ID");
				service.revertVMStart(vm, snapId);
				return ControllerUtil.getSuccessMap(param);
			}
		}
		).run(request);

	}


}
