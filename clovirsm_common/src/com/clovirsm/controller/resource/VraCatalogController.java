package com.clovirsm.controller.resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.common.NCConstant;
import com.clovirsm.controller.ReqController;
import com.clovirsm.hv.NumberUtil;
import com.clovirsm.service.TaskService;
import com.clovirsm.service.resource.VraCatalogService;
import com.clovirsm.sys.hv.ni.GetNWFlow; 
import com.clovirsm.util.JSONUtil;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@RestController
@RequestMapping(value =  "/vra_catalog" )
public class VraCatalogController extends ReqController{

	@Autowired VraCatalogService service;
	@Override
	protected DefaultService getService() {
		return service;
	}
	protected void jsonPutMap(String formInfo, Map map)
	{
		if(formInfo == null) return;
		org.json.simple.JSONObject json = JSONUtil.createJSON(formInfo);
		Set<String> keys = json.keySet();
		for(String key:keys)
		{
			map.put(key, json.get(key));
		}
	}

	@Autowired GetNWFlow getNWFlow;
	
	@RequestMapping(value =  "/nw_traffic" )
	public Object nwTraffic(final HttpServletRequest request, HttpServletResponse response)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				List<Map> vmList = service.selectListByQueryKey("list_NC_VM", param);
				String[] vmNames = new String[vmList.size()];
				int i=0;
				for(Map m : vmList) {
					vmNames[i++] = (String)m.get("VM_NM");
				}
				
				return getNWFlow.run(vmNames, param);
				
			}
		}).run(request);
		
	}
	@RequestMapping(value =  "/extern_api" )
	public Object externAPI(final HttpServletRequest request, HttpServletResponse response)
	{
		try {
			return (service.externAPI(ControllerUtil.getParam(request))).get("DATA").toString() ;
		} catch (Exception e) {
			return ControllerUtil.getErrMap(e);
		}
		
	}
	@RequestMapping(value =  "/form_info" )
	public Object formInfo(final HttpServletRequest request, HttpServletResponse response)
	{

		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map info = new HashMap();
				if(param.get("CATALOGREQ_ID") != null)
				{
					info = service.selectOneByQueryKey("selectByPrimaryKey_NC_VRA_CATALOGREQ_REQ", param);
					param.put("CATALOG_ID", info.get("CATALOG_ID"));
				}
				Map info1 = service.selectOneByQueryKey("list_NC_VRA_CATALOG", param);
				info.putAll(info1);
				jsonPutMap((String)info.get("FORM_INFO"), info);
				return info;

			}

		}).run(request);
	}

	
	@RequestMapping(value =  "/icon" )
	public void getICON(final HttpServletRequest request, HttpServletResponse response) {
		Map param;
		try {
			param = ControllerUtil.getParam(request);
			service.getICON(response, param);
		} catch (Exception e) {
		 
			e.printStackTrace();
		}
	
	}
	@RequestMapping(value =  "/list_step" )
	public Object listStep(final HttpServletRequest request, HttpServletResponse response) {
		try {
			return service.listStep(ControllerUtil.getParam(request));
		} catch (Exception e) {
			 
			return ControllerUtil.getErrMap(e);
		}
	}
	@RequestMapping(value =  "/summary" )
	public Object getSummary(final HttpServletRequest request, HttpServletResponse response) {
	 
		List result;
		try {
			result = service.getSummary( request.getParameter("FORM_INFO"),  request.getParameter("DATA_JSON"),  request.getParameter("FIXED_JSON"), NumberUtil.getInt(request.getParameter("DEPLOY_TIME"), -1));
			return result;
		} catch (Exception e) {
			return ControllerUtil.getErrMap(e);
		}
		 
		
	}
	
	
	@RequestMapping(value =  "/updateReDeploy" )
	public Object swReqSave(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				 
				service.updateRedeploy( param );
				
				return ControllerUtil.getSuccessMap(param);
			}
		}).run(request);
	}
	
	@RequestMapping(value =  "/updateVraJson" )
	public Object updateVraJson(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				
				service.updateVraJson(param);
				
				return ControllerUtil.getSuccessMap(param);
			}
		}).run(request);
	}
	
	@RequestMapping(value =  "/updateStatusSync" )
	public Object updateStatusSync(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				 
				TaskService taskService = (TaskService)SpringBeanUtil.getBean("taskService");
				taskService.updateRecheckTask((String)param.get("CATALOGREQ_ID"), NCConstant.SVC.C.toString(), (String)param.get("CATALOGREQ_ID"), "NC_VRA_CATALOGREQ", "CATALOGREQ_ID");
				 
				
				return ControllerUtil.getSuccessMap(param);
			}
		}).run(request);
	}
	
}
