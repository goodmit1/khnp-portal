package com.clovirsm.controller.admin;

import com.clovirsm.service.admin.CategoryConfService;
import com.clovirsm.service.admin.TeamConfService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value =  "/category_conf" )
public class CategoryConfController extends DefaultController {

	@Autowired
	CategoryConfService service;
	@Override
	protected CategoryConfService getService() {
		return service;
	}

	@RequestMapping({"/save"})
	@Override
	public Map save(HttpServletRequest request, HttpServletResponse response) {
		return ((Map) new ActionRunner() {
			@SuppressWarnings("unchecked")
			protected Object run(Map param) throws Exception {
				String[] keyArr = new String[] {"IP", "CATEGORY", "CLUSTER"};
				for(String key : keyArr) {
					if(param.get(key) != null){
						param.put("KUBUN", key);
						param.put("VAL1", param.get(key));
						param.put("IU", param.get(key + "_IU"));
						if("".equals(param.get(key))){
							if("I".equals(param.get("IU"))) {
								continue;
							}
							param.put("IU", "D");
						}
						service.insertOrUpdateOrDelete(param);
					}
				}
				return ControllerUtil.getSuccessMap(param, service.getPks());
			}

		}.run(request));
	}
	@RequestMapping({"/save_clusterGroup"})
	public Map saveClusterGroup(HttpServletRequest request, HttpServletResponse response) {
		return ((Map) new ActionRunner() {
			@SuppressWarnings("unchecked")
			protected Object run(Map param) throws Exception {
				service.insertOrUpdate(param);
				return ControllerUtil.getSuccessMap(param, service.getPks());
			}

		}.run(request));
	}
	@RequestMapping({"/list_cluster_group_setting"})
	public Map listClusterGroupSetting(HttpServletRequest request, HttpServletResponse response) {
		return ((Map) new ActionRunner() {
			@SuppressWarnings("unchecked")
			protected Map run(Map param) throws Exception {
				List list = new ArrayList();
				int maxLevel = 0;
				list =  service.list("list_CLUSTER_GROUP_SETTING",param);
				for(int i = 0 ; i < list.size(); i++) {
					Map object = (Map) list.get(i);
					Map newObject = new HashMap();
					String nameArr[] = String.valueOf(object.get("CATEGORY_NMS")).split(">");
						newObject.put("CATEGORY_ID", object.get("CATEGORY_ID"));
						newObject.put("CATEGORY_NM", object.get("CATEGORY_NM"));
						newObject.put("CATEGORY_NMS", object.get("CATEGORY_NMS"));
						for(int y = 0 ; y < nameArr.length ; y++) {
							newObject.put("NAME_"+y,nameArr[y]);
						}
						if(object.get("ID") != null) {
							newObject.put(object.get("ID"), object.get("VAL1"));
							
						} 
						if(nameArr.length>maxLevel)
						{
							maxLevel = nameArr.length;
						}
						for(int z = i+1 ; z < list.size() ; z++) {
							Map tempObject = (Map) list.get(z);
							if(object.get("CATEGORY_ID").equals(tempObject.get("CATEGORY_ID"))) {
								newObject.put(tempObject.get("ID"), tempObject.get("VAL1"));
								list.remove(z);
								z--;
							}
						}
						
						list.set(i, newObject);

						
				}
				Map result = new HashMap();
				result.put("list", list);
				result.put("maxLevel", maxLevel);
				return result;
			}

		}.run(request));
	}
}