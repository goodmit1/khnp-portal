package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import com.clovirsm.common.IJob;
import com.clovirsm.service.resource.SnapshotService;
import com.clovirsm.service.workflow.DeployFailService;
import com.clovirsm.sys.hv.executor.PowerOffVM;
import com.clovirsm.sys.hv.executor.PowerOnVM;
import com.clovirsm.sys.hv.executor.RebootVM;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class VScheduleJob implements IJob {
	
	 
	@Autowired SnapshotService sservice;

	String id;

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return id;
	}
	
	public Map<String, String> snapshotCreate() {
		Map<String, String> result = new HashMap();
		try {
			result.put("LAST_RUN_RESULT", "S");
		}catch (Exception e) {
			e.printStackTrace();
			result.put("LAST_RUN_RESULT", "F");
			result.put("FAIL_MSG", e.getMessage());
		}
		return result;
	}
	
	public Map<String, String> snapshotDelete() {
		Map<String, String> result = new HashMap();
		try {
			result.put("LAST_RUN_RESULT", "S");
		}catch (Exception e) {
			e.printStackTrace();
			result.put("LAST_RUN_RESULT", "F");
			result.put("FAIL_MSG", e.getMessage());
		}
		return result;
	}
	
	public Map<String, String> vmPowerOff() {
		Map<String, String> result = new HashMap();
		try {
			PowerOffVM powerOffVM = (PowerOffVM)SpringBeanUtil.getBean("powerOffVM");
			powerOffVM.run(this.id);
			result.put("LAST_RUN_RESULT", "S");
		}catch (Exception e) {
			e.printStackTrace();
			result.put("LAST_RUN_RESULT", "F");
			result.put("FAIL_MSG", e.getMessage());
		}
		return result;
	}

	public Map<String, String> redeploy() {
		Map<String, String> result = new HashMap();
		try {
			DeployFailService service = (DeployFailService)SpringBeanUtil.getBean("deployFailService");
			String cudCd = this.id.substring(0,1);
			String insDt =  this.id.substring(1,15);
			String svcId =  this.id.substring(15);
			String svcCd = svcId.substring(0,1);
			service.update_redeploy(svcCd, svcId, insDt, cudCd);
			result.put("LAST_RUN_RESULT", "S");
		}catch (Exception e) {
			e.printStackTrace();
			result.put("LAST_RUN_RESULT", "F");
			result.put("FAIL_MSG", e.getMessage());
		}
		return result;
	}
	public Map<String, String> vmPowerOn() {
		Map<String, String> result = new HashMap();
		try {
			PowerOnVM powerOnVM = (PowerOnVM)SpringBeanUtil.getBean("powerOnVM");
			String ip = powerOnVM.run(this.id);
			result.put("LAST_RUN_RESULT", "S");
		}catch (Exception e) {
			e.printStackTrace();
			result.put("LAST_RUN_RESULT", "F");
			result.put("FAIL_MSG", e.getMessage());
		}
		return result;
	}
	
	public Map<String, String> vmReboot() {
		Map<String, String> result = new HashMap();
		try {
			RebootVM rebootVM = (RebootVM)SpringBeanUtil.getBean("rebootVM");
			rebootVM.run(this.id);
			result.put("LAST_RUN_RESULT", "S");
		}catch (Exception e) {
			e.printStackTrace();
			result.put("LAST_RUN_RESULT", "F");
			result.put("FAIL_MSG", e.getMessage());
		}
		return result;
	}

}