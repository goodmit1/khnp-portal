package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.MmPayService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/mmpay" )
public class MmPayController extends DefaultController {

	@Autowired MmPayService service;
	@Override
	protected MmPayService getService() {

		return service;
	}

	@RequestMapping(value =  "/fee_chart_his_info" )
	public Object getFeeChartHistoryInfo(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		return  (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				List list = getService().selectListByQueryKey("history_NC_MM_STAT_FEE", param);
				Map tmp = new HashMap();
				tmp.put("LIST", list);
				tmp.put("TEAM_NM", param.get("TEAM_NM"));
				return tmp;
			}
		}).run(request);
	}

}