package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.LicenseService;
import com.clovirsm.service.batch.ScheduleService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@RestController
@RequestMapping(value =  "/license" )
public class LicenseController extends DefaultController {

	@Autowired LicenseService service;
	@Override
	protected LicenseService getService() {
		return service;
	}

	@RequestMapping(value =  "/sync" )
	public Map update(final HttpServletRequest request, HttpServletResponse response) throws Exception {
	 
		ScheduleService service = (ScheduleService)SpringBeanUtil.getBean("clovirScheduleService");  
		service.runInsertLicense();
		
		return ControllerUtil.getSuccessMap(new HashMap());
	}
}