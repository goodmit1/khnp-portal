package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.common.NCReqService;
import com.clovirsm.service.admin.NasMngService;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.fliconz.fm.admin.service.UserService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fw.runtime.util.MapUtil;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
import com.mysql.jdbc.log.Log;

@RestController
@RequestMapping(value =  "/bulkImport" )
public class BulkImportController extends DefaultController{

	@Autowired 
	DCService service;
	 
	
	@Override
	protected DefaultService getService() {
		 
		try {
			return NCReqService.getService("S");
		} catch (Exception e) {
			return (VMService)SpringBeanUtil.getBean("VMService");
		}
	}
	
	
	@RequestMapping(value= "/sync")
	public void bulikImport(final HttpServletRequest request, HttpServletResponse response ) throws Exception {
		
		Map param = new HashMap();
		param.put("DC_ID", request.getParameter("DC_ID"));
		param.put("INS_ID",request.getParameter("INS_ID"));
		param.put("LOGIN_ID",request.getParameter("LOGIN_ID"));
		param.put("VM_PREFIX",request.getParameter("VM_PREFIX"));
		
		String vm_prifix=request.getParameter("VM_PREFIX");
		DC dc = service.getDC(request.getParameter("DC_ID"));
		param.putAll(dc.getProp());
		Map result = dc.getAPI().getVMListInDC(param);
		List<Map> list = (List)result.get("LIST");
		
		for(Map m : list  )
		{
			System.out.println(m);
			m.put("VM_NM", m.get("NAME"));
			if(!((String)m.get("VM_NM")).startsWith(vm_prifix)) continue;
			MapUtil.copy(param, m, new String[]{"DC_ID","INS_ID","LOGIN_ID"});
			((VMService)getService()).insertVMfromDC(m);
		}
		
		
		
	}











}


