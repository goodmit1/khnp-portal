package com.clovirsm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.monitor.OverUnderVMService;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/overunder" )
public class OverUnderVMController extends DefaultController{

	@Autowired OverUnderVMService service;
	
	@Override
	protected DefaultService getService() {
		// TODO Auto-generated method stub
		return service;
	}
	
}


