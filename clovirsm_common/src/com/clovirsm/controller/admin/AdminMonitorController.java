package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.monitor.AdminMonitorService;
import com.clovirsm.service.monitor.MonitorService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/admin_monitor" )
public class AdminMonitorController extends DefaultController {
	
	@Autowired AdminMonitorService service;
	@Override
	protected DefaultService getService() {
		return service;
	}
	@RequestMapping(value =  "/list/{id}" )
	public Object monitorList(final HttpServletRequest request, HttpServletResponse response,@PathVariable("id") final String id)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				List list = getService().selectList(id, param);
				result.put("list", list);
				result.put("total", list.size());
			 
				return result;
				 
			}

		}).run(request);
			 
	}
	
}
