package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.VmUserMngService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/vm_user_mng" )
public class VmUserMngController extends DefaultController {

	@Autowired VmUserMngService service;
	@Override
	protected VmUserMngService getService() {

		return service;
	}

	@RequestMapping(value =  "/update" )
	public Object update(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return  (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				service.updateApprovalList(param);
				return new HashMap();
			}
		}).run(request);

	}

}