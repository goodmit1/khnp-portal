package com.clovirsm.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.FwMngService;
import com.clovirsm.service.admin.UserSearchService;
import com.clovirsm.sys.hv.executor.AddDelUser;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fw.runtime.util.ListUtil;

@RestController
@RequestMapping(value =  "/fw_mng" )
public class FwMngController extends DefaultController {

	@Autowired FwMngService service;
	@Autowired AddDelUser addDelUser;
	
	@Override
	protected FwMngService getService() {

		return service;
	}

	@RequestMapping(value =  "/update" )
	public Map update(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return (Map)(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				service.updateApprovalList(param);
				return new HashMap();
			}
		}).run(request);

	}
	@RequestMapping(value =  "/syncUser" )
	public Map syncUser(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return (Map)(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				String jsonArrStr = (String)param.get("LIST");
				List<Map> list = ListUtil.makeJson2List(jsonArrStr);
				for(Map m:list)
				{	
					
					addDelUser.runOne(m);
				}
				return new HashMap();
			}
		}).run(request);

	}
	@RequestMapping(value =  "/deployFw" )
	public Map deployFw(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return (Map)(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				String jsonArrStr = (String)param.get("LIST");
				List<Map> list = ListUtil.makeJson2List(jsonArrStr);
				for(Map m:list)
				{	
					
					service.deploy(m);
				}
				return new HashMap();
			}
		}).run(request);

	}
}