package com.clovirsm.controller.admin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.cm.sys.git.GitLabAPI;
import com.clovirsm.service.admin.ScriptService;
import com.clovirsm.service.admin.SwService;
import com.clovirsm.service.admin.VraCatalogMngService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fm.mvc.util.ControllerUtil;
import com.fliconz.fw.runtime.util.PropertyManager;

@RestController
@RequestMapping(value =  "/script" )
public class ScriptController extends DefaultController {

	@Autowired ScriptService service;
	@Override
	protected ScriptService getService() {
		 
		return service;
	}
	
	@RequestMapping(value =  "/scriptSave" )
	public int swSave(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		return (int)(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				int result = 0;
				if(param.get("SCRIPT_ID") == null) {
					param.put("SCRIPT_ID", "" + System.nanoTime());
					result = service.insert(param);
				} else {
					result = service.update(param);
					
				}
				
				return result;
			}
		}).run(request);
	}
	
	@RequestMapping(value =  "/codeGitSave" )
	public String codeGitSave(final HttpServletRequest request, HttpServletResponse response) throws Exception{
		return (String)(new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				GitLabAPI git = new GitLabAPI();
				String result ="";
				git.setUrl(PropertyManager.getString("clovir.git.url","http://172.16.33.3:8963"));
				git.setToken(PropertyManager.getString("clovir.git.token","vrEgmKwTxbsyL8xvyTEX"));
				String id = PropertyManager.getString("clovir.git.id","root");
				String repoName = PropertyManager.getString("clovirsm.git.repo","AppConfig");
				try {
					git.saveFile( id, repoName, "master",param.get("type").toString() +"/"+param.get("name").toString() , param.get("content").toString(),param.get("commitMsg").toString());
					String path = PropertyManager.getString("local_shell_path")+"/"+param.get("purpose").toString();
					File Folder = new File(path);
					if (!Folder.exists()) {
						try{
						    Folder.mkdir(); //폴더 생성합니다.
						    System.out.println("폴더가 생성되었습니다.");
					    }
						catch(Exception e){
							    e.getStackTrace();
						}         
					}
					OutputStream output = new FileOutputStream(path +"/"+ param.get("name"));
				    String str =param.get("content").toString();
				    byte[] by=str.getBytes();
				    try {
				    	output.write(by);
				    	result = "1";
					} catch (Exception e) {
						result = "0";
						// TODO: handle exception
					}
					return result;
				} catch (Exception e) {
					result = "0";
					e.printStackTrace();
					// TODO: handle exception
				}
				return result;
			}
		}).run(request);
	}
}
