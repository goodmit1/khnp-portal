package com.clovirsm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.PortgroupService;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/portgroup" )
public class PortgroupController extends DefaultController {

	@Autowired PortgroupService service;
	@Override
	protected PortgroupService getService() {

		return service;
	}

}
