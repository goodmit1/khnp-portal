package com.clovirsm.controller.admin;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.ComponentService;
import com.clovirsm.service.admin.NotUseVMService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fw.runtime.util.DateUtil;

@RestController
@RequestMapping(value =  "/not_use_vm" )
public class NotUseVMController extends DefaultController {

	@Autowired NotUseVMService service;
	@Override
	protected NotUseVMService getService() {
		 
		return service;
	}
	
	
	@RequestMapping(value =  "/list/minuse/" )
	public Object listMinUse(final HttpServletRequest request, HttpServletResponse response )
	{
		return  (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				 
				Map result = new HashMap();
				long now = System.currentTimeMillis();
				result.put("list", service.listMinUse(param));
				System.out.println("total_time=" + (System.currentTimeMillis() - now )/1000/60);
				return result;
				
				 
			}

		}).run(request);
			 
	}
	@RequestMapping(value =  "/list/over_size/" )
	public Object listOverSize(final HttpServletRequest request, HttpServletResponse response )
	{
		return  (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				param.put("CPU_MAX", ComponentService.getEnv("oversize_cpu", 10));
				param.put("MEM_MAX", ComponentService.getEnv("oversize_mem", 10));
				param.put("DISK_MAX", ComponentService.getEnv("oversize_disk", 10));
				
				String term = (String) ComponentService.getEnv("oversize_term_day", "0");
				Calendar startTime = Calendar.getInstance();
				startTime.add(Calendar.DATE, -1 * Integer.parseInt(term));
				param.put("START_DT", DateUtil.format(startTime.getTime(), "yyyyMMddHHmmss"));
				
				List list = getService().selectListByQueryKey("list_OVER_SIZE_VM", param);
				param.put("LIST", list);
				return param;
				 
			}

		}).run(request);
			 
	}
}