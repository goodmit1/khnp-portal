package com.clovirsm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.admin.FabSettingService;
import com.fliconz.fm.mvc.DefaultController;

@RestController
@RequestMapping(value =  "/fab_setting" )
public class FabSettingController extends DefaultController {

	@Autowired FabSettingService service;
	@Override
	protected FabSettingService getService() {
		return service;
	}

}
