package com.clovirsm.controller.monitor;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.monitor.UseHistoryService;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;

@RestController
@RequestMapping(value =  "/use_history" )
public class UseHistoryController extends DefaultController {
	
	@Autowired UseHistoryService service;
	@Override
	protected DefaultService getService() {
		return service;
	}
	
	@RequestMapping(value =  "/history" )
	public List monitorList(final HttpServletRequest request, HttpServletResponse response )
	{
		return (List)(new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return service.selectListByQueryKey("history_NC_MM_STAT" , param);
				 
			}

		}).run(request);
			 
	}
		
}
