package com.clovirsm.controller.monitor;

import java.util.HashMap;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.monitor.MonitorService;
import com.clovirsm.service.workflow.WorkService;
import com.clovirsm.sys.hv.vmware.VROpsAction;
import com.fliconz.fm.mvc.ActionRunner;
import com.fliconz.fm.mvc.DefaultController;
import com.fliconz.fm.mvc.DefaultService;
import com.fliconz.fw.runtime.util.ListUtil;
import com.fliconz.fw.runtime.util.MapUtil;


@RestController
@RequestMapping(value =  "/monitor" )
public class MonitorController extends DefaultController {

	@Autowired MonitorService service;
	@Autowired WorkService workService;
	@Override
	protected DefaultService getService() {
		return service;
	}



	@RequestMapping(value =  "/alarm/{dcId}/{type}/{name}" )
	public Object alarmList(final HttpServletRequest request, HttpServletResponse response,@PathVariable("dcId") final String dcId
			, @PathVariable("type") final String type, @PathVariable("name") final String name)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				 
				return service.getAlarmList(dcId, type, name);

			}

		}).run(request);

	}
	@RequestMapping(value =  "/event/{dcId}/{type}/{name}" )
	public Object eventList(final HttpServletRequest request, HttpServletResponse response,@PathVariable("dcId") final String dcId
			, @PathVariable("type") final String type, @PathVariable("name") final String name)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return service.getEventList(dcId, type, name);

			}

		}).run(request);

	}
	@RequestMapping(value =  "/perf_history_type/{hv_cd}/{type}" )
	public Object getPerfHistoryTypes (final HttpServletRequest request, HttpServletResponse response,@PathVariable("hv_cd") final String hv_cd
			, @PathVariable("type") final String type )
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				Map result = new HashMap();
				result.put("type",  service.getPerfHistoryTypes(hv_cd, type ));
				return result;

			}

		}).run(request);
	}
	@RequestMapping(value =  "/perf_history/{dcId}/{type}/{name}" )
	public Object perfHistoryList(final HttpServletRequest request, HttpServletResponse response,@PathVariable("dcId") final String dcId
			, @PathVariable("type") final String type, @PathVariable("name") final String name)
	{
		return (new ActionRunner() {

			@Override
			protected Object run(Map param) throws Exception {
				return service.getPerfHistoryList((String)param.get("PERIOD"), dcId, type, name, param);

			}

		}).run(request);

	}


	@RequestMapping(value =  "/task" )
	public Object taskList(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				Map tmp = new HashMap();
				List list = service.selectListByQueryKey("list_NC_TASK_ING", param);
				service.addCodeNames(list);
				tmp.put("LIST", list);
				return tmp;
			}
		}).run(request);
	}

	@RequestMapping(value =  "/get_qna_req" )
	public Object getQnaReq(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				Map tmp = new HashMap();
				List list = service.selectListByQueryKey("list_NC_INQUIRY", param);
				tmp.put("QNA_CNT", list);
				return tmp;
			}
		}).run(request);
	}
	
	
	@RequestMapping(value =  "/get_qna_req_cnt" )
	public Object getQnaReqCnt(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				Map tmp = new HashMap();
				int qna = service.selectCountByQueryKey("get_qna_cnt", param);
				int req = 0;
				int work = 0;
				req = service.selectListByQueryKey("com.clovirsm.workflow.approval.Approval", "list_NC_REQ", param).size();
				work = workService.selectCountByQueryKey("list_NC_REQ_CNT", param);
				
				tmp.put("QNA_CNT", qna);
				tmp.put("REQ_CNT", req);
				tmp.put("WORK_CNT", work);
				return tmp;
			}
		}).run(request);
	}

	@RequestMapping(value =  "/get_fee_vm_cnt" )
	public Object getFeeVmCnt(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				Map tmp = new HashMap();
				int teamFee = service.selectCountByQueryKey("get_TEAM_FEE", param);
				int priFee = service.selectCountByQueryKey("get_PRI_FEE", param);
				int teamCnt = service.selectCountByQueryKey("get_TEAM_VM_CNT", param);
				int priCnt = service.selectCountByQueryKey("get_PRI_VM_CNT", param);
				tmp.put("TEAM_FEE", teamFee);
				tmp.put("PRI_FEE", priFee);
				tmp.put("TEAM_VM_CNT", teamCnt);
				tmp.put("PRI_VM_CNT", priCnt);
				return tmp;
			}
		}).run(request);
	}

	@RequestMapping(value =  "/alarm/read" )
	public Object alarmRead(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				String listStr = (String)param.get("list");
				List<Map> list = ListUtil.makeJson2List(listStr);
				if(list.size() != 0){
					param.put("list", list);
					service.updateByQueryKey("alarm_read", param);
				}
				return new HashMap();
			}
		}).run(request);
	}
	
	
	@Autowired VROpsAction vropsAction; 
	@RequestMapping(value =  "/vrops/meta" )
	public Object vropsMeta(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				return vropsAction.getMetricMeta(param);
			}
		}).run(request);
	}
	@RequestMapping(value =  "/vrops/data" )
	public Object vropsData(final HttpServletRequest request, HttpServletResponse response) throws Exception {
		return (new ActionRunner() {
			@Override
			protected Object run(Map param) throws Exception {
				Map result =  vropsAction.getMetricData(param);
				param.putAll(result);
				return param;
			}
		}).run(request);
	}
}
