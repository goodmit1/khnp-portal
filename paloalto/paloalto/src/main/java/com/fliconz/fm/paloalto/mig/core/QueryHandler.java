package com.fliconz.fm.paloalto.mig.core;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.fliconz.fm.common.util.DataMap;
import com.fliconz.fm.paloalto.core.PaloaltoAPIException;
import com.fliconz.fm.paloalto.log.LogFileManager;

public class QueryHandler {

	protected Connection conn;

	public QueryHandler(DataMap<String, Object> connParam){
		this(false, connParam);
	}

	public QueryHandler(boolean isAutoCommit, DataMap<String, Object> connParam){
		try {
			this.conn = DBClientFactory.getConnection(connParam);
			this.conn.setAutoCommit(isAutoCommit);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int update(String query, Map<String, Object> param, String[] paramKeys) throws Exception {
		PreparedStatement pstmt = null;
		int rs = -1;
		try {
			pstmt = this.conn.prepareStatement(query);
			int paramIndex = 0;
			for(String paramKey: paramKeys){
				pstmt.setObject(++paramIndex, param.get(paramKey));
			}
			rs = pstmt.executeUpdate();
			LogFileManager.makeQueryLogFile(query, param, paramKeys, rs);
			pstmt.close();
		} catch(Exception e){
			if(pstmt != null){
				try {
					pstmt.close();
				} catch (SQLException e2) {
					throw new PaloaltoAPIException(e.getMessage() + e2.getMessage());
				}
			}
			throw e;
		}
		return rs;
	}

	public DataMap<String, Object> excute(String query, DataMap<String, Object> param, String[] resultKeys, int[] pks) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		DataMap<String, Object> resultMap = new DataMap<String, Object>();
		try {
			pstmt = this.conn.prepareStatement(query);
			rs = pstmt.executeQuery();

			int cnt = 0;
			while(rs.next()){
				cnt++;
				DataMap<String, Object> result = new DataMap<String, Object>();
				result.put("_ROW_", cnt);
				if(resultKeys != null){
					for(int i = 0; i < resultKeys.length; i++){
						try {
							result.put(resultKeys[i], rs.getObject(resultKeys[i]));
						} catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				String pk = "";
				for(int i = 0; i < pks.length; i++){
					if(i > 0) pk += "_";
					pk += rs.getObject(resultKeys[pks[i]]);
				}
				resultMap.put(pk, result);
			}
			rs.close();
			pstmt.close();
			LogFileManager.makeQueryLogFile(query, param, resultKeys, resultMap);
		} catch(Exception e){
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
			} catch (SQLException e2) {
				throw new PaloaltoAPIException(e.getMessage() + e2.getMessage());
			}
			throw e;
		}
		return resultMap;
	}

	public void rollback(){
		try {
			if(this.conn != null) this.conn.rollback();
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	public void commit(){
		try {
			if(this.conn != null) this.conn.commit();
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	public void finaliza(){
		try {
			if(this.conn != null) this.conn.close();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
