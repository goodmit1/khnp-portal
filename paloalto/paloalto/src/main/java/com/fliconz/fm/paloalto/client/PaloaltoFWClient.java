package com.fliconz.fm.paloalto.client;

import java.util.Map;

import org.apache.log4j.LogManager;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.paloalto.core.PaloaltoAPIHelper;
import com.fliconz.fw.runtime.util.PropertyManager;

public class PaloaltoFWClient {

	protected static final String FIREWALL_IP = PropertyManager.getString("paloalto.firewall.ip", "10.230.253.28");
	// protected static final String API_URL = "http://localhost:8080/test/test.xml";
	protected static final String API_URL = "https://" + FIREWALL_IP + "/api/";

	private static final String FORMAT_DESTINATION_ADDRESS_GROUP = "%s";
	private static final String FORMAT_SOURCE_ADDRESS_GROUP = "%s_USER";
	private static final String FORMAT_SOURCE_USER = "mobis\\%s";
	private static final String FORMAT_SERVICE_GROUP = "%s_PORT";
	private static final String FORMAT_SERVICE= "TCP_%s";

	private static final String[] DEFAUlT_SERVICES = {"RDP","SSH"};//3389, 22
	public static final String[] DEFAUlT_PORT = {"3389","22"};//3389, 22
	private String svrName;
	private String url;
	private String apiKey;

	private Map prop;
	private org.apache.log4j.Logger log = LogManager.getLogger(this.getClass());

	public PaloaltoFWClient(String url, String userId, String pwd, String svrName, String[] serverIps, Map prop) throws Exception {
		log.info("------------------- PaloaltoFWClient.Constructor ------------------- ");
		log.info("svrName=" + svrName);
		log.info("url=" + url);
		log.info("userId=" + userId);
		log.info("prop=" + prop);
		log.info("------------------------------------------------ ");
		this.svrName = svrName;
		this.url = url;
		this.prop = prop;
		if(this.url != null && !"".equals(this.url) && !this.url.endsWith("/api/")){
			this.url += "/api/";
		}

		this.init(userId, pwd, serverIps);
	}

	/**
	 * 초기화: 지정된 url과 userId, pwd를 이용하여 해당 서버에서 apiKey를 발급받은 후, 현재 clorvirSM에서
	 * 요청된 VM에 대하여 룰이 없으면 생성해준다.
	 *
	 * @throws Exception
	 */
	private void init(String userId, String pwd, String[] serverIps) throws Exception {
		this.apiKey = PaloaltoAPIHelper.getAPIKey(url, userId, pwd);

		log.info("------------------- PaloaltoFWClient.init ------------------- ");
		log.info("apiKey=" + apiKey);
		log.info("serverIps=" + serverIps);
		log.info("------------------------------------------------ ");

		if(!PaloaltoAPIHelper.existsSecurityRule(url, apiKey, svrName)){
			// 1. Security Rule 생성
			// 2. destination 생성
			PaloaltoAPIHelper.createSecurityRule(url, apiKey, svrName, prop);
		}
			//address 객체를 destination으로 설정한다.
		PaloaltoAPIHelper.setDestinationAddress(url, apiKey, svrName, FORMAT_DESTINATION_ADDRESS_GROUP, serverIps);

			//기본 서비스  그룹 및 서비스를 추가
		//PaloaltoAPIHelper.setRuleServiceGroup(url, apiKey, svrName, String.format(FORMAT_SERVICE_GROUP, svrName), DEFAUlT_SERVICES);
		PaloaltoAPIHelper.addServiceToPolicy(url, apiKey, svrName, DEFAUlT_SERVICES);
		
	}

	/**
	 * 접근제어 추가
	 *
	 * @param serverIp
	 * @param clientIp
	 * @param ports
	 * @param sabun
	 * @throws Exception
	 */
	public void insert(String[] clientIps, String[] ports, String sabun) throws Exception {
		log.info("------------------- insert ------------------- ");
		log.info("svrName=" + svrName);
		log.info("clientIps=" + clientIps);
		log.info("ports=" + TextHelper.join(ports));
		log.info("sabun=" + sabun);
		log.info("------------------------------------------------ ");

		//ip 설정 & 사용자 설정
		PaloaltoAPIHelper.setSourceData(url, apiKey, svrName, FORMAT_SOURCE_ADDRESS_GROUP, clientIps, sabun, FORMAT_SOURCE_USER);

		// 포트 추가
		//PaloaltoAPIHelper.addServices(url, apiKey, String.format(FORMAT_SERVICE_GROUP, svrName), ports, FORMAT_SERVICE);
		PaloaltoAPIHelper.addServicesToPolicy(url, apiKey,  svrName , ports, FORMAT_SERVICE);
	}

	/**
	 * 허용 IP 추가
	 * @param ports
	 * @throws Exception
	 */
	public void insertServerIp(String[] serverIps) throws Exception {
		//ip 추가 및 서비스 그룹에 추가된 IP 어드레스 매핑
		PaloaltoAPIHelper.addDestinationData(url, apiKey, svrName, FORMAT_DESTINATION_ADDRESS_GROUP, serverIps);
	}

	/**
	 * 허용 IP 추가
	 * @param ports
	 * @throws Exception
	 */
	public void insertClientIp(String[] clientIps) throws Exception {
		//ip 추가 및 서비스 그룹에 추가된 IP 어드레스 매핑
		PaloaltoAPIHelper.addSourceData(url, apiKey, svrName, FORMAT_SOURCE_ADDRESS_GROUP, clientIps);
	}

	/**
	 * 포트 추가
	 *
	 * @param ports
	 * @throws Exception
	 */
	public void insertPort(String[] ports) throws Exception {
		log.info("------------------- insertPort ------------------- ");
		log.info("svrName=" + svrName);
		log.info("ports=" + TextHelper.join(ports));
		log.info("------------------------------------------------ ");

		// 포트 설정
		//PaloaltoAPIHelper.addServices(url, apiKey, String.format(FORMAT_SERVICE_GROUP, svrName), ports, FORMAT_SERVICE);
		PaloaltoAPIHelper.addServicesToPolicy(url, apiKey, this.svrName, ports, FORMAT_SERVICE);
	}

	/**
	 * 사번(사용자) 추가
	 *
	 * @param sabun
	 * @throws Exception
	 */
	public void insertSabun(String sabun) throws Exception {
		log.info("------------------- insertSabun ------------------- ");
		log.info("svrName=" + svrName);
		log.info("sabun=" + sabun);
		log.info("------------------------------------------------ ");

		//사번 등록
		String userName = String.format(FORMAT_SOURCE_USER, sabun);
		PaloaltoAPIHelper.setSourceUser(url, apiKey, svrName, userName);
	}

	/**
	 * 서버 삭제
	 *
	 * @param svrName
	 * @throws Exception
	 */
	public void deleteServer() throws Exception {
		log.info("------------------- deleteServer ------------------- ");
		log.info("svrName=" + svrName);
		log.info("------------------------------------------------ ");

		//security rule 삭제
		PaloaltoAPIHelper.deleteSecurityRule(url, apiKey, svrName);

		PaloaltoAPIHelper.deleteAddress(url, apiKey, String.format(FORMAT_DESTINATION_ADDRESS_GROUP, svrName));
		//destination:serverIp address-group 삭제
		PaloaltoAPIHelper.deleteAddressGroup(url, apiKey, String.format(FORMAT_DESTINATION_ADDRESS_GROUP, svrName));

		PaloaltoAPIHelper.deleteAddress(url, apiKey, String.format(FORMAT_SOURCE_ADDRESS_GROUP, svrName));
		//source:clientIp address-group 삭제
		PaloaltoAPIHelper.deleteAddressGroup(url, apiKey, String.format(FORMAT_SOURCE_ADDRESS_GROUP, svrName));

		//service:port service group 삭제
		PaloaltoAPIHelper.deleteServiceGroup(url, apiKey, String.format(FORMAT_SERVICE_GROUP, svrName));
	}

	/**
	 * Client IP삭제
	 * @param clientIp
	 * @throws Exception
	 */
	public void deleteServerIp(String[] serverIps) throws Exception {
		log.info("------------------- deleteClientIp ------------------- ");
		log.info("svrName=" + svrName);
		log.info("serverIps=" + serverIps);
		log.info("------------------------------------------------ ");

		//address group에서 해당 client ip를 가진 address memeber 제거
		PaloaltoAPIHelper.removeAddressFromGroup(url, apiKey, String.format(FORMAT_DESTINATION_ADDRESS_GROUP, svrName), serverIps);
	}

	/**
	 * Client IP삭제
	 * @param clientIp
	 * @throws Exception
	 */
	public void deleteClientIp(String[] clientIps) throws Exception {
		log.info("------------------- deleteClientIp ------------------- ");
		log.info("svrName=" + svrName);
		log.info("clientIps=" + clientIps);
		log.info("------------------------------------------------ ");

		//address group에서 해당 client ip를 가진 address memeber 제거
		PaloaltoAPIHelper.removeAddressFromGroup(url, apiKey, String.format(FORMAT_SOURCE_ADDRESS_GROUP, svrName), clientIps);
	}

	/**
	 * 포트 삭제
	 *
	 * @param ports
	 * @throws Exception
	 */
	public void deletePort(String[] ports) throws Exception {
		log.info("------------------- deletePort ------------------- ");
		log.info("svrName=" + svrName);
		log.info("ports=" + TextHelper.join(ports));
		log.info("------------------------------------------------ ");

		// 포트 삭제
		PaloaltoAPIHelper.removeServices(url, apiKey, String.format(FORMAT_SERVICE_GROUP, svrName), ports, FORMAT_SERVICE);
	}

	/**
	 * 사번 삭제
	 *
	 * @param sabun
	 * @throws Exception
	 */
	public void deleteSabun(String sabun) throws Exception {
		log.info("------------------- deleteSabun ------------------- ");
		log.info("svrName=" + svrName);
		log.info("sabun=" + sabun);
		log.info("------------------------------------------------ ");

		// 사용자 삭제
		PaloaltoAPIHelper.removeSourceUser(url, apiKey, svrName, String.format(FORMAT_SOURCE_USER, sabun));
	}

}