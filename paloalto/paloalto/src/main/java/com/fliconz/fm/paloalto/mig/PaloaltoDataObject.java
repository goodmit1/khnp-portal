package com.fliconz.fm.paloalto.mig;

import java.io.Serializable;
import java.util.List;

import com.fliconz.fm.common.util.DataMap;

public class PaloaltoDataObject implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<DataMap<String, Object>> paloaltoDataList;

	public PaloaltoDataObject(List<DataMap<String, Object>> paloaltoDataList) {
		this.paloaltoDataList = paloaltoDataList;
	}

	public List<DataMap<String, Object>> getData(){
		return this.paloaltoDataList;
	}
}
