package com.clovirsm.khnp.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.clovirsm.common.IAsyncJob;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.service.site.SiteService;
import com.clovirsm.service.site.thinClnt.ThinClntService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service("KhnpVMService")
public class KhnpVMService extends VMService implements IAsyncJob {

    @Override
    protected void onAfterInsertReq(String tableNm, Map param) throws Exception {
        super.onAfterInsertReq(tableNm, param);

        int result = 0;
        if ("NC_VM_REQ".equals(tableNm)) {
            if (param.get("THIN_REQ_YN") != null) {
                SiteService siteService = (SiteService) SpringBeanUtil.getBean("siteService");
                result = siteService.updateByQueryKey("update_NC_VM_REQ_THIN_REQ", param);
                if (result == 0) {
                    throw new RuntimeException("씬 클라이언트 신청 내역 저장에 실패하였습니다.");
                }
            }
        }
    }

    @Override
    protected int insertDBTable(String tableNm, Map param) throws Exception {
        int result = super.insertDBTable(tableNm, param);
        if ("NC_VM".equals(tableNm)) {
            if (param.get("THIN_REQ_YN") != null) {
                SiteService siteService = (SiteService) SpringBeanUtil.getBean("siteService");
                result = siteService.updateByQueryKey("update_NC_VM_THIN_REQ", param);
                if (result == 0) {
                    throw new RuntimeException("씬 클라이언트 내역 저장에 실패하였습니다.");
                }
            }
        }
        return result;
    }

    @Override
    protected int updateDBTable(String tableNm, Map param) throws Exception {
        int resultResult = super.updateDBTable(tableNm, param);


        if (resultResult > 0) {
            int result = 0;
            if ("NC_VM".equals(tableNm)) {
                ThinClntService thinClntService = (ThinClntService) SpringBeanUtil.getBean("thinClntService");
                deleteVmThinClientReset(param, thinClntService);
                //씬 클라이언트 수정
                if (param.get("SN") != null && !"".equals(param.get("SN"))) {
                    if (thinClntService != null) {

                        result = 0;
                        //기존에 할당되었던 THIN CLIENT가 있으면 초기화
                        if (!param.get("ORG_SN").equals("") && !param.get("ORG_SN").equals(param.get("SN"))) {
                            result = thinClntService.updateByQueryKey("init_NC_THIN_CLIENT", param);
                            if (result == 0) {
                                throw new RuntimeException("기존 씬 클라이언트 초기화에 문제가 발생했습니다.");
                            }
                        }

                        if ("".equals(param.get("ORG_IP")) || param.get("ORG_IP") == null || !param.get("ORG_IP").equals(param.get("IP"))
                                || "".equals(param.get("ORG_IP")) || param.get("ORG_PORT") == null || !param.get("ORG_PORT").equals(param.get("PORT"))) {
                            if (param.get("ORG_SN") != null && !"".equals(param.get("ORG_SN")) && param.get("ORG_SN").equals(param.get("SN"))) {
                                result = thinClntService.updateByQueryKey("update_vm_NC_THIN_CLIENT", param);
                                if (result == 0) {
                                    throw new RuntimeException("씬 클라이언트 정보 변경에 문제가 발생했습니다.");
                                }
                            } else {
                                result = thinClntService.updateByQueryKey("bind_vm_NC_THIN_CLIENT", param);
                                if (result == 0) {
                                    throw new RuntimeException("씬 클라이언트 할당에 문제가 발생했습니다.");
                                }
                            }
                        }
                    }
                }
            }
        }
        return resultResult;
    }

    public void deleteVmThinClientReset(Map param, ThinClntService thinClntService) throws Exception {
        int result = 0;
        if (!param.containsKey("CUD_CD")) return;

        Map info = getThinClientInfo(param, thinClntService);
        if (!(boolean) info.get("THIN_CLIENT_CHECK")) return;

        if (param.get("CUD_CD").equals("D") && param.get("THIN_REQ_YN").equals("Y")) {
            String sn = info.get("SN").toString();
            param.put("ORG_SN", sn);
            result = thinClntService.updateByQueryKey("init_NC_THIN_CLIENT", param);
            if (result == 0) {
                throw new RuntimeException("기존 씬 클라이언트 초기화에 문제가 발생했습니다.");
            }
        }
    }

    private Map getThinClientInfo(Map param, ThinClntService thinClntService) throws Exception {
        Map<String, Object> info;
        info = thinClntService.getInfo(param);

        if (info == null || info.isEmpty()) {
			info = new HashMap<>();
            info.put("THIN_CLIENT_CHECK", false);
        }
        info.put("THIN_CLIENT_CHECK", info.containsKey("SN"));
        return info;
    }
}

