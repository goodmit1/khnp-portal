package com.clovirsm.service.site.thinClnt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;

@Service
public class ThinClntService extends NCDefaultService {

	protected String getNameSpace() {
		return "com.clovirsm.admin.thinClnt";
	}

	@Override
	public String[] getPks() {
		return new String[]{"SN"};
	}

	@Override
	protected String getTableName() {
		return "NC_THIN_CLIENT";
	}

	@Override
	public List list(Map searchParam) throws Exception {
		// TODO Auto-generated method stub
		List  result = super.list(searchParam);
		System.out.println("thin client list: "+result);
		return result;
	}

	@Override
	protected int insertDBTable(String tableNm, Map param) throws Exception {
		return super.insertDBTable(tableNm, param);
	}

	@Override
	protected Map<String, String> getCodeConfig() {
		Map<String, String> conf = new HashMap<String, String>();
		conf.put("STATUS", "CLIENT_STATUS");
		return conf;
	}

}