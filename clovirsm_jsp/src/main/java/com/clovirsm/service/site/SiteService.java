package com.clovirsm.service.site;

import java.util.*;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;
import com.clovirsm.service.resource.VraCatalogService;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class SiteService extends NCDefaultService {

	public static String NS="com.clovirsm.site";
	protected String getNameSpace() {
		return NS;
	}
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String[] getPks() {
		// TODO Auto-generated method stub
		return null;
	}

	public void vraRequest(Map param) throws Exception {
		// DATA_JSON: {"vm_count":"1","_svc":"AIR","_fab":"m14","_p_kubun":"qa","_purpose":"web"}
		
		JSONObject dataJson = new JSONObject();
		dataJson.put("_count_vm", param.get("_count_vm"));
		dataJson.put("_svc", param.get("CATEGORY_CODE"));
		dataJson.put("_fab", param.get("FAB"));
		dataJson.put("_p_kubun", param.get("P_KUBUN"));
		dataJson.put("_purpose", param.get("S_PURPOSE"));
		
		try {
			Map dcInfo = this.selectOneByQueryKey("com.clovirsm.monitor", "select_dc_by_fab", param);
			if(dcInfo !=null) {
				param.put("DC_ID", dcInfo.get("DC_ID"));
				if( dcInfo.get("ZONE_TAG") != null) {
					dataJson.put("_zone_tag", dcInfo.get("ZONE_TAG"));
				}
			}
			param.put("DATA_JSON", dataJson.toString());
			VraCatalogService catalogService = (VraCatalogService)SpringBeanUtil.getBean("vraCatalogService");
			System.out.println(param);
			catalogService.insertReq1(param, true);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}


	public void previousIpUpdate(String targetTable, String updateTable) throws Exception {
		Map<String, Object> param = new HashMap<>();
		List<Map<String, Object>> prevFwCidrList = this.selectListByQueryKey(targetTable,param);
		int row = 0;
		for (Map<String, Object> p : prevFwCidrList) {
			try {
				Set<String> ips = new HashSet();
				String oldCidr = (String) p.get("CIDR");
//				param.put("FW_USER_ID",	"7815");
				param.put("FW_USER_ID", p.get("FW_USER_ID"));
				Map<String, Object> afterInfo = this.selectOneByQueryKey("selectBeforeInfo", param);

				//ip담아서 보낼값
				//befortable beforip
				String prvIps = (String) afterInfo.get("cidr_before");
				String nextIps = (String) afterInfo.get("cidr_after");

				//nc_fw cidr을 담음
				addSet(ips, ipArrays(oldCidr));
				//before ip를 제거
				reomoveSet(ips, ipArrays(prvIps));

				if (isExist(oldCidr, prvIps)){
					addSet(ips, ipArrays(nextIps));
				}

				//after ip추가
				System.out.println("cidr set : " + ips);
				//변경된 ip를 담아서 보냄 "[]" 제거후
				param.put("NEXT_CIDR", ips.toString().replaceAll("[\\[\\]]", ""));
				param.put("FW_ID", p.get("FW_ID"));
				//nc_fw cidr update
				int resultRow = this.updateByQueryKey(updateTable, param);
				row += resultRow;
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("result row : "+row);

		}
	}

	public static String[] ipArrays(String ip) {
		String arr[];
		arr = ip.replaceAll(" ", "").split(",");
		return arr;
	}

	public Set addSet(Set set, String[] ips){
//        Collections.addAll(set, ips);
		for (String ip : ips) {
			set.add(ip);
		}
		return set;
	}
	public Set<String> reomoveSet(Set set, String[] ips){
		for (String ip : ips) {
			 set.remove(ip);
		}
		return set;
	}

	public boolean isExist(String oldCidr, String prvIps){
		boolean bol = false;
		for (String s : ipArrays(oldCidr)) {
			for (String ipArray : ipArrays(prvIps)) {
				if(s.equals(ipArray)){
					bol=true;
					break;
				}
			}
		}
		return bol;
	}
}
