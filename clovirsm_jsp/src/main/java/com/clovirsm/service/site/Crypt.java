package com.clovirsm.service.site;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;


public class Crypt {
    private static String IV = "IV_VALUE_16_BYTE";
    private static String PASSWORD = "GOODMORNING123!@#"; //숫자 문자 혼합 + 페이지
    private static String SALT = "CLOVIRSM";


    public static void main(String[] args) {
        Crypt c = new Crypt();
        try {
            //암호화
            String decode = c.encryptAndEncode("test20201022");
            System.out.println("decode : "+decode);
            //복호화
            System.out.println(c.decodeAndDecrypt(decode));
        } catch (Exception e) {
            // TODO Auto- generatedcatch block
            e.printStackTrace();
        }
    }
    public String encryptAndEncode(String raw) {
        try {
            Cipher c = getCipher(Cipher.ENCRYPT_MODE);
            byte[] encryptedVal = c.doFinal(getBytes(raw));
            String s = DatatypeConverter.printBase64Binary(encryptedVal);
//            String s = getString(Base64.getEncoder().encode(encryptedVal));
            return s;
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    public String decodeAndDecrypt(String encrypted) throws Exception {
//        byte[] decodedValue = Base64.getDecoder().decode(getBytes(encrypted));
        byte[] decodedValue = DatatypeConverter.parseBase64Binary(encrypted);
        Cipher c = getCipher(Cipher.DECRYPT_MODE);
        byte[] decValue = c.doFinal(decodedValue);
        return new String(decValue);
    }

    private String getString(byte[] bytes) throws UnsupportedEncodingException {
        return new String(bytes, "UTF-8");
    }

    private byte[] getBytes(String str) throws UnsupportedEncodingException {
        return str.getBytes("UTF-8");
    }

    private Cipher getCipher(int mode) throws Exception {
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        byte[] iv = getBytes(IV);
        c.init(mode, generateKey(), new IvParameterSpec(iv));
        return c;
    }

    private Key generateKey() throws Exception {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        char[] password = PASSWORD.toCharArray();
        byte[] salt = getBytes(SALT);

        KeySpec spec = new PBEKeySpec(password, salt, 65536, 256);
        SecretKey tmp = factory.generateSecret(spec);
        byte[] encoded = tmp.getEncoded();
        return new SecretKeySpec(encoded, "AES");
    }

}