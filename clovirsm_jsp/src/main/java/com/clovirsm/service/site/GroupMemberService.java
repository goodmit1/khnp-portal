package com.clovirsm.service.site;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.clovirsm.common.NCDefaultService;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

@Service
public class GroupMemberService extends NCDefaultService {
	@Autowired
	  private PasswordEncoder passwordEncoder;

	  @Autowired
	  private SaltSource saltSource;

	protected String getNameSpace() {
		return "com.clovirsm.myinfo.group.GroupMember";
	}
	public void updateMoveFromLegacy(ResultSet rs) throws Exception
	{
		 
		//SqlSessionTemplate sqlSession = (SqlSessionTemplate)SpringBeanUtil.getBean("sqlSessionTemplateBatch");
		Long maxId = (Long)this.selectOneObjectByQueryKey("get_max_id", new HashMap());
		this.updateByQueryKey(  "update_all_unuse", new HashMap());
		ArrayList updateList = new ArrayList();
		ArrayList insertList = new ArrayList();
		while(rs.next())
		{
			Map<String, Object> param = new HashMap();
			param.put("USE_YN", "Y");
			
			param.put("TEAM_CD", rs.getObject("TEAM_CD"));
			param.put("LOGIN_ID", rs.getObject("LOGIN_ID"));
			param.put("USER_NAME", rs.getObject("USER_NAME"));
			param.put("POSITION", rs.getObject("POSITION"));
			param.put("EMAIL", rs.getObject("EMAIL"));
			Map info = (Map)this.selectOneByQueryKey( "select_FM_USER_by_LOGIN_ID", param);
			if(info != null && info.size()>0)
			{
				
				param.put("USER_ID", info.get("USER_ID"));
				updateList.add(param);
			}
			else
			{
				setInsertParam(param );
				param.put("USER_ID", ++maxId);
				insertList.add(param);
			}
		}
		rs.close();
		if(updateList.size()>0)
		{
			executeBatch("update_FM_USER_batch", updateList);
			 
		}
		if(insertList.size()>0)
		{
			executeBatch("insert_FM_USER_batch", insertList);
			 
		}
		
	
	}
	
	public void onAfterUserBatch( ) throws Exception {
		 
			Map param = new HashMap();
			this.updateByQueryKey("update_default_team", param);
			this.updateByQueryKey("update_team_NC_VM", param);
			this.updateByQueryKey("update_team_NC_IMG", param);
			param.put("LEAVE_CMT", MsgUtil.getMsg("retire", null));
			this.updateByQueryKey("update_leavers_NC_FW", param);
	}
	private void executeBatch(String key, List list) throws Exception
	{
		int i=0;
		while(i<list.size())
		{
			Map param = new HashMap();
			int idx = i + 1000;
			if(idx>list.size())
			{
				idx = list.size();
			}
			if(i==idx) break;
			param.put("list", list.subList(i, idx));
			this.updateByQueryKey(  key, param);
			i = idx;
		}
	}
	
	protected int updateDBTable(String tableNm, Map param) throws Exception{
		addCommonParam(param);
		if(param.containsKey("OLD_USER_ID") && !param.get("USER_ID").toString().equals(param.get("OLD_USER_ID").toString())){
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("USER_ID", param.get("OLD_USER_ID"));
			getWriteDAO().update(String.format("%s.%s%s", getNameSpace(), "delete_", tableNm), paramMap);
		}
		return getWriteDAO().update(String.format("%s.%s%s", getNameSpace(), "update_", tableNm), param);
	}
	public int update(Map param) throws Exception
	{
		return this.updateDBTable("FM_USER", param);
	}
	protected void setInsertParam(Map param ) throws Exception
	{
		//addCommonParam(param);
		//param.put("USER_ID", System.nanoTime());
		
		UserVO vo = new UserVO(param);

		String hashedPassword = (String)param.get("PASSWORD");
		if(hashedPassword==null)
		{
			hashedPassword = (String)param.get("LOGIN_ID");

		}
		hashedPassword = this.passwordEncoder.encodePassword(hashedPassword, this.saltSource.getSalt(vo));
		   
		param.put("PASSWORD", hashedPassword);
		 
	}
	public static void main(String[] args)
	{
		java.util.List<String> fields = new ArrayList();
		System.out.println(fields.subList(0, 100));
	}
	@Override
	protected String getTableName() {

		return "FM_USER";
	}
	@Override
	public String[] getPks() {
		return new String[]{"USER_ID"};
	}
}