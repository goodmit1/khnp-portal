package com.clovirsm.service.site;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fliconz.fm.common.util.TextHelper;
import com.thoughtworks.xstream.alias.ClassMapper.Null;
import com.clovirsm.common.NCDefaultService;

@Service
public class GroupService extends NCDefaultService {
	protected String getNameSpace() {
		return "com.clovirsm.myinfo.group.Group";
	}

	public void updateMoveFromLegacy(ResultSet rs) throws Exception
	{
		Map<String, Object> param = new HashMap();
		this.updateByQueryKey("update_all_unuse", param);
		boolean isFirst = true;
		while(rs.next())	{

			param.put("USE_YN", "Y");
			param.put("COMP_ID", "102216547406718");
			param.put("TEAM_CD", rs.getObject("TEAM_CD"));
			Object parent = rs.getObject("PARENT_CD");
			if(parent == null) {
				parent = "0";
			}
			param.put("PARENT_CD", parent);
			param.put("TEAM_NM", rs.getObject("TEAM_NM"));
			//param.put("UPD_TMS", rs.getObject("UPD_TMS"));
			int row = updateByQueryKey("update_FM_TEAM", param);
			if(row==0)	{
				row = updateByQueryKey("insert_FM_TEAM", param);
			}
			try	{
				if(!TextHelper.isEmpty(rs.getString("TEAM_OWNER"))) {
					if(isFirst) {
						this.updateByQueryKey("update_usertype_teamjang_reset", param);
						isFirst = false;
					}
					param.put("TEAM_OWNER", rs.getObject("TEAM_OWNER"));
					this.updateByQueryKey("update_usertype_teamjang", param);
					
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		updateByQueryKey("updateInvalidParent", param);
	}
	@Override
	protected int insertDBTable(String tableNm, Map param) throws Exception {

		if(!TextHelper.isEmpty((String)param.get("TEAM_OWNER")) && tableNm.equals("FM_TEAM"))
		{
			updateByQueryKey("updateOtherTeamOwner", param);
		}
		if(tableNm.equals("FM_TEAM"))
		{
			if(param.get("TEAM_CD") == null || "".equals(param.get("TEAM_CD")))
			{
//				param.put("TEAM_CD", System.currentTimeMillis());
				Map map = selectOneByQueryKey("select_FM_TEAM_CD_MAX",param);
				String max = map.get("CD_MAX").toString();
				param.put("TEAM_CD", max);
			}
		}
		int row =  super.insertDBTable(tableNm, param);
		if(!TextHelper.isEmpty((String)param.get("TEAM_OWNER")) && tableNm.equals("FM_TEAM"))
		{
			updateByQueryKey("updateUserTeam", param);
		}
		return row;
	}

	@Override
	protected int deleteDBTable(String tableNm, Map param) throws Exception {

		int row = super.deleteDBTable(tableNm, param);
		if(tableNm.equals("FM_TEAM"))
		{
			param.remove("TEAM_CD");
			updateByQueryKey("updateUserTeam", param);
		}
		return row;
	}

	@Override
	protected int updateDBTable(String tableNm, Map param) throws Exception {
		if(!TextHelper.isEmpty((String)param.get("TEAM_OWNER")) && tableNm.equals("FM_TEAM"))
		{
			updateByQueryKey("updateOtherTeamOwner", param); // 바뀐 팀장이 다른 팀장인 경우  그 팀 팀장 정보 clear
			updateByQueryKey("updateUserTeam", param); //바뀐 팀장의 팀코드 update
		}
		/*Map selectedParentTeam = this.selectOneByQueryKey("select_parent_FM_TEAM", param);
		if(selectedParentTeam != null && param.get("TEAM_CD").toString().equals(selectedParentTeam.get("PARENT_CD").toString())){
			this.updateByQueryKey("update_parent_FM_TEAM", param);
		}*/
		return super.updateDBTable(tableNm, param);
	}



	@Override
	protected String getTableName() {

		return "FM_TEAM";
	}

	@Override
	public String[] getPks() {

		return new String[]{"TEAM_CD"};
	}
}