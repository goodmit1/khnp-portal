package com.clovirsm.site;

import com.clovirsm.service.ComponentService;

import java.util.Map;

public class KhnpBeforeAfteVMWare extends SiteBeforeAfterVMWare {

    public KhnpBeforeAfteVMWare(){
        super();
    }

    @Override
    public String getFolderName( Map param) throws Exception{

        String osType = (String) param.get("LINUX_YN");
        String folderName = "Linux";
        if(osType.equals("N")){
            folderName = "Window";
        }
        return folderName;
    }


}
