package com.clovirsm.site;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.clovirsm.common.NCReqService;
import com.clovirsm.service.resource.VMService;
import com.clovirsm.service.site.SiteService;
import com.clovirsm.service.workflow.ApprovalService;
import com.clovirsm.service.workflow.DefaultNextApprover;
import com.clovirsm.service.workflow.DeployFailService;
import com.clovirsm.service.workflow.StepInfo;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.SpringBeanUtil;

public class HynixNextApprover extends DefaultNextApprover {
	protected String[] getReqSvcList() {
		return new String[] {"S","C","E", "V"};
	}
	protected void chgVMOwner(String vmId, String owner) throws Exception{
		VMService vmService = (VMService) NCReqService.getService("S");
		vmService.updateOwner(vmId, owner);
	}
	public List getExpireWarnMailTarget(  Map m, Map param, boolean isExpire) throws Exception{
		List result = super.getExpireWarnMailTarget(m, param, isExpire);
		try {
			List<Object> adminIds = service.selectListByQueryKey("com.clovirsm.monitor", "select_fab_admin", (Map)((List)param.get("S_list")).get(0));
			result.addAll(adminIds);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	@Override
	public void onAfterDeploy(Map param, List<Map> details) throws Exception {
		 
		for(Map m : details) {
			 
			if(!"C".equals(m.get("CUD_CD")) ) {
				chgVMOwner((String)m.get("SVC_ID"), param.get("INS_ID").toString());
			}
		}
		DeployFailService service = (DeployFailService)SpringBeanUtil.getBean("deployFailService");
		List failList = service.selectListByQueryKey("failMsgByReqId", param);
		String title = "mail_title_approve";
		if(failList != null && failList.size()>0) {
			param.put("FAIL_MSG", failList); 
			title = "mail_title_deploy_fail";
			service.sendMailToAdmin(title, "approve", param); //어드민에게 메일
			
		}
		
		service.sendMail(param.get("INS_ID"), title, "approve", param);
		
	}
	
	 
	
	public void sendMailToFABAdmin( String titleKey, String template,  Map paramMap) throws Exception
	{
		paramMap.put("EX_PORTAL_ADM", "Y");
		List<Object> adminIds = service.selectListByQueryKey("com.clovirsm.monitor", "select_fab_admin", paramMap);
		paramMap.remove("EX_PORTAL_ADM");
		paramMap.put("SendForAdmin", "Y");
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		for(Object id : adminIds) {
			 
			service.sendMail(id, titleKey, template, paramMap);
		}
		paramMap.remove("SendForAdmin");
	}
	@Override
	public StepInfo getNextApprover(int stepIdx, Map param) throws Exception {
		 
  
		StepInfo info =  super.getNextApprover(stepIdx, param);
		
		if(info != null && info.getStepName().equals("ROLE_FAB_MANAGER")) {
		 
			info.setApproverList(service.selectListByQueryKey("com.clovirsm.monitor", "select_fab_admin", param));
		}
		else if(info != null && info.getStepName().equals("ITOM")) {
			info.addApprover(-1);
		}
		return info;
		
	} 
}
