package com.clovirsm.site;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.service.ComponentService;
import com.clovirsm.service.site.SiteService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.NextIPHelper;
import com.clovirsm.sys.hv.vmware.BeforeAfterVMWare;
import com.fliconz.fm.common.util.StringUtil;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
 
public  class SiteBeforeAfterVMWare extends BeforeAfterVMWare{

	public SiteBeforeAfterVMWare( ) {
		super( );
		 
	}
	@Override
	public String getFolderName( Map param) throws Exception{
		getTeamCategoryCode(param) ;
		String pattern= (String) ComponentService.getEnv("vmFolder","{CODE}");
		return setVarVal(pattern, param);
	}
	protected int getCnt0(String code)
	{
		int cnt0 = 1;
		int pos = code.indexOf("0");
		if(pos<0) return 0;
		while(true) {
			if(code.length()==pos+cnt0 ||  code.charAt(pos+cnt0) != '0') {
				break;
			}
			cnt0++;
		}
		 
		return cnt0;
	}
	
	
	@Override
	public String getVMName(Map param) throws Exception
	{
		String code = getEtcCd(param);
		String[] arr = code.split(",");
		Set<String> notFullCode = new LinkedHashSet(); /* 99개가 안 채워진 코드 */
		for(String a : arr)
		{
			String vmName =  getNextVMName(a, getCnt0(a), notFullCode);
			if(vmName != null)
			{
				 
				return vmName;
			}
		}
		if(notFullCode.size()==0) {
			throw new Exception(MsgUtil.getMsg("msg_alert_no_vm_code", null)); // "그룹의 코드가 없습니다. 관리자에게 문의하세요."	
			
		}
		/* 빈 이름 찾기 */
		code = (String) notFullCode.iterator().next();
		param.put("ETC_CD", code);
		 
		for(String code1:notFullCode)
		{
			String vmName = getEmptyVMName(code);
			if(vmName != null)
			{
				return vmName;
			}
		}
		throw new Exception(MsgUtil.getMsg("msg_alert_no_vm_code", null)); // "그룹의 코드가 없습니다. 관리자에게 문의하세요."
	}
	@Override
	public String[] getVMNames(Map param, int count) throws Exception
	{ 
		String code = getEtcCd(param);
		 
		 
		return  getNextVMNamesSeq(code, getCnt0(code), null, count);
			 
	}
	 
	protected String getNextVMName(String code, int cnt0, String current) throws Exception
	{
		String like = getVMName(code,cnt0,0);
		
		int startPos = like.length();
		int pos = like.indexOf("%");
		if(pos>=0) {
			startPos = pos;
		}
		String num = current.substring(startPos, startPos +  cnt0);
		return getVMName(code, cnt0, Integer.parseInt(num)+1);
	}
	protected String[] getNextVMName(String code, int cnt0, String current, int count) throws Exception
	{
		String like = getVMName(code,cnt0,0);
		
		int startPos = like.length();
		int pos = like.indexOf("%");
		if(pos>=0) {
			startPos = pos;
		}
		String num = current.substring(startPos, startPos +  cnt0);
		String[] result = new String[count];
		for(int i=1; i<= count;i++) {
			result[i-1]= getVMName(code, cnt0, Integer.parseInt(num)+i);
		}
		return result;
	}
	protected void getTeamCategoryCode(Map paramOrg) throws Exception{
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		paramOrg.put("CODE", getIdByTeam(paramOrg,  "TEAM_ETC_CD"));
		paramOrg.put("CATEGORY_CODE", service.selectOneObjectByQueryKey("select_CATEGORY_CODE",paramOrg));
	}
	protected String getEtcCd(Map paramOrg) throws Exception{
		getTeamCategoryCode(paramOrg) ;
		String pattern= (String) ComponentService.getEnv("vmNaming","{CODE}{PURPOSE}000{P_KUBUN}");
		return setVarVal(pattern, paramOrg);
	}
	protected String getVMName(String code, int cnt0, int idx)
	{
		int pos = code.indexOf("0");
		return  code.substring(0, pos) + (idx>0 ?( TextHelper.lpad(idx, cnt0,'0')  ):"%")   +  code.substring(pos+cnt0) ;
	}
	
	protected int getNextVMNameLen(String code, int cnt0) throws Exception
	{
		return getVMName(code,cnt0,1).length();
	}
	protected String[] getNextVMNamesSeq(String code, int cnt0, Set notFullCode, int count) throws Exception
	{
		Map param = new HashMap();
		param.put("VM_NM_LIKE", getVMName(code,cnt0,0));
		param.put("VM_NM_LEN",  getNextVMNameLen(code, cnt0));
		param.put("VRA_YN", "Y");
		String[] result = new String[count];
		Map maxInfo=super.getService().selectOneByQueryKey(SiteService.NS, "select_max_vm", param);
		if(maxInfo == null || maxInfo.get("VM_NM") == null)
		{
			for(int i=1; i  <= count; i++) {
				result[i-1]= getVMName( code, cnt0, i);
			}
			return  result;
		}

		String maxVMName = (String)maxInfo.get("VM_NM");
		 
		int max = Integer.parseInt( TextHelper.lpad("", cnt0, "9"));
		if(maxVMName.equals(getVMName(code,cnt0, max )))
		{
			 
			return getEmptyVMNames(param, code, cnt0, count);
		}
		else
		{
			return getNextVMName( code, cnt0, maxVMName, count)  ;
		}
	}
	protected String[] getEmptyVMNames(Map param, String code, int cnt0, int count) throws Exception{
		 
		List<String> vmNames = super.getService().selectListByQueryKey(SiteService.NS, "select_VM_NM_byCode", param);
		int maxNum = Integer.parseInt( TextHelper.lpad("", cnt0, "9"));
		if(vmNames.size()==maxNum) {
			throw new Exception("VM Name is full");
		}
		Set<String> allVmNames = new LinkedHashSet<>();
		
		for(int i=1; i <= maxNum ; i++) {
			allVmNames.add(this.getVMName(code, cnt0, i));
		}
		for(String nm :  vmNames) {
			 
			boolean rnm = allVmNames.remove(nm);
			if(!rnm ) {
				String arr[] = nm.split(",");
				for(String a:arr) {
					allVmNames.remove(a);
				}
			}
		}
		String[] result = new String[count];
		int idx = 0;
		for(String nm : allVmNames) {
			result[idx++] = nm;
			if(idx==count) break;
		}
		return result;
	}
	@Override
	public boolean  onBeforeCreateVM(boolean isNew, Map param) throws Exception {
		isNew = super.onBeforeCreateVM(isNew, param);
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		String cluster = getIdByTeam(service, param ,  "CLUSTER" );
		if(cluster != null)
		{

			param.put(VMWareAPI.PARAM_CLUSTER,cluster);
			String host = (String)service.selectOneObjectByQueryKey(SiteService.NS,"selectHostByCluster", param);
			if(host == null)
			{
				throw new Exception("Host not exist");
			}
			param.put("HOST_HV_ID", host);
			return true;

		}
		return isNew;
	}
	@Override
	public List<IPInfo> getNextIp( int firstNicId, Map param) throws Exception {
		NextIPHelper ipHelper = new NextIPHelper( );
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		IPInfo info = new IPInfo();
		info.nicId = firstNicId++;
		String cidrs = getIdByTeam(service, param,  "IP" );
		if(cidrs == null)
		{
			throw new Exception("Not configure IP setting for team");
		}
		String[] cidrArr = cidrs.split(",");
		for(String cidr:cidrArr)
		{
			ipHelper.getNextIp(cidr, info, param);
			
			if(info.ip != null) break;
			
		}
		if(info.ip == null)
		{
			throw new Exception("no more IP");
		}
		
		param.put("NW_IP", info.nw_ip);
		info.network = (String)service.selectOneObjectByQueryKey("selectNetworkByIp", param);
		List list = new ArrayList();
		list.add(info);
		return list;

	}
	protected String getIdByTeam(Map param, String kubun) throws Exception{
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		return getIdByTeam(service, param, kubun);
	} 
	protected String getIdByTeam(SiteService service,  Map param,   String kubun) throws Exception{
		if(param.get("TEAM_CD")==null)
		{
			param.put("TEAM_CD", UserVO.getUser().getTeam().getTeamCd());
		}
		String field = PropertyManager.getString("clovirsm.vm.code.field","TEAM_CD");
		return getIdByTeam(service, param.get("DC_ID"),  param.get(field), kubun);
	}
	protected String getIdByTeam(SiteService service, Object dc_id, Object team,   String kubun) throws Exception{
		Map param = new HashMap();
		param.put("DC_ID", dc_id);
		param.put("TEAM_CD", team);
		param.put("KUBUN", kubun);
		return (String)service.selectOneObjectByQueryKey("getInfoByTeam", param);
	}
	protected String getNextVMName(String code, int cnt0, Set notFullCode) throws Exception
	{
		Map param = new HashMap();
		param.put("VM_NM_LIKE", getVMName(code,cnt0,0));
		param.put("VM_NM_LEN",  getNextVMNameLen(code, cnt0));

		Map maxInfo=super.getService().selectOneByQueryKey(SiteService.NS, "select_max_vm", param);
		if(maxInfo == null || maxInfo.get("VM_NM") == null)
		{
			return getVMName( code, cnt0, 1) ;
		}

		String maxVMName = (String)maxInfo.get("VM_NM");
		if(maxVMName == null)
		{
			return getVMName( code, cnt0, 1) ;
		}
		TextHelper.lpad("", cnt0, "9");
		int max = Integer.parseInt( TextHelper.lpad("", cnt0, "9"));
		if(maxVMName.equals(getVMName(code,cnt0, max )))
		{
			if(NumberUtil.getInt(maxInfo.get("CNT")) < max)
			{
				notFullCode.add(code);
			}
			return null;
		}
		else
		{
			return getNextVMName( code, cnt0, maxVMName)  ;
		}

	}
	protected String getEmptyVMName(String code) throws Exception
	{
		Map param = new HashMap();
		int cnt0 = this.getCnt0(code);
		String firstNM = getVMName(code,cnt0,1);
		String prefix = getVMName(code,cnt0,0);
		int len = firstNM.length();
		param.put("PREFIX",  prefix);
		param.put("CNT0",  cnt0);
		param.put("LEN",  len);
		param.put("POSTFIX",  firstNM.substring(prefix.length()+cnt0));
		return (String)super.getService().selectOneObjectByQueryKey(SiteService.NS, "getEmptyVMName", param);
	}
}
