package com.clovirsm.site;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.clovirsm.hv.IAfterProcess;
import com.clovirsm.hv.IPInfo;
import com.clovirsm.hv.vmware.VMWareAPI;
import com.clovirsm.service.admin.FwMngService;
import com.clovirsm.service.resource.FWService;
import com.clovirsm.service.site.SiteService;
import com.clovirsm.sys.hv.DC;
import com.clovirsm.sys.hv.DCService;
import com.clovirsm.sys.hv.NextIPHelper;
import com.clovirsm.sys.hv.executor.AddDelUser;
import com.clovirsm.sys.hv.vmware.BeforeAfterVMWare;
import com.clovirsm.sys.hv.vmware.OnAfterCreateVMVMWare;
import com.fliconz.fm.common.CodeHandler;
import com.fliconz.fm.common.util.StringUtil;
import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fm.mvc.util.MsgUtil;
import com.fliconz.fm.paloalto.client.PaloaltoFWClient;
import com.fliconz.fm.paloalto.core.PaloaltoAPIHelper;
import com.fliconz.fm.security.UserVO;
import com.fliconz.fw.runtime.util.NumberUtil;
import com.fliconz.fw.runtime.util.PropertyManager;
import com.fliconz.fw.runtime.util.SpringBeanUtil;
 

public class MobisBeforeAfterVMWare extends SiteBeforeAfterVMWare{

	int maxCount = 1000;
	public MobisBeforeAfterVMWare( ) {
		super( );
		 
	
		try {
			maxCount = service.getIpCnt();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onAfterDeleteVM(Map param) {
		super.onAfterDeleteVM(param);

		try {
			PaloaltoFWClient fwClient = getPaloaltoFWClient(param);
			if(fwClient == null) return;
			fwClient.deleteServer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public IAfterProcess onAfterProcess(String svcCd, String cudCd, Map param ) throws Exception {
		if(svcCd.equals("S") && cudCd.equals("C")) {
			return new OnAfterCreateVMVMWare(  param){				 

				@Override
				protected void onAfterSuccessExtra() throws Exception {
					// 메일 보내지 않는다.
				}
			};
		}
		else {
			return super.onAfterProcess(svcCd, cudCd, param);
		}
			
	}

	/*
	@Override
	public IAfterProcess getAfterUpdateVM(Map param) throws Exception {
		return new OnAfterCreateVMVMWare(getService(), param){
			@Override
			public void onAfterSuccess(String taskId) throws Exception {
				Thread thread = new Thread()
						{
							public void run()
							{
								AddDelUser addDelUser =  (AddDelUser)SpringBeanUtil.getBean("addDelUser");
								try
								{
									addDelUser.run((String)param.get("DC_ID"),(String)param.get("VM_ID"), (String)param.get("INS_DT"));
								}
								catch(Exception e)
								{
									e.printStackTrace();
								}
							}
						};

				thread.start();



			}
		};
	}*/



	@Override
	public String getImgName(Map param) throws Exception
	{
		String lastImgName = (String) super.getService().selectOneObjectByQueryKey(SiteService.NS, "getMaxImgName", param);
		String firstImgName = "Template_";

		if(lastImgName==null)
		{
			return firstImgName + param.get("VM_NM") + "_Img1";
		}
		else
		{
			int pos = lastImgName.lastIndexOf("_Img");
			if(pos<0)
			{
				return firstImgName + param.get("VM_NM") + "_Img1";
			}
			int lastNum = NumberUtil.getInt(lastImgName.substring(pos+4));
			return firstImgName + param.get("VM_NM") + "_Img" + (lastNum+1);
		}
	}
	 
	
	@Override
	protected int getCnt0(String code )
	{
		return 2;
	}
	
	@Override
	protected String getVMName(String code, int cnt0, int idx)
	{
		return "KRDMA" + code + (idx>0 ?( TextHelper.lpad(idx, cnt0,'0') + "R"):"");
	}
	
	@Override
	public	int chgNaming(Object categoryId, String naming) throws Exception
	{
		return 0;
	}
	@Override
	protected String getEtcCd(Map param) throws Exception
	{
		
		String etcCd= getIdByTeam(param,  "TEAM_ETC_CD");
		if(etcCd == null || "".equals(etcCd))
		{
			throw new Exception(MsgUtil.getMsg("msg_alert_no_vm_code", null)); // "그룹의 코드가 없습니다. 관리자에게 문의하세요."
			
		}
		return etcCd;
	}
 
	
	@Override
	public String getFolderName(  Map param) throws Exception
	{

		String code = getEtcCd(param);
		String[] arr = code.split(",");
		return arr[0];
	}

	
	@Override
	public boolean  onBeforeCreateVM(boolean isNew, Map param) throws Exception {
		isNew = super.onBeforeCreateVM(isNew, param);
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		String cluster = getIdByTeam(service, param ,  "CLUSTER" );
		if(cluster != null)
		{

			param.put(VMWareAPI.PARAM_CLUSTER,cluster);
			String host = (String)service.selectOneObjectByQueryKey(SiteService.NS,"selectHostByCluster", param);
			if(host == null)
			{
				throw new Exception("Host not exist");
			}
			param.put("HOST_HV_ID", host);
			return true;

		}
		return isNew;
	}
	@Override
	public List<IPInfo> getNextIp( int firstNicId, Map param) throws Exception {
		NextIPHelper ipHelper = new NextIPHelper( );
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		IPInfo info = new IPInfo();
		info.nicId = firstNicId++;
		String cidrs = getIdByTeam(service, param,   "IP" );
		if(cidrs == null)
		{
			throw new Exception("Not configure IP setting for team");
		}
		String[] cidrArr = cidrs.split(",");
		for(String cidr:cidrArr)
		{
			ipHelper.getNextIp(cidr, info, param);
			
			if(info.ip != null) break;
			
		}
		if(info.ip == null)
		{
			throw new Exception("no more IP");
		}
		
		param.put("NW_IP", info.nw_ip);
		info.network = (String)service.selectOneObjectByQueryKey("selectNetworkByIp", param);
		List list = new ArrayList();
		list.add(info);
		return list;

	}
	/**
	 * DC에서 가져오기 한 다음.
	 */
	@Override
	public boolean onAfterGetVM(Map result) throws Exception{
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		Map specInfo = service.selectOneByQueryKey( "com.clovirsm.resources.vm.VM","getVmSpecByCpu", result);
		if(specInfo == null)
		{
			throw new Exception(MsgUtil.getMsg("msg_cant_find_spec_info", new String[] {String.valueOf(result.get("CPU_CNT"))}));
		}

		
		result.put("SPEC_ID", specInfo.get("SPEC_ID"));
		return true; 

	}

	@Override
	public void onAfterFWDeploy(Map info)   {
		fWDeploy(info);
		if("Y".equals(info.get("VM_USER_YN")) && "N".equals(info.get("DEL_YN")))
		{
			if(!"Y".equals(info.get("ACCT_AUTO_CREATE_YN")))
			{
				AddDelUser addDelUser =  (AddDelUser)SpringBeanUtil.getBean("addDelUser");
				try {
					addDelUser.runOne(info);
				} catch (Exception e) {
					 e.printStackTrace();
				}
			}
		}
		

	}
	private static String[] getIps(String ip)
	{
		if(ip.indexOf(",")>0)
		{
			ip = ip.replace(" ", "");

			return TextHelper.split(ip);
		}
		else if(ip.indexOf("/")>0)
		{
			ip = ip.replace(" ", "");
			return TextHelper.split(ip, "/");
		}
		else
		{
			return new String[] {ip};
		}
	}

	protected PaloaltoFWClient getPaloaltoFWClient(Map vmInfo) throws Exception
	{
		/*if(TextHelper.isEmpty((String)vmInfo.get("IP")))
		{
			throw new Exception("Server IP not found");
		}*/
		if((String)vmInfo.get("IP") == null)
		{
			vmInfo.put("IP", vmInfo.get("PRIVATE_IP"));
		}
		Map fwConnInfo = getDcInfo().getPropMap("FW");
		if(fwConnInfo==null || TextHelper.isEmpty((String)fwConnInfo.get("CONN_URL")) ||  TextHelper.isEmpty((String)vmInfo.get("IP"))) return null;
		String url = (String) fwConnInfo.get("CONN_URL");
		String userId = (String) fwConnInfo.get("CONN_USERID");
		String userPwd = (String) fwConnInfo.get("CONN_PWD");
		Map prop = new HashMap();
		
		SiteService service = (SiteService)SpringBeanUtil.getBean("siteService");
		String nearVM = (String) service.selectOneObjectByQueryKey("selectNearVMName", vmInfo);
		prop.put("rulePositionWhere", "where=after&dst=" + nearVM );
		return new PaloaltoFWClient(url, userId, userPwd,(String)vmInfo.get("VM_NM"),  ((String)vmInfo.get("IP")).split(",") , prop );

	}

	protected void fWDeploy(Map info)   {




			String cudCd = (String)info.get("CUD_CD");
			FWService fwService = (FWService)SpringBeanUtil.getBean("fwService");
			try
			{
				Map vmInfo = this.getService().getVMInfo((String)info.get("VM_ID"));
				Map userInfo = this.getService().getUserInfo(info.get("FW_USER_ID"));

				PaloaltoAPIHelper.isSaveLog = true;

				PaloaltoFWClient fwClient = getPaloaltoFWClient(vmInfo);
				if(fwClient==null) return;

				String[] ips = getIps( (String)info.get("CIDR"));
				String[] ports = TextHelper.isEmpty((String)info.get("PORT")) ? new String[] {}: ((String)info.get("PORT")).split(",");
				if(cudCd.equals(("C")))
				{
					//String svrName, String serverIp, String clientIp, String[] port, String sabun

					fwClient.insert( ips , ports, (String)userInfo.get("LOGIN_ID"));

				}
				else if(cudCd.equals(("U")))
				{
					Map oldInfo = fwService.getOldInfo((String)info.get("FW_ID"), (String)info.get("INS_DT"));
					System.out.println("oldINfo : "+oldInfo);
					String[] oldPorts = oldInfo == null ? null : (oldInfo.get("PORT")==null? null : ((String)oldInfo.get("PORT")).split(","));
					String[] oldIps =  oldInfo == null ? null :  (oldInfo.get("CIDR")==null? null : getIps((String)oldInfo.get("CIDR")));
					String[] insertPort = fwService.getDiffPort(info, ports,  oldPorts );
					String[] deleteIps = fwService.getDiff(   oldIps, ips );
					//String[] insertIps = fwService.getDiff(  ips,  oldIps );
					/*String[] delPort = fwService.getDiffPort(info,  oldPorts, ports );
					if(delPort != null && delPort.length>0)
					{
						fwClient.deletePort( delPort);
					}*/
					if(insertPort != null && insertPort.length>0)
					{
						fwClient.insertPort( insertPort);
					}
					fwClient.insertClientIp( ips);
					if(deleteIps != null && deleteIps.length>0)
					{
						fwClient.deleteClientIp( deleteIps);
					}
				}
				else if(cudCd.equals(("D")))
				{
					//Map oldInfo = fwService.getOldInfo((String)info.get("FW_ID"), (String)info.get("INS_DT"));
					/*String[] oldPorts = oldInfo.get("PORT")==null? null : ((String)oldInfo.get("PORT")).split(",");
					String[] delPort = fwService.getDiffPort(info,oldPorts , new String[] {} );
					if(delPort != null && delPort.length>0)
					{
						fwClient.deletePort( delPort);
					}
					*/
					if(!fwService.existFWUser((String)info.get("VM_ID"), (String)info.get("FW_ID"), (String)info.get("FW_USER_ID")))
					{

						fwClient.deleteSabun( (String)userInfo.get("LOGIN_ID"));
					};


					fwClient.deleteClientIp (  ips );

				}
				fwService.updateFWDeployResult((String)info.get("FW_ID"), "Y");

				Map fwInfo = fwService.selectInfo("NC_FW_org", info);
				if("N".equals(fwInfo.get("VM_USER_YN")) || "Y".equals(fwInfo.get("ACCT_AUTO_CREATE_YN")))
				{
					FwMngService fwMngService = (FwMngService)SpringBeanUtil.getBean("fwMngService");
					fwInfo.put("VM_NM", vmInfo.get("VM_NM"));
					fwInfo.put("SERVER_IP", vmInfo.get("IP"));
					fwInfo.put("EMAIL", userInfo.get("EMAIL"));
					fwInfo.put("USER_NAME", userInfo.get("USER_NAME"));
					fwInfo.put("LOGIN_ID", userInfo.get("LOGIN_ID"));
					fwInfo.put("TASK_STATUS_CD", "S");
					fwInfo.put("FAIL_MSG", "");
					fwMngService.updateApproval(fwInfo);
				}
					// if vm user create then send mail

		}
		catch(Exception e)
		{
			e.printStackTrace();
			fwService.updateFWDeployResult((String)info.get("FW_ID"), e.getMessage());
		}
	}


	public static void main(String[] args)
	{
		 
			String newcidr="10.230.56.190,10.230.56.197,10.230.37.58" ;
			String oldcidr="10.230.56.190, 10.230.56.197, 10.230.21.133";
			FWService s = new FWService();
			String[] arr = s.getDiff(getIps(oldcidr),getIps( newcidr));
			System.out.println(arr.length);
			
		 
	}
}
