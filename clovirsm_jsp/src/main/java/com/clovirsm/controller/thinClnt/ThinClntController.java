package com.clovirsm.controller.thinClnt;

import com.fliconz.fm.mvc.util.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.clovirsm.service.site.thinClnt.ThinClntService;
import com.fliconz.fm.mvc.DefaultController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/thin_client")
public class ThinClntController extends DefaultController {

    @Autowired
    ThinClntService service;

    @Override
    protected ThinClntService getService() {
        return service;
    }

    @RequestMapping(value = "/update_status", method = RequestMethod.POST)
    public Object updateStatus(HttpServletRequest request, HttpServletResponse response) {
        try {
            Map param = ControllerUtil.getParam(request);
            int row = service.updateByQueryKey("update_NC_THIN_CLIENT_STATUS", param);

            return resReturnValue(param, row);
        } catch (Exception e) {
            return ControllerUtil.getErrMap(e);
        }
    }

    private Map<String, Object> resReturnValue(Map param, int row) {
        Map<String, Object> resParam = new HashMap<>();
        String result = row > 0 ? "ok" : "fail";
        resParam.put("result", result);
        resParam.put("SN", param.get("SN"));

        return resParam;
    }
}