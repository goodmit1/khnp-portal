<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.cm.AIModelTrainingService"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>
<%
String kubun = request.getParameter("kubun");
AIModelTrainingService service = (AIModelTrainingService)SpringBeanUtil.getBean("AIModelTrainingService");
Map param = new HashMap();
param.put("APP_ID","A778563088340500");

param.put("BATCH_SIZE", "10");
param.put("EPOCH_SIZE", "10");
param.put("MODEL_VER", "1");

if("update".equals(kubun)){
	param.put("MODEL_TR_SEQ","1");
	param.put("COMMIT_MSG", "test-2");
	param.put("RSV_DT","20191111100000");
	service.update(param);
}
else if("delete".equals(kubun)){
	param.put("MODEL_TR_SEQ","1");
	service.delete(param);
}
else{
	param.put("COMMIT_MSG", "test");
	param.put("RSV_DT","20191110100000");
	service.insert(param);
}
%>
</body>
</html>