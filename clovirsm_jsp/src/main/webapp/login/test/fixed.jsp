<%@page import="com.clovirsm.service.resource.VraCatalogService"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.clovirsm.hv.NumberUtil"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.admin.VraCatalogMngService"%>
<%@page contentType="text/html; charset=UTF-8"%>
<pre>
<%
VraCatalogService vraCatalogService = (VraCatalogService)SpringBeanUtil.getBean("vraCatalogService");
Map param = new HashMap();

List<Map> list = vraCatalogService.selectList("NC_VRA_CATALOG", param) ;
for(Map m : list){
	if("N".equals(m.get("ALL_USE_YN")) &&  m.get("ORG_CATALOG_ID") == null  &&  "A".equals(m.get("KUBUN"))){
		JSONObject fixed = new JSONObject();
		JSONObject formInfo = new JSONObject((String)m.get("FORM_INFO"));
		JSONObject resources = formInfo.getJSONObject("resources");
		if(formInfo.getJSONObject("inputs").has("_purpose")){
			JSONObject purpose = formInfo.getJSONObject("inputs").getJSONObject("_purpose");
			if(purpose.has("default")){
				fixed.put("_purpose", purpose.getString("default"));
			}
		}
		Iterator it = resources.keys();
		while(it.hasNext()){
			String k = (String)it.next();
			JSONObject resource = resources.getJSONObject(k);
			if("Cloud.vSphere.Machine".equals(resource.getString("type"))){
				JSONObject properties = resource.getJSONObject("properties");
				fixed.put("cpu", properties.get("cpuCount"));
				try{
					fixed.put("memory", properties.getInt("totalMemoryMB")/1024);
				}
				catch(Exception ignore){
					
				}
				if(properties.has("multi_disk")) fixed.put("multi_disk", properties.getString("multi_disk").split(","));
				if(properties.has("application")) fixed.put("app", properties.getString("application").split(","));
				if(properties.has("os_param")) fixed.put("os_param", properties.getString("os_param").split(","));
				fixed.put("vm_image", properties.get("image"));
				//fixed.put("_app_param", properties.get("application"));
				out.println("update NC_VRA_CATALOG_DRV set FIXED_JSON='" + fixed.toString() + "' where DRV_ID='" + m.get("CATALOG_ID") + "';");
				break;
			}
		}
	}
}
%>
</pre>	