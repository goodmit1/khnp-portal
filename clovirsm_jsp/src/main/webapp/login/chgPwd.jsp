<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page import="com.ibm.icu.util.Calendar"%>
<%@page import="com.fliconz.fw.runtime.util.DateUtil"%>
<%@page import="java.util.Date"%>
<%@page import="com.fliconz.fm.security.UserVO"%><%@ taglib prefix= "c" uri = "http://java.sun.com/jsp/jstl/core" %><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<% session.setAttribute("javax.servlet.jsp.jstl.fmt.locale.session", pageContext.getResponse().getLocale());%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title><spring:message code="msg_password_re_setting" /></title>
	<script src="/res/js/jquery.min.js"></script>
	<script	src="/res/js/bootstrap.min.js"></script>
	<link rel="stylesheet"
			href="/res/css/bootstrap.min.css">
	<link rel="stylesheet" href="/res/css/menu.css">
	<link rel="stylesheet" href="/res/css/common.css">
	<script src="/res/js/security.js"></script>
	<script src="/res/js/common.js"></script>
	<script>
var msg_password_chk_fail = "<spring:message code="msg_password_chk_fail" text="" />";
var msg_password_role_info = "<spring:message code="msg_password_role_info" text="" />";
</script>
	<style>
		/* body
		{
			background-color:#a3a0af;
		} */
		.panel-heading h4
		{
			color : #2d062d;
		}
		.loginForm
		{
			 width:400px;
			 margin-left : auto;
			 margin-right : auto;
			 margin-top : 150px;
		}
		.row
		{
			margin-top: 20px;
		}
		.panel-footer
		{
			margin-top: 40px;
			text-align: right;
		}
		</style>
</head>
<body>
	
	
	<form action="/j_spring_security_check" id="login-form" method="POST">
	<input type="hidden" value='<c:out value="${param.at}"/>' id="authToken" name="authToken" />
	<fmtags:include page="_chgPwd.jsp"/>
	</form>
		<script>
			$(document).ready(function(){
				
			})

			function request()
			{
				if(checkPassword("password","password1"))
				{
					post('/api/guest/chgPwd', $("#login-form").serialize(), function(data){
						alert('<spring:message code="msg_password_changed" />');
						location.href="/";

					})
				}
			}
			
		</script>

</body>
</html>
