<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm"  >
    	<jsp:include page="list.jsp"></jsp:include>
    	</form>
    </layout:put>
    <layout:put block="popup_area">
    	<jsp:include page="/admin/popup/team.jsp"></jsp:include>
    	<script>
    	var s_popup_team = newPopup("S_TEAM_NM", "team_popup");
    	s_popup_team.setInputInfo({"keyword":"S_TEAM_NM"});
    	s_popup_team.setMapping({"TEAM_NM":"S_TEAM_NM", "TEAM_CD":"S_TEAM_CD"});

    	var popup_team = newPopup("TEAM_NM", "team_popup");
    	popup_team.setInputInfo({"keyword":"TEAM_NM"});
    	popup_team.setMapping({"TEAM_NM":"TEAM_NM", "TEAM_CD":"TEAM_CD"});
    	</script>
    </layout:put>
</layout:extends>