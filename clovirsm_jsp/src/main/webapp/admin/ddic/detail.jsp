<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel detail-resize panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-6">
			<fm-input id="DD_ID" name="DD_ID" :disabled="form_data.IDU != 'I'" required="true" title="<spring:message code="FM_DDIC_DD_ID" text="그룹 ID" />"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-input id="DD_NM" name="DD_NM" required="true" title="<spring:message code="FM_DDIC_DD_NM" text="그룹명" />"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-input id="DD_VALUE" name="DD_VALUE" :disabled="form_data.IDU != 'I'" required="true" title="<spring:message code="FM_DDIC_DD_VALUE" text="코드값" />"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-input id="KO_DD_DESC" name="KO_DD_DESC" required="true" title="<spring:message code="FM_DDIC_KO_DD_DESC" text="코드명" />"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-input id="SORT" name="SORT" required="true" title="<spring:message code="FM_DDIC_SORT" text="정렬순서" />"></fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-select-yn id="USE_YN" name="USE_YN" title="<spring:message code="USE_YN" text=""/>">
			</fm-select-yn>
		</div>
	</div>
	<script>
	</script>
</div>
