<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
			<div class="panel-body">

					<input type="hidden" id="ID" name="ID">

					<div class="layout background mid">
						<div class="col col-sm-12" style="border-top: #e0e0e0 solid 1px;">
							<fm-output id="TITLE" name="TITLE" disabled="ture" required="true" title="<spring:message code="label_title"/>"></fm-output>
						</div>
						<div class="col col-sm-12">
							<div style="display: flex">
								<label class="control-label grid-title value-title" style="height: initial;" ><spring:message code="label_contents" text="내용" /></label>
								<div v-html="form_data.CONTENTS"></div>
							</div>
							<%--<fm-output id="CONTENTS" name="CONTENTS" disabled="ture" required="true" title="<spring:message code="label_contents"/>"></fm-output>--%>
							<%--<div v-html="form"></div>--%>
						</div>
						<div class="col col-sm-12">
							<div style="display: flex">
								<label class="control-label grid-title value-title" ><spring:message code="label_files" text="첨부파일" /></label>
								<jsp:include page="/home/common/_fileAttach.jsp">
									<jsp:param value="notice" name="kubun"/>
								</jsp:include>
							</div>
						</div>
					</div>


			</div>