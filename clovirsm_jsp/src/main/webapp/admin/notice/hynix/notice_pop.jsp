<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
    String popupid = request.getParameter("popupid");
    request.setAttribute("popupid", popupid);

    //캐시를 저장 안함
  	response.setHeader("Cache-Control","no-cache");
    response.setHeader("Pragma","no-cache");
    response.setDateHeader("Expires",0);
%>
<style>
#${popupid} > .modal-dialog {
   width: 1000px;
}
.col .row-fluid.Editor-container{
	width: 725px;
    height: 96%;
}
.popup_area .form-control.hastitle, .popup_area .output.hastitle{
	height: 98%;
}
.Editor-editor{
	height: 167px !important;
}
.detail-panel .panel-body .col {
    margin: unset;
    padding: unset;
    min-height: 38px;
    margin: 0px;
    border-bottom: none;
}
</style>
<fm-modal id="${popupid}" title="<spring:message code="title_noti" text="" />" cmd="header-title">

    <div class="form-panel detail-panel panel panel-default">
		<div class="panel-body" id="popup_area">
				<form id="${popupid}_form" action="none">
					<div id="outputList" class="" >
						<jsp:include page="outputList.jsp"></jsp:include>  
	        		 </div> 
				</form>
		</div>
	</div>




    <script>
	    var ${popupid}_param = {};
		var ${popupid}_parent_vue = null;
		var req = new Req();
		var ${popupid}_vue = makePopupVue('${popupid}','${popupid}', {
			form_data: ${popupid}_param
        });

	    $(function(){
		});
                // 최초 클릭할 때, param는 호출 시 사용한 param attribute
        function ${popupid}_click(vue,  param, callback){
        	 
        	if(param){
        		${popupid}_param = param;
            	
           		noticefileAttach_list(param.ID);
           		parent.hideLoading();
            	${popupid}_vue.form_data = param;
           	
            	$("#outputList").css("display","block");
            	$("#row_selected_Y").css("display","block");
        	}
            ${popupid}_vue.callback =  callback;
            return true;
        }
                // 완료 후 콜백 호출
        function ${popupid}_to_work()
        {
          if(${popupid}_vue.callback){
            eval(${popupid}_vue.callback + '();');
           }
        }
        function  ${popupid}_save() {
        	if(validate("popup_area")) {
        		${popupid}_vue.form_data.INQUIRY =  $("#INQUIRY_Edit_N").Editor("getText" );
        		post('/api/qna/save', ${popupid}_vue.form_data, function(data){
					alert(msg_complete);
					search();
					});
        		$('#inquiry_pop').trigger('click');
        	}
        }

        </script>
</fm-modal>