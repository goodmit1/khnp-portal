<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--  게시물 관리 -->
<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
<style>
#START_DATE_HH, #END_DATE_HH {
	margin-bottom: 0px;
}

</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-4">
			<fm-input id="S_TITLE" name="TITLE" title="<spring:message code="label_title"/>"></fm-input>
		</div>
<!-- 		 <div class="col"> -->
<!-- 			<fm-select url="/api/code_list?grp=sys.yn" id="S_DEL_YN" -->
<%-- 							name="DEL_YN" title="<spring:message code="NC_DISK_TYPE_DEL_YN" text="삭제 여부"/>" emptystr="<spring:message code="label_all"/>"></fm-select> --%>
<!-- 		</div> -->
		<div class="col btn_group nomargin">
			<input type="button" class="searchBtn btn" onclick="search()" value="<spring:message code="btn_search" text="" />" />
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel(mainTable,'notice')" class="layout excelBtn"></button>
			<c:if test="${sessionScope.ADMIN_YN == 'Y'}">
				<fm-sbutton cmd="update" class="btn btn-primary newBtn contentsBtn tabBtnImg new" onclick="newNoti()" ><spring:message code="btn_new" text="" /></fm-sbutton>
				<fm-sbutton cmd="delete" class="btn btn-primary delBtn contentsBtn tabBtnImg del" onclick="deleteNoti()" ><spring:message code="btn_delete" text=""/></fm-sbutton>
				<fm-sbutton cmd="update" class="btn btn-primary saveBtn contentsBtn tabBtnImg save" onclick="saveNoti()" ><spring:message code="btn_save" text="" /></fm-sbutton>
			</c:if>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" style="height: 300px" class="ag-theme-fresh"></div>
	</div>
	</div>
	<div class="form-panel detail-panel panel panel-default box_s" style="margin-top: 0px; margin-bottom: 0px; border: 0px;">
		<div id="inputList" class="" style="display:none;">
			<jsp:include page="inputList.jsp"></jsp:include> 
        </div>
        <fm-popup-button popupid="notice_popup" popup="/admin/notice/hynix/notice_pop.jsp" style="display:none;" param="Noti.getData()"></fm-popup-button>
	</div>
</div>
<script>
					var Noti = new Req();
					mainTable;
					$(function() {
						
						if(accessInfo.update){
								$("#inputList").css("display","block");				
						} 
						
						var
						columnDefs = [ {
							headerName : "ID",
							field : "ID",
							width : 50,
						}, {
							headerName : "<spring:message code="label_title"/>",
							field : "TITLE"
						},
// 						{
// 							headerName : "<spring:message code="label_top_position"/>",
// 							field : "TOP_YN",
// 							width : 60,
// 						},
// 						{
// 							headerName : "<spring:message code="NC_DISK_TYPE_DEL_YN" text="삭제 여부"/>",
// 							field : "DEL_YN",
// 							width : 60,
// 						},
						{
							headerName : "<spring:message code="label_post_term"/>",
							field : "START_DATE",
							width : 160,
							valueGetter:function(params)
							{
								var date = '';
								 
								if(params.data.START_DATE) date += formatDatePattern( new Date(params.data.START_DATE),'yyyy-MM-dd HH<spring:message code="hour"/>')
								if(params.data.END_DATE) date += "~" + formatDatePattern( new Date(params.data.END_DATE),'yyyy-MM-dd HH<spring:message code="hour"/>')
								return date;
							},
						}, {
							headerName : "<spring:message code="label_read_cnt"/>",
							field : "HITS",
							cellClass : "text-right",
							width : 60,
						}, {
							headerName : "<spring:message code="INS_TMS"/>",
							field : "INS_TMS",
							valueFormatter:function(params)
							{
								return formatDate(params.value,'datetime')
							},
							width : 80,
						} ];
						var
						gridOptions = {
							columnDefs : columnDefs,
							rowData : [],
							getRowNodeId : function(data) {
			    			    	return "" + data.ID;
			    			},
							onSelectionChanged : function() {
								var arr = mainTable.getSelectedRows();
								$("#LOGIN_ID").attr("readonly", true);
								$("#row_selected_N").css('display','none');
								$("#row_selected_Y").css('display','block')
								Noti.getInfo('/api/notice/info?ID='
										+ arr[0].ID, function(data){
										noticefileAttach_list( arr[0].ID);
										if(data.START_DATE)
										{
											var date = formatDate(data.START_DATE, 'datetime');
											Noti.putData({START_DATE_DT: date.substring(0,10), START_DATE_HH :date.substring(11,13) });
										}
										if(data.END_DATE)
										{
											var date = formatDate(data.END_DATE, 'datetime');
											Noti.putData({END_DATE_DT: date.substring(0,10), END_DATE_HH :date.substring(11,13) });
										}
									$("#CONTENTS").Editor("setText", data.CONTENTS ? data.CONTENTS:"");
									Noti.search('/api/noticeTrg/list?ID=' + arr[0].ID, function(data){
										var selectedValues = [];
										for(var i in data.list){
											selectedValues.push(data.list[i].TEAM_CD);
										}
										if(!accessInfo.update){
											$("#notice_popup_button").click();		
										} 
									
										
										 
										$("#TRG_TEAM_CD").val(selectedValues);

									})
								});
							},
							enableSorting : true,
							enableColResize : true,
							rowSelection : 'single'
						}

						mainTable = newGrid("mainTable", gridOptions);
						search();
					});

					//조회
					function search() {
						Noti.search('/api/notice/list', function(data) {
							mainTable.setData(data);
							if(Noti.getData().ID)
							{
								mainTable.setSelectedById(Noti.getData().ID, true);
							}


						});
					}

					//저장
					function saveNoti() {
						if(validate("input_area")) {
							form_data.CONTENTS = $("#CONTENTS").Editor("getText" )
							if(form_data.START_DATE_DT) {
								form_data.START_DATE = form_data.START_DATE_DT.replace(/-/g, '') +   (form_data.START_DATE_HH ? form_data.START_DATE_HH:'00') + '0000';
							} else {
								delete form_data.START_DATE ;
							}
							if(form_data.END_DATE_DT) {
								form_data.END_DATE = form_data.END_DATE_DT.replace(/-/g, '') +  (form_data.END_DATE_HH?form_data.END_DATE_HH:'23') + '0000';
							} else {
								delete form_data.END_DATE ;
							}
							Noti.saveFiles('/api/notice/save_fileAttach',"files", function(data) {
								if(data.ID) Noti.getData().ID=data.ID;
								post("/api/noticeTrg/save_multi", {ID: form_data.ID, TEAM_CD:$("#TRG_TEAM_CD").val()}, function(){
									alert(msg_complete);
									search();
								})
							});
							 
						}
					}

					//삭제
					function deleteNoti() {
						var arr = mainTable.getSelectedRows();
						if(arr.length > 0){
							if(confirm(msg_confirm_delete))
							{
								post('/api/notice/delete', {ID:form_data.ID}, function(){
									alert(msg_complete);
									search();
								})
							}
						} else{
							alert("<spring:message code="select_first" text="" />")
						}

					}

					// 추가
					function newNoti(){
						$("#CONTENTS").Editor("setText","");
						Noti.setData({DEL_YN:'N',TOP_YN:'N'});
						$("#TITLE").focus();
						$("#TRG_TEAM_CD").val("");
					}
					// 엑셀 내보내기
					function exportExcel() {
						exportExcelServer("mainForm", '/api/notice/list_excel', 'Notice',mainTable.gridOptions.columnDefs, Noti.getRunSearchData())
					}
				</script>


