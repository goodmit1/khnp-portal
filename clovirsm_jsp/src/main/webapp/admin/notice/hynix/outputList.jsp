<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
			<div class="table_title layout name">
				<div class="search_info" style=" margin-top: -17px; font-size: 22px;">
					{{form_data.TITLE}}
				</div>
				<div style="float: right; margin-right: 12px; font-size: 13px; font-weight: 300; margin-top: -9px;">
					<spring:message code="INS_TMS" text="" /> : {{ formatDate(form_data.INS_TMS,'datetime')}}
				</div>
			</div>
			<div class="panel-body">

					<input type="hidden" id="ID" name="ID">
					<div id="row_selected_Y" style="display: none;">
					
						<div class="layout" style="padding: 10px; min-height: 400px;">
							<div class="col col-sm-6" style="    float: right;border:none;">
								<jsp:include page="/home/common/_fileAttach.jsp">
									<jsp:param value="notice" name="kubun"/>
								</jsp:include>
							</div>
							<div class="col col-sm-12" style="border:none;">
								<div v-html="form_data.CONTENTS"></div>
							</div>
						</div>
					</div>
<!-- 					<div id="row_selected_N" style="display: block;min-height: 189px;"> -->
<!-- 						<p class="select_N">선택된 항목이 없습니다.</p> -->
<!-- 					</div> -->
					<!-- <div class="col col-sm-12" style="height: 349px;">
						<div v-html="form_data.CONTENTS"   style="padding:10px;min-height: 200px;overflow-y: auto;word-break: break-word;"></div>
					</div> -->
			</div>