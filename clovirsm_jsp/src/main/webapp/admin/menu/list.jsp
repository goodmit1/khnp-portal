<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<!--  메뉴 관리 -->
<style>
#tree {
	height:calc(100vh - 221px);
	overflow: auto;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<div class="col col-sm">
			<fm-input id="S_MENU_NM" name="MENU_NM" title="<spring:message code="title_menu"/>"></fm-input>

		</div>

		<div class="col btn_group nomargin">
			<button type="button" class="btn searchBtn" onclick="searchMenu()"><spring:message code="btn_search" text="" /></button>
		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<div class="btn_group">
			<fm-sbutton cmd="update" class="btn btn-primary exeBtn contentsBtn tabBtnImg sync" onclick="reloadMemory()"><spring:message code="label_apply_mem"/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary newBtn contentsBtn tabBtnImg new" onclick="newMenu()" ><spring:message code="btn_new" text="" /></fm-sbutton>
			<fm-sbutton cmd="delete" class="btn btn-primary delBtn contentsBtn tabBtnImg del" onclick="delMenu()"><spring:message code="btn_delete" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary saveBtn contentsBtn tabBtnImg save" onclick="saveMenu()"><spring:message code="btn_save" text="" /></fm-sbutton>
		</div>
	</div>
	<div class="col-sm-3 padding0 tree">
		<div class="panel panel-default padding0">
			<div class="panel-body">
				<fm-tree id="tree" url="/api/menu/list" dnd="true"
					:field_config="{onselect:menu_click, onmove:node_move, root:1,id:'ID',parent:'PARENT_ID',text:'MENU_NM',icon:'ICON'}"></fm-tree>
			</div>
		</div>
	</div>
	<div class="col-sm-9 right-panel">
	
		<div class="form-panel detail-panel panel panel-default h100">

			<div class="panel-body">

				<div class="col col-sm-6">
					<fm-input id="MENU_NM" name="MENU_NM" title="<spring:message code="title_menu"/>" required="true"></fm-input>

				</div>
				<div class="col col-sm-6">
					<fm-popup id="PARENT_ID_NM" name="PARENT_ID_NM" title="<spring:message code="label_parent_menu"/>"></fm-popup>

				</div>
				<div class="col col-sm-12">
					<fm-input id="PRGM_PATH" name="PRGM_PATH" title="<spring:message code="label_path"/>"></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-input id="PKG_NM" name="PKG_NM" title="<spring:message code="label_data_path"/>"></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-input id="MENU_ORDER" name="MENU_ORDER" title="<spring:message code="label_ORD"/>"></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-input id="ICON" name="ICON" title="<spring:message code="NC_VRA_CATALOG_ICON" text="아이콘" />"></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-select url="/api/code_list?grp=sys.yn" id="USE_YN"
						name="USE_YN" title="<spring:message code="USE_YN" text=""/>"></fm-select>
				</div>
				<div class="col col-sm-6">
					<fm-select url="/api/code_list?grp=sys.yn" id="IS_POPUP"
						name="IS_POPUP" title="<spring:message code="label_is_popup"/>"></fm-select>
				</div>
				<div class="col col-sm-12" style="height: 140px;">
					<fm-textarea id="MENU_DESC" name="MENU_DESC" title="<spring:message code="NC_DC_CMT" text="" />"></fm-textarea>

				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var menu = new Req();

	//메모리 반영
	function reloadMemory()
	{
		post("/api/menu/save_memory",{}, function(){
			alert("<spring:message code="msg_applied"/>");
		});
	}

	// 조회
	function searchMenu() {
		$("#tree").trigger("search", [ menu.getSearchData() ]);

	}

	//삭제
	function delMenu() {
		menu.del('/api/menu/delete', function() {
			$("#tree").trigger("reload");
		});
	}

	//drag & drop 순서 및 부모 변경
	function node_move(node)
	{
		post("/api/menu/save_move", node);
	}

	//저장
	function saveMenu() {
		if(validate("input_area"))
		{
			if(!form_data.PARENT_ID || form_data.PARENT_ID=='')
			{
				form_data.PARENT_ID	='1';
			}
			menu.save('/api/menu/save', function() {
				$("#tree").trigger("reload");
			});
		}
	}

	//추가
	function newMenu() {
		var curr_data = menu.getData();
		if(!curr_data || !curr_data.ID || curr_data.ID=='')
		{
			curr_data = {PARENT_ID:1, PARENT_ID_NM:''};
		}
		var data = {
			USE_YN : "Y",
			IS_POPUP : "N",
			"PARENT_ID" : curr_data.ID,
			"PARENT_ID_NM" : curr_data.MENU_NM
		};
		menu.setData(data);
		$("#MENU_NM").focus();
	}

	// tree node click시  메뉴 상세 정보 조회
	function menu_click(selectedArr) {

		menu.getInfo('/api/menu/info?ID=' + selectedArr[0].ID);
	}
</script>

