<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm" >
     
    	<jsp:include page="list.jsp"></jsp:include>
    	</form>
    </layout:put>
    <layout:put block="popup_area">
    	<jsp:include page="/admin/popup/prgm.jsp"></jsp:include>
    	<jsp:include page="/admin/popup/user.jsp"></jsp:include>
    	<script>
    	var popup_prgm = newPopup("PRGM_NM", "prgm_popup");
    	popup_prgm.setMapping({"PRGM_NM":"PRGM_NM", "PRGM_ID":"PRGM_ID"});
    	popup_prgm.setInputInfo({"keyword":"PRGM_NM"});
    	    	

    	var popup_prgm_s = newPopup("S_PRGM_NM", "prgm_popup");
    	popup_prgm_s.setMapping({"PRGM_NM":"PRGM_NM", "PRGM_ID":"PRGM_ID"});
    	popup_prgm_s.setInputInfo({"keyword":"PRGM_NM"});
    	
    	
    	var popup_user_s = newPopup("S_USER_NAME", "user_popup");
    	popup_user_s.setMapping({"USER_NAME":"USER_NAME", "USER_ID":"USER_ID"});
    	popup_user_s.setInputInfo({"keyword":"USER_NAME"});
    	
    	var popup_user  = newPopup("USER_NAME", "user_popup");
    	popup_user.setMapping({"USER_NAME":"USER_NAME", "USER_ID":"USER_ID"});
    	popup_user.setInputInfo({"keyword":"USER_NAME"});

    	</script>
	</layout:put>
</layout:extends>