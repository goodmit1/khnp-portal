<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
 
String popupid = request.getParameter("popupid");
request.setAttribute("popupid", popupid);
    
%>
<style>
#${popupid} > .modal-dialog {
   width: 1000px;
}
.col .row-fluid.Editor-container{
	width:90%;
}
.col .row-fluid.Editor-container{
	width: 725px;
    height: 96%;
}
.popup_area .form-control.hastitle, .popup_area .output.hastitle{
	height: 98%;
}
.Editor-editor{
	height: 167px !important;
}
#INQUIRY_Y > div.col.col-sm-12 > label{
	min-height: 37px;
    line-height: 37px;
    width: auto;
    height: 100%;
    max-width: none;
    text-align: left !important;
    background: #f8f8f8;
    padding-left: 5px;
    font-weight: bold;
}
#INQ_TITLE, #UPD_NAME{
	white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
#INQUIRY_Y > div.col.col-sm-12 > div > div, #ANSWER_Y > div.col.col-sm-12 > div > div{
	padding:10px;
	min-height: 250px; 
	max-height: 250px;
	overflow-y: auto;
	vertical-align: middle;
	display: inline-block;
	width: 80%;
	word-break: break-all;

}
label.control-label.grid-title.value-title {
    float: left;
}
.popup_area .form-control.hastitle, .popup_area .output.hastitle {
    width: calc(100% - 150px);
    min-width: 50px;
    height: 95%;
    padding: 10px;
    float: left;
}
.col .row-fluid.Editor-container {
    vertical-align: middle;
    display: inline-block;
    width: 84%;
}
</style>
<fm-modal id="${popupid}" title="Q&A" cmd="header-title">

    <span slot="footer" id="${popupid}_footer">
   			<fm-sbutton cmd="delete" id="deleteBtn" class="btn btn-primary delBtn popupBtn cancel top5" onclick="deleteInq()"><spring:message code="btn_delete" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" id="saveBt" class="btn btn-primary saveBtn popupBtn finish top5" onclick="${popupid}_save()" ><spring:message code="btn_save" text="" /></fm-sbutton>
			<c:if test="${ADMIN_YN eq 'Y'}"><fm-sbutton cmd="etc" id="saveAnswer" class="btn btn-primary saveBtn popupBtn change top5" onclick="${popupid}_saveAnswer()" ><spring:message code="NC_INQ_ANSWER" text="답변" /></fm-sbutton></c:if>
    </span>
    <div class="form-panel detail-panel panel panel-default" style="height: 550px; overflow: auto;">
		<div class="panel-body" id="popup_area">
		<jsp:include page="./detail.jsp"></jsp:include>
		</div>
		
	</div>




   
</fm-modal>