<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!--  게시물 관리 -->
<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
<style>
.col .row-fluid.Editor-container{
	width:85%;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col">
			<fm-input id="S_INQ_TITLE" name="INQ_TITLE" title="<spring:message code="NC_INQ_INQ_TITLE" text="문의제목"/>"></fm-input>
		</div>
		<div class="col">
			<fm-input id="S_INS_NAME" name="INS_NAME" title="<spring:message code="NC_INQ_INS_NAME" text="문의자명"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=SVC" id="S_SVC_CD" emptystr=""
				name="SVC_CD" title="<spring:message code="NC_INQ_SVC" text="서비스"/>"></fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=INQ" id="S_INQ_CD" emptystr=""
				name="INQ_CD" title="<spring:message code="NC_INQ_INQ" text="문의유형코드"/>"></fm-select>
		</div>
		<div class="col btn_group nomargin">
			<input type="button" class="searchBtn btn" onclick="search()" value="<spring:message code="btn_search" text="" />" />
		</div>
		<%-- <div class="col btn_group_under">

			<fm-sbutton cmd="update" class="btn btn-primary newBtn" onclick="newInq()" ><spring:message code="btn_new" text="" /></fm-sbutton>
			<fm-sbutton cmd="delete" id="deleteBtn" class="btn btn-primary delBtn" onclick="deleteInq()"><spring:message code="btn_delete" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary saveBtn" onclick="saveInq()" ><spring:message code="btn_save" text="" /></fm-sbutton>
		</div> --%>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<fm-sbutton cmd="update" class="contentsBtn tabBtnImg new " onclick="newInq()" ><spring:message code="btn_new" text="" /></fm-sbutton>
			<fm-sbutton cmd="delete" id="deleteBtn" class="contentsBtn tabBtnImg del" onclick="deleteInq()"><spring:message code="btn_delete" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary saveBtn contentsBtn tabBtnImg save" onclick="saveInq()" ><spring:message code="btn_save" text="" /></fm-sbutton>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" style="height: 300px" class="ag-theme-fresh"></div>
	</div>
	<c:if test="${sessionScope.ADMIN_YN ne 'Y'}">
		<jsp:include page="detail.jsp"></jsp:include>
	</c:if>
	<c:if test="${sessionScope.ADMIN_YN eq 'Y'}">
		<jsp:include page="mngDetail.jsp"></jsp:include>
	</c:if>
</div>
<script>
var Inq = new Req();
var disableYn = false;
mainTable;
$(function() {
	var
	columnDefs = [{
		headerName : "",
		field : "INQ_ID",
		hide : true
	}, {
		headerName : "<spring:message code="NC_INQ_INQ_TITLE" text="문의제목"/>",
		field : "INQ_TITLE",
		width : 500
	}, {
		headerName : "<spring:message code="NC_INQ_SVC" text="서비스"/>",
		field : "SVC_CD_NM",
		width : 130
	}, {
		headerName : "<spring:message code="NC_INQ_INQ" text="문의유형코드"/>",
		field : "INQ_CD_NM",
		width : 200
	}, {
		headerName : "<spring:message code="NC_VM_INS_TMS" text="등록일시"/>",
		field : "INS_TMS",
		valueGetter:function(params) {
			return formatDate(params.data.INS_TMS,'datetime');
		}
	}, {
		headerName : "<spring:message code="NC_INQ_ANSWER_YN" text="답변여부"/>",
		field : "ANSWER_YN",
		cellRenderer:function(params) {
			var name = "RED";
			if(params.data.ANSWER_YN == "Y"){
				name = "GREEN";
			}
			return '<img src="/res/img/' + name + '-icon.png">';
		}
	}, {
		headerName : "<spring:message code="NC_INQ_INS_NAME" text="문의자"/>",
		field : "INS_NAME"
	} ];
	var
	gridOptions = {
		hasNo : true,
		columnDefs : columnDefs,
		rowData : [],
		cacheBlockSize: 100,
		enableSorting : true,
		enableColResize : true,
		rowSelection : 'single',
		enableServerSideSorting: false,
		onSelectionChanged : function() {
			var arr = mainTable.getSelectedRows();
			$("#row_selected_N").css('display','none');
			$("#row_selected_Y").css('display','block')
			Inq.getInfo('/api/qna/info?INQ_ID=' + arr[0].INQ_ID, function(data){
				$("#INQUIRY").Editor("setText", data.INQUIRY ? data.INQUIRY:"");
				$("#ANSWER").Editor("setText", data.ANSWER ? data.ANSWER:"");
				if(form_data.ANSWER != null && form_data.ANSWER.trim() != ""){
					disableYn = true;
				}else{
					disableYn = false;
				}
				<c:if test="${sessionScope.ADMIN_YN ne 'Y'}">
				if(!disableYn && data.INS_ID==_USER_ID_)
				{
					$("#deleteBtn").prop("disabled",false);
				}
				else
				{
					$("#deleteBtn").prop("disabled",true);
				}
			</c:if>
			});
		}
	}
	mainTable = newGrid("mainTable", gridOptions);
	search();
});

//조회
function search() {
	search_data._IS_ADMIN_=ADMIN_YN;
	Inq.search('/api/qna/list', function(data) {
		 
		mainTable.setData(data);
	});
}

//저장

function saveInq() {
	if(validate("input_area")) {

		<c:if test="${sessionScope.ADMIN_YN ne 'Y'}">
		if(disableYn){
			alert("<spring:message code="msg_already_answer" text="답변이 있는 문의는 수정할 수 없습니다."/>");
			return false;
		}
		</c:if>
		form_data.INQUIRY =  $("#INQUIRY").Editor("getText" );
		
		<c:if test="${sessionScope.ADMIN_YN eq 'Y'}">
		saveAnswer();
		</c:if>
		Inq.save("/api/qna/save", function(){
			alert(msg_complete);
			search();
		})
	}
}

<c:if test="${sessionScope.ADMIN_YN eq 'Y'}">
function saveAnswer() {
	if(validate("input_area")) {
		form_data.ANSWER =  $("#ANSWER").Editor("getText" );
		 
		Inq.save("/api/qna/saveAnswer", function(){
			search();
		})
	}
}
</c:if>

//삭제
function deleteInq() {
	Inq.del('/api/qna/delete', function() {
		alert(msg_complete);
		search();
	});
}

// 추가
function newInq(){
	Inq.setData({});
	$("#row_selected_N").css("display","none");
	$("#row_selected_Y").css("display","block");
}

</script>