 <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%
    String popupid = request.getParameter("popupid");
    request.setAttribute("popupid", popupid);
    String INQ_ID = request.getParameter("INQ_ID");
    request.setAttribute("INQ_ID", INQ_ID);

    
%>
<layout:extends name="base/popup_main">
<layout:put block="content">
<style>
.col .row-fluid.Editor-container{
	width:90%;
}
.col .row-fluid.Editor-container{
	width: 725px;
    height: 96%;
}
.popup_area .form-control.hastitle, .popup_area .output.hastitle{
	height: 98%;
}
.Editor-editor{
	height: 152px !important;
}
#INQUIRY_Y > div.col.col-sm-12 > label{
	min-height: 37px;
    line-height: 37px;
    width: auto;
    height: 100%;
    max-width: none;
    text-align: left !important;
    background: #f8f8f8;
    padding-left: 5px;
    font-weight: bold;
}
#INQ_TITLE, #UPD_NAME{
	white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
#INQUIRY_Y > div.col.col-sm-12 > div > div, #ANSWER_Y > div.col.col-sm-12 > div > div{
	padding:10px;
	min-height: 250px; 
	max-height: 250px;
	overflow-y: auto;
	vertical-align: middle;
	display: inline-block;
	width: 80%;
	word-break: break-all;

}
label.control-label.grid-title.value-title {
    float: left;
}
.popup_area .form-control.hastitle, .popup_area .output.hastitle {
    width: calc(100% - 150px);
    min-width: 50px;
    height: 95%;
    padding: 2px;
    float: left;
    margin-top: 9px;
    margin-left: 4px;
}
.col .row-fluid.Editor-container {
    vertical-align: middle;
    display: inline-block;
    width: 81%;
}
.popup_top {
    width: 100%;
    padding: 10px;
    padding-bottom: 0px;
    padding-left: 30px;
    font-size: 20px;
    color: black;
    font-weight: bold;
    background: #ffffff;
}
.popup_mid {
    width: 97%;
    height: 378px;
    border: 1px solid #ddd;
    margin: 15px;
}
</style>
<div id="input_area" class="detail-panel" style="text-align: center;">
<div class="panel-body">
<div class="popup_top" style="text-align: left;"><span>Q&A</span></div>
<div class="popup_mid" style="text-align: left;">
	<%@include file="detail.jsp"%>
</div>
<fm-sbutton cmd="update" id="saveBt" class="btn btn-primary saveBtn popupBtn finish top5" onclick="${popupid}_save()" ><spring:message code="btn_save" text="" /></fm-sbutton>
</div>
</div>
<script>
$(document).ready(function(){
	$('#INS_NAME').html(_USER_NM_)
	$('#TEAM_NM').html(_TEAM_NM_)
	$('#INQUIRY_Y').hide();
	$(".comment_div").hide();
	${popupid}_vue.form_data = form_data;
})</script>
</layout:put>
</layout:extends>
