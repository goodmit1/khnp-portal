<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%  
%>

<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
<form id="${popupid}_form" action="none">
				<div id="INQUIRY_N">
					<div class="col col-sm-12">
						<fm-input id="INQ_TITLE" name="INQ_TITLE" required="true" input_style="width: 77%;" title="<spring:message code="NC_INQ_INQ_TITLE" text="문의제목"/>"></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-select url="/api/code_list?grp=INQ" id="INQ_CD"
							name="INQ_CD" title="<spring:message code="NC_INQ_INQ" text="문의유형코드"/>"></fm-select>
					</div>
					<div class="col col-sm-6">
						<fm-select url="/api/code_list?grp=SVC" id="SVC_CD" emptystr=" "
							name="SVC_CD" title="<spring:message code="NC_INQ_SVC" text="서비스"/>"></fm-select>
					</div>
					<div class="col col-sm-6">
						<fm-output id="INS_NAME" name="INS_NAME" disabled="true" title="<spring:message code="NC_INQ_INS_NAME" text="문의자"/>"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="TEAM_NM" name="TEAM_NM" disabled="true" title="<spring:message code="title_group" text="팀"/>"></fm-output>
					</div>
					<div class="col col-sm-12" style="height: 248px;">
						<fm-textarea id="INQUIRY_Edit_N" name="INQUIRY" required="true" title="<spring:message code="NC_INQ_INQUIRY" text="문의" />"></fm-textarea>
					</div>
				</div>
				<div id="INQUIRY_Y">
					<div class="col col-sm-12">
						<fm-output id="INQ_TITLE" name="INQ_TITLE" disabled="true" ostyle="min-height: auto;" title="<spring:message code="NC_INQ_INQ_TITLE" text="문의제목"/>" tooltip="INQ_TITLE"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="INQ_CD_NM" name="INQ_CD_NM" disabled="true" title="<spring:message code="NC_INQ_INQ" text="문의유형코드"/>"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="SVC_CD_NM" name="SVC_CD_NM" disabled="true" title="<spring:message code="NC_INQ_SVC" text="서비스"/>"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="INS_NAME" name="INS_NAME" disabled="true" title="<spring:message code="NC_INQ_INS_NAME" text="문의자"/>"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="TEAM_NM" name="TEAM_NM" disabled="true" title="<spring:message code="title_group" text="팀"/>"></fm-output>
					</div>
					<div class="col col-sm-12" style="height: 251px;">
						<div>
							<label class="control-label grid-title value-title inquiry"><spring:message code="NC_INQ_INQUIRY" text="문의" /></label>
							<div v-html="form_data.INQUIRY"> </div>
						</div>
					</div>
				</div>
				<div id="ANSWER_N">
					<div class="col col-sm-6">
						<fm-output id="UPD_NAME" name="UPD_NAME" title="<spring:message code="NC_INQ_UPD_NAME" text="답변자"/>"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="UPD_TMS" :value="formatDate(form_data.UPD_TMS,'datetime')" name="UPD_TMS" title="<spring:message code="NC_INQ_UPD_TMS" text="답변일시"/>"></fm-output>
					</div>
					<div class="col col-sm-12" style="height: 248px;">
						<fm-textarea id="ANSWER_Edit_N" name="ANSWER" disabled="true" title="<spring:message code="NC_INQ_ANSWER" text="답변" />"></fm-textarea>
					</div>
				</div>
				<div id="ANSWER_Y">
					<div class="col col-sm-6">
						<fm-output id="UPD_NAME" name="UPD_NAME" title="<spring:message code="NC_INQ_UPD_NAME" text="답변자"/>"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="UPD_TMS" :value="formatDate(form_data.UPD_TMS,'datetime')" name="UPD_TMS" title="<spring:message code="NC_INQ_UPD_TMS" text="답변일시"/>"></fm-output>
					</div>
					<div class="col col-sm-12" style="height: 251px;">
						<div>
							<label class="control-label grid-title value-title"><spring:message code="NC_INQ_ANSWER" text="답변" /></label>
							<div v-html="form_data.ANSWER" ></div>
						</div>
					</div>
				</div>
				
				<jsp:include page="./comment.jsp"></jsp:include>
				</form>
				
				 <script>
	    var ${popupid}_param = {ANSWER_YN:'',INQUIRY:'',INQ_CD:'', INQ_CD_NM:'',INQ_TITLE:'',INS_ID:'',INS_IP:'',INS_NAME:'',INS_TMS:'',LOGIN_ID:'',MOBILE_NO:'',READ_TMS:'',READ_YN:'',SVC_CD:'',SVC_CD_NM:'',TEAM_NM:'',UPD_TMS:'' };
		var ${popupid}_parent_vue = null;
		var ${popupid}_vue = makePopupVue('${popupid}','${popupid}', {
			form_data: ${popupid}_param
        });

	    $(function(){
	    	//$('#INQUIRY_Y').hide();
	    	$('#ANSWER_Y').hide();
	    	$('#ANSWER_N').hide();
			setTimeout(function(){
				$("#INQUIRY_Edit_N").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false , 'source':false});
				$("#ANSWER_Edit_N").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false , 'source':false});
				/* if(form_data.ANSWER_YN == "N" || ${sessionScope.accessInfo.get('etc')}) {
				} */
			},200)
		});
                // 최초 클릭할 때, param는 호출 시 사용한 param attribute
        function ${popupid}_click(vue,  param, callback){
        	 
            ${popupid}_vue.callback =  callback;
            $('#INQUIRY_Y').hide();
            ${popupid}_search(param);
            $("#INQUIRY_N").show();
            
            return true;
        }
                // 완료 후 콜백 호출
        function ${popupid}_to_work()
        {
          if(${popupid}_vue.callback){
            eval(${popupid}_vue.callback + '();');
           }
        }
        function  ${popupid}_save() {
        	if(!validate("${popupid}_form")) return;
        		${popupid}_vue.form_data.INQUIRY =  $("#INQUIRY_Edit_N").Editor("getText" );
        		post('/api/qna/save', ${popupid}_vue.form_data, function(data){
					alert(msg_complete);
					if(opener){
						opener.search();
						window.close();
					}
					else{
						$('#${popupid}').modal('hide');
						search();
					}
					
					});
        		$('#inquiry_pop').trigger('click');
        }

        <c:if test="${sessionScope.accessInfo.get('etc')}">
        function ${popupid}_saveAnswer() {
        	if(validate("popup_area")) {
        		form_data.ANSWER =  $("#ANSWER_Edit_N").Editor("getText" );
        		 
        		Inq.save("/api/qnaAnswer/saveAnswer", function(){
        			search();
        			$('#${popupid}').modal('hide');
        		})
        	}
        }
        </c:if>

        function deleteInq() {
        	Inq.del('/api/qna/delete', function() {
        		alert(msg_complete);
        		search();
        		$('#${popupid}').modal('hide');
        	});
        }
        
        function ${popupid}_search(param){
        	
        	 
        	if(param){
				Inq.getInfo('/api/qna/info?INQ_ID=' + param, function(data){
					${popupid}_vue.form_data = data;
					 ${popupid}_param = data;
					if(form_data.ANSWER != null && form_data.ANSWER.trim() != ""){
						disableYn = true; //Y
					}else{
						disableYn = false; //N
					}
					
					setTimeout(function(){
						$("#INQUIRY_Edit_N").Editor("setText", data.INQUIRY ? data.INQUIRY:"");
						$("#ANSWER_Edit_N").Editor("setText", data.ANSWER ? data.ANSWER:"");
						/* if(disableYn ||  ${sessionScope.accessInfo.get('etc')}) {
						} */
					},200)
					 
					 
					commentList();
					if(data.INS_ID === _USER_ID_ || ${sessionScope.accessInfo.get('etc')}) // 글쓴 사람과 접속자가 같거나 etc 권한? 
					{
						$("#deleteBtn").prop("disabled",false);
						$('#ANSWER_N').hide();
						if(${sessionScope.accessInfo.get('etc')} && data.INS_ID !== _USER_ID_){
							$("#INQUIRY_Y").show();		
							$("#INQUIRY_N").hide();
							$('#ANSWER_Y').hide();
							$('#ANSWER_N').show();
							$("#saveBt").hide();
						}
						else if(${sessionScope.accessInfo.get('etc')} && data.INS_ID === _USER_ID_){
							$("#INQUIRY_N").show();
							$("#INQUIRY_Y").hide();
							$('#ANSWER_Y').hide();
							$('#ANSWER_N').show();
						}else{
							if(disableYn) //답변이 있을때 
							{
								$("#INQUIRY_Y").show();		
								$("#INQUIRY_N").hide();
								$("#ANSWER_Y").show();
							}else{//답변이 없을때 
								$("#ANSWER_Y").hide();
							}
						}
					
					}
					else
					{
						$('#saveBt').hide();
						$("#deleteBtn").prop("disabled",true);
						$('#ANSWER_N').hide();
						$('#ANSWER_Y').show();
						$("#INQUIRY_Y").show();		
						$("#INQUIRY_N").hide();
					}
					
				});
        	}else{
        		disableYn = false;
        		$("#deleteBtn").css("display","none");
        		 
        		 
        		setTimeout(function(){
					$("#INQUIRY_Edit_N").Editor("setText", '');
					$("#ANSWER_Edit_N").Editor("setText", '');
	        		${popupid}_vue.form_data = ${popupid}_param;
					/* if(disableYn  ||  ${sessionScope.accessInfo.get('etc')}) {
						 
					} */
				},200)

        	}
       	 // 
        }
        </script>