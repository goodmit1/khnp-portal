<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<% response.setHeader("Cache-Control","no-cache"); response.setHeader("Pragma","no-cache"); response.setDateHeader("Expires",0); %>
<!--  게시물 관리 -->
<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
<style>
.col .row-fluid.Editor-container{
	width:90%;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col">
			<fm-input id="S_INQ_TITLE" name="INQ_TITLE" title="<spring:message code="NC_INQ_INQ_TITLE" text="문의제목"/>"></fm-input>
		</div>
		<div class="col">
			<fm-input id="S_INS_NAME" name="INS_NAME" title="<spring:message code="NC_INQ_INS_NAME" text="문의자명"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=SVC" id="S_SVC_CD" emptystr=""
				name="SVC_CD" title="<spring:message code="NC_INQ_SVC" text="서비스"/>"></fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=INQ" id="S_INQ_CD" emptystr=""
				name="INQ_CD" title="<spring:message code="NC_INQ_INQ" text="문의유형코드"/>"></fm-select>
		</div>
		<div class="col btn_group">
			<input type="button" class="searchBtn btn" onclick="search()" value="<spring:message code="btn_search" text="" />" />
		</div>
		<div class="col btn_group_under">

			<fm-sbutton cmd="update" class="btn btn-primary newBtn" onclick="newInq()" ><spring:message code="btn_new" text="" /></fm-sbutton>

<%-- 			<fm-sbutton cmd="delete" id="deleteBtn" class="btn btn-primary delBtn" onclick="deleteInq()"><spring:message code="btn_delete" text=""/></fm-sbutton> --%>
<%-- 			<fm-sbutton cmd="update" class="btn btn-primary saveBtn" onclick="saveInq()" ><spring:message code="btn_save" text="" /></fm-sbutton> --%>
		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div id="mainTable" style="height: 450px" class="ag-theme-fresh"></div>
	<c:if test="${sessionScope.ADMIN_YN ne 'Y'}">
<%-- 		<jsp:include page="detail.jsp"></jsp:include> --%>
	</c:if>
	<c:if test="${sessionScope.ADMIN_YN eq 'Y'}">
<%-- 		<jsp:include page="mngDetail.jsp"></jsp:include> --%>
	</c:if>
		<fm-popup-button popupid="inquiry_pop" style="display:none" popup="./inquiry_pop.jsp" cmd="update" param="ReqParam"></fm-popup-button>
</div>
<script>
var Inq = new Req();
var ReqParam = null;
var disableYn = false;
mainTable;
$(function() {


	var
	columnDefs = [{
		headerName : "",
		field : "INQ_ID",
		hide : true
	}, {
		headerName : "<spring:message code="NC_INQ_INQ_TITLE" text="문의제목"/>",
		field : "INQ_TITLE",
		width : 500
	}, {
		headerName : "<spring:message code="NC_INQ_SVC" text="서비스"/>",
		field : "SVC_CD_NM",
		width : 130
	}, {
		headerName : "<spring:message code="NC_INQ_INQ" text="문의유형코드"/>",
		field : "INQ_CD_NM",
		width : 200
	}, {
		headerName : "<spring:message code="NC_VM_INS_TMS" text="등록일시"/>",
		field : "INS_TMS",
		valueGetter:function(params) {
			return formatDate(params.data.INS_TMS,'datetime');
		}
	}, {
		headerName : "<spring:message code="NC_INQ_ANSWER_YN" text="답변여부"/>",
		field : "ANSWER_YN",
		cellRenderer:function(params) {
			var name = "RED";
			if(params.data.ANSWER_YN == "Y"){
				name = "GREEN";
			}
			return '<img src="/res/img/' + name + '-icon.png">';
		}
	}, {
		headerName : "<spring:message code="NC_INQ_INS_NAME" text="문의자"/>",
		field : "INS_NAME"
	} ];
	var
	gridOptions = {
		hasNo : true,
		columnDefs : columnDefs,
		rowData : [],
		cacheBlockSize: 100,
		enableSorting : true,
		enableColResize : true,
		sizeColumnsToFit: true,
		rowSelection : 'single',
		enableServerSideSorting: false,
		onSelectionChanged : function() {
			var arr = mainTable.getSelectedRows();
			ReqParam = arr[0].INQ_ID;
			checkUserId(arr[0].INS_ID);
			$('#inquiry_pop_button').trigger('click');
// 			Inq.getInfo('/api/qna/info?INQ_ID=' + arr[0].INQ_ID, function(data){
// 				ReqParam = data;
// 				$("#INQUIRY").Editor("setText", data.INQUIRY ? data.INQUIRY:"");
// 				$("#ANSWER").Editor("setText", data.ANSWER ? data.ANSWER:"");
// 				if(form_data.ANSWER != null && form_data.ANSWER.trim() != ""){
// 					disableYn = true;
// 				}else{
// 					disableYn = false;
// 				}
// 				<c:if test="${sessionScope.ADMIN_YN ne 'Y'}">
// 				if(!disableYn && data.INS_ID==_USER_ID_)
// 				{
// 					$("#deleteBtn").prop("disabled",false);
// 				}
// 				else
// 				{
// 					$("#deleteBtn").prop("disabled",true);
// 				}

// 			</c:if>
// 			});

		}
	}
	mainTable = newGrid("mainTable", gridOptions);
	search();
});

//조회
function search() {
	search_data.ALL_YN="${sessionScope.accessInfo.get('etc') ?"Y":"N"}";
	Inq.search('/api/qna/list', function(data) {
		 
		mainTable.setData(data);
	});
}

//저장




//삭제
function deleteInq() {
	Inq.del('/api/qna/delete', function() {
		alert(msg_complete);
		search();
		$('#inquiry_pop').trigger('click');
	});
}

// 추가
function newInq(){
	ReqParam = null;
	accessInfo.update = true;
	$('#ANSWER_N').hide();
	$('#ANSWER_Y').hide();
	$('#inquiry_pop_button').trigger('click');
	$('#INS_NAME').html(_USER_NM_)
	$('#TEAM_NM').html(_TEAM_NM_)
	Inq.setData({});
}

function checkUserId(param) {
	accessInfo.update = _USER_ID_ === param;
}

</script>