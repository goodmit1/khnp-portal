<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm" onsubmit="return false">
    	<jsp:include page="list.jsp"></jsp:include>
    	</form>
    </layout:put>
    <layout:put block="popup_area">
    	<jsp:include page="/admin/popup/prgm.jsp"></jsp:include>
    	<script>
    	var popup_prgm = newPopup("PRGM_NM", "prgm_popup");
    	popup_prgm.setMapping({"PRGM_NM":"PRGM_NM", "PRGM_ID":"PRGM_ID"});
    	popup_prgm.setInputInfo({"PRGM_ROLE_ID":"ROLE_ID"});
    	//popup_prgm.multi=true;    	


    	</script>
	</layout:put>
</layout:extends>