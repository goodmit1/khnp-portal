<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html; charset=UTF-8"%>
<style>
#mainTable { height:calc(100vh - 221px);}
#mainTable2 { height:calc(100vh - 300px); clear:both }
div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(1) > div.ag-cell-label-container.ag-header-cell-sorted-none{
	width: auto;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->

		<div class="col col-sm">
			<fm-input id="S_ROLE_NM" name="ROLE_NM" title="<spring:message code="FM_USER_USER_TYPE" text="" />"></fm-input>

		</div>
		<div class="col col-sm">
			<fm-input id="S_ROLE_DESC" name="ROLE_DESC" title="<spring:message code="NC_DC_CMT" text="" />"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<button type="button" class="searchBtn btn" onclick="searchRole()"><spring:message code="btn_search" text="" /></button>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<div class="btn_group">
			<fm-sbutton cmd="update" class="btn btn-primary exeBtn contentsBtn tabBtnImg sync" onclick="memoryReload()"><spring:message code="label_apply_mem" text="메모리 반영"/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary newBtn contentsBtn tabBtnImg new" onclick="newRole()"><spring:message code="btn_new" text="" /></fm-sbutton>
			<c:if test="${sessionScope._USER_TYPE_ == '1'}">
				<fm-sbutton cmd="delete" class="btn btn-primary delBtn contentsBtn tabBtnImg del" onclick="delRole()"><spring:message code="btn_delete" text=""/></fm-sbutton>
			</c:if>
			<fm-sbutton cmd="update" class="btn btn-primary saveBtn contentsBtn tabBtnImg save" onclick="saveRole()"><spring:message code="btn_save" text="" /></fm-sbutton>
		</div>
	</div>
	<div class="col-sm-3 h100 ">

				<div id="mainTable"  class="ag-theme-fresh"></div>

	</div>
	<div class="col-sm-9 right-panel">
		<div class="form-panel detail-panel panel panel-default h100">

			<div class="panel-body">


				<div class="col col-sm-6">
					<fm-input id="ROLE_NM" name="ROLE_NM" required="true" title="<spring:message code="FM_USER_USER_TYPE"/>"></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-input id="ROLE_DESC" name="ROLE_DESC" title="<spring:message code="NC_DC_CMT" text="" />"></fm-input>
				</div>
				<div class="col col-sm-12">
					<div class="table_title" style="margin:0px;">
						<%-- <spring:message code="label_prgm_permission"/> --%>

						<div class="btn_group" style="margin-top: 5px; margin-bottom: 5px; text-align: right;  margin-right: 10px;">
							<fm-sbutton cmd="update" onclick="newPrgm()" title="<spring:message code="btn_new" text="" />" class="btn icon"><i class="fa fat add fa-plus-square"></i></fm-sbutton>
							<fm-sbutton cmd="update" onclick="delPrgm()" title="<spring:message code="btn_delete" text=""/>" class="btn icon"><i class="fa fat red fa-minus-square"></i></fm-sbutton>
					 	</div>
					</div>
					<div id="mainTable2"  class="ag-theme-fresh"></div>
				</div>
				<script>

					var grid1, grid2;
					var role = new Req();
					$(document).ready(function(){
						var columnDefs =[{headerName: "<spring:message code="FM_USER_USER_TYPE" text="" />", field: "ROLE_NM"},
						    		     {headerName: "<spring:message code="NC_DC_CMT" text="" />", field: "ROLE_DESC"},

						    		     ];
			    		var gridOptions = {
			    			    columnDefs: columnDefs,
			    			    rowData: [],
			    			    enableSorting: true,
			    			    enableColResize: true,
			    			    editable : false,
			    			    rowSelection:'single',

			    			    onSelectionChanged: function()
							    {
			    			    	getRoleInfo();


							    }
			    			};
			    		grid1 = newGrid("mainTable", gridOptions); // 권한 목록
			    		//SELECT_YN
			    		var columnDefs2 =[{headerName: "ID", field: "PRGM_ID", hide:true},
			    		                  {headerName: "<spring:message code="title_prgm"/>", field: "PRGM_NM"},
						    		     {headerName: "<spring:message code="btn_search" text="" />", field: "SELECT_YN", maxWidth: 70, cellRenderer:YNCheckboxRenderer},
						    		     {headerName: "<spring:message code="btn_save" text="" />", field: "UPDATE_YN", maxWidth: 70,cellRenderer:YNCheckboxRenderer},
						    		     {headerName: "<spring:message code="btn_delete" text=""/>", field: "DELETE_YN", maxWidth: 70,cellRenderer:YNCheckboxRenderer},
						    		     //{headerName: "추가", field: "INSERT_YN", maxWidth: 70,cellRenderer:YNCheckboxRenderer},
						    		     //{headerName: "기타", field: "PRINT_YN", maxWidth: 70,cellRenderer:YNCheckboxRenderer},
						    		     {headerName: "<spring:message code="btn_others" text=""/>", field: "PRINT_YN", maxWidth: 70,cellRenderer:YNCheckboxRenderer}
						    		     ];
			    		var gridOptions2 = {
			    			    columnDefs: columnDefs2,
			    			    rowData: [],
			    			    enableSorting: true,
			    			    enableColResize: true,
			    			    getRowNodeId : function(data) {
			    			    	return "" + data.PRGM_ID;
			    			    },
			    			    editable : true,

			    			};
			    		grid2 = newGrid("mainTable2", gridOptions2); //프로그램 목록

						searchRole();
					})

					// 메모리 반영
					function memoryReload()
					{
						post('/api/role/save_memory',{}, function(){
							alert("<spring:message code="msg_applied"/>");
						})
					}

					// 신규 프로그램
					function newPrgm(obj)
					{
						popup_prgm.onClosePopup=function()
				    	{
				    		grid2.insertRow({PRGM_ID: role.getData().PRGM_ID, PRGM_NM: role.getData().PRGM_NM, SELECT_YN:'Y', INSERT_YN:'Y', DELETE_YN:'Y', UPDATE_YN:'Y', PRINT_YN:'Y'});
				    	}
						clickOpenPopup(obj, inputvue, "PRGM_NM");
					}

					// 상세 정보
					function getRoleInfo()
					{
						var arr = grid1.getSelectedRows();
				    	role.getInfo('/api/role/info?ROLE_ID='
								+ arr[0].ROLE_ID, function(data){

				    		grid2.setData(data.ROLE_PRGM || []);
				    	});
					}

					// 권한 조회
					function searchRole() {
						role.search('/api/role/list',
								function(data){
									grid1.setData(data);
									role.clearData();
									grid2.clearData();

								});

					}

					// 권한 새로 고침
					function reloadRole()
					{
						var rowIdx = grid1.getSelectedRowIndex();
						role.search('/api/role/list',
								function(data){
									grid1.setData(data);
									grid1.setSelected(rowIdx);

								});
					}

					// 프로그램 삭제
					function delPrgm()
					{
						var param = grid2.getSelectedRows()[0];
						if(!param)
						{
							alert(msg_select_first);
							return false;
						}
						post('/api/role/delete_prgm', {list: JSON.stringify(grid2.getSelectedRows())}, function(data){

							})

						grid2.deleteRow();
					}

					// 권한 삭제
					function delRole() {

						role.del('/api/role/delete', function() {
								searchRole();
								role.setData({a:""});
						});
					
					}

					// 권한 저장
					function saveRole() {
						if(validate("input_area"))
						{
							role.putData({"ROLE_PRGM" : JSON.stringify(grid2.getData())})
							role.save('/api/role/save', function(data) {
								 role.getData().ROLE_ID=data.ROLE_ID
								reloadRole();
							});
						}
					}
					// 권한 추가
					function newRole() {
						var newData = {ROLE_PRGM: role.getData().ROLE_PRGM};
						role.setData(newData);
						 document.getElementById("ROLE_NM").focus();
					}

				</script>

			</div>
		</div>
	</div>
</div>

