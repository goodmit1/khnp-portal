<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->


			<div class="col ">
					<table>
					<tr > <td>
					<fm-date id="CONNECTTIME_FROM" name="CONNECTTIME_FROM" title="<spring:message code="label_access_time"/>"></fm-date>
					</td>
					<td class="tilt"> ~ </td>
					<td>
					<fm-date id="CONNECTTIME_TO" name="CONNECTTIME_TO" ></fm-date>
					</td></tr>
					</table>
			</div>
			<div class="col col-sm">
					<fm-input id="PRGM_NM" name="PRGM_NM" title="<spring:message code="title_menu"/>"></fm-input>
			</div>
			<div class="col col-sm">
					<fm-input id="USER_ID" name="USER_ID" title="<spring:message code="FM_USER_USER_ID" text=""/>"></fm-input>
			</div>

		<div class="col btn_group nomargin">
			<button type="button" class="btn searchBtn" onclick="search()"><spring:message code="btn_search" text="" /></button>


		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
				<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="amount" text="총 개수"/> : <span id="mainTable_total">0</span>&nbsp)
			<div class="btn_group">
				<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
			</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height:400px;"></div>
	</div>	
	</div>
	<div class="box_s">
	<h5 class="table_title layout name"><spring:message code="label_con_user"/></h5>
	<div class="layout background mid">
		<div id="mainTable2" style="height:180px" class="ag-theme-fresh"></div>
	</div>
	</div>
</div>
<script>
					var req = new Req();
					var mainTable, mainTable2;


					$(function() {


						var
						columnDefs = [ {
							headerName : "<spring:message code="title_menu"/>",
							field : "PRGM_NM"
						},{
							headerName : "<spring:message code="ACCESS_TMS"/>",
							field : "CONNECTTIME"}, {
							headerName : "<spring:message code="label_con_cnt"/>", cellStyle:{'text-align':'center'}, valueFormatter:function(params){
								return formatNumber(params.value);
							}
							,
							field : "CONNECT_CNT"
						} ];
						var
						gridOptions = {
							hasNo : true,
							columnDefs : columnDefs,
							rowModelType: 'infinite',
							cacheBlockSize: 100,
							rowData : [],
						    enableSorting : false,
							enableColResize : true,
							rowSelection : 'single',
							onSelectionChanged : function() {
								var arr = mainTable.getSelectedRows();
								req.search('/api/log_prgm/list_sub?PRGM_ID='
										+ arr[0].PRGM_ID + '&CONNECTTIME=' + formatDatePattern( arr[0].CONNECTTIME,'yyyy-MM-dd'), function(data){
									mainTable2.setData(data);
								});
							},
						}

						var
						columnDefs2 = [ {
							headerName : "<spring:message code="FM_USER_USER_ID" text=""/>",
							field : "USER_ID"
						}, {
							headerName : "<spring:message code="FM_USER_USER_NAME" text="" />",
							field : "USER_NAME"
						}, {
							headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />",
							field : "TEAM_NM"
						},{
							headerName : "<spring:message code="ACCESS_TMS"/>",
							field : "INS_TMS",
							valueGetter: function(params) {
						    	return formatDate(params.data.INS_TMS,'datetime')
							}
						}, {
							headerName : "IP",
							field : "IPADDRESS"
						} ];

						var	gridOptions2 = {
							hasNo : true,
							columnDefs : columnDefs2,
							rowData : [],
							enableSorting : true,
							enableColResize : true,
						}
						mainTable = newGrid("mainTable", gridOptions);
						mainTable2 = newGrid("mainTable2", gridOptions2);
						var today = formatDate(new Date(), 'date');

						req.setSearchData({CONNECTTIME_TO: today, CONNECTTIME_FROM:today});
						search();
					});
					function search() {


						req.searchPaging('/api/log_prgm/list', mainTable);
						mainTable2.clearData();
					}
					// 엑셀 내보내기
					function exportExcel()
					{
						var	columnDefs3 = [ {
							headerName : "<spring:message code="title_menu"/>",
							field : "PRGM_NM"
						}, {
							headerName : "<spring:message code="FM_USER_USER_ID" text=""/>",
							field : "USER_ID"
						}, {
							headerName : "<spring:message code="FM_USER_USER_NAME" text="" />",
							field : "USER_NAME"
						}, {
							headerName : "<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />",
							field : "TEAM_NM"
						},{
							headerName : "<spring:message code="ACCESS_TMS"/>",
							field : "INS_TMS",
							valueGetter: function(params) {
						    	return formatDate(params.data.INS_TMS,'datetime')
							}
						}, {
							headerName : "IP",
							field : "IPADDRESS"
						} ];
						exportExcelServer("mainForm", '/api/log_prgm/list_excel', '<spring:message code="label_log" /><spring:message code="title_prgm" />',columnDefs3, req.getRunSearchData())
					}
				</script>


