<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
	<!-- 입력 Form -->
	<div class="form-panel detail-panel panel panel-default">

			<div class="panel-body">

					<input type="hidden" id="USER_ID" name="USER_ID">
					<input type="hidden" id="LOGIN_ID" name="LOGIN_ID">

					<div class="col col-sm-6">
						<fm-input id="LOGIN_ID" name="LOGIN_ID" required="true" title="ID"></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-input id="USER_NAME" name="USER_NAME" title="<spring:message code="NC_FW_USER_NAME" text="사용자"/>" required="true"></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-select url="/api/code_list?grp=sys.yn" id="USE_YN"
							name="USE_YN" title="<spring:message code="USE_YN" text=""/>"></fm-select>
					</div>
					<div class="col col-sm-6">
						<fm-popup id="TEAM_NM" name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_NM" text="팀명"/>"></fm-popup>
						<input type="hidden" id="TEAM_CD" name="TEAM_CD" />
					</div>
					<div class="col col-sm-6">
						<fm-input id="OFFICE_NO" name="OFFICE_NO" title="<spring:message code="OFFICE_NO" text="내선번호"/>"></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-input  id="MOBILE"
							name="MOBILE" title="<spring:message code="MOBILE" text="핸드폰"/>"></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-input  id="EMAIL"
							name="EMAIL" title="<spring:message code="EMAIL" text="이메일"/>"></fm-input>
					</div>

			</div>

	</div>
