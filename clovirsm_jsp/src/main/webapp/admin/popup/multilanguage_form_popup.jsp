<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<fm-modal id="multilanguage_popup" title="<spring:message code="FM_DDIC_FM_NL_POP" text="다국어"/>" cmd="header-title">
	<span slot="footer">
		<template>
			<fm-sbutton cmd="update" class="btn btn-primary" onclick="saveLang()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
			<fm-sbutton cmd="delete" class="btn btn-primary" onclick="delLang()"><spring:message code="btn_delete" text="삭제"/></fm-sbutton>
		</template>
	</span>
	<form id="multilanguage_popup_form" action="none">
		<template>
			<div class="table_title">
				[<span id="selected_ddic_val"></span>]
				<div class="btn_group">
			 		<button type="button" onclick="newLang()" class="btn icon"><i class="fa fat add fa-plus-square"></i></button>
				</div>
			</div>
			<div id="multilanguage_popup_grid" style="height:250px" class="ag-theme-fresh"></div>
		</template>
	</form>

	<script>
		var multilanguage_popup = newPopup("multilanguage_popup_button", "multilanguage_popup");
		var multilanguage_popup_param = {};
		var multilanguage_popup_parent_vue = null;
		var multilanguage_popup_vue = new Vue({
			el: '#multilanguage_popup',
			data: {
				form_data: multilanguage_popup_param
			}
		});
		function saveLang(){
			if(multilanguage_popup_grid.getSelectedRows().length==0){
				alert(msg_select_first);
				return;
			}
			makeParam();
			post("/api/popup/multilang/save", multilanguage_popup_param, function(){
				alert(msg_complete);
				searchLang();
			})
		}
		function delLang(){
			if(multilanguage_popup_grid.getSelectedRows().length==0){
				alert(msg_select_first);
				return;
			}
			makeParam();
			post("/api/popup/multilang/delete", multilanguage_popup_param, function(){
				alert(msg_complete);
				searchLang();
			});
		}
		function makeParam(){
			multilanguage_popup_grid.stopEditing();
			multilanguage_popup_param.selected_json = JSON.stringify(multilanguage_popup_grid.getSelectedRows());
		}
		function newLang(){
			multilanguage_popup_grid.insertRow({IU:"I"});
		}
		function multilanguage_popup_click(vue, param){
			multilanguage_popup_parent_vue = vue;
			if(param && param.DD_ID && param.DD_ID != ""){
				multilanguage_popup_search(param);
			} else {
				alert(msg_select_first);
				return false;
			}
			return true;
		}

		function multilanguage_popup_search(param){
			$("#selected_ddic_val").html(param.KO_DD_DESC);
			multilanguage_popup_param = {
				ID: param.DD_ID + '.' + param.DD_VALUE,
				TABLE_NM: "FM_DDIC",
				FIELD_NM: "DD_VALUE"
			}
			multilanguage_popup_vue.form_data = multilanguage_popup_param;
			searchLang();
		}

		function searchLang(){
			multilanguage_popup_req.searchSub('/api/popup/multilang/list', multilanguage_popup_param, function(data) {
				multilanguage_popup_grid.setData(data);
			});
		}

		var multilanguage_popup_req = new Req();
		var multilanguage_popup_grid;
		$(document).ready(function(){
			$.get("/api/code_list?grp=sys.lang", function(data){
				var list = [];
				for(var i=0; i<data.length; i++){
					if(i == "ko"){
						continue;
					}
					list.push(i);
				}
//				for(var i in data){
//				}
				makeGrid(list);
			})

		});

		function makeGrid(langList){
			var multilanguage_popup_gridColumnDefs =[
   				{headerName: "", field: "TABLE_NM", hide: true},
   				{headerName: "", field: "FIELD_NM", hide: true},
   				{headerName: "", field: "ID", hide: true},
   				{headerName: "Lang", field: "LANG_TYPE", width: 100,
   					editable: function(params)	{return params.data.IU=='I'},
   					cellEditor: 'agSelectCellEditor',
   					cellEditorParams : {
   							values: langList
   					}
   				},
   				{headerName: "Name", field: "NM", width: 140, editable: true}

   			];
   			var multilanguage_popup_gridOptions = {
   				columnDefs: multilanguage_popup_gridColumnDefs,
   				rowData: [],
   				sizeColumnsToFit:true,
   				enableSorting: true,
   				enableColResize: true,
   				editable : true
   			};
 			multilanguage_popup_grid = newGrid("multilanguage_popup_grid", multilanguage_popup_gridOptions);
		}
	</script>
</fm-modal>