<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
pageContext.setAttribute("popup_id", "menu");
%>
<fm-modal id="${popup_id}_popup" title="<spring:message code="select_menu" text="메뉴 선택"/>"  cmd="header-title">

	<span slot="footer">
		<input type="button" class="btn" value="<spring:message code="no_select" text="선택없음"/>" onclick="setPopupReturnVal({})" >
		<input type="button" class="btn" value="<spring:message code="btn_reload" text="새로고침"/>" onclick='$("#popup_menu_tree").trigger("reload");' >
	</span>
	<form id="${popup_id}_popup_form" style="height:300px" action="none">
			<fm-tree id="popup_menu_tree"  url="/api/menu/list" :field_config="{onselect:menu_popup_click, root:0,id:'ID',parent:'PARENT_ID',text:'MENU_NM',icon:'ICON'}" ></fm-tree>
	</form>



	<script>
	function menu_popup_click(selectedArr)
	{

		setPopupReturnVal(selectedArr[0]);
	}

	</script>


</fm-modal>