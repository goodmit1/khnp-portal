<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%
pageContext.setAttribute("popup_id", "user");
%>
<fm-modal id="${popup_id}_popup" title="<spring:message code="label_select_user" text=""/>" cmd="header-title" >

	<span slot="footer">
		<input type="button" class="btn" value="<spring:message code="no_select" text="선택없음"/>" onclick="setPopupReturnVal({})" >

	</span>
	<form id="${popup_id}_popup_form"  >
	<input type="hidden" name="POPUP_ID" value="207" />
	<input type="hidden" name="TEAM_CD" id="TEAM_CD"  v-model="form_data.TEAM_CD" />
	<input type="hidden" name="default_search_field" value="USER_ID,LOGIN_ID,USER_NAME" />
	<div id="${popup_id}_popupsearch_area" class="search-panel panel panel-default">
		<div class="panel-body">

			<div class="row">
			<div class="col col-sm-2">
				<fm-select id="keyword_field" emptystr="<spring:message code="label_field" text=""/>" :options="{'LOGIN_ID':'<spring:message code="FM_USER_USER_ID" text=""/>', USER_NAME:'<spring:message code="FM_USER_USER_NAME" text="사용자" />'}" ></fm-select>
			</div>
				<div class="col col-sm-4">
				<fm-select id="keyword_operator" emptystr="<spring:message code="label_operator" text=""/>" url="/api/code_list?grp=sys.sqlCond" ></fm-select>
			</div>


			<div class="col col-sm-4">
				<fm-input id="keyword" name="keyword" placeholder="<spring:message code="label_keyword" text=""/>"></fm-input>
			</div>
			<div class="col btn_group">
			<input type="button" onclick="cur_popup.doSearch()" class="btn" value="<spring:message code="btn_search" text="검색"/>" />
			</div>
			</div>
		</div>


	</div>
	<div id="${popup_id}_grid1" style="height:250px" class="ag-theme-fresh"></div>
	</form>



	<script>
	var ${popup_id}_popup_grid1;
	$(document).ready(function(){
		var columnDefs1 =[
		                  {headerName: "<spring:message code="FM_USER_USER_ID" text=""/>", field: "LOGIN_ID", width:200},
		   			     {headerName: "<spring:message code="FM_USER_USER_NAME" text="" />", field: "USER_NAME", width: 300}];
		var gridOptions1 = {
			    columnDefs: columnDefs1,
			    rowData: [],
			    sizeColumnsToFit:false,
			    enableSorting: true,
			    rowSelection:'single',
			    enableColResize: true,
			    onSelectionChanged: function()
			    {
			    	var arr = ${popup_id}_popup_grid1.getSelectedRows();
			    	setPopupReturnVal(arr[0]);
			    }
			};
		${popup_id}_popup_grid1 = newGrid("${popup_id}_grid1", gridOptions1);

	})

	</script>


</fm-modal>
