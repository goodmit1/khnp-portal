<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<style>


</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->


		<div class="col col-sm-4">
						<fm-input id="S_BATCH_PRGM_NM" name="BATCH_PRGM_NM" title="<spring:message code="title_prgm"/>"></fm-input>
		</div>
		<div class="col col-sm-4">

			<fm-input id="S_CLASS_NM" name="CLASS_NM" title="클래스 명"></fm-input>
		</div>
		<div class="col btn_group">
			<input type="button" class="searchBtn btn" onclick="search()" value="<spring:message code="btn_search" text="" />" />
			<input class="btn btn-primary" type="button" onclick="exec()" value="실행 " />
			<fm-sbutton cmd="update" class="btn btn-primary" onclick="newBatch()"><spring:message code="btn_new" text="" /></fm-sbutton>
			<fm-sbutton cmd="delete" class="btn btn-primary" onclick="deleteBatch()"><spring:message code="btn_delete" text=""/></fm-sbutton>
			<fm-sbutton cmd="update" class="btn btn-primary" onclick="saveBatch()"><spring:message code="btn_save" text="" /></fm-sbutton>


		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div id="mainTable" style="height: 200px" class="ag-theme-fresh"></div>


	<ul class="nav nav-tabs">
	  	<li class="active">
	        <a  href="#tab1" data-toggle="tab"><spring:message code="title_subTable"/></a>
		</li>
	 	<li >
	        <a  href="#tab2" data-toggle="tab" onclick="listExecHistory()">실행이력</a>
		</li>

	</ul>
	<div class="tab-content "  >
		<div style="height:235px" class="form-panel detail-panel tab-pane active" id="tab1">


						<div class="col col-sm-6">
							<fm-input id="BATCH_PRGM_NM" name="BATCH_PRGM_NM" required="true" title="프로그램"></fm-input>
						</div>
						<div class="col col-sm-6">

							<fm-input id="CLASS_NM" name="CLASS_NM" title="클래스 명" required="true"></fm-input>
						</div>
						<div class="col col-sm-6">
							<fm-input id="CRON" name="CRON" title="CRON" required="true"></fm-input>
						</div>

						<div class="col col-sm-6">
							<fm-select url="/api/code_list?grp=sys.yn" id="USE_YN"
								name="USE_YN" title="<spring:message code="USE_YN" text=""/>"></fm-select>
						</div>
						<div class="col col-sm-12">
							<fm-input id="REMARK" name="REMARK" title="<spring:message code="NC_DC_CMT" text="" />"></fm-input>
						</div>
						<div class="col col-sm-12">
						* 클래스는 com.fliconz.fm.common.scheduler.IScheduleJob을 implements하여야 한다.
						 <a href="https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm">CRON 참고</a>
						</div>

		</div>
		<div style="height:235px" class="tab-pane" id="tab2">
			<div id="mainTable2" style="height:100%" class="ag-theme-fresh"></div>
		</div>
	</div>
</div>

				<script>
					var batch_admin = new Req();
					var mainTable;
					$(function() {
						var
						columnDefs = [ {
							headerName : "",
							hide : true,
							field : "BATCH_PRGM_ID"
						}, {
							headerName : "<spring:message code="title_prgm"/>",
							field : "BATCH_PRGM_NM"
						}, {
							headerName : "클래스 명",
							field : "CLASS_NM"
						}, {
							headerName : "CRON",
							field : "CRON"
						},   {
							headerName : "<spring:message code="USE_YN" text=""/>",
							field : "USE_YN",
							maxWidth:80
						}, {
							headerName : "최근실행일자",
							field : "EXE_TMS",
							valueFormatter: function(params) {
						    	return formatDate(params.value,'datetime')
							}
						}, {
							headerName : "실행결과",
							field : "RESULT_MSG",
							valueFormatter: function(params) {
								if(params.node.data.BATCH_EXE_RESULT_CD == 'F')
								{
									return  params.node.data.BATCH_EXE_RESULT_CD_NM + ' : ' + params.value;
								}
								else
								{
									return params.node.data.BATCH_EXE_RESULT_CD_NM;
								}
							}
						}];
						var
						gridOptions = {
							columnDefs : columnDefs,
							rowData : [],
							onSelectionChanged : function() {
								var arr = mainTable.getSelectedRows();
								batch_admin.getInfo('/api/batch_admin/info?BATCH_PRGM_ID='
										+ arr[0].BATCH_PRGM_ID);
								if($("#tab2").is(":visible"))
								{
									listExecHistory();
								}
							},
							enableSorting : true,
							enableColResize : true,
							rowSelection : 'single'
						}
						mainTable = newGrid("mainTable", gridOptions);

						var	columnDefs2 = [ {
							headerName : "실행일자",
							field : "EXE_TMS",
							valueFormatter: function(params) {
						    	return formatDate(params.value,'datetime')
							}
						},{
							headerName : "종료일자",
							field : "EXE_FINISH_TMS",
							valueFormatter: function(params) {
						    	return formatDate(params.value,'datetime')
							}
						}, {
							headerName : "실행상태",
							field : "BATCH_EXE_RESULT_CD_NM",
							width: 80
						}
						, {
							headerName : "결과메시지",
							field : "RESULT_MSG",
							width : 450,
							valueFormatter: function(params) {
								if(params.node.data.BATCH_EXE_RESULT_CD == 'S')
								{
									return  params.value + '건';
								}
								else
								{
									return params.value;
								}

							}
						}];
						var	gridOptions2 = {
							columnDefs : columnDefs2,
							rowData : [],
							enableSorting : true,
							sizeColumnsToFit:false,
							enableColResize : true
						}
						mainTable2 = newGrid("mainTable2", gridOptions2);
						search() ;
					});
					function search() {
						batch_admin.search('/api/batch_admin/list', function(data) {
							mainTable.setData(data);
						});
					}
					function listExecHistory(){
						var arr = mainTable.getSelectedRows();
						if(arr.length==0) return;
						post('/api/batch_admin/list_exec_history?BATCH_PRGM_ID='
								+ arr[0].BATCH_PRGM_ID, function(data){
							mainTable2.setData(data)
						});
					}
					function exec(){
						var arr = mainTable.getSelectedRows();
						if(arr.length==0) return;
						post('/api/batch_admin/exec?BATCH_PRGM_ID=' +
								+ arr[0].BATCH_PRGM_ID + '&CLASS_NM=' +  arr[0].CLASS_NM,
							function(data){
								alert(data.COUNT+"건 실행");

							}
						);
					}
					function saveBatch() {
						if(validate("input_area"))
						{
							batch_admin.save('/api/batch_admin/save', function(data) {
								batch_admin.getData().BATCH_PRGM_ID=data.BATCH_PRGM_ID
								search();
							});
						}
					}
					function deleteBatch() {
						batch_admin.del('/api/batch_admin/delete', function() {
							search();
						});
					}
					function newBatch(){
						batch_admin.setData({USE_YN:'Y'});
					}

				</script>

