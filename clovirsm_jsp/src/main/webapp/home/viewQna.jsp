<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<script>
function showQnAceDetail(e)
{
	showLoading();
	 
	var Stringid = e.getAttribute("data-id");
	 
	
	$("#qnaFrame").attr("src","/home/_viewQna.jsp?INQ_ID=" + Stringid );
	$("#qna_popup").modal();
}
</script>
<div id="qna_popup" class="modal  fade popup_dialog" role="dialog">
	<div class="modal-dialog  ">

		<!-- Modal content-->
		<div class="modal-content  container-fluid">
			<div class="modal-header header-title">
				<button type="button" id="qnaCloseBtn" class="close" data-dismiss="modal">&times;</button>
				Q&A
			</div>

			<div class="modal-body">
				<iframe name="qnaFrame" id="qnaFrame" scrolling="no"
					style="height: 370px; width: 100%; border: 0;"></iframe>
			</div>
		</div>
	</div>
</div>
<script>

function close()
{
	$("#qnaCloseBtn").click();
}
</script>

</fm-modal>
