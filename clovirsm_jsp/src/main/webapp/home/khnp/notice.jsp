<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div class="panel panel-default chart_area " id="notiList">
	<div class="panel-heading"><spring:message code="title_noti" text="" /><span onclick="go(event,'/admin/notice/index.jsp');"></span></div>
	<div class="panel-body panel-sm">
		<div class="dashboard_list" style="width:100%">
			<table>
			  	<tbody>
			  		<tr v-for="(req, index) in notiList" style="width:100%" v-if="notiList.length != 0">
			  			<td class="textleft"><a :href="'javascript:viewNotice(' + req.ID + ')'" class="notice_link">{{req.TITLE}}</a></td>
			  			<!-- <td><span class="dashboard_noti_date">{{req.INS_TMS | date('datetime')}}</span></td> -->
			  			<td><span class="dashboard_noti_date">{{formatDate(req.INS_TMS ,'date')}}</span></td>
			  		</tr>
			  		<tr v-if="notiList.length == 0" style="border-bottom: none;">
			  			<td><spring:message code="search_result_empty" text="조회된 결과가 없습니다."/></td>
			  		</tr>
			  	</tbody>
			</table>
		</div>
	
	
		<!-- <ul class="dashboard_list">
			<li class="dashboard_list_item" v-for="req in notiList" style="width:100%">
				<div class="dashboard_list_nm noti ellipsis">
					<a :href="'javascript:viewNotice(' + req.ID + ')'" class="notice_link">{{req.TITLE}}</a>
				</div>
				<div class="dashboard_list_icon noti_date">
					<span class="dashboard_noti_date">{{req.INS_TMS | date('yyyy-MM-dd')}}</span>
				</div>
			</li>
		</ul> -->
	</div>
</div>
<style>
#noticePop_button {
	display: none;
}
</style>
<script>
	var notiList = new Vue({
		el: '#notiList',
		data: {
			notiList: []
		},
		methods: {
			getReqInfo: function () {
				var that = this;
				$.get('/api/monitor/list/list_FM_NOTICE/', function(data){
					that.notiList= data;
				});
			}
		}
	});
	
	$(document).ready(function(){
		notiList.getReqInfo();
		refCallback.push("notiList.getReqInfo");
	});
	
</script>
