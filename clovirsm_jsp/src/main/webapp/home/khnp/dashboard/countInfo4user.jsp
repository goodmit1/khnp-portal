<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- DS 상태 -->
<div class="panel panel-default chart_area " id="rsc_cnt_area" >
	<div class="panel-heading"></div>
	<div class="panel-body panel-sm nopadding"   >
		<div class="sm_panel_content " style="display:flex; padding:0px;margin:0px; position:inherit; height: 100%;">
			 <div class="panel_fit color_area blue_area" id="ds_area" style="border-radius:0px;">
				<img src="/res/img/hynix/newHynix/dashboard/shape-35.png" />
				<div class="sm_panel_title" ><spring:message code="label_cost"/></div>
				<hr>
				<a href="/clovirsm/monitor/ds/index.jsp"><div class="ds_sm_val"  >{{count_data.FEE|number}}</div></a>
			</div>
			<div class="panel_fit color_area orange_area" id="vm_area" style="border-radius:0px;">
				<img src="/res/img/hynix/newHynix/dashboard/shape-36.png" />
				<div class="sm_panel_title" ><spring:message text="Server" code="server"/></div>
				<hr>
				<a href="/clovirsm/monitor/vm/index.jsp"><div class="ds_sm_val"  >{{count_data.NC_VM|number}}</div></a>
			</div>
		 	<div class="panel_fit color_area red_area" id="img_area" style="border-radius:0px;">
				<img src="/res/img/hynix/newHynix/dashboard/shape-37.png" />
				<div class="sm_panel_title"><spring:message code="expire" text="만료"/></div>
				<hr>
				<a href="/clovirsm/resources/image/index.jsp"><span class="ds_sm_val">{{count_data.NC_IMG }}</span></a>
				<!-- <div class="rsc_sm_val" >{{count_data.NC_IMG }}</div> -->
			</div>
		</div>
	</div>
</div>
<script>
var rsc_cnt_area = new Vue({
  el: '#rsc_cnt_area',
  data: {
	  count_data: {FEE:0, VM_CNT:0, VRA_CNT:0, IMG_CNT:0}
  },
  methods: {
	  getReqInfo: function () {
		var that = this;
		var todt = new Date();
		var param = {};
		var yyyy = todt.getFullYear();
		var mm = todt.getMonth() + 1;
		mm = mm > 9 ? mm : "0" + mm;
		$("#rsc_cnt_area > .panel-heading").text(yyyy + dateLabel.year + " " + mm + " " + dateLabel.month + " " + msg_use_item_status);
		param.YYYYMM = yyyy + mm;
		$.post('/api/monitor/list/select_count_resource/', param, function(data)
			{
				that.count_data= data[0];

			}
		)
    }
  }
});
$(document).ready(function(){
	rsc_cnt_area.getReqInfo();
	refCallback.push("rsc_cnt_area.getReqInfo");
})
</script>
