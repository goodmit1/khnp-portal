<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

		<!-- DS 상태 -->
<div class="panel panel-default chart_area "  >
	<div class="panel-heading"><a href="/clovirsm/monitor/ds/index.jsp">Datastore <spring:message code="label_usage"/></a></div>
	<div class="panel-body">
		<div id="ds_dc_area" class="dc_area">
		</div>
		<div id="monitor_ds" style="height: 300px" class="chart"></div>
	</div>
</div>

			<script>
			function getDSMonitorInfo(DC_ID) {
				$.get('/api/monitor/list/list_NC_MONITOR_DS_SUM/?DC_ID=' + DC_ID, function(data) {
					drawDSMonitorChart(data,DC_ID)
				});
			}
			function drawDSMonitorChart(list,DC_ID) {
				list = toLowerCaseJson(list);
				var cnt = 0;
				for(var i=0; i < list.length; i++) {
					cnt += list[i].y;
				}
				 
				var data = []
				for(var i = 0; i <= 10; i++) {
					var tmp = {rate: i, y: 0, id: "CNT_" + i, name: ((i ) + (i>0 ?"0":"")) + "%", color: "#48bdb6"};
					for(var idx in list) {
						if(list[idx].rate == i){
							tmp.y = list[idx].y;
							break;
						}
					}
					if(i > 2) {
						tmp.color = "#f7bc60";
					}
					if(i > 6) {
						tmp.color = "#fd5d55";
					}
					tmp.DC_ID=DC_ID;
					data.push(tmp);
				}
				 
				$("#ds_cnt").text(cnt);
				var config = {
						  chart: {
							    type: 'column'
							  },
							  title: {
							    text: ''
							  },
							  xAxis: {
								    type: 'category',
								    title:{
								    	text: 'disk <spring:message code="label_usage"/>'
								    }
								  },
							  yAxis: {
								  title: {
								      text: 'disk <spring:message code="cnt"/>'
								    }
								  },
							  legend: {
							    enabled: false
							  },
							  plotOptions: {
							    series: {
							      borderWidth: 0,
							      dataLabels: {
							        enabled: true,
							        /* format: '{point.name}:{point.y:.0f}<spring:message code="label_amount"/>' */
							        format: '{point.y:.0f}<spring:message code="label_amount"/>'
							      } 
							    }
							  },	
							series:[{name:'disk',data:data}]
						};

				 config.plotOptions.column={
						cursor:"pointer",
						events:{
							click:function(e) {
								location.href="/clovirsm/monitor/ds/index.jsp?DC_ID=" + e.point.options.DC_ID + "&START_PER=" + (e.point.options.rate*10)+ "&END_PER=" + ((e.point.options.rate*10)+9)
							}

						}
					}
				Highcharts.chart('monitor_ds', config);
			}
			chgDcCallback.ds_dc_area = "getDSMonitorInfo";
			$(document).ready(function(){
			})

			</script>
