<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div class="panel panel-default chart_area " id="qnaList">
	<div class="panel-heading">Q&A<span onclick="go(event,'/admin/inquiry/index.jsp');"></span></div>
	<div class="panel-body">
		<div class="dashboard_list" style="width:100%">
			<table>
			  	<tbody>
			  		<tr v-for="req in qnaList" v-if="qnaList.length != 0">
			  			<td class="textleft"><a style="cursor: pointer;" onclick="javascript:showQnAceDetail(this)"  :data-id=" req.INQ_ID"  >{{req.INQ_TITLE}}</a></td>
			  			
			  	<!-- 		<td><span >{{req.INS_TMS | date('datetime')}}</span></td> -->
			  			<td><span class="dashboard_qna_date">{{formatDate(req.INS_TMS ,'date')}}</span></td>
			  		</tr>
			  		<tr v-if="qnaList.length == 0" style="border-bottom: none;">
			  			<td><spring:message code="search_result_empty" text="조회된 결과가 없습니다."/></td>
			  		</tr>
			  	</tbody>
			</table>
		</div>


		<!-- <ul class="dashboard_list">
			<li class="dashboard_list_item" v-for="req in qnaList" style="width:100%">
				<div class="dashboard_list_nm qna ellipsis">
					<a :href="'javascript:showQnAceDetail(' + req.INQ_ID + ')'"    >{{req.INQ_TITLE}}</a>
				</div>
				<div class="dashboard_list_icon noti_date">
					<span class="dashboard_noti_date">{{req.INS_TMS | date('yyyy-MM-dd')}}</span>
					<img class="req_img" :src="'/res/img/dashboard/req_' + ((req.ANSWER==null  ||  req.ANSWER=='')?'D':'A') + '.png'"  ></img>
				</div>

			</li>
		</ul> -->
	</div>
</div>
<script>

	function adminCheck(){
		var val = ADMIN_YN;
		if(val === 'Y')
		{
			return val;
		}
		else{
			return '';
		}
		
		
	}

	var qnaList = new Vue({
		el: '#qnaList',
		data: {
			qnaList: []
		},
		methods: {
			getReqInfo: function () {
				var that = this;
				$.get('/api/monitor/get_qna_req?ALL_YN='+adminCheck(), function(data){
					 
					that.qnaList= data.QNA_CNT;
				});
			}
		}
	});

	$(document).ready(function(){
		qnaList.getReqInfo();
		refCallback.push("qnaList.getReqInfo");
	});
</script>
