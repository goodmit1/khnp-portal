<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.monitor.MonitorService"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
	String[] img = {"/res/img/rectangle2.png","/res/img/rectangle3.png","/res/img/rectangle4.png","/res/img/rectangle.png"};
	MonitorService monitorService = (MonitorService)SpringBeanUtil.getBean("monitorService");
	Map searchParam = new HashMap();
	searchParam.put("DEL_YN", "N");
	searchParam.put("TOP_YN", "Y");
	searchParam.put("limit", "4");
	searchParam.put("oder", "ASC");
	List noticeList = monitorService.getNoticeList(searchParam);
	request.setAttribute("list", noticeList);
	request.setAttribute("img", img);
%>
<style>
.carousel-inner img{
    width: 100%;
    height: 378px !important;
}
</style>
<div class="" style="width: 100%; height: 100%;">
  <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 100%;">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    	<c:forEach items="${ list }" var="i" end="3" varStatus="status">
    		<c:if test="${status.index eq 0}">
	    		<li data-target="#myCarousel" data-slide-to="${status.index}" class="active"></li>
    		</c:if>
    		<c:if test="${status.index ne 0}">
	    		<li data-target="#myCarousel" data-slide-to="${status.index}"></li>
    		</c:if>
    	</c:forEach>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
    <c:forEach items="${ list }" var="i" end="3" varStatus="status">
    	<c:if test="${status.index eq 0}">
	      <div class="item active">
	        <img src="${img[status.index]}" style="width:100%;">
	        <div class="carousel-caption">
	          <h3>${i.TITLE}</h3>
	          <div>${i.CONTENTS}</div>
	        </div>
	      </div>
      </c:if>
      <c:if test="${status.index ne 0}">
      	<div class="item">
	        <img src="${img[status.index]} " style="width:100%;">
	        <div class="carousel-caption">
	          <h3>${i.TITLE}</h3>
	          <div>${i.CONTENTS}</div>
	        </div>
        </div>
      </c:if>
	</c:forEach>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
</div>