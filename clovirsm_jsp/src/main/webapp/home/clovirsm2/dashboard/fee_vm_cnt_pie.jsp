<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

		<!-- DS 상태 -->

				<div id="fee_vm_cnt_chart" class="chart_area"></div>

			<script>
			function getFeeVmCntInfo()
			{
				$.get('/api/monitor/get_fee_vm_cnt', function(data) {
					drawFeeVmCntChart(data)
				});
			}
			function drawFeeVmCntChart(data) {
				 

				var vmCntData = [];
				var feeData = [];
				feeData[0] = {
					y: data.TEAM_FEE,
					name: "<spring:message code="CONST_TEAM_FEE" text="팀의 비용" />",
					id: "TEAM_FEE"
				};
				feeData[1] = {
						y: data.PRI_FEE,
						name: "<spring:message code="CONST_PRI_FEE" text="나의 비용" />",
						id: "PRI_FEE"
				};
				vmCntData[0] = {
						y: data.TEAM_VM_CNT,
						name: "<spring:message code="CONST_TEAM_VM_CNT" text="팀의 VM개수" />",
						id: "TEAM_VM_CNT"
				};
				vmCntData[1] = {
						y: data.PRI_VM_CNT,
						name: "<spring:message code="CONST_PRI_VM_CNT" text="나의 VM개수" />",
						id: "PRI_VM_CNT"
				};

				var config = {
					chart: { type: 'pie' },
					title: { text: '<spring:message code="CONST_VM_CNT_AND_FEE" text="VM개수 및 비용 현황" />' },
					xAxis: { type: 'category' },
					yAxis: {
						title: { text: '' }
					},
					legend: { enabled: false },
					plotOptions: {
						series: {
							borderWidth: 0,
							dataLabels: {
								enabled: true,
								format: '{point.name} {point.y:.0f}'
							}
						}
					},
					series:[{name:'<spring:message code="CONST_VM_CNT" text="VM개수" />',data: vmCntData},{name:'<spring:message code="CONST_VM_FEE" text="비용" />',data: feeData}]
				};

				//config.yAxis.min=0;

				config.plotOptions.column = { cursor:"pointer" }
				Highcharts.chart('fee_vm_cnt_chart', config);
			}
			$(document).ready(function(){
				getFeeVmCntInfo();
				refCallback.push("getFeeVmCntInfo");
			})

			</script>
