<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- DS 상태 -->
<style>
		.parent_area {
		    width: 100%;
		    height: 740px;
		    display: inline-block;
		    margin: auto;
		    margin-top: 20px;
		    position: relative;
		}
		.area_border{
			background: white;
		    min-width: 980px;
		    width: 95%;
		    margin: auto;
		    height: auto;
		}
		.parent_area .click_area {
			border-radius: 4px;
			box-shadow: 0px 3px 25px 0 rgba(0, 0, 0, 0.1);
			background-color: #ffffff;
			width: calc(24% - 20px);
			margin-left: 23px;
			height: 285px;
			display: inline-block;
			background: #fff;
		/* 	position: absolute; */
		/* 	bottom: 0px; */
		/* 	right: 0px; */
			float:left;
		    padding-top: 18px;
			padding-left: 39px;
			cursor: pointer;
		}
		/* .parent_area > .click_area.top { */
		/* 	top: 0px; */
		/* } */
		/* .parent_area > .click_area.left { */
		/* 	left: 0px; */
		/* } */
		.parent_area .click_area img{
			margin-top: 18px;
			width: 75px;
			float: left;
		}
		.parent_area .click_area .title_area {
			font-size: 26px;
			font-weight: bold;
			text-align: left;
			color: #000;
		}
		.parent_area .click_area .cmt_area {
			margin-top: 103px;
			font-size: 14px;
			text-align: left;
		}
		.parent_area .click_area .cmt_area ul {
			padding-left: 0px;
			list-style: none; /* Remove default bullets */
			width: 95%;
		}
		
		.parent_area .click_area .cmt_area ul li::before {
			content: "\2022";  /* Add content: \2022 is the CSS Code/unicode for a bullet */
			color: red; /* Change the color */
			font-weight: bold; /* If you want it to be bold */
			display: inline-block; /* Needed to add space between the bullet and the text */
			width: 1em; /* Also needed for space (tweak if needed) */
			margin-left: -1em; /* Also needed for space (tweak if needed) */
		}
</style>
<div class="panel panel-default chart_area " id="createServer" >
	<div class="panel-heading"><spring:message code="btn_server_create" text="서버생성요청" /></div>
	<div class="panel-body"  >
		<div id="input_area" style="text-align: center;">
			<div style="display: none">
				<fm-popup-button popupid="vm_insert_popup" popup="/clovirsm/resources/reqVm/hynix/popup.jsp?action=insert" cmd="update" class="newBtn" param="{}"></fm-popup-button>
				<fm-popup-button popupid="vra_insert_popup" popup="/clovirsm/resources/vra/newVra/popup.jsp?action=insert" cmd="update" class="newBtn" param="{}"></fm-popup-button>
				<fm-popup-button popupid="vm_spec_change" popup="/clovirsm/popup/vm_spec_change_form_popup.jsp?popupid=vm_spec_change" cmd="update" class="newBtn" param="{}"></fm-popup-button>
				<fm-popup-button popupid="vm_delete_form" popup="/clovirsm/popup/vm_delete_form_popup.jsp?popupid=vm_delete_form" cmd="update" class="newBtn" param="{}"></fm-popup-button>
			</div>
			<div class="parent_area">
				<div class="area_border">
					<div style="margin: auto;width: 95%;">
						<div class="click_area top left" onclick="vmClick();">
							<div class="title_area"><spring:message code="NEW_VM_TITLE_1" text="단일 서버"/></div>
							<img src="/res/img/hynix/icon-ask-server-one.png">
							<div class="cmt_area">
								<ul>
									<li><spring:message code="NEW_VM_CMT_1_1" text="1개의 서버 생성 요청"/></li>
								</ul>
								<ul>
									<li><spring:message code="NEW_VM_CMT_1_2" text="서비스별로 기본 프로그램이 설치된 서버를 복사하여 생성"/></li>
								</ul>
							</div>
						</div>
						<div class="click_area top" onclick="vraClick();">
							<div class="title_area"><spring:message code="NEW_VM_TITLE_2" text="패키지 서버"/></div>
							<img src="/res/img/hynix/icon-ask-server-many.png">
							<div class="cmt_area">
								<ul>
									<li><spring:message code="NEW_VM_CMT_2_1" text="서버의 묶음 생성 요청"/></li>
								</ul>
								<ul>
									<li><spring:message code="NEW_VM_CMT_2_2" text="예: Web, WAS, DB 3tier 서버군"/></li>
								</ul>
							</div>
						</div>
						<div class="click_area left" onclick="editClick();">
							<div class="title_area"><spring:message code="NEW_VM_TITLE_3" text="자원 변경"/></div>
							<img src="/res/img/hynix/icon-ask-server-delete.png">
							<div class="cmt_area">
								<ul>
									<li><spring:message code="NEW_VM_CMT_3_1" text="CPU, Mem, Disk 사이즈 변경요청"/></li>
								</ul>
								<ul>
									<li><spring:message code="NEW_VM_CMT_3_2" text="서버 한 대 단위로 변경"/></li>
								</ul>
								<ul>
									<li><spring:message code="NEW_VM_CMT_3_3" text="묶음 신청된 서버도 한 대 단위로 변경"/></li>
								</ul>
							</div>
						</div>
						<div class="click_area" onclick="delClick();">
							<div class="title_area"><spring:message code="NEW_VM_TITLE_4" text="삭제 요청"/></div>
							<img src="/res/img/hynix/icon-ask-server-change.png">
							<div class="cmt_area">
								<ul>
									<li><spring:message code="NEW_VM_CMT_4_1" text="서버 한 대 단위로 삭제 요청"/></li>
								</ul>
								<ul>
									<li><spring:message code="NEW_VM_CMT_4_2" text="묶음 신청된 서버도 한 대 단위로 삭제"/></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function vmClick() {
	$("#vm_insert_popup_button").click();
}
function vraClick() {
	$("#vra_insert_popup_button").click();
}
function editClick() {
	$("#vm_spec_change_button").click();
}
function delClick() {
	$("#vm_delete_form_button").click();
}
function callback() {
	 
}
</script>
