<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

		<!-- DS 상태 -->
		<div class="panel panel-default chart_area "  >
			<div class="panel-heading"><spring:message code="title_svc_vm_cnt" text="업무별 VM 갯수"/></div>
			<div class="panel-body" style="position:relative">


			 		<div id="monitor_vm_cnt" class="chart" ></div>

			</div>
		</div>


			<script>
			function getVMCntMonitorInfo()
			{
				$.get('/api/monitor/list/list_svc_pkubun_vmcount/?ROWS=10', function(data)
				{
					drawVMCntMonitorChart(data)
				});
			}
			function drawVMCntMonitorChart(list) {

				var data = [];
				var category=[];
				var p_kubun=[];
				var colors = ["#5d6ea3","#908bd0", "#c497cf"]
				var cidx = 0;
				for(var i=0; i< list.length; i++)
				{
					var idx = category.indexOf(list[i].CATEGORY_NM);
					if(idx<0){
						category.push(list[i].CATEGORY_NM);
						
					}
					var idx = p_kubun.indexOf(list[i].P_KUBUN_NM);
					if(idx<0){
						p_kubun.push(list[i].P_KUBUN_NM);
						data.push({name:list[i].P_KUBUN_NM, color:colors[cidx++], data:[]});
					}
				 
				}
				for(var i=0; i< list.length; i++)
				{
					var yidx = category.indexOf(list[i].CATEGORY_NM);
					var xidx = p_kubun.indexOf(list[i].P_KUBUN_NM);
					data[xidx].data.updateAtIdx(yidx, list[i].CNT, 0 );
				}
				 
				var config = {
						  chart: {
							    type: 'column'
							  },
							  title: {
							    text: ''
							  },
							  xAxis: {
								    type: 'category',
								    categories:category

								  },
							  yAxis: {
								    title: {
								      text: 'VM ' + msg_cnt
								    }

								  },
							  legend: {
							    enabled: false
							  },
							  plotOptions: {
							    series: {
							      borderWidth: 0,
							      dataLabels: {
							        enabled: true,
							        format: '{point.y}'
							      }
							    }
							  },
							series:data
						};

				//config.yAxis.min=0;


				Highcharts.chart('monitor_vm_cnt', config);
			}
			$(document).ready(function(){
				getVMCntMonitorInfo();
				refCallback.push("getVMCntMonitorInfo");
			})

			</script>
