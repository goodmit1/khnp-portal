<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<script>
function viewNotice(id)
{
	showLoading();
	$("#noticeFrame").attr("src","/home/_viewNotice.jsp?ID=" + id );
	$("#notice_popup").modal();
}
</script>
<div id="notice_popup" class="modal  fade popup_dialog" role="dialog">
	<div class="modal-dialog  ">

		<!-- Modal content-->
		<div class="modal-content  container-fluid">
			<div class="modal-header header-title">
				<button type="button" id="noticeCloseBtn" class="close" data-dismiss="modal"></button>
				<spring:message code="title_noti" text="" />
			</div>

			<div class="modal-body">
				<iframe name="noticeFrame" id="noticeFrame" scrolling="no"
					style="height: 502px; width: 100%; border: 0;"></iframe>
			</div>
		</div>
	</div>
</div>
<script>

function close()
{
	$("#noticeCloseBtn").click();
}


</script>

</fm-modal>
