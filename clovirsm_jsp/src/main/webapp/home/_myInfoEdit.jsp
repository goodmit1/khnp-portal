<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/popup_main">
<layout:put block="content">
<script src="/res/js/security.js"></script>
<style>
.col-sm-6 {width: 50%; float:left}
</style>
<form id="mainForm">
<div class="fullGrid" id="input_area" >

<div class="form-panel detail-panel panel panel-default padding0">
<div class="panel-body">

<div class="col col-sm-12">
	<fm-input id="USER_NAME" name="USER_NAME" title="<spring:message code="FM_USER_USER_NAME" text="사용자" />" required="true"></fm-input>
</div>
<div class="col col-sm-12">
	<fm-password id="PASSWORD" name="PASSWORD"  title="<spring:message code="label_password"/>" placeholder="<spring:message code="msg_password_role"/>"></fm-password>
</div>
<div class="col col-sm-12">
	<fm-password id="PASSWORD1" name="PASSWORD1" title="<spring:message code="label_password"/> <spring:message code="label_confirm" />"  placeholder="<spring:message code="msg_password_role"/>"></fm-password>
</div>
<div class="col col-sm-12">
	<fm-input  id="MOBILE_NO"
		name="MOBILE_NO" title="<spring:message code="MOBILE"/>"></fm-input>
</div>
<div class="col col-sm-12">
	<fm-input  id="EMAIL"
		name="EMAIL" title="<spring:message code="EMAIL" text="이메일"/>"></fm-input>
</div>
</div>
</div>
</div>

</form>
<script>

var req = new Req();
$(document).ready(function(){

})

function loadData()
{
	req.getInfo("/api/my/info")
}

function saveUser()
{
	if($("#PASSWORD").val()!='')
	{
		if(!checkPassword("PASSWORD","PASSWORD1"))
		{
			return false;

		}
	}
	req.save("/api/my/save", function(data){
		if(data.error)
		{
			alert(data.error);
		}
		else
		{
			alert("<spring:message code="saved"/>");
			parent.close();
		}
	})
}

</script>
</layout:put>
</layout:extends>
