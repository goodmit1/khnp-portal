<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

		<!-- DS 상태 -->
		<div class="panel panel-default chart_area "  >
			<div class="panel-heading"><spring:message code="title_group_vm_use"/></div>
			<div class="panel-body" style="position:relative">


			 		<div id="monitor_vm_cnt" class="chart" ></div>

			</div>
		</div>


			<script>
			function getVMCntMonitorInfo()
			{
				$.get('/api/monitor/list/list_GRP_VM_CNT/', function(data)
				{
					drawVMCntMonitorChart(data)
				});
			}
			function drawVMCntMonitorChart(list) {

				var data = [];
				for(var i=0; i< list.length; i++)
				{
					var obj =   {  y: ( list[i].CNT  ),  name: list[i].TEAM_NM  };
					if( list[i].TEAM_CD==_TEAM_CD_)
					{
						obj.color="#e27066";
					}
					data.push(obj )	;
				}

				var config = {
						  chart: {
							    type: 'column'
							  },
							  title: {
							    text: ''
							  },
							  xAxis: {
								    type: 'category'


								  },
							  yAxis: {
								    title: {
								      text: 'VM ' + msg_cnt
								    }

								  },
							  legend: {
							    enabled: false
							  },
							  plotOptions: {
							    series: {
							      borderWidth: 0,
							      dataLabels: {
							        enabled: true,
							        format: '{point.y}'
							      }
							    }
							  },
							series:[{name:'VM ' + msg_cnt,data:data}]
						};

				//config.yAxis.min=0;


				Highcharts.chart('monitor_vm_cnt', config);
			}
			$(document).ready(function(){
				getVMCntMonitorInfo();
				refCallback.push("getVMCntMonitorInfo");
			})

			</script>
