<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div class="panel panel-default chart_area " id="host_mv_vm_List">
	<div class="panel-heading"><spring:message code="host_mv_vm_list" text="호스트 이동 VM" /></div>
	<div class="panel-body">
		<div class="dashboard_list" style="width:100%">
			<table>
				<colgroup>
					<col width="15%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
					<col   />
				</colgroup>
			 	<thead>
				    <tr>
				      <th><spring:message code="NC_DC_DC_NM"/></th>
				      <th><spring:message code="NC_VM_VM_NM"/></th>
				      <th><spring:message code="HOST_NAME"/></th>
				      <th>Old <spring:message code="HOST_NAME"/></th>
				      <th><spring:message code="move_dttm" text="이동일시"/></th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		<tr v-for="req in host_mv_vm_List" style="width:100%">
			  			<td>{{req.DC_NM}}</td>
						<td>{{req.VM_NM}}</td>
						 <td>{{req.HOST_NM}}</td>
						 <td>{{req.OLD_HOST_NM}}</td>
						<td>{{formatDate(req.HOST_CHG_DTTM ,'datetime')}}</td>
			  
			  		</tr>
			  	</tbody>
			</table>
		</div>
	
		 
	</div>
</div>
<style>
#noticePop_button {
	display: none;
}
</style>
<script>
 
	var host_mv_vm_List = new Vue({
		el: '#host_mv_vm_List',
		data: {
			host_mv_vm_List: []
		},
		methods: {
			getReqInfo: function () {
				var that = this;
				$.get('/api/monitor/list/select_host_move_vm/', function(data){
					that.host_mv_vm_List = data;
				});
			}
		}
	});

	$(document).ready(function(){
		host_mv_vm_List.getReqInfo();
		refCallback.push("host_mv_vm_List.getReqInfo");
	});
</script>
