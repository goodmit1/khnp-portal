<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

		<!-- DS 상태 -->
<div class="panel panel-default chart_area "  >
	<div class="panel-heading">${param.TITLE}</div>
	<div class="panel-body">
		 
		<div id="container_${param.CL_ID}${param.NODE_NM}" style="height: 300px" class="chart"></div>
	</div>
	<script>
	$(document).ready(function(){
			$.ajax({
				type : 'GET',
	            url:'/api/cm/node/nodeMonitor/${param.CL_ID}/${param.IP}/${param.inputType}/${param.start}/${param.end}/${param.step}',
	            dataType:"JSON",
	            success:function(data){
	            	 
	            	if(!data){
	            		data = {};
	            	}
	            	drawChart('${param.CL_ID}${param.NODE_NM}','${param.type}', data, '${param.inputType}', '${param.color}');
	            }
	        });
	});
	</script>
</div>