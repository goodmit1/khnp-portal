<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

		<!-- DS 상태 -->
<div class="panel panel-default chart_area "  >
	<div class="panel-heading">${param.TITLE}</div>
	<div class="panel-body">
		 
		<div id="container_${param.CL_ID}${param.APP_CD}" style="height: 300px" class="chart"></div>
	</div>
	<script>
	$(document).ready(function(){
		var param1 = {};
		
		param1.APP_CD = '${param.APP_CD}';
		param1.CL_ID = '${param.CL_ID}';
		
		
		post('/api/cm/pod/list',  param1, function(data)	{
			 
			$.ajax({
				type : 'GET',
			 	url:'/api/cm/pod/podMonitor/${param.CL_ID}/${param.PRJ_ID}/'+data.list[0].POD_NM+'/${param.inputType}/${param.start}/${param.end}/${param.step}',
	            dataType:"JSON",
	            success:function(data){
	            	if(!data){
	            		data = {};
	            	}
	            	drawChart('${param.CL_ID}${param.APP_CD}',"${param.type}", data, '', "${param.color}");
	            }
	        });
		});
	})
	</script>
</div>