<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

		<!-- DS 상태 -->
<div class="panel panel-default chart_area "  >
	<div class="panel-heading">${param.TITLE}</div>
	<div class="panel-body">
		 
		<div id="container_${param.resourceId}${param.statKey.replace("|","")}" style="height: 300px" class="chart"></div>
	</div>
	<script>
	$(document).ready(function(){
		var param1 = {};
		param1.PERIOD="1h";
		param1.resourceId='${param.resourceId}';
		param1.statKeys='${param.statKey}';
		
		post('/api/monitor/vrops/data',  param1, function(data)	{
			 
			makeMultiLineChart('${param.resourceId}${param.statKey}', data, "area" ,'${param.unit}'   );
		});
	})
	</script>
</div>