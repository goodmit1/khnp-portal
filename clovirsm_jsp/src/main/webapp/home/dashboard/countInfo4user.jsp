<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- DS 상태 -->
<div class="panel panel-default chart_area " id="rsc_cnt_area" >
	<div class="panel-heading"><spring:message code="title_retention_system"/> <spring:message code="label_use_item_status"/></div>
	<div class="panel-body"  >

		<div class="sm_panel_content">

			 <div class="sm_panel " id="ds_area">
				<img src="/res/img/dashboard/d_ds.png" />
				<div class="sm_panel_title"><spring:message code="label_cost"/></div>
				<a href="/clovirsm/monitor/ds/index.jsp"><div class="panel_value"  >{{count_data.FEE|number}}</div></a>
			</div>
			<div class="sm_panel " id="vm_area">
				<img src="/res/img/dashboard/d_vm.png" />
				<div class="sm_panel_title">VM</div>
				<a href="/clovirsm/monitor/vm/index.jsp"><div class="panel_value"  >{{count_data.VM_CNT}}</div></a>
			</div>
			<c:if test="${sessionScope.VRA_SHOW }">
			<div class="sm_panel" id="host_area">
				<img src="/res/img/dashboard/d_host.png" />
				<div class="sm_panel_title">VRA</div>
				<a href="/clovirsm/monitor/host/index.jsp"><div class="panel_value"  >{{count_data.NC_VRA_CATALOGREQ }}</div></a>
			</div>
			</c:if>
			<c:if test="${sessionScope.IMG_SHOW }">
				<div class="sm_panel " id="img_area">
					<img src="/res/img/dashboard/d_img.png" />
					<div class="sm_panel_title"><spring:message code="title_img"/></div>
					<a href="/clovirsm/resources/image/index.jsp"><div class="panel_value" >{{count_data.NC_IMG }}</div></a>
				</div>
			</c:if>
		</div>
	</div>
</div>
<script>
var rsc_cnt_area = new Vue({
  el: '#rsc_cnt_area',
  data: {
	  count_data: {FEE:0, VM_CNT:0, VRA_CNT:0, IMG_CNT:0}
  },
  methods: {
	  getReqInfo: function () {
		var that = this;
		var todt = new Date();
		var param = {};
		var yyyy = todt.getFullYear();
		var mm = todt.getMonth() + 1;
		mm = mm > 9 ? mm : "0" + mm;
		$("#rsc_cnt_area > .panel-heading").text(   msg_use_item_status + "(" + yyyy +  "-" + mm + ")" );
		param.YYYYMM = yyyy + "" +  mm;
		$.post('/api/monitor/list/select_count_resource/', param, function(data)
			{
				that.count_data= data[0];

			}
		)
    }
  }
});
$(document).ready(function(){
	rsc_cnt_area.getReqInfo();
	refCallback.push("rsc_cnt_area.getReqInfo");
})
</script>