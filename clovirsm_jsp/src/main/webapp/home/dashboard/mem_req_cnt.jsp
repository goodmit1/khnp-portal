<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
			<div class="panel panel-default chart_area " id="req_cnt_area">
				<div class="panel-heading"><spring:message code="label_req_info"/></div>
				<div class="panel-body">
					<div class="sm_panel_content" style="margin-top: 45px;">
					 	<img src="/res/img/dashboard/req_cnt.png" />
						<div id="R_CNT_area" class="req_cnt1"><a href="/clovirsm/workflow/work/index.jsp"><span class="work_sm_val">{{count_data.ING}}</span>
							<span class="work_sm_title"><spring:message code="label_work"/></span></a>
						</div>
						<div id="C_CNT_area" class="req_cnt1"><a href="/clovirsm/workflow/requestHistory/index.jsp?APPR_STATUS_CD=R"><span class="work_sm_val" id="C_CNT">{{count_data.R_REQ}}</span>
							<span class="work_sm_title"><spring:message code="label_wait"/></span></a>
						</div>
						<div id="U_CNT_area" class="req_cnt1"><a href="/clovirsm/workflow/requestHistory/index.jsp?APPR_STATUS_CD=A"><span class="work_sm_val" id="U_CNT">{{count_data.A_REQ}}</span>
							<span class="work_sm_title"><spring:message code="btn_approval_accept" text=""/></span></a>
						</div>
						<div id="D_CNT_area" class="req_cnt1"><a href="/clovirsm/workflow/requestHistory/index.jsp?APPR_STATUS_CD=D"><span class="work_sm_val" id="D_CNT">{{count_data.D_REQ}}</span>
							<span class="work_sm_title"><spring:message code="btn_approval_deny" text=""/></span></a>
						</div>
					</div>
				</div>
			</div>
		<script>




		var req_cnt_area = new Vue({
		  el: '#req_cnt_area',
		  data: {
			  req_data: {ING:0, R_REQ:0, A_REQ:0, D_REQ:0}
		  },
		  methods: {
			  getReqInfo: function () {
				var that = this;
				var dateFrom = new Date();
				dateFrom.setMonth(dateFrom.getMonth()-1);
				$.get('/api/monitor/list/list_CNT/?DATE_FROM=' + formatDatePattern(dateFrom,'yyyy-MM-dd HH:mm:ss'), function(data)
					{
						that.count_data= data[0];

					}
				)
		    }
		  }
		});
		$(document).ready(function(){
			req_cnt_area.getReqInfo();
			refCallback.push("req_cnt_area.getReqInfo");
		})
		</script>

