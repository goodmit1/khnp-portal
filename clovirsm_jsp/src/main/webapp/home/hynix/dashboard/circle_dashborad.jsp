<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
 <%@page contentType="text/html; charset=UTF-8"%>
<script src="/res/js/solid-gauge.js"></script>
<div class="" id="circle_dashboard_area">
	<div class="dashboard_title"><spring:message code="title_dc_res_use" text="데이터센터 현황"/></div>
	<div class="header">
		<div class="RED"></div>
		<div class="YELLOW"></div>
		<div class="GREEN"></div>
	</div>
	<div id="container">
		<img src="/res/img/bg_data_circle.png" style="position: absolute;width: 440px;height: 440px;left: 0px;top: 10px;">
		<div id="circle">
			<ul>
			</ul>
		</div>
		<div class="center">
			<div id="LI_area">
				<img class="center_icon" src="/res/img/dashboard/icon_data_license.png">
				<div class="title">LICENSE</div>
				<div class="panel_value" onclick="circleLIClick(this);"></div>
			</div>
			<div id="HOST_area">
				<img class="center_icon" src="/res/img/dashboard/icon_data_host.png">
				<div class="title">VM/HOST</div>
				<div class="panel_value" onclick="circleHostClick(this);"></div>
			</div>
			
			<div id="DS_area">
				<img class="center_icon" src="/res/img/dashboard/icon_data_storage.png">
				<div class="title">DATA STORE(TB)</div>
				<div class="panel_value" onclick="circleDSClick(this);"></div>
			</div>
			<div id="MEM_area">
				<img class="center_icon" src="/res/img/dashboard/icon_data_mem.png">
				<div class="title">MEMORY(TB)</div>
				<div class="panel_value" onclick="circleHostClick(this);"></div>
			</div>
			<div id="CPU_area">
				<img class="center_icon" src="/res/img/dashboard/icon_data_cpu.png">
				<div class="title">CPU(vCore)</div>
				<div class="panel_value" onclick="circleHostClick(this);"></div>
			</div>
			<div id="GPU_area">
				<img class="center_icon" src="/res/img/dashboard/icon_data_gpu.png">
				<div class="title">GPU(Q)</div>
				<div class="panel_value" onclick="circleVMClick(this);"></div>
			</div>
		</div>
		<div class="tail">
		</div>
		<div class="circle_head">
			<span>
			</span>
		</div>
	</div>
	<div id="tableList" style="height: 300px; overflow-y: auto; margin-top: 30px;" class="dashboard_list">
		<table>
			<colgroup>
				<col width="30%" />
				<col width="10%" />
				<col width="10%" />
				<col width="10%" />
				<col width="10%" />
				<col width="10%" />
				<col width="10%" />
				</colgroup>
			  <thead>
			    <tr>
			      <th><spring:message code="" text="클러스터"/></th>
			      <th><spring:message code="" text="Host 갯수"/></th>
			      <th><spring:message code="" text="VM갯수"/></th>
			      <th><spring:message code="" text="MEM(TB)"/></th>
			      <th><spring:message code="" text="CPU"/></th>
			      <th><spring:message code="" text="MEM(%)"/></th>
			      <th><spring:message code="" text="CPU(%)"/></th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr  v-for="data in dashboradTable">
					<td>{{data.CLUSTER_NM}}</td>
					<td>{{data.HOST_CNT}}</td>
					<td>{{data.VM_CNT}}</td>
					<td>{{data.TOTAL_MEM}}</td>
					<td>{{data.TOTAL_CPU}}</td>
					<td>{{data.MEMP}}</td>
					<td>{{data.CPUP}}</td>
			    </tr>
			  </tfoot>
		</table>
	</div>
</div>

<script>
//<![CDATA[
var data = []
var deg = 0;
var curr = 0;
var curr_deg = 0;
var circle_r = 220;
var li_r = 9;
var level = 2;
var tableList = new Vue({
	el: '#tableList',
	data: {
		dashboradTable: [],
		rawData : []
	},
	mounted: function () {
		 
		var that = this;
		$.get('/api/monitor/list/list_DC_CIRCLE_TABLE/', function(data){
			that.rawData= data;
		});
	},
	methods: {
		getInfo: function (DC_ID) {
			this.dashboradTable = []
		 	for(var i=0; i < this.rawData.length; i++){
		 		if(!DC_ID || this.rawData[i].DC_ID==DC_ID){
		 			this.dashboradTable.push(this.rawData[i]);
		 		}
		 	}
		},
		getSummary: function(DC_ID, data){
			//HOST_CNT: 100, CPU: 450, CPUP: 37.5, MEM: 0, MEMP: 0, CLUSTER: "mgmt",VM_CNT: 40, TOTAL_CPU: 1200, TOTAL_MEM: 1.17
			var result = {TOTAL_CPU:0, CPU:0, TOTAL_MEM:0, MEM:0, HOST_CNT:0, VM_CNT:0, TOTAL_GPU:0, GPU:0 };
			var that = this;
			for(var i=0; i < this.rawData.length; i++){
		 		if(DC_ID == ''  || this.rawData[i].DC_ID==DC_ID){
		 			Object.keys(result).forEach(function(key){
		 				result[key] += that.rawData[i][key];
		 			});
		 		}
		 	}
			var totalLi = 0;
			var li = 0;
			var totalDs = 0;
			var ds = 0;
			var colorDs = null
			var colorHost = null;
			 
			for(var i=0; i < data.length; i++){
				if(DC_ID == '' || data[i].DC_ID==DC_ID){
					totalLi += nvl(data[i].TOTAL_LI,0);
					li += nvl(data[i].USED_LI,0);
					totalDs += nvl(data[i].TOTAL_DS,0);
					ds += nvl( data[i].DS,0);
					if(colorDs == null || colorDs > data[i].COLOR_DS){
						colorDs = data[i].COLOR_DS;
					}
					if(colorHost == null || colorHost > data[i].COLOR_HOST){
						colorHost = data[i].COLOR_HOST;
					}
				}
			}
			
			 
			result.TOTAL_LI = totalLi;
			result.USED_LI =  li;
			result.COLOR_LI = (totalLi- li)>0 ? 'GREEN':'RED';
			result.TOTAL_DS =  totalDs ;
			result.DS = ds;
			getCMGRate(result, "DS"); 
			result.COLOR_DS = colorDs == null ? 'GREEN' : colorDs;
			result.COLOR_HOST =  colorHost == null ? 'GREEN' : colorHost;
			
			
			result.TOTAL_HOST = result.HOST_CNT;
			result.USED_HOST = result.VM_CNT;
			
			getCMGRate(result, "CPU"); 
			getCMGRate(result, "MEM");
			getCMGRate(result, "GPU");
			return result;
		}
	}
});
function getCMGRate(result, key){
	result["USED_" + key ] = (result[key] / result["TOTAL_" + key] * 100 ).toFixed(0) + '%';
	result["COLOR_" + key] = getColorByRate(result["USED_" + key]);
	result["TOTAL_" + key] = result["TOTAL_" + key].toFixed(1)
}		 			
function getColorByRate(rate){
	if(rate >= 60 && rate < 80){
		return 'ORANGE';
	}
	else if(rate>=80){
		return 'RED';
	}
	return 'GREEN';
}		 			
function circleDSClick(that) {
	if(data[curr] != null) {
		location.href = "/clovirsm/monitor/ds/index.jsp?DC_ID=" + data[curr].DC_ID + "&OVERALLSTATUS=" + getCircleColor(that);
	}
}
function circleVMClick(that) {
	if(data[curr] != null) {
		if(getCircleColor(that) != "GREEN"){
			location.href = "/clovirsm/monitor/alarm/index.jsp?DC_ID=" + data[curr].DC_ID + "&OVERALLSTATUS=" + getCircleColor(that) + "&TARGET_TYPE=VM";
			return;
		}
		location.href = "/clovirsm/monitor/admin_vm/index.jsp?DC_ID=" + data[curr].DC_ID;
	}
}
function circleHostClick(that) {
	if(data[curr] != null) {
		location.href = "/clovirsm/monitor/host/index.jsp?DC_ID=" + data[curr].DC_ID + "&OVERALLSTATUS=" + getCircleColor(that);
	}
}
function circleLIClick(that) {
	if(data[curr] != null) {
		location.href = "/clovirsm/admin/license/index.jsp?DC_ID=" + data[curr].DC_ID;
	}
}
function getDcCircleMonitorInfo() {
	$.post('/api/monitor/list/list_DC_CIRCLE/', {}, function(data1){
		data = data1
		onAfterGetData(data);
	});
}
function getCircleColor(obj) {
	if($(obj).parent().hasClass("sm_panel_GREEN")){
		return "GREEN";
	}else if($(obj).parent().hasClass("sm_panel_YELLOW")){
		return "YELLOW";
	}else if($(obj).parent().hasClass("sm_panel_RED")){
		return "RED";
	}
}

$(function(){
	
	 
		$("#circle_dashboard_area #circle").css("width", (circle_r * 2) + "px");
		$("#circle_dashboard_area #circle").css("height", (circle_r * 2) + "px");
		getItems();
	 
		refCallback.push("getItems");
});

function itemClick(idx){
	$("#container > .tail").hide();
	$("#container > .circle_head").hide();
	var angle = getAngle(idx);
	curr = idx;
	$("#circle_dashboard_area #circle").css("transform", "rotate(" + (angle * -1.0) + "deg)");
	curr_deg = angle;
	$("#circle_dashboard_area #circle li").css("transform", "rotate(" + angle + "deg)");
	$("#circle_dashboard_area #circle li").each(function(i){
		var p_angle = deg * i;
		$(this).removeClass("over180 under180 eq180");
		var mod = (angle - p_angle) % 360;
		if(Math.abs(mod) % 180 == 0){
			if(Math.abs(mod) == 180) {
				$(this).addClass("eq180");
			}
		}else if(Math.abs(mod) > 180) {
			$(this).addClass("over180");
		}else if (Math.abs(mod) < 180) {
			$(this).addClass("under180");
		}
	})
	$("#circle_dashboard_area #circle li").removeClass("selected");
	$("#circle_dashboard_area #circle li:nth-child(" + ++idx + ")").addClass("selected");
	if(data[curr] != null) {
		setTimeout(function(){
			$("#container > .circle_head > span").html(data[curr].DC_NM)
			$("#container > .tail").removeClass("RED GREEN YELLOW");
			$("#container > .circle_head").removeClass("RED GREEN YELLOW");
			$("#container > .tail").show();
			$("#container > .circle_head").show();
			$("#container > .tail").addClass(data[curr].CATEGORY);
			$("#container > .circle_head").addClass(data[curr].CATEGORY);
		}, 500);
		tableList.getInfo(data[curr].DC_ID);
		var result = tableList.getSummary(data[curr].DC_ID, data);
		try{
			drawInner(result, "HOST");
			drawInner(result, "LI");
			drawInner(result, "DS");
			drawInner(result, "CPU");
			drawInner(result, "MEM");
			drawInner(result, "GPU");
			 
			} catch(e){
			} finally{
				if(data[curr].DC_ID != '') {
					$("select[id^=DC_ID]").val(data[curr].DC_ID);
					$("select[id^=DC_ID]").trigger("change");
				}
			}
		 
	}
}
function drawInner(result, KUBUN){
	var that = $(".center > #" + KUBUN + "_area");
	that.removeClass();
	if(result["TOTAL_" + KUBUN] == 0) {
		that.hide();
		return;
	}
	that.show();
	
	that.addClass("sm_panel_" + result["COLOR_" + KUBUN]);
	that.find(".panel_value").html(result["USED_" + KUBUN] + "/" + result["TOTAL_" + KUBUN]);
	 
}
function getItems(kubun){
	$("#circle_dashboard_area #circle ul").html("");
	$("#circle_dashboard_area #circle").removeClass("delay");
	$("#circle_dashboard_area #circle").css("transform", "rotate(18000deg)");
	getDcCircleMonitorInfo();
}

function onAfterGetData(data){
	$("#circle_dashboard_area #circle").addClass("delay");
	curr_deg = -18000;
	var cate = "GREEN";
	var red_cnt = 0;
	var yellow_cnt = 0;
	var green_cnt = 0;
	for(var i in data) {
		if(data[i].CATEGORY == 'RED') {
			cate = 'RED'
			break;
		}else if(data[i].CATEGORY == 'YELLOW') {
			cate = 'YELLOW';
		}
	}
	if(cate == 'RED') red_cnt--;
	else if(cate == 'YELLOW') yellow_cnt--;
	else green_cnt--;


	data.unshift({CATEGORY: cate, DC_ID: '', DC_NM: 'ALL'});

	makeCircle(data);

	for(var i in data){
		var row = data[i];
		switch (row.CATEGORY) {
			case "RED":
				red_cnt++;
			break;
			case "YELLOW":
				yellow_cnt++;
			break;
			case "GREEN":
				green_cnt++;
			break;
		}
	}
	$("#circle_dashboard_area .header > .RED").html(red_cnt);
	$("#circle_dashboard_area .header > .YELLOW").html(yellow_cnt);
	$("#circle_dashboard_area .header > .GREEN").html(green_cnt);
	itemClick(0);
}

function getAngle(idx){
	var plus_angle = deg * idx;
	var minus_angle = plus_angle - 360;
	var mod = curr_deg % 360;
	if(Math.abs(plus_angle - mod) > Math.abs(minus_angle - mod)){
		return curr_deg + minus_angle - mod;
	}else{
		return curr_deg + plus_angle - mod;
	}
}

function makeCircle(data){
	deg = 360 / data.length;
	var parent = $("#circle_dashboard_area #circle ul");
	parent.html("");
	for(var i=0; i < data.length; i++){
		var row = data[i];
		var pos = getPostion(i);
		var html = '<li id="circle' + i + '" class="delay circle_' + row.CATEGORY + '" onclick="itemClick(' + i + ')" style="top: ' + pos.y + 'px; left: ' + pos.x + 'px;">';
		html += '<div>';
		html += row.DC_NM;
		html += '</div>';
		html += '</li>';
		parent.append(html);
	}
	$("#circle_dashboard_area #circle li").css("width", (li_r * 2) + "px");
	$("#circle_dashboard_area #circle li").css("height", (li_r * 2) + "px");
}

function getPostion(idx){
	var result = {};
	var angle = (180 - deg * idx) * Math.PI / 180;
	result.x = circle_r - li_r + Math.sin(angle) * circle_r;//x
	result.y = circle_r - li_r + Math.cos(angle) * circle_r;//y
	return result;
}
//]]>
</script>
