<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>

	<script type="text/javascript" src="/res/js/heatmap.js"></script>
 
		<!-- Host 상태 -->
		<div class="panel panel-default chart_area "  >
			<div class="panel-heading"><a href="/clovirsm/monitor/svchostcnt/index.jsp"  >업무 중복 배포 목록</a></div>
			<div class="panel-body" style="position:relative">
				<div class="dashboard_list" id="svc_host_list" style=" height: 300px; overflow-y: auto;">
					<table>
					<colgroup>
						<col width="20%" />
						<col width="25%" />
						<col width="45%"  />
						<col  />
						 
						</colgroup>
					  <thead>
					    <tr>
					      <th><spring:message code="NC_DC_DC_NM"/></th>
					      <th><spring:message code="HOST_NAME"/></th>
					      <th><spring:message code="TASK"/></th>
					      <th><spring:message code="count"/></th>
					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr v-if="svchostList.length>0"  v-for="svchost in svchostList">
					      <td><a :href="'/clovirsm/monitor/svchostcnt/index.jsp?' + svchost.DC_ID">{{svchost.DC_NM}}</a></td>
					      <td>{{svchost.HOST_NM}}</td>
					      <td>{{svchost.TASK}}</td>
					      <td>{{svchost.CNT}}</td>
					     
		
					    </tr>
					    <tr v-if="svchostList.length==0">
					    <td colspan="4"><spring:message code="search_result_empty"></spring:message></td>
					    </tr>
					  </tfoot>
					</table>
				</div>
			</div>
		</div>


		<script>
		
		 
		var svchostList = new Vue({
			el: '#svc_host_list',
			data: {
				svchostList: []
			},
			methods: {
				getReqInfo: function ( ) {
					var that = this;
					$.get('/api/monitor/list/list_vm_host_count_by_tag/?' + getTagUrlParam() + 'CNT=1' , function(data)
					{
						for(var i=0; i < data.length; i++){
							data[i].TASK=getTagTaskNm(data[i]);
							that.svchostList.push(data[i])
						}
					});
				}
			}
		});
		
			 
			var tags= ["svc","purpose","p_kubun"];
			function getTagTaskNm( obj ){
				var result = '';
				var resultNm = '';
				for(var i=0; i < tags.length; i++) {
					result += obj[tags[i].toUpperCase()]
					resultNm += obj[tags[i].toUpperCase() + "_NM"] + " "
				}
				return resultNm + '(' + result + ')';
			}
			function getTagUrlParam( ){
				var result = '';
				for(var i=0; i < tags.length; i++) {
					 
					result +='TAGS=' + tags[i] + "&"
				}
				 
				return result;
			}
			 
			$(document).ready(function(){
				svchostList.getReqInfo();
			})
			 

			</script>
