<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
			<div class="dashboardTopItem white" style=" width: 100%">
					<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<div class="reqDisplay">
	<div class="reqImg">
		<div class="vmCircleArea"><div class="reqCircle"></div></div>
		<a href="/clovirsm/workflow/approval/index.jsp"><div class="reqImgText"><spring:message code="req_state" text="요청 현황"/></div></a>
	</div>
	<div class="reqText">
		<div class="req_info">
			<span><spring:message code="new_service" text="신규 신청"/> : </span><span>{{req_data.CC}}</span>
		</div>
		<div class="req_info">
			<span><spring:message code="NEW_VM_TITLE_3" text="자원 변경"/> : </span><span>{{req_data.SU}}</span>
		</div>
		<div class="req_info">
			<span><spring:message code="NEW_VM_TITLE_4" text="삭제 요청"/> : </span><span>{{req_data.SD}}</span>
		</div>
		<div class="req_info">
			<span><spring:message code="NEW_VM_TITLE_5" text="기간 변경"/> : </span><span>{{req_data.EU}}</span>
		</div>
		<div class="req_info">
			<span><spring:message code="title_server_reuse" text="서버복원"/> : </span><span>{{req_data.VU}}</span>
		</div>
		<div class="req_info">
			<span><spring:message code="TITLE_ADD_TEMPLATE" text="카탈로그 추가"/> : </span><span>{{req_data.ZU}}</span>
		</div>
		 
	</div>
</div>
<script>
var req_cnt = new Vue({
  el: '.reqDisplay',
  data: {
	  req_data: {CC:0, SD:0, SU:0, EU:0, VU:0, ZU:0}
  },
  methods: {
	  getReqInfo: function () {
		var that = this;
		$.get('/api/monitor/list/list_req_cnt/?dd=7', function(data)
			{
				for(var i = 0; i < data.length; i++){
					var cd = data[i].CD;
					that.req_data[cd] = data[i].NUM;
				}
			}
		)
    },
  	  date_change: function(date){
	  	  var year = date.getFullYear();
	  	  var month = date.getMonth()+1;
	  	  month = (month < 10) ? '0' + month : month;
	      var day = date.getDate();
	      day = (day < 10) ? '0' + day : day;
	      return year+""+month;
  	  }
  }
});

$(document).ready(function(){
	req_cnt.getReqInfo();
	refCallback.push("req_cnt.getReqInfo");
})

</script>
				</div>