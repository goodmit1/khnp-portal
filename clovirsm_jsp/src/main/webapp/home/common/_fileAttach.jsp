<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
    <style>

    </style>
    <div id="${param.kubun}fileAttach" class="fileAttachViewArea">

    	 <ol>
		    <li class="choice" v-for="file in form_data.${param.kubun}_file" v-bind:data-seq="file.SEQ" v-bind:data-id="file.ID">
		      <c:if test="${param.editable eq 'Y' }">
		      	<span class="choice_remove" onclick="removeAttach('${param.kubun}',this)"  role="presentation">×</span>
		      </c:if>
		      <a href="#" onclick="downloadAttach('${param.kubun}',this)" :title="file.FILE_NM"><i class="fa fa-download"></i></a>

		    </li>
		  </ol>
    </div>

<script>
<c:if test="${param.editable eq 'Y' }">
function removeAttach(kubun, obj) {
	if(confirm(msg_confirm_delete)) {
		var id = $(obj).parent().attr("data-id");
		var seq = $(obj).parent().attr("data-seq");
		$.get('/api/' + kubun + '/delete_fileAttach/' + id + "/" + seq, function(data){
			eval(kubun + "fileAttach_list (  id)");
		})
	}
}
</c:if>
function downloadAttach(kubun, obj) {
	var id = $(obj).parent().attr("data-id");
	var seq = $(obj).parent().attr("data-seq");
	location.href='/api/' + kubun + '/get_fileAttach/' + id + "/" + seq;
}

function ${param.kubun}fileAttach_list (id) {
	$.get('/api/${param.kubun}/list_fileAttach/' + id, function(data){
		inputvue.$set( form_data, '${param.kubun}_file', data);
	})
}



</script>