<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
    
		<link rel="stylesheet" href="/res/css/jquery-ui.css">
		<link rel="stylesheet" href="/res/css/bootstrap.min.css">
		<link rel="stylesheet" href="/res/css/font-awesome.min.css">
		<link rel="stylesheet" href="/res/css/common.css">
		<link rel="stylesheet" href="/res/css/menu.css">
		<link rel="stylesheet" href="/res/css/jstree.style.min.css" />
			
		<script src="/res/js/jquery.min.js"></script>
		<script src="/res/js/jquery-ui.min.js"></script>
		
		<script src="/res/js/jstree.min.js"></script>
		<script src="/res/js/highcharts.js"></script>
		<script src="/res/js/highcharts-more.js"></script>
		<script src="/res/js/wordcloud.js"></script>
		<script src="/res/js/exporting.js"></script>
		<script src="/res/js/vue.js"></script>
	 	<script src="/res/js/ag-grid.js"></script>
	 	
	 	<script src="/res/js/aggridconfig.js" ></script>
		<script	src="/res/js/bootstrap.min.js"></script>
		<script src="/res/js/jstreeconfig.js" ></script>
		<script src="/res/js/hichartconfig.js" ></script>
		<script src="/res/js/vue_comp.js" ></script>
		<script src="/res/js/common.js" ></script>
		<script src="/res/js/popup.js" ></script>
		<script src="/res/js/site.js" ></script>
		<div id="container"></div>
		<script>
		$(document).ready(function(){
var chart=Highcharts.chart('container', {
	chart:{
		type:"bar"
	},
    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        alert('Category: ' + this.category + ', value: ' + this.y);
                    }
                }
            }
        }
    },

    series: [{
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
    }]
});
setTimeout(function(){
 chart.series[0].data[0].firePointEvent("click")
},1000)
		});
</script>