<%@page import="java.util.List"%>
<%@page import="com.fliconz.fm.mvc.security.MenuUtil"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.fliconz.fm.common.CodeHandler"%>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<%@page import="java.util.Map"%>
<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page import="com.clovirsm.service.monitor.MonitorService"%>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page import="com.fliconz.fm.security.FMSecurityContextHelper"%>
<%@page import="java.util.ResourceBundle" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="org.json.JSONObject"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"  %>
<%
 
JSONObject access = new JSONObject();
if( UserVO.getUser() == null){
	response.sendRedirect("/");
	return ;
}


 
List menuPathIds = MenuUtil.getSelectedPathMenuId();
int menuId = 0;
if(menuPathIds != null && menuPathIds.size()>0){
	menuId = (int)menuPathIds.get(menuPathIds.size()-1);
}
access.put("menuPathIds", menuPathIds);
access.put("update", !FMSecurityContextHelper.checkMethodPermission("update"));
access.put("delete",  !FMSecurityContextHelper.checkMethodPermission("delete"));
access.put("etc",  !FMSecurityContextHelper.checkMethodPermission("print"));
access.put("search",  true);
 
boolean mymenu = PropertyManager.getBoolean("clovirsm.display.mymenu", false);

session.setAttribute("FW_SHOW", PropertyManager.getBoolean("clovirsm.display.fw", false));

session.setAttribute("IMG_SHOW", PropertyManager.getBoolean("clovirsm.display.img", false));
session.setAttribute("MYMENU", mymenu);
session.setAttribute("SNAPSHOT", PropertyManager.getBoolean("clovirsm.display.snapshot", false));
session.setAttribute("STATE", PropertyManager.getBoolean("clovirsm.display.state", false)); // P_KUBUN 보일 지 여부
session.setAttribute("WORK_SHOW", PropertyManager.getBoolean("clovirsm.display.work", true)); // 작업함 사용 여부
session.setAttribute("HTML_TITLE", PropertyManager.getString("clovirsm.html.tilte"));

try
{
	if(mymenu) {
		
		Map myMenuObj = (Map)UserVO.getUser().getUserMenuManager().getLeftMyMenuModel();
		if(myMenuObj != null && myMenuObj.get(menuId) != null) {
			access.put("mymenu", true);
		}
	}

}
catch(Exception ignore)
{

}
//if(session.getAttribute("ADMIN_YN") == null)
{
	NextApproverService nextApproverService = (NextApproverService)SpringBeanUtil.getBean("nextApproverService");

	session.setAttribute("ADMIN_YN", nextApproverService.isAdmin()?"Y":"N");
	session.setAttribute("APPROVER_YN", nextApproverService.isApprover()?"Y":"N");
	session.setAttribute("GRP_ADMIN_YN", nextApproverService.isGrpMember()?"N":"Y");
	session.setAttribute("_TEAM_CD_", UserVO.getUser().getTeam().getTeamCd());
	session.setAttribute("_TEAM_NM_", UserVO.getUser().getTeam().getTeamNm());
	session.setAttribute("_COMP_ID_", UserVO.getUser().getTeam().getCompId());
	session.setAttribute("_USER_ID_", UserVO.getUser().getUserId());
	session.setAttribute("_USER_NM_", UserVO.getUser().getUserName());
	session.setAttribute("_USER_IP_", UserVO.getUser().getLastLoginIp());
	session.setAttribute("_USER_TYPE_", UserVO.getUser().getUserType());
	session.setAttribute("_LANG_", UserVO.getUser().getLangKnd());
	if(request.getParameter("POPUP_YN") == null) 
	{	
		System.out.println("=========================================");
		System.out.println(access.toString());
		System.out.println("=========================================");
		session.setAttribute("accessInfo", access);
	}
}
session.setAttribute("IS_AUTO_VM_NAME", PropertyManager.getBoolean("clovirsm.vm.name.auto", false));
session.setAttribute("IS_AUTO_IMAGE_NAME", PropertyManager.getBoolean("clovirsm.image.name.auto", false));

CodeHandler codeHandler = (CodeHandler)SpringBeanUtil.getBean("codeHandler");
Map etcP = codeHandler.getCodeList("HV_ETC_PRODUCT_V", "ko");
session.setAttribute("FW_API_YN", (etcP !=null && etcP.get("FW") != null)?"Y":	"N");
session.setAttribute("VM_USER_YN", (etcP !=null && etcP.get("LINUX_GUEST") != null)? "Y":"N");
session.setAttribute("VRA_SHOW", (etcP !=null && etcP.get("VRA") != null)? "Y":"N");
session.setAttribute("LOG_SHOW", (etcP !=null && etcP.get("VRLI") != null)? "Y":"N");
session.setAttribute("SITE_CODE", PropertyManager.getString("site.code"));
%>
<!DOCTYPE html>
<html lang="en" id="html">
    <head>
		<title>${sessionScope.HTML_TITLE}</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=0.4">
		<link rel="stylesheet" href="/res/css/jquery-ui.css">
		<link rel="stylesheet" href="/res/css/bootstrap.min.css">
		<link rel="stylesheet" href="/res/css/font-awesome.min.css">
		<link rel="stylesheet" href="/res/css/jstree.style.min.css" />
		<link rel="stylesheet" href="/res/css/common.css">
		<link rel="stylesheet" href="/res/css/menu.css">
		<link href="/res/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
		<script>
		 
		var method;
		var noop = function noop() {};
		var methods = [
		'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
		'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
		'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
		'timeStamp', 'trace', 'warn'
		];
		var length = methods.length;
		var console = (window.console = window.console || {});
		var __query_param__ = {};
		<c:forEach var="param1" items="${param}" >
		 __query_param__.<c:out value="${param1.key}" escapeXml="true"/>="<c:out value="${param1.value}" escapeXml="true"/>"
		</c:forEach> 	
		while (length--) {
		method = methods[length];

		// Only stub undefined methods.
		if (!console[method]) {
		console[method] = noop;
		}}
		
			var diskUnitOptions = {
				G: 'GB',
				T: 'TB'
			}
			var STATE = ${STATE};
			var HTML_TITLE ="${sessionScope.HTML_TITLE}"
			var ADMIN_YN = "${sessionScope.ADMIN_YN }";
			var GRP_ADMIN_YN = "${sessionScope.GRP_ADMIN_YN }";
			var APPROVER_YN = "${sessionScope.APPROVER_YN }";
			var _TEAM_CD_ = "${sessionScope._TEAM_CD_  }";
			var _TEAM_NM_ = "${sessionScope._TEAM_NM_  }";
			var _COMP_ID_ = "${sessionScope._COMP_ID_  }";
			var _USER_ID_ = "${sessionScope._USER_ID_}";
			var _USER_NM_ = "${sessionScope._USER_NM_}";
			var _USER_IP_ = "${sessionScope._USER_IP_}";
			var _USER_TYPE_ = "${sessionScope._USER_TYPE_}";
			var NAS_SHOW = "${sessionScope.NAS_SHOW}";
			var VRA_SHOW = "${sessionScope.VRA_SHOW}";
			var WORK_SHOW = "${sessionScope.WORK_SHOW}";
			var IS_AUTO_VM_NAME = ${IS_AUTO_VM_NAME};
			var IS_AUTO_IMAGE_NAME = ${IS_AUTO_IMAGE_NAME};
			var SNAPSHOT = ${SNAPSHOT};
			var accessInfo = ${sessionScope.accessInfo};
			var RSAModulus = "${RSAModulus}";
			var RSAExponent= "${RSAExponent}";
			var msg_admin_chk = "<spring:message code="msg_admin_chk" text="" />";
			var msg_max_err = "<spring:message code="msg_max_err" text="" />";
			var msg_min_err = "<spring:message code="msg_min_err" text="" />";
			var msg_select_first= "<spring:message code="select_first" text="" />";
			var msg_confirm_save= "<spring:message code="msg_confirm_save" text="" />";
			var msg_delete_request= "<spring:message code="delete_request" text="" />";
			var msg_confirm_delete_request= "<spring:message code="confirm_delete_request" text="" />";
			var msg_confirm_delete= "<spring:message code="confirm_delete" text="" />";
			var msg_please_select_for_delete= "<spring:message code="msg_please_select_for_delete" text="" />";
			var msg_complete= "<spring:message code="msg_complete" text="" />";
			var msg_complete_work_continue= "<spring:message code="msg_complete_work_continue" text="" />";
			var msg_complete_work= "<spring:message code="msg_complete_work" text="" />";
			var msg_complete_work_move = "<spring:message code="msg_complete_work_move" text="" />";
			var msg_complete_work_move_save = "<spring:message code="msg_complete_work_move_save" text="" />";
			var msg_cnt = "<spring:message code="cnt" text="" />";
			var msg_yes = "<spring:message code="label_yes" text="" />";
			var msg_no = "<spring:message code="label_no" text="" />";
			var msg_price_unit = "<spring:message code="label_price_unit" text=""/>";
			var msg_TEAM_VM_CNT = "<spring:message code="TEAM_VM_CNT" text="" />";
			var msg_use_item_status = "<spring:message code="label_use_item_status" text="" />";
			var msg_password_chk_fail = "<spring:message code="msg_password_chk_fail" text="" />";
			var msg_need_login = "<spring:message code="msg_need_login" text="" />";
			var msg_empty_value = "<spring:message code="msg_empty_value" text="" />";
			var msg_ip_reg = "<spring:message code="msg_ip_reg" text="" />";
			var msg_jsf_notifications = "<spring:message code="msg_jsf_notifications" text="Alert" />";
			var msg_jsp_error = "<spring:message code="msg_jsp_error" text="" />";
			var msg_password_role_info = "<spring:message code="msg_password_role_info" text="" />";
			var msg_start_dt_over_end_dt = "<spring:message code="msg_start_dt_over_end_dt" text="" />";
			var msg_search_keyword = "<spring:message code="search_keyword" text="" />";
			var msg_not_number = "<spring:message code="NOT_NUMBER" text=""/>";
			var btn_close = "<spring:message code="btn_close" text="" />";
			var msg_confirm = "<spring:message code="msg_confirm" text="OK" />";
			var label_cancel =  "<spring:message code="label_cancel" text="Cancel" />";
			var ERR_ENG_NUM_H_MESSAGE = "<spring:message code="ERR_ENG_NUM_H_MESSAGE" text="" />";
			var label_expire = '<spring:message code="dt_expiredate" text="만료일"/>'
			var dateLabel = {};
			dateLabel.label_term = "<spring:message code="label_term" text="" />";
			dateLabel.year = "<spring:message code="label_year" text="" />";
			dateLabel.month = "<spring:message code="label_month" text="" />";
			dateLabel.week = "<spring:message code="label_week" text="" />";
			dateLabel.date = "<spring:message code="label_date" text="" />";
			dateLabel.hour = "<spring:message code="hour" text="" />";
			dateLabel.minute = "<spring:message code="minute" text="" />";
			dateLabel.second = "<spring:message code="second" text="" />";

			dateLabel.day = "<spring:message code="label_day" text="" />";
			dateLabel.today = "<spring:message code="dt_today" text="" />";
			dateLabel.pre_month = "<spring:message code="label_pre_month" text="" />";
			dateLabel.next_month = "<spring:message code="label_next_month" text="" />";

			dateLabel.sunday_min = "<spring:message code="label_sunday" text="" />";
			dateLabel.monday_min = "<spring:message code="label_monday" text="" />";
			dateLabel.tuesday_min = "<spring:message code="label_tuesday" text="" />";
			dateLabel.wednesday_min = "<spring:message code="label_wednesday" text="" />";
			dateLabel.thursday_min = "<spring:message code="label_thursday" text="" />";
			dateLabel.friday_min = "<spring:message code="label_friday" text="" />";
			dateLabel.saturday_min = "<spring:message code="label_saturday" text="" />";

			dateLabel.sunday    = dateLabel.sunday_min    + dateLabel.day;
			dateLabel.monday    = dateLabel.monday_min    + dateLabel.day;
			dateLabel.tuesday   = dateLabel.tuesday_min   + dateLabel.day;
			dateLabel.wednesday = dateLabel.wednesday_min + dateLabel.day;
			dateLabel.thursday  = dateLabel.thursday_min  + dateLabel.day;
			dateLabel.friday    = dateLabel.friday_min    + dateLabel.day;
			dateLabel.saturday  = dateLabel.saturday_min  + dateLabel.day;

		</script>
		<script src="/res/js/jquery.min.js"></script>
		<script src="/res/js/jquery-ui.min.js"></script>
		 
		<script src="/res/js/jstree.min.js"></script>

		<script src="/res/js/highcharts.js"></script>
		<script src="/res/js/data.js"></script>
		<script src="/res/js/highcharts-more.js"></script>
		<script src="/res/js/no-data-to-display.js"></script>
		<script src="/res/js/solid-gauge.js"></script>
		<script src="/res/js/wordcloud.js"></script>
		<script src="/res/js/exporting.js"></script>
		<script src="/res/js/export-data.js"></script>
		<script src="/res/js/offline-exporting.js"></script>

		<script src="/res/js/hichartconfig.js" ></script>
		<script type="text/javascript" src="/res/js/rsa.js"></script>
		<script type="text/javascript" src="/res/js/jsbn.js"></script>
		<script type="text/javascript" src="/res/js/prng4.js"></script>
		<script type="text/javascript" src="/res/js/rng.js"></script>
 		<link rel="stylesheet" type="text/css" href="/res/css/select2.min.css">
        <script type="text/javascript" src="/res/js/select2.js"></script>


		<script src="/res/js/vue.js"></script>
	 	<script src="/res/js/ag-grid.min.js"></script>
	 	<script src="/res/js/aggridconfig.js"></script>
		<script	src="/res/js/bootstrap.min.js"></script>
		<script src="/res/js/jstreeconfig.js" ></script>


		<script src='/res/js/moment-with-locales.min.js'></script>
		<script src='/res/js/bootstrap-datetimepicker.min.js'></script>


		<script src="/res/js/vue_comp.js" ></script>
		<script src="/res/js/common.js?v=4" ></script>
		<script src="/res/js/clovir_common.js" ></script>
		<script src="/res/js/popup.js?v=2" ></script>
		<script src="/res/js/site.js?v=2" ></script>
	
		
	 	<script src="/res/js/${sessionScope.SITE_CODE}/site.js"></script>
		<link rel="stylesheet" href="/res/css/${sessionScope.SITE_CODE}/common.css?v=2">
		<link rel="stylesheet" href="/res/css/${sessionScope.SITE_CODE}/menu.css?v=2">
		<link rel="stylesheet" href="/res/css/${sessionScope.SITE_CODE}/dashboard.css">
		<link rel="stylesheet" href="/res/css/${sessionScope.SITE_CODE}/aggrid_${sessionScope.SITE_CODE}.css">
		<jsp:include page="/res/comp/vue_comp.jsp"></jsp:include>
		<iframe name="downWin" id="downWin" style="display:none"></iframe>
		<div id="logFullDisplay" style="display:none;">
			<div id="NofullScrennBtn" class="NofullScrennBtn" onclick="fullScreenClick('T', this);"></div>
			<iframe  style="width:100%;height:100%;"></iframe>
		</div>
</head>