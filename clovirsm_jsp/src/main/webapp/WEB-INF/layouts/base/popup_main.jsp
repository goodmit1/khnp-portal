<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<jsp:include page="_common.jsp?POPUP_YN=Y" />
	
<body class="popup-body">
<layout:block name="content"></layout:block>
</body>
</html>