<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.fliconz.fm.mvc.security.MenuUtil"%>
<%@page import="com.fliconz.fm.usermenu.UserMenuManager"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.fliconz.fm.common.vo.TreeNodeMap"%>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
Map menu =  MenuUtil.getMenuTitleNavi();
if(menu != null && !menu.isEmpty()){
	 
	request.setAttribute("MENU", MenuUtil.getMenuTitleNavi());
}
else {
	String title = (String)request.getAttribute("MENU_TITLE");
 
	menu = new HashMap();
	menu.put("title", title);
	request.setAttribute("MENU", menu);
}
%>
<script>
	<c:if test="${sessionScope.MYMENU }">
	var mymenu = accessInfo.mymenu;
	var mymenuId = accessInfo.menuId;

	$(document).ready(function(){
		if(mymenu == true){
			$('#favoriteButton').removeClass("add");
			$('#favoriteButton').addClass("remove");
		}
	});

	function favoriteButton(that){

		if(mymenu == true){
			post('/api/my/deleteMyMenu?MENU_ID='+ mymenuId,null, function(data)
					{
						mymenu = false;
						location.reload();
					}
				)
		}
		else{
			post('/api/my/saveMyMenu?MENU_ID='+ mymenuId,null, function(data)
					{
						mymenu = true;
						location.reload();
					}
				)
		}
	}
	</c:if>
</script>
<div id="ribbon" class="navi">
	<h4>${MENU.title} <h6 class="menu_desc">${MENU.desc}</h6></h4>
	<c:if test="${sessionScope.MYMENU }">
	<button class="favoriteButton add" id="favoriteButton" onclick="favoriteButton(this);"><spring:message code="favorite" text="즐겨찾기"/></button>
	</c:if>
	<ol class="breadcrumb">

		<c:forEach var="navi" items="${MENU.navi}">
			<li>${ navi }</li>
		</c:forEach>
	</ol>
</div>