<%@page import="com.fliconz.fm.common.util.TextHelper"%>
<%@page import="com.fliconz.fm.mvc.security.MenuUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.fliconz.fm.common.vo.TreeNodeMap"%>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>
<div id="left_menu" >
	<div class="title"></div>
	<div class="left_hide" onclick="left_hide();"></div>
	<div id="left_menu2"></div>
</div>
               
<script>
$(document).ready(function(){
	var menu = getMenuById(menuJSON.child, accessInfo.menuPathIds[1]);
	if(menu){
		$("#left_menu").show();
		$("#left_menu .title").text(menu.title);
		if(menu.hasOwnProperty("child")){
			for(var i=0; i < menu.child.length; i++){
				var html = displayMenu(menu.child[i])
				var child3 = menu.child[i].child;
				for(var j=0; child3 && j < child3.length; j++){
					if(j==0){
						if(accessInfo.menuPathIds.indexOf(menu.child[i].id)>=0)
			                  html += "<div id='left_menu3' class='showMenu'>";
			               else
			                  html += "<div id='left_menu3'>";
					}
					html += displayMenu(child3[j])
					
					if(j== child3.length-1){
						html += "</div>";
					}
				}
				$("#left_menu2").append(html);
			}
		}    
	} 
});
	function left_show(){
		$(".left_show").hide();
		$(".main").css("width","calc(100% - 257px)");
		$("#left_menu").show("slide", { direction: "left" }, 500, function(){
		});
}
	function left_hide(){
		$("#left_menu").hide("slide", { direction: "left" }, 500, function(){
			$(".left_show").show();
			$(".main").css("width","100%");
		});
}
</script>
