<%@page import="com.fliconz.fm.common.util.TextHelper"%>
<%@page import="com.fliconz.fm.mvc.security.MenuUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.fliconz.fm.common.vo.TreeNodeMap"%>
<%@page import="com.fliconz.fm.security.UserVO"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>

<div class="bottom">
	<div class="bottomLogo">
		<img src="/res/img/hynix/gray.png">
	</div>
	<div class="bottomText">
	BABUL-EUP, ICHEON-SI, GYEONGGI-DO, KOREA
	<br/>
	COPYRIGHTⓒ SK HYNIX, ALL RIGHT RESERVED.
	</div>
</div>