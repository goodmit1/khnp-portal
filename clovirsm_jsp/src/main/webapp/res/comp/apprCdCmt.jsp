<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<fm-select   id="${param.prefix}APPR_CMT_CD" 
onchange="${param.prefix}apprSelectChange()" emptystr=" " name="APPR_CMT_CD" style=" width: 40%; margin-bottom: 10px;"></fm-select>
				<textarea name="APPR_CMT" id="${param.prefix}APPR_CMT" 
				style="width: 100%; border: 1px solid #ddd; display: block; height: 90px;" class="form-control input hastitle"/>
				<fm-popup-button style="display:none;" popupid="${param.prefix}newItem"
										popup="/clovirsm/popup/newItem.jsp"
										callback="${param.prefix}putApprData"
										cmd="update" param="${param.prefix}apprData">
				</fm-popup-button>						
<script>
$(document).ready(function(){
	${param.prefix}putApprData();
});
var ${param.prefix}apprData
 
function ${param.prefix}apprSelectChange( ){
	if($("#${param.prefix}APPR_CMT_CD option:selected").val() == "newItem"){
		 
		$("#${param.prefix}newItem_button").click();
	}else{
		$("#${param.prefix}APPR_CMT").css("display","inline-block");
		$("#${param.prefix}APPR_CMT").val($("#${param.prefix}APPR_CMT_CD option:selected").html()+" ");
	}
}

function ${param.prefix}putApprData(){
	post("/api/approval/appr_comment_list?lang=ko&DD_ID=APPR_COMMENT&USE_YN=Y",null,function(data){

		var apprArray = new Array();
		var apprObject = new Object();
		for(var i = 0; i < data.list.length ; i++){
			var val = data.list[i].DD_VALUE
			apprObject[val] = data.list[i].DD_DESC;
			
		}
		apprObject["newItem"] = "==<spring:message code="NEW_ITEM_ADD" text="새 결재의견 추가" />==";
		${param.prefix}apprData = apprObject;
		fillOptionByData("${param.prefix}APPR_CMT_CD", apprObject, ' ', null, null);
	});
}
</script>										