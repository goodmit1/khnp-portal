function checkPasswordPattern(str) {
	var pattern1 = /[0-9]/;	// 숫자
	var pattern2 = /[a-zA-Z]/;	// 문자
	var pattern3 = /[~!@#$%^&*()_+|<>?:{}]/;	// 특수문자
	if(!pattern1.test(str) || !pattern2.test(str) || !pattern3.test(str) || str.length < 8)
	{
		alert(msg_password_role_info);
		return false;
	} else {
		return true;
	}
}
function checkPassword(pwd,pwd1) {
	if(checkPasswordPattern($("#" + pwd).val()))
	{
		if($("#" + pwd).val() != $("#" + pwd1).val())
		{
			alert(msg_password_chk_fail);
			$("#" + pwd).focus();
			return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}