<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body">

		<div class="col col-sm-3">
			<fm-ibutton id="CATEGORY_NM" name="CATEGORY_NM" title="<spring:message code="NC_OS_TYPE_SERVICE"/>" class="inline" btn_class="cate_btn_height1">
				<fm-popup-button popupid="s_category" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="selectCategory"></fm-popup-button>
			</fm-ibutton>
<%--			<input type="text" style="display:none" id="S_CATEGORY" name="CATEGORY">--%>
		</div>
		<input type="text" style="display:none" id="DC_ID" name="DC_ID" :value="form_data.DC_ID">
		<div class="col col-sm-9">
			<fm-input id="IP"
					  tooltip="<spring:message code="team_ip_guide" text="CIDR형식으로 입력하며  ,로 구분하여 여러 개를 입력한다. 예:10.3.10.0/24,10.230.112.0/24"/>"
					  placeholder="<spring:message code="team_ip_guide" text="CIDR형식으로 입력하며  ,로 구분하여 여러 개를 입력한다."/>"
					  name="IP" title="<spring:message code="NC_TEAM_CONF_IP" text="IP" />"  >
			</fm-input>
		</div>
		<div class="col col-sm-6">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.getCluster" id="CLUSTER"
				keyfield="OBJ_NM" titlefield="OBJ_NM"
				name="CLUSTER" title="<spring:message code="NC_TEAM_CONF_CLUSTER" text="클러스터"/>">
			</fm-select>
		</div>
		<div class="col col-sm-6">
			<fm-input id="CATEGORY" name="CATEGORY" title="<spring:message code="NC_TEAM_CONF_ETC_CD" text="Code" />"  ></fm-input>
		</div>
<%--		<pre><spring:message code="ip_setting_guide" text=""></spring:message></pre>--%>
	</div>
	<script>
	$(function(){
		setTimeout(function(){dcChange()},100);
	})
	function dcChange(){
		fillOption("CLUSTER", "/api/code_list?grp=dblist.com.clovirsm.common.Component.getCluster&DC_ID=" + searchvue.form_data.DC_ID,
				"", function(data){},"OBJ_NM", "OBJ_NM")
	}

	function cidrValidate(){
		var reg = new RegExp('^(\/([1-9]|[1-2][0-9]|[3][0-2]))$') // cidr
		var trueFalse = new Array();
		var x = true;
		var ip = $('#IP').val();
		var pattern = '/';
		var standard = ip.indexOf(',')
		arr = ip.split(pattern)


		if(standard === -1)
		{
			var val = cidrDiscriminant(ip,pattern,reg)
			if(val){return true}
			else {return false}
		}

		if(standard > -1)
		{
			arr = ip.split(',');

			arr = arr.filter(function(n) {return n;})


			arr.forEach(function(el) {
				var val = cidrDiscriminant(el,pattern,reg)
				trueFalse.push(val)
			})


			//배열안의 값 비교
			return trueFalse.every(function (x) {
				return x === true;
			});

		}
	}
	
	function cidrDiscriminant(ip,pattern,reg)
	{
		var start = ip.indexOf(pattern);
		var end = ip.length;				
		var cidr = ip.substring(start,end)
		var srt = reg.test(cidr)

		return srt; 
	}
	
	
	</script>
</div>
