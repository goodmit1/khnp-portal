<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">

	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="S_DC_ID"
				  keyfield="DC_ID" titlefield="DC_NM"
				name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
			</fm-select>
		</div>
		<div class="col btn_group nomargin">
			<input type="button" id="searchBtn" onclick="search()" class="searchBtn btn" value="<spring:message code="btn_search" text="" />">
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<spring:message code="amount" text="총 개수"/> : <span id="mainTable_total">0</span>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn" id="excelBtn"></button>
			<span class="portGroup btn_group span">
				<select id="new_network"></select>
				<button type="button" onclick="insertRow()"   style="top: 0px;" class="btn icon">
					<i class="fa fat add fa-plus-square"></i>
				</button>
			</span>
			<fm-sbutton cmd="update" class="saveBtn  contentsBtn tabBtnImg save" style="top:-10px;" onclick="save()"><spring:message code="btn_save" text="" /></fm-sbutton>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 600px" ></div>
	<spring:message code="no_ip_template_msg" text="별도로 setting되지 않는 IP는 템플릿의 Network을 그대로 사용합니다." />
	<spring:message code="ip_input_msg" text="IP는 여러개 입력할 때는 ,로 구분합니다. 사용예)10.1.2.0/24,10.1.3.0/24" />
	<br />
</div>
<script>
	var portgroupReq = new Req();
	mainTable;

	$(function() {
		var columnDefs = [ 
						  {headerName : "<spring:message code="NC_PORTGROUP_ID" text="Network" />", field : "NM"},
		                  
		                  {headerName : "<spring:message code="NC_PORTGROUP_VAL1" text="IP" />", width:600,  field : "VAL1", editable: true}
		                  ]
		var
		gridOptions = {
			hasNo : true,
			editable:true,
			columnDefs : columnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onCellEditingStopped: function(event) {
				 
			} 
		}
		mainTable = newGrid("mainTable", gridOptions);
		setTimeout(function(){search();},100);
	});
	function insertRow()
	{
		mainTable.insertRow({DC_ID:search_data.DC_ID, KUBUN:'IPNW', ID:$("#new_network").val(), NM:$("#new_network option:selected").text()});

	}
	var objList=[]
	// 조회
	function search() {
		$.get('/api/dc_mng/list/list_NC_HV_OBJ/?OBJ_TYPE_NM=Network&DC_ID=' + search_data.DC_ID, function(list){
			 
			objList=[];
			$("#new_network").html('');
			for(var i=0; i < list.length;i++)
			{
				objList.push({id:list[i].OBJ_ID, text:list[i].OBJ_NM});
				
			}
			$("#new_network").select2({

				  data : objList,
				  tags: false,
				  width: '150px'
				});
		});
		portgroupReq.search('/api/portgroup/list?KUBUN=IPNW&ALL_YN=Y', function(data){mainTable.setData(data)});
	}
 
			 
		
	function save() {
		if(confirm(msg_confirm_save)){
			mainTable.stopEditing();
			var arr = mainTable.getData();
			var list = [];
			for(var i=0; i<arr.length; i++){
			 	if(!arr[i].ID)
				{
			 		arr[i].ID = arr[i].NM;
				}	
				arr[i].ALL_YN='Y';
				list.push(arr[i]);
			}
//			for(var i in arr){
				 
//			}
			post('/api/portgroup/save_multi', {selected_json: JSON.stringify(list)}, function(){
				alert("<spring:message code="saved" text="저장되었습니다"/>");
				search();
			})
		}
	}

	// 엑셀 내보내기
	function exportExcel() {
		
		 mainTable.exportCSV({fileName:'portGroup'})
		//  exportExcelServer("mainForm", '/api/portgroup/list_excel', 'vmuser',mainTable.gridOptions.columnDefs, portgroupReq.getRunSearchData())
	}

</script>