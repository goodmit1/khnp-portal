<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
#mainTable { height:calc(100vh - 220px); }

#DISK_SIZE,#CPU_CNT,#RAM_SIZE { width: 100px !important; }
#DD_FEE { width: 100px !important; }
#SIZE{
	width: 48% !important;
}
#inputForm > div:nth-child(3) > div > div > div.fm-select{
	width: 40%;
    display: inline-block;
}
#inputForm > div:nth-child(4) > div > span{
	 width: calc(100% - 160px) !important;
}
#inputForm > div:nth-child(4) > div > span > span.selection > span{
	border-radius: 0;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm">
			<fm-input id="DC_ID" name="DC_ID" title="<spring:message code="NC_DC" text="DC" />" onchange="setValue('DC')"></fm-input>
		</div>
		<div class="col col-sm">
			<input type="hidden" id="INS_ID" name="INS_ID" />
			<fm-ibutton id="USER_NM" name="USER_NM" title="<spring:message code="NC_DISK_TYPE_USER_NM" text="사용자명"/>" div_style="width: 150px;" >
				<fm-popup-button popupid="from_user_search_form_popup" popup="/popup/user_search_form_popup.jsp" cmd="update" param="{MY_TEAM:'Y'}" callback="select_from_owner_user"></fm-popup-button>
			</fm-ibutton>
		</div>
		<div class="col col-sm">
			<fm-input id="VM_PREFIX" name="VM_PREFIX" title="<spring:message code="VM_NAME_PREFIX" text="대상 VM명 PREFIX" />" onchange="setValue('VM')"></fm-input>
		</div>
		<div class="col btn_group_under">
			<fm-sbutton cmd="update" class="saveBtn" onclick="save()"   ><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
	


</div>
<div class="fullGrid" id="input_area">
</div>
<script>
	var req = new Req();
	var mainTable;
	$(function() {
		$('.saveBtn').attr('disabled',false);
	});
	
	// 조회
	function search() {
	}
	

	// 저장
	function save() {
		/* req.save('/api/bulkImport/sync',function(){
		}) */
		var param = {
			DC_ID: form_data.DC_ID,
			INS_ID: form_data.INS_ID,
			LOGIN_ID: form_data.LOGIN_ID,
			VM_PREFIX: form_data.VM_PREFIX,
		}
	
		 
		
		$.ajax({
			type: 'GET',
            url: '/api/bulkImport/sync',
            async: false,
            data: param,
            success: function(data) {
          		//showLoading()
   		 		 
        	  	 
            },
            error: function(request,status,error){
            	hideLoading()
                alert("code = "+ request.status + " error = " + error); // 실패 시 처리
                 
            }
		}) 
	}
	
	function setValue(val){
		
		if(val ==='DC')
		{
			var DC_ID = $("#DC_ID").val();
			form_data.DC_ID = DC_ID;
		}
		else{
			var VM_PREFIX = $("#VM_PREFIX").val();
			form_data.VM_PREFIX = VM_PREFIX;
		}
	}
	
	function select_from_owner_user(args, callback){
		$('#INS_ID').val(args.USER_ID);
		$('#USER_NM').val(args.USER_NAME);

		form_data.INS_ID = args.USER_ID;
		form_data.LOGIN_ID = args.LOGIN_ID;
		if(callback) callback();
	}

</script>


