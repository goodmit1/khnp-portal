<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 입력 Form -->
<div class="tab-buttons">
	<fm-sbutton cmd="update" class="exeBtn tabBtn tabBtnImg sync"  title="<spring:message code="label_password_reset_message" text="초기화시 비밀번호는 ID와 동일하게 변경됩니다." />" onclick="passwordReset()"><spring:message code="btn_password_reset" text="비밀번호 초기화"/></fm-sbutton>
	<fm-sbutton cmd="update" class="exeBtnx tabBtn tabBtnImg sync" style="float:right" onclick="saveUserType()"><spring:message code="btn_save_USER_TYPE" text="권한저장"/></fm-sbutton>
</div>
<div class="form-panel detail-panel  panel panel-default">
	<div class="panel-body">
		<input id="USER_ID" name="USER_ID" type="hidden" v-model="form_data.USER_ID" />
		<div class="col col-sm-6">
			<fm-output id="TEAM_NM" name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<div class="fm-output"><label for="USER_NAME" class="control-label grid-title value-title"><spring:message code="FM_USER_USER_NAME" text="사용자명" /></label>
            <div id="USER_NAME" name="USER_NAME" class="output  hastitle">{{form_data.USER_NAME}}({{form_data.LOGIN_ID}})</div></div>
		</div>
		<div class="col col-sm-6">
			<fm-output id="USE_YN_NM" name="USE_YN_NM" title="<spring:message code="USE_YN" text=""/>"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-select url="/api/code_list?grp=USER_TYPE" id="USER_TYPE"
				keyfield="USER_TYPE" titlefield="USER_TYPE_NM"
				name="USER_TYPE" title="<spring:message code="FM_USER_USER_TYPE" text="권한"/>">
			</fm-select>
		</div>
		<div class="col col-sm-6">
			<fm-output id="LAST_ACCESS_TMS" name="LAST_ACCESS_TMS"
				title="<spring:message code="FM_USER_LAST_ACCESS_TMS" text="마지막 접속시간" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="AUTH_FAIL" name="AUTH_FAIL" ostyle="width: calc(100% - 200px);"
				title="<spring:message code="FM_USER_AUTH_FAIL" text="비밀번호 실패 횟수" />"></fm-output>
		</div>
	</div>
	<script>
	function saveUserType(){
		if(chkSelection()){
			var param = {USER_ID:form_data.USER_ID, LOGIN_ID:form_data.LOGIN_ID,USER_TYPE:form_data.USER_TYPE}
			post("/api/user_mng/save", param, function(){
				alert("<spring:message code="saved" text="저장되었습니다"/>");
				userSearch();
			})
		}
	}
	</script>
</div>
