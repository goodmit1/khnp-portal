<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:240px;margin: 0px;">
	<div id="userResTable" class="ag-theme-fresh" style="height: 200px;" ></div>
</div>
<script>
	var userResReq = new Req();
	userResTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="label_resource_type" text="" />", field : "SVC_CD_NM"},
		                  {headerName : "<spring:message code="label_resource_nm" text="" />", field : "SVC_NM", width:200},
		                  {headerName : "<spring:message code="label_event" text="" />", field : "CUD_NM"},
		                  {headerName : "<spring:message code="NC_VM_CMT" text="" />", field : "CMT",
		                	  valueGetter: function(params) {
		                		  if(params.data && params.data.CMT){
							    	  return removeTag(params.data.CMT)
		                		  }
		                		  return "";  
		                  }},
		                  {headerName : "<spring:message code="NC_VM_INS_DT" text="" />", field : "INS_DT", format:'datetime'} 
		                  ]
		var
		userResTableGridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		userResTable = newGrid("userResTable", userResTableGridOptions);
	});

	function userResSearch(VM_ID){
		userResReq.searchSub('/api/user_mng/querykey/list_NC_VM_REQ', {USER_ID: mainTable.getSelectedRows()[0].USER_ID}, function(data) {
			userResTable.setData(data);
		});
	}

	// 엑셀 내보내기
	function exportExcel(){
		exportExcelServer("mainForm", '/api/vm/user_list_excel', 'userResList',userResListTable.gridOptions.columnDefs, userResReq.getRunSearchData())
	}

</script>


