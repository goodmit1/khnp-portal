<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 코드 관리 -->
<style>
div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(1) > div.ag-cell-label-container.ag-header-cell-sorted-none{
	width: auto;
}
</style>
<!-- <div id="search_area" class="search-panel panel panel-default"> -->
<!-- 	<div class="panel-body"> -->
<!-- 		<div class="col col-sm"> -->
<!-- 				<fm-select2 url="" id="S_CONN_ID" keyfield="CONN_ID" titlefield="CONN_NM" emptystr="&nbsp" class="select2" -->
<%-- 							name="CONN_ID" title="<spring:message code="" text="연결명"/>"></fm-select2> --%>
<!-- 		</div> -->
<!-- 	</div> -->
<!-- </div> -->
<div class="fullGrid" id="input_area">
	<div class="box_s"> 
	<div class="table_title layout name">
		<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportEtc()" class="layout excelBtn" id="excelBtn"></button>
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg top del" class="btn btn-primary" onclick="delAlert()"><spring:message code="btn_del" text="삭제"/></fm-sbutton>
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg top save" class="btn btn-primary" onclick="changeEtc()"><spring:message code="" text="수정"/></fm-sbutton>
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg top new" class="btn btn-primary" onclick="newEtc()"><spring:message code="new" text="신규"/></fm-sbutton>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height:450px;"></div>
	</div>
	<fm-popup-button style="display: none;" popupid="repositoryAdd_popup" popup="/clovirsm/popup/repositoryAdd.jsp" callback="searchEtc" cmd="update" param="param"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
</div>
<script>
var param;
var etcReq = new Req();
function exportEtc(){
	mainTable.exportCSV({fileName:''})
}
function delAlert(){
	FMAlert("<img src='/res/img/hynix/confirm.png' style='margin-bottom:20px;margin-top: 20px;'><div style='font-size: 16px; font-weight: 900; margin-bottom:20px;'><spring:message code="" text=""/>삭제시 시스템에 문제가 생길 수도 있습니다. 삭제하시겠습니까?</div>",'경고','삭제', function(){delEtc();} );					 
}
function delEtc(){
	var arr = mainTable.getSelectedRows();
	var data = JSON.stringify(arr);
	post('/api/etcConnMng/delete_multi', {'selected_json' : data} ,function(){
		searchEtc();
	});
}
function newEtc(){
	param = {};
	$("#repositoryAdd_popup_button").click();
}
function changeEtc(){
	
	param = mainTable.getSelectedRows()[0];
	
	if(param){
		$("#repositoryAdd_popup_button").click();
	} else{
		alert(msg_select_first);
	}
}
function scheduleSelect(value){
	var param = new Object();
	param.DD_ID = 'ENV';
	param.DD_VALUE = 'schedule_svr';
	param.KO_DD_DESC = value;
	
	post('/api/ddicUser/save', param , function(){
		alert("<spring:message code="saved" text="저장되었습니다"/>");
		searchEtc();
	})
}
function searchEtc(){
	etcReq.search('/api/etcConnMng/list', function(data){
		mainTable.setData(data);
	});
}
function connData(){
	post("/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_ETC_CONN_NM",{"CONN_TYPE":"VRA"},function(data){
		fillOptionByData('S_CONN_ID', data, '', "CONN_ID", "CONN_NM");
	});
}
var mainTable;
$(document).ready(function(){
	var columnDefs =[{ field: "CONN_ID", hide:true},
	    { headerName: "<spring:message code="name" text="명칭" />", field: "CONN_NM"},
	    {
			headerName: "TYPE",
			field: "CONN_TYPE",
		},
	    {
			headerName: "URL",
			field: "CONN_URL",
			width:400
		},
	    {
			headerName: "USER ID",
			field: "CONN_USERID",
		},
	    {
			headerName: "PWD",
			field: "CONN_PWD",valueFormatter:function(params){
				return "**";
			},
		},
	    {
			headerName: "PROP",
			field: "CONN_PROP",
		},
		{
			headerName: "비고",
			field: "ETC",
			cellRenderer: function(params) {
				if(params.data.CONN_TYPE == 'Portal' ) {
					if(params.data.ETC){
						return '스케줄 서버';				
					} else{
						return '<input type="button" value="스케줄 서버 선택"  onclick="scheduleSelect(\'' + params.data.CONN_NM + '\', this);return false">';
					}
					
				} else{
					return "";
				}
			}
			
		}
	     ];
	var gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		};
	mainTable = newGrid("mainTable", gridOptions);
	connData();
	searchEtc();
});
</script>