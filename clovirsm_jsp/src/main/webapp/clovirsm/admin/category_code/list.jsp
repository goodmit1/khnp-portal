<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  서버 목록 조회 -->
<style>
.plus_btn{
    width: 20px;
    padding: 0px 3px !important;
    font-weight: 100;
    font-size: 10px;
    }
.pic{
    margin-left: 15px;
    font-weight: bold;
    color: #00a832;
   }    
.pic .del{
    display: inline-block;
    overflow: hidden;
    width: 9px;
    height: 9px;
    margin: -2px 10px 0 6px;
    background: url(https://ssl.pstatic.net/imgshopping/static/pc-real-2017/img/search/sp_search_v4.png) no-repeat -180px -50px;
    line-height: 9999px;
    vertical-align: middle;
   }     
#filter_area{
 	margin-left: 50px;
 }  
.bar {
    margin: 0 -4px 0 12px;
    padding: 0;
  }
.clear {
    display: inline-block;
    overflow: hidden;
    margin-left: 15px;
    font-size: 12px;
    color: #6b6b6a;
    vertical-align: middle;
	}   
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		 
		<div class="col col-sm">
			<fm-input id="S_OS_NM" name="CATEGORY_NM" title="<spring:message code="TASK" text="업무" />"></fm-input>
		</div>
		 <div class="col col-sm">
			<fm-input id="S_OS_NM" name="CATEGORY_CODE" title="<spring:message code="NC_OS_TYPE_VM_NAMING" text="VM Naming" />"></fm-input>
		</div>
	 	 
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="ostypeSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:540px">
	<div class="box_s">
	<div class="table_title layout name">
		<div style="float:left">
		<spring:message code="mainTable_Search_information" text="검색정보" />
				<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="osGrid_total">0</span>&nbsp)</span>
		</div>
		<div id="filter_area" style="float:left"></div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn" id="excelBtn"></button>
      			</div>
		
	</div>
	<div id="osGrid" class="ag-theme-fresh" style="height: 600px" ></div>
	<div  class="form-panel panel panel-default detail-panel">
	<div class="panel-body">
		<div class="col col-sm-5">
			<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM" input_div_class="padding-top1" title="<spring:message code="TASK"/>" class="inline">
				<fm-popup-button popupid="s_category" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
			</fm-ibutton>
			<input type="text" style="display:none" id="S_CATEGORY" name="CATEGORY_ID">
			<input type="text" style="display:none" id="S_CATEGORY_IDS" name="CATEGORY_IDS">
		</div>
		<div class="col col-sm-2">
		<fm-input title="<spring:message code="NC_OS_TYPE_VM_NAMING" text="VM Naming" />"   name="CATEGORY_CODE" id="N_CATEGORY_CODE"></fm-input>
		</div>
		<div class="col col-sm-2">
		<fm-input title="<spring:message code="" text="VM NUM" />"  name="CATEGORY_NUM" id="N_CATEGORY_NUM"></fm-input>
		</div>
		<div class="col col-sm-2">
		<fm-select title="<spring:message code="CLUSTER_GROUP" text="Cluster Group" />"  url="/api/code_list?grp=CLUSTER_GROUP" name="KUBUN" id="N_KUBUN"></fm-select>
		</div>
		<div class="col col-sm-1" style="line-height:39px">
			<fm-sbutton id="insert_ostype_button" cmd="update" class="newBtn tabBtnImg new" onclick="save()"><spring:message code="btn_new" text="신규"/></fm-sbutton>
		</div>
	</div>
	</div>
	 </div>
</div>
<script>
	var ostypeReq = new Req();
	var clusterGrop = new Array() ;
	osGrid;

	function select_S_CATEGORY(data){
		console.log(data);
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
		$("#S_CATEGORY").val(data.CATEGORY);
		$("#S_CATEGORY_IDS").val(data.CATEGORY_IDS);
	}
	
	function allClearFilter()
	{
		osGrid.gridOptions.api.setFilterModel(null);
		osGrid.gridOptions.api.onFilterChanged();
		 $("#filter_area").html(""); 
	}
	function clearFilter(obj, field){
		var filter = osGrid.gridOptions.api.getFilterInstance(field);
		 filter.setFilter("");

		 osGrid.gridOptions.api.onFilterChanged();
		 $(obj).remove();
		 if($("#filter_area .pic").length==0)
		 {
			 $("#filter_area").html(""); 
		 }
	}
	function applyAllFilter()
	{
		 $("#filter_area .pic").each(function(){
			 
			 
			 var filter = osGrid.gridOptions.api.getFilterInstance($(this).attr("data-field"));
			 filter.setFilter($(this).attr("data-val"));

			 osGrid.gridOptions.api.onFilterChanged();
		 })
	}
	function filter(field,text)	{
		var filter = osGrid.gridOptions.api.getFilterInstance(field);
		 filter.setFilter(text);

		 osGrid.gridOptions.api.onFilterChanged();
		 if( $("#filter_area").html()=="") {
			 $("#filter_area").append('<a id="_resetFilter" href="#" class="clear" onclick="allClearFilter();return false"><span class="ico_clear"></span><spring:message code="all_clear" text="전체해제"/></a><span class="bar">|</span>')
		 }
		 $("#filter_area").append("<a class='pic' href='#' data-field='" + field + "' data-val='" + text + "' onclick='clearFilter(this, \"" + field + "\");return false'>" + text + "<span class='del'>Clear</span></a>");
	}
	var columnDefs;
	$(function() {
		columnDefs = [{headerName : "<spring:message code="NC_OS_TYPE_VM_NAMING" text="VM Naming" />", field : "CATEGORY_CODE", editable : true },
					  {headerName : "<spring:message code="VM_NUM" text="VM NUM" />", field : "CATEGORY_NUM", editable : true },
					  {headerName : "<spring:message code="CLUSTER_GROUP" text="Cluster Group" />", field : "KUBUN", editable : true, cellEditor: "select",
					        cellEditorParams: {
					            values: []
					        	} },
					  {headerName : 'X', cellRenderer:function(params){
						  
						  	return "<a href=# onclick='deleteCode(\""+ params.data.CATEGORY_ID + "\");return false'>X</a>"
					        		}      	
					   }
		              ]
		var
		gridOptions = {
			hasNo : false,
			columnDefs : columnDefs,
			
			//rowModelType: 'infinite',
			//rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [] ,
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				var arr = osGrid.getSelectedRows();
				 
			},
			onCellClicked : function(params){
				
					if("A"==params.event.srcElement.tagName) return ;
					if(params.colDef.field.indexOf("NAME")<0 || params.value=="" || !params.value) return;
					filter(params.colDef.field, params.value)
			},
			onCellValueChanged: function(params) {
				if(params.newValue != params.oldValue)
				{
					if("CATEGORY_CODE" == params.colDef.field)
					{
						if(categoryCodeCheck(params)){
							var obj = {CATEGORY_ID:params.data.CATEGORY_ID, CATEGORY_CODE:params.newValue}
							 
							post("/api/popup/category/save", obj, function(data){
								//var changedData = [params.data];
								//osGrid.gridOptions.api.updateRowData({update: changedData});
								//osGrid.gridOptions.api.refreshCells()
								osGrid.gridOptions.api.redrawRows()
							});	
						} else{
							 
							FMAlert("<spring:message code='err.DUPLICATE_NAME' arguments='"+params.newValue+"' text='중복된 이름이 존재합니다.' />",'요청 완료',null, null );
						}
					} else if("CATEGORY_NUM" == params.colDef.field){
						var obj = {CATEGORY_ID:params.data.CATEGORY_ID, CATEGORY_NUM:params.newValue}
						 
						post("/api/popup/category/save", obj, function(data){
							//var changedData = [params.data];
							//osGrid.gridOptions.api.updateRowData({update: changedData});
							//osGrid.gridOptions.api.refreshCells()
							osGrid.gridOptions.api.redrawRows()
						});	
					} else if("KUBUN" == params.colDef.field){
						var obj = {CATEGORY_ID:params.data.CATEGORY_ID, KUBUN:params.newValue}
						 
						post("/api/popup/category/save", obj, function(data){
							//var changedData = [params.data];
							//osGrid.gridOptions.api.updateRowData({update: changedData});
							//osGrid.gridOptions.api.refreshCells()
							osGrid.gridOptions.api.redrawRows()
						});	
					}
				}	
		       
		    },
		    onCellEditingStarted: function(event) {
				if(event.column.colId=='KUBUN'){
					$("#osGrid div.ag-cell-value[col-id='KUBUN'] select").select2({

					  data : clusterGrop,
					  tags: true,

					  createTag: function (params) {
						    return {
						      id: params.term,
						      text: params.term,
						      newOption: true
						    }
						  }})
				}
		    }
		}
		osGrid = newGrid("osGrid", gridOptions);
		initClusterGroup();
		ostypeSearch();
	});
	function deleteCode(id){
		var params={};
		params.CATEGORY_ID =id;
		params.CATEGORY_CODE ='';
		params.CATEGORY_NUM ='';
		params.KUBUN ='';
		post("/api/popup/category/save", params, function(data){
			ostypeSearch();
		});	
	}
	function save(){
		var params={};
		var data ={};
		params.CATEGORY_ID = $("#S_CATEGORY").val();
		params.newValue = $("#N_CATEGORY_CODE").val();
		params.CATEGORY_CODE = $("#N_CATEGORY_CODE").val();
		params.CATEGORY_NUM = $("#N_CATEGORY_NUM").val();
		params.KUBUN = $("#N_KUBUN").val();
		data.IDS = $("#S_CATEGORY_IDS").val
		params.data = data;
		
		if(categoryCodeCheck(params)){
			 
			 
			post("/api/popup/category/save", params, function(data){
				ostypeSearch();
			});	
		} else{
			 
			FMAlert("<spring:message code='err.DUPLICATE_NAME' arguments='"+params.newValue+"' text='중복된 이름이 존재합니다.' />",'요청 완료',null, null );
		}
	}
	 
	// 조회
	function ostypeSearch() {
		ostypeReq.search('/api/ostype/list_include_category?HAS_CODE_YN=Y' , function(data) {
			 var maxLevel = data.maxLevel;
			 var colArr = [];
			 for(var i=0; i < maxLevel; i++) {
				 colArr.push({	
					 headerName : "<spring:message code="TASK" text="업무" />" + (i+1),   
				 	field : "NAME_" + i,  
				 	 })
			 }
			
			 osGrid.gridOptions.api.setColumnDefs($.merge(colArr, columnDefs));
			 osGrid.setData(data);
			 applyAllFilter();
			 
		});
		
	}
	// vmName 중복 확인
	function categoryCodeCheck(item){
		var result = true;
		var osList = osGrid.getData();
		for(var i = 0 ; i < osList.length ; i++){
			if(osList[i].CATEGORY_CODE == item.newValue && (item.data && osList[i].IDS != item.data.IDS)){
				result = false; 
				return result;
			}
		}
		return result;
	}
	 
	// 엑셀 내보내기
	function exportExcel(){
		osGrid.exportCSV({fileName:'Template'});
		//exportExcelServer("mainForm", '/api/ostype/list_excel', 'OS_TYPE',osGrid.gridOptions.columnDefs, ostypeReq.getRunSearchData())
	}
	function initFormData()
	{
		<c:if test="${!empty param.NEW_CATEGORY_CODE}">
			$("#NEW_CATEGORY_CODE").attr("checked", true);
			search_data.NEW_CATEGORY_CODE = ${param.NEW_CATEGORY_CODE};
		</c:if>
	}
	
	function initClusterGroup() {
		post('/api/code_list?grp=CLUSTER_GROUP', {}, function(data) {
			for(i in data){
				clusterGrop.push(data[i])
			}
		});
		
	}
</script>


