<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<div class="col col-sm-3" style="width: 13.666667%;min-width:240px;">
			<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM" title="<spring:message code="TASK"/>" class="inline">
				<fm-popup-button popupid="category1" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
			</fm-ibutton>
			<input type="text" style="display:none" id="S_CATEGORY" name="CATEGORY">
		</div>
		<div class="col col-sm">
			<fm-input id="S_ID" name="ID" title="<spring:message code="PURPOSE" text="용도" />"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			
			<fm-sbutton cmd="search" class="searchBtn" onclick="search('defalut')"  ><spring:message code="btn_search" text=""/></fm-sbutton>
		</div>
<!-- 		<div class="col btn_group_under"> -->
<%-- 			<fm-sbutton cmd="search" class="exeBtn" onclick="sync()"  ><spring:message code="btn_sync_ip" text="IP정보수집"/></fm-sbutton> --%>
<!-- 		</div> -->
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<button type="button" id="del" class="contentsBtn tabBtnImg del"  onclick="purposeDel()"><spring:message code="" text="삭제"/></button></div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 450px;" ></div>
 	</div>
	<jsp:include page="detail.jsp"></jsp:include>
	</div>
	<fm-popup-button style="display:none;" popupid="taskPurposeMapping"
	popup="/clovirsm/popup/taskpurposeMapping.jsp" callback="purpose_auto" cmd="update" param="temp">
</div>

<script>
var temp = new Object();
var etcReq = new Req();
function exportEtc(){
	mainTable.exportCSV({fileName:''})
}
function search(){
	etcReq.search('/api/category/map/list?KUBUN=P', function(data){
		mainTable.setData(data);
	});
}
var mainTable;
$(document).ready(function(){
	var columnDefs =[
	    { headerName: "<spring:message code="TASK" text="업무" />", field: "CATEGORY_FULL_NM"},{
			headerName: "<spring:message code="PURPOSE" text="용도" />",
			field: "ID",
		}
	     ];
	var gridOptions = {
			editable:true,
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			//rowSelection : 'single',
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		};
	mainTable = newGrid("mainTable", gridOptions);
	search();
	setCategory();
});

function purposeDel(){
	var arr = mainTable.getSelectedRows();
	var data = JSON.stringify(arr);
	post('/api/category/map/delete_multi', {'selected_json' : data} ,function(){
		search();
	});
	
}

function select_S_CATEGORY(data){
	searchvue.form_data.CATEGORY = data.CATEGORY;
	searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
	$("#S_CATEGORY").val(data.CATEGORY);
	$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
}
function setCategory(){
	fillOption("_svc", "/api/category/map/list/list_NC_CATEGORY_MAP_SELECT_TASK/?ALL_USE_YN=Y", '', function(data){

	}, "CATEGORY_ID", "CATEGORY_NM");
}
function purpose_auto(){
	temp.CATEGORY_ID = $("#_svc").val();
	fillOption("S_PURPOSE", '/api/category/map/list/list_NC_CATEGORY_MAP_PURPOSE/?CATEGORY_ID='+ $("#_svc").val(), '', function(data){
		
	}, "ID", "TITLE");
	
}

function taskPurposeMappingClick() {
	if($("#_svc").val() != null && $("#_svc").val() != ''){
		$('#taskPurposeMapping_button').trigger('click');
	} else{
		alert("<spring:message code="" text="업무를 선택해주세요."/>")
	}
};
</script>






