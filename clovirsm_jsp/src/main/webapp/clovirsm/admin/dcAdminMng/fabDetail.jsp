<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-3">
			<fm-select  id="S_TAB1_DC" name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>" emptystr=" " keyfield="DC_ID" titlefield="DC_NM" required="true" onchange="vCenterChange(this);">
		</div>
		<div class="col col-sm-3">
			<fm-select url="" id="S_cluster" name="VAL2" title="Cluster" >
		</div>
		<div class="col col-sm-2">
			<fm-select url="/api/code_list?grp=FAB" id="FAB" name="ID" title="FAB" required="true">
		</div>
		<div class="col col-sm-3">
			<fm-select url="/api/code_list?grp=P_KUBUN" id="KUBUN" name="VAL1" title="<spring:message code="" text="구분"/>" required="true">
		</div>
		<div class=" col col-sm-1">
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg save top5" onclick="adminSave('DC_FAB');"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
	
	 
</div>
