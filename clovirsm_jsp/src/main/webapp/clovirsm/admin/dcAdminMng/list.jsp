<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 코드 관리 -->
<style>
div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(1) > div.ag-cell-label-container.ag-header-cell-sorted-none{
	width: auto;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다  -->
		<div class="col col-sm">
			<fm-select url="" id="S_DC" name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>" emptystr=" " keyfield="DC_ID" titlefield="DC_NM">
		</div>
		<div class="col btn_group nomargin" style="padding-left:5px;">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>
<ul class="nav nav-tabs" style="margin-left:1%">
	 	<li  class="active">
	        <a href="#tab1" data-toggle="tab" onclick="setTabIdx( 1)"><spring:message code=""  text="FAB"/></a>
		</li>
		<li >
	        <a href="#tab2" data-toggle="tab" onclick="setTabIdx( 2)" ><spring:message code=""  text="관리자"/></a>
		</li>
</ul>
<div class="fullGrid" id="input_area">
	<div class="tab-content ">
		<div  class="form-panel detail-panel tab-pane active" id="tab1"  >
			<div class="box_s"> 
				<div>
					<div class="table_title layout name">FAB
						<div class="btn_group">
							<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg top del" class="btn btn-primary" onclick="fabDel()"><spring:message code="btn_del" text="삭제"/></fm-sbutton>
							<fm-popup-button popupid="fab_popup" popup="/clovirsm/popup/fabAdd_popup.jsp" class="contentsBtn tabBtnImg new " ><spring:message code="" text="FAB추가"/></fm-sbutton>
						</div>
					</div>
					<div id="fabTable" class="ag-theme-fresh" style="height:450px;"></div>
				</div>
			</div>
			<jsp:include page="fabDetail.jsp"></jsp:include>
		</div>
		<div  class="form-panel detail-panel tab-pane" id="tab2"  >
			<div class="box_s"> 
				<div>
					<div class="table_title layout name">관리자
						<div class="btn_group">
							<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg top del" class="btn btn-primary" onclick="adminDel()"><spring:message code="btn_del" text="삭제"/></fm-sbutton>
						</div>
					</div>
					<div id="adminTable" class="ag-theme-fresh" style="height:450px;"></div>
				</div>
			</div>
			<jsp:include page="adminDetail.jsp"></jsp:include>
		</div>
	</div>
</div>
<script>
var fabData ; 
var adminData ; 
var fabSetting = new Req();
function exportspecSetting(){
	adminTable.exportCSV({fileName:'ServerSetting.csv'})
}

function fabSearch(){
	fabSetting.setSearchData({KUBUN:"DC_FAB"})
	fabSetting.search('/api/fab_setting/list/list_NC_DC_KUBUN_CONF_FAB/', function(data){
		fabTable.setData(data);
	});
}
function adminSearch(){
	fabSetting.setSearchData({KUBUN:"DC_ADMIN"})
	fabSetting.search('/api/fab_setting/list/list_NC_DC_KUBUN_CONF_ADMIN/', function(data){
		adminTable.setData(data);
	});
}
function search(){
	fabSearch();
	adminSearch();
}
function adminSave(kubun){
	if(validate("content")){
		var arr = {};
		var filter = [];
		if(kubun == "DC_ADMIN"){
			arr = adminData;
			filter = ["DC_ID","ID"];
		} else{
			filter =  ["ID","VAL1"];
			arr = fabData;
		}
		var object = fabSetting.getData();
		if (checkVaildate(arr , object, filter)){
			alert("<spring:message code="" text="중복된 값이 있습니다."/>");
			return false;
		}
		fabSetting.putData({"KUBUN":kubun});
		fabSetting.save("/api/fab_setting/save",function(){
			alert("<spring:message code="saved" text="저장되었습니다"/>");
			fabSearch();
			adminSearch();
		});
	}
	
}

function checkVaildate(arr, object,filter){
	
	for(var i = 0 ; i < arr.length; i++){
		var result = false;
		for(var z = 0 ; z < filter.length ; z++){
			
			if(object[filter[z]] == arr[i][filter[z]]){
				result  = true;
			}
			else{ 
				result = false;
				break;
			}
		}
		if(result)
			return result;
	}
}
function adminDel(){
	var arr =adminTable.getSelectedRows();
	fabSetting.putData({KUBUN:"DC_ADMIN", ID : arr[0].ID , DC_ID : arr[0].DC_ID});
	fabSetting.del("/api/fab_setting/delete",function(){
		alert("<spring:message code="deleted" text="삭제되었습니다."/>");
		adminSearch();
	});
	
	
}
function fabDel(){
	var arr =fabTable.getSelectedRows();
	fabSetting.putData({KUBUN:"DC_FAB", ID : arr[0].ID , DC_ID : arr[0].DC_ID, VAL1 : arr[0].VAL1});
	fabSetting.del("/api/fab_setting/delete",function(){
		alert("<spring:message code="deleted" text="삭제되었습니다."/>");
		fabSearch();
	});
}
function ${popupid}_select_user(args, callback){
	 
	form_data.ID = args.USER_ID;
	$('#USER_NAME').val(args.USER_NAME);
	if(callback) callback();
}
var adminTable;
var fabTable;
var tabIdx = 1;
function setTabIdx(idx)
{
	tabIdx = idx;
	fabSetting.setData();
	
}

$(document).ready(function(){
	var fabDefs =[
		{
			headerName: "<spring:message code="" text="지역" />",
			field: "LOCATION",
		},
		{
			headerName: "<spring:message code="" text="FAB" />",
			field: "FAB_NM",
		},
		{
			headerName: "<spring:message code="" text="구분" />",
			field: "P_KUBUN_NM",
		},
		{
			headerName: "<spring:message code="NC_DC_DC_ID" text="데이터센터"/>",
			field: "DC_NM"
		},
		
		{
			headerName: "<spring:message code="" text="Cluster" />",
			field: "VAL2",
		}
	     ];
	var adminDefs =[
	    {
			headerName: "<spring:message code="NC_DC_DC_ID" text="데이터센터"/>",
			field: "DC_NM"
		},
		{
			headerName: "<spring:message code="" text="부서" />",
			field: "TEAM_NM",
		},
		{
			headerName: "<spring:message code="" text="이름" />",
			field: "USER_NAME",
		}
	     ];
	var fabGridOptions = {
		    columnDefs: fabDefs,
		    rowData: [],
			rowSelection : 'single',
		    enableSorting: true,
		    enableColResize: true
		};
	var adminGridOptions = {
		    columnDefs: adminDefs,
		    rowData: [],
			rowSelection : 'single',
		    enableSorting: true,
		    enableColResize: true
		};
	fabTable = newGrid("fabTable", fabGridOptions)
	adminTable = newGrid("adminTable", adminGridOptions)
	
	search();
	 
	
	post("/api/dc_mng/list", null, function(data){
		
	    fillOptionByData("S_TAB1_DC", data.list , '',  'DC_ID','DC_NM');
	    fillOptionByData("S_TAB2_DC", data.list , '',  'DC_ID','DC_NM');
	    fillOptionByData("S_DC", data.list , '',  'DC_ID','DC_NM');
	})
	
	post('/api/fab_setting/list/list_NC_DC_KUBUN_CONF_FAB/' , {KUBUN:"DC_FAB"} , function(data){
		fabData = data;
	})

	post('/api/fab_setting/list/list_NC_DC_KUBUN_CONF_ADMIN/' , {KUBUN:"DC_ADMIN"} , function(data){
		adminData = data;
	})
});
function vCenterChange(that){
	var dcId = $(that).val();
	clusterInit(dcId);
}
function clusterInit(dcId){
	var id = dcId; 
	if(!id){
		id = $("#S_TAB1_DC").val();
	} 
	post("/api/fab_setting/list/list_cluster/?DC_ID="+id, null, function(data){
    	fillOptionByData("S_cluster", data , '',  'OBJ_NM','OBJ_NM');
	});

	
}
</script>