<%@page contentType="text/html; charset=UTF-8" %><%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm"  >
    	<jsp:include page="list.jsp"></jsp:include>
    	<fmtags:include page="after.jsp" />
    	</form>
    </layout:put>
    <layout:put block="popup_area">
    	<jsp:include page="./img_category_input_popup.jsp"></jsp:include>
    	<jsp:include page="./input_popup.jsp"></jsp:include>
	</layout:put>
</layout:extends>