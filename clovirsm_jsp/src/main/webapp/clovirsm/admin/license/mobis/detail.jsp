<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->
<div id="subTable" class="ag-theme-fresh padding25" style="height: 400px; width: 100%;" ></div>
<script>
var subTable;

$(function() {
	var columnDefs = [
		
		{headerName : "<spring:message code="NC_DC" text="DC" />",field : "DC_NM"},
		{headerName : "<spring:message code="NC_Cluster" text="Cluster" />",field : "PARENT_OBJ_NM"},
		{headerName : "<spring:message code="HOST_NAME" text="HOST" />",field : "NAME"},
		{headerName : "<spring:message code="NC_LICENSE_NUMCPUPKGS" text="" />", cellStyle:{'text-align':'right'}, valueFormatter:function(params){
			return formatNumber(params.value);
		}
		,field : "NUMCPUPKGS"},
		
		]
	var
	gridOptions = {
		hasNo : true,
		columnDefs : columnDefs,
		//rowModelType: 'infinite',
		rowSelection : 'single',
		sizeColumnsToFit: false,
		cacheBlockSize: 100,
		rowData : [],
		enableSorting : true,
		enableColResize : true,
		enableServerSideSorting: false
	}
	subTable = newGrid("subTable", gridOptions);
});
function subSearch(licenseKey) {
	req.search('/api/license/list/list_NC_LICENSE_HOST/?LICENSE_KEY=' + licenseKey , function(data) {
		subTable.setData(data);
	});
}
</script>