<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->
<div id="summaryTable" class="ag-theme-fresh" style="height: 400px; width: 100%;" ></div>
<script>
var summaryTable;
var summaryArray = new Array();
$(function() {
	var columnDefs = [
		{headerName : "<spring:message code="" text="제품명" />",field : "LICENSE_NAME"},
		{headerName : "<spring:message code="" text="구입갯수" />",field : "LICENSE_CNT"},
		{headerName : "<spring:message code="" text="수집갯수" />",field : "LICENSE_COLLECTION_CNT"},
		{headerName : "<spring:message code="" text="사용갯수" />",field : "LICENSE_USE_CNT"},
		{headerName : "<spring:message code="" text="잔여갯수" />",field : "LICENSE_RESIDUAL_CNT",valueGetter:function(params) {
			var num = Number(params.data.LICENSE_CNT) - Number(params.data.LICENSE_USE_CNT);
				return num;
        }}
		]
	var
	gridOptions = {
		hasNo : true,
		columnDefs : columnDefs,
		//rowModelType: 'infinite',
		pinnedBottomRowData:createInputFooter(0),
		rowSelection : 'single',
		sizeColumnsToFit: true,
		cacheBlockSize: 100,
		rowData : [],
		enableSorting : true,
		enableColResize : true,
		enableServerSideSorting: false
	}
	summaryTable = newGrid("summaryTable", gridOptions);
	
	setTimeout(function() {
		summarySearch();
	}, 500);
});
function summarySearch() {
	  
	 var mainData = mainTable.getData();
	 var inputData = inputTable.getData();
	 var unique = {};
	 summaryArray=[];
	 for(var i = 0 ; i < mainData.length; i++){
		
		var LICENSE_CNT = 0;
		var summaryObject = unique[ mainData[i].LICENSE_NAME] ;
		if(summaryObject== null){
			  summaryObject = new Object();
			  summaryObject.LICENSE_NAME = mainData[i].LICENSE_NAME;
			  summaryObject.LICENSE_COLLECTION_CNT = 0;
			  summaryObject.LICENSE_USE_CNT = 0;
			  unique[ mainData[i].LICENSE_NAME] = summaryObject;
			  summaryArray.push(summaryObject);
		}
		
		summaryObject.LICENSE_COLLECTION_CNT += mainData[i].LICENSE_TOTAL;
		summaryObject.LICENSE_USE_CNT += mainData[i].LICENSE_USED;
		for(var z = 0 ; z < inputData.length ; z++){
			if(mainData[i].LICENSE_NAME == inputData[z].LICENSE_NAME){
				
				if(inputData[z].LICENSE_CNT != null && inputData[z].LICENSE_CNT != 'undefined')
					LICENSE_CNT += Number(inputData[z].LICENSE_CNT); 
			}
		}
		summaryObject.LICENSE_CNT = LICENSE_CNT;
		
	 }
	 summaryTable.setData(summaryArray);
	 
	 var summaryTableData = summaryTable.getData();
	 var TOTAL_LICENSE_CNT = 0;
	 var TOTAL_LICENSE_COLLECTION_CNT = 0;
	 var TOTAL_LICENSE_USE_CNT = 0;
	 var TOTAL_LICENSE_RESIDUAL_CNT = 0;
	 
	 for(var i = 0 ; i < summaryTableData.length ; i++){
		 TOTAL_LICENSE_CNT += summaryTableData[i].LICENSE_CNT;
		 TOTAL_LICENSE_COLLECTION_CNT += summaryTableData[i].LICENSE_COLLECTION_CNT;
		 TOTAL_LICENSE_USE_CNT += summaryTableData[i].LICENSE_USE_CNT;
		 TOTAL_LICENSE_RESIDUAL_CNT += summaryTableData[i].LICENSE_RESIDUAL_CNT;
	 }
	 
	 summaryTable.gridOptions.api.setPinnedBottomRowData(createInputFooter(TOTAL_LICENSE_CNT,TOTAL_LICENSE_COLLECTION_CNT,TOTAL_LICENSE_USE_CNT,TOTAL_LICENSE_RESIDUAL_CNT));

}
</script>