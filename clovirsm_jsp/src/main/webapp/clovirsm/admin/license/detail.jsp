<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->
<div id="mainTable" class="ag-theme-fresh" style="height: 400px; width: 100%;" ></div>
<script>
var mainTable;
var req = new Req();
$(function() {

	var columnDefs = [{headerName : "<spring:message code="NC_LICENSE_LICENSE_KEY" text="" />",field : "LICENSE_KEY"},
		{headerName : "<spring:message code="NC_LICENSE_LICENSE_NAME" text="" />",field : "LICENSE_NAME", width:300, valueGetter:function(params){
			return params.data.LICENSE_NAME + (params.data.COST_UNIT ? "(" +  params.data.COST_UNIT   + ")" :""); 
		}},
		{headerName : "<spring:message code="NC_LICENSE_LICENSE_TOTAL" text="LICENSE_TOTAL" />", cellStyle:{'text-align':'right'}, width:100,valueFormatter:function(params){
			return formatNumber(params.value);
		}
		,field : "LICENSE_TOTAL"},
		{headerName : "<spring:message code="NC_LICENSE_LICENSE_USED" text="LICENSE_USED" />",width:100, cellStyle:{'text-align':'right'}, valueFormatter:function(params){
			return formatNumber(params.value);
		}
		,field : "LICENSE_USED"},
		{headerName : "<spring:message code="NC_LICENSE_LICENSE_LEFT" text="" />",width:100, cellStyle:{'text-align':'right'}, valueFormatter:function(params){
			return formatNumber(params.value);
		}
		,field : "LICENSE_LEFT"},
		{headerName : "<spring:message code="NC_DC_DC_ID" text="" />",width:100, field : "DC_NM"}
		]
	var
	gridOptions = {
		hasNo : true,
		columnDefs : columnDefs,
		//rowModelType: 'infinite',
		pinnedBottomRowData:createFooter(0),
		rowSelection : 'single',
		sizeColumnsToFit: true,
		cacheBlockSize: 100,
		rowData : [],
		enableSorting : true,
		enableColResize : true,
		enableServerSideSorting: false
	}
	mainTable = newGrid("mainTable", gridOptions);

	search();
});
function search() {
	req.search('/api/license/list' , function(data) {
		 
		mainTable.setData(data);
		license_total = 0;
		license_used = 0;
		license_left = 0;
		for(var i=0; i < data.list.length; i++)
		{
			if(data.list[i].LICENSE_TOTAL != null && data.list[i].LICENSE_TOTAL != "")
				license_total += data.list[i].LICENSE_TOTAL;
			if(data.list[i].LICENSE_USED != null && data.list[i].LICENSE_USED != "")
				license_used += data.list[i].LICENSE_USED;
			if(data.list[i].LICENSE_LEFT != null && data.list[i].LICENSE_LEFT != "")
				license_left += data.list[i].LICENSE_LEFT;
		}

		mainTable.gridOptions.api.setPinnedBottomRowData(createFooter(license_total,license_used,license_left));
	});
}
</script>