<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%
String DC_ID = request.getParameter("DC_ID");
request.setAttribute("DC_ID", DC_ID == null ? "null" : "'" + DC_ID + "'");
%>
<style>
#mainTable { height:calc(100vh - 220px); }
#subTable{
	border-bottom: 1px solid #626262;
	margin-bottom: 20px;
}
</style>
<!-- <div id="search_area" class="search-panel panel panel-default"> -->
<!-- 	<div class="panel-body"> -->

		<!-- 조회의 id는 S_를 붙인다. -->
		<%-- <div class="col col-sm">
			<fm-input id="LICENSE_KEY" name="LICENSE_KEY" title="<spring:message code="NC_LICENSE_LICENSE_KEY" text="" />"></fm-input>
		</div> --%>
<!-- 		<div class="col col-sm"> -->
<!-- 			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="DC_ID" -->
<!-- 				keyfield="DC_ID" titlefield="DC_NM" emptystr="" -->
<%-- 				name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>"></fm-select> --%>
<!-- 		</div> -->
<!-- 		<div class="col btn_group nomargin"> -->
<%-- 			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text=""/></fm-sbutton> --%>
<!-- 		</div> -->
<!-- 		<div class="col btn_group_under"> -->
<%-- 			<fm-sbutton cmd="search" class="exeBtn contentsBtn tabBtnImg collection" onclick="sync()"  ><spring:message code="btn_sync" text="동기화"/></fm-sbutton> --%>
<!-- 		</div> -->
<!-- 	</div> -->
<!-- </div> -->
<div class="fullGrid" id="input_area">

	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab1" data-toggle="tab"  onclick="chgTab(1)"><spring:message code="" text="요약"/></a>
		</li>
		<li>
			<a href="#tab2" data-toggle="tab"  onclick="chgTab(2)"><spring:message code="" text="입력"/></a>
		</li>
		<li>
			<a href="#tab3" data-toggle="tab"  onclick="chgTab(3)"><spring:message code="" text="수집"/></a>
		</li>
	</ul>
	
	<div class="tab-content ">
		<div class="form-panel detail-panel tab-pane active" id="tab1">
			<jsp:include page="summary.jsp"></jsp:include>
		</div>
		<div class="form-panel detail-panel tab-pane" id="tab2" style="height: 340px;">
			<div class="tab-buttons">
				<button type="button" title="<spring:message code="btn_new" text="신규"/>" onclick="inputTableDel();" class="exeBtn contentsBtn tabBtnImg del"><spring:message code="btn_del" text="삭제"/></button>
				<button type="button" title="<spring:message code="btn_work" text="저장"/>" onclick="inputTableSave();" class="exeBtn contentsBtn tabBtnImg save "><spring:message code="btn_save" text="저장"/></button>
				<button type="button" title="<spring:message code="btn_new" text="신규"/>" onclick="inputTableAdd();" class="exeBtn contentsBtn tabBtnImg new"><spring:message code="btn_new" text="신규"/></button>
			</div>
			<jsp:include page="input.jsp"></jsp:include>
		</div>
		<div class="form-panel detail-panel tab-pane" id="tab3" >
			<div class="tab-buttons">
				<fm-sbutton cmd="search" class="exeBtn contentsBtn tabBtnImg sync" onclick="sync()"  ><spring:message code="btn_sync" text="동기화"/></fm-sbutton>
			</div>
			<jsp:include page="detail.jsp"></jsp:include>
		</div>
	</div>
</div>
<script>
var req = new Req();
var mainTable;
var license_total = 0;
var license_used = 0;
var license_left = 0;


$(function() {
});
function initFormData() {

}
//동기화
function sync() {
	post('/api/license/sync',{}, function(data){
		alert(msg_complete);
		search() ;
	} )
}
// 조회
var tabIdx = 1;

function chgTab(idx)
{
	tabIdx = idx;
}

function createFooter(license_total,license_used,license_left,dc_nm) {
	var result= [];
	result.push({LICENSE_KEY:'<spring:message code="total_count" text="전체" />', LICENSE_NAME:'',LICENSE_TOTAL: license_total, LICENSE_USED: license_used,LICENSE_LEFT: license_left,DC_NM:''});
	return result;
}
function createInputFooter(TOTAL_LICENSE_CNT,TOTAL_LICENSE_COLLECTION_CNT,TOTAL_LICENSE_USE_CNT,TOTAL_LICENSE_RESIDUAL_CNT) {
	var result= [];
	result.push({LICENSE_NAME:'<spring:message code="total_count" text="전체" />', LICENSE_CNT: TOTAL_LICENSE_CNT, LICENSE_COLLECTION_CNT: TOTAL_LICENSE_COLLECTION_CNT,LICENSE_USE_CNT: TOTAL_LICENSE_USE_CNT,LICENSE_RESIDUAL_CNT:TOTAL_LICENSE_RESIDUAL_CNT});
	return result;
}
// 엑셀 내보내기
function exportExcel() {
	mainTable.exportCSV({fileName:'license'}) 
}

</script>


