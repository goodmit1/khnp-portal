<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  서버 목록 조회 -->
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col btn_group rt nomargin">
			<fm-sbutton id="insert_diskTypeMng_button" cmd="update" class="newBtn contentsBtn tabBtnImg top save" onclick="insertDiskType()" ><spring:message code="btn_new"  text="신규"/></fm-sbutton>
			<fm-sbutton id="save_diskTypeMng_button"   cmd="update" class="saveBtn contentsBtn tabBtnImg top save" onclick="diskTypeMngSave()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:540px">
	<div class="table_title">
		<spring:message code="count" text="건수"/> : <span id="diskTypeGrid_total">0</span>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div>
	</div>
	<div id="diskTypeGrid" class="ag-theme-fresh" style="height: 550px" ></div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>
	var diskTypeMngReq = new Req();
	diskTypeGrid;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_DISK_TYPE_DISK_TYPE_NM" text="" />", field : "DISK_TYPE_NM"},
		                  {headerName : "<spring:message code="NC_DISK_TYPE_DEL_YN" text="삭제여부" />", field : "DEL_YN_NM"},
		                  {headerName : "<spring:message code="NC_DISK_TYPE_DD_FEE" text="" />", cellStyle:{'text-align':'right'}, valueFormatter:function(params){
								return formatNumber(params.value);
							}
							, field : "DD_FEE"},
		                  {headerName : "<spring:message code="NC_DISK_TYPE_INS_TMS" text="" />", field : "INS_TMS",
		                	  valueGetter: function(params) {
					    	  	return formatDate(params.data.INS_TMS,'datetime')
							  }}
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				var arr = diskTypeGrid.getSelectedRows();
				diskTypeMngReq.getInfo('/api/disk_type_mng/info?DISK_TYPE_ID='+ arr[0].DISK_TYPE_ID, onRowClick);

			}
		}
		diskTypeGrid = newGrid("diskTypeGrid", gridOptions);
		diskTypeMngSearch();
	});

	// 조회
	function diskTypeMngSearch() {
		diskTypeMngReq.search('/api/disk_type_mng/list' , function(data) {
			diskTypeGrid.setData(data);
			var i = diskTypeGrid.getRow(0);
			if(i){
				diskTypeMngReq.getInfo('/api/disk_type_mng/info?DISK_TYPE_ID='+ i.data.DISK_TYPE_ID, onRowClick);
			}
		});
	}

	function onRowClick(data){
		setButtonClickable('save_diskTypeMng_button', true);
		setButtonClickable('delete_diskTypeMng_button', true);
	}

	//저장
	function diskTypeMngSave(){
		 
			diskTypeMngReq.save('/api/disk_type_mng/save', function(){
				diskTypeMngSearch();
				alert('<spring:message code="saved" text="저장되었습니다"/>');
			});
		 
	}

	function insertDiskType(){
		form_data.DISK_TYPE_ID = ""
		form_data.HH_FEE       = ""
		form_data.DD_FEE       = ""
		form_data.FEE_UNIT     = ""
		form_data.DISK_TYPE_NM = ""
		form_data.MM_FEE       = ""
		form_data.INS_TMS      = ""
		form_data.DEL_YN       = "N"
	}

	// 엑셀 내보내기
	function exportExcel(){
		diskTypeGrid.exportCSV({fileName:'diskType'})
		 
	}

</script>


