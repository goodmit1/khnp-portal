<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<div class="table_title layout name">
				<spring:message code="NC_DC_PROP_TITLE" text="기타" /> <span> ( <spring:message code="count" text="건수"/> : <span id="diskTable_total">0</span> ) </span>
				<div class="btn_group">
				 		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel(propTable, 'Etc_prop')" class="excelBtn layout" id="excelBtn"></button>
				</div>

	</div>

	<div id="propTable" class="ag-theme-fresh" style="height: 200px" ></div>
	<script>
		var osTable;

		function getPropData(param)
		{
			post('/api/dc_mng/list/prop', param, function(data){
				propTable.setData(data);
			})
		}
		$(function() {


			var columnDefs = [{headerName : "<spring:message code="NC_DC_PROP_PROP_NM" text="" />",field : "PROP_NM_TITLE"},
				{headerName : "<spring:message code="NC_DC_PROP_PROP_VAL" text="경로" />",field : "PROP_VAL", editable:true,
				cellEditor: "select",
	            cellEditorParams: {
	                values: []
	            }},

				];
			var
			gridOptions = {

				editable:true,
				hasNo : true,
				columnDefs : columnDefs,
				//rowModelType: 'infinite',
				//rowSelection : 'single',
				sizeColumnsToFit: true,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false,
				onCellEditingStarted: function(event) {
					 
					if(event.column.colId=='PROP_VAL')
					{
						$("#propTable div.ag-cell-value[col-id='PROP_VAL'] select").select2({

						  data : objList[event.data.PROP_TYPE_NM.trim()],
						  tags: true,

						  createTag: function (params) {
							    return {
							      id: params.term,
							      text: params.term,
							      newOption: true
							    }
							  }})
					}
			    },
			}
			propTable = newGrid("propTable", gridOptions);


		});
	</script>