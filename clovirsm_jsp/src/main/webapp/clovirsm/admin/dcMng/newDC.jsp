<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
String popupid = request.getParameter("popupid");
request.setAttribute("popupid", popupid);
%>
<fm-modal id="${popupid}" title="<spring:message code="new_DC" text="신규 데이터 센터"/>" cmd="header-title">
	<span slot="footer">
		<span style="float: left;"><spring:message code="new_dc_save_guide" text="저장 시 몇 분 정도 소요될 수 있습니다."/></span><input type="button" class="btn ${popupid}_save_btn popupBtn finish top5" value="<spring:message code="btn_save" text="저장"/>" onclick="${popupid}_save()" />
	 
	</span>
	<div class="form-panel detail-panel">
	<form id="${popupid}_form" action="none">
		<div class="panel-body" style="height:300px;overflow:auto">
					<div class="col col-sm-6">
						<fm-input id="new_DC_NM" name="DC_NM" required="true" placeholder="<spring:message code="internal_dc_nm" text="내부 사용 데이터 센터명" />" title="<spring:message code="NC_DC_DC_NM" text="데이터 센터" />"></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-select url="/api/code_list?grp=HYPERVISOR" id="new_HV_CD"
								name="HV_CD" title="<spring:message code="NC_DC_HV_CD" text="Hypervisor코드"/>"></fm-select>
					</div>
					<div class="col col-sm-12">
						<fm-input id="new_CONN_URL" name="CONN_URL" required="true"   placeholder="https://<ip>/sdk"  title="<spring:message code="NC_DC_CONN_URL" text="CONN_URL" />"></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-input id="new_CONN_USERID" name="CONN_USERID" required="true"  title="<spring:message code="NC_DC_CONN_USERID" text="CONN_USERID" />"></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-password id="new_CONN_PWD" name="CONN_PWD" required="true"  title="<spring:message code="NC_DC_CONN_PWD" text="CONN_PWD" />"></fm-password>
					</div>
					<div class="col col-sm-6">
						<fm-input id="new_REAL_DC_NM" name="REAL_DC_NM" required="true"  title="<spring:message code="NC_DC_REAL_DC_NM" text="REAL_DC_NM" />" placeholder="<spring:message code="vcenter_dc_nm" text="vCenter에 등록된 데이터 센터명" />"></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-select url="/api/code_list?grp=db.com.clovirsm.common.Component.selectDiskType" id="new_IMG_DISK_TYPE_ID" name="IMG_DISK_TYPE_ID" required="true" label_style="max-width: 150px;"  title="<spring:message code="NC_DC_IMG_DISK_TYPE_ID" text="템플릿 디스크종류" />"></fm-select>
					</div>
		</div>			
	</form>
	</div>
	<script>
		var ${popupid}_vue = makePopupVue("${popupid}", "${popupid}", {HV_CD:'V',DEL_YN:'N'});
		function ${popupid}_click(thisvue, param){
			${popupid}_vue.callback=thisvue.callback;
			return true;
		}
		function ${popupid}_save()
		{
			if(!validate("${popupid}_form")) return false;
			
			post('/api/dc_mng/save', ${popupid}_vue.form_data, function(data){
				
				var data1 = ${popupid}_vue.form_data;
				data1.DC_ID=data.DC_ID;
				if( ${popupid}_vue.callback) eval( ${popupid}_vue.callback + '(data1);');
				$("#${popupid}").modal('hide');
			});
		}
	</script>
</fm-modal>
	