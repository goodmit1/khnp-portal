<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="S_DC_ID"
				keyfield="DC_ID" titlefield="DC_NM"
				name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_CPU" name="CPU" title="CPU"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_MEM" name="MEM" title="Memory"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_NET" name="NET" title="Network"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_DAY" name="DAY" required="true" placeholder="<spring:message code="label_day" text="일"/>" title="<spring:message code="label_term" text="기간"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<input type="radio" name="ANDOR" id="ANDOR" value="AND">AND <input type="radio" id="ANDOR" name="ANDOR" value="OR" checked>OR
		</div>
		<div class="col btn_group">
			<input type="button" class="btn searchBtn" onclick="search()" value="<spring:message code="btn_search" text="" />" />
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title">
	<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="amount" text="총 개수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="excelBtn" ></button>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 600px;" ></div>
	<br />
</div>
<script>
	var notusevmReq = new Req();
	mainTable;
	function initFormData(){
		search_data.CPU=0;
		search_data.MEM=0;
		search_data.NET=0;
		search_data.DAY=30;
	}
	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_VM_VM_NM" text="" />", field : "VM_NM"},
		                  {headerName : "<spring:message code="INS_ID" text="" />", field : "INS_ID_NM"},
		                  {headerName : "<spring:message code="NC_VM_USER_TEAM_NM" text="" />", field : "TEAM_NM"},
		                  {headerName : "IP",field : "PRIVATE_IP"},
		                  {headerName : "CPU(%)",field : "cpu_usage_max"},
		                  {headerName : "Memory(%)",field : "mem_usage_max"},
		                  {headerName : "Network(Kbps)",field : "net_usage_max"},
		                   
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		mainTable = newGrid("mainTable", gridOptions);
		 
	});

	// 조회
	function search() {
		search_data.ANDOR=$("#ANDOR:checked").val();
		notusevmReq.search('/api/not_use_vm/list/minuse/', function(data){mainTable.setData(data)});
	}

	 

	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'NOT_USE_VM.csv'})
	}

</script>