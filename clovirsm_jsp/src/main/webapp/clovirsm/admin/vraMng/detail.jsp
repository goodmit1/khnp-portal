<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- 입력 Form -->

<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body" >
			<div class="col col-sm-12">
				<fm-output id="CATALOG_NM" name="CATALOG_NM"   title="<spring:message code="NC_VRA_CATALOG_CATALOG_NM" text="스냅샷명" />"></fm-output>
			</div>
			<div class="col col-sm-6">
				<div class="input-popup inline hastitle" style="width: 100%;">
					<label for="ICON_upload" class="control-label grid-title value-title"><spring:message code="CM_TMPL_ICON" text="아이콘" /></label>
					<div class="input-group input">
						<input type="file" id="ICON_upload" accept="image/*" name="ICON_upload" class="form-control input hastitle" onchange="uploadFile()" style="width: 285px;">
						<input type="hidden" id="ICON" name="ICON" :value="form_data.ICON">
						<img id="ICON_p" style="height: 30px;">
						<button type="button" style=" min-width: 79px !important; right: 0px; top: 2px !important;" onclick="iconCreate();" class="contentsBtn tabBtnImg save top5" >아이콘생성</button>
					</div>
				</div>
			</div>
			<div class="col col-sm-6">
				<fm-input id="ICON_COLOR" name="ICON_COLOR" title="<spring:message code="CM_TMPL_ICON_COLOR" text="아이콘 배경 색상" />" onfocusout="updateColor()"></fm-input>
			</div>
			<div class="col col-sm-3">
				<fm-input id="FEE" name="FEE" title="<spring:message code="label_VRA_FEE" text="FEE" />"></fm-input>
			</div>
			<div class="col col-sm-3">
				<fm-input id="DEPLOY_TIME" name="DEPLOY_TIME" title="<spring:message code="DEPLOY_TIME" text="소요시간" />"></fm-input>
			</div>
			<div class="col col-sm-6	">
				<fm-select id="ALL_USE_YN" name="ALL_USE_YN"  url="/api/code_list?grp=sys.yn"  title="<spring:message code="NC_VRA_CATALOG_ALL_USE_YN" text="ALL_USE_YN" />"></fm-select>
			</div>
		 	<div class="col col-sm-12" id="TEAM_sel" v-if="form_data.ALL_USE_YN=='N'">
				
				<fm-select2 id="CATEGORY_IDS" keyfield="CATEGORY_ID" titlefield="CATEGORY_NMS"  name="CATEGORY_IDS"  url="/api/code_list?grp=dblist.com.clovirsm.common.Component.listLeafCategory" multiple="true"    title="<spring:message code="TASK" text="업무"/>"></fm-select2>
			</div>
			
	</div>
		<fm-popup-button popupid="iconCreate_popup" popup="/clovirsm/popup/iconCreate_popup.jsp"  style="display:none;" param="param" callback="search()"></fm-popup-button>
	
</div>
<script>
function uploadFile() {
	var file = $("#ICON_upload")[0].files[0];
	var reader = new FileReader();
	
	reader.addEventListener("load", function () {
		inputvue.form_data.ICON = reader.result;
		$("#ICON_p").prop("src", inputvue.form_data.ICON);
	}, false)
	if (file) {
		reader.readAsDataURL(file);
		
	}
}

</script>