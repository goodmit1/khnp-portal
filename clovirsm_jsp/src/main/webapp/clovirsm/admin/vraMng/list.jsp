<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
#mainTable { height:calc(100vh - 220px); }
.padding-top1{padding-top: 1px;}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->
		<!-- 
		<div class="col col-sm">
				<fm-select2 url="/api/etc/team_list" id="S_TEAM_CD" emptystr="&nbsp" class="select2"
							name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_NM" text="팀"/>"></fm-select2>
		</div>
		 -->
		<div class="col col-sm">
				<fm-input id="CATALOG_NM" name="CATALOG_NM" title="<spring:message code="NC_VRA_CATALOG_CATALOG_NM" text="" />"></fm-input>
		</div>
		<div class="col col-sm" style="width: 267px;">
			<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM" input_div_class="padding-top1" title="<spring:message code="TASK"/>" class="inline">
				<fm-popup-button popupid="s_category" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
			</fm-ibutton>
			<input type="text" style="display:none" id="S_CATEGORY" name="CATEGORY_ID">
		</div>
		<div class="col col-sm">
				<fm-select2 url="" id="S_CONN_ID" keyfield="CONN_ID" titlefield="CONN_NM" emptystr="&nbsp" class="select2"
							name="CONN_ID" title="<spring:message code="" text="VRA 연결"/>"></fm-select2>
		</div>
		<div class="col col-sm">
			<div class="">
			 	<label class="checkbox-inline" style="padding-left: 0px;"><spring:message code="NEW_YN" text="신규 여부" /></label>
				<input type="checkbox" id="NEW_CATALOG" name="NEW_CATALOG" style="position: relative; top: 4px;" onchange="checkboxChange()"/> 
			 </div>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>
		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
		<div class="table_title layout name">
					<%-- <spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>
					<div class="btn_group">
						<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
					</div> --%>
			<div class="search_info">
				<spring:message code="mainTable_Search_information" text="검색정보" />
				<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
			</div>
			<div class="btn_group">
				<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
				<fm-sbutton cmd="update" class="exeBtn contentsBtn tabBtnImg collection" onclick="collect()"   ><spring:message code="btn_COLLECT" text="수집"/></fm-sbutton>
				<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg save" onclick="save()"   ><spring:message code="btn_save" text="저장"/></fm-sbutton>
			</div>
		</div>
		<div class="layout background mid">
			<div id="mainTable" class="ag-theme-fresh" style="height: 500px" ></div>
		</div>
	<jsp:include page="detail.jsp"></jsp:include>
	</div>
</div>
<script>
	var req = new Req();
	var mainTable;

	
	$(function() {

		var columnDefs = [{headerName : "<spring:message code="NC_VRA_CATALOG_CATALOG_NM" text="" />",field : "CATALOG_NM", width:200},
			{headerName : "<spring:message code="VER" text="Ver" />",field : "VER", width:50},
			{headerName : "<spring:message code="NC_VRA_CATALOG_CATALOG_CMT" text="" />",field : "CATALOG_CMT"},
			{headerName : "<spring:message code="" text="VRA 연결정보" />",field : "CONN_NM"},
			{headerName : "<spring:message code="TASK_CNT" text="업무 갯수" />",field : "CATEGORY_CNT",   width:200},
			{headerName : "<spring:message code="label_VRA_FEE" text="FEE" />", width:80, format:'number', field : "FEE",headerTooltip:"<spring:message code="tooltip_vra_fee" text="VM과 별도로 최초 한번만 부과되는 비용" />"},
			{headerName : "<spring:message code="NC_VRA_CATALOG_ALL_USE_YN" text="" />", width:80,field : "ALL_USE_YN_NM"},
			]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			 enableServerSideSorting: false,
			 onSelectionChanged : function() {
					var arr = mainTable.getSelectedRows();
					req.getInfo('/api/vra_catalog_mng/info?CATALOG_ID='
							+ arr[0].CATALOG_ID, function(data){
								$("#ICON_p").attr("src", data.ICON )
								$("#ICON_upload").val("");
								updateColor(data.ICON_COLOR);
								if(data.TEAM_CD && data.TEAM_CD != ''){
									form_data.TEAM_CD = data.TEAM_CD.split(",");
									$("#TEAM_CD").val(form_data.TEAM_CD)
								}
								else{
									$("#TEAM_CD").val("");
								}
								if(data.CATEGORY_IDS && data.CATEGORY_IDS != ''){
									 
									form_data.CATEGORY_IDS = data.CATEGORY_IDS.split(",");
									$("#CATEGORY_IDS").val(form_data.CATEGORY_IDS)
								}
								else{
									 
									$("#CATEGORY_IDS").val("");
								}
							});
				},
		}
		mainTable = newGrid("mainTable", gridOptions);
		connData();
		search();
	});
	function initFormData()
	{
		<c:if test="${!empty param.NEW_CATALOG}">
			$("#NEW_CATALOG").attr("checked", true);
			search_data.NEW_CATALOG = ${param.NEW_CATALOG};
		</c:if>
	}
	function connData(){
		post("/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_ETC_CONN_NM",{"CONN_TYPE":"VRA"},function(data){
			fillOptionByData('S_CONN_ID', data, '', "CONN_ID", "CONN_NM");
		});
	}
	//검색 카테고리
	function select_S_CATEGORY(data){
		searchvue.form_data.CATEGORY_ID = data.CATEGORY;
		searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}
	//입력 카테고리
	function select_CATEGORY(data){
		inputvue.form_data.CATEGORY = data.CATEGORY;
		inputvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#CATEGORY_NM").val(data.CATEGORY_NM);
	}
	// 조회
	function search() {


		req.search('/api/vra_catalog_mng/list' , function(data) {
			mainTable.setData(data);
		});
	}
	// 수집
	function collect() {


		post('/api/vra_catalog_mng/collect',{}, function() {
			search();
		});
	}
	// 저장
	function save() {
		var arr = mainTable.getSelectedRows();
		if(arr.length > 0){
			req.save('/api/vra_catalog_mng/save', function(){
				search();
			});
		} else{
			alert("<spring:message code="select_first" text="" />")
		}
	}
	// 엑셀 내보내기
	function exportExcel()
	{
		  mainTable.exportCSV({fileName:'vra'})
	}
	
	function checkboxChange(){
		req.setSearchData({NEW_CATALOG : $("#NEW_CATALOG").is(":checked")});
	}
	function updateColor(ICON_COLOR){
		var color = '';
		 
		if(ICON_COLOR == null){
			ICON_COLOR = $("#ICON_COLOR").val();
		} 
		$("#ICON_p").css("background-color", ICON_COLOR);
	}
	function iconCreate(){
		var arr = mainTable.getSelectedRows();
		if(arr.length > 0){
			param = { "CATEGORY_IDS" : $("#CATEGORY_IDS").val(), "CATALOG_ID" : arr[0].CATALOG_ID, "action" : "CATALOG"};
			$('#iconCreate_popup_button').trigger('click');
		} else{
			alert(msg_select_first);
		}
	}
</script>


