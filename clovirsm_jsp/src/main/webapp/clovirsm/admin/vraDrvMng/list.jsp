<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%request.setAttribute("fields",  "cpu,memory,apps,image,os_param,multi_disk,_purpose" ); %>
<style>
#mainTable { height:calc(100vh - 220px); }
.padding-top1{padding-top: 1px;}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
				<fm-input id="S_DRV_NM" name="DRV_NM" title="<spring:message code="NC_VRA_CATALOG_CATALOG_NM" text="" />"></fm-input>
		</div>
		<div class="col col-sm" style="width: 267px;">
			<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM" input_div_class="padding-top1" title="<spring:message code="TASK"/>" class="inline">
				<fm-popup-button popupid="s_category" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
			</fm-ibutton>
		</div>
		<div class="col col-sm">
				<fm-select2 url="" id="S_CONN_ID" keyfield="CONN_ID" titlefield="CONN_NM" emptystr="&nbsp" class="select2"
							name="CONN_ID" title="<spring:message code="" text="VRA 연결"/>"></fm-select2>
		</div>
		<div class="col col-sm" >
				<fm-input id="S_FIXED_JSON" name="FIXED_JSON" title="JSON" input_style="width:300px"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>
		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
		<div class="table_title layout name">
			<div class="search_info">
				<spring:message code="mainTable_Search_information" text="검색정보" />
				<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
			</div>
			<div class="btn_group">
				<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
				<fm-sbutton class="contentsBtn tabBtnImg save" onclick="newDrvJson();" cmd="update">JSON일괄수정</fm-sbutton>
				<fm-sbutton class="contentsBtn tabBtnImg save" onclick="newDrv();" cmd="update"><spring:message code="btn_new" text="신규"/></fm-sbutton>
				<fm-popup-button popupid="vra_category_drv_popup" popup="/clovirsm/popup/vra_category_drv_popup.jsp" style="display:none;" param="param" callback="search();"></fm-popup-button>
				<fm-popup-button popupid="vra_category_JSON_popup" popup="/clovirsm/popup/vra_category_JSON_popup.jsp" style="display:none;" param="paramArray" callback="search();"></fm-popup-button>
			</div>
		</div>
		<div class="layout background mid">
			<div id="mainTable" class="ag-theme-fresh" style="height: 500px" ></div>
		</div>
	</div>
</div>
<script>
	var req = new Req();
	var mainTable;
	var param = {};
	var paramArray = new Array();
	
	$(function() {

		var columnDefs = [
			{width: 52, minWidth: 52,maxWidth: 52,   checkboxSelection: true, 
			    headerCheckboxSelection: true, headerCheckboxSelectionFilteredOnly:true },
			{headerName : "<spring:message code="NC_VRA_CATALOG_CATALOG_NM" text="" />",field : "DRV_NM", width:200, cellRenderer:function(params){
				return '<a href="#" onclick=DrvOpen("' + params.data.DRV_ID +'");return false>' + params.value + '</a>';
			}},
			{headerName : "<spring:message code=" " text="원본 카탈로그" />",field : "CATALOG_NM", width:200},
			{headerName : "<spring:message code="TASK" text="업무" />",field : "CATEGORY_NMS",   width:200},
			{headerName : "<spring:message code="" text="VRA 연결정보" />",field : "CONN_NM",   width:200},
			<c:forTokens  var="name" items="${fields}" delims=",">

				{headerName : "${name}", width:150, field : "${name}"},

			</c:forTokens>

 
			
			 
			{ hide:true ,field : "DRV_ID"}
			]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			rowSelection:'multiple',
			checkboxSelection: true,
			suppressRowClickSelection: true,
			enableColResize : true,
		    enableServerSideSorting: false
		}
		mainTable = newGrid("mainTable", gridOptions);
		connData();
		search();
	});
	//검색 카테고리
	function select_S_CATEGORY(data){
		searchvue.form_data.CATEGORY_ID = data.CATEGORY;
		searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}
	function connData(){
		post("/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_ETC_CONN_NM",{"CONN_TYPE":"VRA"},function(data){
			fillOptionByData('S_CONN_ID', data, '', "CONN_ID", "CONN_NM");
		});
	}
	// 조회
	function search() {
		search_data.JSON_PARAMS="${fields}";
		req.search('/api/vra_drv/list' , function(data) {
			mainTable.setData(data);
		});
	}
	
	function newDrv(){
		param = new Object();
		param.DRV_ID='';
		param.DRV_NM='';
		param.action = "new";
		$('#vra_category_drv_popup_button').trigger('click');
	}
	function DrvOpen(id){
		 
		req.getInfo('/api/vra_drv/info?DRV_ID='	+ id, function(data){
				param = data;
				param.action = "update";
				 
				$('#vra_category_drv_popup_button').trigger('click');
		});
	}
	function newDrvJson(){
		var arr = mainTable.getSelectedRows();
		if(arr.length > 0){
			paramArray = arr;
			$('#vra_category_JSON_popup_button').trigger('click');
		}
		else
			alert("<spring:message code="select_first" text="" />");
	}
	// 엑셀 내보내기
	function exportExcel()
	{
		  mainTable.exportCSV({fileName:'vra'})
	}
	
</script>


