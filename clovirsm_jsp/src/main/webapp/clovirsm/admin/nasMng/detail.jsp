<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
	<!-- 입력 Form -->
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String direct = request.getParameter("direct");
	request.setAttribute("direct", direct);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
	
%>	



	<div class="form-panel detail-panel panel panel-default">

			<div class="panel-body" id="inputForm" >

					<div class="col col-sm-4">
						<fm-input id="NAS_IP" name="NAS_IP" required="true"  title="<spring:message code="NC_NAS_IP" text="NC_NAS_IP" />"></fm-input>
					</div>
					<div class="col col-sm-4">
						<fm-input id="NAS_PATH" name="NAS_PATH" required="true"  title="<spring:message code="NC_NAS_PATH" text="NAS 경로" />"></fm-input>
					</div>
					<div class="col col-sm-4">
						<fm-spin id="SIZE" title="<spring:message code="NC_NAS_VOLUME_SIZE" text="볼륨용량" />" name="SIZE" required="true" >
							<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NAS_UNIT_CD" id="S_TASK_STATUS_CD"
							keyfield="TASK_STATUS_CD" titlefield="TASK_STATUS_CD_NM" emptystr=" "
							name="UNIT" label_style="display: inline-block;width: 20%;"></fm-select>
						</fm-spin>
					</div>
					<div class="col col-sm-4">
						<fm-select url="/api/etc/team_list" id="TEAM_CD" title="<spring:message code="팀" text="팀"/>"
						keyfield="FM_TEAM_TEAM_CD" titlefield="FM_TEAM_TEAM_CD" emptystr=" "
						name="TEAM_CD"></fm-select>
					<%-- 	<fm-select2 url="/api/etc/team_list" id="TEAM_CD" emptystr=" "
								name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀"/>"></fm-select2> --%>
					</div>
					<div class="col col-sm-4" id="user_input_y">
						<fm-input id="NAS_DESC" name="NAS_DESC" title="<spring:message code="label_note" text="비고"/>"></fm-input>
 					</div>
			</div>

	</div>
	
<script>
				
</script>
