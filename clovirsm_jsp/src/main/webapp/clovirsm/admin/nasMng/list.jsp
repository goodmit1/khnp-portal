<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
#mainTable { height:calc(100vh - 220px); }

#DISK_SIZE,#CPU_CNT,#RAM_SIZE { width: 100px !important; }
#DD_FEE { width: 100px !important; }
#SIZE{
	width: 48% !important;
}
#inputForm > div:nth-child(3) > div > div > div.fm-select{
	width: 40%;
    display: inline-block;
}
#inputForm > div:nth-child(4) > div > span{
	 width: calc(100% - 160px) !important;
}
#inputForm > div:nth-child(4) > div > span > span.selection > span{
	border-radius: 0;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->
			<div class="col col-sm">
					<fm-input id="S_USER_NM" name="USER_NM" title="<spring:message code="NC_NAS_APPR_USER_NM" text="신청자" />"></fm-input>
			</div>
			<div class="col col-sm">
					<fm-input id="S_NAS_IP" name="NAS_IP" title="<spring:message code="NC_NAS_IP" text="NAS IP" />"></fm-input>
			</div>
			<div class="col col-sm">
					<fm-input id="S_NAS_PATH" name="NAS_PATH" title="<spring:message code="NC_NAS_PATH" text="NAS 경로" />"></fm-input>
			</div>
		<div class="col btn_group">

			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text=""/></fm-sbutton>
		</div>
		<div class="col btn_group_under">
			<fm-sbutton cmd="update" class="newBtn" onclick="insert()"   ><spring:message code="btn_new" text="신규"/></fm-sbutton>
			<fm-sbutton cmd="delete" class="delBtn" onclick="del()"   ><spring:message code="btn_del" text="삭제"/></fm-sbutton>
			<fm-sbutton cmd="update" class="saveBtn" onclick="save()"   ><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
	


</div>
<div class="fullGrid" id="input_area">
	<div class="table_title">
				<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
				<div class="btn_group">
					<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
				</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 450px;" ></div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>
	var req = new Req();
	var mainTable;
	$(function() {
		var columnDefs = [
			{headerName : "<spring:message code="NC_NAS_IP" text="NAS IP" />",field : "NAS_IP",tooltipField:"NAS_IP"},	
			{headerName : "<spring:message code="NC_NAS_PATH" text="NAS 경로" />",field : "NAS_PATH",tooltipField:"NAS_PATH"},
			{headerName : "<spring:message code="NC_NAS_TEAM" text="팀" />",field : "TEAM_NM",tooltipField:"TEAM_NM"},
			/* {headerName : "<spring:message code="NC_NAS_APPR_USER_NM" text="신청자" />",field : "USER_NM"}, */
			{headerName : "<spring:message code="NC_NAS_VOLUME_DATE" text="볼륨생성일" />",field : "INS_TMS",
          	  valueGetter: function(params) {
		    	  	return formatDate(params.data.INS_TMS,'datetime')
			}},
			{headerName : "<spring:message code="NC_NAS_VOLUME_SIZE" text="볼륨용량" />",field : "SIZE",
				valueGetter: function(params) {
          		  var size = params.data.SIZE;
          		  var unit = params.data.NAS_UNIT;
          		  var voluesize = size+''+unit
		    		  return voluesize;
			  }},
			{headerName : "<spring:message code="label_note" text="비고"/>",field : "NAS_DESC",tooltipField:"NAS_DESC"},
			]
		
	
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowData : [],
			rowSelection : 'single',
			//rowModelType: 'infinite',
			enableSorting : true,
			enableColResize : true,
		 	enableServerSideSorting: false,
			//sizeColumnsToFit: false,
			cacheBlockSize: 100,
		 	onSelectionChanged : function() {
				var arr = mainTable.getSelectedRows();
				req.getInfo('/api/nas_mng/info?NAS_IP='
						+ arr[0].NAS_IP + '&NAS_PATH=' + arr[0].NAS_PATH);
			},
		}
		mainTable = newGrid("mainTable", gridOptions);

		search();
	});
	function pwdIsNullReset()
	{
		/* $.getJSON('/api/nas_mng/test/pwdreset',   function(data){
			 
			data.forEach(function (el) {
				post("/api/user_mng/passwordReset", el, function(data){
					 
					 
				});
			})
		}); */
	}
	// 조회
	function search() {
		$("#NAS_IP").prop("disabled", true)
		$("#NAS_PATH").prop("disabled", true)
		req.search('/api/nas_mng/list' , function(data) {
			mainTable.setData(data);
		});
	}
	
	function validateSize(id) // common.js 가져다 고쳐씀 
	{
		var arr = $("#" + id +" *[required='required']");
		var isok=true;
		var msg = '';
		for(var i=0; i < arr.length; i++)
		{
				$(arr[i]).find("input").each(function(idx){
					if($(this).val()<=0)
					{
						 
						isok = false;
						/* msg += $(arr[i]).find("label").text() + "0보다 사이즈 커야 합니다.\n"; */
						msg += $(arr[i]).find("label").text() + "<spring:message code="vmspec_warning_size" text="" />" + "\n";
					}
					
				})

		}
		if(!isok) alert(msg);
		return isok;
	}

	// 저장
	function save() {
		var ip = $('#NAS_IP').val();
		if(!validate("inputForm")) return false;
		if(!ipValidate(ip)) return false;
		//if(!validateSize("inputForm")) return false;
		req.save('/api/nas_mng/save', function(){
			search();
		});
		$("#NAS_IP").prop("disabled", true)
		$("#NAS_PATH").prop("disabled", true)
	}
	// 삭제
	function del() {
		req.del('/api/nas_mng/delete', function(){
			search();
		});
	}
	// 추가
	function insert() {
		 $("#NAS_IP").prop("disabled", false)
		 $("#NAS_PATH").prop("disabled", false)
		 form_data.NAS_IP ="";
		 form_data.NAS_PATH ="";
		 form_data.NAS_DESC="";
		 form_data.SIZE ="";
		 form_data.UNIT ="";
		 document.getElementById("NAS_IP").focus();
		 //search();
	}
	// 엑셀 내보내기
	function exportExcel()
	{
		 mainTable.exportCSV({fileName:'nas'})
		 
	}

</script>


