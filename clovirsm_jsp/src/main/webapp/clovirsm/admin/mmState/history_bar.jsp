<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div id="monitor_ds" class="chart_area col col-sm-8"></div>

<script>
var chart;
function addChart(args) {
	var list = args.LIST;
	var series = {name:args.TEAM_NM, data:[]};
	if(!list) return;

	for(var i=0;  i < list.length; i++) {
		var data1=[list[i].YYYYMM, list[i].FEE];
		series.data.push(data1);
	}
	chart.addSeries(series);
}
function getHistoryMonitorInfo(teamCd, teamNm){
	var param = {TEAM_CD: teamCd, TEAM_NM: teamNm};
// 	setDate();
	param.YYYYMM_FROM = searchvue.form_data.YYYYMM_FROM;
	param.YYYYMM_TO = searchvue.form_data.YYYYMM_TO;
	post("/api/mmpay/fee_chart_his_info", param, addChart, true);
}
 
function toggleHistory(keyword, title, idx, selected)
{
	if(!selected)
	{
		for(var i=0; i < chart.series.length; i++)
		{
			if(chart.series[i].name==title)
			{
				chart.series[i].remove();
				break;
			}
		}
	}
	else
	{
		getHistoryMonitorInfo(keyword, title);
		
	}
}
	
function setHistoryX(  S_YYYY_FROM,   S_MM_FROM,   S_YYYY_TO,   S_MM_TO)
{
	var dateFrom = new Date(S_YYYY_FROM, S_MM_FROM -1, 1);
	var dateTo = new Date(S_YYYY_TO, S_MM_TO -1, 1);
	var arr = [];
	for(var i=0; i<100; i++ )
	{
		
		arr.push(formatDatePattern(dateFrom,'yyyyMM'));
		dateFrom.setMonth(dateFrom.getMonth()+1);
		
		if(dateFrom>dateTo)
		{
			break;	
		}
	}
	chart.xAxis[0].setCategories(arr, true);
	for(var i=0; i < chart.series.length; i++)
	{
		chart.series[i].remove();
	}
}
function drawMonitorChart() {

	var config1 = {
			  chart: {
				    type: 'line'
			  },
			  title: {
			    text: '<spring:message code="month_fee_history"/>'
			  },
			  xAxis: {
			        type: 'category',
			        
			    },
			  yAxis: [
				  {
				    title: {
				      text: '<spring:message code="COL_FEE"/>(<spring:message code="label_price_unit"/>)'
				    },
				    min :0

				  }
				],
			  legend: {
			    enabled: true
			  } ,
			  series:[]
		};

	chart = Highcharts.chart('monitor_ds', config1);
}
</script>