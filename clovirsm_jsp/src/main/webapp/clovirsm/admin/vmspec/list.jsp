<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
#mainTable { height:calc(100vh - 220px); }

#DISK_SIZE,#CPU_CNT,#RAM_SIZE { width: 100px !important; }
#DD_FEE { width: 100px !important; }

</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->
			<div class="col col-sm">
					<fm-input id="S_SPEC_NM" name="SPEC_NM" title="<spring:message code="NC_VM_SPEC_SPEC_NM" text="명칭" />"></fm-input>
			</div>

			<div class="col col-sm">
					<fm-select url="/api/code_list?grp=sys.yn" id="S_DEL_YN" emptystr=" "
								name="DEL_YN" title="<spring:message code="DEL_YN" text="삭제여부"/>"></fm-select>
			</div>



		<div class="col btn_group">

			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text=""/></fm-sbutton>
		</div>
		<div class="col btn_group_under">
			<fm-sbutton cmd="update" class="newBtn" onclick="insert()"   ><spring:message code="btn_new" text=""/></fm-sbutton>
			<fm-sbutton cmd="delete" class="delBtn" onclick="del()"   ><spring:message code="btn_del" text="삭제"/></fm-sbutton>
			<fm-sbutton cmd="update" class="saveBtn" onclick="save()"   ><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="table_title">
				<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
				<div class="btn_group">
					<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="excelBtn"></button>
				</div>

	</div>

	<div id="mainTable" class="ag-theme-fresh" style="height: 450px" ></div>
	<jsp:include page="detail.jsp"></jsp:include>

</div>
<script>
					var req = new Req();
					var mainTable;


					$(function() {


						var columnDefs = [{headerName : "<spring:message code="NC_VM_SPEC_SPEC_NM" text="" />",field : "SPEC_NM"},
							{headerName : "<spring:message code="NC_VM_SPEC_CPU_CNT" text="" />",field : "CPU_CNT", cellStyle:{'text-align':'right'}, valueFormatter:function(params){
								return formatNumber(params.value);
							}},
							{headerName : "<spring:message code="NC_VM_SPEC_RAM_SIZE" text="" />",field : "RAM_SIZE", cellStyle:{'text-align':'right'}, valueFormatter:function(params){
								return formatNumber(params.value);
							}},
							{headerName : "<spring:message code="NC_VM_SPEC_DISK_SIZE" text="" />",field : "DISK_SIZE", cellStyle:{'text-align':'right'}, valueFormatter:function(params){
								return formatNumber(params.value);
							}},
							{headerName : "<spring:message code="NC_VM_SPEC_DISK_TYPE_NM" text="" />",field : "DISK_TYPE_NM"},
							{headerName : "<spring:message code="NC_VM_SPEC_DD_FEE" text="" />",field : "DD_FEE", cellStyle:{'text-align':'right'}, valueFormatter:function(params){
								return formatNumber(params.value);
							}},
							]
						var
						gridOptions = {
							hasNo : true,
							columnDefs : columnDefs,
							//rowModelType: 'infinite',
							rowSelection : 'single',
							sizeColumnsToFit: true,
							cacheBlockSize: 100,
							rowData : [],
							enableSorting : true,
							enableColResize : true,
							 enableServerSideSorting: false,
							 onSelectionChanged : function() {
									var arr = mainTable.getSelectedRows();
									req.getInfo('/api/vm_spec_mng/info?SPEC_ID='
											+ arr[0].SPEC_ID );
								},
						}
						mainTable = newGrid("mainTable", gridOptions);

						search();
					});
					function initFormData()
					{

					}
					// 조회
					function search() {

						req.search('/api/vm_spec_mng/list' , function(data) {
							mainTable.setData(data);
						});
					}

					function validateSize(id) // common.js 가져다 고쳐씀
					{
						var arr = $("#" + id +" *[required='required']");
						var isok=true;
						var msg = '';
						for(var i=0; i < arr.length; i++)
						{

								$(arr[i]).find("input").each(function(idx){
									if($(this).val()<=0)
									{
										 
										isok = false;
										/* msg += $(arr[i]).find("label").text() + "0보다 사이즈 커야 합니다.\n"; */
										msg += $(arr[i]).find("label").text() + "<spring:message code="vmspec_warning_size" text="" />" + "\n";
									}

								})

						}
						if(!isok) alert(msg);
						return isok;
					}

					// 저장
					function save() {

						if(!validate("inputForm")) return false;
						if(!validateSize("inputForm")) return false;

						req.save('/api/vm_spec_mng/save', function(){
							search();
						});
					}
					// 삭제
					function del() {
						req.del('/api/vm_spec_mng/delete', function(){
							search();
						});
					}
					// 추가
					function insert() {
						 req.setData({DEL_YN:'N'})
						 document.getElementById("SPEC_NM").focus();
						 search();
					}
					// 엑셀 내보내기
					function exportExcel()
					{
						 mainTable.exportCSV({fileName:'spec'})
						 // exportExcelServer("mainForm", '/api/vm_spec_mng/list_excel', 'SpecList',mainTable.gridOptions.columnDefs, req.getRunSearchData())
					}
				</script>


