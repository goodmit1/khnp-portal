<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 코드 관리 -->
<style>
div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(1) > div.ag-cell-label-container.ag-header-cell-sorted-none{
	width: auto;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다  -->
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=FAB" id="S_FAB" name="ID" title="FAB" emptystr=" ">
		</div>
		<div class="col col-sm">
			<fm-popup-button popupid="fab_popup" popup="/clovirsm/popup/fabAdd_popup.jsp" class="contentsBtn tabBtnImg new top0" style="left: 5px;"><spring:message code="label_add" text="추가"/></fm-sbutton>
		</div>		
		<div class="col btn_group nomargin" style="padding-left:5px;">
			<fm-sbutton cmd="search" class="searchBtn" onclick="adminSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area">
	<div class="box_s"> 
		<div>
			<div class="table_title layout name">관리자
				<div class="btn_group">
					<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg top del" class="btn btn-primary" onclick="fabDel()"><spring:message code="btn_del" text="삭제"/></fm-sbutton>
				</div>
			</div>
			<div id="adminTable" class="ag-theme-fresh" style="height:450px;"></div>
		</div>
	</div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>

var fabSetting = new Req();
function exportspecSetting(){
	adminTable.exportCSV({fileName:'ServerSetting.csv'})
}
function savespecSetting(){
	adminTable.adminGridOptions.api.stopEditing();
	var arr = adminTable.getData();
	arr.forEach(function(value,i){
		arr[i].IU = "U";
	})
//	for(var i in arr){
//	}
	var selected_json = JSON.stringify(arr);
	post("/api/server_setting/save_multi", {"selected_json" : selected_json}, function(){
		adminSearch();
	})
}
function adminSearch(){
	fabSetting.setSearchData({KUBUN:"FAB_ADMIN"})
	fabSetting.search('/api/fab_setting/list', function(data){
		adminTable.setData(data);
	});
}
function fabSave(){
	if(validate("contents")){
		var arr = adminTable.getData();
		var object = fabSetting.getData();
		for(var i = 0 ; i < arr.length; i++){
			if(arr[i].USER_ID == object.DC_ID && arr[i].ID == object.ID ){
				alert("<spring:message code="" text="중복된 권한이 있습니다."/>");
				return false;
			}
		}
		fabSetting.putData({KUBUN:"FAB_ADMIN"});
		fabSetting.save("/api/fab_setting/save",function(){
			alert("<spring:message code="saved" text="저장되었습니다"/>");
			adminSearch();
		});
	}
	
}
function fabDel(){
	var arr =adminTable.getSelectedRows();
	fabSetting.putData({KUBUN:"FAB_ADMIN", ID : arr[0].ID , DC_ID : arr[0].DC_ID});
	fabSetting.del("/api/fab_setting/delete",function(){
		alert("<spring:message code="deleted" text="삭제되었습니다."/>");
		adminSearch();
	});
	
	
}
function ${popupid}_select_user(args, callback){
	 
	form_data.DC_ID = args.USER_ID;
	$('#USER_NAME').val(args.USER_NAME);
	if(callback) callback();
}
var adminTable;

$(document).ready(function(){
	var adminDefs =[
	    {
			headerName: "<spring:message code="" text="FAB" />",
			field: "DD_DESC"
		},
		{
			headerName: "<spring:message code="" text="부서" />",
			field: "TEAM_NM",
		},
		{
			headerName: "<spring:message code="" text="이름" />",
			field: "USER_NAME",
		}
	     ];
	var adminGridOptions = {
		    columnDefs: adminDefs,
		    rowData: [],
			rowSelection : 'single',
		    enableSorting: true,
		    enableColResize: true
		};
	adminTable = newGrid("adminTable", adminGridOptions)
	
	adminSearch();
});
</script>