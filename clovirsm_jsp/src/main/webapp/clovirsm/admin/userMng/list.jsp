<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<style>
	#search_area > div > div.col.col-sm-2 > div > div > div > button{
		height: 24px;
	}
	
	#input_area > div.form-panel.detail-panel.panel.panel-default > div > div:nth-child(7) > div > div > div > button{
		height: 21px;
	}
</style>

<!--	서버 목록 조회 -->
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm-2" style="width: 284px;">
			<fm-popup id="S_TEAM_NM" name="S_TEAM_NM" input_div_class="floatLeft"  title="<spring:message code="FM_TEAM_TEAM_CD" text="" />"></fm-popup>
			<input type="hidden" name="S_TEAM_CD" id="S_TEAM_CD">
		</div>
		<div class="col col-sm">
			<fm-input id="S_LOGIN_ID" name="LOGIN_ID" title="<spring:message code="FM_USER_LOGIN_ID" text="사용자" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_USER_NAME" name="USER_NAME" title="<spring:message code="FM_USER_USER_NAME" text="사용자" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=USER_TYPE" id="S_USER_TYPE"
				keyfield="USER_TYPE" titlefield="USER_TYPE_NM" emptystr=" "
				name="USER_TYPE" title="<spring:message code="FM_USER_USER_TYPE" text="권한"/>">
			</fm-select>
		</div>
		<div class="col">
			<table>
				<tr>
					<td>
						<fm-date id="S_DATE_TMS_FROM" name="DATE_TMS_FROM" title="<spring:message code="FM_USER_LAST_ACCESS_TMS" text="" />"></fm-date>
					</td>
					<td class="tilt">~</td>
					<td>
						<fm-date id="S_DATE_TMS_TO" name="DATE_TMS_TO" ></fm-date>
					</td>
				</tr>
			</table>
		</div>
		<div class="col btn_group">
			<fm-sbutton cmd="search" class="searchBtn" onclick="userSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
		<div class="col btn_group_under">
			<fm-sbutton cmd="search" class="newBtn" onclick="insertUser()"><spring:message code="btn_new" text="신규"/></fm-sbutton>
			<fm-sbutton cmd="search" class="update" onclick="saveUser()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
			<fm-sbutton cmd="search" class="delBtn" onclick="deleteUser()"><spring:message code="btn_delete" text="삭제"/></fm-sbutton>
			<fm-sbutton cmd="search" class="update" onclick="passwordReset()"><spring:message code="btn_password_reset" text="비밀번호 초기화"/></fm-sbutton>
		</div>
		<div id="popup-button-html">
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:340px">
	<div class="table_title">
		<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="excelBtn" id="excelBtn"></button>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 300px" ></div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>
	var userReq = new Req();
	mainTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_VM_TEAM_NM" text="팀" />", field : "TEAM_NM"},
						{headerName : "<spring:message code="FM_USER_LOGIN_ID" text="사용자" />", field : "LOGIN_ID"},
						{headerName : "<spring:message code="FM_USER_USER_NAME" text="사용자" />", field : "USER_NAME"},
						{headerName : "<spring:message code="FM_USER_USER_TYPE" text="권한" />", field : "USER_TYPE_NM"},
						{headerName : "<spring:message code="FM_USER_POSITION" text="직급" />", field : "POSITION"},
						{headerName : "<spring:message code="FM_USER_LAST_ACCESS_TMS" text="마지막 접속시간" />", field : "LAST_ACCESS_TMS",
							valueGetter: function(params) {
								if(params.data && params.data.LAST_ACCESS_TMS){
									return formatDate(params.data.LAST_ACCESS_TMS,'datetime')
								}
								return "";
						}}
		]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			cacheBlockSize: 100,
			rowSelection : 'single',
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: true,
			onSelectionChanged : function() {
				var arr = mainTable.getSelectedRows();
				userReq.getInfo('/api/user_mng/info?USER_ID='+ arr[0].USER_ID, function(data){
				});
			},
		}
		mainTable = newGrid("mainTable", gridOptions);
		userSearch();
	});

	// 조회
	function userSearch() {
		userReq.searchPaging('/api/user_mng/list' , mainTable);
	}

	//
	function passwordReset(){
		if(chkSelection()){
			post("/api/user_mng/passwordReset", form_data, function(data){
				userSearch();
			});
		}
	}

	function deleteUser(){
		if(chkSelection() && confirm(msg_confirm_delete)){
			userReq.del("/api/user_mng/delete", function(){
				userSearch();
				alert(msg_complete);
			})
		}
	}
	function initFormData(){
		form_data.TEAM_NM="";
		form_data.TEAM_CD="";
	}

	function insertUser(){
		form_data.USER_ID = "";
		form_data.USER_NAME = "";
		form_data.LOGIN_ID = "";
		form_data.EMAIL = "";
		form_data.USE_YN = "Y";
		form_data.POSITION = "";
		form_data.USER_TYPE = "18";
		form_data.TEAM_CD = "${_TEAM_CD_}";
		form_data.TEAM_NM = "<%= UserVO.getUser().getTeam().getTeamNm() %>";
		form_data.IDU = "I";
	}

	function saveUser(){
		if(form_data.IDU == "I" || chkSelection()){
			post("/api/user_mng/save", form_data, function(){
				alert("<spring:message code="saved" text="저장되었습니다"/>");
				userSearch();
			})
		}
	}

	function chkSelection(){
		var arr = mainTable.getSelectedRows();
		if(arr[0]){
			return true;
		}
		alert(msg_select_first);
		return false
	}

	// 엑셀 내보내기
	function exportExcel(){
		exportExcelServer("mainForm", '/api/user_mng/list_excel', 'UserList',mainTable.gridOptions.columnDefs, userReq.getRunSearchData())
	}

</script>


