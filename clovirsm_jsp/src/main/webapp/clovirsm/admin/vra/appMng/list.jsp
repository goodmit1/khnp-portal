<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>

<script src="/res/js/lib/ace/ace.js"></script>


<style>
#mainTable { height:calc(100vh - 220px); }

.shellBtn{
    border: none;
    background: #6E7783;
    color: white;
    border-radius: 5px;
    width: 45px;
    height: 25px;
}
.shellBtn:disabled {
    border: none;
    background: #bfbfbf;
    color: white;
    border-radius: 5px;
    width: 45px;
    height: 25px;
}
#S_SW_NM {
	width:200px;
}
#editor { 
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
    }
#search_area > div > div:nth-child(3) > div > div > div{
	padding-top: 1px;
}    

</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->
		 
		<div class="col col-sm">
				<fm-input id="S_SW_NM" name="SW_NM" title="Application"></fm-input>
		</div>
		<div class="col col-sm">
				<fm-select id="S_PURPOSE" name="PURPOSE" title="<spring:message code="CATEGORY" text="종류" />" url="/api/code_list?grp=S_PURPOSE" emptyStr="" ></fm-select>
				
		</div>
		<div class="col col-sm" style="width: 267px;">
			<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM" title="<spring:message code="NC_OS_TYPE_SERVICE"/>" class="inline">
				<fm-popup-button popupid="s_category" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
			</fm-ibutton>
			<input type="text" style="display:none" id="S_CATEGORY" name="CATEGORY">
		</div>
			
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>
		</div>
	
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
				<spring:message code="mainTable_Search_information" text="검색정보" />
				<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)</span>
				<div class="btn_group">
					<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn" id="excelBtn"></button>
					<fm-sbutton cmd="update" class="newBtn contentsBtn tabBtnImg new" onclick="insert()"   ><spring:message code="btn_new" text="신규"/></fm-sbutton>
					<fm-sbutton cmd="delete" class="delBtn contentsBtn tabBtnImg del" onclick="del()"   ><spring:message code="btn_del" text="삭제"/></fm-sbutton>
					<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg save" onclick="save()"   ><spring:message code="btn_save" text="저장"/></fm-sbutton>
				</div>

	</div>

	<div id="mainTable" class="ag-theme-fresh" style="height: 500px" ></div>
	
	<div class="gab"></div>
	<ul class="nav nav-tabs vmDetailTab">
		<li class="active">
			<a href="#tab1" data-toggle="tab"  onclick="chgTab(1)"><spring:message code="detail" text="상세정보"/></a>
		</li>
		<li>
			<a href="#tab2" data-toggle="tab"  onclick="chgTab(2)"><spring:message code="INSTALLED_VM" text="설치된 VM"/></a>
		</li>
		<li>
			<a href="#tab3" data-toggle="tab"  onclick="chgTab(3)"><spring:message code="SW_ENV_TAB" text="ENV설정"/></a>
		</li>
<!-- 		<li> -->
<%-- 			<a href="#tab3" data-toggle="tab"  onclick="chgTab(3)"><spring:message code="INSTALLED_VM" text="설치된 VM"/></a> --%>
<!-- 		</li> -->
	</ul>
	
	<div class="tab-content ">
		<div class="form-panel detail-panel tab-pane active" id="tab1">
			<jsp:include page="detail.jsp"></jsp:include>
		</div>
		<div class="form-panel detail-panel tab-pane" id="tab2" style="height: 340px;">
		
			<jsp:include page="vmInstalledList.jsp"></jsp:include>
		</div>
		<div class="form-panel detail-panel tab-pane" id="tab3" >
			<jsp:include page="vmEnvList.jsp"></jsp:include>
		</div>
	</div>
<fm-popup-button style="display: none;" popupid="shell_popup" popup="/clovirsm/popup/shellPopup.jsp" cmd="update" param="req.getData()"></fm-popup-button>
</div>
<script>
	var req = new Req();
	var mainTable;
	var arr ;
	$(function() {

		var columnDefs = [
			{headerName : "",field : "SW_ID", hide : true},
			{headerName : "Application",field : "SW_NM"},
			{headerName : "VERSION",field : "SW_VER"},
			{headerName : "<spring:message code="CATEGORY" text="종류" />", field : "PURPOSE", },
			{headerName : "<spring:message code="NC_VRA_CATALOG_ALL_USE_YN" text="" />",field : "ALL_USE_YN"},
			{headerName : "<spring:message code="NC_REQ_SVC_CD" text="" />",field : "CATEGORY_NMS"},
			{headerName : "",field : "CATEGORY_ID", hide : true}
			]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onSelectionChanged : function() {
				
					arr = mainTable.getSelectedRows();
					envSearch(arr);
					
					req.getInfo("/api/sw/info?SW_ID="+arr[0].SW_ID, function(data){
						$("#shellBtn").attr("disabled",false);
							if(data.CATEGORY_IDS && data.CATEGORY_IDS != ''){
								 
								form_data.CATEGORY_IDS = data.CATEGORY_IDS.split(",");
								$("#CATEGORY_IDS").val(form_data.CATEGORY_IDS)
							}
							else{
								 
								$("#CATEGORY_IDS").val("");
							}
							$("#SW_DESC").val(data.SW_DESC);
							req.putData({ansiblePlayBook : data.SW_NM + "_" +data.SW_VER+ ".yaml"});
							$("#ansiblePlayBook").val(data.SW_NM + "_" +data.SW_VER+ ".yaml");
							vmInstallSearch();
							});
				},
		}
		mainTable = newGrid("mainTable", gridOptions);

		search();
	});
	
	
	var tabIdx = 1;
	function chgTab(idx)
	{
		tabIdx = idx;
	}
	
	//검색 카테고리
	function select_S_CATEGORY(data){
		searchvue.form_data.CATEGORY = data.CATEGORY;
		searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}
	//입력 카테고리
	function select_CATEGORY(data){
		inputvue.form_data.CATEGORY = data.CATEGORY;
		inputvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#CATEGORY_NM").val(data.CATEGORY_NM);
	}
	// 조회
	function search() {


		req.search('/api/sw/list' , function(data) {
			mainTable.setData(data);
		});
	}

	function insert(){
		req.setData();
		$("#shellBtn").attr("disalbed",true);
	}
	
	function del(){
		var arr = mainTable.getSelectedRows();

		if(arr.length > 0){
			req.del('/api/sw/delete', function(){
				search();
			});
		} else{
			alert("<spring:message code="select_first" text="" />")
		}
	}
	function shellEdit(ob, type){
		
		if($("#SW_NM").val() != '' && $("#SW_VER").val() != '' ){
			var title = $(ob).parent('div').children('label').html();
			var name = $(ob).val();
			var purpose = $("#PURPOSE").val();
			req.putData({'title' : title , "type" : type, "name" : name ,"url" : 'sw', "purpose":purpose });
			$("#shell_popup_button").click();
		}
	}
	function nameChange(){
		var shName = $("#SW_NM").val() + "_" + $("#SW_VER").val() + ".yaml"
		$("#ansiblePlayBook").val(shName);
	}
	// 저장
	function save() {
		if(!validate("input_area")) return;
		var w = mainTable.getSelectedRows();
		if(w.length > 0){
			req.putData({"CATEGORY_ID" : w[0].CATEGORY_ID })
		}
		
		req.save('/api/sw/swSave', function(){
			search();
		});
		
	}
	// 엑셀 내보내기
	function exportExcel()
	{
		 mainTable.exportCSV({fileName:'SW'})
		 // exportExcelServer("mainForm", '/api/vra_catalog_mng/list_excel', 'Snapshot',mainTable.gridOptions.columnDefs, req.getRunSearchData())
	}
	
	
	
</script>


