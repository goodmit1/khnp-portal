 <%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:340px">
	<div class="table_title layout name">
		<spring:message code="count" text="건수"/> : <span id="envTable_total">0</span>
		<div class="btn_group">


			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel_snapshot()" class="layout excelBtn" id="excelBtn"></button>
			<fm-sbutton cmd="delete" class="delBtn contentsBtn tabBtnImg del" onclick="envDel()"   ><spring:message code="btn_del" text="삭제"/></fm-sbutton>
			<fm-popup-button style="display: none;" popupid="envInsertPopup" popup="/clovirsm/popup/envInsertPopup.jsp" cmd="update" param="envParam"></fm-popup-button>
			<fm-sbutton cmd="update" class="newBtn contentsBtn tabBtnImg new" onclick="envInsert()"   ><spring:message code="btn_new" text="신규"/></fm-sbutton>
			<button type="button" title="<spring:message code="btn_reload" text="새로고침"/>" onclick="envSearch(arr[0].SW_ID)" class="contentsBtn tabBtnImg sync"><spring:message code="btn_reload" text="새로고침"/></button>

		</div>
	</div>
	<div id="envTable" class="ag-theme-fresh" style="height: 200px" ></div>
</div>
<script>
	var envReq = new Req();
	envTable;
	var envArr;
	
	var envParam={
			SW_ID:"",
			NAME:"",
			TYPE:"",
			REQUIRED:""
	};
	
	function envInit(){
		envParam={	
			SW_ID: arr[0].SW_ID,	
			SW_NM: "",
			REQUIRED: "",
			ENCRYPTED:"",
			TITLE: "",
			TYPE: "",
			NAME: "",
			MINIMUM:"",
			MAXIMUM:"",
			FIELDNAME:"",
			PATTERN:"",
			DEFAULT:"",
			DESCRIP0TION:""
		}
	}
	
	$(function() {
		var
		ColumnDefs = [ {
			headerName : '',
			hide : true,
			field : 'SW_ID'
		},  {
			headerName : '<spring:message code="SW_ENV_NAME" text="ENV명" />',
			field : 'NAME',
			width: 300
		},{
			headerName : '<spring:message code="SW_ENV_TITLE" text="제목" />',
			field : 'TITLE',
			width: 130
		},{
			headerName : '<spring:message code="SW_ENV_DEFAULT" text="기본값" />',
			field : 'DEFAULT',
			width: 110
		},{
			headerName : '<spring:message code="SW_ENV_TYPE" text="타입" />',
			field : 'TYPE',
			width: 130
		},{
			headerName : '<spring:message code="SW_ENV_REQUIRED" text="필수여부" />',
			field : 'REQUIRED',
			width: 130
		},{
			headerName : '<spring:message code="SW_ENV_MIN" text="최소값" />',
			field : 'MINIMUM',
			width: 130
		},{
			headerName : '<spring:message code="SW_ENV_MIN" text="최대값" />',
			field : 'MAXIMUM',
			width: 130
		},{
			headerName : '<spring:message code="SW_ENV_ENCRYPTED" text="암호여부" />',
			field : 'ENCRYPTED',
			width: 130
		},{
			headerName : '<spring:message code="SW_ENV_PATTERN" text="정규식" />',
			field : 'PATTERN',
			width: 130
		},{
			headerName : '<spring:message code="SW_ENV_DEC" text="설명" />',
			field : 'DESCRIPTION',
			width: 130
		}
		 ];
		var
		envGridOptions = {
			hasNo : true,
			columnDefs : ColumnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				envArr =  envTable.getSelectedRows();
				if(envArr.length > 0){
					//var nc_req = arr[0];
					envParam = envArr[0];
			    	$('#envInsertPopup_button').trigger('click');
				}
			}
		}
		envTable = newGrid("envTable", envGridOptions);
	});

	// 조회
	function envSearch(param) {
		 
		 
		if(param[0].SW_ID){
			envParam.SW_ID=param[0].SW_ID;
		}else{
			envParam.SW_ID=param
		}
		envReq.searchSub('/api/sw/env/list/list_SW_ENV/', { SW_ID: arr[0].SW_ID }, function(data) {
			envTable.setData(data);
		});
		
	}
	
	function envDel(){
		if(envArr[0] !== undefined && envArr[0] !== null ){
			post('/api/sw/env/delete',envArr[0],function(data){
				envSearch(data.SW_ID);
			});
		}
	}
	
	
	function envInsert(){
		if(envParam!==undefined && envParam!==null && arr != null){
			 
			envInit();
			$("#envInsertPopup_button").click();
		}else{
			alert("application을 선택해주세요")
			return;
		}
	}


</script>


