<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  서버 목록 조회 -->
<style>
#osGrid{
    border-top: 1px solid #626262;
    border-bottom: 1px solid #626262;
}
.switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 25px;
    vertical-align: middle;
    margin-right: 14px;
}
 
/* Hide default HTML checkbox */
.switch input {
	display:none;
}
 
/* The slider */
.slider {
	position: absolute;
	cursor: pointer;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background-color: #6E7783;
	-webkit-transition: .4s;
	transition: .4s;
}
 
.slider:before {
    position: absolute;
    content: "";
    height: 20px;
    width: 20px;
    left: 6px;
    bottom: 3px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
}
 
input:checked + .slider {
  	background-color: #77AAAD;
}
 
input:focus + .slider {
  	box-shadow: 0 0 1px #2196F3;
}
 
input:checked + .slider:before {
  	-webkit-transform: translateX(26px);
  	-ms-transform: translateX(26px);
  	transform: translateX(26px);
}
 
/* Rounded sliders */
.slider.round {
  	border-radius: 34px;
}
 
.slider.round:before {
  	border-radius: 50%;
}
.contentsItemBottom{
  	text-align: right; 
    position: relative;
    top: 17px;
    right: 10px;
    
}
.contentsItemBottom p{
  	margin:0px;
  	display:inline-block;
	font-size:15px;
	font-weight:bold;
    position: relative;
    top: -1px;
    left: -3px;
    width: 65px;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm">
			<fm-input id="S_OS_NM" name="OS_NM" title="<spring:message code="NC_OS_TYPE_GUEST_NM" text="OS명" />" input_style="width:150px;"></fm-input>
		</div>
		<div class="col col-sm">
				<fm-select2 url="" id="S_CONN_ID" keyfield="CONN_ID" titlefield="CONN_NM" emptystr="&nbsp" class="select2"
							name="CONN_ID" title="<spring:message code="" text="VRA 연결"/>"></fm-select2>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="ostypeSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:1080px; padding: 10px;">
	<div class="table_title layout name">
		<div class="btn_group">
			<fm-sbutton id="insert_ostype_button" cmd="update" class="newBtn contentsBtn  contentsBtn tabBtnImg save topM20" onclick="collect()"><spring:message code="btn_COLLECT"  text="수집"/></fm-sbutton>
		</div>
	</div>
		<div id="osItem"></div>
</div>
<script>
	var ostypeReq = new Req();

	
	
	$(document).ready(function(){
		connData();
		ostypeSearch();
	});

	function collect(){
		post('/api/ostype/collect', {}, function(data){
			ostypeSearch();
		})
	}
	function connData(){
		post("/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_ETC_CONN_NM",{"CONN_TYPE":"VRA"},function(data){
			fillOptionByData('S_CONN_ID', data, '', "CONN_ID", "CONN_NM");
		});
	}
	// 조회
	function ostypeSearch() {
		ostypeReq.search('/api/ostype/list' , function(data) {
			drawOs(data);
		});
	}

	function osChange(that){
		 
		var check = $(that).is(":checked");
		var params = new Object();
		params.OS_ID = $(that).attr('id').substring(2);
		check ? params.P_KUBUN = 'Y' : params.P_KUBUN = 'N' 
		 
		post("/api/ostype/save",params, function(){
			$(that).parents().children('p').toggle();
		});
	}
	function initFormData(){
		search_data.DEL_YN='N'
	}
	function drawOs(data){
		
		 
		
		$('#osItem').empty();
		for(var i = 0 ; i < data.list.length ; i++){
			var html = '<div class="contentsItem">';
			html += '<div class="contentsItemTop">';
			html += '<div class="itemName">'+ data.list[i].OS_NM +'</div>';
			html += '</div>';
			html += '<div class="contentsItemMid">';
			html += '<img src="/res/img/osImg.svg">';
			html += '</div>';
			html += '<div class="contentsItemBottom"> <label class="switch">';
			if(data.list[i].P_KUBUN == 'Y'){
				html += '<input type="checkbox" onchange="osChange(this)" id="OS'+data.list[i].OS_ID+'" class="P_KUBUN_Check" name="P_KUBUN" checked>';
			} else{
				html += '<input type="checkbox" onchange="osChange(this)" id="OS'+data.list[i].OS_ID+'" class="P_KUBUN_Check" name="P_KUBUN">';
			}
			
			html += '<span class="slider round"></span></label>';
			
			
			if(data.list[i].P_KUBUN == 'Y'){
				html += '<p style="display:none;"><spring:message code=" " text="사용안함" /></p>';
				html += '<p><spring:message code=" " text="사용함" /></p>';
			} else{
				html += '<p><spring:message code=" " text="사용안함" /></p>';
				html += '<p style="display:none;"><spring:message code=" " text="사용함" /></p>';
			}
			html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			$('#osItem').append(html);
		}
	}

</script>


