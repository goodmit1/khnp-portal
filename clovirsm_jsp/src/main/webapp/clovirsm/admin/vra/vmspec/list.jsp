<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<style>
#mainTable { height:calc(100vh - 220px); }

#DISK_SIZE,#CPU_CNT,#RAM_SIZE { width: 100px !important; }
#DD_FEE { width: 100px !important; }

</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
	<fm-popup-button style="display: none;" popupid="payConfigPopup" popup="/clovirsm/popup/payConfigPopup.jsp" cmd="update" param="specIdR"></fm-popup-button>
		<!-- 조회의 id는 S_를 붙인다. -->
			<div class="col col-sm">
					<fm-input id="S_SPEC_NM" name="SPEC_NM" title="<spring:message code="NC_VM_SPEC_SPEC_NM" text="명칭" />"></fm-input>
			</div>
		<div class="col btn_group nomargin">

			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text=""/></fm-sbutton>
		</div>
		<div class="col btn_group_under ">
			<fm-sbutton cmd="update" class="newBtn contentsBtn tabBtnImg collection top5" onclick="collect()"   ><spring:message code="btn_COLLECT"  text="수집"/></fm-sbutton>
		</div>
	</div>


</div>
<div class="fullGrid" id="input_area" style="padding: 10px;">
	<div id="vmspecItem"></div>
</div>
<script>
					var req = new Req();
					var specIdR;

					$(function() {

						search();
					});
					// 조회
					function search() {
						
						req.search('/api/vm_spec_mng/list' , function(data) {
							drawDisk(data);
						});
					}
					function collect(){
						post('/api/vm_spec_mng/collect', {}, function(data){
							search();
						})
					}
					function drawDisk(data){
						 
						$('#vmspecItem').empty();
						for(var i = 0 ; i < data.list.length ; i++){
							 
							var html = '<div class="contentsItem">';
							html += '<div class="contentsItemTop">';
							html += '<div class="itemName">'+ data.list[i].SPEC_NM +'</div>';
							if(data.list[i].DD_FEE){
								html += '<div class="itemPay" onclick="payConfig(\''+ data.list[i].SPEC_ID +'\')"><spring:message code="COL_FEE_UPDATE" text="금액 수정"/></div>';
							}
							html += '</div>';
							html += '<div class="contentsItemMid">';
							html += '<img src="/res/img/hynix/newHynix/shape-36.png" style="width: 60px; margin-left: 110px; margin-top: 20px;">';
							html += '</div>';
							html += '<div class="contentsItemBot">';
							if(data.list[i].DD_FEE){
								html += '<div class="contentsItemPay"><span><spring:message code="currency" text=""/></span> ' + formatNumber(data.list[i].DD_FEE) + ' <span style="font-size: 10px;">/ <spring:message code="label_date" text="일"/></span></div>';
							} else{
								html += '<div class="contentsItemPay"><span><spring:message code="currency" text=""/></span> ' + formatNumber(data.list[i].VM_DD_FEE) + ' <span style="font-size: 10px;">/ <spring:message code="label_date" text="일"/></span></div>';
							}
								
							html += '<div class="contentsSpec"><div>CPU ' + data.list[i].CPU_CNT + ', MEM ' + data.list[i].RAM_SIZE + '</div>';
							html += '</div>';
							html += '</div>';
							html += '</div>';
							$('#vmspecItem').append(html);
						}
					}
					function payConfig(spaceId){
						
						post('/api/vm_spec_mng/info?SPEC_ID='+spaceId,null,function(data){
							 
							specIdR = data;
							$("#payConfigPopup_button").click();
						});
						
					}
					function initFormData(){
						search_data.DEL_YN='N';
					}
					// 엑셀 내보내기
					function exportExcel()
					{
						 mainTable.exportCSV({fileName:'spec'})
						 // exportExcelServer("mainForm", '/api/vm_spec_mng/list_excel', 'Snapshot',mainTable.gridOptions.columnDefs, req.getRunSearchData())
					}
				</script>


