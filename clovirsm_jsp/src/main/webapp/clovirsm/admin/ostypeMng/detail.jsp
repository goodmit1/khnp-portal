<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body add-border">
		<div class="col col-sm-4">
			<fm-input id="OS_NM"  name="OS_NM" required="true" title="<spring:message code="NC_OS_TYPE_OS_NM" text="템플릿명" />"  ></fm-input>
			<input type="text" style="display:none" id="OS_ID" name="OS_ID" :value="form_data.OS_ID">
		</div>
		<c:if test="${sessionScope.STATE }">
		<div class="col col-sm-4">
			<fm-select url="/api/code_list?grp=P_KUBUN" required="true" id="P_KUBUN"
				emptystr=" "
				name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
			</fm-select>
		</div>
		</c:if>
		<div class="col col-sm-4">
			<fm-ibutton id="CATEGORY_NM" name="CATEGORY_NM" title="<spring:message code="NC_OS_TYPE_SERVICE" text="카테고리"/>" btn_class="cate_btn_height2" >
				<fm-popup-button popupid="category" popup="/clovirsm/popup/img_category_search_form_popup.jsp?sel_leaf_yn=Y" cmd="update" param="false" callback="select_CATEGORY"></fm-popup-button>
			</fm-ibutton>
			<input type="text" style="display:none" id="CATEGORY" name="CATEGORY" :value="form_data.CATEGORY">
		</div>
		<c:if test="${sessionScope.STATE }">
		<div class="col col-sm-4">
			<fm-select url="/api/code_list?grp=PURPOSE" id="PURPOSE"
				emptystr=" "
				name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>">
			</fm-select>
		</div>
		</c:if>
		<%--<div class="col col-sm-4">
			<fm-input id="YEAR" name="YEAR" title="<spring:message code="NC_OS_TYPE_YEAR" text="연도"/>"/>
			<input type="text" style="display:none" id="CATEGORY" name="CATEGORY" :value="form_data.CATEGORY">
		</div>
		<div class="col col-sm-4">
			<fm-select-yn id="SUB_YN" name="SUB_YN" required="true" onchange="select_user_yn()" title="<spring:message code="NC_OS_TYPE_PARTNER_YN" text="협력사여부"/>" >
		</div>--%>
		<%--<div class="col col-sm-4">
			<fm-input id="TEAM_NAME" name="TEAM_NAME" title="<spring:message code="NC_OS_TYPE_TEAM_NAME" text="팀이름"/>"/>
		</div>--%>

		<%--<div class="col col-sm-4" id="user_input_y">
			<fm-ibutton id="USER_NM"  name="USER_NM" title="<spring:message code="NC_OS_TYPE_USER_ID" text="요청자"/>" btn_class="cate_btn_height2">
				<fm-popup-button popupid="user_popup"
					popup="/popup/user_search_form_popup.jsp"
					cmd="update" param="$('#USER_NM').val()" callback="select_user">
				</fm-popup-button>
			</fm-ibutton>
			<input type="text" style="display:none" id="INS_ID" name="INS_ID" :value="form_data.INS_ID">
		</div>
		<div class="col col-sm-4" id="user_input_n">
			<fm-input id="USER_NM_INPUT" name="USER_NAME"  title="<spring:message code="NC_OS_TYPE_USER_ID" text="요청자"/>"></fm-input>
		</div>--%>
		<div class="col col-sm-4">
			<fm-select-yn id="LINUX_YN" requierd="true"
				name="LINUX_YN" title="<spring:message code="NC_OS_TYPE_LINUX_YN" text="리눅스 여부"/>">
			</fm-select-yn>
		</div>
		<div class="col col-sm-4">
			<fm-select-yn id="DEL_YN"
				name="DEL_YN" title="<spring:message code="HIDE_YN" text="숨김 여부"/>">
			</fm-select-yn>
		</div>
		<div class="col col-sm-4">
			<div class="input-popup inline hastitle" style="width: 620px;">
				<label for="ICON_upload" class="control-label grid-title value-title"><spring:message code="CM_TMPL_ICON" text="아이콘" /></label>
				<div class="input-group input">
					<input type="file" id="ICON_upload" accept="image/*" name="ICON_upload" class="form-control input hastitle" onchange="uploadFile()" style="width: 285px;">
					<input type="hidden" id="ICON" name="ICON" :value="form_data.ICON">
					<img id="ICON_p" style="height: 30px;">
					<button type="button" style=" min-width: 79px !important; right: 0px; top: 2px !important; outline: none;" onclick="iconCreate();" id="tabCreatIconBtn" class="contentsBtn tabBtnImg save top5" >아이콘생성</button>
				</div>
			</div>
		</div>
		<div class="col col-sm-12" style="height: 140px;">
			<fm-textarea id="CMT" name="CMT" title="<spring:message code="NC_IMG_CMT" text="설명" />"></fm-textarea>
		</div>
	</div>
	<fm-popup-button popupid="iconCreate_popup" popup="/clovirsm/popup/iconCreate_popup.jsp"  style="display:none;" param="param" callback="ostypeSearch()"></fm-popup-button>
	<script>
	
		function select_user(params, callback){
			form_data.USER_NM = params.USER_NAME;
			form_data.INS_ID = params.USER_ID;
			$('#USER_NM').val(params.USER_NAME)
			$('#TEAM_NAME').val(params.TEAM_NM)
			if(callback)callback();
		}
		function select_user_yn(){
			 
			if($('#SUB_YN').val() == 'Y'){
				$('#user_input_y').css("display","none");
				$('#user_input_n').css("display","block");
				$('#TEAM_NAME').removeAttr("disabled");
			} else if($('#SUB_YN').val() == 'N'){
				$('#user_input_y').css("display","block");
				$('#user_input_n').css("display","none");
				$('#TEAM_NAME').attr("disabled","true");
			}
		}
		function select_user_yn_click(data){
			 
			if(data == 'Y'){
				$('#user_input_y').css("display","none");
				$('#user_input_n').css("display","block");
				$('#TEAM_NAME').removeAttr("disabled");
			} else if(data == 'N'){
				$('#user_input_y').css("display","block");
				$('#user_input_n').css("display","none");
				$('#TEAM_NAME').attr("disabled","true");
			}
		}
		function uploadFile() {
			var file = $("#ICON_upload")[0].files[0];
			var reader = new FileReader();

			reader.addEventListener("load", function () {
				inputvue.form_data.ICON = reader.result;
				$("#ICON_p").prop("src", inputvue.form_data.ICON);
			}, false)
			if (file) {
				reader.readAsDataURL(file);

			}
		}
		function tabCreatIconBtnDisplay(enabled) {
			if(enabled){
				document.querySelector('#tabCreatIconBtn').style.display = 'block';
			}else{
				document.querySelector('#tabCreatIconBtn').style.display = 'none';
			}
		}
		
	</script>
</div>