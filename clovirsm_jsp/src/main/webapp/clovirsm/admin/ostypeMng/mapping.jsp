<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:240px; margin-left: 0;">
	<div id="mappingTable" class="ag-theme-fresh" style="height: 200px" ></div>
</div>
<script>
	var mappingReq = new Req();
	mappingTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_DC_DC_NM" text="" />", field : "DC_NM", maxWidth:300,width:300},
		                  {headerName : "<spring:message code="NC_DC_OS_TYPE_TMPL_PATH" text="" />", field : "TMPL_PATH", editable:true},
		                  {headerName : "<spring:message code="NC_VM_CPU_CNT" text="CPU" />", field : "CPU_CNT",  width:100, format:'number'},
		                  {headerName : "<spring:message code="NC_VM_RAM_SIZE" text="Memory(GB)" />", field : "RAM_SIZE", format:'number' },
		                  {headerName : "<spring:message code="NC_IMG_DISK_SIZE" text="디스크사이즈" />(GB)", field : "DISK_SIZE" , format:'number'},
		                  {headerName : "<spring:message code="NC_OS_TYPE_GUEST_NM" text="OS명" />", field : "GUEST_NM" },
		                  ]
		var
		mappingTableGridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			//rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onCellEditingStarted: function(event) {
				if(event.column.colId=='TMPL_PATH')
				{
					 
					autocomplete('#mappingTable div.ag-cell-value[col-id="TMPL_PATH"] input',function(){
						return '/api/code_list?grp=dblist.com.clovirsm.common.Component.selectTemplate&DC_ID=' + event.data.DC_ID
							
					},'OBJ_NM','OBJ_NM', 1);
					 
				}
		    },
		}
		mappingTable = newGrid("mappingTable", mappingTableGridOptions);
	});

	function mappingSearch(OS_ID){
		mappingReq.searchSub('/api/ostype/list/list_NC_DC_OS_TYPE/?OS_ID=' + OS_ID, {}, function(data) {
			 
			mappingTable.setData(data);
		});
	}

	// 엑셀 내보내기
	function exportExcel(){
		mappingListTable.exportCSV({fileName:'mappingList'})
		 
	}

</script>


