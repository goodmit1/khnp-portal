<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<!--  서버 목록 조회 -->
<style>
#osGrid{
    border-top: 1px solid #626262;
    border-bottom: 1px solid #626262;
}
.input-group{
	width:55%
}

.cate_btn_height1{
	height: 24px;
}
.cate_btn_height2{
	height: 21px;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm">
			<fm-input id="S_OS_NM" name="OS_NM" title="<spring:message code="NC_OS_TYPE_OS_NM" text="템플릿명" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select-yn id="S_LINUX_YN"
				name="LINUX_YN" emptystr=" " title="<spring:message code="NC_OS_TYPE_LINUX_YN" text="리눅스 여부"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-select-yn id="S_DEL_YN"
				name="DEL_YN" emptystr=" " title="<spring:message code="HIDE_YN" text="숨김 여부"/>">
			</fm-select>
		</div>
		<c:if test="${sessionScope.STATE }">
			<div class="col col-sm">
				<fm-select url="/api/code_list?grp=PURPOSE" id="S_PURPOSE"
					emptystr=" "
					name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>">
				</fm-select>
			</div>
			<div class="col col-sm">
				<fm-select url="/api/code_list?grp=P_KUBUN" id="S_P_KUBUN"
					emptystr=" "
					name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
				</fm-select>
			</div>
		</c:if>
		<div class="col col-sm" style="width: 267px;">
			<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM" title="<spring:message code="NC_OS_TYPE_SERVICE"/>" class="inline" btn_class="cate_btn_height1">
				<fm-popup-button popupid="s_category" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
			</fm-ibutton>
			<input type="text" style="display:none" id="S_CATEGORY" name="CATEGORY">
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="ostypeSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:450px">
	<div class="table_title layout name">
		<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="osGrid_total" style="color: royalblue;">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
      		<fm-popup-button popupid="category_mng" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" class="newBtn" param="true"><spring:message code="NC_OS_TYPE_SERVICE_EDIT" text="수정"/></fm-popup-button>
			<fm-sbutton id="insert_ostype_button" cmd="update" class="fm-popup-button contentsBtn tabBtnImg new" onclick="insertOstype()"><spring:message code="btn_new" text="신규"/></fm-sbutton>
			<fm-sbutton id="delete_ostype_button" cmd="delete" class="delBtn contentsBtn tabBtnImg del" onclick="deleteOstype()"><spring:message code="btn_delete" text="삭제"/></fm-sbutton>
			<fm-sbutton id="save_ostype_button" cmd="update"  class="saveBtn contentsBtn tabBtnImg save" onclick="ostypeSave()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
	<div id="osGrid" class="ag-theme-fresh" style="height: 450px" ></div>
	<ul class="nav nav-tabs">
		<li class="active" >
			<a href="#tab1" id="tab1_btn" data-toggle="tab"><spring:message code="title_subTable"/></a>
		</li>
		<li>
			<a href="#tab2" data-toggle="tab"><spring:message code="NC_OS_TYPE_OS_NM"/> <spring:message code="label_mapping"/></a>
		</li>
	</ul>
	<div class="tab-content ">
		<div class="form-panel detail-panel tab-pane active border-remove" id="tab1">
			<fmtags:include page="detail.jsp" />
		</div>
		<div  class="form-panel detail-panel tab-pane" id="tab2">
			<jsp:include page="mapping.jsp"></jsp:include>
		</div>
	</div>
</div>
<script>
	var ostypeReq = new Req();
	osGrid;
	function select_S_CATEGORY(data){
		searchvue.form_data.CATEGORY = data.CATEGORY;
		searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}
	function select_CATEGORY(data){
		inputvue.form_data.CATEGORY = data.CATEGORY;
		inputvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#CATEGORY_NM").val(data.CATEGORY_NM);
	}

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="NC_OS_TYPE_OS_NM" text="" />", field : "OS_NM", width:300},
		                  {headerName : "<spring:message code="NC_OS_TYPE_CMT" text="" />", field : "CMT"},
		                  {headerName : "<spring:message code="NC_VM_PURPOSE" text="사용용도"/>", field : "PURPOSE", width:140},
		                  {headerName : "<spring:message code="NC_VM_P_KUBUN" text="구분"/>", field : "P_KUBUN_NM", width:140},
		                  {headerName : "<spring:message code="NC_OS_TYPE_SERVICE" text="업무"/>", field : "CATEGORY_NM"},
		                  {headerName : "<spring:message code="NC_OS_TYPE_LINUX_YN" text="리눅스여부" />", field : "LINUX_YN", width:100 },
		                  {headerName : "<spring:message code="HIDE_YN" text="" />", field : "DEL_YN", width:100 },
		                  {headerName : "<spring:message code="NC_OS_TYPE_INS_TMS" text="" />", field : "INS_TMS", width:140,
		                	  valueGetter: function(params) {
					    	  	return formatDate(params.data.INS_TMS,'datetime')
							  }},
		                  <%--{headerName : "<spring:message code="NC_OS_TYPE_TEAM_NAME" text="팀이름"/>", field : "TEAM_NM", width:140},--%>
		                  <%--{headerName : "<spring:message code="NC_OS_TYPE_REQUESTER" text="요청자"/>", field : "USER_NAME", width:140},--%>
		                  <%--{headerName : "<spring:message code="NC_OS_TYPE_YEAR" text="연도"/>", field : "YEAR", width:100},--%>
		                  <%--{headerName : "<spring:message code="NC_VRA_CATALOGREQ_PURPOSE" text="용도"/>", field : "PURPOSE", width:100},--%>
		                  ]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				tabCreatIconBtnDisplay(true);
				var arr = osGrid.getSelectedRows();
				ostypeReq.getInfo('/api/ostype/info?OS_ID='+ arr[0].OS_ID, onRowClick);
				mappingSearch(arr[0].OS_ID)
			}
		}
		osGrid = newGrid("osGrid", gridOptions);
		ostypeSearch();
	});

	// 조회
	function ostypeSearch() {
		ostypeReq.search('/api/ostype/list' , function(data) {
			osGrid.setData(data);
			/*var i = osGrid.getRow(0);
			if(i){
				ostypeReq.getInfo('/api/ostype/info?OS_ID='+ i.data.OS_ID, onRowClick);
				mappingSearch(i.data.OS_ID);
			}*/
		});
	}

	function onRowClick(data){
		$("#ICON_p").attr("src", data.ICON );
		$("#ICON_upload").val("");
		if(data.APPR_STATUS_CD || data.CUD_CD){
			setButtonClickable('save_ostype_button', false);
			setButtonClickable('delete_ostype_button', false);
			setInputEnable('CMT', false);
			select_user_yn_click(data.SUB_YN);
		} else {
			setButtonClickable('save_ostype_button', true);
			setButtonClickable('delete_ostype_button', true);
			setInputEnable('CMT', true);
			select_user_yn_click(data.SUB_YN);
		}
	}

	//저장
	function ostypeSave(){

			ostypeReq.save('/api/ostype/save', function(data){
				mappingTable.stopEditing();
				post('/api/ostype/save_mapping', {OS_ID: data.OS_ID, LIST: JSON.stringify(mappingTable.getData())}, function(){
					if(!$("#tab2").hasClass("active")){
						ostypeSearch();
					}
					alert(msg_complete);
				})
			});

	}

	function insertOstype(){
		form_data.CMT      = '';
		form_data.DD_FEE   = '';
		form_data.DEL_YN   = '';
		form_data.HH_FEE   = '';
		form_data.INS_ID   = '';
		form_data.INS_IP   = '';
		form_data.INS_PGM  = '';
		form_data.INS_TMS  = '';
		form_data.KUBUN    = '';
		form_data.LINUX_YN = 'Y';
		form_data.SUB_YN   = 'N';
		form_data.YEAR     = '';
		form_data.USER_NAME= '';
		form_data.MM_FEE   = 0;
		form_data.OS_ID    = '';
		form_data.OS_NM    = '';
		form_data.PART_YN  = 'N';
		form_data.UPD_ID   = '';
		form_data.UPD_IP   = '';
		form_data.UPD_PGM  = '';
		form_data.UPD_TMS  = '';
		form_data.USER_NM  = '';
		form_data.TEAM_NM  = '';
		form_data.IDU  = 'I';
		form_data.DEL_YN='N';
		form_data.ICON = '';
		mappingSearch('');
		document.getElementById('ICON_p').src = '';
		document.getElementById("OS_NM").focus();
		tabCreatIconBtnDisplay(false);
		$("#tab1_btn").click();
	}
	function deleteOstype(){
		if(form_data.OS_ID && form_data.OS_ID != null){
			ostypeReq.del('/api/ostype/delete', function(){
				ostypeSearch();
				alert(msg_complete);
			});
		} else {
			alert(msg_select_first);
		}
	}

	// 엑셀 내보내기
	function exportExcel(){
		osGrid.exportCSV({fileName:'OS'})
		//exportExcelServer("mainForm", '/api/ostype/list_excel', 'OS_TYPE',osGrid.gridOptions.columnDefs, ostypeReq.getRunSearchData())
	}

	function updateColor(ICON_COLOR){
		var color = '';

		if(ICON_COLOR == null){
			ICON_COLOR = $("#ICON_COLOR").val();
		}
		$("#ICON_p").css("background-color", ICON_COLOR);
	}
	function iconCreate(){
		var arr = osGrid.getSelectedRows();
		if(arr.length > 0){
			param = { "CATEGORY_IDS" : $("#CATEGORY_IDS").val(), "OS_ID" : arr[0].OS_ID, "action" : "OSTYPE"};
			$('#iconCreate_popup_button').trigger('click');
		} else{
			alert(msg_select_first);
		}
	}

</script>


