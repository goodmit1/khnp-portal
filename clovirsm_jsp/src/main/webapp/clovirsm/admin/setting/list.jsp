<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 코드 관리 -->
<style>
div.ag-header.ag-pivot-off > div.ag-header-viewport > div > div > div:nth-child(1) > div.ag-cell-label-container.ag-header-cell-sorted-none{
	width: auto;
}
</style>
<div class="fullGrid" id="input_area">
	<div class="box_s"> 
	<div class="table_title layout name">
		<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportspecSetting()" class="layout excelBtn" id="excelBtn"></button>
			<fm-sbutton cmd="update" class="saveBtn contentsBtn tabBtnImg top save" class="btn btn-primary" onclick="savespecSetting()"><spring:message code="btn_save" text="저장"/></fm-sbutton>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height:450px;"></div>
	</div>
</div>
<script>

var specSetting = new Req();
function exportspecSetting(){
	mainTable.exportCSV({fileName:'ServerSetting.csv'})
}
function savespecSetting(){
	mainTable.gridOptions.api.stopEditing();
	var arr = mainTable.getData();
	arr.forEach(function(value,i){
		arr[i].IU = "U";
	})
//	for(var i in arr){
//	}
	var selected_json = JSON.stringify(arr);
	post("/api/ddicUser/save_multi", {"selected_json" : selected_json}, function(){
		searchspecSetting();
	})
}
function searchspecSetting(){
	specSetting.search('/api/ddicUser/list/list_FM_DDIC_USER/?DD_ID=ENV', function(data){
		mainTable.setData(data);
	});
}

var mainTable;

$(document).ready(function(){
	var columnDefs =[{ headerName: "", field: "ID", hide: true },
	    { headerName: "<spring:message code="NC_SPEC_SETTING_NM" text="구분" />", field: "EN_DD_DESC", maxWidth: 800},
	    {
			headerName: "<spring:message code="SETTING_RESULT" text="사용자 선택 값" />",
			field: "KO_DD_DESC",
			editable: true
		},
		{
			field: "DD_VALUE",
			hide: true
		}
	     ];
	var gridOptions = {
		    columnDefs: columnDefs,
		    rowData: [],
			rowSelection : 'single',
		    enableSorting: true,
		    enableColResize: true
		};
	mainTable = newGrid("mainTable", gridOptions)
// 	mainTable.columnApi.setColumnVisible('DD_VALUE',false)
	searchspecSetting();
});
</script>