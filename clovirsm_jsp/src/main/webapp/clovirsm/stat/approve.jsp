<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%request.setAttribute("url",PropertyManager.getString("stat.url.approve")); %>

<layout:extends name="base/index">
    <layout:put block="content">
		<jsp:include page="./_date_sel.jsp"></jsp:include>
		<iframe id="statWin" class="iframe150" src="${url}" height="600" width="800"></iframe>
		
	</layout:put>
</layout:extends>		