<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
 

<layout:extends name="base/index">
    <layout:put block="content">
    	<div class="stat_charts">
    		<script src="/res/js/stat_chart_config.js" ></script>
			<script src="/res/js/stat.js" ></script>
    		<div class="col col-sm-12">
    			<jsp:include page="./_mm_sel.jsp"></jsp:include>
    		 </div>
    		<div class="col col-sm-4">
       			<div id="dept_pie" class="chart"  data-type="chart" data-setting="dept_pie_config"></div>
         	</div>	 
         	<div class="col col-sm-4">
       			<div id="category_pie" class="chart"  data-type="chart" data-setting="category_pie_config"></div>
         	</div>	
         	<div class="col col-sm-4">
       			<div id="kubun_pie" class="chart"  data-type="chart" data-setting="kubun_pie_config"></div>
         	</div> 
         	
    		<div class="col col-sm-4">
       			<div id="dept_line" class="chart"  data-type="chart" data-setting="dept_line_config"></div>
         	</div>	
         	<div class="col col-sm-4">
       			<div id="category_line" class="chart"  data-type="chart" data-setting="category_line_config"></div>
         	</div>	
         	<div class="col col-sm-4">
       			<div id="kubun_line" class="chart"  data-type="chart" data-setting="kubun_line_config"></div>
         	</div>	
         <!-- 
         	<div class="col col-sm-4">
       			<div id="grid" class="chart ag-theme-fresh" data-type="grid" data-setting="grid_config"></div>
         	</div>	
          -->	
         	 
          
       	</div>
		 <script>
		 	var chart =  new Chart("stat_NC_MM_STAT",null,false);
		 	$(document).ready(function(){
		 		$("#mm6").click();
		 		
		 	})
		 	function grid_config(){
		 		var setting = {};
		 	 
		 		
		 		setting.rowData=chart.chart_data;
		 		var columnDefs = [
		 			{headerName : '월', field: "YYYYMM"  },
		 			{headerName : '팀', field: "TEAM_NM"},
		 			{headerName : '업무', field: "CATEGORY_NM"},
		 			{headerName : '구분', field: "P_KUBUN_NM"},
		 			{headerName : '비용', field: "FEE", aggr:"sum"},
		 			{headerName : 'VM 개수', field: "count"},
		 			 
		 			
		 		];
		 		var data  = calData4Grid(chart.chart_data, columnDefs );
		 		data = array_sort(data, 'FEE',true);
		 		return get_grid_config(columnDefs, data, "월별 비용 목록");
		 		 
		 	}

		 	function dept_pie_config(){
		 		var setting = countByGroup(chart.chart_data,   "TEAM_NM",null, "FEE|sum");
		 		setting.title = "팀별 비용";
		 		setting.ytitle = '비용';
		 		setting.xtitle  = "팀";
		 		setting.objName = "chart"
				setting.name = "TEAM_NM";
		 		setting.unit="원"
		 		return get_pie_config(setting);
		 	}
		 	function category_pie_config(){
		 		var setting = countByGroup(chart.chart_data,   "CATEGORY_NM",null, "FEE|sum");
		 		setting.title = "업무별 비용";
		 		setting.ytitle = '비용';
		 		setting.xtitle  = "업무";
		 		setting.objName = "chart"
				setting.name = "CATEGORY_NM";
		 		setting.unit="원"
		 		return get_pie_config(setting);
		 	}
		 	function kubun_pie_config(){
		 		var setting = countByGroup(chart.chart_data,   "P_KUBUN_NM",null, "FEE|sum");
		 		setting.title = "구분별 비용";
		 		setting.ytitle = '비용';
		 		setting.xtitle  = "구분";
		 		setting.objName = "chart"
				setting.name = "P_KUBUN_NM";
		 		setting.unit="원"
		 		return get_pie_config(setting);
		 	}
		 	function dept_line_config(){
		 		
		 		var setting = countByGroup(chart.chart_data,  "TEAM_NM" ,"YYYYMM","FEE|sum");
		 		 
		 		setting.title = "팀별 비용";
		 		setting.ytitle = '비용';
		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
			 
			function category_line_config(){
		 		
		 		var setting = countByGroup(chart.chart_data, "CATEGORY_NM" ,"YYYYMM", "FEE|sum" );
		 		setting.title = "업무별 비용";
		 		setting.ytitle = '비용';
		 		extractTopMulti(setting, 10, 'Other')
		 		return get_line_config(setting);
		 	}
			function kubun_line_config(){
		 		
		 		var setting = countByGroup(chart.chart_data,   "P_KUBUN_NM"  ,"YYYYMM",  "FEE|sum");
		 		setting.title = "구분별 비용";
		 		setting.ytitle = '비용';
		 		setting.stacking = true;
		 		return get_column_config(setting);
		 	}
		 </script>
		
	</layout:put>
</layout:extends>		