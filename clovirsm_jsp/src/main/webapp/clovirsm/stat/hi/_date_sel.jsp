<%@page contentType="text/html; charset=UTF-8" %>    
    <div style="float:right">
      <table style="float:right; position: relative; bottom: 8px;"><tr><td>
	     <div class="input-group" style="width: 80px;"><input type="text" name="FROM_DT" id="FROM_DT" class="form-control  dateInput"> <span class="input-group-addon calendar-btn"><i class="icon-append fa fa-calendar"></i></span></div>
	     </td><td>~</td>
	     <td>
			<div class="input-group" style="width: 80px;"><input type="text" name="TO_DT" id="TO_DT" class="form-control  dateInput"> <span class="input-group-addon calendar-btn"><i class="icon-append fa fa-calendar"></i></span></div>
		</td>
		<td><input type="button" value="조회" class="contentsBtn tabBtnImg sync" id="goStatFrom" style="margin-left: 30px; top: -2px;" onclick="goStatFromTo(this);">
		</td></tr>
	  </table>
	  <div id="timeStat" style="    display: inline-block;">
	      <input type="button" class="contentsBtn tabBtnImg sync" value="오늘" onclick='goStat1(this,"d");'>  
	      <input type="button" class="contentsBtn tabBtnImg sync" value="어제 ~" onclick='goStat1(this,"-1d");'>
	      <input type="button" class="contentsBtn tabBtnImg save" value="7일전 ~" onclick='goStat1(this,"-7d");'>  
	      <input type="button" class="contentsBtn tabBtnImg sync" value="이번주" onclick='goStat1(this,"w");'>  
	      <input type="button" class="contentsBtn tabBtnImg sync" value="이번달" onclick='goStat1(this,"m");'>  
      </div>
	  <script>
	 	function goDefault(){
	  		$("#timeStat .save").click();
	  	} 
	  	function goStat1(that, term){
	  		var kubun = term.substring(term.length-1);
	  		var d = 0;
	  		if(term.length>1){
	  			d = 1 * term.substring(0,term.length-1)
	  		}
	  		var date = new Date();
	  		if(kubun=='d'){
	  			date.setDate(date.getDate() + d );
	  		}
	  		else if(kubun=='m'){
	  			date.setDate(1);
	  		}
	  		else if(kubun=='w'){
	  			date.setDate(date.getDate()-date.getDay());
	  		}
	  		goStat(that , formatDate(date, 'date'), formatDate(new Date(),'date'))
	  	}
	  	function goStatFromTo(that){
	  		$("#goStatFrom").removeClass("sync");
	  		$("#goStatFrom").addClass("save");
	  		$("#timeStat input[type='button']").removeClass("save");
	  		$("#timeStat input[type='button']").addClass("sync");
	  		if($("#FROM_DT").val()==''){
	  			alert(msg_empty_value);
	  			$("#FROM_DT").focus();
	  			return;
	  		}
	  		
	  		var toDate ; 
	  		if($("#TO_DT").val()==''){
	  			toDate = formatDate(new Date(),'date');	
	  		}
	  		else{
	  			toDate =  $("#TO_DT").val();
	  		}
	  		goStat(that, $("#FROM_DT").val() , toDate, true);
	  		 
	  	}
	  	function clearDateVal(){
	  		$("#FROM_DT").val("");
	  		$("#TO_DT").val("");
	  	}
	  	function goStat(that, from, to, isFromDate){
	  		chart.getData(from, to)
	  		$("#goStatFrom").removeClass("save");
	  		$("#goStatFrom").addClass("sync");
	  		$("#timeStat input[type='button']").removeClass("save");
	  		$("#timeStat input[type='button']").addClass("sync");
	  		$(that).removeClass("sync");
	  		$(that).addClass("save");
	  		 
	  		if(!isFromDate){
	  			clearDateVal();
	  		}
	  	}
	  </script>
     </div> 