<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
 

<layout:extends name="base/index">
    <layout:put block="content">
    	<div class="stat_charts">
    		<script src="/res/js/stat_chart_config.js" ></script>
			<script src="/res/js/stat.js" ></script>
    		 <div class="col col-sm-6">
    		 	<div id='filter' class="chart" data-type="filter" data-setting="filter_setting" style="height:175px;"></div>
    		 </div>
    		 <div class="col col-sm-2">
         		<div id="cpu_metric" class="chart" data-type="metric" data-setting="cpu_metric" style="height:175px;"></div>
         	</div>	
    		 <div class="col col-sm-2">
         		<div id="server_metric" class="chart" data-type="metric" data-setting="server_metric" style="height:175px;"></div>
         	</div>	
    		 <div class="col col-sm-2">
         		<div id="vm_metric" class="chart" data-type="metric" data-setting="vm_metric" style="height:175px;"></div>
         	</div>	
         	<div class="col col-sm-6">
         		<div id="cpu_bar" class="chart" data-type="chart" data-setting="cpu_bar_setting"></div>
         	</div>	
    		
         	 <div class="col col-sm-6">
         		<div id="server_bar" class="chart" data-type="chart" data-setting="server_bar_setting"></div>
         	</div>	
         	<div class="col col-sm-6">
         		<div id="vm_bar" class="chart" data-type="chart" data-setting="vm_bar_setting"></div>
         	</div>	
         </div>	
		 <script>
		 	var chart = new Chart("stat_NC_LICENSE")
		 	$(document).ready(function(){
		 		chart.getDataByAPI();
		 		
		 	})
		 	function  bar_setting(kubun, kubunStr){
		 		var data = filter(chart.chart_data, 'data.COST_UNIT=="' + kubun +  '"');
		 		var columnDefs = [
		 			{headerName : '제품', field: "LICENSE_NAME"},		 		 
		 			{headerName : '전체', field: "LICENSE_TOTAL", aggr:"sum"},
		 			{headerName : '사용', field: "LICENSE_USED", aggr:"sum"} 
		 			
		 		];
		 		 
		 		
		 		var setting = makeChartData(data, columnDefs)
		 		setting.title = "제품별 라이센스 개수(" + kubunStr + ")";
		 		setting.ytitle  = "개수";
		 		extractTop(setting, 10, 'Other');
		 		return get_bar_config(setting);
		 	}
		 	
		 	function metric_setting(kubun, kubunStr){
		 		var setting = {};
		 		var data = summary(filter(chart.chart_data, 'data.COST_UNIT=="' + kubun + '"'), 'LICENSE_TOTAL') - summary(filter(chart.chart_data, 'data.COST_UNIT=="' + kubun + '"'), 'LICENSE_USED') ;
		 		setting.title = kubunStr;
		 		setting.val = data;
		 		
		 		return setting;
		 	}
		 	
		 	function vm_bar_setting(){
		 		 return bar_setting('vm',"VM");
		 	}
		 	function server_bar_setting(){
		 		 return bar_setting('server',"Server");
		 		 
		 	}
		 	function cpu_bar_setting(){
		 		 return bar_setting('cpuPackage',"Cpu");
		 		 
		 	}
		 	
		 	function cpu_metric(){
		 		return metric_setting('cpuPackage','CPU(remain)');
		 	}
		 	function server_metric(){
		 		return metric_setting('server','Server(remain)');
		 	}
		 	function vm_metric(){
		 		return metric_setting('vm','VM(remain)');
		 	}
		 	
		 	function filter_setting(){
		 		var setting = {};
		 		
		 		setting.title = 'DataCenter';
		 		setting.data = chart.getCategories("DC_NM");
		 		setting.labelTitle = "DataCenter";
		 		setting.name = "DC_NM";
		 		setting.objName = "chart";
		 		return setting;
		 	}
		 	
		 </script>
		
	</layout:put>
</layout:extends>		