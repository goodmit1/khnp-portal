<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
 

<layout:extends name="base/index">
    <layout:put block="content">
    	<div class="stat_charts">
    		<script src="/res/js/stat_chart_config.js" ></script>
			<script src="/res/js/stat.js" ></script>
    		<div class="col col-sm-6">
       			<div id="warning_cnt" class="chart"  data-type="chart" data-setting="warning_cnt"></div>
         	</div>	
    		<div class="col col-sm-6">
       			<div id="host_warning_cnt" class="chart"  data-type="chart" data-setting="host_warning_cnt"></div>
         	</div>	
    		<div class="col col-sm-6">
       			<div id="vm_warning_cnt" class="chart"  data-type="chart" data-setting="vm_warning_cnt"></div>
         	</div>	
    		<div class="col col-sm-6">
       			<div id="ds_volume" class="chart"  data-type="chart" data-setting="ds_volume"></div>
         	</div>	
         </div>	
		 <script>
		 	var chart = new Chart("stat_NC_DC")
		 	$(document).ready(function(){
		 		chart.getDataByAPI();
		 	})
		 	function bar_setting(kubun1,kubun2){
		 		var data = chart.chart_data;
		 		var columnDefs = [
		 			{headerName : '데이터센터', field: "DC_NM"},		 		 
		 			{headerName : 'RED', field: kubun1+"_CNT", aggr:"sum"},
		 			{headerName : 'YELLOW', field: kubun2+"_CNT", aggr:"sum"} 
		 			
		 		];
		 		 
		 		var setting = makeChartData(data, columnDefs)
		 		
		 		return setting;
		 	}
		 	function bar_setting_cnt(kubun1){
		 		var data = chart.chart_data;
		 		var columnDefs = [
		 			{headerName : '데이터센터', field: "DC_NM"},		 		 
		 			{headerName : '개수', field: kubun1+"_CNT", aggr:"sum"}
		 			
		 		];
		 		 
		 		var setting = makeChartData(data, columnDefs)
		 		
		 		return setting;
		 	}
			function warning_cnt(){
				var setting = bar_setting('RED','YELLOW');
		 		setting.title = "경고갯수";
		 		setting.ytitle = 'Count';
		 		setting.stacking = true;
		 		return get_column_config(setting);
		 	}
			function host_warning_cnt(){
				var setting = bar_setting_cnt('HOST');
		 		setting.title = "호스트갯수";
		 		setting.ytitle = 'Count';
		 		setting.stacking = true;
		 		return get_column_config(setting);
		 	}
			function vm_warning_cnt(){
				var setting = bar_setting_cnt('HOST');
		 		setting.title = "VM갯수";
		 		setting.ytitle = 'Count';
		 		setting.stacking = true;
		 		return get_column_config(setting);
		 	}
			function ds_volume(){
				var data = chart.chart_data;
		 		var columnDefs = [
		 			{headerName : '데이터센터', field: "DC_NM"},		 		 
		 			{headerName : '총 용량', field: "DS_CAPACITY", aggr:"sum"},
		 			{headerName : '사용 용량', field: "DS_FREESPACE", aggr:"sum"} 
		 			
		 		];
		 		 
		 		var setting = makeChartData(data, columnDefs)
		 		setting.title = "DataStore용량";
		 		setting.ytitle = '용량(TB)';
		 		setting.stacking = false;
		 		return get_column_config(setting);
			}
		 </script>
		
	</layout:put>
</layout:extends>		