<%@page contentType="text/html; charset=UTF-8" %>    
    <div style="float:right">
      <table style="float:right; position: relative; bottom: 8px;"><tr><td>
	     <input name="FROM_DT"  id="FROM_DT" style="width:60px"  maxlength="6"   placeholder="yyyyMM">
	     </td><td>~</td>
	     <td>
			<input name="TO_DT"  id="TO_DT" style="width:60px" maxlength="6" placeholder="yyyyMM">
		</td>
		<td><input type="button" value="조회" class="contentsBtn tabBtnImg sync" id="goStatFrom" style="margin-left: 30px; top: -2px;" onclick="goStatFromTo(this);">
		</td></tr>
	  </table>
	  <div id="timeStat" style="    display: inline-block;">
 
	      <input type="button" id="yy1" class="contentsBtn tabBtnImg save" value="1년전~" onclick='goStat1(this,"-12m");'>  
	      <input type="button" id="mm6" class="contentsBtn tabBtnImg sync" value="6개월~" onclick='goStat1(this,"-5m");'>  
	      <input type="button" id="mm" class="contentsBtn tabBtnImg sync" value="이번달" onclick='goStat1(this,"m");'>  
      </div>
	  <script>
	 	function goDefault(){
	  		$("#timeStat .save").click();
	  	} 
	  	function goStat1(that, term){
	  		var kubun = term.substring(term.length-1);
	  		var d = 0;
	  		if(term.length>1){
	  			d = 1 * term.substring(0,term.length-1)
	  		}
	  		var date = new Date();
	  		if(kubun=='m'){
	  			date.setDate(1);
	  			date.setMonth(date.getMonth() + d );
	  		}
	  		 
	  		
	  		goStat(that , formatDatePattern(date, 'yyyyMM'), formatDatePattern(new Date(),'yyyyMM'))
	  	}
	  	function isMonth(fromMM ){
	  		if(fromMM.length!=6){
	  			alert('6자리로 입력해 주세요');
	  			return false;
	  		}
	  		try
	  		{
	  			var from_date = new Date(fromMM.substring(0,4),1*fromMM.substring(4,6) - 1, 1);
	  			if( formatDatePattern(from_date, 'yyyyMM') == fromMM ){
	  				return true;
	  			}
	  			else{
	  				alert('정확히 입력해 주세요');
		  			return false;
	  			}
	  		}
	  		catch(e){
	  			alert('정확히 입력해 주세요');
	  			return false;
	  		}
	  	}
	  	function goStatFromTo(that){
	  		$("#goStatFrom").removeClass("sync");
	  		$("#goStatFrom").addClass("save");
	  		$("#timeStat input[type='button']").removeClass("save");
	  		$("#timeStat input[type='button']").addClass("sync");
	  		if($("#FROM_DT").val()==''){
	  			alert(msg_empty_value);
	  			$("#FROM_DT").focus();
	  			return;
	  		}
	  		
	  		var toDate ; 
	  		if($("#TO_DT").val()==''){
	  			toDate = formatDatePattern(new Date(),'yyyyMM');	
	  		}
	  		else{
	  			toDate =  $("#TO_DT").val();
	  		}
	  		if(isMonth($("#FROM_DT").val()) && isMonth(toDate)){
	  			if($("#FROM_DT").val()  >  toDate ){
	  				alert("시작월이 더 큽니다.");
	  				return false;
	  			}
	  			goStat(that, $("#FROM_DT").val() , toDate, true);
	  		}
	  		 
	  	}
	  	function clearDateVal(){
	  		$("#FROM_DT").val("");
	  		$("#TO_DT").val("");
	  	}
	  	function goStat(that, from, to, isFromDate){
	  		chart.getData(from, to)
	  		$("#goStatFrom").removeClass("save");
	  		$("#goStatFrom").addClass("sync");
	  		$("#timeStat input[type='button']").removeClass("save");
	  		$("#timeStat input[type='button']").addClass("sync");
	  		$(that).removeClass("sync");
	  		$(that).addClass("save");
	  		 
	  		if(!isFromDate){
	  			clearDateVal();
	  		}
	  	}
	  </script>
     </div> 