<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<%@page import="java.util.List"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("isCancelabel", "cancel".equals(action));
	request.setAttribute("isEditable", !"view".equals(action) && !"cancel".equals(action));

	NextApproverService nextApproverService = (NextApproverService)SpringBeanUtil.getBean("nextApproverService");
	List<String> stepNames = nextApproverService.getAllStepNames();
	request.setAttribute("STEP_NAMES", stepNames);
	
%>
<style>
.Rounded-Rectangle_in{
	margin-top: 0px;
    height: 8px;
    border-radius: 7px;
    width: 0%;
    background-color: #f58025;
    }
  #approval_req_form_popup_APPR_COMMENT{
  	width: 77%;
  	height: 130px;
  }
  #approval_req_form_popup_form > div > div:nth-child(7) > div > div{
  	min-height: 200px;
  	overflow-y: auto;
  	word-break: break-all;
  }
</style>
<fm-modal id="${popupid}" title="<spring:message code="tab_approval_request" />" cmd="header-title">
	<span slot="footer">

		<input v-if="${isCancelabel}" id="approval_cancel_button" type="button" class="btn" value="<spring:message code="btn_auth_request_cancel" text=""/>" onclick="${popupid}_cancel()" >
		<input v-if="${isEditable}" type="button" class="btn" value="<spring:message code="btn_approval_deny" text=""/>" onclick="${popupid}_deny()" >
		<input v-if="${isEditable}" type="button" class="btn" value="<spring:message code="btn_approval_accept" text=""/>" onclick="${popupid}_approve()" />
	</span>

	<div class="appProgressBar">
		<div class="bar_progress in" style="width:75%;">
			<div  class="progress-coming-back zindex" >
				<div class="progress-coming-pre">1</div>
			</div>
			<div  class="progress-done zindex" >
				<div class="progress-coming-normal">${fn:length(STEP_NAMES)+2}</div>
			</div>
			
			<span>
			<c:if test="${fn:length(STEP_NAMES) >0}">
			<c:set var="num" value="${fn:length(STEP_NAMES)+1}" />
				<c:forEach items="${STEP_NAMES}" var="name" varStatus="index">
					<div  class="progress-coming-back zindex" style="left:${((90/num) * index.count)}%;">
						<div class="progress-coming-normal">${index.count+1}</div>
					</div>
					<div class="approvalName-detail" style="left:calc(${90/num * index.count}% - 15px);">
						${name}
					</div>
				</c:forEach>
			</c:if>
			</span>
			<div class="Rounded-Rectangle-Full" style="width:100%;top: 95px;"></div>
			<div class="approvalName-detail last"> <spring:message code="finish" text="완료"/></div>
			<div class="approvalName-detail first"> <spring:message code="sincere" text="상신"/></div>
		</div>
	</div>

	<ul class="nav nav-tabs">
		<li>
			<a href="#tab1" data-toggle="tab"><spring:message code="tab_approval_request" /></a>
		</li>
		<li>
			<a href="#tab2" data-toggle="tab"><spring:message code="work_list" text="작업내역"/></a>
		</li>
		<li>
			<a href="#tab3" data-toggle="tab"><spring:message code="tab_pay_history" text="결재내역"/></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="form-panel detail-panel tab-pane" id="tab1">
			<fm-hidden id="${popupid}_STEP" name="STEP" />
			<form id="${popupid}_form" action="none">
				<div class="panel-body">
					<%-- <div class="col col-sm-6">
						<fm-output id="${popupid}_REQ_ID" name="REQ_ID" title="<spring:message code="NC_REQ_REQ_NO" text="No"/>"></fm-output>
					</div> --%>
					<div class="col col-sm-6">
						<fm-output id="${popupid}_REQ_NM" name="REQ_NM" title="<spring:message code="NC_REQ_REQ_NM" text="?쒕ぉ"/>" tooltip="<spring:message code="NC_REQ_REQ_NM" text="?쒕ぉ"/>"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="${popupid}_APPR_STATUS_CD_NM" name="APPR_STATUS_CD_NM" title="<spring:message code="NC_REQ_DETAIL_APPR_STATUS_CD" text="寃곗옱?곹깭"/>"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="${popupid}_INS_TMS" name="INS_TMS" title="<spring:message code="NC_REQ_INS_TMS" text="요청일시"/>" :value="formatDate(form_data.INS_TMS, 'datetime')"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="${popupid}_USER_NAME" name="USER_NAME" title="<spring:message code="NC_REQ_INS_ID_NM" text="요청자"/>"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="${popupid}_APPR_TMS" name="APPR_TMS" title="<spring:message code="NC_REQ_APPR_TMS" text="결재일자"/>" :value="formatDate(form_data.APPR_TMS, 'datetime')"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="${popupid}_APPR_USER_NAME" name="STEP_NM" title="<spring:message code="NC_REQ_APPR_NM" text="결재자"/>"></fm-output>
					</div>
					<div class="col col-sm-12">
						<div style="height: 200px;">
							<label for="CMT" class="control-label grid-title value-title"><spring:message code="NC_VM_CMT" text="설명" /></label>
							<div class="output hastitle" style="word-break: break-word;">
								<p v-html="form_data.CMT" ></p>
							</div>
						</div>
					</div>
					<div class="col col-sm-12" style="height: 140px;">
						<fm-textarea id="${popupid}_APPR_COMMENT" name="APPR_COMMENT" ${!isEditable ? 'disabled="disabled"': ''} title="<spring:message code="NC_REQ_DETAIL_APPR_COMMENT" text="결재의견"/>"></fm-textarea>
					</div>


				</div>
			</form>
		</div>
		<div  class="form-panel detail-panel tab-pane" id="tab2">
			<fm-popup-button style="display:none;"popupid="vm_detail_tab_popup" popup="../../popup/vm_detail_tab_form_popup.jsp?action=insert&isAppr=Y" cmd="update" param="vmReq.getData()"><spring:message code="btn_modify" text="생성 정보 수정"/></fm-popup-button>
			<fm-popup-button style="display:none;"popupid="vm_update_popup" popup="../../popup/vm_detail_form_popup.jsp?&action=update&isAppr=Y" cmd="update" param="vmReq.getData()"><spring:message code="btn_spec_modify" text="사양 변경"/></fm-popup-button>
			<fm-popup-button style="display:none;"popupid="vm_req_info_popup" popup="../../popup/vm_detail_form_popup.jsp?&action=appr" cmd="update" param="vmReq.getData()"><spring:message code="label_vm_info" text="서버 정보"/></fm-popup-button>
			<fm-popup-button style="display:none;"popupid="fw_insert_save_popup" popup="../../popup/fw_detail_form_popup.jsp?action=insert&isAppr=Y" cmd="update" param="fwReq.getData()"><spring:message code="btn_modify" text="생성 정보 수정"/></fm-popup-button>
			<fm-popup-button style="display:none;"popupid="fw_update_popup" popup="../../popup/fw_detail_form_popup.jsp?action=update&isAppr=Y" cmd="update" param="fwReq.getData()"><spring:message code="btn_update_req" text="변경 요청"/></fm-popup-button>
			<fm-popup-button style="display:none;"popupid="fw_req_info_popup" popup="../../popup/fw_detail_form_popup.jsp" cmd="update" param="fwReq.getData()"><spring:message code="label_fw_info" text="접근제어 정보"/></fm-popup-button>
			<fm-popup-button style="display:none;"popupid="img_insert_save_popup" popup="../../popup/img_detail_form_popup.jsp?action=insert&isAppr=Y" cmd="update" param="imgReq.getData()"><spring:message code="btn_modify" text="생성 정보 수정"/></fm-popup-button>
			<fm-popup-button style="display:none;"popupid="img_req_info_popup" popup="../../popup/img_detail_form_popup.jsp" cmd="update" param="imgReq.getData()"><spring:message code="label_img_info" text="템플릿 정보"/></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="new_app_popup" popup="/clovircm/popup/newApp/new_app_popup.jsp" cmd="update" param="appReq.getData()"></fm-popup-button>
			<fm-popup-button style="display: none;" popupid="newVRAPopup" popup="/clovirsm/popup/new_vra_popup.jsp?action=disable" param="vraInfo"></fm-popup-button>
			<div class="fullGrid" id="input_area" style="height:240px;">
				<div id="${popupid}_grid" class="ag-theme-fresh" style="height:200px;"></div>
			</div>
		</div>
		<div  class="form-panel detail-panel tab-pane" id="tab3">

			<div class="fullGrid" id="appr_grid" style="height:240px;">
				<div id="${popupid}_appr_grid" class="ag-theme-fresh" style="height:200px;"></div>
			</div>
		</div>
	</div>
	<script>
		var re = null;
		var popup_${popupid}_form = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		function ${popupid}_click(vue, param){
			if(param.REQ_ID){
				 
				 
				${popupid}_param = $.extend({}, param);
				${popupid}_vue.form_data = ${popupid}_param;
				$(".bar_progress.in > span > div > div ").attr("class","progress-coming-normal zindex");
				$(".progress-done > div ").attr("class","progress-coming-normal zindex");
				$(".approvalName-detail").removeClass("in");
				$(".approvalName-detail").removeClass("red");
				if(${fn:length(STEP_NAMES)} >=  param.STEP){
					
					$(".bar_progress.in > span > div").eq(param.STEP -1).prevAll("div").children().attr("class","progress-coming-pre zindex");
					if(param.APPR_STATUS_CD_T == 'D' || param.APPR_STATUS_CD == 'C' || param.APPR_STATUS_CD_T == 'R'){
						$(".bar_progress.in > span > div > div ").eq(param.STEP -1).attr("class","progress-coming-red zindex");
						$(".bar_progress.in > span > .approvalName-detail").eq(param.STEP -1).attr("class","approvalName-detail red");
						re = setInterval(function() {
							$(".progress-coming-red").fadeOut("slow" );
							$(".progress-coming-red").fadeIn("slow" );
						},1000);
					} else if( param.APPR_STATUS_CD == 'A'){
						$(".approvalName-detail").removeClass("in");
						$(".bar_progress.in > span > div > div").attr("class","progress-coming-pre zindex");
						$(".progress-done > div").attr("class","progress-coming-in zindex");
						$(".approvalName-detail.last").addClass("in");
						
					} else{
						$(".bar_progress.in > span > div > div ").eq(param.STEP -1).attr("class","progress-coming-in zindex");
						$(".bar_progress.in > span > .approvalName-detail").eq(param.STEP -1).attr("class","approvalName-detail in");
						re = setInterval(function() {
							$(".progress-coming-in").fadeOut("slow");
							$(".progress-coming-in").fadeIn("slow");
						},1000);
					}
					
				} 
				
				if(${popupid}_param.APPR_STATUS_CD == 'R'){
					$('#approval_cancel_button').show();
					$('#approval_cancel_button').show();
				} else if(${popupid}_param.APPR_STATUS_CD == 'C'){
					$('#approval_rework_button').show();
					$('#approval_cancel_button').show();
				} else {
					$('#approval_rework_button').hide();
					$('#approval_cancel_button').hide();
				}

				${popupid}_gridSearch(${popupid}_param.REQ_ID);
				return true;
			} else {
				${popupid}_param = {};
				${popupid}_vue.form_data = ${popupid}_param;
				alert(msg_select_first);
				return false;
			}
		}

		function ${popupid}_onAfterOpen(){
			$('#${popupid} li:first > a').tab('show');
		}

		function ${popupid}_rework(){
			if(!confirm('<spring:message code="msg_approval_rework_confirm"/>')) return;
			var param = {
				REQ_ID: ${popupid}_param.REQ_ID,
				INS_ID: ${popupid}_param.INS_ID
			}
			post('/api/request_history/rework', param,  function(data){
				if(confirm(msg_complete_work_move)) {
					window.location.href = '/clovirsm/workflow/work/index.jsp';
				} else {
					search();
					$('#${popupid}').modal('hide');
				}
			});
		}
		function ${popupid}_cancel(){
			if(!confirm('<spring:message code="msg_approval_cancel_confirm"/>')) return;
			var param = {
				REQ_ID: ${popupid}_param.REQ_ID,
				INS_ID: ${popupid}_param.INS_ID
			}
			post('/api/request_history/cancel', param,  function(data){
				search();
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_approve(){
			if(!confirm('<spring:message code="msg_cofirm_approval_accept"/>')) return;
			var param = {
				REQ_ID: ${popupid}_param.REQ_ID,
				STEP: $('#${popupid}_STEP').val(),
				APPR_COMMENT: $('#${popupid}_APPR_COMMENT').text()
			}
			 
			post('/api/approval/approve', param,  function(data){
				search();
				getQnaReqCnt();
				alert('<spring:message code="msg_alert_approval_accept_success"/>');
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_deny(){
			if(!confirm('<spring:message code="msg_cofirm_approval_deny"/>')) return;
			if($('#${popupid}_APPR_COMMENT').text() == '' && !confirm('<spring:message code="msg_appr_msg_is_empty"/>')) return;
			var param = {
				REQ_ID: ${popupid}_param.REQ_ID,
				STEP: $('#${popupid}_STEP').val(),
				APPR_COMMENT: $('#${popupid}_APPR_COMMENT').text()
			}
			 
			post('/api/approval/deny', param,  function(data){
				search();
				alert('<spring:message code="msg_alert_approval_deny_success"/>');
				$('#${popupid}').modal('hide');
				getQnaReqCnt();
			});
		}

		var ${popupid}_Req = new Req();
		${popupid}_grid;
		${popupid}_appr_grid;
		var vmReq = new Req();
		var fwReq = new Req();
		var imgReq = new Req();

		$(function() {
			var
			${popupid}_columnDefs = [
				{
					headerName : "<spring:message code="NC_REQ_CUD_CD_NM" text="서비스유형" />",
					width:180,
					maxWidth: 180,
					field : "CUD_CD_NM",
					valueGetter: function(params) {
				    	return params.data.SVC_CD_NM + ' ' + params.data.CUD_CD_NM;
					}

				},{
					headerName : "<spring:message code="NC_REQ_SVC_NM" text="서비스명" />",
					width: 326,
					field : "SVC_NM"
				}
			];
			var
			${popupid}_gridOptions = {
				columnDefs : ${popupid}_columnDefs,
				rowSelection : 'single',
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				sizeColumnsToFit:true,
				enableColResize : true,
				enableServerSideSorting: false,
				onRowClicked: function(e){
					var row = e.data;
					if(row.SVC_CD == 'C'){
// 						openVra4Work(row.SVC_ID, row.INS_DT);
						openVra(row.SVC_ID, row.INS_DT);
		    			return;
			    	}else if(row.SVC_CD == 'S'){
						vmReq.getInfo('/api/vm/info?VM_ID='+ row.SVC_ID + '&INS_DT=' + row.INS_DT, function(data){
							rowDetailParam = data;
							if(data.CUD_CD == 'C'){
								$('#vm_detail_tab_popup_button').trigger('click');
							} else if(data.CUD_CD == 'U'){
								$('#vm_update_popup_button').trigger('click');
							} else if(data.CUD_CD == 'D'){
								$('#vm_req_info_popup_button').trigger('click');
							}
						});
					} else if(row.SVC_CD == 'F'){
						fwReq.getInfo('/api/fw/info?FW_ID='+ row.SVC_ID + '&INS_DT=' + row.INS_DT, function(data){
							rowDetailParam = data;
							if(data.CUD_CD == 'C'){
								$('#fw_insert_save_popup_button').trigger('click');
							} else if(data.CUD_CD == 'U'){
								$('#fw_update_popup_button').trigger('click');
							} else if(data.CUD_CD == 'D'){
								$('#fw_req_info_popup_button').trigger('click');
							}
						});
					} else if(row.SVC_CD == 'G'){
						imgReq.getInfo('/api/image/info?IMG_ID='+ row.SVC_ID + '&INS_DT=' + row.INS_DT, function(data){
							rowDetailParam = data;
							if(data.CUD_CD == 'C'){
								$('#img_insert_save_popup_button').trigger('click');
							} else if(data.CUD_CD == 'D'){
								$('#img_req_info_popup_button').trigger('click');
							}
						});
					}
				}
			}
			
			
			
			var
			${popupid}_appr_columnDefs = [
				{
					headerName : "<spring:message code="label_userName" text="이름" />",
					width:180,
					maxWidth: 180,
					field : "APPR_ID"

				},{
					headerName : "<spring:message code="NC_REQ_DETAIL_APPR_STATUS_CD" text="결재상태" />",
					width: 326,
					maxWidth: 326,
					field : "APPR_STATUS_CD",
					valueGetter: function(params) {
				    	if(params.data.APPR_STATUS_CD == "A"){
							return "<spring:message code="btn_approval_accept" text="승인" />"
				    	} else if(params.data.APPR_STATUS_CD == "D"){
							return "<spring:message code="btn_approval_deny" text="반려" />"
				    	}
						
					}
				},{
					headerName : "<spring:message code="NC_REQ_APPR_TMS" text="결재일자" />",
					width: 326,
					maxWidth: 326,
					field : "APPR_TMS",
					valueGetter: function(params) {
				    	return formatDate(params.data.APPR_TMS,'datetime')
					}
				},{
					headerName : "<spring:message code="NC_REQ_DETAIL_APPR_COMMENT" text="결재의견" />",
					width: 326,
					maxWidth: 326,
					field : "APPR_COMMENT"
				}
				
			];
			var
			${popupid}_appr_gridOptions = {
				columnDefs : ${popupid}_appr_columnDefs,
				rowSelection : 'single',
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				sizeColumnsToFit:true,
				enableColResize : true,
				enableServerSideSorting: false
			}
			
			${popupid}_grid = newGrid("${popupid}_grid", ${popupid}_gridOptions);
			${popupid}_appr_grid = newGrid("${popupid}_appr_grid", ${popupid}_appr_gridOptions);
		});

		
		function openVra(SVC_ID, INS_DT){
			var pramTemp = new Object();
			pramTemp.CATALOGREQ_ID = SVC_ID;
			pramTemp.INS_DT = INS_DT;
			
			post('/api/vra_catalog/reqUpdateList',pramTemp,function(data){
				vraInfo = data;
				$('#newVRAPopup_button').click();
			});
		}
		
		$("#approval_req_form_popup").click(function(){
			clearInterval(re);
		});
		// 조회
		function ${popupid}_gridSearch(REQ_ID, INS_DT) {
			${popupid}_Req.setSearchData({REQ_ID: REQ_ID});
			${popupid}_Req.search('/api/approval/detail_list', function(data) {
				${popupid}_grid.setData(data);
			});
			${popupid}_Req.search('/api/approval/appr_list', function(data) {
				${popupid}_appr_grid.setData(data);
			});
		}

	</script>
</fm-modal>