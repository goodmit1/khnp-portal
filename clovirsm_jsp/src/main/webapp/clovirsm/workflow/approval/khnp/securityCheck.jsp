<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<form id="${popupid}_security" action="none">
    <div class="panel-body">
        <div class="col col-sm-12">
            <div>
                <label for="SEC_PRECAUTION_CMT" class="control-label grid-title value-title" style="min-height: 90px; padding-top: 26px;"><spring:message code="NC_OS_TYPE_SEC_PRECAUTION_CMT" text="보안 유의사항 확인" /></label>
                <div class="output  hastitle" style="min-height: 75px;">
                    <input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/>
                    <div v-html="cmtValue(${popupid}_security.form_data.SEC_PRECAUTION_CMT)" style="overflow-y: auto;word-break: break-word; display: inline;"></div>
                </div>
            </div>
        </div>

        <div class="col col-sm-12"  style="border-left-color: transparent;border-right-color: transparent;" v-if="${popupid}_security.form_data.THIN_REQ_YN == 'Y'">
            <div class="fm-output">
                <label class="control-label grid-title value-title" style="min-width: 100%;">접속 단말 보안 예외 신청 항목</label>
            </div>
        </div>
        <div class="col col-sm-12" v-if="${popupid}_security.form_data.THIN_REQ_YN == 'Y'">
            <div class="fm-output">
                <label class="control-label grid-title value-title">NAC 설치</label>
                <div class="output  hastitle">
                    <input type="checkbox" checked="checked" disabled="disabled" style="margin-right: 5px;"/><label>NAC 설치 예회 신청</label>
                </div>
            </div>
        </div>
        <div class="col col-sm-12"v-if="${popupid}_security.form_data.THIN_REQ_YN == 'Y'">
            <div class="fm-output">
                <label class="control-label grid-title value-title">무결성 검사</label>
                <div class="output  hastitle">
                    <input type="checkbox" checked="checked" disabled="disabled" style="margin-right: 5px;"/><label>무결성 검사 예외 신청</label>
                </div>
            </div>
        </div>
        <div class="col col-sm-12" v-if="${popupid}_security.form_data.THIN_REQ_YN == 'Y'">
            <div class="fm-output">
                <label class="control-label grid-title value-title" style="height:180px;">무결성 검사 예외<br/> 보안 프로그램</label>
                <div class="output  hastitle" style="height: 150px;">
                    <input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>백신(V3)</label><br/>
                    <input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>백신(알약)</label><br/>
                    <input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>매체제어(nProtect)</label><br/>
                    <input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>문서보안</label><br/>
                    <input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>출력문보안</label><br/>
                    <input type="checkbox" checked="checked" disabled="disabled" style="margin-right:5px;"/><label>자산관리</label>
                </div>
            </div>
        </div>
        <div class="col col-sm-12" v-if="${popupid}_security.form_data.THIN_REQ_YN == 'Y'">
            <fm-output id="THIN_REQ_CMT" name="THIN_REQ_CMT" title="<spring:message code="NC_THIN_CLIENT_THIN_REQ_CMT" text="예외사유" />" :value="${popupid}_security.form_data.THIN_REQ_CMT"></fm-output>
        </div>
    </div>
</form>

<script>
    var ${popupid}_sec_param = {VM_ID:'',VM_NM:'',THIN_REQ_YN:'',THIN_REQ_CMT:'',SEC_PRECAUTION_CMT:''};
    var ${popupid}_security = new Vue({
        el: '#${popupid}_security',
        data: {
            form_data: ${popupid}_sec_param,
        }
    });

    function cmtValue(cmt){

        var cmtArr = String(cmt).split("\n");

        var str = "";
        if(cmtArr.length > 1){
            for(var i = 0 ; i < cmtArr.length ; i++){
                str += cmtArr[i] + "<br>";
            }
        } else{
            if(cmt != null && cmt != 'undefined')
                str = cmt;
        }
        return str;
    }
</script>