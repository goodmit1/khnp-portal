<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	 
%>
<style>

#${popupid} .modal-content{
	width:1000px;
	height:700px
}

#${popupid} .modal-content td,#${popupid} .modal-content th{
	padding:10px !important;
}
</style>

<fm-modal id="${popupid}" title="<spring:message code="new_vra_detail" text="서비스 요청 상세"/>" cmd="header-title"  >
 <table>
 <tr><th><spring:message code="STATE" text="상태" />:</th>
 <td>{{form_data.APPR_STATUS_CD_NM}}</td>
<td>{{form_data.APPR_COMMENT}}</td>
</tr>

 </table> 
	 <iframe id="${popupid}_content" style="border:none;width: 100%;height: 560px;overflow-y: auto;"></iframe>
	
</fm-modal>	

<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		  

		 function ${popupid}_click(vue, param)
		 {
			  
			 ${popupid}_vue.form_data=param;
			 $("#${popupid}_content").attr("src","/clovirsm/workflow/requestItemsHistory/detail.jsp?SVC_CD=" + param.SVC_CD + "&SVC_ID=" +  param.SVC_ID + "&INS_DT=" + param.INS_DT);
			 return true;
		 }
	</script>