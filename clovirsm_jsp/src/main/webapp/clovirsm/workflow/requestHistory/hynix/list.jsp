<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<%
	NextApproverService nextApproverService = (NextApproverService)SpringBeanUtil.getBean("nextApproverService");
	List<String> stepNames = nextApproverService.getAllStepNames();
	request.setAttribute("STEP_NAMES", stepNames);
%>
<!--  접근 목록 조회 -->
<style>
	#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=APP_KIND" id="S_APPR_STATUS_CD" emptystr=" "
                  name="APPR_STATUS_CD" title="<spring:message code="NC_REQ_APPR_STATUS_CD" text="결재상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_REQ_NM" name="REQ_NM" title="<spring:message code="APPLICATION_INFORMATION" text="신청정보" />"></fm-input>
		</div>
		<div class="col">
			<table>
				<tr>
					<td>
						<fm-date id="S_INS_TMS_FROM" name="INS_TMS_FROM" title="<spring:message code="NC_REQ_INS_TMS" text="요청일시"/>"></fm-date>
					</td>
					<td class="tilt">~</td>
					<td>
						<fm-date id="S_INS_TMS_TO" name="INS_TMS_TO" ></fm-date>
					</td>
				</tr>
			</table>
		</div>
		<div class="col btn_group nomargin">
			<fm-popup-button popupid="approval_req_form_popup" style="display:none" popup="../approval/approval_request_popup.jsp?action=cancel" cmd="update" param="approvalReqParam"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button>
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="appProgressBar box_s">
<%-- 	<c:out value="${fn:length(STEP_NAMES)}"></c:out> --%>

	<div class="bar_progress">
		<div class="progress_frist zindex" onClick="approvalWidth(this,'0',0,${fn:length(STEP_NAMES)})">
			<img src="/res/img/hynix/newHynix/approvalIcon/shape-2778.png" >
		</div>
		<img src=""  class="bar_progress_img zindex">
		<div class="progress-done zindex" onClick="approvalWidth(this,'100',100,${fn:length(STEP_NAMES)})">
			<img src="/res/img/hynix/newHynix/approvalIcon/shape-745.png" >
		</div>
		<span>
		<c:if test="${fn:length(STEP_NAMES) >0}">
		<c:set var="num" value="${fn:length(STEP_NAMES)+1}" />
		<c:set var="numC" value="100" />
			<c:forEach items="${STEP_NAMES}" var="name" varStatus="index">
<%-- 				<c:set var="numC" value="${numC/num}" /> --%>
				<div class="progress-coming zindex" style="left:${100/num * index.count}%;" onClick="approvalWidth(this,'${100/num * index.count}',${index.index},${fn:length(STEP_NAMES)});">
					<img src="/res/img/hynix/newHynix/approvalIcon/shape-2782.png" >
				</div>
				<div class="approvalName" style="left:calc(${100/num * index.count}% - 11px);">
					${name}
				</div>
			</c:forEach>
		</c:if>
		</span>
		<div class="Rounded-Rectangle-Full">
			<div class="Rounded-Rectangle"></div>

		</div>
		<div class="approvalName last"> <spring:message code="finish" text="완료"/></div>
	</div>
	<div style="margin-left: 12px;  font-size: 12px;">※&nbsp<spring:message code="appr_info" text="결재단계 클릭시 해당 결재단계 조회가능합니다."/></div>
</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
	<%-- 	<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
		
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 450px; border-top: 1px solid #eeee;" ></div>
	</div>
	</div>
<%-- 	<jsp:include page="../../popup/vra_detail_form_popup.jsp?action=view"></jsp:include> --%>
</div>
<script>
	var req = new Req();
	mainTable;
	var approvalReqParam = null;

	$(function() {
		var
		columnDefs = [
			{
				headerName : "<spring:message code="NC_REQ_REQ_ID" text="No" />",
				maxWidth: 160,
				width: 160,
				cellStyle:{'text-align':'right'},
				field : "REQ_ID"
			},{
				headerName : "<spring:message code="APPLICATION_INFORMATION" text="신청정보" />",
				maxWidth: 400,
				width: 380,
				field : "REQ_NM"
			},{
				headerName : "<spring:message code="NC_REQ_APPR_STEP" text="단계" />",
				maxWidth: 100,
				width: 100,
				field : "STEP_NM"
			},{
				headerName : "<spring:message code="NC_REQ_APPR_STATUS_CD" text="결재상태" />",
				maxWidth: 100,
				width: 100,
				field : "APPR_STATUS_CD_NM"
			},{
				headerName : "<spring:message code="NC_REQ_APPR_COMMENT" text="결재의견" />",
				maxWidth: 640,
				width: 460,
				field : "APPR_COMMENT"
			},{
				headerName : "<spring:message code="NC_REQ_APPR_NM" text="결재자" />",
				maxWidth: 160,
				width: 160,
				field : "APPR_USER_NAME"
			},{
				headerName : "<spring:message code="NC_REQ_INS_TMS" text="요청일시"/>",
				maxWidth: 180,
				width: 180,
				field : "INS_TMS",
				valueGetter: function(params) {
			    	return formatDate(params.data.INS_TMS,'datetime')
				}
			},{
				headerName : "<spring:message code="NC_REQ_APPR_TMS" text="결재일자"/>",
				maxWidth: 180,
				width: 180,
				field : "APPR_TMS",
				valueGetter: function(params) {
			    	return formatDate(params.data.APPR_TMS,'datetime')
				}
			},{
				field : 'USER_NAME',
				hide : true
			},{
				field : 'STEP',
				hide : true
			},{
				field : 'APPR_STATUS_CD_T',
				hide : true
			}
		];
		var
		gridOptions = {
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				var arr = mainTable.getSelectedRows();
				if(arr.length > 0){
					var nc_req = arr[0];
			    	approvalReqParam = nc_req;
			    	$('#approval_req_form_popup_button').trigger('click');
				}
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
// 		var today = formatDate(new Date(), 'date');
// 		req.setSearchData({INS_TMS_TO: today, INS_TMS_FROM: today});
		search();
	});

	function approvalWidth(that,wid,num,total){
		var width = null;
		$(".bar_progress .approvalName").removeClass("active")
		req.setSearchData("APPR_STATUS_CD",null);
		if(wid == 100){
			width = parseFloat(wid);
			$(".Rounded-Rectangle").animate({width: ""+width+"%"},400);
			$(".bar_progress > span > .progress-coming").attr("class","progress-present zindex");
			$(".progress-done").attr("style","background-color: #ea4a88;");
			$(".bar_progress .approvalName").eq(total).addClass("active");
			req.setSearchData("STEP",total);
			req.setSearchData("APPR_STATUS_CD","A");
			search();
			return;
		} else if (wid == 0){
			width = parseFloat(wid);
			$(".Rounded-Rectangle").animate({width: ""+width+"%"},400);
			$(".progress-done").attr("style","background-color: #dddddd;");
			$(".bar_progress > span > .progress-present").not(".approvalName").attr("class","progress-coming zindex");
			req.setSearchData("STEP",null);
			width = 0;
			search();
			return;
		}
		width = parseFloat(wid) + 1.2;
		$(".Rounded-Rectangle").animate({width: ""+width+"%"},400);
		$(".progress-done").attr("style","background-color: #dddddd;");
		$(".bar_progress > span > .progress-coming").eq(num).prevAll(".progress-coming").attr("class","progress-present zindex");
		$(".bar_progress > span > .progress-present").eq(num).nextAll(".progress-present").attr("class","progress-coming zindex");
		$(".bar_progress .approvalName").eq(num).addClass("active");

// 		that.setAttribute("src","/res/img/progress-present.png");
		that.setAttribute("class","progress-present zindex");
		req.setSearchData("STEP",num+1);
		search();
	}

	function initFormData()
	{
		 
	}
	// 조회
	function search() {
		req.search('/api/request_history/list' , function(data) {
			mainTable.setData(data);
		});
	}

	// 엑셀 내보내기
	function exportExcel() {
		exportExcelServer("mainForm", '/api/request_history/list_excel', 'RequestHistory',mainTable.gridOptions.columnDefs, req.getRunSearchData())
	}
</script>