<%@page import="com.fliconz.fm.mvc.util.MsgUtil"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%

request.setAttribute("MENU_TITLE", MsgUtil.getMsgWithDefault("TITLE_ADD_TEMPLATE" , new String[]{}, "신규 서비스 요청"));
%>
<layout:extends name="base/index">
	<layout:put block="content">
    	<form id="mainForm"  >
    		<fmtags:include page="list.jsp" />
    	</form>
	</layout:put>
	<layout:put block="popup_area">
	</layout:put>
</layout:extends>