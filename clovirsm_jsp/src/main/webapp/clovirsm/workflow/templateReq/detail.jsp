<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!-- 입력 Form -->
<div class="form-panel detail-panel panel panel-default">
	<div class="panel-body">
		<div class="col col-sm-6">
			<fm-output id="CATEGORY_NMS" name="CATEGORY_NMS"   title="<spring:message code="TASK" text="업무" />" ></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="TITLE" name="TITLE"   title="<spring:message code="label_title" text="제목" />" ></fm-output>
		</div>
		
		<div class="col col-sm-3">
			<fm-output id="OS" name="OS"   title="OS" ></fm-output>
		</div>
		<div class="col col-sm-3">
			<fm-output id="OS_VER" name="OS_VER"   title="<spring:message code="OS_VER" text="OS 버젼" />" ></fm-output>
		</div>
		<div class="col col-sm-3">
			<fm-output id="CPU_CNT" name="CPU_CNT"   title="CPU" ></fm-output>
		</div>
		<div class="col col-sm-3">
			<fm-output id="RAM_SIZE" name="RAM_SIZE"   title="Memory(GB)" ></fm-output>
		</div>
		<div class="col col-sm-3">
			<fm-output id="DISK_SIZE" name="DISK_SIZE"   title="Disk(GB)" ></fm-output>
		</div>
		<div class="col col-sm-3">
			<fm-output id="DISK_PATH" name="DISK_PATH"   title="Disk Path" ></fm-output>
		</div>
		<div class="col col-sm-3" style="">
			<div>
				<label for="STATE_NM" class="control-label grid-title value-title"><spring:message code="NC_REQ_ATT_FILE_PATH"/></label>
				<jsp:include page="/home/common/_fileAttach.jsp">
					<jsp:param value="template_req" name="kubun"/>
				</jsp:include>
			</div>
		</div>
		<div class="col col-sm-3">
			<fm-output id="STATE_NM" name="TASK_STATUS_CD_NM"   title="<spring:message code="NC_IMG_TASK_STATUS_CD_NM" text="결제 상태" />" ></fm-output>
		</div>
		<div class="col col-sm-12">
			<fm-output id="INSTAL_SW" name="INSTAL_SW"   title="S/W <spring:message code="NC_VRA_CATALOG_CMT" text="설명"/>" label_style="height: 150px;" div_style="white-space: pre;height: 150px;"></fm-output>
		</div>
		<div class="col col-sm-12">
			<fm-output id="ETC" name="ETC"   title="<spring:message code="ETC" text="기타"/>" label_style="height: 150px;" div_style="white-space: pre;height: 150px;"></fm-output>
		</div>
		<div class="col col-sm-3">
			<fm-output id="INS_ID" name="USER_NAME"  title="<spring:message code="NC_REQ_INS_ID_NM" text="요청자" />" ></fm-output>
		</div>
		<div class="col col-sm-3">
			<fm-output id="INS_TMS" name="INS_TMS" :value="formatDate(form_data.INS_TMS, 'datetime')"   title="<spring:message code="REQ_TMS" text="요청일" />" ></fm-output>
		</div>
		<div class="col col-sm-3">
			<fm-output id="APPR_NM" name="APPR_NM"   title="<spring:message code="worker" text="작업자" />" ></fm-output>
		</div>
		<div class="col col-sm-3">
			<fm-output id="APPR_TMS" name="APPR_TMS" :value="formatDate(form_data.APPR_TMS, 'datetime')"  title="<spring:message code="Working_date" text="작업일" />" ></fm-output>
		</div>
		<div class="col col-sm-12">
			<fm-output id="APPR_COMMENT" name="APPR_COMMENT"   title="<spring:message code="ADMIN_CMT" text="관리자 의견"/>" label_style="height: 50px;" div_style="white-space: pre;height: 50px;"></fm-output>
		</div>
	</div>
</div>
