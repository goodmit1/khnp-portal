<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<!--  접근 목록 조회 -->
<style>
	#mainTable { height:calc(100vh - 340px); }
	.fileAttachViewArea{display: inline-block;}
	.choice {
	    background-color: #d2e2e4;
	    border: 1px solid #aaa;
	    border-radius: 4px;
	    cursor: default;
	    float: left;
	    margin-right: 5px;
	    /* margin-top: 5px; */
	    padding: 0 5px;
	    position: relative;
	    list-style: none;
	    left: -30px;
	    top: 10px;
	}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		
		<div class="col col-sm">
			<fm-input id="S_TITLE" name="TITLE" title="<spring:message code="label_title" text="제목" />" input_style="width: 175px;"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select id="S_TASK_STATUS_CD" name="TASK_STATUS_CD" title="<spring:message code="NC_IMG_TASK_STATUS_CD_NM" text="작업 상태" />" url="/api/code_list?grp=TASK_STATUS_CD1" emptyStr="" ></fm-select>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
	<%-- 	<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<c:if test="${ADMIN_YN eq 'N'}">
				<fm-sbutton cmd="search" class="exeBtn contentsBtn tabBtnImg save" onclick="addTemplate()"><spring:message code="label_add" text="추가"/></fm-sbutton>
			</c:if>
			<c:if test="${ADMIN_YN eq 'Y'}">
				<fm-sbutton cmd="search" class="adminBtn exeBtn contentsBtn tabBtnImg save" cmd="update" onclick="approveTemplateBtn()"><spring:message code="btn_task_yes" text="작업완료"/></fm-sbutton>
				<fm-sbutton cmd="search" class="adminBtn exeBtn contentsBtn tabBtnImg del" cmd="update" onclick="denyTemplate()"><spring:message code="btn_task_deny" text="작업 반려"/></fm-sbutton>
			</c:if>
		</div>
		
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 350px;" ></div>
		<!-- fm-popup-button popupid="approveTemplate_popup" style="display:none" popup="/clovirsm/popup/approveTemplate_popup.jsp" cmd="update" param="mainTable.getSelectedRows()[0]"><spring:message code="tab_approval_request" text="결재 요청서"/></fm-popup-button-->
		 
		<fm-popup-button style="display: none;" popupid="denyTemplate_popup" popup="/clovirsm/popup/reason_popup.jsp?title=btn_task_deny" param="mainTable.getSelectedRows()[0]" callback="denyTemplateBtn"></fm-popup-button>
	
	</div>
	<jsp:include page="detail.jsp"></jsp:include>
	
</div>
<script>
	var req = new Req();
	mainTable;
	var approvalReqParam = null;

	$(function() {
		var
		columnDefs = [
			{
				field : "TEMPLATE_ID",
				hide : true
			},{
				headerName : "<spring:message code="TASK" text="업무" />",
				width: 250,
				field : "CATEGORY_NMS"
			}
			,{
				headerName : "<spring:message code="label_title" text="제목" />",
				width: 250,
				field : "TITLE"
			},{
				headerName : "<spring:message code="REQ_TMS" text="요청일시"/>",
				width: 200,
				field : "INS_TMS",
				valueGetter: function(params) {
			    	return formatDate(params.data.INS_TMS,'datetime')
				}
			},{
				headerName : "<spring:message code="NC_REQ_INS_ID_NM" text="" />",
				width: 150,
				field : "USER_NAME" 
			},{
				headerName : "<spring:message code="Working_date" text=""/>",
				width: 200,
				field : "APPR_TMS",
				valueGetter: function(params) {
			    	return formatDate(params.data.APPR_TMS,'datetime')
				}
			},{
				headerName : "<spring:message code="NC_IMG_TASK_STATUS_CD_NM" text="작업 상태" />",
				width: 100,
				field : "TASK_STATUS_CD_NM"
			}
		];
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
// 			rowModelType: 'infinite',
			rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onRowClicked: function(){
				var arr = mainTable.getSelectedRows();
				 
				template_reqfileAttach_list(arr[0].TEMPLATE_ID);
				req.getInfo('/api/template_req/info?TEMPLATE_ID='+ arr[0].TEMPLATE_ID, function(data){
					if(data.TASK_STATUS_CD=='W'){
						$(".adminBtn").prop("disabled", false);
					}
					else{
						$(".adminBtn").prop("disabled", true);
					}
				});
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
// 		var today = formatDate(new Date(), 'date');
// 		req.setSearchData({INS_TMS_TO: today, INS_TMS_FROM: today});
		
		search();
	});
	function approveTemplateBtn(){
		 
		form_data.TASK_STATUS_CD = 'S' ;
		 
		post('/api/template_req/save', form_data, function(){
			search();
			alert(msg_complete) 
		})
	}
	function initFormData(){
		search_data.TASK_STATUS_CD='W'
	}
	function denyTemplateBtn(cd, cmt){
		var param = mainTable.getSelectedRows()[0] ;
		param.APPR_COMMENT= cmt;
	 
		param.TASK_STATUS_CD= 'D'
	 
		post('/api/template_req/save', param, function(){
			search();
			 
		})
	}
	// 조회
	function search() {
		req.search('/api/template_req/list' , function(data) {
			mainTable.setData(data);
		});
	}
	//검색 카테고리
	function select_S_CATEGORY(data){
		 
		searchvue.form_data.CATEGORY_ID = data.CATEGORY;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}
	//입력 카테고리
	function select_CATEGORY(data){
		inputvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#CATEGORY_NM").val(data.CATEGORY_NM);
	}
	// 엑셀 내보내기
	function exportExcel() {
		mainTable.exportCSV({fileName:'templateReq'});
	}
	function addTemplate(){
		location.href="/clovirsm/workflow/templateAdd/index.jsp"
	}
	function approveTemplate(){
		$("#approveTemplate_popup_button").click();
	}
	function denyTemplate(){
		$("#denyTemplate_popup_button").click();
	}
</script>