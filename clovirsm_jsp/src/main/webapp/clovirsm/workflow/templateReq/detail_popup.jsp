<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	 
%>
<style>

#${popupid} .modal-content{
	width:1200px;
	height:600px
}

#${popupid} .modal-body {
     
    overflow: auto;
    height: 500px;
 }
</style>

<fm-modal id="${popupid}" title="<spring:message code="new_template_req_detail" text="템플릿 요청 상세"/>" cmd="header-title"  >
	 
		
 
	<div class="form-panel detail-panel"   >
		 <jsp:include page="detail.jsp"></jsp:include>
	</div>
	<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		  

		 function ${popupid}_click(vue, param)
		 {
			 	post('/api/template_req/info', param, function(data){
			 		${popupid}_vue.form_data=data;
			 	});
				 
			  
			 return true;
		 }
	</script>
	<style>

	</style>
</fm-modal>