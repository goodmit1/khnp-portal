<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("isEditable", !"view".equals(action));
%>
<fm-modal id="${popupid}" title="<spring:message code="label_fw_req" text="" />" >
	<span slot="footer">
		<input id="fw_${popupid}_save_button" type="button" class="btn saveBtn" value="<spring:message code="btn_save" text="" />" onclick="fw_${popupid}_save()" v-if="${isEditable} && '${popupid}' != 'fw_delete_req_form_popup'" />
	</span>
	<form id="${popupid}_form" action="none">
		<fm-output id="${popupid}_DC_NM" name="DC_NM"  title="<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>"></fm-output>
		<fm-output id="${popupid}_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="서버명"/>"></fm-output>
		<fm-output id="${popupid}_PUBLIC_IP" name="PUBLIC_IP"  title="<spring:message code="NC_FW_SERVER_IP" text="서버IP"/>"></fm-output>
		<fm-input id="${popupid}_CIDR" name="CIDR" title="<spring:message code="NC_VM_CIDR" text="대상IP"/>" v-if="${isEditable} && '${popupid}' == 'fw_insert_req_form_popup'"></fm-input>
		<fm-output id="${popupid}_CIDR" name="CIDR" title="<spring:message code="NC_VM_CIDR" text="대상IP"/>" v-if="${!isEditable} || '${popupid}' != 'fw_insert_req_form_popup'"></fm-output>
		<fm-input id="${popupid}_PORT" name="PORT" title="<spring:message code="NC_VM_PORT" text="Port"/>" v-if="${isEditable} && '${popupid}' != 'fw_delete_req_form_popup'"></fm-input>
		<fm-output id="${popupid}_PORT" name="PORT" title="<spring:message code="NC_VM_PORT" text="Port"/>" v-if="${!isEditable} || '${popupid}' == 'fw_delete_req_form_popup'"></fm-output>
		<fm-output id="${popupid}_USE_MM" name="USE_MM" title="<spring:message code="NC_VM_USE_MM" text="사용기간(개월)"/>"></fm-output>
		<fm-output id="${popupid}_CMT" name="CMT" title="<spring:message code="NC_VM_CMT" text="설명"/>"></fm-output>
		<fm-output id="${popupid}_TEAM_NM" name="TEAM_NM"  title="<spring:message code="NC_VM_TEAM_NM" text="사용자 팀"/>"></fm-output>
		<fm-output id="${popupid}_USER_NAME" name="USER_NAME" title="<spring:message code="FM_USER_USER_NAME" text="사용자"/>"></fm-output>
	</form>
	<script>
		var popup_${popupid}_form = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {
			VM_NM: '',
			CMT: '',
			DC_ID: -1,
			DC_NM: '',
			IMG_ID: -1,
			IMG_NM: '',
			SPEC_ID: -1,
			SPEC_NM: '',
			TEAM_NM: ''
		};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		function ${popupid}_click(vue, param){
			if(param.FW_ID){
				${popupid}_param = $.extend({}, param);
				${popupid}_vue.form_data = ${popupid}_param;
			} else {
				${popupid}_param = {};
				${popupid}_vue.form_data = ${popupid}_param;
			}
			return true;
		}

		function ${popupid}_onAfterOpen(){

		}

		function fw_${popupid}_save(){
			var action = 'insertReq';
			if(${popupid}_param.CUD_CD == 'U') action = 'updateReq';
			else if(${popupid}_param.CUD_CD == 'D') return;
			post('/api/fw/' + action, ${popupid}_param,  function(data){
				alert(msg_complete_work);
				workSearch();
				$('#${popupid}').modal('hide');
			});
		}
	</script>
</fm-modal>