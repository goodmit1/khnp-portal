<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="java.util.Map"%>
<%@page import="com.fliconz.fw.runtime.util.PropertyManager"%>
<%@page import="com.clovirsm.service.workflow.NextApproverService"%>
<%

	session.setAttribute("NEXT_APPR_ID", PropertyManager.getString("workflow.line.NEXT_APPR_ID"));
	NextApproverService nextApproverService = (NextApproverService)SpringBeanUtil.getBean("nextApproverService");
	Map ListName = null;
	ListName = nextApproverService.getListName();
	if(ListName != null){
		request.setAttribute("ListName", ListName.get("NEXT_APPR_ID"));
	}
%>
<style>
	#work_form_popup .modal-dialog {
		width: 700px;
	}
	#F_CMT{
		width:74%;
	}
</style>
<fm-modal id="work_form_popup" title="<spring:message code="title_approval_request"/>" >
	<span slot="footer">
		<input type="button" class="btn" value="<spring:message code="btn_save" text="" />" onclick="work_form_submit()" />
	</span>
	<div class="form-panel detail-panel">
		<form id="work_form_popup_form" action="none">
			<div class="panel-body">
				<div class="col col-sm-12">
					<fm-input id="F_REQ_NM" name="REQ_NM" title="<spring:message code="NC_REQ_REQ_NM" text="제목"/>"></fm-input>
				</div>
				<div class="col col-sm-12" style="height: 140px;">
					<fm-textarea id="F_CMT" name="CMT" title="<spring:message code="NC_REQ_CMT" text="내용"/>"></fm-textarea>
				</div>
				<c:if test="${_USER_TYPE_ eq 18}">
					<div class="col col-sm-12" >
						<fm-select id="NEXT_APPROVAL"  keyfield="USER_ID" titlefield="USER_NAME" emptystr=""  url="/api/nextApprover/list/nextApproverList/?NEXT_APPR_ID=${NEXT_APPR_ID}" name="NEXT_APPROVAL" title="<spring:message code="next_approval_name" text="다음 결재자"/>"></fm-select>
					</div>
				</c:if>
			</div>
		</form>
	</div>
	
	<script>
	
		var ListName = "${ListName}";
		if(ListName != null){
			setTimeout(function(){ chageListName(); }, 100);
		}
		var popup_work_form = newPopup("work_form_popup_button", "work_form_popup");
		var work_form_popup_param = {
			REQ_NM: '',
			CMT: ''
		};
		function chageListName(){
			$("#NEXT_APPROVAL").val(${ListName}).prop("selected", true);
		}
		var work_form_popup_vue = new Vue({
			el: '#work_form_popup',
			data: {
				form_data: work_form_popup_param
			}
		});
		function work_form_popup_click(vue, rows){
			if(rows.length > 0){
				var req_nm = rows[0].SVC_CD_NM + rows[0].CUD_CD_NM + '('+rows[0].SVC_NM+')';
				if(rows.length > 1) {
					req_nm += getMessage("<spring:message code="msg_work_form_popup_1"/>", rows.length-1);
				}
				work_form_popup_param.REQ_NM = req_nm;
				work_form_popup_param.checked = rows;
				work_form_popup_vue.form_data = work_form_popup_param;

				$("#F_REQ_NM").select();
				return true;
			}
			work_form_popup_param = null;
			work_form_popup_vue = null;
			alert(msg_select_first);
			return false;
		}

		function work_form_submit(){
			if($('#F_REQ_NM').val() == '') {
				alert('<spring:message code="msg_input_req_title"/>');
				$('#F_REQ_NM').select();
				return;
			}
			if($("#NEXT_APPROVAL").val() == ''){
				alert('<spring:message code="nextapproval_error"/>');
				$('#NEXT_APPROVAL').select();
				return;
			}
			var param = {
				checked: JSON.stringify(work_form_popup_param.checked),
				selRow: JSON.stringify({
					REQ_NM: $('#F_REQ_NM').val(),
					CMT: $('#F_CMT').val(),
					NEXT_APPR_ID: $('#NEXT_APPROVAL').val()
				})
			}
			post('/api/work/request', param,  function(data){
				alert(msg_complete_work);
				workflowSearch();
				$('#work_form_popup').modal('hide');
			});
		}
		
	

	</script>
</fm-modal>