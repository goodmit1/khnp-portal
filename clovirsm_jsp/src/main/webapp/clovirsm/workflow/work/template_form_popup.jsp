<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("isEditable", !"view".equals(action));
%>
<fm-modal id="${popupid}" title="<spring:message code="label_img_req" text="" />">
	<span slot="footer">
		<input id="template_${popupid}_save_button" type="button" class="btn" value="<spring:message code="btn_save" text="" />" onclick="template_${popupid}_save()" v-if="${isEditable} && '${popupid}' != 'template_delete_req_form_popup'" />
	</span>
	<form id="${popupid}_form" action="none">
		<input type="hidden" name="VM_ID" />
		<fm-output id="${popupid}_VM_NM" name="VM_NM" title="<spring:message code="NC_IMG_FROM_NM" text="원본 서버명"/>"></fm-output>
		<fm-output id="${popupid}_IMG_NM" name="IMG_NM" title="<spring:message code="NC_IMG_IMG_NM" text="템플릿명"/>"></fm-output>
		<fm-input id="${popupid}_CMT" name="CMT" title="<spring:message code="NC_VM_CMT" text="설명"/>" disabled="disabled"></fm-input>
		<fm-output id="${popupid}_DC_NM" name="DC_NM"  title="<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>"></fm-output>
		<fm-output id="${popupid}_TEAM_NM" name="TEAM_NM"  title="<spring:message code="NC_VM_TEAM_NM" text="팀명"/>"></fm-output>
		<fm-output id="${popupid}_OS_NM" name="OS_NM" title="<spring:message code="NC_VM_OS_NM" text="OS명"/>"></fm-output>
	</form>
	<script>
		var popup_${popupid}_form = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {
			VM_ID: -1,
			VM_NM: '',
			CMT: '',
			DC_ID: -1,
			DC_NM: '',
			IMG_ID: -1,
			IMG_NM: '',
			TEAM_NM: '',
			OS_NM: '',
		};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		function ${popupid}_click(vue, param){
			if(param.IMG_ID){
				${popupid}_param = $.extend({
					VM_ID: -1,
					VM_NM: '',
					CMT: '',
					DC_ID: -1,
					DC_NM: '',
					IMG_ID: -1,
					IMG_NM: '',
					TEAM_NM: '',
					OS_NM: '',
				}, param);
				${popupid}_vue.form_data = ${popupid}_param;
			} else {
				${popupid}_param = {};
				${popupid}_vue.form_data = ${popupid}_param;
			}
			return true;
		}

		function ${popupid}_onAfterOpen(){

		}

		function template_${popupid}_save(){
			var action = 'insertReq';
			if(${popupid}_param.CUD_CD == 'U') action = 'updateReq';
			else if(${popupid}_param.CUD_CD == 'D') return;
			post('/api/image/' + action, ${popupid}_param,  function(data){
				alert(msg_complete_work);
				workSearch();
				$('#${popupid}').modal('hide');
			});
		}

	</script>
</fm-modal>