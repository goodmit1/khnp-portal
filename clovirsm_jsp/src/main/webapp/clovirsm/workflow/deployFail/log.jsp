<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><div id="log_popup" class="modal  fade popup_dialog" role="dialog">
		<div class="modal-dialog  "  >
			<!-- Modal content-->
			<div class="modal-content  container-fluid" >
				<div class="modal-header header-title">
					<button type="button" id="logCloseBtn" class="close" data-dismiss="modal"></button>
					<spring:message code="title_log" text="에러로그" />
				</div>
	
				<div class="modal-body">
    			<iframe name="logFrame" id="logFrame" scrolling="no"
					style="height: 400px; width: 100%; border: 0;" ></iframe>  
    			</div>
    			<script>
    			var log_popup_button = newPopup("log_popup_button", "log_popup");
    			function log_popup_click(obj,param){
    				showErrMsg(param.log)
    				return true;
    			}
    			function showErrMsg(msg){
    				$("#logFrame").attr("src", "/clovirsm/workflow/deployFail/_log.jsp");
    				setTimeout( function(){ 
    					document.getElementById("logFrame").contentWindow.showErrMsg(msg)
    				}, 1000);
    			}</script>
    		</div>
    	</div>
    	</div>	