<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<div class="col col-sm">
	<fm-input id="S_APPR_USER_NAME" name="APPR_USER_NAME" title="<spring:message code="NC_REQ_APPR_NM" text="결재자" />"></fm-input>
</div>