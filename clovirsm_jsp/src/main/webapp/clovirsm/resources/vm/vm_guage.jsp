<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<link rel="Stylesheet" type="text/css" href="/res/css/vm_chart.css" />
<script src="/res/js/perf-chart.js"></script>

 <div id="vmChartArea">
<jsp:include page="/clovirsm/monitor/_time_sel_area.jsp"></jsp:include>

<table ><tr><td>
<div class="content30">
<div class="title">CPU</div>
 <div class="arrow-down"></div>
<div class="chartHeader">
<span class="per cpu" id="cpu_per"></span>
 <!--
<span class="info">( <span class="cpu cur" id="cpu_cur"></span> / <span id="cpu_total"></span> ) </span>
 -->
</div>

<div id="container_cpu_usage" class="chart"></div>
</div>
</td><td>
<div class="content30">
<div class="title">Memory</div>
 <div class="arrow-down"></div>
<div class="chartHeader">
<span class="per mem" id="mem_per"></span>
<span class="info">( <span class="mem cur" id="mem_cur"></span> / <span id="mem_total"></span> ) </span>
</div>
<div id="container_mem_usage" class="chart"></div>
</div>
 </td><td>
<div class="content30">
<div class="title">Network Speed</div>
 <div class="arrow-down"></div>
<div class="chartHeader">
<span class="per disk" id="net_usage"></span>
<!--
<span class="info">( <span class="disk cur" id="disk_cur">10GHz</span> / <span class="mem_total">40GHz</span> ) </span>
 -->
</div>

<div id="container_net_usage" class="chart"></div>
</div>
</td></tr></table>
</div>
 


<script>
	//<![CDATA[
var oldPeriodId='1h' ;
function getGaugeData()
{
	post('/api/vm/guageData', form_data, function(data){
		onAfterGauge(data);
	})
}
function reloadVMPerf()
{
	if(form_data.VM_ID!=null && form_data.VM_ID)
	{
		var param1 = {}
		param1.PERIOD = oldPeriodId;
		param1.START_DT = $("#START_DT").val();
		param1.FINISH_DT = $("#FINISH_DT").val();
		post('/api/monitor/perf_history/' + form_data.DC_ID + '/VM/' + form_data.VM_ID +'/',  param1, function(data)
				{
					this.drawVMPerfArea(  data.LIST );
				}
			)
	}
}
function drawVMPerfArea(data)
{
	var dataMap = makeMap(data);
	makeLineChart("cpu_usage", dataMap, 'area','#2e8fe2',100);
	makeLineChart('mem_usage',dataMap, 'area','#ffaa28',100);
	makeLineChart('net_usage',dataMap, 'area','#54bf19');
	if(dataMap.net_usage)
	{
		var netValList = dataMap.net_usage.valList;
		var net_usage=netValList[netValList.length-1][1]
		selText("net_usage",	 net_usage , "KBPs" );
	}
}
$(document).ready(function()
{
	chkPeriod($("#p_1h")[0]);
	$("#vmChartArea dd").click(function()
	{

		chgPeriod(this);
	}
	);
}
);
function chkPeriod( obj )
{
	var val = $(obj).attr("period");
	if(oldPeriodId)
	{
		$("#p_" + oldPeriodId).removeClass("selected");
	}
	oldPeriodId = val;
	$(obj).addClass("selected");
	if(val == null || "" == val){
		$(obj).parent().find("span[id$='hideArea']").show();
		return true;
	}else{
		$("#START_DT").val(null);
		$("#FINISH_DT").val(null);

		$(obj).parent().find("span[id$='hideArea']").hide();
	}

}
function chgPeriod( obj )
{
	if(chkPeriod(obj) == null){
		reloadVMPerf();
	}
}
function reloadMonitor()
{

	if(form_data.VM_ID!=null && form_data.VM_ID)
	{
		reloadVMPerf();
		getGaugeData();
	}


}

function selText(id , val, unit )
{
	if(!val)
	{
		val = "0";
	}

	$("#" + id).text(val + unit);
}
function onAfterGauge(data1)
{

	var data = data1.LIST;
	var mem_total = form_data.MEM_SIZE;
	var cpu_usage = data.cpu_usage;
	var mem_usage = data.mem_usage;
	var disk_usage = data.disk_usage;

	//selText("cpu_cur",args.cpu_usagemhz , "MHz");
	//selText("cpu_total", parseInt( args.cpu_usagemhz / cpu_usage * 100  ) , "MHz");
	selText("cpu_per",  (  cpu_usage  )   , "%");

	selText("mem_cur",   mem_usage * mem_total  / 100  , "GB");
	selText("mem_total", mem_total  ,"GB");
	selText("mem_per",   mem_usage      ,"%");



}
//]]>
</script>


</html>