<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<fm-modal id="vm_copy_popup" title="<spring:message code="label_vm_copy"/>" >
	<span slot="footer">
		<input type="button" class="btn saveBtn" value="<spring:message code="btn_work" text=""/>" onclick="vm_copy_to_work()" />
		<input type="button" class="btn exeBtn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="vm_copy_request()" />
	</span>
	<div class="form-panel detail-panel">
		<form id="vm_copy_popup_form" action="none">
			<input type="hidden" name="VM_ID" id="SCF_VM_ID"  />
			<input type="hidden" name="SPEC_ID" id="SCF_SPEC_ID"  />
			<input type="hidden" name="CPU_CNT" id="SCF_CPU_CNT"  />
			<input type="hidden" name="DISK_SIZE" id="SCF_DISK_SIZE"  />
			<input type="hidden" name="DISK_UNIT" id="SCF_DISK_UNIT"  />
			<input type="hidden" name="RAM_SIZE" id="SCF_RAM_SIZE"  />
			<div class="panel-body">
				<div class="col col-sm-6">
<!-- 					<fm-select id="SCF_DC_NM" name="DC_ID" -->
<!-- 						url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" -->
<!-- 						keyfield="DC_ID" titlefield="DC_NM" -->
<%-- 						title="<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>"> --%>
<!-- 					</fm-select> -->
					<fm-output id="SCF_DC_NM" name="DC_NM" title="<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>"></fm-output>
				</div>
				<div class="col col-sm-6">
					<fm-output id="SCF_FROM_NM" name="FROM_NM" title="<spring:message code="NC_VM_FROM_NM" text="원본 서버명"/>"></fm-output>
				</div>
			 
				<div class="col col-sm-12"  v-if="${!IS_AUTO_VM_NAME || sessionScope.ADMIN_YN == 'Y'}"  >
		 
		
					<fm-input id="SCF_VM_NM" placeholder="${IS_AUTO_VM_NAME ? '<spring:message code='NC_VM_AUTO_NAME' text='입력하지 않으면 자동 생성'/>':''}" name="VM_NM" 
						title="<spring:message code="NC_VM_VM_NM" text="VM명"/>" ${!IS_AUTO_VM_NAME ? 'required':''}  ></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-select url="/api/code_list?grp=P_KUBUN" id="SCF_P_KUBUN" emptystr=" " name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>"></fm-select>
				</div>
				
				<div class="col col-sm-12">
					<fm-output id="SCF_SPEC_INFO" name="SPEC_INFO" title="<spring:message code="NC_VM_SPEC_NM" text="사양"/>"></fm-output>
				</div>
				<div class="col col-sm-12" style="height: 405px;">
					<div>
						 
						<fm-textarea id="SCF_CMT" name="CMT" style=" display:inline-block;"></fm-textarea>
					</div>
				</div>
			</div>
		</form>
	</div>
	<script>
		$(function(){
			setTimeout(function(){
				$("#SCF_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
			},1000)
		});
		var popup_vm_copy_form = newPopup("vm_copy_popup_button", "vm_copy_popup");
		var vm_copy_popup_param = {};
		var vm_copy_popup_vue = new Vue({
			el: '#vm_copy_popup',
			data: {
				form_data: vm_copy_popup_param
			}
		});
		function vm_copy_popup_click(vue, param){
			 
			if(param.VM_ID){
				vm_copy_popup_param = $.extend({}, param);
				vm_copy_popup_param.FROM_ID = vm_copy_popup_param.VM_ID;
				vm_copy_popup_param.FROM_NM = vm_copy_popup_param.VM_NM;
				vm_copy_popup_param.VM_NM = "";
				vm_copy_popup_vue.form_data = vm_copy_popup_param;
				delete vm_copy_popup_param.VM_ID;
				setTimeout(function(){$("#SCF_CMT").Editor("setText", vm_copy_popup_param.CMT ? vm_copy_popup_param.CMT:"")},1000);
				return true;
			} else {
				vm_copy_popup_param = null;
				alert(msg_select_first);
				return false;
			}
		}

		function vm_copy_to_work(){
			vm_copy_popup_param.CMT = $("#SCF_CMT").Editor("getText" );
			 
			
			post('/api/vm/insertReq', vm_copy_popup_param,  function(data){
				if(confirm(msg_complete_work_move)){
					location.href="/clovirsm/workflow/work/index.jsp";
					return;
				}
				vmSearch();
				$('#vm_copy_popup').modal('hide');
			});
		}

		function vm_copy_request(){
			vm_copy_popup_param.CMT = $("#SCF_CMT").Editor("getText" );
			 
			vm_copy_popup_param.DEPLOY_REQ_YN = 'Y';
			 
			post('/api/vm/insertReq', vm_copy_popup_param,  function(data){
				alert(msg_complete_work);
				vmSearch();
				$('#vm_copy_popup').modal('hide');
			});
		}

	</script>
	<style>
	.row-fluid.Editor-container {
		width:calc(100% - 150px);
		float: right;
	}
	</style>
</fm-modal>