<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<%
String VM_NM = request.getParameter("VM_NM");
request.setAttribute("VM_NM", VM_NM == null ? "null" : "'" + VM_NM + "'");
String DC_ID = request.getParameter("DC_ID");
request.setAttribute("DC_ID", DC_ID == null ? "null" : "'" + DC_ID + "'");


%>
<style>

.input-group.input{
    width: 54%;
}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<c:if test="${sessionScope.GRP_ADMIN_YN == 'Y'}">
			<div class="col col-sm">
				<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" " name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-select>
			</div>
			<div class="col col-sm">
				<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="S_DC_ID"
					emptystr=" " keyfield="DC_ID" titlefield="DC_NM"
					name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
				</fm-select>
			</div>
		</c:if>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=RUN_CD" id="S_RUN_CD" emptystr=" "
				name="RUN_CD" title="<spring:message code="NC_VM_RUN_CD" text="서버상태"/>">
			</fm-select>
		</div>
		<c:if test="${sessionScope.STATE }">
			<div class="col col-sm">
				<fm-select url="/api/code_list?grp=P_KUBUN" id="S_P_KUBUN" emptystr=" "
					name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
				</fm-select>
			</div>
		</c:if>
			<div class="col col-sm">
				<fm-select-yn id="S_EXPIRE_YN" emptystr=" "
					name="EXPIRE_YN" title="<spring:message code="NC_VM_EXPIRE_YN" text="만료여부"/>">
				</fm-select-yn>
			</div>
			<c:if test="${sessionScope.STATE }">
			<div style="display: block; clear: both;">
				<div class="col col-sm">
				<fm-select url="/api/code_list?grp=PURPOSE" id="${popupid}_PURPOSE" name="PURPOSE"
					title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>"
					emptystr=" " keyfield="PURPOSE" titlefield="PURPOSE_NM">
				</fm-select>
				</div>
			</c:if>
			<c:if test="${sessionScope.GRP_ADMIN_YN == 'Y' && sessionScope.STATE }">
				<div class="col col-sm"  >
					<fm-select url="/api/site/querykey/selectPartCategory/" id="S_CATEGORY"
						emptystr="" keyfield="CATEGORY_ID" titlefield="CATEGORY_NM"
						name="CATEGORY" title="<spring:message code="PART" text="부품"/>">
					</fm-select>
				</div>
			</c:if>
			<div class="col col-search">
				<fm-input id="keyword_input" class="keyword" name="keyword"
					placeHolder="<spring:message code="NC_VM_VM_NM" text="서버명" />/<spring:message code="NC_VM_GUEST_NM" text="OS명" />/<spring:message code="NC_VM_PRIVATE_IP" text="IP" />/<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"
					title="<spring:message code="keyword_input" text="키워드" />">
				</fm-input>
				<fm-sbutton cmd="search" class="searchBtn" onclick="vmSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
			</div>
			<div class="col btn_group_under">
				<button type="button" onclick="moveInsert();" class="btn btn-primary fm-popup-button newBtn"><spring:message code="btn_server_create" text="서버생성요청"/></button>
				<fm-popup-button popupid="vm_copy_popup" popup="/clovirsm/resources/vm/mobis/vm_copy_popup.jsp" cmd="update" class="newBtn" param="vmReq.getData()"><spring:message code="btn_server_copy" text="서버복사요청"/></fm-popup-button>
				<fm-popup-button popupid="img_insert_popup" popup="../../popup/mobis/img_detail_form_popup.jsp?action=insert" cmd="newBtn" param="vmReq.getData()"><spring:message code="btn_image_create" text="개인템플릿생성요청"/></fm-popup-button>
				<fm-popup-button popupid="vm_update_popup" popup="/clovirsm/popup/vm_detail_form_popup.jsp?action=update&callback=onAfterModify" cmd="update" class="saveBtn" param="vmReq.getData()"><spring:message code="btn_spec_modify" text="사양 변경"/></fm-popup-button>
				<fm-popup-button popupid="vm_delete_popup" popup="../../popup/delete_req_form_popup.jsp?svc=vm&key=VM_ID&callback=vmSearch" cmd="delete" class="delBtn" param="vmReq.getData()"><spring:message code="btn_delete_req" text="삭제요청"/></fm-popup-button>
				<c:if test="${sessionScope.ADMIN_YN == 'Y'}">
				<input type="button" onclick="javascript:syncAll();" class="btn btn-primary fm-popup-button newBtn update" value="Sync">
	
				<fm-popup-button popupid="get_vm_form_popup" popup="get_vm_form_popup.jsp?callback=onAfterModify" cmd="update" class="exeBtn" param=""><spring:message code="btn_vm_polling" text="서버 가져오기"/></fm-popup-button>
				</c:if>
				<div class="btn-group">
				    <button id="vm_action_button" type="button" class="btn btn-primary dropdown-toggle exeBtn" data-toggle="dropdown"><spring:message code="btn_action" text="실행"/><span class="caret"></span></button>
				    <ul class="dropdown-menu" role="menu">
				      <li><a href="javascript:powerOn();"><spring:message code="btn_poweron" text="시작"/></a></li>
				      <li><a href="javascript:powerOff();"><spring:message code="btn_poweroff" text="정지"/></a></li>
				      <li><a href="javascript:reboot();"><spring:message code="btn_reboot" text="재부팅"/></a></li>
				      <li><a href="javascript:sync();"><spring:message code="btn_ip_sync" text="상태/IP 싱크"/></a></li>
				      <!-- <li><a href="javascript:openConsole();"><spring:message code="btn_console" text="콘솔 열기"/></a></li>
				      <li><a href="javascript:downloadConsole();"><spring:message code="btn_server_create" text="콘솔 다운로드"/></a></li>
				       -->
				    </ul>
				</div>
			</div>
		</div>
		<div id="popup-button-html">
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:340px">
	<div class="table_title">
	<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div>
	</div>
	<div id="mainTable" class="ag-theme-fresh" style="height: 300px" ></div>
	<div class="gab"></div>
	<ul class="nav nav-tabs vmDetailTab">
		<li class="active">
			<a href="#tab1" data-toggle="tab"  onclick="chgTab(1)"><spring:message code="detail" text="상세정보"/></a>
		</li>
		<c:if test="${sessionScope.FW_SHOW }">
			<li>
				<a href="#tab2" data-toggle="tab"  onclick="chgTab(2)"><spring:message code="access_control" text="접근제어"/></a>
			</li>
		</c:if>
		<c:if test="${sessionScope.SNAPSHOT}">
			<li>
				<a href="#tab3" data-toggle="tab"  onclick="chgTab(3)"><spring:message code="snapshot" text="스냅샷"/></a>
			</li>
		</c:if>
		<li>
			<a href="#tab4" data-toggle="tab" onclick="chgTab(5)"><spring:message code="monitoring" text="모니터링"/></a>
		</li>
		<li>
			<a href="#tab5" data-toggle="tab" onclick="chgTab(6)"><spring:message code="history" text="이력"/></a>
		</li>
	</ul>

	<div class="tab-content ">
		<div class="form-panel detail-panel tab-pane active" id="tab1">
			<div class="tab-buttons">

				<fm-popup-button  v-show="isSavable(form_data)" popupid="vm_save_popup" popup="/clovirsm/popup/vm_detail_form_popup.jsp?action=save&callback=onAfterModify" cmd="update" class="saveBtn" param="vmReq.getData()"><spring:message code="btn_modify" text="정보 수정"/></fm-popup-button>
				<c:if test="${sessionScope.ADMIN_YN == 'Y'}"><fm-popup-button popupid="vm_recreate_popup" popup="./vm_recreate_popup.jsp" cmd="update" class="saveBtn"  :disabled="form_data.RUN_CD !=  null && form_data.RUN_CD != 'F'" param="vmReq.getData()"><spring:message code="btn_recreate_work" text="재생성..."/></fm-popup-button></c:if>
			</div>
			<jsp:include page="detail.jsp"></jsp:include>
		</div>
		<c:if test="${sessionScope.FW_SHOW }">
			<div  class="form-panel detail-panel tab-pane" id="tab2">
				<jsp:include page="../vm_fw.jsp"></jsp:include>
			</div>
		</c:if>
		<c:if test="${sessionScope.SNAPSHOT}">
			<div  class="form-panel detail-panel tab-pane" id="tab3">
				<jsp:include page="../vm_snapshot.jsp"></jsp:include>
			</div>
		</c:if>
		<div  class="form-panel detail-panel tab-pane" id="tab4"  >
			<jsp:include page="../vm_guage.jsp"></jsp:include>
		</div>
		<div  class="form-panel detail-panel tab-pane" id="tab5"  >
			<jsp:include page="../vm_history.jsp"></jsp:include>
		</div>
	</div>
</div>
<script>
	var vmReq = new Req();
	mainTable;
function moveInsert(){
	location.href='/clovirsm/resources/newVm/mobis/index.jsp';
}
	$(function() {
		var
		columnDefs = [ {
			headerName : '',
			hide : true,
			field : 'VM_ID'
		},{
			headerName : '<spring:message code="NC_VM_VM_NM" text="서버명" />',
			field : 'VM_NM',
			maxWidth: 180,
			width: 180,
			minWidth: 150,
			tooltip: function(params){
				if(params.data) return params.data.VM_NM;
			}
		},{
			headerName : '<spring:message code="NC_VM_PRIVATE_IP" text="IP" />',
			field : 'PRIVATE_IP',
			maxWidth: 140,
			width: 140,
			minWidth: 140
		},{
			headerName : '<spring:message code="NC_VM_RUN_CD" text="서버상태" />',
			field : 'RUN_CD',
			maxWidth: 200,
			width: 100,
			minWidth: 100,
			tooltip: function(params){
				var serverStatus = '-';
				if(params.data && params.data.RUN_CD) {

					serverStatus = params.data.RUN_CD_NM;
					if(params.data && params.data.CUD_CD &&  params.data.CUD_CD != ''){
						if(params.data.CUD_CD == 'C') serverStatus = params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM;
						else  serverStatus += '('+params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM + ')';
					}


				}
				return serverStatus;
			},
			cellRenderer: function(params) {
				 
				var serverStatus = '-';
				if(params.data && params.data.RUN_CD) {
				
					serverStatus = '<img src="/res/img/RUN_CD_'+params.data.RUN_CD+'_icon.png" onerror="$(this).replaceWith(\''+ params.data.RUN_CD_NM+'\')"/>';
					if(params.data && params.data.CUD_CD){
						if(params.data.CUD_CD == 'C') serverStatus = params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM;
						else  serverStatus += '('+params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM + ')';
					}
					else if ((params.data.RUN_CD =='S' || params.data.RUN_CD == 'R') && params.data.FW_TASK_STATUS_CD == 'W')
					{
						var name_data ='<spring:message code="admin_work_ing" text="어드민작업중" />'
						serverStatus='<img src="/res/img/RUN_CD_W_icon.png" onerror="'+name_data+'"/>';
// 						serverStatus='<spring:message code="admin_work_ing" text="어드민작업중" /> params.data.RUN_CD_NM';
						
					}
				}
				return serverStatus;
			}
		},{
			headerName : '<spring:message code="NC_OS_TYPE_GUEST_NM" text="OS명" />',
			field : 'GUEST_NM',
			maxWidth: 400,
			width: 280,
			minWidth: 280,
			tooltip: function(params){
				if(params.data) return params.data.GUEST_NM;
			}
		},{
			headerName : '<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />',
			field : 'TEAM_NM',
			// maxWidth: 120,
			width: 120,
			minWidth: 120,
			tooltip: function (params) {
				if (params.data) return params.data.TEAM_NM
			}
		},{
			headerName : '<spring:message code="FM_TEAM_INS_ID_NM" text="담당자" />',
			field : 'INS_ID_NM',
			maxWidth: 120,
			width: 120,
			minWidth: 100
		},{
			headerName : '<spring:message code="NC_VM_INS_TMS" text="생성일시"/>',
			field : 'INS_TMS',
			maxWidth: 100,
			width: 100,
			minWidth: 100,
			valueGetter: function(params) {
				if(params.data) return formatDate(params.data.INS_TMS,'date')
			}
		},
		<c:if test="${sessionScope.STATE }">
		{
			headerName : "<spring:message code="PART" text="부품"/>",
			field : "ORG_CATEGORY",
			minWidth: 120
		},
	
		{
			headerName : '<spring:message code="NC_VM_P_KUBUN" text="구분" />',
			field : 'P_KUBUN_NM',
			maxWidth: 100,
			sort_field: 'P_KUBUN',
			width: 100,
			minWidth: 100
		},
	
		{
			headerName : '<spring:message code="NC_VM_PURPOSE" text="사용용도"/>',
			field : 'PURPOSE_NM',
			maxWidth: 120,
			width: 120,
			minWidth: 100
		},
		</c:if>
		{
			headerName : '<spring:message code="NC_VM_CPU_CNT" text="CPU" />',
			field : 'CPU_CNT',
			maxWidth: 80,
			width: 80,
			minWidth: 80,
			format:'number'
		},{
			headerName : '<spring:message code="NV_VM_RAM_CNT" text="Memory" />(GB)',
			field : 'RAM_SIZE',
			maxWidth: 80,
			width: 80,
			minWidth: 80,
			format:'number'
		},{
			headerName : '<spring:message code="NC_VM_DISK_CNT" text="DISK"/>(GB)',
			field : 'DISK_SIZE',
			maxWidth: 100,
			width: 100,
			minWidth: 100,
			format:'number'
		}  ];
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			cacheBlockSize: 100,
			rowSelection : 'single',
			rowData : [],
			resizable: true,
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: true,
			onSelectionChanged : function(e) {
				var arr = mainTable.getSelectedRows();
				getVMInfo(arr[0].VM_ID)
			},

		}
		mainTable = newGrid("mainTable", gridOptions);
		 
		vmSearch();
	});
	function getVMInfo(VM_ID )
	{
		vmReq.getInfo('/api/vm/info?VM_ID='+  VM_ID, function(data){
			 
			setButtonClickable('vm_copy_popup_button', true);
			setButtonClickable('img_insert_popup_button',true);
			setButtonClickable('vm_save_popup_button', isSavable(data));
			setButtonClickable('vm_update_popup_button',  isRequestable(data));
			setButtonClickable('vm_delete_popup_button', isDeletable(data) || isRequestable(data));
			setButtonClickable('vm_action_button', isOwner(data) && data.CUD_CD != 'C');
			if(data.P_KUBUN == "P") {
				setButtonClickable('vm_action_button', ADMIN_YN == "Y");
			}

			if(data.CUD_CD == 'C') {
				$('ul.vmDetailTab').find('li:first a').trigger('click');
				$('ul.vmDetailTab').find('li:not(:first)').addClass('disabledTab');
			} else {
				try
				{
					historySearch();
					vmFWSearch();
				}catch(e){}
				if(SNAPSHOT){
					snapshotSearch();
				}
				chgTab(tabIdx);
				$('.disabledTab').removeClass('disabledTab');
			}

		});
	}

	function openVMSavePopup( )
	{

		var data = vmReq.getData();
		if(data.CUD_CD){
			if(data.CUD_CD == 'C'){
				$('#vm_detail_tab_popup_button').trigger('click');
			} else if(data.CUD_CD == 'U'){
				$('#vm_update_popup_button').trigger('click');
			} else if(data.CUD_CD == 'D'){
				$('#vm_req_info_popup_button').trigger('click');
			} else {
				$('#vm_save_popup_button').trigger('click');
			}
		} else {
			$('#vm_save_popup_button').trigger('click');
		}

	}
	var tabIdx = 1;
	function chgTab(idx)
	{
		tabIdx = idx;
		if(idx==5)
		{
			reloadMonitor();
		}
	}
	function setVMButtonActive(isActive){
		setButtonClickable('vm_save_button', isActive);
		setButtonClickable('vm_update_popup', isActive);
		setButtonClickable('vm_delete_popup', isActive);
		//setButtonClickable('vm_owner_change_popup', isActive);
		//setButtonClickable('vm_action_button', isActive);

		//setInputEnable('VM_NM', isActive);
		setInputEnable('PURPOSE', isActive);
		setInputEnable('CMT', isActive);
	}


	// 조회
	function vmSearch(callback) {
		
		//showLoading();
		vmReq.clearData();
		vmReq.searchPaging('/api/vm/list' , mainTable, function(data){
			$('ul.vmDetailTab').find('li:first a').trigger('click');
			$('ul.vmDetailTab').find('li').addClass('disabledTab');

			setButtonClickable('vm_copy_popup_button', false);
			setButtonClickable('img_insert_popup_button', false);
			setButtonClickable('vm_update_popup_button', false);
			setButtonClickable('vm_delete_popup_button', false);
			//setButtonClickable('vm_owner_change_popup_button', false);
			setButtonClickable('vm_action_button', false);
			mainTable.gridOptions.api.sizeColumnsToFit()
			/* if(data.total > 0){

				mainTable.setSelected(0);

				if(callback) callback();
			} */
		});
	}

	//서버 생성 요청
	function createServerRequest(){
		window.location.href = '/clovirsm/resources/reqVm/index.jsp';
	}

	//저장
	function vmSave(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			vmReq.save('/api/vm/save', function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//서버 시작
	function powerOn(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/powerOn', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//서버 정지
	function powerOff(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/powerOff', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//서버 재부팅
	function reboot(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/reboot', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}
	//상태/IP 싱크
	function syncAll(){

			post('/api/vm/syncAll', form_data, function(){
				vmSearch();
			});

	}

	//상태/IP 싱크
	function sync(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/sync', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//콘솔 열기
	function openConsole(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/openConsole', form_data, function(a, data){
				window.open(data.url, '_blank');
			});
		} else {
			alert(msg_select_first);
		}
	}

	//콘솔 다운로드
	function downloadConsole(){
		window.open('https://www.vmware.com/go/download-vmrc', '_blank');
	}

	// 엑셀 내보내기
	function exportExcel(){
		//mainTable.exportCSV({fileName:'Server'})
		exportExcelServer("mainForm", '/api/vm/list_excel', 'VM',mainTable.gridOptions.columnDefs, vmReq.getRunSearchData())
	}

	function onAfterModify(data){
		vmSearch(function()
		{
				getVMInfo(data.VM_ID)
		});
	}

	function onChangeOwner(data){

	}

	function select_S_CATEGORY(data){
		searchvue.form_data.CATEGORY = data.CATEGORY;
		searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}

</script>


