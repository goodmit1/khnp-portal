<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%
request.setAttribute("popupid", request.getParameter("popupid"));
request.setAttribute("isAppr","N");
%>
<style>
#${popupid} .modal-content
{
	width:1000px;
}
</style>
<fm-modal id="${popupid}" title="<spring:message code="label_vm_copy"/>" cmd="header-title">
	<span slot="footer">
		<input type="button" class="btn saveBtn" value="<spring:message code="btn_work" text=""/>" onclick="vm_copy_to_work()" />
		<input type="button" class="btn exeBtn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="vm_copy_request()" />
		<fm-popup-button style="display:none;" popupid="${ popupid }_search_form_popup" popup="/clovirsm/popup/nas_search_form_popup.jsp" cmd="" param="" callback="${ popupid }_select_nas"></fm-popup-button>
	</span>
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#${popupid}_copyReqTab1" data-toggle="tab"><spring:message code="NC_VM_SPEC_NM" text="사양"/></a>
		</li>
		<c:if test="${sessionScope.FW_SHOW }">
		<li>
			<a href="#${popupid}_copyReqTab2" data-toggle="tab"><spring:message code="NC_VM_ACCESS_CONTROL" text="접근제어" /></a>
		</li>
		</c:if>
	</ul>
	<div class="tab-content ">
		<div class="form-panel detail-panel tab-pane active" id="${popupid}_copyReqTab1">
			<form id="${popupid}_form" action="none">
				<input type="hidden" name="VM_ID" id="SCF_VM_ID"  />
				<input type="hidden" name="SPEC_ID" id="SCF_SPEC_ID"  />
				<input type="hidden" name="CPU_CNT" id="SCF_CPU_CNT"  />
				<input type="hidden" name="DISK_SIZE" id="SCF_DISK_SIZE"  />
				<input type="hidden" name="DISK_UNIT" id="SCF_DISK_UNIT"  />
				<input type="hidden" name="RAM_SIZE" id="SCF_RAM_SIZE"  />
				<div class="panel-body">
					<div class="col col-sm-6">
	<!-- 					<fm-select id="SCF_DC_NM" name="DC_ID" -->
	<!-- 						url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" -->
	<!-- 						keyfield="DC_ID" titlefield="DC_NM" -->
	<%-- 						title="<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>"> --%>
	<!-- 					</fm-select> -->
						<fm-output id="SCF_DC_NM" name="DC_NM" title="<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>"></fm-output>
					</div>
					<div class="col col-sm-6">
						<fm-output id="SCF_FROM_NM" name="FROM_NM" title="<spring:message code="NC_VM_FROM_NM" text="원본 서버명"/>"></fm-output>
					</div>

					<div class="col col-sm-12"  v-if="${!IS_AUTO_VM_NAME || sessionScope.ADMIN_YN == 'Y'}"  >


						<fm-input id="SCF_VM_NM" placeholder="<spring:message code="no_input_autocreation"/>" name="VM_NM"
							title="<spring:message code="NC_VM_VM_NM" text="VM명"/>" ${!IS_AUTO_VM_NAME ? 'required':''}  ></fm-input>
					</div>
					<div class="col col-sm-6">
						<fm-select url="/api/site/querykey/selectPartCategory/" id="${popupid}_CATEGORY"
							keyfield="CATEGORY_ID" titlefield="CATEGORY_NM"
							:disabled="${isAppr != 'N'}"
							name="CATEGORY" title="<spring:message code="PART" text="부품"/>">
						</fm-select>
					</div>
					<div class="col col-sm-6">
						<fm-select url="/api/code_list?grp=P_KUBUN" id="SCF_P_KUBUN" emptystr=" " name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>"></fm-select>
					</div>
					 <div class="col col-sm-6">
						<fm-select url="/api/code_list?grp=PURPOSE" id="SCF_PURPOSE" emptystr=" " name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="구분"/>"></fm-select>
					</div>
					<div class="col col-sm-6">
						<fm-spin id="SCF_USE_MM" name="USE_MM" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" min="6"></fm-spin>
					</div>
					<div class="col col-sm-12">
						<fm-output id="SCF_SPEC_INFO" name="SPEC_INFO" title="<spring:message code="NC_VM_SPEC_NM" text="사양"/>"></fm-output>
					</div>
					<c:if test="${sessionScope.NAS_SHOW}">
					<div class="col col-sm-12" style="max-height: 278px;">
						<div class="hastitle">
							<label for="${popupid}_NAS"
								class="control-label grid-title value-title"
								style="vertical-align: top;">
								<spring:message code="NC_NAS_NAS" text="NAS" />
								<button type="button" class="btn btn-primary btn icon" onclick="${popupid}_newNas()" title="btn_new">
									<i class="fa fat add fa-plus-square"></i>
								</button>
							</label>
							<div class="hastitle output" id="${popupid}_NAS_TABLE" style="height: 100%;overflow: auto;">
							</div>
						</div>
					</div>
					</c:if>
					<div class="col-sm-12" style="height: 275px;">
						<div>
							<fm-textarea id="SCF_CMT" name="CMT" style=" display:inline-block;"></fm-textarea>
						</div>
					</div>
				</div>
			</form>
		</div>
		<c:if test="${sessionScope.FW_SHOW }">
			<div  class="form-panel detail-panel tab-pane" id="${popupid}_copyReqTab2" style="height:380px">
				<jsp:include page="/clovirsm/resources/reqVm/user_list.jsp"></jsp:include>
			</div>
		</c:if>
	</div>


	<script>
		$(function(){
			setTimeout(function(){
				$("#SCF_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
				
			},100);
			
			
		});
		var popup_vm_copy_form = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		function ${popupid}_click(vue, param){
			 
			if(param.VM_ID){
				${popupid}_param = $.extend({}, param);
				${popupid}_param.FROM_ID = ${popupid}_param.VM_ID;
				${popupid}_param.FROM_NM = ${popupid}_param.VM_NM;
				${popupid}_param.VM_NM = "";
				${popupid}_vue.form_data = ${popupid}_param;
				delete ${popupid}_param.VM_ID;
				if(${popupid}_param.NAS) {
					var nasArr = ${popupid}_param.NAS.split(",");
					nasArr.forEach(function(value,index){
						${popupid}_makeNas(nasArr[index], true);
					})
/* 					for(var i in nasArr){
					}
 */				}
				<c:if test="${sessionScope.FW_SHOW }">
				${popupid}_vmFwReq.searchSub('/api/fw/list/list_NC_FW_current/', {VM_ID: ${popupid}_param.FROM_ID}, function(data) {

					if(data)
					{
						var data1 = [];
						for(var i=0; i < data.length; i++)
						{
							delete data[i].FW_ID;
							//if('Y'==data[i].VM_USER_YN)
							{
								data[i].CUD_CD='C';
								data[i].IU = 'I';
								data[i].IU_NM = '<spring:message code="btn_new" text="신규" />';
								data1.push(data[i])
							}
						}
					}
				 	 
				 	
					${popupid}_fw_init();
					
					${popupid}_vmFwListGrid.setData(data1);
				});
				</c:if>
				setTimeout(function(){$("#SCF_CMT").Editor("setText", ${popupid}_param.CMT ? ${popupid}_param.CMT:"")},1000);
				return true;
			} else {
				${popupid}_param = null;
				alert(msg_select_first);
				return false;
			}
		}
		function vm_copy_makeParam()
		{
			${popupid}_param.NC_VM_FW_LIST= JSON.stringify(${popupid}_vmFwListGrid.getData());
			${popupid}_param.CMT = $("#SCF_CMT").Editor("getText" );
			var list = $("#${popupid}_NAS_TABLE .nas_text");
			var nasStr = [];
			for(var i=0; i<list.length; i++){
				if(isNaN(i) || i == null) {
					continue;
				}
				var val = $(list[i]).text();
				if(val && val.trim() != "") {
					nasStr.push(val);
				}
			}
			//for(var i in list) {
			//}
			if(nasStr[0] != null) {
				${popupid}_param.NAS = nasStr.join(",");
			}
		}
		function ${popupid}_newNas(){
			$("#${ popupid }_search_form_popup_button").click();
		}
		function ${ popupid }_select_nas(data, callback){
			 
			if(callback){
				callback.call();
			}
			for(var i=0; i<data.length; i++){
				var row = data[i];
				if(!${popupid}_chk_dupl_nas(row.PATH)){
					continue;
				}
				${popupid}_makeNas(row.PATH, true);
			}
//			for(var i in data) {
//			}
		}

		function ${popupid}_clearNas(){
			$("#${popupid}_NAS_TABLE").html("");
		}

		function ${popupid}_makeNas(path, delAble) {
			var table = $("#${popupid}_NAS_TABLE");
			var html = '<div class="nas_item choice" style="width: fit-content;float:left;">';
			if(delAble == true){
				html += '<span class="choice_remove" onclick="delete_nas(this);">×</span>';
			}
			html += '<span class="nas_text">'
			html += path;
			html += "</span></div>";
			table.append(html);
		}

		function ${popupid}_chk_dupl_nas(path){
			var list = $("#${popupid}_NAS_TABLE .nas_text");
			
			for(var i=0; i<list.length; i++){
				if(isNaN(i) || i == null) {
					continue;
				}
				var val = $(list[i]).text();
				if(path == val) {
					return false;
				}
			}
			return true;
//			for(var i in list) {
//			}
		}
		var  ${popupid}_save_click=false;
		function ${popupid}_fw_grid_validate(){
			if(!${popupid}_fw_input_validate()) return;
			var value = ${popupid}_vmFwListGrid.getRow(0);
			 
			if((value === undefined || value === 'undefined' ) &&  !${popupid}_save_click)
			{
				 ${popupid}_save_click=true;
				alert('<spring:message code="NC_FW_VALIDATE" text="서버 생성요청자 이외에 서버접속이 필요한 사용자는 접근통제 탭에서 등록해주시기 바랍니다" />')
				return false;
			}
			else return true;
		}
		
		function delete_nas(that){
			var _this = $(that);
			_this.parent().remove();
		}
		function vm_copy_to_work(){
			
			vm_copy_makeParam();
			if(!${popupid}_fw_grid_validate()) return;
			post('/api/vmReq/insertReq', ${popupid}_param,  function(data){
				if(confirm(msg_complete_work_move)){
					location.href="/clovirsm/workflow/work/index.jsp";
					return;
				}
				vmSearch();
				$('#${popupid}').modal('hide');
			});
		}

		function vm_copy_request(){
			if(!${popupid}_fw_grid_validate()) return;
			vm_copy_makeParam();
			${popupid}_param.DEPLOY_REQ_YN = 'Y';

			post('/api/vmReq/insertReq', ${popupid}_param,  function(data){
				alert(msg_complete_work);
				vmSearch();
				$('#${popupid}').modal('hide');
			});
		}

	</script>
	<style>
	.row-fluid.Editor-container {
		width:calc(100% - 150px);
		float: right;
	}
	</style>
</fm-modal>