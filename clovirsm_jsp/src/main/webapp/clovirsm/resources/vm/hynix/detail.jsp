<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 입력 Form -->

<div class="form-panel panel panel-default detail-panel">
	<div class="panel-body">
		<input id="D_VM_ID" name="VM_ID" type="hidden" v-model="form_data.VM_ID" />
		<!-- 
		<div class="col col-sm-6">
			<fm-output id="DC_NM" name="DC_NM" title="<spring:message code="NC_DC_DC_NM" text="데이터센터명" />"></fm-output>
			
		</div>
		 -->
		<div class="col col-sm-6">
			<fm-output id="VM_NM" name="VM_NM"    title="<spring:message code="NC_VM_VM_NM" text="서버명" />"  ></fm-output>
			 
				
		</div>
		<div class="col col-sm-6">
			<fm-output id="RUN_CD_NM" name="RUN_CD_NM" title="<spring:message code="NC_VM_RUN_CD_NM" text="서버 상태" />" :value="getServerStatus(form_data)" :ostyle="getCUDCDStyle(form_data)"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="PRIVATE_IP" name="PRIVATE_IP" title="<spring:message code="NC_VM_PRIVATE_IP" text="IP" />"></fm-output>
		</div>
		
		<div class="col col-sm-6">
			<fm-output   id="P_KUBUN" emptystr=" "  :value="  nvl( form_data.CATEGORY_NM,'') + ' > '   + nvl(form_data.P_KUBUN_NM,'')  + ' '  + nvl(form_data.PURPOSE_NM,'')"
				name="P_KUBUN_NM" title="<spring:message code="TASK" text="업무"/>"> 
			</fm-output>
		</div>
		<!--  
		<div class="col col-sm-6">
			<fm-output  id="PURPOSE_NM"  
				name="PURPOSE_NM" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>">
			</fm-output>
		</div>
		-->
		<div class="col col-sm-6">
			<fm-output id="GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="USE_MM" name="USE_MM" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" :value="getUseMMAndExpireDT(form_data, '<spring:message code="dt_month" text="개월"/>','<spring:message code="dt_expiredate"/>')" :ostyle="getUseMMStyle(form_data)"></fm-output>
		</div>
		<div class="col col-sm-12">
			<fm-output id="SPEC_INFO" name="OLD_SPEC_INFO" title="<spring:message code="NC_VM_SPEC_SPEC_INFOM" text="사양" />" :value="getServerSpecinfo(form_data)" :ostyle="getSpecinfoStyle(form_data)"></fm-output>
		</div>
		<c:if test="${sessionScope.ADMIN_YN == 'Y'}">
		<div class="col col-sm-12" v-if="form_data.FAIL_MSG">
			<fm-output id="FAIL_MSG" name="FAIL_MSG" title="<spring:message code="NC_VM_FAIL_MSG" text="실패 사유" />" ></fm-output>
		</div>
		</c:if>
		<div class="col col-sm-6">
			<fm-output id="FEE" name="DD_FEE" title="<spring:message code="NC_VM_SPEC_FEE" text="요금(일일/총합)" />" :value="getFee(form_data)"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="INS_TMS" name="INS_TMS" title="<spring:message code="NC_VM_INS_TMS" text="등록일시" />" :value="formatDate(form_data.INS_TMS, 'datetime')"></fm-output>
		</div>
		
		<div class="col col-sm-6">
			<fm-output id="TEAM_NM" name="TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-output>
		</div>
		<div class="col col-sm-6">
			<fm-output id="INS_ID_NM" name="INS_ID_NM" title="<spring:message code="FM_TEAM_INS_ID_NM" text="담당자" />"></fm-output>
		</div>
		<div class="col col-sm-12">
			<div style="height: 200px;">
				<label for="CMT" class="control-label grid-title value-title"><spring:message code="NC_VM_CMT" text="설명" /></label>
				<div v-html="cmtValue(form_data.CMT)" class="output hastitle" style="min-height: 200px;overflow-y: auto;word-break: break-word;"></div>
			</div>
		</div>
		
	</div>
	 
</div>
<script>
function recreateVM(){
	if(confirm("<spring:message code="NC_VM_recreate_confirm_msg" text="vcenter에 해당 서버가 없어야 합니다. 있다면 동일 이름으로 추가 생성됩니다. 계속하시겠습니까?" />"))
	{
		post('/api/vm/recreate',{VM_ID:form_data.VM_ID}, function(){
			alert(msg_complete);
			search();
		})
	}
}

function cmtValue(cmt){
	 
	var cmtArr = String(cmt).split("\n");
	 
	var str = "";
	if(cmtArr.length > 1){
		for(var i = 0 ; i < cmtArr.length ; i++){
			str += cmtArr[i] + "<br>";
		}
	} else{
		if(cmt != null && cmt != 'undefined')
			str = cmt;
	}
	return str;
}
</script>