<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page import="com.fliconz.fm.mvc.util.MsgUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<%
String title = MsgUtil.getMsgWithDefault("NEW_VM_TITLE_3" , new String[]{}, "");
request.setAttribute("MENU_TITLE",title);
request.setAttribute("popupid","main");
%>
<style>

</style>
<div id="main" class=" ">
<div id="search_area"  class="search-panel panel panel-default">
		<div class="panel-body">
			<!-- 조회의 id는 S_를 붙인다. -->
			<c:if test="${sessionScope.GRP_ADMIN_YN == 'Y'}">
				<div class="col col-sm">
					<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" " name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-select>
				</div>
			<c:if test="${ADMIN_YN == 'Y'}">
				<div class="col col-sm">
					<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="S_DC_ID"
						emptystr=" " keyfield="DC_ID" titlefield="DC_NM"
						name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
					</fm-select>
				</div>
			</c:if>
			</c:if>
			<div class="col col-sm">
				<fm-select url="/api/code_list?grp=P_KUBUN" id="S_P_KUBUN" emptystr=" "
					name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
				</fm-select>
			</div>
			<div class="col col-sm-3" style="width: 13.666667%;min-width:240px;">
					<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM" title="<spring:message code="TASK"/>" class="inline">
						<fm-popup-button popupid="category1" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
					</fm-ibutton>
					<input type="text" style="display:none" id="S_CATEGORY" name="CATEGORY">
			</div>
			<div class="col col-sm">
				<fm-input id="keyword_input" class="keyword" name="keyword" input_style="width:170px;"
					placeHolder="<spring:message code="NC_VM_VM_NM" text="서버명" />/<spring:message code="NC_VM_GUEST_NM" text="OS명" />/<spring:message code="NC_VM_PRIVATE_IP" text="IP" />/<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"
					title="<spring:message code="keyword_input" text="키워드" />">
				</fm-input>
			</div>
				<div class="col col btn_group nomargin">
					<fm-sbutton cmd="search" class="searchBtn" onclick="main_search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
				</div>
			</div>
	 
</div>
<div class="fullGrid" id="input_area"  style="height:auto;">
	<div class="table_title layout name">
		<%-- <spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
			<spring:message code="mainTable_Search_information" text="검색정보" />
			<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="main_grid_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
				<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<input type="button" class="btn saveBtn contentsBtn tabBtnImg save" value="<spring:message code="btn_chg_req" text="변경 요청" />" onclick="main_to_work()" />
		</div>
	</div>
	<div class="layout background mid">
		<div id="main_grid" style="height:600px;" class="ag-theme-fresh"></div>
		<fm-popup-button style="display:none;" popupid="diskChangePopup" popup="/clovirsm/popup/diskChange_popup.jsp" param="temp_data" callback="main_search"></fm-popup-button>
	</div>
</div>

<script>

<fmtags:include page="js.jsp" />
 
function main_to_work(){
	 
	$('#diskChangePopup_button').trigger('click');
}
var main_req = new Req();
var main_table;
var temp_data = null;
$(document).ready(function(){
	var main_columnDefs =[{
		headerName : '<spring:message code="NC_VM_VM_NM" text="서버명" />',
		field : 'VM_NM',
		width: 180,
		minWidth: 150,
		tooltip: function(params){
			if(params.data) return params.data.VM_NM;
		}
	},{
		headerName : '<spring:message code="NC_VM_P_KUBUN" text="구분" />',
		field : 'P_KUBUN_NM',
		sort_field: 'P_KUBUN',
		width: 120,
		minWidth: 120
	},{
		headerName : '<spring:message code="label_spec" text="Spec" />',
		field : 'OLD_SPEC_INFO',
		width: 200,
		minWidth: 200
	},{
		headerName : '<spring:message code="NC_VM_CPU_CNT" text="CPU" />(core)',
		field : 'OLD_CPU_CNT',
		width: 100,
		minWidth: 100,
		cellRenderer: function(params) {
		 return params.data.CPU_CNT + "core";
		}
	},{
		headerName : '<spring:message code="NC_VM_RAM_SIZE" text="Memory" />(GB)',
		field : 'OLD_RAM_SIZE',
		width: 100,
		minWidth: 100,
		cellRenderer: function(params) {
		 return params.data.RAM_SIZE + "GB";
		}
	},{
		headerName : '<spring:message code="NC_IMG_DISK" text="디스크" />',
		field : 'DISK_SIZE',
		width: 100,
		minWidth: 100,
		cellRenderer: function(params) {
			 
		 return params.data.DISK_SIZE + params.data.DISK_UNIT;
		}
	},{
		headerName : '<spring:message code="NC_OS_TYPE_GUEST_NM" text="OS명" />',
		field : 'GUEST_NM',
		width: 280,
		minWidth: 280,
		tooltip: function(params){
			if(params.data) return params.data.GUEST_NM;
		}
	},{
		headerName : '<spring:message code="NC_VM_PRIVATE_IP" text="IP" />',
		field : 'PRIVATE_IP',
		width: 140,
		minWidth: 140
	},{
		headerName : '<spring:message code="TASK" text="업무명" />',
		field : 'CATEGORY_NM',
		maxWidth: 180,
		width: 120,
		minWidth: 120
	},{
		headerName : '<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />',
		field : 'TEAM_NM',
		maxWidth: 120,
		width: 120,
		minWidth: 120
	},{
		headerName : '<spring:message code="FM_TEAM_INS_ID_NM" text="담당자" />',
		field : 'INS_ID_NM',
		maxWidth: 120,
		width: 120,
		minWidth: 100
	} ];
	var main_gridOptions = {
		columnDefs: main_columnDefs,
		rowData: [],
		sizeColumnsToFit:true,
		enableSorting: true,
		rowSelection:'single',
	    enableColResize: true,
	    onRowClicked: function(){
			var arr = main_table.getSelectedRows();
			main_req.getInfo('/api/vm/info?VM_ID=' + arr[0].VM_ID, function(data){
				 
				temp_data = data;
			} );
	    }
	};
	main_table = newGrid("main_grid", main_gridOptions);
	main_search();
});

function select_S_CATEGORY(data){
	searchvue.form_data.CATEGORY = data.CATEGORY;
	searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
	$("#S_CATEGORY").val(data.CATEGORY);
	$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
}

//엑셀 내보내기
function exportExcel(){
	main_table.exportCSV({fileName:'vm'})
	//exportExcelServer("mainForm", '/api/vm/list_excel', 'Snapshot',mainTable.gridOptions.columnDefs, vmReq.getRunSearchData())
}
</script>
