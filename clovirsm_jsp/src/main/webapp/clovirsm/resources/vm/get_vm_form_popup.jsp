<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<fm-modal id="get_vm_form_popup" title="<spring:message code="btn_vm_polling" text="서버 가져오기"/>" cmd="header-title">
	<span slot="footer">
		<input type="button" class="btn exeBtn" value="<spring:message code="btn_vm_polling" text="서버 가져오기"/>" onclick="get_vm()" />
	</span>
	<form  class="form-panel detail-panel" id="get_vm_form_popup_form" action="none" style="height:200px">
		<input type="hidden" name="USER_ID" />
		<div class="col col-sm-12">
		<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="DC_ID"
			keyfield="DC_ID" titlefield="DC_NM"
			name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
		</fm-select>
		</div>
		<div class="col col-sm-12">
		<fm-input id="VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="서버명" />"></fm-input>
		</div>
		<div class="col col-sm-12">
		<fm-ibutton id="SEARCH_USER_NAME" name="USER_NAME" title="<spring:message code="NC_VRA_CATALOGREQ_USER_NAME" text="" />">
			<fm-popup-button v-show="false" popupid="get_vm_user_search" popup="/popup/user_search_form_popup.jsp" cmd="update" param="$('#SEARCH_USER_NAME').val()" callback="select_get_vm_user"><spring:message code="btn_vm_polling" text="서버 가져오기"/></fm-popup-button>
		</fm-ibutton>
		</div>
	</form>

	<script>
		var popup_get_vm_form = newPopup("get_vm_form_popup_button", "get_vm_form_popup");
		var get_vm_form_popup_vue = new Vue({
			el: '#get_vm_form_popup',
			data: {
				form_data: {
					USER_ID: -1,
					USER_NAME: '',
					DC_ID: '',
					VM_NM: ''
				}
			}
		});
		function get_vm_form_popup_click(vue, param){
			return true;
		}

		function get_vm(){
			post('/api/vm/get_vm', get_vm_form_popup_vue.form_data,  function(data){
				alert(msg_complete_work);
				vmSearch();
				$('#get_vm_form_popup').modal('hide');
			});
		}

		function select_get_vm_user(args, callback){
			get_vm_form_popup_vue.form_data.USER_NAME = args.USER_NAME;
			get_vm_form_popup_vue.form_data.USER_ID = args.USER_ID;
			if(callback) callback();
		}

	</script>
</fm-modal>