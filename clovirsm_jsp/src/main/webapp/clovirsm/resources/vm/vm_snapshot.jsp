 <%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:240px">
	<div class="table_title">
		<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp<spring:message code="msg_snap_shot_auto_del_info"/>
		<div class="btn_group">

			<fm-sbutton id="reverse_snapshot_button"  class="bt" onclick="revertSnapshot()" title="<spring:message code="btn_restore"/>"><spring:message code="btn_restore"/></fm-sbutton>
			<fm-popup-button popupid="create_snapshot_form_popup" class="bt icon" popup="create_snapshot_form_popup.jsp" param=""><i class="fa fat add fa-plus-square"></i></fm-popup-button>
			<fm-sbutton id="delete_snapshot_button"  class="bt icon" onclick="deleteSnapshot()" title="<spring:message code="btn_delete" text=""/>"><i class="fa fat red fa-minus-square"></i></fm-sbutton>

			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel_snapshot()" class="btn" id="excelBtn"></button>
			<button type="button" title="<spring:message code="btn_reload" text="새로고침"/>" onclick="snapshotSearch(snapShotReq.VM_ID)" class="btn" id="reeloadlBtn"></button>

		</div>
	</div>
	<div id="snapshotTable" class="ag-theme-fresh" style="height: 200px" ></div>
</div>
<script>
	var snapShotReq = new Req();
	snapshotTable;

	$(function() {
		var
		snapShotColumnDefs = [ {
			headerName : '',
			hide : true,
			field : 'SNAPSHOT_ID'
		},  {
			headerName : '<spring:message code="NC_SNAPSHOT_SNAPSHOT_NM" text="스냅샷명" />',
			field : 'SNAPSHOT_NM',
			tooltip: function(params){
				return params.data.SNAPSHOT_NM;
			},
			width: 300
		},{
			headerName : '<spring:message code="NC_SNAPSHOT_CMT" text="설명" />',
			field : 'CMT',
			width: 110
		},{
			headerName : '<spring:message code="NC_SNAPSHOT_TASK_STATUS_CD_NM" text="작업상태" />',
			field : 'TASK_STATUS_CD_NM',
			width: 130
		},{
			headerName : '<spring:message code="NC_SNAPSHOT_INS_TMS" text="생성일시"/>',
			field : 'INS_TMS',
			width: 120,
			valueGetter: function(params) {
		    	return formatDate(params.data.INS_TMS,'date')
			}
		} ];
		var
		snapShotGridOptions = {
			hasNo : true,
			columnDefs : snapShotColumnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onSelectionChanged : function() {

				setButtonClickable('delete_snapshot_button', isRequestable(form_data));
				setButtonClickable('reverse_snapshot_button', isRequestable(form_data));
			}
		}
		snapshotTable = newGrid("snapshotTable", snapShotGridOptions);
	});

	function revertSnapshot(){
		var i = snapshotTable.getSelectedRows()[0];
		if(i){
			if(confirm("<spring:message code="msg_restore_confirm"/>")){
				post("/api/snapshot/revert", i, function(data){
					alert(msg_complete);
					snapshotSearch(snapShotReq.VM_ID);
				});
			}
		}else{
			alert(msg_select_first)
		}
	}

	function deleteSnapshot(){
		snapShotReq.delGrid(snapshotTable, '/api/snapshot/delete_multi', function(){
			snapshotSearch(snapShotReq.VM_ID);
		})
	}

	// 조회
	function snapshotSearch(vmId) {
		setButtonClickable('create_snapshot_form_popup_button', isOwner(form_data));
		setButtonClickable('delete_snapshot_button', false);
		setButtonClickable('reverse_snapshot_button', false);
		if(!vmId && vmReq.getData())
		{
			vm_Id = vmReq.getData().VM_ID;
		}	
		if(vm_Id){
		 
			snapShotReq.searchSub('/api/snapshot/list', { VM_ID: vm_Id }, function(data) {
				snapShotReq.VM_ID = vm_Id;
				snapshotTable.setData(data);
			});
		}
	}

	// 엑셀 내보내기
	function exportExcel_snapshot(){
		snapshotTable.exportCSV({fileName:'Snapshot'})
	}

</script>


