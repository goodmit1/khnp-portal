<%@page import="com.fliconz.fm.security.UserVO"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<%
String VM_NM = request.getParameter("VM_NM");
request.setAttribute("VM_NM", VM_NM == null ? "null" : "'" + VM_NM + "'");
String DC_ID = request.getParameter("DC_ID");
request.setAttribute("DC_ID", DC_ID == null ? "null" : "'" + DC_ID + "'");


%>

<style>
	.titleLabel{
		background-color: #edeef3;
	}
</style>
<script type="text/javascript" src="/res/js/es6-promise/es6-promise.min.js"></script>
<script type="text/javascript" src="/res/js/es6-promise/es6-promise.auto.min.js"></script>
<script type="text/javascript" src="/res/js/jspdf.debug.js"></script>
<script type="text/javascript" src="/res/js/html2canvas.min.js"></script>
<script>


</script>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<c:if test="${sessionScope.GRP_ADMIN_YN == 'Y'}">
			<div class="col col-sm">
				<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" " name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-select>
			</div>
			<div class="col col-sm">
				<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="S_DC_ID"
					emptystr=" " keyfield="DC_ID" titlefield="DC_NM"
					name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
				</fm-select>
			</div>
		</c:if>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=RUN_CD" id="S_RUN_CD" emptystr=" "
				name="RUN_CD" title="<spring:message code="NC_VM_RUN_CD" text="서버상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=P_KUBUN" id="S_P_KUBUN" emptystr=" "
				name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
			</fm-select>
		</div>
		<!-- <div class="col col-sm">
			<fm-select-yn id="S_EXPIRE_YN" emptystr=" "
				name="EXPIRE_YN" title="<spring:message code="NC_VM_EXPIRE_YN" text="만료여부"/>">
			</fm-select-yn>
		</div>
		 -->
		<c:if test="${sessionScope.GRP_ADMIN_YN == 'Y'}">
			<div class="col col-sm-3" style="width: 13.666667%;min-width:240px;">
				<fm-ibutton id="S_CATEGORY_NM" name="CATEGORY_NM" title="<spring:message code="TASK"/>" class="inline">
					<fm-popup-button popupid="category1" popup="/clovirsm/popup/img_category_search_form_popup.jsp" cmd="update" param="false" callback="select_S_CATEGORY"></fm-popup-button>
				</fm-ibutton>
				<input type="text" style="display:none" id="S_CATEGORY" name="CATEGORY">
			</div>
		</c:if>
		<div class="col col-sm">
			<fm-input id="keyword_input" class="keyword" name="keyword" input_style="width:167px;"
				placeHolder="<spring:message code="NC_VM_VM_NM" text="서버명" />/<spring:message code="NC_VM_GUEST_NM" text="OS명" />/<spring:message code="NC_VM_PRIVATE_IP" text="IP" />/<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"
				title="<spring:message code="keyword_input" text="키워드" />">
			</fm-input>
		</div>
		
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="vmSearch()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
		<div id="popup-button-html">
		</div>
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:340px">
	<div class="table_title layout name">
		<div class="search_info">
			<spring:message code="total_info" text="종합 정보" />
		</div>
	</div>
	<jsp:include page="userMonitor.jsp"></jsp:include>
	<div class="table_title layout name">
		<%-- <spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
			<fm-popup-button popupid="user_search_select_popup" popup="/popup/user_search_select_form_popup.jsp" cmd="update" class="exeBtn layout sendMailBtn" :popup_title="'메일전송'" param="" callback="user_search_select_popup_select_user" :action="sendMail" :action_title="'메일전송'" :disabled="true" style="min-width:30px;"></fm-popup-button>
			<fm-sbutton id="pdfPrintBtn" cmd="update" title="<spring:message code="btn_print" text="인쇄" />" class="btn icon layout pdfBtn" onclick="printPdf();" :disabled="true"><i class="fa fa-file-pdf-o" style="font-size:16px; color: red;"></i></fm-sbutton>
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<div class="btn-group" style="float: right;">
				<button id="vm_action_button" type="button" class="btn btn-primary fm-popup-button update" data-toggle="dropdown" style="border-radius: 20px;" ><spring:message code="btn_action" text="실행"/><span class="caret"></span></button>
				<ul class="dropdown-menu execBtn" role="menu">
					<li><a href="javascript:powerOn();"><spring:message code="btn_poweron" text="시작"/></a></li>
					<li><a href="javascript:powerOff();"><spring:message code="btn_poweroff" text="정지"/></a></li>
					<li><a href="javascript:reboot();"><spring:message code="btn_reboot" text="재부팅"/></a></li>
				</ul>
			</div>
			<c:if test="${sessionScope.ADMIN_YN == 'Y'}">
				<fm-popup-button popupid="vm_recreate_popup" popup="vm_recreate_popup.jsp" cmd="update" class="saveBtn"  :disabled="form_data.RUN_CD !=  null && form_data.RUN_CD != 'F'" param="vmReq.getData()"><spring:message code="btn_recreate_work" text="재생성..."/></fm-popup-button>
			</c:if>
		<c:if test="${sessionScope.ADMIN_YN == 'Y'}">
			<fm-popup-button popupid="get_vm_form_popup" popup="get_vm_form_popup.jsp?callback=onAfterModify" cmd="update" class="exeBtn" param=""><spring:message code="btn_vm_polling" text="서버 가져오기"/></fm-popup-button>
			<fm-popup-button  v-show="isSavable(form_data)" popupid="vm_save_popup" popup="/clovirsm/popup/vm_detail_form_popup.jsp?action=save&callback=onAfterModify" cmd="update" class="saveBtn" param="vmReq.getData()"><spring:message code="btn_modify" text="정보 수정"/></fm-popup-button>	 
			<fm-sbutton cmd="update" class="contentsBtn tabBtnImg sync" onclick="javascript:sync();"><spring:message code="btn_ip_sync" text="상태/IP 싱크"/></fm-sbutton>   
			<fm-sbutton cmd="update" class="contentsBtn tabBtnImg sync" onclick="javascript:syncAll();"><spring:message code="btn_ip_syncAll" text="상태/IP 싱크 All"/></fm-sbutton>
		</c:if>

		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 300px" ></div>
	</div>
	<div class="gab"></div>
	<!-- <ul class="nav nav-tabs vmDetailTab">
		<li class="active">
			<a href="#tab1" data-toggle="tab"  onclick="chgTab(1)"><spring:message code="detail" text="상세정보"/></a>
		</li>
	</ul>
 -->
	<div class="tab-content ">
		<div class="form-panel detail-panel tab-pane active" id="tab1">
			<div class="tab-buttons">
			</div>
			<jsp:include page="detail.jsp"></jsp:include>
		</div>
	</div>
</div>
<script>
	var vmReq = new Req();
	mainTable;

	$(function() {
		let
		columnDefs = [ {
			headerName : '',
			hide : true,
			field : 'VM_ID'
		},{
			headerName : '<spring:message code="NC_DC_DC_NM" text="DC" />',
			field : 'DC_NM',
			hide : ADMIN_YN != 'Y',
			width: 180,
			minWidth: 150
			 
		},{
			headerName : '<spring:message code="NC_VM_VM_NM" text="서버명" />',
			field : 'VM_NM',
			maxWidth: 180,
			width: 180,
			minWidth: 150,
			tooltip: function(params){
				if(params.data) return params.data.VM_NM;
			}
		},{
			headerName : '<spring:message code="NC_VM_P_KUBUN" text="구분" />',
			field : 'P_KUBUN_NM',
			maxWidth: 120,
			sort_field: 'P_KUBUN',
			width: 120,
			minWidth: 120
		},{
			headerName : "<spring:message code="TASK" text="업무"/>",
			field : "CATEGORY_NM",
			minWidth: 120
		},{
			headerName : '<spring:message code="NC_VM_PRIVATE_IP" text="IP" />',
			field : 'PRIVATE_IP',
			maxWidth: 140,
			width: 140,
			minWidth: 140
		},{
			headerName : '<spring:message code="NC_VM_RUN_CD" text="서버상태" />',
			field : 'RUN_CD',
			maxWidth: 200,
			width: 100,
			minWidth: 100,
			tooltip: function(params){
				var serverStatus = '-';
				if(params.data && params.data.RUN_CD) {
					serverStatus = params.data.RUN_CD_NM;
					if(params.data && params.data.CUD_CD){
						if(params.data.CUD_CD == 'C') serverStatus = params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM;
						else  serverStatus += '('+params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM + ')';
					}
				}
				return serverStatus;
			},
			cellRenderer: function(params) {
				var serverStatus = '-';
				if(params.data && params.data.RUN_CD) {
					<c:if test="${sessionScope.ADMIN_YN == 'N'}">
						if(params.MY_FW_TASK_STATUS_CD  && params.MY_FW_TASK_STATUS_CD != 'S'){
							return "<spring:message code="admin_working" text="관리자 작업중" />"
						}
					</c:if>
					serverStatus = params.data.RUN_CD_NM;
					if(params.data && params.data.CUD_CD){
						if(params.data.CUD_CD == 'C') serverStatus = params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM;
						else  serverStatus += '('+params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM + ')';
					}
				}
				return serverStatus;
			}
		},{
			headerName : '<spring:message code="NC_OS_TYPE_GUEST_NM" text="OS명" />',
			field : 'GUEST_NM',
			maxWidth: 400,
			width: 280,
			minWidth: 280,
			tooltip: function(params){
				if(params.data) return params.data.GUEST_NM;
			}
		},{
			headerName : '<spring:message code="FM_TEAM_TEAM_CD" text="팀" />',
			field : 'TEAM_NM',
			maxWidth: 120,
			width: 120,
			minWidth: 120
		},{
			headerName : '<spring:message code="FM_TEAM_INS_ID_NM" text="담당자" />',
			field : 'INS_ID_NM',
			maxWidth: 120,
			width: 120,
			minWidth: 100
		},{
			headerName : '<spring:message code="NC_VM_INS_TMS" text="생성일시"/>',
			field : 'INS_TMS',
			maxWidth: 110,
			width: 110,
			minWidth: 110,
			valueGetter: function(params) {
				if(params.data) return formatDate(params.data.INS_TMS,'date')
			}
		} ];
		let
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowModelType: 'infinite',
			cacheBlockSize: 100,
			rowSelection : 'single',
			rowData : [],
			resizable: true,
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: true,
			onSelectionChanged : function(e) {
				var arr = mainTable.getSelectedRows();
				getVMInfo(arr[0].VM_ID)
			},

		}
		mainTable = newGrid("mainTable", gridOptions);
		 
		
		vmSearch();
		
	});
	function onAfterVMOwnerChg(){
		var arr = mainTable.getSelectedRows();
		getVMInfo(arr[0].VM_ID)
	}
	function getVMInfo(VM_ID )
	{
		vmReq.getInfo('/api/site/info/selectByPrimaryKey_NC_VM_khnp/?VM_ID='+  VM_ID, function(data){
			setButtonClickable('vm_copy_popup_button', isRequestable(data));
			setButtonClickable('img_insert_popup_button', isRequestable(data));
			setButtonClickable('vm_save_popup_button', isSavable(data));
			setButtonClickable('vm_update_popup_button',  isRequestable(data));
			setButtonClickable('vm_delete_popup_button', isDeletable(data) || isRequestable(data));
			setButtonClickable('vm_action_button', isOwner(data) && data.CUD_CD != 'C');
			if(data.P_KUBUN == "P") {
				setButtonClickable('vm_action_button', ADMIN_YN == "Y");
			}

			if(data.CUD_CD == 'C') {
				$('ul.vmDetailTab').find('li:first a').trigger('click');
				$('ul.vmDetailTab').find('li:not(:first)').addClass('disabledTab');
			} else {
				try
				{
					vmFWSearch();
				}catch(e){}
				if(SNAPSHOT){
					snapshotSearch();
				}
				chgTab(tabIdx);
				$('.disabledTab').removeClass('disabledTab');
			}
			
			if(data.VM_ID){
				$('#user_search_select_popup_button').prop('disabled', false);
				$('#pdfPrintBtn').prop('disabled', false);
			} else {
				$('#user_search_select_popup_button').prop('disabled', true);
				$('#pdfPrintBtn').prop('disabled', true);
			}
			
			if(vm_detail_vue) vm_detail_vue.form_data = data;
			
			

		});
	}

	function openVMSavePopup( )
	{

		var data = vmReq.getData();
		if(data.CUD_CD){
			if(data.CUD_CD == 'C'){
				$('#vm_detail_tab_popup_button').trigger('click');
			} else if(data.CUD_CD == 'U'){
				$('#vm_update_popup_button').trigger('click');
			} else if(data.CUD_CD == 'D'){
				$('#vm_req_info_popup_button').trigger('click');
			} else {
				$('#vm_save_popup_button').trigger('click');
			}
		} else {
			$('#vm_save_popup_button').trigger('click');
		}

	}
	var tabIdx = 1;
	function chgTab(idx)
	{
		tabIdx = idx;
		if(idx==5)
		{
			reloadMonitor();
		}
	}
	function setVMButtonActive(isActive){
		setButtonClickable('vm_save_button', isActive);
		setButtonClickable('vm_update_popup', isActive);
		setButtonClickable('vm_delete_popup', isActive);
		//setButtonClickable('vm_owner_change_popup', isActive);
		//setButtonClickable('vm_action_button', isActive);

		//setInputEnable('VM_NM', isActive);
		setInputEnable('PURPOSE', isActive);
		setInputEnable('CMT', isActive);
	}
	function search(){
		vmSearch();
	}
	 
	function vmUserState(){
		var date = new Date();
		var dateYY = formatDatePattern(date,'yyyyMMdd');
		search_data.YYYYMM = dateYY;
		post('/api/vm/list/list_user_state/',search_data , function(data){
			 
			$("#ALL_FEE").html(data[0].ALL_FEE == null ? 0 : formatNumber(data[0].ALL_FEE));
			$("#VM_COUNT").html(data[0].VM_COUNT);
			$("#ALL_DISK_SIZE").html((1.0 * data[0].ALL_DISK_SIZE / 1024).toFixed(1));
			$("#ALL_CPU_CNT").html(data[0].ALL_CPU_CNT);
			$("#ALL_RAM_SIZE").html(data[0].ALL_RAM_SIZE);
						
		});
	}
	// 조회
	function vmSearch(callback) {
		
		    //vmReq.setSearchData('MONITOR','Y');
	        vmReq.clearData();
	        vmReq.searchPaging('/api/vm/list' , mainTable, function(data){
			$('ul.vmDetailTab').find('li:first a').trigger('click');
			$('ul.vmDetailTab').find('li').addClass('disabledTab');

			setButtonClickable('vm_copy_popup_button', false);
			setButtonClickable('img_insert_popup_button', false);
			setButtonClickable('vm_update_popup_button', false);
			setButtonClickable('vm_delete_popup_button', false);
			//setButtonClickable('vm_owner_change_popup_button', false);
			setButtonClickable('vm_action_button', false);

			if(data.total > 0){

				mainTable.setSelected(0);

				if(callback) callback();
			}
		});
	    vmUserState();
	}

	//서버 생성 요청
	function createServerRequest(){
		window.location.href = '/clovirsm/resources/reqVm/index.jsp';
	}

	//저장
	function vmSave(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			vmReq.save('/api/vm/save', function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//서버 시작
	function powerOn(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/powerOn', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//서버 정지
	function powerOff(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/powerOff', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//서버 재부팅
	function reboot(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/reboot', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//상태/IP 싱크
	function sync(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/sync', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	//상태/IP 싱크
	function syncAll(){
		post('/api/vm/syncAll', form_data, function(){
			vmSearch();
		});

	}

	//콘솔 열기
	function openConsole(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/openConsole', form_data, function(a, data){
				window.open(data.url, '_blank');
			});
		} else {
			alert(msg_select_first);
		}
	}

	//콘솔 다운로드
	function downloadConsole(){
		window.open('https://www.vmware.com/go/download-vmrc', '_blank');
	}

	// 엑셀 내보내기
	function exportExcel(){
		exportExcelServer("mainForm", '/api/vm/list_excel', 'VM',mainTable.gridOptions.columnDefs, vmReq.getRunSearchData())
	}

	function onAfterModify(data){
		vmSearch()
		getVMInfo(data.VM_ID)
	}

	function onChangeOwner(data){

	}

	function select_S_CATEGORY(data){
		searchvue.form_data.CATEGORY = data.CATEGORY;
		searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}
	
	function initFormData()
	{
		 
	}
	
	//상태/IP 싱크
	function sync(){
		if(form_data.VM_ID && form_data.VM_ID != null){
			post('/api/vm/sync', form_data, function(){
				vmSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}
	
	function printPdf(){
		html2canvas(document.getElementById("vm_detail_wrap")).then(function(canvas) {
          var imgData = canvas.toDataURL('image/png');
          var imgWidth = 210;
          var pageHeight = imgWidth * 1.414;
          var imgHeight = canvas.height * imgWidth / canvas.width;

          var doc = new jsPDF({
            'orientation': 'p',
            'unit': 'mm',
            'format': 'a4'
          });

          doc.addImage(imgData, 'PNG', 0, 0, imgWidth, imgHeight);
          
          var arr = mainTable.getSelectedRows();
          var vm_nm = arr[0].VM_NM;
          var filename = vm_nm + '_' + new Date().getTime()  + '.pdf';
          doc.save(filename);
        });
	}
	
	function showSendMail(){
		$('#send_vm_detail_mail_popup_button').click();
	}
	
	function sendMail(selectedUserList){
		var receivers = selectedUserList;
		var receiverMails = [];
		for(var i = 0; i < selectedUserList.length; i++){
			var receiverEmail = selectedUserList[i].EMAIL;
			receiverMails.push(receiverEmail);
		}
		
		//var svgdataurl = await ddd("vm_detail_wrap");
		var vmDetailHTML = $('#vm_detail_wrap_hide').html();
		var param = {
			receivers: receivers
			, receiverMails: receiverMails
			, CONTENTS: escape(vmDetailHTML)
			, content_title: '안녕하세요. 메일 제목 테스트입니다.'
			, email_title: 'VM 상세정보를 보내드립니다.'
			, email_template: 'vm_detail'
		}
		
		//메일 전송 요청
		post('/api/send_mail/', param, function(){
			$('#user_search_select_popup').modal('hide');
			alert('메일 전송이 완료되었습니다.');
		});
		
	}
	
</script>


