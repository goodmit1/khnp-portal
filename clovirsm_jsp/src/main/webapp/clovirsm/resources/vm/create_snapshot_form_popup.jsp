<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<fm-modal id="create_snapshot_form_popup" title="<spring:message code="label_new_snapshot"/>" cmd="header-title">
	<span slot="footer">
		<input type="button" class="btn newBtn" value="<spring:message code="label_new_snapshot"/>" onclick="create_snapshot()" />
	</span>
	<div class="detail-panel" style="border-bottom:none;">
		<div class="panel-body">
			<form id="create_snapshot_form_popup_form" action="none">
				<div class="col col-sm-12">
					<fm-input id="SNAPSHOT_NM" name="SNAPSHOT_NM" title="<spring:message code="NC_SNAPSHOT_SNAPSHOT_NM" text="스냅샷명" />"></fm-input>
				</div>
				<div class="col col-sm-12">
					<fm-textarea  id="CMT" name="CMT" title="<spring:message code="NC_SNAPSHOT_CMT" text="설명" />"  ></fm-textarea>
				</div>
			</form>
		</div>
	</div>
	<script>
		var popup_create_snapshot_form = newPopup("create_snapshot_form_popup_button", "create_snapshot_form_popup");
		var create_snapshot_form_popup_vue = new Vue({
			el: '#create_snapshot_form_popup',
			data: {
				form_data: {
					VM_ID: '',
					SNAPSHOT_NM: '',
					CMT: '',
					TASK_STATUS_CD: 'W'
				}
			}
		});
		function create_snapshot_form_popup_click(vue, param){
			if(!snapShotReq.VM_ID || snapShotReq.VM_ID == ""){
				alert(msg_select_first);
				return false;
			}
			create_snapshot_form_popup_vue.form_data.VM_ID = snapShotReq.VM_ID;
			return true;
		}

		function create_snapshot(){
			post('/api/snapshot/save', create_snapshot_form_popup_vue.form_data,  function(data){
				alert(msg_complete_work);
				snapshotSearch(snapShotReq.VM_ID);
				$('#create_snapshot_form_popup').modal('hide');
			});
		}
	</script>
</fm-modal>