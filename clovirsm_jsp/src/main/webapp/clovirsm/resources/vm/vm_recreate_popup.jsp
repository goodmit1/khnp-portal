<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<fm-modal id="${popupid}" title="<spring:message code="label_recreate_copy" text="재생성"/>" cmd="header-title">
	<span slot="footer">
		<input type="button" class="btn saveBtn" value="<spring:message code="btn_recreate_work" text="재생성"/>" onclick="doRecreate()" />

	</span>
	<div class="form-panel detail-panel">
		<form id="${popupid}_form" action="none" style="height:150px">
		<span id="reKubun0"><input type="radio" value="O" name="reKubun"  checked onchange="${popupid}_kubun_change(this.value)"><spring:message code="NC_VM_TMPL_OLD" text="기존 템플릿"/></span>
		<input type="radio" value="N" name="reKubun" id="reKubun1" onchange="${popupid}_kubun_change(this.value)"><spring:message code="NC_VM_TMPL_NEW" text="신규 템플릿"/></input>
		<div id="new_tmpl" style="display:none">
			<input type="hidden" name="VM_ID" id="RE_VM_ID"  />
			 <jsp:include page="../reqVm/_selOs.jsp" >
			 	<jsp:param value="recreate" name="action"/>
			 </jsp:include>
			  <input type="hidden" name="DC_ID" id="${popupid}_DC_ID"  />
			  
		</div>

		</form>
	</div>
	<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		$(document).ready(function(){
			 vm_recreate_popup_addRootImgCategorySelect();
			 $("#${popupid}_DC_ID_area").hide();
			 $("#${popupid}_OS_ID").change(function(){
					var data =$("#${popupid}_OS_ID option:selected").prop("data");
					 
					 $("#${popupid}_DC_ID").val(data.DC_ID); 
					
				})
		})
		function ${popupid}_kubun_change(val)
		{
			if(val=='O')
			{
				$("#new_tmpl").hide();
			}
			else
			{
				$("#new_tmpl").show();
			}
		}
		function doRecreate()
		{
			post('/api/vm/recreate', $("#vm_recreate_popup_form").serialize(), function(){
				alert(msg_complete);
				vmSearch();
				$('#${popupid}').modal('hide');
			})
		}

		 function ${popupid}_click()
		 {
			
			 $("#RE_VM_ID").val(form_data.VM_ID);
			 if(!form_data.FROM_ID && !form_data.OS_ID)
			 {
				 $("#reKubun0").hide();
				 $("#reKubun1").click();
			 }
			 else
			 {
				 $("#reKubun0").show();
				 $("#reKubun0 input").click();
			 }

			 return true;
		 }
	</script>
	<style>

	</style>
</fm-modal>