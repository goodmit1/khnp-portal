<%@page contentType="text/html; charset=UTF-8"%><%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  서버 목록 조회 -->
<style>

#input_area .value-title {
    min-width: 150px;
    max-width: 230px;
    width: auto;
    text-align: center !important;
    padding-top: 5px;
    background: white;
}
.fa-times{
	text-align: center;
    width: 100%;

}
.input-group{
	width:700px;
}
.fa-remove:before, .fa-close:before, .fa-times:before {
    content: "\f00d";
    color: #ef1313;
    font-size: 25px;
    position: relative;
    left: -5px;
    top: -5px;
    cursor: pointer;
}
.newServiceMoveBtn{
	background: #236fa9;
    min-width: 270px;
    display: inline-block;
    height: 50px;
    padding: 15px;
    font-size: 18px;
    width: auto;
    font-weight: bold;
    color: white;
    cursor: pointer;
}


</style>

<div class="fullGrid" id="input_area" style="height:340px">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="taskGrid_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
			<fm-sbutton cmd="search" class="contentsBtn tabBtnImg save" onclick="taskSave()"><spring:message code="label_add" text="추가"/></fm-sbutton>
		</div>
		<div style="display: inline-block; position: relative; top: -20px; font-size: 15px; width:800px">
			<fm-multi-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.listCategory" titlefield="CATEGORY_NM" id="S_CATEGORY"  defaultField="CATEGORY_IDS" keyfield="CATEGORY_ID" parfield="PAR_CATEGORY_ID" rootdata="0"  title=" <spring:message code='TASK' text='업무'/>" selectcmd="serviceStyle" label_style="text-align: center; font-weight: bolder; background: #f5f6fa; height: 50px; padding-top: 13px;
							" name="CATEGORY" ></fm-multi-select>
		</div>
	</div>
	<div class="layout background mid ">
		<div id="taskGrid" class="ag-theme-fresh hide" style="height: 400px" ></div>
		<div id="taskInfo" style="margin-top:40px">
			<div>담당 업무가 존재해야 신규 서비스 요청으로 이동할 수 있습니다.</div>
			<div>담당업무를 선택 후 추가 버튼을 눌러 추가해주세요.</div>
		</div>
	</div>
	<div style="text-align: center; margin-top: 20px;">
		<div class="newServiceMoveBtn" onclick="newServiceMoveBtn();">
			<spring:message code="NEW_SERVICE_REQ_MOVE" text="신규서비스 요청으로 이동" />		
		</div>
	</div>
	<div style="text-align: right; font-weight: bold;">
		<div class="template_info_display" style="width: 302px;">
<%-- 		<div style=" display: inline-block;"><span><spring:message code="NO_TASK" text="업무가 없으십니까?" /></span>&nbsp<a href="/clovirsm/workflow/templateAdd/index.jsp?CHK_TASK=Y" style="color: black; font-style: oblique; text-decoration: underline; cursor: pointer;"><spring:message code="ADD_TASK_TEMPLATES_REQ_BTN" text="업무 및 템플릿 추가 요청" /></a></div> --%>
		</div>
	</div>
	<div style="text-align: right; font-weight: bold;">

	</div>
</div>
<script>
	var taskReq = new Req();
	var taskGrid;

		
	$(function() {
		var
		columnDefs = [ {
			headerName : '',
			hide : true,
			field : 'CATEGORY_ID'
		},{
			headerName : '<spring:message code="TASK" text="업무" />',
			field : 'CATEGORY_FULL_NM',
			cellRenderer:function(params){
				return params.data.CATEGORY_FULL_NM;
			}
		},{
			headerName : '<spring:message code="btn_delete" text="삭제"/>',
			maxWidth: 150,
			cellRenderer: function(params) {
      		  	return "<i class='fa fa-times' onclick='taskDel(\""+params.data.CATEGORY_ID+"\")' ></i>";
		    	
			}
		}];
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			onSelectionChanged: function(){
				var arr = taskGrid.getSelectedRows();
					
			}
		}
		taskGrid = newGrid("taskGrid", gridOptions);
		taskSearch();
	});

	// 조회
	function taskSearch() {
		taskReq.search('/api/category/map/list?KUBUN=U', function(data) {
			 
			taskGrid.setData(data);
			showInfo();
		});
	}

	//저장
	function taskSave(){
		var param = Object();
		param.CATEGORY_ID = $("#S_CATEGORY select option:selected").last().val();
		param.KUBUN = "U";
		param.ID = _USER_ID_;
		 
		post('/api/category/map/save',param ,function(){
				taskSearch();
		});
	}

	function taskDel(id){
		var param = Object();
		param.ID =  _USER_ID_;
		param.CATEGORY_ID = id;
		param.KUBUN = 'U';
		post('/api/category/map/delete',param ,function(){
			taskSearch();
		});
	}
	function newServiceMoveBtn(){
		location.href="/clovirsm/resources/vra/newVra/index.jsp";
		return true;
	}
	function showInfo(){
		if(taskGrid.getData().length> 0){
			$("#taskGrid").removeClass("hide");
			$("#taskInfo").addClass("hide");
			$(".newServiceMoveBtn").removeClass("hide");
		} else{
			$("#taskGrid").addClass("hide");
			$("#taskInfo").removeClass("hide");
			$(".newServiceMoveBtn").addClass("hide");
		}
	}
	// 엑셀 내보내기
	function exportExcel(){
		taskGrid.exportCSV({fileName:'task'})
		//exportExcelServer("mainForm", '/api/vm/list_excel', 'Snapshot',mainTable.gridOptions.columnDefs, vmReq.getRunSearchData())
	}
</script>


