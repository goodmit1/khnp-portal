<%@page import="com.fliconz.fm.mvc.util.MsgUtil"%>
<%@page import="org.json.JSONObject"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%
 
request.setAttribute("MENU_TITLE", MsgUtil.getMsgWithDefault("registration" + request.getParameter("KUBUN") , new String[]{}, "담당 업무 등록 및 변경"));
%>
<layout:extends name="base/index">
    <layout:put block="content">
    	<form id="mainForm"  >
    	<jsp:include page="list.jsp"></jsp:include>
    	</form>
    </layout:put>
    <layout:put block="popup_area"> 
	</layout:put>
</layout:extends>