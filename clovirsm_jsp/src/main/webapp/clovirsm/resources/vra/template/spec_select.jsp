<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String template_id = request.getParameter("UID");
	request.setAttribute("template_id", template_id == null ? ("uid_" + (long)Math.floor(Math.random() * 1000000000000l)): template_id);

	String DC_ID = request.getParameter("DC_ID");
	request.setAttribute("DC_ID", DC_ID == null ? -1: DC_ID);

	String SPEC_ID = request.getParameter("SPEC_ID");
	request.setAttribute("SPEC_ID", SPEC_ID == null ? -1: SPEC_ID);

	String SPEC_NM = request.getParameter("SPEC_NM");
	request.setAttribute("SPEC_NM", SPEC_NM == null ? "": SPEC_NM);

	String CPU_CNT = request.getParameter("CPU_CNT");
	request.setAttribute("CPU_CNT", CPU_CNT == null ? 1: CPU_CNT);

	String RAM_SIZE = request.getParameter("RAM_SIZE");
	request.setAttribute("RAM_SIZE", RAM_SIZE == null ? 1: RAM_SIZE);

	
	request.setAttribute("RAM_SIZE_MIN", request.getParameter("RAM_SIZE_MIN") == null ? 0: request.getParameter("RAM_SIZE_MIN"));
	request.setAttribute("RAM_SIZE_MAX", request.getParameter("RAM_SIZE_MAX") == null ? 0: request.getParameter("RAM_SIZE_MAX"));
	request.setAttribute("CPU_CNT_MIN", request.getParameter("CPU_CNT_MIN") == null ? 0: request.getParameter("CPU_CNT_MIN"));
	request.setAttribute("CPU_CNT_MAX", request.getParameter("CPU_CNT_MAX") == null ? 0: request.getParameter("CPU_CNT_MAX"));
	String DISK_SIZE = request.getParameter("DISK_SIZE");
	request.setAttribute("DISK_SIZE", DISK_SIZE == null ? 10: DISK_SIZE);

	String DISK_UNIT = request.getParameter("DISK_UNIT");
	request.setAttribute("DISK_UNIT", DISK_UNIT == null ? "G": DISK_UNIT);

	String DD_FEE = request.getParameter("DD_FEE");
	request.setAttribute("DD_FEE", DD_FEE == null ? 0: DD_FEE);
%>
<div id="${template_id}" name="${template_id}" class="section-row1">
	<input type="hidden" id="${template_id}_UID" name="UID" v-model="form_data.UID"/>
	<div  class="margin10">
		<fm-input id="${template_id}_CPU_CNT"  style="width: 400px" title="<spring:message code="NC_VM_CPU_CNT" text="CPU" />" name="CPU_CNT" required="true"  class="chkMinMax"   min="${CPU_CNT_MIN}"   max="${CPU_CNT_MAX}" >
		</fm-input>
	</div>	
 	<div  class="margin10">
		<fm-input id="${template_id}_RAM_SIZE" style="width: 400px" title="<spring:message code="NC_VM_RAM_SIZE" text="Memory" />(GB)" name="RAM_SIZE" required="true" class="chkMinMax"    min="${RAM_SIZE_MIN}"   max="${RAM_SIZE_MAX}"  >
		</fm-input>
	</div>
	<div  class="margin10">
		<fm-output id="${template_id}_DISK_SIZE" name="DISK_SIZE"   title="<spring:message code="NC_VM_DISK_SIZE" text="DISK" />(GB)" ></fm-output>
	</div>   
	<div  class="margin10">
		<!-- <fm-spin id="${template_id}_DISK_SIZE" name="DISK_SIZE" required="true" title="<spring:message code="NC_VM_DISK_SIZE" text="DISK(GB)" />" class="inline compact" style="display:inline-block;" onchange="${template_id}_calcFee()" min="1" max="9999">
			<fm-select id="${template_id}_DISK_UNIT" name="DISK_UNIT" class="inline" :options="${template_id}_diskUnitOptions" style="width:50px;display:inline-block;" onchange="${template_id}_calcFee()"></fm-select>
		</fm-spin>
		 -->
		 <fm-output title="<spring:message code="label_DD_FEE" text="비용"/>" id="${template_id}_DD_FEE" name="DD_FEE" :value="formatNumber(form_data.DD_FEE)" ></fm-output>
	</div>
	<script>
		var ${template_id}_diskUnitOptions = {
			G: 'GB',
			T: 'TB'
		}

		var ${template_id}_button = newPopup("${template_id}_button", "${template_id}");
		var ${template_id}_param = {
			UID: '${template_id}',
			DC_ID: ${DC_ID},
			SPEC_ID: '0',
			SPEC_NM: '${SPEC_NM}',
			CPU_CNT: ${CPU_CNT},
			RAM_SIZE: ${RAM_SIZE},
			DISK_SIZE: ${DISK_SIZE},
			DISK_UNIT: '${DISK_UNIT}',
			DD_FEE: ${DD_FEE}
		};
		var ${template_id}_vue = new Vue({
			el: '#${template_id}',
			data: {
				form_data: ${template_id}_param
			}
		});
		var ${template_id}_callback = '${callback}';

		function select_${template_id}_spec(spec, callback){
			${template_id}_vue.form_data.SPEC_ID = spec.SPEC_ID;
			${template_id}_vue.form_data.SPEC_NM = spec.SPEC_NM;
			${template_id}_vue.form_data.CPU_CNT = spec.CPU_CNT;
			${template_id}_vue.form_data.DISK_SIZE = spec.DISK_SIZE;
			${template_id}_vue.form_data.DISK_UNIT = 'G';
			${template_id}_vue.form_data.RAM_SIZE = spec.RAM_SIZE;
			${template_id}_vue.form_data.SPEC_INFO = getSpecInfo(${template_id}_vue.form_data);
			${template_id}_calcFee();
			if(callback) callback();
		}

		function ${template_id}_tuingSpecInfo(){
			var data = ${template_id}_vue.form_data;
			data.SPEC_INFO = getSpecInfo(data.SPEC_NM, data.CPU_CNT, data.RAM_SIZE, data.DISK_SIZE, data.DISK_UNIT, data.DISK_TYPE_NM);
		}

		function ${template_id}_calcFee(){
			setTimeout(function(){
				getDayFee(${template_id}_vue.form_data.DC_ID,${template_id}_vue.form_data.CPU_CNT, ${template_id}_vue.form_data.RAM_SIZE, '', ${template_id}_vue.form_data.DISK_SIZE, ${template_id}_vue.form_data.DISK_UNIT, ${template_id}_vue.form_data.SPEC_ID, '${template_id}_DD_FEE', function(data){
					${template_id}_vue.form_data.DD_FEE = data.FEE;
					if(calcTotalFee) calcTotalFee(${template_id}_vue.form_data);
				})
			}, 10);
		}
		$('#${template_id}_CPU_CNT').change(function(){
			${template_id}_calcFee();
		})
		$('#${template_id}_RAM_SIZE').change(function(){
			${template_id}_calcFee();
		})
		$('#${template_id}_DISK_SIZE').change(function(){
			${template_id}_calcFee();
		})
		$('#${template_id}_SPEC_NM').css('width', '100px');
		$('#${template_id}_SPEC_INFO').css('width', '155px');
		//$('#${template_id}_DISK_SIZE').parent().css('width', '161px');
		$('#${template_id}_DISK_SIZE').css('min-width', '104px').css('width', '80px').css('text-align', 'right');
		$('#${template_id}_DISK_UNIT').css('min-width', '50px').css('width', '50px');
		$('#${template_id}_DD_FEE').css('width', '104px').css('text-align', 'right');
		$('label[for="${template_id}_DD_FEE"]').css('width', '90px').css('text-align', 'right');
		${template_id}_calcFee();
	</script>
</div>