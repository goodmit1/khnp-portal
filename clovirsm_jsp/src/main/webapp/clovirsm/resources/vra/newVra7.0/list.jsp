<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.clovirsm.service.popup.CategorySearchPopupService"%>
<%@page import="com.fliconz.fw.runtime.util.SpringBeanUtil"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--  접근 목록 조회 -->

<%
Map param=new HashMap();
param.put("PAR_CATEGORY_ID", "0");
CategorySearchPopupService categorySearchPopupService = (CategorySearchPopupService)SpringBeanUtil.getBean("categorySearchPopupService");
List<Map> list = categorySearchPopupService.list(param);
System.out.println(list);
request.setAttribute("list", list);
%>
<div style="padding: 17px;" id="input_area">
	<div class="scrollCategoryTeb" id="scrollCategoryTeb">
 		<div class="categoryItem select" id="0">
 			<div class="categoryLabel">All Items </div>
 		</div>
		<c:forEach items="${ list }" var="i" varStatus="status">
			<div class="categoryItem" id="${i.CATEGORY}">
	 			<div class="categoryLabel">${ i.CATEGORY_NM }</div>
	 		</div>
		</c:forEach>
		<div class="categoryBut Prv" id="categoryButPrv"><i></i></div>
		<div class="categoryBut Next" id="categoryButNext"><i></i></div>
	</div>
	<div class="listSearchBar">
	
		<div style="display: inline-block; width: 150px; float: left; margin-right:10px;  height: 34px;">
			<fm-select id="S_SEARCH"  emptystr="" select_style="width:100%; padding: 6px; border-width: 1px; border-style: solid;  border-color: #706f6f #706f6f #706f6f #706f6f"></fm-select>
		</div>
		<div style="display: inline-block; width:85%; float: left;  height: 34px;">
			<fm-input id="S_APP_NM" name="APP_NM" placeholder='<spring:message code="input_placeholder" text="정보를 입력해주세요" />' input_style="width: 100%; padding: 7px; border-width: 1px; border-style: solid;  border-color: #706f6f;" onkeyup="KeyupSearchItem();"></fm-input>
		</div>
		
	</div>
	<div id="listContext" class="listContext"></div>
	<c:if test="${popupid == null}">
		<jsp:include page="input_popup.jsp"></jsp:include>

	</c:if>	
</div>
<script>
var pram = new Object();
var infoPram = new Object();
var vraItem = new Array();
var vraItemArr = new Array();
var vraSearchObj = new Object();
var vraSearchArr = new Array();
var start = 0;
var end = 50;
var req = new Req();
// -- 검색 Select바 DATA
vraSearchObj.name = '<spring:message code="name" text="명칭" />';
vraSearchObj.value = 'name';
vraSearchArr[0] = vraSearchObj;
vraSearchObj = new Object();
vraSearchObj.name = '<spring:message code="NC_VRA_CATALOG_CMT" text="설명" />';
vraSearchObj.value = 'CMT';
vraSearchArr[1] = vraSearchObj;
// --

$( document ).ready(function() {
	chekLeftScrollWid();
	
	searchVra();
	
	fillOptionByData("S_SEARCH",vraSearchArr,"<spring:message code="NC_LICENSE_LICENSE_TOTAL" text="전체" />","value","name");
	$(window).resize(function (){
		chekLeftScrollWid();
	});
	$(".categoryItem").on("click",function(){
		$(".scrollCategoryTeb").children().removeClass("select");
		$(this).addClass("select");
		start = 0;
		end = 50;
		pram.CATEGORY_ID =$(this).attr("id");
		searchVra();
	});
	$("#categoryButPrv").on("click",function(){
		var scrollChange = $("#scrollCategoryTeb").scrollLeft()-300;
		$("#scrollCategoryTeb").stop().animate({scrollLeft:scrollChange},600);
		chekLeftScroll();
		
	});
	$("#categoryButNext").on("click",function(){
		var scrollChange = $("#scrollCategoryTeb").scrollLeft()+300;
		$("#scrollCategoryTeb").stop().animate({scrollLeft:scrollChange},600);
		chekLeftScroll();
	});
	
	$("#listSearchBarClear").on("click",function(){
		$('#S_APP_NM').val("");
		KeyupSearchItem();
	});
	
	$("#listContext").scroll(function(event){
		chekTopScroll();
	});
});
	

	function openReq(obj)
	{
		var id = $(obj).attr("data-id");
		var idx = $(obj).attr("data-index");
		var info = vraItemArr[idx];
		req.putData(info);
		openVraPopup(req, id);
	
	}
	
	// 검색 input에 keyup이 있을 때 검색 해주는 메소드
	function KeyupSearchItem(e){
		$(".listContext").empty();
		var SearchFilter = $("#S_SEARCH").val();
		if(SearchFilter == 'name'){
			for(var i=0; i<vraItemArr.length; i++){
				if(vraItemArr[i].CATALOG_NM.indexOf($("#S_APP_NM").val()) != -1)
					drawItemOne(vraItemArr[i],i);
			}
		} else if(SearchFilter == 'CMT'){
			for(var i=0; i<vraItemArr.length; i++){
				if(vraItemArr[i].CATALOG_CMT.indexOf($("#S_APP_NM").val()) != -1)
					drawItemOne(vraItemArr[i],i);
			}
		} else{
			for(var i=0; i<vraItemArr.length; i++){
				if(vraItemArr[i].CATALOG_CMT.indexOf($("#S_APP_NM").val()) != -1 || vraItemArr[i].CATALOG_NM.indexOf($("#S_APP_NM").val()) != -1)
					drawItemOne(vraItemArr[i],i);
				 
			}
		}
	}
	
	
	// 스크롤바 위치를 체크하여 categoryBut을 보여주거나 안보여주는 메소드
	function chekLeftScroll(){
		setTimeout(function() {
			if($("#scrollCategoryTeb").scrollLeft() != 0){
				$("#categoryButPrv").css("display","inherit");
			    $("#categoryButNext").css("display","inherit");
			} else{
				$("#categoryButPrv").css("display","none");
			    $("#categoryButNext").css("display","inherit");
			}
		}, 700);
	}
	
	
	
	function chekTopScroll(){
		var elmnt = document.getElementById("listContext");
		if($("#listContext").scrollTop() > (elmnt.scrollHeight - elmnt.clientHeight )-50){
			start = end;
			if(vraItem.length/50 < 2){
				end = end + vraItem.length%end;
			} else{
				end += 50;
			}
			drawItem(vraItem);
		}
	}
	
	
	// 창크기를 확인하여 스크롤이 생기는지 안생기는지 확인하는 메소드
	function chekLeftScrollWid(){
		var elmnt = document.getElementById("scrollCategoryTeb");
		if (elmnt.offsetWidth < elmnt.scrollWidth) {
			chekLeftScroll();
			} else {
			   $("#categoryButPrv").css("display","none");
			   $("#categoryButNext").css("display","none");
		}
	}

	
	
	
	
	// ajax로 데이터를 가져오는 메소드
	function searchVra(){
		pram.PAR_CATEGORY_ID = $(".scrollCategoryTeb").find(".select").attr("id");
		pram.ORDER_CD = "A";
		if(pram.PAR_CATEGORY_ID == 0){
			pram.PAR_CATEGORY_ID = null;
		}
		vraItemArr = new Array();
		$.ajax({
		    type:"post",
		    data: pram,
		    url:"/api/vra_catalog/list/list_NC_VRA_CATALOG/",
		    success: function(data) {
				$(".listContext").empty();
				vraItemArr = dataSave(data);
				drawItem(data); 
		    }
		  });
	}

	// 데이터를 array에 넣어주는 메소드
	function dataSave(data){
			vraItem = new Array();
			for(var i=0; i< data.length; i++){
	    		vraItemArr[i] =  data[i];
			}
	    		vraItem = data;
				return vraItem;
	}
	
	
	
	
	
	function drawItemOne(data,i){
		$(".listContext").append('<div class="listContextItem" id="itemCode"> '+
				'<div class="listContext-bar"> '+
				'<div class="listContext-bar-icon" style="background-repeat: no-repeat; background-position: center; background-image: url( \'data:'+data.ICON_TYPE +";base64,"+data.ICON+'\' );">'+
				'<i class="addListBtn" onclick="openReq(this);return false;" data-index="'+ i +'" data-id="'+data.CATALOG_ID+'"" id="'+data.CATALOG_ID+'"></i>'+
				'</div>'+
				'</div> '+
				'<div class="listContext-title">'+data.CATALOG_NM+'</div>'+
				'<div class="listContext-contents">'+data.CATALOG_CMT+'</div>'+
				'<div id="listContext-bottom'+i+'"class="listContext-bottom">'+
				'</div></div>');
		$("#listContext-bottom"+i+"").append('<span class="listContext-tag blue" style="max-width: 250px;"><spring:message code="label_VRA_FEE" text="" /> '+formatNumber(data.FEE)+'</span>');
		
	}
	
	// 데이터를 가져와 Item을 그려주는 메소드
	function drawItem(data){
		 
		if(data.length < end){
			end = data.length;
		}
		 
    	for(var i=start; i< end; i++){
    		$(".listContext").append('<div class="listContextItem" id="itemCode"> '+
    									'<div class="listContext-bar"> '+
    									'<div class="listContext-bar-icon" style="background-repeat: no-repeat; background-position: center; background-image: url( \'data:'+data[i].ICON_TYPE +";base64,"+data[i].ICON+'\' );">'+
    									'<i class="addListBtn" onclick="openReq(this);return false;" data-index="'+ i +'" data-id="'+data[i].CATALOG_ID+'" id="'+data[i].CATALOG_ID+'"></i>'+
    									'</div>'+
    									'</div> '+
    									'<div class="listContext-title">'+data[i].CATALOG_NM+'</div>'+
    									'<div class="listContext-contents">'+data[i].CATALOG_CMT+'</div>'+
    									'<div id="listContext-bottom'+i+'"class="listContext-bottom">'+
    									'</div></div>');
    		$("#listContext-bottom"+i+"").append('<span class="listContext-tag blue" style="max-width: 250px;"><spring:message code="label_VRA_FEE" text="" /> '+formatNumber(data[i].FEE)+'</span>');
    	}
    	
    }


</script>


