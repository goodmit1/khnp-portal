<%@page contentType="text/html; charset=UTF-8" %>
<%
String popupid = request.getParameter("popupid");
request.setAttribute("popupid", popupid);
request.setAttribute("isAppr","N");
request.setAttribute("callback", "parent." + popupid + "_onAfterSave");

%>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/popup_main">
    <layout:put block="content">
		<div id='${popupid}' class="menu-layout-3 form-panel " style="width: 900px;">
			<form id="mainForm"  >
				<jsp:include page="list.jsp"></jsp:include>
			</form>
		</div>
		<div id="popup_area"></div>
		<script>
			function openVraPopup(req, catalogid, reqid, dt)
			{
				openVra(req, catalogid, reqid, dt);
				$("#step1").hide();
				$("#search_area").hide();
				$("#step2").show();
				parent.${popupid}_setStep(2)
			}
			function goFirst(step, oldStep)
			{
				$("#step2" ).hide();
				$("#step1").show();
				$("#search_area").show();

			}

			$(document).ready(function(){
			})
		</script>
	</layout:put>
</layout:extends>
