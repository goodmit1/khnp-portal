<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
    <%
	 
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String direct = request.getParameter("direct");
	request.setAttribute("direct", direct);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);

%>
<style>
#${popupid} .modal-dialog{
	width:990px;
	height:600px;

}
</style>
<fm-modal id="${popupid}" title="<spring:message code="btn_vra_create" text="VRA생성요청"/>" cmd="header-title" >
	<span slot="footer" id="${popupid}_footer">
		<input v-if="${isAppr == 'N'}" type="button" class="btn" id="vra_prevBtn" value="<spring:message code="btn_prev" text="이전" />" @click="goFirst()" />

		<input v-if="${isAppr == 'N'} && ${direct == null && action == 'insert'}  " type="button" class="btn vra_saveBtn" value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_to_work()" />
		<%-- <input v-if="${isAppr == 'N'} && ${direct == null && action == 'insert'}  " type="button" class="btn vra_saveBtn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="${popupid}_request()" /> --%>
		<input v-if="${isAppr == 'N'} && (${action == 'insert'} && form_data.CUD_CD == 'C') || ${action == 'save'} || ${direct == 'Y'}" type="button" class="btn vra_saveBtn"  value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_save()" />
	</span>
	<div class="form-panel" >
		<iframe src="/clovirsm/resources/vra/newVra/_popup.jsp?popupid=${popupid}&callback=parent.${popupid}_onAfterSave" id="${popupid}Frm" style="width:100%;height:400px;border:none"></iframe>
	</div>
	<script>



		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				step :0,

				stepTitle : ['<spring:message code="btn_vra_create_step1" text="1. 카탈로그 선택"/>', '<spring:message code="btn_vra_create_step2" text="2. 정보 입력"/>'],

				form_data: ${popupid}_param
			},
			methods: {
				goFirst:function()
				{
					this.setStep(1)
					var obj = document.getElementById("${popupid}Frm");

					var objDoc = obj .contentWindow || obj .contentDocument;
					objDoc.goFirst();


				},
				setStep :function(step1)
				{

					this.step = step1;
					this.setStepTitle();
				},
				setStepTitle:function()
				{
					var headerTitle = '<spring:message code="btn_vra_create" text="VRA생성요청"/>';


					headerTitle += ' > ' +  this.stepTitle[this.step-1];
					$('#${popupid}_title').text(headerTitle);
					if(this.step>1)
					{
						$("#vra_prevBtn").show();
						$(".vra_saveBtn").show();
					}
					else
					{
						$("#vra_prevBtn").hide();
						$(".vra_saveBtn").hide();
					}


				}
			}
		});
		 

		function ${popupid}_setStep(step)
		{
			${popupid}_vue.setStep(step)
		}
		function ${popupid}_click(vue, param){
			 

				${popupid}_param = {

				};
				${popupid}_vue.form_data = ${popupid}_param;

			return true;
		}

		function ${popupid}_onAfterOpen(){

			${popupid}_vue.goFirst();

		}

		function ${popupid}_to_work(){
			var obj = document.getElementById("${popupid}Frm");

			var objDoc = obj .contentWindow || obj .contentDocument;


			objDoc.vra_to_work();
		}

		function ${popupid}_request(){
			var obj = document.getElementById("${popupid}Frm");

			var objDoc = obj .contentWindow || obj .contentDocument;


			objDoc.vra_deploy();
		}

		function ${popupid}_save(){
			var obj = document.getElementById("${popupid}Frm");

			var objDoc = obj .contentWindow || obj .contentDocument;


			objDoc.vra_save();
		}
		function ${popupid}_onAfterSave()
		{
			<c:if test="${callback != null && callback != ''}">
			${callback}();
			</c:if>
			$('#${popupid}').modal('hide');
		}


	</script>
</fm-modal>