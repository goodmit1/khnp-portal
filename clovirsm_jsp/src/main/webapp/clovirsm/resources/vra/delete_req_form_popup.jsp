<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<fm-modal id="delete_req_form_popup" title="<spring:message code="delete_request" text="삭제요청"/>" cmd="header-title" >
	<span slot="footer">
		<c:if test="${sessionScope.WORK_SHOW }">
			<input type="button" class="btn newBtn contentsBtn tabBtnImg new top5" value="<spring:message code="btn_work" text="작업함에 담기"/>" onclick="delete_req_to_work()" />
		</c:if>
		<input type="button" class="btn newBtn contentsBtn tabBtnImg new top5" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="delete_req_request()" />
	</span>
	<form id="delete_req_form_popup_form" action="none">
		<spring:message code="confirm_delete_request" text="삭제요청하시겠습니까?"/>
	</form>
	<script>
		var popup_delete_req_form = newPopup("delete_req_form_popup_button", "delete_req_form_popup");
		var delete_req_form_popup_param = {};
		var delete_req_form_popup_vue = new Vue({
			el: '#delete_req_form_popup',
			data: {
				form_data: delete_req_form_popup_param
			}
		});
		function delete_req_form_popup_click(vue, param){
			 
			if(param.CATALOGREQ_ID){
				delete_req_form_popup_param = $.extend({}, param);
				delete_req_form_popup_vue.form_data = delete_req_form_popup_param;
				return true;
			} else {
				delete_req_form_popup_param = null;
				alert(msg_select_first);
				return false;
			}
		}

		function delete_req_to_work(){
			post('/api/vra_catalog/deleteReq', delete_req_form_popup_param,  function(data){
				search();
				$('#delete_req_form_popup').modal('hide');

				if(!confirm(msg_complete_work_continue))
				{
					location.href="/clovirsm/workflow/work/index.jsp";
					return;
				}
			});
		}

		function delete_req_request(){
			delete_req_form_popup_param.DEPLOY_REQ_YN = 'Y';
			post('/api/vra_catalog/deleteReq', delete_req_form_popup_param,  function(data){
				search();
				$('#delete_req_form_popup').modal('hide');
				alert('<spring:message code="label_req_complete" text="요청이 완료되었습니다"/>');
			});
		}

	</script>
</fm-modal>