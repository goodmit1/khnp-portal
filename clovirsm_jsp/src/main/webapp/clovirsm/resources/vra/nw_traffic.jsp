<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>

<script src="/res/js/sankey.js"></script>
<script src="/res/js/dependency-wheel.js"></script>
<script src="/res/js/accessibility.js"></script>
<div class="col col-sm-5">
	<div class="hourSel"><fm-select onchange="onclickNWTraffic()" style="width: 100px;   height: 30px;    position: absolute;  z-index: 100;" :options="[{ val:'24', title:'1Day'},{ val:'72', title:'3Day'},{ val:'168', title:'7Day'},{ val:'240', title:'10Day'},{ val:'360', title:'15Day'},{ val:'720', title:'30Day'}]" keyfield="val" titlefield="title" id="HOURS" name="HOURS"></fm-select>	</div>
	
	<div id="container" style="height: 400px" ></div>
</div>
<div class="col col-sm-7">
	<div id="trafficTable" class="ag-theme-fresh" style="height: 400px" ></div>
</div>	
	<script>
		var trafficTable;

		function drawFlow(data){
			Highcharts.chart('container', {

			    chart:{ type: 'dependencywheel'},
			    title: {
					text: ""
				},
			    

			    series: [{
			        keys: ['from', 'to', 'weight'],
			        data:  data ,
			        type: 'dependencywheel',
			        name: 'N/W Traffic(kb)',
			        
			        size: '95%'
			    }]

			});
		}
		$(function() {


			/*var columnDefs = [{headerName : "<spring:message code="NC_VM_VM_NM" text="" />",field : "VM_NM"},
				{headerName : "<spring:message code="PORT" text="PORT" />",field : "PORT"},
				{headerName : "<spring:message code="TOTAL_BYTE" text="BYTE(kb)" />",field : "TOTAL_BYTES", format:'number', valueFormatter:function(params){
					return formatNumber(params.value/1024,0)
				}},
				
				];*/
				var columnDefs = [{headerName : "<spring:message code="FROM" text="FROM" />",field : "FROM"},
					{headerName : "<spring:message code="TO" text="TO" />",field : "TO", width:300},
					{headerName : "<spring:message code="TOTAL_BYTE" text="BYTES(kb)" />", width:120,field : "TOTAL_BYTE", format:'number', valueFormatter:function(params){
						return formatNumber(params.value/1024,0)
					}},
					
					];	
			var
			gridOptions = {
				hasNo : true,
				columnDefs : columnDefs,
				//rowModelType: 'infinite',
				//rowSelection : 'single',
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false
			}
			trafficTable = newGrid("trafficTable", gridOptions);
		 	

		});
		
		function onclickNWTraffic(){
			if(!form_data.CATALOGREQ_ID) return;
			  
			 post('/api/vra_catalog/nw_traffic', form_data, function(data){
				 drawFlow(data.FLOW);
				 //trafficTable.setData(data.PORT_TRAFFIC)
				 trafficTable.setData(data.FLOW_BYTES)
			 })
		}
	</script>