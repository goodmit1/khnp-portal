<<%@page import="com.fliconz.fm.mvc.util.MsgUtil"%>
<%@page import="org.json.JSONObject"%>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
 
request.setAttribute("MENU_TITLE", MsgUtil.getMsgWithDefault("NEW_VM_TITLE_5" + request.getParameter("KUBUN") , new String[]{}, "기간 연장"));
%>
<layout:extends name="base/index">
    <layout:put block="content">
<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
    	<form id="mainForm"  >
    		<fmtags:include page="list.jsp" />
    	</form>
    </layout:put>
</layout:extends>