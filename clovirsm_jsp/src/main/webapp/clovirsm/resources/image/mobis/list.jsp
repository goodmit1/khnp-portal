<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
String IMG_NM = request.getParameter("IMG_NM");
request.setAttribute("IMG_NM", IMG_NM == null ? "null" : "'" + IMG_NM + "'");
%>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm">
			<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" " name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀" />"></fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="DC_ID"
				emptystr=" " keyfield="DC_ID" titlefield="DC_NM"
				name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_IMG_NM" name="IMG_NM" title="<spring:message code="NC_IMG_IMG_NM" text="서버명" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-input id="S_GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="서버명" />"></fm-input>
		</div>
		<c:if test="${sessionScope.STATE }">
			<div class="col col-sm">
				<fm-select url="/api/code_list?grp=PURPOSE" id="S_PURPOSE"
					emptystr=" "
					name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>">
				</fm-select>
			</div>
		</c:if>
		<div class="col col-sm">
			<fm-select-yn id="S_EXPIRE_YN" emptystr=" "
				name="EXPIRE_YN" title="<spring:message code="NC_VM_EXPIRE_YN" text="만료여부"/>">
			</fm-select-yn>
		</div>
		<div class="col btn_group">
			<fm-sbutton cmd="search" class="searchBtn" onclick="vmImageSearch()"  ><spring:message code="btn_search" text="검색"/></fm-sbutton>
		</div>
		<br>
		<div class="col btn_group_under">

			<fm-popup-button popupid="vm_create_popup" popup="/clovirsm/popup/vm_detail_tab_form_popup.jsp?direct=Y&action=insert" cmd="update" class="exeBtn" param="getParam4CreateVM()"><spring:message code="label_vm_req" text="서버생성요청"/></fm-popup-button>
			<fm-popup-button v-show="false" popupid="img_insert_save_popup" popup="../../../popup/mobis/img_detail_form_popup.jsp?action=insert&callback=onAfterModify" cmd="update" param="imgReq.getData()"><spring:message code="btn_modify" text="생성 정보 수정"/></fm-popup-button>
			<fm-popup-button popupid="img_delete_popup" popup="/clovirsm/popup/delete_req_form_popup.jsp?svc=image&key=IMG_ID&callback=vmImageSearch" cmd="delete" class="delBtn" param="imgReq.getData()"><spring:message code="btn_delete_req" text="삭제요청"/></fm-popup-button>

			<fm-popup-button v-show="false" popupid="img_req_info_popup" popup="../../../popup/mobis/img_detail_form_popup.jsp" cmd="update" param="imgReq.getData()"><spring:message code="label_img_info" text="템플릿 정보"/></fm-popup-button>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area" style="height:340px">
	<div class="table_title">
		<spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="vmImageGrid_total" style="color: royalblue;">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div>
	</div>
	<div id="vmImageGrid" class="ag-theme-fresh" style="height: 300px" ></div>
	<jsp:include page="detail.jsp"></jsp:include>
</div>
<script>
	var imgReq = new Req();
	vmImageGrid;

	$(function() {

		var
		vmImageGridColumnDefs = [ {
			headerName : "",
			hide : true,
			field : "IMG_ID"
		},{
			headerName : "<spring:message code="NC_DC_DC_NM" text="데이터 센터" />",
			field : "DC_NM",
			maxWidth: 180,
			width: 120,
			headerTooltip:'<spring:message code="NC_DC_DC_NM" text="데이터센터명" />',
		},{
			headerName : "<spring:message code="NC_IMG_FROM_ID_NM" text="원본서버명" />",
			field : "VM_NM",
			maxWidth: 200,
			width: 200
		},{
			headerName : "<spring:message code="NC_IMG_IMG_NM" text="템플릿명" />",
			field : "IMG_NM",
			maxWidth: 200,
			width: 200
		},{
			headerName : "<spring:message code="NC_IMG_DISK_SIZE" text="디스크 사이즈" />",
			field : "DISK_SIZE",
			maxWidth: 150,
			width: 150,
			cellStyle:{'text-align':'right'},
			valueFormatter:function(params){
				return getTemplateDiskSize(params.data);
			}
		},{
			headerName : "<spring:message code="NC_VM_GUEST_NM" text="OS명" />",
			field : "GUEST_NM",
			maxWidth: 300,
			width: 300,
			tooltipField:"GUEST_NM",
		},
		<c:if test="${sessionScope.STATE }">
		{
			headerName : "<spring:message code="PART" text="부품"/>",
			field : "ORG_CATEGORY"
		},
		</c:if>
		{
			headerName : "<spring:message code="NC_SNAPSHOT_TASK_STATUS_CD" text="작업상태" />",
			field : "TASK_STATUS_CD_NM",
			maxWidth: 120,
			width: 120,
			valueGetter: function(params) {
				if(params.data.CUD_CD ){
					params.data.TASK_STATUS_CD_NM = params.data.CUD_CD_NM + ' ' + params.data.APPR_STATUS_CD_NM;
				}
				return params.data.TASK_STATUS_CD_NM;
			}
		},{
			headerName : "<spring:message code="FM_TEAM_TEAM_CD" text="팀" />",
			field : "TEAM_NM",
			maxWidth: 140,
			width: 140
		},{
			headerName : "<spring:message code="FM_TEAM_USER_NAME" text="담당자" />",
			field : "USER_NAME",
			maxWidth: 140,
			width: 140
		},{
			headerName : "<spring:message code="NC_VM_INS_TMS" text="등록일시"/>",
			field : "INS_DT",
			maxWidth: 180,
			width: 180,
			valueGetter: function(params) {
		    	return formatDate(params.data.INS_DT,'datetime')
			}
		}  ];
		var
		vmImageGridOptions = {
			hasNo : true,
			columnDefs : vmImageGridColumnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onSelectionChanged : function(e) {
				var arr = vmImageGrid.getSelectedRows();
				imgReq.getInfo('/api/image/info?IMG_ID='+ arr[0].IMG_ID, function(data){
					if(data.APPR_STATUS_CD == 'W'){
						if(data.CUD_CD == 'C') {
							data.TASK_STATUS_CD_NM = '<spring:message code="label_req_ongoing"/>';
						} else if(data.CUD_CD == 'U') {
							data.TASK_STATUS_CD_NM = '<spring:message code="label_save_ongoing"/>';
						} else if(data.CUD_CD == 'D') {
							data.TASK_STATUS_CD_NM = '<spring:message code="label_del_ongoing"/>';
						}
					}
					form_data.DISK_UNIT="G";
					setButtonClickable('vm_create_popup_button', isRunnable(data));
					setButtonClickable('img_save_popup_button', isRequestable(data));
					setButtonClickable('img_delete_popup_button', isDeletable(data) || isRequestable(data));
					setButtonClickable('img_owner_change_popup_button', isRequestable(data));
				});

			},
			onRowDoubleClicked: function(e){
				var data = e.data;
				if(data.CUD_CD){
					if(data.CUD_CD == 'C'){
						$('#img_insert_save_popup_button').trigger('click');
					} else if(data.CUD_CD == 'D'){
						$('#img_req_info_popup_button').trigger('click');
					} else {
						$('#img_save_popup_button').trigger('click');
					}
				} else {
					$('#img_save_popup_button').trigger('click');
				}
			}
		}
		vmImageGrid = newGrid("vmImageGrid", vmImageGridOptions);

		if(${IMG_NM}){
			$("#S_IMG_NM").val(${IMG_NM});
			searchvue.form_data.IMG_NM = ${IMG_NM};
		}
		vmImageSearch();
	});
	function getParam4CreateVM(){
		var param = imgReq.getData();
		param.TEMPLATE_NM= param.IMG_NM;
		param.FROM_ID= param.IMG_ID;
		param.VM_NM = "";
		param.INS_ID = null;
		param.TEAM_CD = null;


		 
		return param;
	}
	// 조회
	function vmImageSearch() {
		imgReq.clearData();
		imgReq.search('/api/image/list' , function(data) {
			vmImageGrid.setData(data);
		});
	}

	// 저장
	function vmImageSave(){
		if(form_data.IMG_ID && form_data.IMG_ID != null){
			imgReq.save('/api/image/save', function(){
				vmImageSearch();
			});
		} else {
			alert(msg_select_first);
		}
	}

	// 엑셀 내보내기
	function exportExcel()
	{
		vmImageGrid.exportCSV({fileName:'Image'})
		  //exportExcelServer("mainForm", '/api/image/list_excel', 'Image',vmImageGrid.gridOptions.columnDefs, imgReq.getRunSearchData())
	}

	function onAfterModify(data){

	}

	function select_S_CATEGORY(data){
		searchvue.form_data.CATEGORY = data.CATEGORY;
		searchvue.form_data.CATEGORY_NM = data.CATEGORY_NM;
		$("#S_CATEGORY_NM").val(data.CATEGORY_NM);
	}

</script>