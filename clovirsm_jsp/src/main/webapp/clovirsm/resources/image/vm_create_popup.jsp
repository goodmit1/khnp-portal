<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<fm-modal id="vm_create_popup" title="<spring:message code="label_vm_req" text="" />" >
<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
	<fmtags:include page="_vm_create.jsp" />
</fm-modal>