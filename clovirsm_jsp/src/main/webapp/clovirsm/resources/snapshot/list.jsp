<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<!--  접근 목록 조회 -->
<style>
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->


			<c:if test="${sessionScope.ADMIN_YN == 'Y'}">
			<div class="col col-sm">
					<fm-select url="/api/etc/team_list" id="S_TEAM_CD" emptystr=" "
								name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀"/>"></fm-select>
			</div>
			</c:if>
			<div class="col col-sm">
					<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="DC_ID"
						emptystr=" " keyfield="DC_ID" titlefield="DC_NM"
						name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>"></fm-select>
			</div>

			<div class="col col-sm">
					<fm-input id="VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="서버명" />"></fm-input>
			</div>
		<div class="col btn_group nomargin">

			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>
		</div>
		<div class="col btn_group_under">
			<fm-sbutton cmd="delete" class="delBtn contentsBtn tabBtnImg top5 del" id="snapshot_delete_button" onclick="deleteReq()"   ><spring:message code="btn_delete" text="삭제"/></fm-sbutton>

		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>

	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 600px" ></div>
	</div>
	<jsp:include page="detail.jsp"></jsp:include>

</div>
<script>
					var req = new Req();
					var mainTable;


					$(function() {


						var
						columnDefs = [ {
							headerName : "",
							hide : true,
							field : "SNAPSHOT_ID"
						},  {
							headerName : "<spring:message code="NC_SNAPSHOT_SNAPSHOT_NM" text="스냅샷명" />",
							field : "SNAPSHOT_NM"
						},{
							headerName : "<spring:message code="NC_DC_DC_NM" text="데이터센터명" />",
							field : "DC_NM"
						},{
							headerName : "<spring:message code="NC_VM_VM_NM" text="서버명" />",
							field : "VM_NM"
						},{
							headerName : "<spring:message code="NC_VM_RUN_CD" text="서버상태" />",
							field : "RUN_CD_NM"
						},{
							headerName : "<spring:message code="NC_SNAPSHOT_TASK_STATUS_CD" text="작업상태" />",
							field : "TASK_STATUS_CD_NM"
						},{
							headerName : "<spring:message code="FM_TEAM_TEAM_CD" text="팀" />",
							field : "TEAM_NM"
						},{
							headerName : "<spring:message code="FM_TEAM_USER_NAME" text="담당자" />",
							field : "USER_NAME",
							maxWidth: 140,
							width: 140
						},{
							headerName : "<spring:message code="INS_TMS" text="등록일시"/>",
							field : "INS_TMS",
							valueGetter: function(params) {
						    	return formatDate(params.data.INS_TMS,'datetime')
							}
						}  ];
						var
						gridOptions = {
							hasNo : true,
							columnDefs : columnDefs,
							//rowModelType: 'infinite',
							rowSelection : 'single',
							//sizeColumnsToFit: false,
							cacheBlockSize: 100,
							rowData : [],
							enableSorting : true,
							enableColResize : true,
							 enableServerSideSorting: false,
							 onSelectionChanged : function() {
									var arr = mainTable.getSelectedRows();
									setButtonClickable('snapshot_delete_button', isRequestable(arr[0]));
									req.getInfo('/api/snapshot/info?SNAPSHOT_ID='
											+ arr[0].SNAPSHOT_ID);
								},
						}
						mainTable = newGrid("mainTable", gridOptions);
						search();
					});

					// 조회
					function search() {

						setButtonClickable('snapshot_delete_button', false);
						req.search('/api/snapshot/list' , function(data) {
							mainTable.setData(data);
						});
					}
					// 삭제
					function deleteReq() {


						req.del('/api/snapshot/delete', function() {
							search();
						});
					}
					// 엑셀 내보내기
					function exportExcel()
					{
						mainTable.exportCSV({fileName:'Snapshot'})	
					  	//exportExcelServer("mainForm", '/api/snapshot/list_excel', 'Snapshot',mainTable.gridOptions.columnDefs, req.getRunSearchData())
					}

				</script>


