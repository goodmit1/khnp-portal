<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%
	String popupid = request.getParameter("popupid");
	if(popupid == null || "".equals(popupid)){
		popupid = "newVm_iframe";
	}
	request.setAttribute("popupid", popupid);
	request.setAttribute("isAppr","N");
	request.setAttribute("callback", "parent." + popupid + "_onAfterSave");
%>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/popup_main">
	<layout:put block="content">
		<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
		<style>

			.Editor-editor {
    			height: 200px;
    		}
   		</style>
		<script src="/res/js/editor.js"></script>
		<div id='${popupid}' class="menu-layout-3 form-panel  ">
			<fmtags:include page="__popup.jsp" />
		</div>
	</layout:put>
</layout:extends>