<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="detail-panel">
<form id="${param.popupid}_form" action="none" >
	<div id="step1" class="popup_area" >
		<jsp:include page="step1.jsp"></jsp:include>
	</div>
	<div id="step2" style="display: none;margin-top: 40px;" class="popup_area">
		<div style="float: right;">
			<input type="button" class="btn saveBtn" style="display:none;" value="<spring:message code="btn_work" text="작업함에 담기"/>" onclick="${popupid}_to_work()" />
			<input type="button" class="btn saveBtn" style="display:none;" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="${popupid}_request()" />
		</div>
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#tab1" data-toggle="tab"><spring:message code="detail" text="상세정보"/></a>
			</li>
			<c:if test="${sessionScope.FW_SHOW }">
			<li>
				<a href="#tab2" data-toggle="tab"><spring:message code="access_control" text="접근통제"/></a>
			</li>
			</c:if>
		</ul>
		<div class="tab-content ">
			<div class="form-panel tab-pane active" id="tab1">
				<jsp:include page="vm_input.jsp"></jsp:include>
			</div>
			<div class="form-panel tab-pane" id="tab2">
				<div class="panel-body">
					<jsp:include page="user_list.jsp"></jsp:include>
				</div>
			</div>
		</div>

	</div>
</form>
</div>
<div id="popup_area" class="popup_area"></div>
<script>
	var ${popupid}_param = {
			DC_ID: null,
			DC_NM: null,
			OS_ID: null,
			OS_NM: null,
			VM_ID: null,
			VM_NM: null,
			PURPOSE: null,
			P_KUBUN: null,
			SPEC_ID: null,
			SPEC_NM: null,
			SPEC_INFO: null,
			CATEGORY: null,
			CPU_CNT: 0,
			RAM_SIZE: 0,
			DISK_SIZE: 0,
			DISK_UNIT: 'G',
			USE_MM : 6,
			CMT: null,
			root_category_id: null
		};
	var ${popupid}_vue = new Vue({
		el: '#${popupid}',
		data: {
			form_data: ${popupid}_param
		}
	});

	function showStep(step, oldStep){
		try{
			if(oldStep && step > oldStep && eval("!step" + step + "_checkVal()")){
				return;
			}
		}catch (e) {}
		if(step == 1){
			//parent.setFrameHeight("600");
		}else if(step == 2){
			if(!${popupid}_checkServerInfo()){
				return;
			}
			${popupid}_vue.form_data.VM_NM = "";
			$("#step2").show();
			//$(parent.document.getElementsByTagName("html")).animate({scrollTop : 999}, 400);
			${popupid}_validationDisk();
			$("#${popupid}_CMT").Editor("setText", "");
			//parent.setFrameHeight("1000");
			if(${popupid}_vue.form_data.root_category_id == "2") {
				//parent.setFrameHeight("1110");
			}
			showWorkBtn();
			if(!isStep2) {
				var fwData = {};
				fwData.IU = "I"
				fwData.IU_NM = "<spring:message code="btn_new" text="신규" />";
				fwData.CUD_CD = "C";
				fwData.CIDR = _USER_IP_;
				fwData.PORT = "";
				fwData.USE_MM = 6;
				fwData.TEAM_CD = _TEAM_CD_;
				fwData.TEAM_NM = _TEAM_NM_;
				fwData.FW_USER_ID = _USER_ID_;
				fwData.USER_NAME = _USER_NM_;
				fwData.VM_USER_YN = "Y";
				${popupid}_vmFwListGrid.insertRow(fwData);
				setVueData(${popupid}_vue, 'FW_IU',         fwData.IU);
				setVueData(${popupid}_vue, 'FW_IU_NM',      fwData.IU_NM);
				setVueData(${popupid}_vue, 'FW_CUD_CD',     fwData.CUD_CD);
				setVueData(${popupid}_vue, 'FW_CIDR',       fwData.CIDR);
				setVueData(${popupid}_vue, 'FW_PORT',       fwData.PORT);
				setVueData(${popupid}_vue, 'FW_USE_MM',     fwData.USE_MM);
				setVueData(${popupid}_vue, 'FW_TEAM_CD',    fwData.TEAM_CD);
				setVueData(${popupid}_vue, 'FW_TEAM_NM',    fwData.TEAM_NM);
				setVueData(${popupid}_vue, 'FW_USER_ID',    fwData.FW_USER_ID);
				setVueData(${popupid}_vue, 'FW_USER_NAME',  fwData.USER_NAME);
				setVueData(${popupid}_vue, 'FW_VM_USER_YN', fwData.VM_USER_YN);
				${popupid}_setEnabledFWForm(true);
			}
			isStep2 = true;
		}
	}
	var isStep2 = false;
	function showWorkBtn(){
		$(".saveBtn").show();
	}

	$(document).ready(function(){
		${popupid}_fw_init();
		${popupid}_setEnabledFWForm(false);
		//setTimeout(function(){parent.newVm_vue.goStep(null, 1);},100);

	});
	$("#${popupid}_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false, 'placeholder':'<spring:message code="MSG_CMT_REQUIRED" text="설명(필수)"/>'});
</script>