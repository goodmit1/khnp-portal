<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> <%@ taglib prefix="fmtags" uri ="http://fliconz.kr/jsp/tlds/fmtags" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<div class="leftStepInfo">
	<ol>
		<li id="leftStepInfo1" class="leftStepInfoC active">1. 서비스 선택</li>
		<li id="leftStepInfo2" class="leftStepInfoC">2. 사양 선택</li>
		<li id="leftStepInfo3" class="leftStepInfoC">3. 기타</li>
		<li id="leftStepInfo4" class="leftStepInfoC">4. 최종점검 및 완료</li>
	</ol>
</div>

