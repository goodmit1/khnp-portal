<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %> <%@ taglib prefix="fmtags" uri ="http://fliconz.kr/jsp/tlds/fmtags" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<div class="section" style="width:100%;" id="img_category_row"  ></div>
	 	<div class="section" style="width:100%;" id="img_row">

		</div>

		<div class="col col-sm-12" v-if="form_data.VM_ID != null">
		 	<fm-output   id="P_KUBUN" emptystr=" "  :value=" (form_data.CATEGORY_NM?  form_data.CATEGORY_NM + '>' : '' ) + (form_data.P_KUBUN_NM?form_data.P_KUBUN_NM:'') "
				name="P_KUBUN_NM" title="">
			</fm-output>
		</div>
		<div class="col col-sm-6"  id="${popupid}_OS_ID_cell" style="display:none">
			<fm-select id="${popupid}_OS_ID" name="OS_ID"
				title="<spring:message code="NC_IMG_IMG_NM" text="템플릿"/>"
				required="true"
				emptystr=" " keyfield="IMG_ID" titlefield="IMG_NM"
				disabled  v-if="${action != 'insert'}">
			</fm-select>
			<fm-select id="${popupid}_OS_ID" name="FROM_ID"
				title="<spring:message code="NC_IMG_IMG_NM" text="템플릿"/>"
				required="true"
				emptystr=" " keyfield="IMG_ID" titlefield="IMG_NM"
				disabled v-if="Number(form_data.root_category_id) &lt; 0 && ${action != 'insert'}">
			</fm-select>
			<fm-output name="TEMPLATE_NM" title="<spring:message code="NC_VM_FROM_NM" text="원본 서버명"/>" v-if="form_data.FROM_ID && form_data.FROM_ID.substring(0,1) == 'S' && ${action == 'insert'}"></fm-output>
			<fm-output name="TEMPLATE_NM" :value="form_data.CATEGORY_NM + ' : '  + form_data.TEMPLATE_NM"  title="<spring:message code="NC_VM_VM_IMG_CATEGORY" text="카테고리"/>" v-if="((form_data.FROM_ID && form_data.FROM_ID.substring(0,1) == 'G') || form_data.OS_ID) && ${action == 'insert'}"></fm-output>

		</div>

	<script src="/clovirsm/resources/reqVm/reqVM.js"></script>
	<script>

	function ${popupid}_onChangeFromDC(target){
		var $this = $(target);
		${popupid}_addImgCategorySelect($this.val());

	}

var mappingParam={};


function setLastCategory(){
	post("/api/vmReq/list/selectLastCategory/", {}, function(data){
		data = data[0];
		if(data == null) {
			$("#root_img_category").trigger('change.root_img_category');
			return;
		}
		 
		var arr = data.CATEGORYS.split(",");
		$("#${popupid}_P_KUBUN").val(data.P_KUBUN);
		$("#${popupid}_PURPOSE").val(data.PURPOSE);
		$("#root_img_category").val(arr[0]);
		$("#root_img_category").change();
		setCategoryVal(arr, 0);
	})
}
function setCategoryVal(arr, idx){
	if(arr[idx] != null) {
		var target = $("#img_category_" + arr[idx]);
		if(target[0] != null){
			target.val(arr[++idx])
			target.change();
		}
		setTimeout(function(){
			setCategoryVal(arr, idx)
		}, 50);;
	}
}

mappingParam[-3]={text:"<spring:message code="NC_VM_VM_IMG_NAME" text="개인템플릿"/>", url:"/api/vmReq/list/list_IMG_SELECT/"};
mappingParam[-4]={text:"<spring:message code="NC_VM_VM_CNT_NAME" text="자주사용서버"/>",url:"/api/vmReq/list/list_IMG_SELECT_4_CNT/"};
mappingParam[-5]={text:"<spring:message code="NC_VM_VM_TMS_NAME" text="최근사용서버"/>",url:"/api/vmReq/list/list_IMG_SELECT_4_TMS/"};
var pKubunList=${fmtags:getCodeJson('P_KUBUN')}; //암호화 방식 코드
var ${popupid}_all_category = new CategoryAll();
// 전체 카테고리 정보 가져오기
function ${popupid}_addRootImgCategorySelect(){


	post('/api/vmReq/list/list_CATEGORY_SELECT_INUSE/', null, function(data){

		for(var i=0; i< data.length; i++)
		{
			${popupid}_all_category.addCategory( data[i].CATEGORY_IDS.split(','), data[i].CATEGORY_FULL_NM.split('>'), data[i]);

		}
		var $row = $('<div class="col col-sm-12" style="border-bottom:none;">');
		$('#img_category_row').prepend($row);
		var $selectWrapper = $('<div></div>');
		var $label = $('<label for="root_img_category" class="control-label input-group-title grid-title value-title" style="background: none; line-height: 25px; min-width: 59px;"><spring:message code="NC_VM_VM_IMG_CATEGORY" text="서비스"/></label>');
		$label.appendTo($selectWrapper);
		$selectWrapper.appendTo($row);
		var $select = $('<select id="root_img_category" name="root_category" style="width:110px; margin-bottom: 10px; margin-left:15px; " class="root_category img_category_select form-control input" level="0">');
		$select.appendTo($selectWrapper);
		fillOptionByData('root_img_category', ${popupid}_all_category.getChildrenList('0') , null, 'CATEGORY_ID', 'CATEGORY_NM');
		$select.on('change.root_img_category', function(e){
			var $this = $(this);
			var img_category_id = $this.val();





			if(img_category_id == '') return;
			${popupid}_vue.form_data.root_category_id = img_category_id;

			${popupid}_clearTemplateInfo();

			if(img_category_id < 0){
				var $img_category_rows = $('#img_category_row .img_category_select');
				for(var i = $img_category_rows.length-1; i >= 1 ; i--){
					$($img_category_rows.get(i)).remove();
				}
				${popupid}_addDCSelect(img_category_id);
			} else {
				${popupid}_addImgCategorySelect(img_category_id, 1);
			}
		});



		setLastCategory();
	});
}
function ${popupid}_clearTemplateInfo()
{
	${popupid}_vue.form_data.DC_ID = null;
	${popupid}_vue.form_data.OS_ID = null;
	${popupid}_vue.form_data.FROM_ID = null;
	${popupid}_vue.form_data.SPEC_ID = null;
	${popupid}_vue.form_data.DISK_SIZE = 0;
	${popupid}_vue.form_data.DISK_UNIT= 'G';
	$('#${popupid}_DISK_SIZE').prop('disabled', true);
	$('#${popupid}_DISK_SIZE').parent().find('button').prop('disabled', true);
	$('#${popupid}_DISK_UNIT').prop('disabled', true);

	$('#${popupid}_OS_ID option').remove();

}


	function ${popupid}_addImgCategorySelect(parent_img_category_id, level){
		var $img_category_rows = $('#img_category_row .img_category_select');
		for(var i = $img_category_rows.length-1; i >= level ; i--){
			$($img_category_rows.get(i)).remove();
		}
		${popupid}_clearTemplateInfo();
		var data = ${popupid}_all_category.getChildrenList(parent_img_category_id)

			if(data.length == 0){
				${popupid}_addDCSelect(parent_img_category_id);
				return;
			}
			var $select = $('<select id="img_category_'+parent_img_category_id+'" name="img_category_'+parent_img_category_id+'" class="img_category_select form-control input" level="'+level+'"style="width: 110px; margin-bottom: 10px; min-width:auto;margin-left:15px;"></select>');
			$("#root_img_category").parent().append($select);
			$("#img_category_" + parent_img_category_id).on('change.img_category_'+parent_img_category_id, function(e){
				var $this = $(this);
				var img_category_id = $this.val();
				if(img_category_id == '') return;
				${popupid}_addImgCategorySelect(img_category_id, Number($this.attr('level'))+1);
			});
			fillOptionByData('img_category_' + parent_img_category_id, data, null, 'CATEGORY_ID', 'CATEGORY_NM');
			
			$("#img_category_" + parent_img_category_id).trigger('change.img_category_'+parent_img_category_id)
			
	}
	//add 개발/운영 combo
	function ${popupid}_addDCSelect(img_category_id){
		var pkubunList = ${popupid}_all_category.getPKubunList(img_category_id, pKubunList);
		var $select = $('<select id="${popupid}_P_KUBUN" name="P_KUBUN" class="form-control img_category_select input"  style="width: 110px; margin-bottom: 10px; min-width:auto; margin-left:15px; display:block;"></select>');
		$("#root_img_category").parent().append($select);
		${popupid}_vue.form_data.P_KUBUN=pkubunList[0].ID;
		fillOptionByData('${popupid}_P_KUBUN', pkubunList, null, 'ID', 'TITLE'  );

		$("#${popupid}_P_KUBUN").on('change.P_KUBUN', function(e) {
				${popupid}_addImgSelect(img_category_id, $("#${popupid}_P_KUBUN").val());
				try {
					${popupid}_chgCPURAM();
				} catch(e) { }
			}
		);
		$("#${popupid}_P_KUBUN").trigger("change.P_KUBUN")

	}
	/* 템플릿 목록 */
	function ${popupid}_addImgSelect(img_category_id, p_kubun){


		var list =  ${popupid}_all_category.getTemplateList(img_category_id, p_kubun);
		fillOptionByData('${popupid}_OS_ID', list, null, 'OS_ID', 'OS_NM');
		${popupid}_vue.form_data.OS_ID=list[0].OS_ID;
		if(list.length==1)
		{
			$("#${popupid}_OS_ID_cell").hide();
		}
		else
		{

			$("#${popupid}_OS_ID_cell").show();
		}

		$("#${popupid}_OS_ID").trigger("change");
	}


	</script>