<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>

<div class="panel-body ">



	<jsp:include page="../_selOs.jsp" />
	<div class="col col-sm-6"  v-if="${!IS_AUTO_VM_NAME || sessionScope.ADMIN_YN == 'Y'}"  >


		<fm-input id="${popupid}_VM_NM" placeholder="${IS_AUTO_VM_NAME ? '입력하지 않으면 자동 생성':''}" name="VM_NM"
			title="<spring:message code="NC_VM_VM_NM" text="VM명"/>" ${!IS_AUTO_VM_NAME ? 'required':''}  ></fm-input>
	</div>

	<div class="col col-sm-6">
		<fm-spin id="${popupid}_USE_MM" name="USE_MM" class="inline compact" style="width: 300px" title="<spring:message code="NC_VM_USE_MM" text="사용기간(개월)" />" min="6"  ></fm-spin>
	</div>
	<fmtags:include page="/clovirsm/popup/_spec_form.jsp" />



	<div class="col col-sm-12"  >
		<div>

			<c:if test="${isAppr != 'N'}">
				<div v-html="form_data.CMT" class="output hastitle" style="min-height: 200px;overflow-y: auto;word-break: break-word;"></div>

			</c:if>
			<c:if test="${isAppr == 'N'}">
				<fm-textarea id="${popupid}_CMT" name="CMT"  ></fm-textarea>
			</c:if>
		</div>
	</div>
</div>
<script>
	<c:if test="${action != 'insert'}">
	${popupid}_addRootImgCategorySelect();
	</c:if>


	var diskUnitOptions = {
		G: 'GB',
		T: 'TB'
	}

	var ${popupid}_callback = '${callback}';

	function ${popupid}_makeInsertReqParam(addParam){

		${popupid}_vue.form_data.CATEGORY=$(".img_category_select:nth-last-child(2)").val()
		${popupid}_vue.form_data.OS_ID=$("#${popupid}_OS_ID").val();
		${popupid}_vue.form_data.P_KUBUN=$("#${popupid}_P_KUBUN").val();
		var param = ${popupid}_vue.form_data;
		try {
			param.NC_VM_FW_LIST = JSON.stringify(${popupid}_vmFwListGrid.getData()) // fw param
		}
		catch(e)
		{
			console.log(e);
		};
		<c:if test="${isAppr == 'N'}">
		param.CMT = $("#${popupid}_CMT").Editor("getText" );
		</c:if>
		if(addParam) param = $.extend(param, addParam);
		try {
			${popupid}_makeDataDiskParam(param); //Data Disk param
		} catch(e) {
			console.log(e);
		}
		return param;
	}


	function ${popupid}_to_work(){
		if(!${popupid}_validation_vm_req()) return;
		var param = ${popupid}_makeInsertReqParam();
		post('/api/vmReq/insertReq', param, function(data){
			if(confirm(msg_complete_work_move_save)){
				parent.location.href="/clovirsm/workflow/work/index.jsp";
				return;
			}
			if(${popupid}_callback != ''){
				eval(${popupid}_callback + '(${popupid}_vue.form_data);');
			}
			$('#${popupid}').modal('hide');
		});
	}

	function ${popupid}_request(){
		if(!${popupid}_validation_vm_req()) return;
		var param = ${popupid}_makeInsertReqParam({DEPLOY_REQ_YN: 'Y'});
		post('/api/vmReq/insertReq', param, function(data){
			alert(msg_complete_work);
			if(${popupid}_callback != ''){
				eval(${popupid}_callback + '(${popupid}_vue.form_data);');
			}
			$('#${popupid}').modal('hide');
		});
	}

	function ${popupid}_save(){
		if(!${popupid}_validation_vm_req()) return;
		var param = ${popupid}_makeInsertReqParam();
		post('/api/vmReq/insertReq', param, function(data){
			alert(msg_complete);
			if(${popupid}_callback != ''){
				try {
					eval(${popupid}_callback + '(${popupid}_vue.form_data);');
				} catch(e){
					eval('parent.' + ${popupid}_callback + '(${popupid}_vue.form_data);');
				}
			}
			$('#${popupid}').modal('hide');
		});
	}

	function ${popupid}_getSpecSearchParam(){
		return {
			DC_ID: ${popupid}_vue.form_data.DC_ID,
			SPEC_NM: $('#${popupid}_SPEC_NM').val()
		}
	}

	function ${popupid}_validation_vm_req(){
		if(!${popupid}_checkServerInfo()) return false;
		if($('#${popupid}_DISK_SIZE').hasClass('invalid-size')){
			alert('<spring:message code="msg_disk_size_re_setting"/>');
			$('#${popupid}_DISK_SIZE').select();
			return false;
		}
		try
		{
			var rows = ${popupid}_vmFwListGrid.getData();
			for(var i = 0 ; i < rows.length; i++){
				var row = rows[i];
				if(row.CIDR == undefined || row.CIDR == ''  ){
					alert('<spring:message code="msg_invalid_fw_info"/>');
					return false;
				} else {

				}
			}
		}catch(e){
			console.log(e);
		}
		return true;
	}

	function select_${popupid}_spec(spec, callback){
		${popupid}_vue.form_data.SPEC_ID = spec.SPEC_ID;
		${popupid}_vue.form_data.SPEC_NM = spec.SPEC_NM;
		${popupid}_vue.form_data.CPU_CNT = spec.CPU_CNT;
		${popupid}_vue.form_data.DISK_SIZE = spec.DISK_SIZE;
		${popupid}_vue.form_data.DISK_UNIT = 'G';
		${popupid}_vue.form_data.RAM_SIZE = spec.RAM_SIZE;
		${popupid}_vue.form_data.SPEC_INFO = getSimpleSpecInfo(spec);
		$("#${popupid}_SPEC_NM").val(spec.SPEC_NM);
		$("#${popupid}_SPEC_INFO").text(${popupid}_vue.form_data.SPEC_INFO)
		getDayFee(${popupid}_vue.form_data.DC_ID, ${popupid}_vue.form_data.CPU_CNT, ${popupid}_vue.form_data.RAM_SIZE, '',${popupid}_vue.form_data.DISK_SIZE, ${popupid}_vue.form_data.DISK_UNIT, ${popupid}_vue.form_data.SPEC_ID, '${popupid}_DD_FEE')
		if(callback) callback();
	}

	$(document).ready(function(){
		$("#${popupid}_P_KUBUN, #${popupid}_PURPOSE").change(function(){
			$(".img_category_select:last").trigger("change")
		})
	})





	function ${popupid}_checkServerInfo(){
		return validate("${popupid}_form");
	}
</script>
