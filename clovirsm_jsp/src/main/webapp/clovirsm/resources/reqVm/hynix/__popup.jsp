<%@page contentType="text/html; charset=UTF-8" %><%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>

<div class="detail-panel">
<form id="${param.popupid}_form" action="none" >
	<div id="step1" class="popup_area">

		<jsp:include page="vm_input.jsp" ></jsp:include>

	</div>
</form>
<c:if test="${sessionScope.FW_SHOW }">
	<div id="step2" style="display:none" class="popup_area" >
		<jsp:include page="user_list.jsp"></jsp:include>
	</div>
</c:if>
</div>
<div id="popup_area" class="popup_area"></div>
<script>
	var ${popupid}_param = {
			DC_ID: null,
			DC_NM: null,
			OS_ID: null,
			OS_NM: null,
			VM_ID: null,
			VM_NM: null,
			PURPOSE: null,
			P_KUBUN: null,
			SPEC_ID: null,
			SPEC_NM: null,
			SPEC_INFO: null,
			CPU_CNT: 1,
			RAM_SIZE: 1,
			DATA_DISK_SIZE: 100,
			OS_DISK_SIZE: 0,
			DISK_UNIT: 'G',

			USE_MM : 6,
			CMT: null,
			root_category_id: null
		};
	var ${popupid}_vue = new Vue({
		el: '#${popupid}',
		data: {
			form_data: ${popupid}_param
		}
	});

	function showStep(step, oldStep){
		$(".centerStep").hide();
		$("#centerStep" + step).show();
		$(".leftStepInfoC").removeClass("active");
		$("#leftStepInfo" + step).addClass("active");
		if(step==3){
			${popupid}_validationDisk();
			$("#${popupid}_CMT").Editor("setText", "");
		}
	}
	$(document).ready(function(){

		try
		{

			${popupid}_fw_init();
			${popupid}_setEnabledFWForm(false);

		}
		catch(e){
			console.log(e)
		}
		${popupid}_click_after();
		$("#${popupid}_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
	});
</script>
