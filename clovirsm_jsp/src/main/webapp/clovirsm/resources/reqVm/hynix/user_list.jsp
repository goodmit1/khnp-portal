<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
			<div class="col col-sm-12">
				<fm-sButton id="${popupid}_add_fw_popup" cmd="update" onclick="${popupid}_add_fw(${popupid}_vmFwListGrid)" :disabled="${isAppr != 'N'}"><spring:message code="btn_add" text="추가"/></fm-sButton>
				<fm-sButton id="${popupid}_delete_check_fw_popup" cmd="update" onclick="${popupid}_delete_check_fw(${popupid}_vmFwListGrid)" :disabled="${isAppr != 'N'}"><spring:message code="btn_delete" text="삭제"/></fm-sButton>
				<fm-sButton id="${popupid}_vmFwSearch_popup" cmd="search" onclick="${popupid}_vmFwSearch()" style="float: right;" :disabled="${isAppr != 'N'}"><spring:message code="btn_reload" text="새로고침"/></fm-sButton>
			</div>
			<div class="col col-sm-12">
				<form id="${popupid}_fw_form" action="none">
					<div class="panel-body">
						<div class="col col-sm-6">
							<fm-ibutton id="${popupid}_FW_USER_NAME" name="FW_USER_NAME"  required="true" title="<spring:message code="FM_USER_USER_NAME" text="이름" />" :disabled="${isAppr != 'N'}">
								<fm-popup-button v-show="false" popupid="${popupid}_user_search_popup" popup="/popup/user_search_form_popup.jsp" cmd="update" param="$('#${popupid}_USER_NAME').val()" callback="${popupid}_select_user"  ><spring:message code="title_chg_owner" text="담당자변경"/></fm-popup-button>
							</fm-ibutton>
						</div>
						<div class="col col-sm-6">
							<fm-input id="${popupid}_FW_CIDR" name="FW_CIDR" required="true" title="<spring:message code="NC_FW_CIDR" text="IP" />" onchange="${popupid}_onchangeFWForm();"  ></fm-input>
						</div>
						<div class="col col-sm-6">
							<fm-input id="${popupid}_FW_PORT" name="FW_PORT" title="<spring:message code="NC_FW_PORT" text="Port" />" onchange="${popupid}_onchangeFWForm();"  ></fm-input>
						</div>
						<div class="col col-sm-6">
							<fm-spin id="${popupid}_FW_USE_MM" name="FW_USE_MM" style="width: 300px" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" min="6" onchange="${popupid}_onchangeFWForm();" ></fm-spin>
						</div>
						<div class="col col-sm-6">
							<fm-select url="/api/code_list?grp=sys.yn" id="${popupid}_FW_VM_USER_YN" name="FW_VM_USER_YN" title="<spring:message code="NC_FW_VM_USER_YN" text="계정생성여부"/>" onchange="${popupid}_onchangeFWForm();"  ></fm-select>
						</div>
						<div class="col col-sm-6">
							<fm-output id="${popupid}_FW_TEAM_NM" name="FW_TEAM_NM" title="<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />"></fm-output>
						</div>

					</div>
				</form>
			</div>
			<div class="col col-sm-12">
				<div id="${popupid}_vmFwListGrid" class="ag-theme-fresh" style="height: 200px" ></div>
			</div>
			<script>

			var ${popupid}_vmFwReq = new Req();
			${popupid}_vmFwListGrid;

			function ${popupid}_fw_init() {
				var
				${popupid}_vmFwListGridColumnDefs = [ {
					headerName : '<spring:message code="NC_FW_CUD_CD" text="구분" />',
					field : 'IU_NM',
					width: 120,
					cellStyle: function(params){
						var style = {
							color: 'black'
						}
						if(params.data.CUD_CD == 'D') style.color = 'red';
						return style;
					},
					valueGetter: function(params){
						if( params.data.IU_NM ) return params.data.IU_NM;
						else return nvl(params.data.CUD_CD_NM,'') + ' ' + nvl(params.data.APPR_STATUS_CD_NM,'');
					}
				},{
					headerName : '<spring:message code="NC_FW_CIDR" text="IP" />',
					field : 'CIDR',
					width: 130
				},{
					headerName : '<spring:message code="NC_FW_PORT" text="Port" />',
					field : 'PORT',
					width: 120
				},{
					headerName : '<spring:message code="NC_FW_FW_USER_ID_NM" text="이름" />',
					field : 'USER_NAME',
					width: 120
				},{
					headerName : '<spring:message code="FM_TEAM_TEAM_NM" text="팀명" />',
					field : 'TEAM_NM',
					width: 150
				},{
					headerName : '<spring:message code="NC_FW_VM_USER_YN" text="계정생성여부" />',
					field : 'VM_USER_YN',
					width: 180,
					valueGetter: function(params){
						 
						if(params.data.VM_USER_YN == undefined || params.data.VM_USER_YN == '') return '';
						return params.data.VM_USER_YN  == 'Y' ? '<spring:message code="label_yes"/>': '<spring:message code="label_no"/>';
					}
				},{
					headerName : '<spring:message code="NC_FW_USE_MM" text="Port" />',
					field : 'USE_MM',
					width: 100
				},  ];
				var
				${popupid}_vmFwListGridOptions = {
					hasNo : true,
					columnDefs : ${popupid}_vmFwListGridColumnDefs,
					//rowModelType: 'infinite',
					rowSelection : 'single',
					sizeColumnsToFit: true,
					cacheBlockSize: 100,
					rowData : [],
					enableSorting : true,
					enableColResize : true,
					enableServerSideSorting: false,
					onRowClicked: function(){
						var arr = ${popupid}_vmFwListGrid.getSelectedRows();
						var data = arr[0];

						setVueData(${popupid}_vue, 'FW_IU', data.IU);
						setVueData(${popupid}_vue, 'FW_IU_NM', data.IU_NM);
						setVueData(${popupid}_vue, 'FW_CUD_CD', data.CUD_CD);
						setVueData(${popupid}_vue, 'FW_CIDR', data.CIDR);
						setVueData(${popupid}_vue, 'FW_PORT', data.PORT);
						setVueData(${popupid}_vue, 'FW_USE_MM', data.USE_MM);
						setVueData(${popupid}_vue, 'FW_TEAM_CD', data.TEAM_CD);
						setVueData(${popupid}_vue, 'FW_TEAM_NM', data.TEAM_NM);
						setVueData(${popupid}_vue, 'FW_USER_ID', data.FW_USER_ID);
						setVueData(${popupid}_vue, 'FW_USER_NAME', data.USER_NAME);
						setVueData(${popupid}_vue, 'FW_VM_USER_YN', data.VM_USER_YN);
						${popupid}_setEnabledFWForm(true);
					}
				}
				${popupid}_vmFwListGrid = newGrid("${popupid}_vmFwListGrid", ${popupid}_vmFwListGridOptions);
				$('#${popupid}_DISK_SIZE').css('width', '100px');
			}

			function ${popupid}_setEnabledFWForm(isEnabled){

					$('#${popupid}_FW_PORT').prop('disabled', !isEnabled);
					$('#${popupid}_FW_USE_MM').parent().find(':input').prop('disabled', !isEnabled);
					$('#${popupid}_FW_USER_NAME').parent().find(':input').prop('disabled', !isEnabled);
					$('#${popupid}_FW_VM_USER_YN').prop('disabled', !isEnabled);

			}

			function ${popupid}_vmFwSearch(){
				${popupid}_setEnabledFWForm(false);
				if(${popupid}_vue.form_data.VM_ID) {
					${popupid}_vmFwReq.searchSub('/api/fw/list', {VM_ID: ${popupid}_vue.form_data.VM_ID,INS_DT: ${popupid}_vue.form_data.INS_DT}, function(data) {
						${popupid}_vmFwListGrid.setData(data);
					});
				}
			}
			function ${popupid}_add_fw(grid){
				var data = {
					IU: 'I',
					IU_NM: '<spring:message code="btn_new" text="신규" />',
					CUD_CD: 'C',
					CIDR: '',
					PORT: '',
					USE_MM: 6,
					TEAM_CD: '',
					TEAM_NM: '',
					FW_USER_ID: '',
					USER_NAME: '',
					VM_USER_YN: ''
				};
				grid.insertRow(data);
				${popupid}_initFWForm();
				setVueData(${popupid}_vue, 'FW_CUD_CD', 'C');
				setVueData(${popupid}_vue, 'FW_VM_USER_YN', 'Y');
				setVueData(${popupid}_vue, 'FW_USE_MM', 6);
				${popupid}_setEnabledFWForm(true);

				$("#${popupid}_user_search_popup_button").click();
			}

			function ${popupid}_initFWForm(){
				setVueData(${popupid}_vue, 'FW_IU', '');
				setVueData(${popupid}_vue, 'FW_IU_NM', '');
				setVueData(${popupid}_vue, 'FW_CUD_CD', '');
				setVueData(${popupid}_vue, 'FW_CIDR', '');
				setVueData(${popupid}_vue, 'FW_PORT', '');
				setVueData(${popupid}_vue, 'FW_USE_MM', '');
				setVueData(${popupid}_vue, 'FW_TEAM_CD', '');
				setVueData(${popupid}_vue, 'FW_TEAM_NM', '');
				setVueData(${popupid}_vue, 'FW_USER_ID', '');
				setVueData(${popupid}_vue, 'FW_USER_NAME', '');
				setVueData(${popupid}_vue, 'FW_VM_USER_YN', '');
			}

			function ${popupid}_delete_check_fw(grid){
				var rows = ${popupid}_vmFwListGrid.getSelectedRows();
				var data = rows[0];
				if(data == undefined) return;
				if(data.IU != undefined){
					if(data.IU == 'I'){
						grid.deleteRow();
						${popupid}_initFWForm();
						${popupid}_setEnabledFWForm(false);
					} else {
						data.IU = 'U';
						data.IU_NM = '<spring:message code="btn_delete" text="삭제"/>';
						data.CUD_CD = 'D';
						grid.updateRow(data);
					}
				} else {
					//if(data.CUD_CD && data.APPR_STATUS_CD)
					{
						data.IU = 'U';
						data.IU_NM = '<spring:message code="btn_delete" text="삭제"/>';
						data.CUD_CD = 'D';
						grid.updateRow(data);
					}
				}
			}

			function ${popupid}_select_user(user, callback){
				 
				setVueData(${popupid}_vue, 'FW_TEAM_CD', user.TEAM_CD);
				setVueData(${popupid}_vue, 'FW_TEAM_NM', user.TEAM_NM);
				setVueData(${popupid}_vue, 'FW_USER_ID', user.USER_ID);
				setVueData(${popupid}_vue, 'FW_USER_NAME', user.USER_NAME);
				setVueData(${popupid}_vue, 'FW_CIDR', user.LAST_LOGIN_IP);
				${popupid}_onchangeFWForm();
				if(callback) callback();
			}

			function ${popupid}_onchangeFWForm(){
				setTimeout(function(){
					var rows = ${popupid}_vmFwListGrid.getSelectedRows();
					var data = rows[0];
					if(data && data != null){
						if(data.CUD_CD == 'D'){
							if(!confirm('<spring:message code="msg_delete_user_edit"/>')) {
								$(obj).val(data['prev_' + obj.name]);
								return;
							}
						}
						if(data.IU != 'I') {
							data.IU = 'U';
							data.IU_NM = '<spring:message code="btn_edit" text=""/>';
							data.CUD_CD = 'U';
						}
						data.CIDR = ${popupid}_vue.form_data.FW_CIDR;
						data.PORT = ${popupid}_vue.form_data.FW_PORT;
						data.USE_MM = ${popupid}_vue.form_data.FW_USE_MM;
						data.TEAM_CD = ${popupid}_vue.form_data.FW_TEAM_CD;
						data.TEAM_NM = ${popupid}_vue.form_data.FW_TEAM_NM;
						data.FW_USER_ID = ${popupid}_vue.form_data.FW_USER_ID;
						data.USER_NAME = ${popupid}_vue.form_data.FW_USER_NAME;
						data.VM_USER_YN = ${popupid}_vue.form_data.FW_VM_USER_YN;
						${popupid}_vmFwListGrid.updateRow(data);
					}
				}, 10);
			}
			</script>