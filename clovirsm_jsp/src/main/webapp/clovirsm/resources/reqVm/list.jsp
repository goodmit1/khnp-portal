<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<style>
.checkbox-inline { width:200px }
</style>

<!--  팀 목록 tree -->

	<div class="col-sm-3 tree"  >
		<div class="panel panel-default" style="margin-bottom: 0px;">
			<div class="panel-body" style="height: 395px; overflow: auto;">
				<fm-tree id="category_tree" url="/api/vmReq/querykey/list_CATEGORY?VM_IMG_NAME=<spring:message code="NC_VM_VM_IMG_NAME" text="개인탬플릿"/>"
					:field_config="{onselect: nodeSelected, root:-1 ,id:'CATEGORY_ID',parent:'PAR_CATEGORY_ID',text:'CATEGORY_NM'}" callback="autoSelectFirstTreeNode"></fm-tree>
			</div>
		</div>
	</div>
	
	<!--  입력 Form -->
	<div class="col-sm-9 right-panel" >
		<!--  조회 조건 및 버튼 -->
		<div id="popup_search_area" class="search-panel panel panel-default">
			<div class="panel-body">
				<input type="hidden" name="CATEGORY_ID" id="S_CATEGORY_ID"/>
				<!-- 조회의 id는 S_를 붙인다. -->
				<div class="col col-sm">
					<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="S_DC_ID"
						keyfield="DC_ID" titlefield="DC_NM"
						name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
					</fm-select>
				</div>
				<div class="col col-sm">
					<fm-input id="S_OS_NM" name="OS_NM" title="<spring:message code="NC_VM_OS" text="NC_VM_OS" />"></fm-input>
				</div>
				<div class="col col-sm">
					<fm-input id="S_GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST" text="OS" />"></fm-input>
				</div>
				<div class="col col-sm">
					<fm-select url="/api/code_list?grp=PURPOSE" id="S_PURPOSE"
						emptystr=" " 
						name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>">
					</fm-select>
				</div>
				<div class="col col-sm">
					<fm-select url="/api/code_list?grp=P_KUBUN" id="S_P_KUBUN"
						emptystr=" "  
						name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
					</fm-select>
				</div>
				<div class="col btn_group">
					<fm-sbutton cmd="search" onclick="template_search()"><spring:message code="btn_search" text="검색"/></fm-sbutton>
				</div>
				
			</div>
		</div>
		<div class="fullGrid" style="height:330px">
			<div id="mainTable" class="ag-theme-fresh" style="height: 100%" ></div>
		</div>
	 
	</div>


<script>
	$(function(){
		var columnDefs = [
			{headerName : "<spring:message code="NC_DC_DC_NM" text="데이터 센터" />", field : "DC_NM"},
			{headerName : "<spring:message code="NC_VM_OS" text="템플릿" />", field : "OS_NM"},
			{headerName : "<spring:message code="NC_VM_GUEST" text="OS" />", field : "GUEST_NM"},
			{headerName : "", field : "SPEC_ID", hide: true},
			{headerName : "", field : "OS_ID", hide: true},
			{headerName : "", field : "FROM_ID", hide: true},
			{headerName : "", field : "DC_ID", hide: true},
			{headerName : "<spring:message code="NC_VM_P_KUBUN" text="구분" />", field : "P_KUBUN_NM"},
			{headerName : "<spring:message code="NC_VM_PURPOSE" text="사용용도" />", field : "PURPOSE_NM"},
			{headerName : "<spring:message code="NC_VM_CPU_CNT" text="CPU" />", field : "CPU_CNT"
				, cellStyle: {'text-align': 'right'}},
			{headerName : "<spring:message code="NC_VM_RAM_SIZE" text="메모리" />", field : "RAM_SIZE"
				, cellStyle: {'text-align': 'right'}, valueGetter: function(params) {
		    		return params.data.RAM_SIZE + "GB";
				}},
			{headerName : "<spring:message code="NC_VM_DISK_SIZE" text="디스크" />", field : "DISK_SIZE"
				, cellStyle: {'text-align': 'right'}, valueGetter: function(params) {
		    		return params.data.DISK_SIZE + params.data.DISK_UNIT + "B";
				}}
		]
		var gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			sizeColumnsToFit: true,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onSelectionChanged : function() {
				var arr = mainTable.getSelectedRows();
				var that = this;
				$.post('/api/vmReq/initFormValue', arr[0], function(data){
					 
					data.USE_MM="6";
					delete data._NOW_;
					setVueFormData(${popupid}_vue,data);
				});
			} 
		}
		mainTable = newGrid("mainTable", gridOptions);
	})

	 

	var vmReq = new Req();

	//템플릿 조회
	function template_search() {
    	post('/api/vmReq/oslist',$("#popup_search_area *" ).serialize(),  function(data) {
   			mainTable.setData(data);
   			 
   		});
	}

	//조회

	function nodeSelected(selectedArr) {
		$("#S_CATEGORY_ID").val(selectedArr[0].CATEGORY_ID);
		template_search();
	}

	var tree_node_data = {
		CATEGORY_ID: -1
	};
	var category_tree_vue = new Vue({
		el: '#category_tree',
		data: {
			form_data: tree_node_data
		}
	});

	function autoSelectFirstTreeNode(){
		$('li[role="treeitem"]:first > a').trigger('click');
	}
</script>

