<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>

<div class="panel-body">
	<jsp:include page="_selOs.jsp" />
	<%-- <jsp:include page="car_part_select.jsp"></jsp:include> --%>
	<div id="${popupid}_tmplListGrid" class="ag-theme-fresh" style="height: 300px;clear:both;display: none;" ></div>
	<div id="${popupid}_partListGrid" class="ag-theme-fresh" style="height: 200px;display: none;" ></div>
</div>
<script>

	var ${popupid}_callback = '${callback}';

	$(function(){
		${popupid}_addRootImgCategorySelect(); //카테고리 항목  load
		var part_columnDefs = [
   			{headerName : "<spring:message code="NC_VM_OS" text="템플릿" />", field : "OS_NM"},
   			{headerName : "<spring:message code="NC_VM_GUEST" text="OS" />", field : "GUEST_NM"},
   			{headerName : "<spring:message code="NC_OS_TYPE_REQUESTER" text="요청자" />", field : "USER_NM"},
   			{headerName : "<spring:message code="FM_TEAM_TEAM_CD" text="팀" />", field : "TEAM_NM"},
   			{headerName : "<spring:message code="NC_OS_TYPE_YEAR" text="년도" />", field : "YEAR", format: "number"},
   			{headerName : "<spring:message code="NC_VM_CPU_CNT" text="CPU" />", field : "CPU_CNT"
   				, cellStyle: {'text-align': 'right'}},
   			{headerName : "<spring:message code="NC_VM_RAM_SIZE" text="메모리" />", field : "RAM_SIZE"
   				, cellStyle: {'text-align': 'right'}, valueGetter: function(params) {
   		    		return params.data.RAM_SIZE + "GB";
   				}},
   			{headerName : "<spring:message code="NC_VM_DISK_SIZE" text="디스크" />", field : "DISK_SIZE"
   				, cellStyle: {'text-align': 'right'}, valueGetter: function(params) {
   		    		return params.data.DISK_SIZE + "GB";
   				}}
   		]
   		var part_gridOptions = {
   			hasNo : true,
   			columnDefs : part_columnDefs,
   			rowSelection : 'single',
   			sizeColumnsToFit: true,
   			cacheBlockSize: 100,
   			rowData : [],
   			enableSorting : true,
   			enableColResize : true,
   			enableServerSideSorting: false,
   			onSelectionChanged : function() {
   				var arr = ${popupid}_partGrid.getSelectedRows();
   				${popupid}_onAfterRowClick(arr[0]);
   			}
   		}
		${popupid}_partGrid = newGrid("${popupid}_partListGrid", part_gridOptions);

		var tmpl_columnDefs = [
            {headerName : "", field : "OS_ID", hide: true},
   			{headerName : "<spring:message code="NC_VM_OS" text="템플릿" />", field : "OS_NM"},
   			{headerName : "<spring:message code="NC_VM_GUEST" text="OS" />", field : "GUEST_NM"},
   			{headerName : "<spring:message code="NC_OS_TYPE_CMT" text="설명" />", field : "CMT"},
   			{headerName : "<spring:message code="NC_VM_CPU_CNT" text="CPU" />", field : "CPU_CNT"
   				, cellStyle: {'text-align': 'right'},width:130},
   			{headerName : "<spring:message code="NC_VM_RAM_SIZE" text="메모리" />", field : "RAM_SIZE"
   				, cellStyle: {'text-align': 'right'},width:130, valueGetter: function(params) {
   		    		return params.data.RAM_SIZE + "GB";
   				}},
   			{headerName : "<spring:message code="NC_VM_DISK_SIZE" text="디스크" />", field : "DISK_SIZE"
   				, cellStyle: {'text-align': 'right'}, valueGetter: function(params) {
   		    		return params.data.DISK_SIZE + "GB";
   				}}
   		]
   		var tmpl_gridOptions = {
   			hasNo : true,
   			columnDefs : tmpl_columnDefs,
   			rowSelection : 'single',
   			sizeColumnsToFit: true,
   			cacheBlockSize: 100,
   			rowData : [],
   			enableSorting : true,
   			enableColResize : true,
   			enableServerSideSorting: false,
   			onSelectionChanged : function() {
   				var arr = ${popupid}_tmplGrid.getSelectedRows();
   				//<![CDATA[
   				if(Number(form_data.root_category_id) > Number(Object.keys(mappingParam)[0])) {
   					${popupid}_vue.form_data.OS_ID = arr[0].OS_ID;
   				}else {
   					${popupid}_vue.form_data.OS_ID = arr[0].OS_ID;
   				}
   				${popupid}_onAfterRowClick(arr[0]);
 				//]]>
   			}
   		}
		${popupid}_tmplGrid = newGrid("${popupid}_tmplListGrid", tmpl_gridOptions);
	})
	function step1_checkVal(){
		if(${popupid}_vue.form_data.OS_ID == null && ${popupid}_vue.form_data.FROM_ID == null) {
			return false;
		}
		return ${popupid}_tmplGrid.getSelectedRows()[0] != null;
	}

	function ${popupid}_onAfterRowClick(rowData) {
		post("/api/vmReq/info/getVmSpecByCpu/", rowData, function(data){
			 
			 
			for(var key in rowData){
				${popupid}_vue.form_data[key] = rowData[key];
			}
			for(var key in data){
				${popupid}_vue.form_data[key] = data[key];
			}
			${popupid}_param.OS_DISK_SIZE = rowData.DISK_SIZE;
			if(${popupid}_vue.form_data.DISK_UNIT == null) {
				${popupid}_vue.form_data.DISK_UNIT = "G";
			}
  			showStep("2", "1");
  			var spec = getSpecInfo(null,${popupid}_vue.form_data.CPU_CNT, ${popupid}_vue.form_data.RAM_SIZE, ${popupid}_vue.form_data.OS_DISK_SIZE,'G');
  			$("#${popupid}_SPEC_INFO").text(spec);
  			if($("#${popupid}_DISK_TYPE_ID option").length>1){
  				$("#${popupid}_DISK_SIZE").parent().find("button").prop("disabled", true);
  				$("#${popupid}_DISK_SIZE").prop("disabled", true);
  				$("#${popupid}_DISK_UNIT").prop("disabled", true);
  			}
		})
	}


</script>
