<%@page contentType="text/html; charset=UTF-8"%><%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<style>
	#input_area > div > div > div > div:nth-child(3) > div > div > div{
		margin-bottom: 1px;
	}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<!-- <div class="col col-sm">
			<fm-select url="/api/code_list?grp=SVC" id="S_SVC_CD" onchange="change_svc()"
				emptystr="<spring:message code="label_all" text="모두"/>" keyfield="SVC_CD" titlefield="SVC_CD_NM"
				name="SVC_CD" title="<spring:message code="NC_REQ_SVC_CD" text="서비스"/>">
			</fm-select>
		</div>
		-->
		<div class="col btn_group nomargin">
			<fm-sbutton id="all_change_owner_button" cmd="update" class="saveBtn contentsBtn tabBtnImg change top5" onclick="all_change_owner()"><spring:message code="btn_all_change_owner" text="담당자 일괄 변경"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area" style="height:340px">
	<div class="box_s">
	<div class="form-panel detail-panel panel panel-default">
		<div class="panel-body">
			<input type="hidden" id="F_SVC_CD" name="SVC_CD" />
			<div class="col col-sm-12">
				<input type="hidden" id="FROM_USER_ID" name="FROM_USER_ID" />
				<fm-ibutton id="FROM_USER_NM" name="FROM_USER_NM" title="<spring:message code="NC_VM_FROM_USER_NM" text="기존 담당자"/>" onchange="resetOwnerData('FROM')">
					<fm-popup-button popupid="from_user_search_form_popup" popup="/popup/user_search_form_popup.jsp" cmd="update" param="{MY_TEAM:'Y'}" callback="select_from_owner_user"></fm-popup-button>
				</fm-ibutton>
			</div>
			<div class="col col-sm-12">
				<input type="hidden" id="TO_USER_ID" name="TO_USER_ID" />
				<input type="hidden" id="TO_TEAM_CD" name="TO_TEAM_CD" />
				<fm-ibutton id="TO_USER_NM" name="TO_USER_NM" title="<spring:message code="NC_VM_TO_USER_NM" text="변경할 담당자"/>" onchange="resetOwnerData('TO')">
					<fm-popup-button popupid="to_user_search_form_popup" popup="/popup/user_search_form_popup.jsp" cmd="update" param="{MY_TEAM:'N'}" callback="select_to_owner_user"></fm-popup-button>
				</fm-ibutton>
			</div>
		</div>
	</div>
	</div>
</div>
<script type="text/javascript">
	function change_svc(){
		form_data.SVC_CD = $('#S_SVC_CD').val();
	}
	function select_from_owner_user(args, callback){
		$('#FROM_USER_ID').val(args.USER_ID);
		$('#FROM_USER_NM').val(args.USER_NAME);

		form_data.FROM_USER_ID = args.USER_ID;
		form_data.FROM_USER_NM = args.USER_NAME;

		if(callback) callback();
	}

	function select_to_owner_user(args, callback){
		$('#TO_USER_ID').val(args.USER_ID);
		$('#TO_USER_NM').val(args.USER_NAME);
		$('#TO_TEAM_CD').val(args.TEAM_CD);

		form_data.TO_USER_ID = args.USER_ID;
		form_data.TO_USER_NM = args.USER_NAME;
		form_data.TO_TEAM_CD = args.TEAM_CD;

		if(callback) callback();
	}

	function resetOwnerData(where){
		if(where == 'FROM'){
			$('#FROM_USER_ID').val('');
			form_data.FROM_USER_ID = '';
		} else if(where == 'TO'){
			$('#TO_USER_ID').val('');
			$('#TO_TEAM_CD').val('');

			form_data.TO_USER_ID = '';
			form_data.TO_USER_NM = '';
			form_data.TO_TEAM_CD = '';
		}
	}


	function all_change_owner(){
		 
		if((  $('#FROM_USER_NM').val() == '' || $('#TO_USER_NM').val() == '')) {
			alert(msg_select_first);
			return;
		}
		if(!confirm('<spring:message code="confirm_change_owner" text="담당자를 변경하시겠습니까?"/>')) return;

		post('/api/all_change_owner/chg_owner', form_data,  function(data){
			alert(msg_complete_work);
		});
	}

	$(document).ready(function(){
		
		form_data.SVC_CD = '';
	});
</script>