<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="fullGrid" id="input_area" style="height:240px">
	<div class="table_title">
		<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span><spring:message code="msg_snap_shot_auto_del_info"/>
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div>
	</div>
	<div id="snapshoptTable" class="ag-theme-fresh" style="height: 200px" ></div>
</div>
<script>
	var req = new Req();
	snapshoptTable;

	$(function() {
		var
		snapShotColumnDefs = [ {
			headerName : '',
			hide : true,
			field : 'SNAPSHOT_ID'
		},  {
			headerName : '<spring:message code="NC_SNAPSHOT_SNAPSHOT_NM" text="스냅샷명" />',
			field : 'SNAPSHOT_NM',
			tooltip: function(params){
				return params.data.SNAPSHOT_NM;
			},
			width: 300
		},{
			headerName : '<spring:message code="NC_SNAPSHOT_CMT" text="설명" />',
			field : 'CMT',
			width: 110
		},{
			headerName : '<spring:message code="NC_SNAPSHOT_TASK_STATUS_CD_NM" text="작업상태" />',
			field : 'TASK_STATUS_CD_NM',
			width: 130
		},{
			headerName : '<spring:message code="NC_SNAPSHOT_INS_TMS" text="생성일시"/>',
			field : 'INS_TMS',
			width: 120,
			valueGetter: function(params) {
		    	return formatDate(params.data.INS_TMS,'date')
			}
		} ];
		var
		snapShotGridOptions = {
			hasNo : true,
			columnDefs : snapShotColumnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			sizeColumnsToFit: false,
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false
		}
		snapshoptTable = newGrid("snapshoptTable", snapShotGridOptions);
	});

	// 조회
	function snapshotSearch(VM_ID) {
		req.search('/api/snapshot/list?VM_ID='+VM_ID , function(data) {
			snapshoptTable.setData(data);
		});
	}

	// 엑셀 내보내기
	function exportExcel(){
		 mainTable.exportCSV({fileName:'user'})
		//exportExcelServer("mainForm", '/api/vm/list_excel', 'Snapshot',snapshoptTable.gridOptions.columnDefs, req.getRunSearchData())
	}

</script>


