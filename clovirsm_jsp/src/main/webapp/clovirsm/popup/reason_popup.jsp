<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String newItemUrl = "/res/comp/apprCdCmt.jsp?prefix=" + popupid ;
%>
<style>

#deploy_del_popup .modal-dialog{
	width : 1000px;
}
#deploy_del_popup .modal-dialog .form-panel.detail-panel{
	height : 500px;
}

</style>

<fm-modal id="${popupid}" title='<spring:message code="${param.title}" text="확인"/>' cmd="header-title">
	<span slot="footer">
		
		<button type="button" class="popupBtn next top5" onclick="${popupid}_save()"><spring:message code="ok" text="확인"/></button>
	</span>
	<div class="form-panel detail-panel" style="height: 150px;">
		<div class="col col-sm-12" style="text-align: left;">
			<div style="display: inline-block; height: 166px; width: 142px; float: left;">
				<label for="item" style="width: 100%; height: 100%; " class="control-label grid-title value-title"><spring:message code="input" text=""/></label>
			</div>
			<div style="display: inline-block; height: 166px; width: 600px;  float: left;  padding: 16px;">
				<jsp:include page="<%=newItemUrl%>"></jsp:include>
			</div>
		</div>
	</div>
	<script>
		var appr = null;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		
		function ${popupid}_click(vue, param, callback){
			 
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
				alert(msg_select_first);
				return;
			}
			${popupid}_vue.form_data = ${popupid}_param;
			${popupid}_vue.form_data.callback=callback;
			return true;
		}
		 
		function ${popupid}_save(){
			 
		 
			var cmt= $("#${popupid}APPR_CMT").val();
			var cd  = $("#${popupid}APPR_CMT_CD").val();
			eval(${popupid}_vue.form_data.callback + "(cd, cmt)");
			$('#${popupid}').modal('hide');
			
		}
	</script>
	<style>

	</style>
</fm-modal>