<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@ taglib prefix="c"
	uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib
	uri="http://www.springframework.org/tags" prefix="spring"%>
<jsp:include page="_cpu_ram_sel.jsp" />
 
<c:if test="${action == 'update'}">
	<div class="col col-sm-6">
		<fm-output id="${popupid}_DISK_SIZE"
			:value="form_data.DISK_SIZE + '' + form_data.DISK_UNIT + 'B'"
			title="<spring:message code="NC_IMG_ALL_DISK_SIZE" text="전체 디스크" />"></fm-output>
	</div>
</c:if>
<div class="col col-sm-6">
	<fm-output id="${popupid}_DD_FEE" name="DD_FEE"
		:value="formatNumber(form_data.DD_FEE)"
		title="<spring:message code="label_DD_FEE" text="비용"/>"></fm-output>
</div>
<div class="col col-sm-12">
	<div class="hastitle">
		<label for="${popupid}_DATA_DISK_TYPE"
			class="control-label grid-title value-title"
			style="vertical-align: top;"> <spring:message code="NC_IMG_DISK" text="디스크" />(GB)
			<button type="button" class="btn btn-primary btn update"
				onclick="${popupid}_newDisk()" title="btn_new">
				<i class="fa fa-plus"></i>
			</button>
		</label>
		<div class="hastitle output" style="height: 100%">
			<table id="${popupid}_data_disk_tbl" style="margin-top: 7px;">
				<tr style="line-height:30px">
					<td>
						<fm-select id="${popupid}_DATA_DISK_TYPE"
							class="DATA_DISK_TYPE" name="DISK_TYPE_ID"
							url="/api/code_list?grp=dblist.com.clovirsm.common.Component.selectDiskType"
							onchange="${popupid}_validationDisk(this)">
						</fm-select>
					</td>
					<td> 
						<fm-input id="${popupid}_DATA_DISK_SIZE_input"
							class="DATA_DISK_SIZE input" style="width:100px;"
							onchange="${popupid}_validationDisk(this)"  >
						</fm-input>
						<fm-select id="${popupid}_DATA_DISK_SIZE_sel"
							class="DATA_DISK_SIZE select" style="width:100px;"
							onchange="${popupid}_validationDisk(this)"  >
						</fm-select>
					</td>
					 
					<td style="padding-left:10px" class="mountPathArea" ></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div class="col col-sm-12">
	<fm-textarea id="CHG_CMT"  name="CHG_CMT" title="<spring:message code="input" text="사유" />" sty="height: 40px;"></fm-textarea>
</div>
<div id="warn_area"></div>
<script>

	// data disk param
	function ${popupid}_makeDataDiskParam(param){
		var tbl = $("#${popupid}_data_disk_tbl");
		if(tbl.length>0) {
			var total = 0;
			var arr = []
			tbl.find("tr").each(function(){
				var data = $(this).prop("data");
				var v = $(this).find(".DATA_DISK_SIZE:visible :input").val();
				var unit = $(this).find(".DATA_DISK_UNIT select").val();
				var diskType = $(this).find(".DATA_DISK_TYPE select").val();
				var diskName = $(this).find(".DATA_DISK_NAME").val();
				if(!v || v=='')	{
					v=0;
				}

				if(unit == 'T')	{
					v *= 1024;

				}
				if(!diskType){
					diskType = "1";
				}
				if(v==0) return;
				data = $.extend(data,{DISK_TYPE_ID:diskType, DISK_SIZE:v, DISK_NM: diskName});
				arr.push(data)
				total += 1*v;
			})
			param.NC_VM_DATA_DISK = JSON.stringify(arr);
			 

		}
	}
	function ${popupid}_validationDisk(obj){

		var trObj = $(obj).parents("tr:first");
		var data = trObj.prop("data");
		var obj = trObj.find(".DATA_DISK_SIZE:visible :input");
		if(obj.length ==0){
			return;
		}
		var v = obj.val();
		var unit = trObj.find(".DATA_DISK_UNIT select").val();
		if(!v || v=='')	{
			v=0;
		}

		if(unit == 'T'){
			v *= 1024;

		}
		if(data != null && data.KUBUN=='DISK' && v < data.DISK_SIZE ){
			alert('<spring:message code="msg_disk_size_re_setting"/>');

		}

		setTimeout(function(){
			${popupid}_calDiskSize()
		}, 100);
		return true;
	}

	// 신규 Data Disk 추가
	function ${popupid}_newDisk(delExclude){
		var tbl = $("#${popupid}_data_disk_tbl tbody  ");
		var trObj = $("<tr>" +  $("#${popupid}_data_disk_tbl tbody tr:first").html()  + "</tr>");
		if(!delExclude){
			trObj.find("td:last-child").html('<input class="DATA_DISK_NAME" style="vertical-align:middle;width:300px" placeholder="Mount Path">');
			trObj.find("td:last-child").append('<button type="button" class="btn btn-primary btn update" onclick="${popupid}_delDisk(this)" title="btn_del"><i class="fa fa-minus"></i></button>');
		}
		var obj = tbl.append(trObj)
		trObj.find(".DATA_DISK_TYPE select").prop("disabled", false);
		trObj.find(".DATA_DISK_SIZE.select").show(); 
		trObj.find(".DATA_DISK_SIZE.input").hide(); 
		trObj.find(".DATA_DISK_SIZE :input").prop("disabled", false);
		trObj.find(".DATA_DISK_UNIT select").prop("disabled", false);
		${popupid}_calDiskSize();
		//${popupid}_calcDiskFee();
		return trObj;

	}

	// 전체 Data Disk 사이즈 계산
	function ${popupid}_calDiskSize(){
		var total  = 0;
		var hasT = false;
		$("#${popupid}_data_disk_tbl tbody .DATA_DISK_SIZE:visible").each(function(){
			var v = $(this).find(":input").val();
			if(!v || v=='')	{
				v=0;
			}
			var unit = $(this).parents("tr:first").find(".DATA_DISK_UNIT select").val();
			if(unit == 'T')	{
				v *= 1024;
				hasT = true;
			}
			total += 1*v;
		})
		if(hasT){
			total = total/1024;
			${popupid}_vue.form_data.DISK_UNIT ='T';
		}
		else{
			${popupid}_vue.form_data.DISK_UNIT ='G';
		}
		${popupid}_vue.form_data.DISK_SIZE =   total;
		//$("#${popupid}_DISK_SIZE").text( total + ${popupid}_vue.form_data.DISK_UNIT + "B");
		${popupid}_calcDiskFee();
	}

	// 디스크 삭제
	function ${popupid}_delDisk(obj){
		if($(obj).parents("tr:first").prop("data") != null && $(obj).parents("tr:first").prop("data").KUBUN=='DISK'){
			var isOK = confirm("<spring:message code="disk_delete_confirm" text="디스크의 모든 데이터가 삭제됩니다. 삭제하시겠습니까?" />")
			if(!isOK) return;
		}

		$(obj).parents("tr:first").remove();
		${popupid}_calDiskSize();

	}
	function ${popupid}_delAllDisk(){
		var trs = $("#${popupid}_data_disk_tbl tbody tr");
		for(var i=1; i < trs.length; i++){
			$(trs[i]).remove();
		}
	}
	// 금액 계산
	function ${popupid}_calcDiskFee(){
		var obj = ${popupid}_vue.form_data;
		var diskTypeSize={};
		$("#${popupid}_data_disk_tbl tbody tr:first .DATA_DISK_TYPE select option").each(function(){
			diskTypeSize[$(this).val()]=0;
		})
		if(${popupid}_vue.form_data.OS_DISK_SIZE){
			diskTypeSize[${popupid}_vue.form_data.DISK_TYPE_ID] = ${popupid}_vue.form_data.OS_DISK_SIZE;
		}
		$("#${popupid}_data_disk_tbl tbody .DATA_DISK_SIZE:visible").each(function(){
			var v = $(this).find(":input").val();
			if(v==0 || v=='') return;
			var unit = $(this).parents("tr:first").find(".DATA_DISK_UNIT select").val();
			var diskType = $(this).parents("tr:first").find(".DATA_DISK_TYPE select").val();
			diskTypeSize[(diskType ? diskType : "1")] += 1*(unit=='T' ? v*1024 : v);
		});
		var keys = extractKeys(diskTypeSize);
		var total = 0;
		for(var i=0; i < keys.length; i++){

			if(i==0){
				getDayFee(obj.DC_ID, obj.CPU_CNT, obj.RAM_SIZE, keys[i],diskTypeSize[keys[i]], 'G', '0', undefined, function(data){
						total += 1*data.FEE;
						$('#${popupid}_DD_FEE').text(formatNumber(total));
					}, obj.GPU_MODEL, obj.GPU_SIZE);
			}
			else{
				getDayFee(obj.DC_ID, 0, 0, keys[i],diskTypeSize[keys[i]], 'G', '', undefined, function(data){
					total += 1*data.FEE;
					$('#${popupid}_DD_FEE').text(formatNumber(total));
				}, obj.GPU_MODEL, obj.GPU_SIZE);
			}
		}

	}
	function ${popupid}_spec_validate(){
	
		
		if(${popupid}_vue.form_data.OLD_SPEC_ID == ${popupid}_vue.form_data.SPEC_ID
				&& ${popupid}_vue.form_data.OLD_CPU_CNT == ${popupid}_vue.form_data.CPU_CNT
				&& ${popupid}_vue.form_data.OLD_RAM_SIZE == ${popupid}_vue.form_data.RAM_SIZE
				&& ${popupid}_vue.form_data.OLD_GPU_SIZE == ${popupid}_vue.form_data.GPU_SIZE
				&& ${popupid}_vue.form_data.OLD_DISK_SIZE == ${popupid}_vue.form_data.DISK_SIZE
				&& ${popupid}_vue.form_data.OLD_DISK_UNIT== ${popupid}_vue.form_data.DISK_UNIT){
				alert('<spring:message code="msg_change_spec"/>');
				return false;
			}
		if($('#${popupid}_DISK_SIZE').hasClass('invalid-size')){
			alert('<spring:message code="msg_disk_size_re_setting"/>');
			$('#${popupid}_DISK_SIZE').select();
			return false;
		}
		var unique = {};
		$.each($(".mountPathArea span"), function (index, item) {
			unique[$(item).text()] = '-';
		})
		var diskSizes = $(".DATA_DISK_SIZE:visible :input");
		for(var i=0; i < diskSizes.length; i++){
			if($(diskSizes[i]).val()==''){
				alert('<spring:message code="msg_empty_disk_size" text="디스크 사이즈값을 입력하셔야 합니다."/>');
				diskSizes[i].focus();
				return false;
				 
			}
		}
		 
		var diskNms = $('input.DATA_DISK_NAME');
		
		var linuxYn = ${popupid}_param.LINUX_YN;
		for(var i=0; i < diskNms.length; i++){
			 
			if($(diskNms[i]).val()==''){
				alert('<spring:message code="msg_empty_disk_name" text="Mount Path값을 입력하셔야 합니다."/>');
				diskNms[i].focus();
				return false;
			}
			if(unique[$(diskNms[i]).val()] ){
				alert('<spring:message code="msg_exist_disk_name" text="이미 존재하는 Mount Path입니다. 다른 값을 입력하세요."/>');
				diskNms[i].focus();
				return false;
			
			}
			
			if(linuxYn=='N'){
				if(!chkWinDriver($(diskNms[i]).val())){
					return false;
				}
			}
			unique[$(diskNms[i]).val()] ='-';
		}
		return true;
	}
	function setUseMM( ){
		if(monthList){
			fillMonth(${popupid}_vue.form_data.p_kubun_arr);
		}
		else{
			var specParam = new Object();
			var monthArray = new Array();
			specParam.KUBUN = 'MONTH';
			post("/api/code_list?grp=dblist.com.clovirsm.common.Component.selectSpecSetting",specParam, function(data){
				monthList={};
				for(var q = 0 ; q < data.length; q++){
					monthList[data[q].ID] = data[q].VAL1.split(",");
				
				 
				}
				fillMonth(${popupid}_vue.form_data.p_kubun_arr);
			});
		}
		
	}
	function ${popupid}_click_after(){
		mappingParam = {};// 개인템플릿 등 제거
		${popupid}_vue.form_data.OLD_GPU_SIZE =  ${popupid}_vue.form_data.GPU_SIZE
		$("#${popupid}_DISK_SIZE").prop("disabled", true);
		$("#${popupid}_DISK_UNIT").prop("disabled", true);
<%--		${popupid}_vue.form_data.DISK_TYPE_ID = "1";--%>
<%--		${popupid}_vue.form_data.DISK_TYPE_ID = ${popupid}_vue.form_data.NC_VM_DATA_DISK[0].DISK_TYPE_ID;--%>
		${popupid}_vue.form_data.SPEC_ID = "0";
		var diskArr = ${popupid}_vue.form_data.NC_VM_DATA_DISK;

		${popupid}_delAllDisk(); // disk
		if(diskArr)	{
			var total = 0;

			for(var i=0; i < diskArr.length; i++){
				var trObj = null;
				if(i>0)	{
					trObj = ${popupid}_newDisk(true);
				}
				else{
					trObj = $("#${popupid}_data_disk_tbl tbody tr:first");
				}
				var diskSize = diskArr[i].DISK_SIZE;
				var diskType = diskArr[i].DISK_TYPE_ID;
				total += 1*diskSize;
				 
				trObj.prop('data', diskArr[i]);
				
				trObj.find(".DATA_DISK_SIZE :input").val(diskSize);
				trObj.find(".DATA_DISK_TYPE select").val(diskType);
				trObj.find(".mountPathArea").html( diskArr[i].DISK_NM  )
				if(diskArr[i].KUBUN=='DISK'){
					trObj.find(".DATA_DISK_TYPE select").prop("disabled", true);
					trObj.find(".DATA_DISK_SIZE :input").prop("disabled", false);
					trObj.find(".DATA_DISK_UNIT select").prop("disabled", true);
				}
			}
			${popupid}_vue.form_data.OS_DISK_SIZE = ${popupid}_vue.form_data.DISK_SIZE - total;
			 
		
		}
		

		$("#${popupid}_CPU_CNT_input, #${popupid}_CPU_CNT_sel").change(function(){
				${popupid}_calcDiskFee();
			}
		);
		$("#${popupid}_RAM_SIZE_input, #${popupid}_RAM_SIZE_sel").change(function()	{
				${popupid}_calcDiskFee();
			}
		);
		$("#${popupid}_GPU_SIZE_input, #${popupid}_GPU_SIZE_sel").change(function()	{
			${popupid}_calcDiskFee();
		}
	);



		$("#${popupid}_OS_ID").on("change",function(){
			var data =$("#${popupid}_OS_ID option:selected").prop("data");
			//$("#${popupid}_RAM_SIZE").val(data.DISK_SIZE);
			$("#${popupid}_DISK_SIZE").prop("disabled", true);
			$("#${popupid}_DISK_UNIT").prop("disabled", true);
			${popupid}_vue.form_data.DC_ID = data.DC_ID;
			${popupid}_vue.form_data.DISK_TYPE_ID = "3";
			${popupid}_vue.form_data.SPEC_ID = "0";
			${popupid}_vue.form_data.CPU_CNT = data.CPU_CNT;
			${popupid}_vue.form_data.RAM_SIZE = data.RAM_SIZE;
			${popupid}_vue.form_data.OS_DISK_SIZE=data.DISK_SIZE;
			${popupid}_vue.form_data.DISK_SIZE=data.DISK_SIZE;
			${popupid}_calcDiskFee();

		})
		
		setTimeout(function(){ 
			if($(".DATA_DISK_TYPE:first option").length==1){
				$(".DATA_DISK_TYPE").hide();
				
			}
			$(".DATA_DISK_SIZE.select").hide(); 
			$(".DATA_DISK_SIZE.input").show(); 
			${popupid}_calDiskSize();
		},500);

	}
	</script>