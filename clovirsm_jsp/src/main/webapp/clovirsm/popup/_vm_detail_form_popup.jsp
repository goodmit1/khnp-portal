<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<!--  VM 정보 수정 -->
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);
	String direct = request.getParameter("direct");
	request.setAttribute("direct", direct);
	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<style>
	#${popupid} .modal-dialog {
		width: 1200px;
	}
	/* #${popupid}_NAS_TABLE > div:nth-child(2) > i {
		top: 50%;
    	transform: translateY(-50%);
	} */
</style>
<fm-modal id="${popupid}" title="<spring:message code="label_vm_info" text=""/>" cmd="header-title">
<link href="/res/css/editor.css" type="text/css" rel="stylesheet"/>
<script src="/res/js/editor.js"></script>
	<span slot="footer">
		 
		<input v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'save')" type="button" class="btn saveBtn contentsBtn tabBtnImg top5 save" value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_save()" />
		<span v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'owner')"><fm-popup-button   popupid="vm_owner_change_popup" popup="/popup/owner_change_form_popup.jsp?svc=vm&key=VM_ID&callback=onAfterVMOwnerChg" cmd="update contentsBtn tabBtnImg top5 sync" param="vmReq.getData()"><spring:message code="btn_change_owner" text="담당자변경"/></fm-popup-button></span>
	</span>
	<div class="form-panel detail-panel">
		<form id="${popupid}_form" action="none">
			<div class="panel-body">
				 
				<div class="col col-sm-6">
					<fm-output id="${popupid}_VM_NM" name="VM_NM" title="<spring:message code="NC_VM_VM_NM" text="VM명"/>"></fm-output>
				</div>
				<div class="col col-sm-6" v-if="${action == 'appr' }">
					<fm-output id="${popupid}_GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명"/>"></fm-output>
				</div>
				<div class="col col-sm-6" v-else-if="${action != 'insert' && action != 'update'}">
					<fm-output id="${popupid}_GUEST_NM" name="GUEST_NM" title="<spring:message code="NC_VM_GUEST_NM" text="OS명"/>"></fm-output>
				</div>
				
				<!--
				<div class="col col-sm-6" v-if="${action != 'insert' && action != 'update'}">
					<fm-output id="${popupid}_RUN_CD_NM" name="RUN_CD_NM" title="<spring:message code="NC_VM_RUN_CD_NM" text="서버 상태" />" :value="getServerStatus(form_data)"></fm-output>
				</div>
				 -->
				 <div class="col col-sm-12" v-if="${action == 'insert' || action == 'update'}">
							<jsp:include page="/clovirsm/popup/_spec_form.jsp"></jsp:include>
						</div>
			  	<div class="col col-sm-6" >
				 	<fm-output id="${popupid}_SPEC_INFO" name="SPEC_INFO" title="<spring:message code="NC_VM_SPEC_NM" text="사양"/>" 
				 	:value="getSpecInfo(form_data,form_data.CPU_CNT, form_data.RAM_SIZE, form_data.OS_DISK_SIZE,'G')" ></fm-output>
				</div>
				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.listLeafCategory" id="${popupid}_CATEGORY"
						keyfield="CATEGORY_ID" titlefield="CATEGORY_NMS"
						:disabled="${isAppr != 'N' || action == null || action =='appr' }"
						name="CATEGORY" title="<spring:message code="TASK" text="부품"/>">
					</fm-select>
				</div>

				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-select url="/api/code_list?grp=PURPOSE" id="${popupid}_PURPOSE" emptystr=" " name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>" onchange="${popupid}_setPurposeNm();" :disabled="  ${ isAppr != 'N' || action == null || action =='appr' }"></fm-select>
				</div>

				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-select url="/api/code_list?grp=P_KUBUN" id="${popupid}_P_KUBUN" emptystr=" "  name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>" onchange="${popupid}_setPKubunNm();" :disabled=" ${isAppr != 'N' || action == null || action =='appr' }"></fm-select>
				</div>
				<div class="col col-sm-6" v-if="${action != 'insert' && action != 'update'}">
					<fm-output id="${popupid}_INS_TMS" name="INS_TMS" title="<spring:message code="NC_VM_INS_TMS" text="등록일시" />" :value="expiryDate(form_data.INS_TMS, form_data.USE_MM)"></fm-output>
				</div>
				<div class="col col-sm-6" v-if="${action != 'update'}">
					<fm-output id="${popupid}_INS_NM" name="INS_ID_NM" title="<spring:message code="NC_VM_INS_ID_NM" text="담당자" />"></fm-output>
				</div>
				<div class="col col-sm-12"><div >
							<textarea id="${popupid}_CMT" name="${popupid}_CMT" style="width:calc(100% - 150px);"></textarea>
						</div>
				</div>		
			</div>
		</form>
	</div>
	<script>
		var param;
		var editor = false;
		var diskUnitOptions = {
			G: 'GB',
			T: 'TB'
		}

		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {SPEC_NM:'', CATEGORY:'', GUEST_NM:'', USE_MM:0, CMT:'', INS_TMS:'', INS_ID_NM:'', P_KUBUN:'', P_KUBUN_NM:'', PURPOSE:'', VM_NM:'', VM_ID:'', DC_ID:'', DC_NM:'', OLD_SPEC_INFO:''};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});

		function onAfterVMOwnerChg()
		{
			if(${popupid}_callback != ''){
				eval(${popupid}_callback + '(${popupid}_vue.form_data);');
			}
			$('#${popupid}').modal('hide');
		}
		 
		var ${popupid}_callback = '${callback}';
		function ${popupid}_click(vue, param){
			 
			this.param = param;
			var ac = "${action}"
			 
			 
			
			 
			if(param.DC_ID) {
				${popupid}_param = $.extend(${popupid}_param, param);
				if(param.CATEGORY){
					$("#${popupid}_CATEGORY").prop("disabled", true)
				}
				 		
				if(param.P_KUBUN){
					$("#${popupid}_P_KUBUN").prop("disabled", true)
				}
				if(param.PURPOSE){
					$("#${popupid}_PURPOSE").prop("disabled", true)
				}
				 
				return true;
			} else {
				
				${popupid}_param = null;
				alert(msg_select_first);
				return false;
			}
		}
	 
		function ${popupid}_onAfterOpen(){
			var headerTitle = '<spring:message code="label_vm_info" text=""/>';
			var status = '';
			if(${popupid}_vue.form_data.CUD_CD) status += ${popupid}_vue.form_data.CUD_CD_NM;
			if(${popupid}_vue.form_data.APPR_STATUS_CD) status += ' ' + ${popupid}_vue.form_data.APPR_STATUS_CD_NM;
			if(status != '') status = '(' + status + ')';
			headerTitle += status;
			$('#${popupid}_title').text(headerTitle);
		}

	 

		 

		 

		 

		function ${popupid}_save(){
			${popupid}_beforeSaveReq();
			${popupid}_vue.form_data.CMT = $("#${popupid}_CMT").Editor("getText" );
			var action = 'insertReq';
			if(${action == 'insert'}) action = 'insertReq';
			else if(${action == 'save'}) action = 'save';
			var param = {};
			param.VM_ID= ${popupid}_vue.form_data.VM_ID;
			param.CMT = ${popupid}_vue.form_data.CMT;
			param.CATEGORY = ${popupid}_vue.form_data.CATEGORY;
			param.P_KUBUN = ${popupid}_vue.form_data.P_KUBUN;
			param.PURPOSE = ${popupid}_vue.form_data.PURPOSE;
			post('/api/vm/' + action, param,  function(data){
			 
				alert(msg_complete);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '(${popupid}_vue.form_data);');
				}
				$('#${popupid}').modal('hide');
			});
		}

	 

		 

		 

		function ${popupid}_setPurposeNm(){
			var purposeNm = $('#${popupid}_PURPOSE option:selected').text();
			${popupid}_vue.form_data.PURPOSE_NM = purposeNm;
		}

		function ${popupid}_setPKubunNm(){
			var pkubunNm = $('#${popupid}_P_KUBUN option:selected').text();
			${popupid}_vue.form_data.P_KUBUN_NM = pkubunNm;
		}

		 

		function ${popupid}_beforeSaveReq(){
			 
		}

		 
		$(function(){
			<c:if test="${action != 'update' && !(isAppr != 'N' || action == null)}">
				$("#${popupid}_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
			</c:if>
		})
	</script>
</fm-modal>