<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
%>
<fm-modal id="noticePop" class="fullGrid" title="<spring:message code="title_noti" text=""/>" cmd="header-title">
	<form id="noticePop_form" action="none">
		<div class="panel-body">
			<div class="col col-sm-12">
				<label><spring:message code="label_title"/></label>
				<div class="inputbox">{{form_data.TITLE}}</div>
			</div>
			<div class="col col-sm-6">
				<label><spring:message code="label_post_term"/></label>
				<div class="inputbox">{{form_data.START_DATE}}</div>
			</div>
			<div class="col col-sm-6">
				<label><spring:message code="label_read_cnt"/></label>
				<div class="inputbox">{{form_data.HITS}}</div>
			</div>
			<div class="col col-sm-12">
				<div class="inputbox">{{form_data.CONTENTS}}</div>
			</div>
		</div>
	<style>
	#noticePop_form div.inputbox {
		display: inline-block !important;
		text-align: right !important;
		background: #f2f2f2 !important;
	}
	</style>
	</form>

	<script>
		var noticePop_button = newPopup("noticePop_button", "noticePop");
		var noticePop_param = { };
		var noticePop_vue = new Vue({
			el: '#noticePop',
			data: {
				form_data: {
					TITLE: "",
					START_DATE: "",
					HITS: 0,
					CONTENTS: ""
				}
			}
		});
		var noticePop_req = new Req();
		function noticePop_click(vue, param){
			if(param && param.ID){
				noticePop_param = $.extend({}, param);
				noticePop_req.getInfo("/api/notice/info?ID=" + param.ID, function(data){
					noticePop_vue.form_data.TITLE      = data.TITLE     ;
					noticePop_vue.form_data.START_DATE = data.START_DATE;
					noticePop_vue.form_data.HITS       = data.HITS      ;
					noticePop_vue.form_data.CONTENTS   = data.CONTENTS  ;
					 
				})
				return true;
			} else {
				noticePop_param = {};
				return false;
			}
		}

	</script>
</fm-modal>