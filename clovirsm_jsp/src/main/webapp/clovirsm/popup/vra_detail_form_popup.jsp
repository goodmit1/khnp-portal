<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String action = request.getParameter("action");
	request.setAttribute("action", action);
	request.setAttribute("callback", "workflowSearch");
%>
<jsp:include page="/clovirsm/resources/vra/newVra/input_popup.jsp?action=${action}"></jsp:include>
<script>
	var vraDetailReq = new Req();
	function openVra4Work(CATALOG_ID, INS_DT){
		openVraPopup(vraDetailReq, null, CATALOG_ID, INS_DT);
	}
</script>
