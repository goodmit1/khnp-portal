<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String url = request.getParameter("url");
	request.setAttribute("url", url);
%>
<style>

#${popupid} .modal-dialog{
	width : 1000px;
}
#${popupid} .modal-dialog .form-panel.detail-panel{
	height : 500px;
}

.ace_editor, .ace_editor *{
    font-family:monospace !important;
}

</style>

<fm-modal id="${popupid}" title="VRA_JSON DATA" cmd="header-title">
	<span slot="footer">
		<button type="button" class="popupBtn finish top5" onclick="jsonSave()">저장</button>
	</span>
	<div class="form-panel detail-panel">
			<div id="editor" name="DATA_JSON" style="width:100%; height:95%;"></div>
			<div style="padding:10px;">※ svc, p_kubun, feb 변경시 포탈과 데이터 불일치가 발생할 수 있습니다.</div>
	</div>
	<script>
		var editor;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			${popupid}_vue.form_data = ${popupid}_param;
			console.log(${popupid}_vue.form_data);
			editorFunction();
			return true;
		}
		
		function editorFunction(){
			 editor = ace.edit("editor");
			    editor.setTheme("ace/theme/monokai");
			    editor.getSession().setMode("ace/mode/sh");
				editor.getSession().setUseSoftTabs(true);
				
			    editor.setOptions({
			        "fontSize" : "12px",
			        "maxLines": "20px",
			       	"line-height": "2!important"
			    });
			    jsonImport();
		}
		function jsonSave(){
			var data = new Object();
			data.CATALOGREQ_ID = ${popupid}_vue.form_data[0].CATALOGREQ_ID;
			data.APPR_STATUS_CD = ${popupid}_vue.form_data[0].APPR_STATUS_CD;
			data.DATA_JSON = editor.getValue();
			
			post("/api/vra_catalog/updateVraJson", data , function(){
				alert("<spring:message code="saved" text="저장되었습니다."/>");
				$('#${popupid}').modal('hide');
			})
		}
		function jsonImport(){
			post("/api/vra_catalog/info/selectByPrimaryKey_VRA_DATA_JSON/", ${popupid}_vue.form_data[0], function(data){
			    editor.setValue(data.DATA_JSON);
			    ${popupid}_vue.form_data[0].CATALOGREQ_ID = data.CATALOGREQ_ID;
			});
		}
	</script>
	<style>

	</style>
</fm-modal>