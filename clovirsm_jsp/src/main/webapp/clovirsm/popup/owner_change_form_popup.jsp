<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String svc = request.getParameter("svc");
	request.setAttribute("svc", svc);

	String key = request.getParameter("key");
	request.setAttribute("key", key);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);
%>
<fm-modal id="${popupid}" title="<spring:message code="btn_change_owner" text="담당자변경"/>" cmd="header-title">
	<span slot="footer">
		<input type="button" class="btn" value="<spring:message code="btn_change_owner" text="담당자변경"/>" onclick="${popupid}_owner_change()" />
	</span>
	<form id="${popupid}_form" action="none">
		<fm-ibutton id="${popupid}_OWNER_SEARCH_USER_NAME" name="OWNER_NAME">
			<fm-popup-button popupid="${popupid}_owner_change_user_search" popup="/popup/user_search_form_popup.jsp" cmd="update" param="$('#${popupid}_OWNER_SEARCH_USER_NAME').val()" callback="${popupid}_select_user"><spring:message code="btn_change_owner" text="담당자변경"/></fm-popup-button>
		</fm-ibutton>
		<input type="hidden" name="USER_ID" />
		<input type="hidden" name="LOGIN_ID" />
		<input type="hidden" name="TEAM_CD" />
		<input type="hidden" name="TEAM_NM" />
		<input type="hidden" name="NEW_USER" />
		<input type="text" name="DUMMY" style="display:none"/>
	</form>

	<script>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		var ${popupid}_callback = '${callback}';
		function ${popupid}_click(vue, param){
			if(param && param.${key}){
				${popupid}_param = $.extend({}, param);
				${popupid}_vue.form_data = ${popupid}_param;
				return true;
			} else {
				${popupid}_param = {};
				alert(msg_select_first);
				return false;
			}
		}

		function ${popupid}_owner_change(){
			post('/api/${svc}/chg_owner', ${popupid}_param,  function(data){
				alert(msg_complete);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '(${popupid}_param);');
					
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_select_user(args, callback){
			$('#${popupid}_OWNER_SEARCH_USER_NAME').val(args.USER_NAME);
			args.INS_NM = args.USER_NAME;
			args.NEW_USER = args.USER_ID;
			${popupid}_param = $.extend(${popupid}_param, args);
			${popupid}_vue.form_data = ${popupid}_param;
			if(callback) callback();
		}

	</script>
</fm-modal>