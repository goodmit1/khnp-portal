<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String url = request.getParameter("url");
	request.setAttribute("url", url);
%>
<style>

#shell_popup .modal-dialog{
	width : 1000px;
}
#shell_popup .modal-dialog .form-panel.detail-panel{
	height : 500px;
}

.ace_editor, .ace_editor *{
    font-family:monospace !important;
}

</style>

<fm-modal id="${popupid}" title="" cmd="header-title">
	<span slot="footer">
		<div class="col col-sm-12" id="commitMessage">
			<p style="text-align: left;">Commit Message</p>
			<fm-textarea id="commit_message" name="commit_message"  sty="height: 100px; width:100%; padding:10px; margin-bottom: 10px;" required></fm-textarea>
		</div>
		<button type="button" class="popupBtn finish top5" onclick="codeSave()">저장</button>
	</span>
	<div class="form-panel detail-panel">
			<div id="editor" style="width:100%; height:100%;"></div>
	</div>
	<script>
		var editor;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		$(document).ready(function(){
		})

		
		
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			${popupid}_vue.form_data = ${popupid}_param;
			$('#shell_popup_title').html(${popupid}_vue.form_data.title);
			 
			
			
			editorFunction();
			    
			return true;
		}
		
		function editorFunction(){
			 editor = ace.edit("editor");
			    editor.setTheme("ace/theme/monokai");
			    editor.getSession().setMode("ace/mode/sh");
			     
			    if(${popupid}_vue.form_data.title.indexOf("Linux") > -1){
			    	editor.getSession().setMode("ace/mode/sh");
			    } else if(${popupid}_vue.form_data.title.indexOf("Windows") > -1){
			    	editor.getSession().setMode("ace/mode/powershell");
			    }
				editor.getSession().setUseSoftTabs(true);
				
			    editor.setOptions({
			        "fontSize" : "12px",
			        "maxLines": "20px",
			       	"line-height": "2!important"
			    });
			    codeImport();
		
		}
		function codeSave(){
			if(validate("${popupid}")){
				var param = new Object();
				var url = ${popupid}_param.url;
				 
				param.type = ${popupid}_param.type;
				param.name = ${popupid}_param.name;
				param.purpose = ${popupid}_param.purpose;
				param.content = editor.getValue();
				param.commitMsg = $("#commit_message").val();
				showLoading();
				$.ajax({
					type: 'POST',
					data: param,
					dataType: 'json',
					url: '/api/'+url+'/codeGitSave',
					success: function(data) {
						 
						if(data == "1"){
							alert("<spring:message code="saved" text="저장되었습니다."/>");
							$('#${popupid}').modal('hide');
						}else{
							alert("<spring:message code="no_saved" text="저장에 실패했습니다."/>");
						}
						hideLoading();
					}
				});
				
			}
		}
		function codeImport(){
			var param = new Object();
			param.type = ${popupid}_param.type;
			param.name = ${popupid}_param.name; 
			param.purpose = ${popupid}_param.purpose;
			$.ajax({
				type: 'POST',
				data: param,
				url: '/api/sw/codeLocalImport',
				success: function(data) {
					if(data != null)
				    	editor.setValue(data);	
					else{
						$.ajax({
							type: 'POST',
							data: param,
							url: '/api/sw/codeGitImport',
							success: function(data) {
							    editor.setValue(data);
							}
						});
					}
				}
			});
		}
	</script>
	<style>

	</style>
</fm-modal>