<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	 
	request.setAttribute("startStr", "${");  
%>
 
<style>
#mynetwork {
	width: 100%;
	height: 500px;
	top:100;
	left : 100;
	border: 1px solid lightgray;
	background-color: #FFFFFF;
}
</style>
<script type="text/javascript" src="/res/js/vis-network.min.js"></script>
<link href="/res/css/vis-network.min.css" rel="stylesheet" type="text/css">
<fm-modal id="${popupid}" title="Topology" cmd="header-title">
  
<span slot="footer">
		 
</span>
<div class="form-panel detail-panel">
	<div id="mynetwork" >
	</div>
</div>
</fm-modal>
	
 
<script>
var network = null;
var conf = {
	Network: {
		size: 30,
		level:0,
		position: "bottom"
	},
	LoadBalancer : {
		size: 10,
		level:0,
		position: "bottom"
	},
	Machine: {
		size: 10,
		level:1,
		position: "middle"
	} 
}
function getType(str) {
	return str.substr(str.lastIndexOf(".") + 1);
}

function getNetwork(list, idx) {
	var r = list[idx];
	var net = null;
	if(r.properties.networks) {
		net = r.properties.networks;
	}else if(r.properties.network){
		net = [{network: r.properties.network}];
	}
	if(!net) {
		return null;
	}
	var result = [];
	for(var n in net) {
		var nn = net[n].network;
		for(var i in list) {
			if(i == idx) {
				continue;
			}
			if(nn.indexOf("." + i + ".") >= 0 || nn.indexOf(i + ".") >= 0) {
				result.push(i);
			}
		}
	}
	return result;
}

function getDisk(list, idx) {
	var r = list[idx];
	var disks = null;
	disks = r.properties.attachedDisks;
	if(!disks) {
		return null;
	}
	var result = [];
	for(var d in disks) {
		var disk = disks[d].source
		for(var i in list) {
			if(i == idx) {
				continue;
			}
			if(disk.indexOf("." + i + ".") >= 0 || disk.indexOf(i + ".") >= 0) {
				result.push(i);
			}
		}
	}
	return result;
}

function getInstances(list, idx) {
	var r = list[idx];
	var ins = "";
	ins = r.properties.instances;
	if(!ins) {
		return null;
	}
	for(var i in list) {
		if(i == idx) {
			continue;
		}
		if(ins.indexOf("." + i + ".") >= 0 || ins.indexOf(i + ".") >= 0) {
			return i;
		}
	}
}
var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
var ${popupid}_param = {};
var ${popupid}_vue = new Vue({
	el: '#${popupid}',
	data: {
		form_data: ${popupid}_param
	}
});
function ${popupid}_click(vue, param){
	 
	${popupid}_param = param;
	setTimeout(function(){
		${popupid}_drawTopology( )
		}, 1000);
	
	return true;
}
 
function ${popupid}_getConf(type){
	var result = conf[type];
	if(!result && result == null){
		result = {
				size: 5,
				position: "top",
				level:2
			} 
	}
	return result;
}
function getRelName(data){
	var arr = data.split(".");
	if(arr.length>2){
		var pos = arr[1].indexOf("[");
		if(pos>0){
			return arr[1].substring(0, pos);
		}
		else{
			return arr[1];
		}
	}
	else{
		var pos = arr[0].indexOf("resource[")
		if(pos>=0){
			var t = arr[0].substring(pos+9,pos+10)
			var pos2 = arr[0].indexOf("]", pos+11);
			return arr[0].substring(pos+11,pos2-2);
			 
		}
		else{
			return arr[0];
		}
	}
}
function getRel(edges, type, from, prop){
	
	var str = "${startStr}";
	var i=0;
	while(i>=0){
		var i2= prop.indexOf(str, i);
		if(i2>0){
			var i3 = prop.indexOf("}" , i2+str.length+1);
			 
			var edge = {};
			edge.from = from;
			edge.to = getRelName(prop.substring(i2+str.length, i3));
			/*if(type == "LoadBalancer"){
				edge.arrows = {
					to: {
						enabled: true,
						type: "inv_curve"
					}
				}
			}*/
			edges.push(edge);
			i=i3;
		}
		else{
			return;	
		}
		
	}
}
// Called when the Visualization API is loaded.
function ${popupid}_drawTopology( ) {
 
	
 	$('#mynetwork').html('');
	 

	
	var div= document.getElementById('html'); // need real DOM Node, not jQuery wrapper
	var hasVerticalScrollbar= div.scrollHeight>div.clientHeight;
	var hasHorizontalScrollbar= div.scrollWidth>div.clientWidth;
	
	 
	var nodes = [];
	var edges = [];
	var res = JSON.parse(${popupid}_param.FORM_INFO).resources;
	 
	// create people.
	// value corresponds with the age of the person
	for(var i in res) {
		var row = res[i];
		var node = {};
		var edge = {};
		node.id = i;
		node.label = i;
		node.shape = 'image';
		var type = getType(row.type);
		node.value =  ${popupid}_getConf(type).size;
		node.level =   ${popupid}_getConf(type).level;
		node.image = '/res/img/topology/' + type  + '.png';
		getRel(edges,type, i, JSON.stringify( row.properties));
		 
		nodes.push(node);
	}
	// create connections between people
	// value corresponds with the amount of contact between two people
	// create a network
	var container = document.getElementById('mynetwork');
	var data = {
		nodes: nodes,
		edges: edges
	};
	var options = {
		edges: {
                smooth: {
                    type: 'cubicBezier',
                    forceDirection:   'horizontal',
                    roundness: 0.4
                }
            },
		nodes: {
			borderWidth: 0,
			color: {
				border: '#406897',
				background: 'aliceblue'
			},
			scaling: {
				customScalingFunction: function (min,max,total,value) {
					return value/total;
				}
			},
			font: {color: '#333333'},
			shapeProperties: {
				useBorderWithImage: true
			}
		},
		//layout: {randomSeed: 1},
		layout: {
                    hierarchical: {
                        direction: 'DU'
                    }
                }
	}
	network = new vis.Network(container, data, options);
}
</script>
</body>
</html>