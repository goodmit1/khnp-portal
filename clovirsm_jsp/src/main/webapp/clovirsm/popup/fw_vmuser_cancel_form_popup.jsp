<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<style>
#input{
	height:200px;
	width:100%;
}
</style>
<fm-modal id="fw_vmuser_cancel_form_popup" title="<spring:message code="task_message_deny" text="작업메세지/작업거부" />" cmd="header-title">
	<span slot="footer">
		<template>
			<input type="button" class="btn saveBtn" value="<spring:message code="btn_save" text="" />" onclick="get_vm()" style="float:right"/>
		</template>
	</span>
	<form id="fw_vmuser_cancel_form_popup_form" action="none">
		<fm-textarea id="input" name="input" tooltip="<spring:message code="input" text="사유" />"></fm-input>
	</form>

	<script>
		var popup_get_vm_form = newPopup("fw_vmuser_cancel_form_popup_button", "fw_vmuser_cancel_form_popup");
		var fw_vmuser_cancel_form_popup_vue = new Vue({
			el: '#fw_vmuser_cancel_form_popup',
			data: {
				form_data: {
					input: ''
				}
			}
		});

		function fw_vmuser_cancel_form_popup_click(vue, param){
			 
			 
			if(!param[0]){
				alert(msg_select_first)
				return false;
			}
			if(mainTable.getSelectedRows()[0]){
				fw_vmuser_cancel_form_popup_vue.callback = param;
				$("#input").val("");
				return true;
			}else{
				alert(msg_select_first)
			}
			/* fw_vmuser_cancel_form_popup_vue.callback = param;
			$("#input").val("");
			return true; */
		}

		function get_vm(){
			var callback = fw_vmuser_cancel_form_popup_vue.callback;
			if(callback != null) eval( callback + '($("#input").val(), function(){ $(\'#fw_vmuser_cancel_form_popup\').modal(\'hide\'); });');
		}

	</script>
</fm-modal>