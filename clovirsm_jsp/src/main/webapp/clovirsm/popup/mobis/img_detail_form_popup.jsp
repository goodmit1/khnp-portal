<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String direct = request.getParameter("direct");
	request.setAttribute("direct", direct);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);
%>
<fm-modal id="${popupid}" title="<spring:message code="label_img_req" text="템플릿 정보" />" cmd="header-title">
	<span slot="footer">
		<span v-if="${isAppr == 'N'} && (  !form_data.CUD_CD && ${action == 'save'}  )" ><fm-popup-button popupid="img_owner_change_popup" popup="/popup/owner_change_form_popup.jsp?svc=image&key=IMG_ID" cmd="update" param="imgReq.getData()"><spring:message code="btn_change_owner" text="담당자변경"/></fm-popup-button></span>
		<input v-if="${isAppr == 'N'} && ${direct == null && action == 'insert'} && isRequestable(form_data)" type="button" class="btn saveBtn" value="<spring:message code="btn_work" text="작업함에 담기"/>" onclick="${popupid}_to_work()" />
		<input v-if="${isAppr == 'N'} && ${direct == null && action == 'insert'} && isRequestable(form_data)" type="button" class="btn exeBtn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="${popupid}_request()" />
		<input v-if="${isAppr == 'N'} && (${action == 'insert'} && form_data.CUD_CD == 'C') || ${action == 'save'} || ${direct == 'Y'}" type="button" class="btn saveBtn" value="<spring:message code="btn_save" text="저장" />" onclick="${popupid}_save()" />

	</span>
	<div class="form-panel detail-panel"  >
		<form id="${popupid}_form" action="none">
			<div class="panel-body">
				<div class="col col-sm-6">
					<fm-ibutton id="${popupid}_FROM_NM" name="FROM_NM" title="<spring:message code="NC_VM_FROM_NM" text="원본 서버명"/>" :disabled="${isAppr != 'N' || action != 'insert'}">
						<fm-popup-button popupid="${popupid}_vm_search" popup="../../popup/vm_search_form_popup.jsp?isActiveOnly=Y&isOnlyOwn=Y" cmd="update" param="${popupid}_getVMSearchParam()" callback="select_${popupid}_vm" :disabled="${isAppr != 'N' || action != 'insert'}"><spring:message code="btn_vm_search" text="서버검색"/></fm-popup-button>
					</fm-ibutton>
				</div>
				<div class="col col-sm-6">
					<fm-input id="${popupid}_IMG_NM" name="IMG_NM" title="<spring:message code="NC_IMG_IMG_NM" text="템플릿명"/>(<spring:message code="label_auto_gen"/>)" v-if="${IS_AUTO_IMAGE_NAME}" disabled="disabled"></fm-input>
					<fm-input id="${popupid}_IMG_NM" name="IMG_NM" title="<spring:message code="NC_IMG_IMG_NM" text="템플릿명"/>" v-if="${!IS_AUTO_IMAGE_NAME}" :disabled="${isAppr != 'N' || action == null}"></fm-input>
				</div>
				<div class="col col-sm-6">
					<fm-output id="${popupid}_DISK_SIZE" name="DISK_SIZE" title="<spring:message code="NC_IMG_DISK_SIZE" text="디스크"/>" :value="getTemplateDiskSize(form_data)"></fm-output>

				</div>
				<div class="col col-sm-6">
					<fm-output id="${popupid}_DD_FEE" name="DD_FEE" :value="formatNumber(form_data.DD_FEE)" title="<spring:message code="label_DD_FEE" text="비용"/>"  ></fm-output>
				</div>
				<c:if test="${sessionScope.STATE }">
					<div class="col col-sm-6">
						<fm-select url="/api/site/querykey/selectPartCategory/" id="${popupid}_CATEGORY"
							keyfield="CATEGORY_ID" titlefield="CATEGORY_NM"
							:disabled="${isAppr != 'N'}"
							name="CATEGORY" title="<spring:message code="PART" text="부품"/>">
						</fm-select>
					</div>
					<div class="col col-sm-6">
						<fm-select url="/api/code_list?grp=P_KUBUN" id="${popupid}_P_KUBUN" emptystr=" "   name="P_KUBUN" title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>" :disabled="${isAppr != 'N' || action == null}"></fm-select>
					</div>
					<div class="col col-sm-6">
						<fm-select url="/api/code_list?grp=PURPOSE" id="${popupid}_PURPOSE" emptystr=" " name="PURPOSE" title="<spring:message code="NC_VM_PURPOSE" text="사용용도"/>" :disabled="${isAppr != 'N' || action == null}"></fm-select>
					</div>
				</c:if>
				<div class="col col-sm-6">
					<fm-spin id="${popupid}_USE_MM" name="USE_MM" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" min="6" :disabled="${isAppr != 'N' || action == null}"></fm-spin>
				</div>
				<div class="col col-sm-12">
					<c:if test="${isAppr == 'N' && action != null}">
						<div>
							<textarea id="${popupid}_CMT" name="CMT" style="width:calc(100% - 150px);"></textarea>
						</div>
					</c:if>
					<c:if test="${isAppr != 'N' || action == null}">
						<div style="height: 200px;">

							<div v-html="form_data.CMT" class="output hastitle" style="min-height: 200px;overflow-y: auto;word-break: break-word;"></div>
						</div>
					</c:if>
				</div>

			</div>
		</form>
	</div>
	<script>
		<c:if test="${isAppr == 'N' && action != null}">
			$(function(){
				setTimeout(function(){
					$("#${popupid}_CMT").Editor({status_bar:false,'togglescreen':false,'select_all':false,'print':false,'rm_format':false});
				},1000)
			});
		</c:if>
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		var ${popupid}_callback = '${callback}';

		function ${popupid}_click(vue, param){
			 
			if(param && (param.VM_ID || param.IMG_ID)){
				${popupid}_param = $.extend({}, param);
				${popupid}_vue.form_data = ${popupid}_param;
				if(param.VM_ID) {
					${popupid}_vue.form_data.FROM_ID = param.VM_ID;
					${popupid}_vue.form_data.FROM_NM = param.VM_NM;
				}
				else
				{
					if(!${popupid}_vue.form_data.FROM_NM)
					{
						${popupid}_vue.form_data.FROM_NM = param.VM_NM;
					}
				}
				$('#${popupid}_DC_ID').prop('disabled', true);
				$('#${popupid}_FROM_NM').parent().find(':input').prop('disabled', true);
				setTimeout(function(){$("#${popupid}_CMT").Editor("setText", param.CMT ? param.CMT:"")},1000);
			} else {
				${popupid}_param = {
					DC_ID: null,
					DC_NM: null,
					FROM_ID: null,
					FROM_NM: null,
					DISK_SIZE: 0,
					DD_FEE: 0,
					PURPOSE: null,
					PURPOSE_NM: null,
					P_KUBUN: null,
					P_KUBUN_NM: null,
					USE_MM : 6,
					CMT: null
				};
				${popupid}_vue.form_data = ${popupid}_param;
				$('#${popupid}_DC_ID').prop('disabled', false);
				$('#${popupid}_FROM_NM').parent().find(':input').prop('disabled', true);
			}
			getDayFee( ${popupid}_vue.form_data.DC_ID, 0,0,'', ${popupid}_vue.form_data.DISK_SIZE, ${popupid}_vue.form_data.DISK_UNIT, '', '${popupid}_DD_FEE')
			return true;
		}

		function ${popupid}_onAfterOpen(){
			var headerTitle = '<spring:message code="label_img_info" text="템플릿 정보"/>';
			var status = '';
			if(${popupid}_vue.form_data.CUD_CD) status += ${popupid}_vue.form_data.CUD_CD_NM;
			if(${popupid}_vue.form_data.APPR_STATUS_CD) status += ' ' + ${popupid}_vue.form_data.APPR_STATUS_CD_NM;
			if(status != '') status = '(' + status + ')';
			headerTitle += status;
			$('#${popupid}_title').text(headerTitle);
		}

		function ${popupid}_to_work(){
			${popupid}_param.CMT = $("#${popupid}_CMT").Editor("getText" );
			post('/api/image/insertReq', ${popupid}_param,  function(data){
				if(confirm(msg_complete_work_move)){
					location.href="/clovirsm/workflow/work/index.jsp";
					return;
				}
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '(${popupid}_vue.form_data);');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_request(){
			${popupid}_param.CMT = $("#${popupid}_CMT").Editor("getText" );
			${popupid}_param.DEPLOY_REQ_YN = 'Y';
			post('/api/image/insertReq', ${popupid}_param,  function(data){
				alert(msg_complete_work);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '(${popupid}_vue.form_data);');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_save(){
			${popupid}_vue.form_data.CMT = $("#${popupid}_CMT").Editor("getText" );
			var action = 'insertReq';
			if(${action == 'insert'}) action = 'insertReq';
			else if(${action == 'save'}) action = 'save';
			post('/api/image/' + action, ${popupid}_vue.form_data,  function(data){
				alert(msg_complete);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '(${popupid}_vue.form_data);');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_onchangeDC(select){
			${popupid}_clearVMInput();
			if($(select).val().trim() != ''){
				$('#${popupid}_FROM_NM').parent().find(':input').prop('disabled', false);
			} else {
				$('#${popupid}_FROM_NM').parent().find(':input').prop('disabled', true);
			}
		}

		function ${popupid}_clearVMInput(){
			${popupid}_vue.form_data.FROM_ID = null;
			${popupid}_vue.form_data.FROM_NM = null;
		}


		function ${popupid}_getVMSearchParam(){
			return {
				DC_ID: $('#${popupid}_DC_ID').val(),
				VM_NM: $('#${popupid}_FROM_NM').val()
			}
		}

		function select_${popupid}_vm(vm, callback){
			${popupid}_vue.form_data.DC_ID = vm.DC_ID;
			${popupid}_vue.form_data.DC_NM = vm.DC_NM;
			${popupid}_vue.form_data.FROM_ID = vm.VM_ID;
			${popupid}_vue.form_data.FROM_NM = vm.VM_NM;
			${popupid}_vue.form_data.PURPOSE = vm.PURPOSE;
			${popupid}_vue.form_data.P_KUBUN = vm.P_KUBUN;
			${popupid}_vue.form_data.DISK_SIZE = vm.DISK_SIZE;
			getDayFee( vm.DC_ID, 0,0,'',vm.DISK_SIZE, vm.DISK_UNIT, '', '${popupid}_DD_FEE')
 			if(callback) callback();
		}

	</script>
</fm-modal>