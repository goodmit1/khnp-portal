<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String conn = request.getParameter("conn");
	request.setAttribute("conn", conn);

	
%>
<style>

	.title {
		    color: #6d6d6d;
		    font-size: 16px;
		    font-weight: 800;
		    padding: 15px 0px 10px 0px;
		}
	#${popupid}_popup_form label{
	    background-color: white !important;
	}
	.oldInfra{
		display: inline-block; 
		width: 76px; 
		color:#a0a0a0;
	}

</style>

<fm-modal id="${popupid}" title='<spring:message code="" text="REPOSITORY 연결정보"/>' cmd="header-title">
	<span slot="footer">
		
		<button type="button" class="contentsBtn tabBtnImg save top5" onclick="saveEtc()"><spring:message code="btn_save" text="저장"/></button>
	</span>
	<form id="${popupid}_popup_form" action="none">
		<div class="title" >
			<spring:message code="" text="REPOSITORY"/>
		</div>
		<div class="form-panel detail-panel">
			<div class="panel-body" style="overflow:auto">
				<div class="col col-sm-12" >
					<div required="required" style="display: inline-block;">
						<label for="CONN_NM" class="control-label grid-title value-title">REPOSITORY</label> 
						<fm-input name="CONN_NM" id="CONN_NM" class="form-control input hastitle" input_style="width: 604px;border: none;" >
					</div>					
				</div>
				<div class="col col-sm-12" >
					<div>
						<label for="CONN_TYPE" class="control-label grid-title value-title">유형</label> 
						<fm-select id="CONN_TYPE" name="CONN_TYPE" url="/api/code_list?grp=REPOSITORY" onchange="typeChange(this)"  titlefield="KO_DD_DESC" keyfield="DD_VALUE" select_style="padding: 5px; border: 1px solid #b5b5b5;   width: 604px; height: 31px;" div_style="display:inline-block;"></fm-select>
					</div>
				</div>
				<div class="col col-sm-12 ">
					<div required="required" style="display: inline-block;">
						<label for="CONN_URL" class="control-label grid-title value-title">URL(SERVER:PORT)</label> 
						<fm-input name="CONN_URL" id="CONN_URL" class="form-control input hastitle" input_style="width: 604px;border: none;" placeholder="127.0.0.1:8080"  >
					</div>
				</div>
				<div class="col col-sm-12 hide" id="databaseKinds_DIV">
					<div required="required" style="display: inline-block;">
						<label for="databaseKinds" class="control-label grid-title value-title">DATABASE 종류</label> 
						<fm-select id="databaseKinds" name="databaseKinds" url="/api/code_list?grp=DATABASE_KIND"  titlefield="KO_DD_DESC" keyfield="DD_VALUE" select_style="padding: 5px; border: 1px solid #b5b5b5; width: 604px; height: 31px;" div_style="display:inline-block;"></fm-select>
					</div>
				</div>
				<div class="col col-sm-12 hide" id="database_DIV">
					<div required="required" style="display: inline-block;">
						<label for="database" class="control-label grid-title value-title">DATABASE</label> 
						<fm-input name="database" id="database" class="form-control input hastitle" input_style="width: 604px;border: none;" placeholder="DATABASE명">
					</div>
				</div>
				<div class="col col-sm-12 ">
					<div required="required" style="display: inline-block;">
						<label for="CONN_USERID" class="control-label grid-title value-title">사용자 ID</label> 
						<fm-input name="CONN_USERID" id="CONN_USERID" class="form-control input hastitle" input_style="width: 604px;border: none;" placeholder="root" >
					</div>
				</div>
				<div class="col col-sm-12 ">
					<div required="required" style="display: inline-block;">
						<label for="CONN_PWD" class="control-label grid-title value-title">비밀번호</label> 
						<fm-input type="password" name="CONN_PWD" id="CONN_PWD" class="form-control input hastitle" input_style="width: 604px;border: none;" >
					</div>
				</div>
			</div>
		</div>
		<div class="form-panel detail-panel">
			<div class="panel-body" style="overflow:auto">
				<div class="title" >
					<spring:message code="" text="부가설정"/>
				</div>
				<div class="col col-sm-12 " style="    margin-bottom: 10px;">
					<div  style="display: inline-block;">
						<fm-textarea id="CONN_PROP" name="CONN_PROP" title="설정(PROPERTY)" label_style="display: block; min-height: 42px; text-align: left !important;  padding-top: 6px; padding-left: 20px;" sty="height: 100px; border: 1px solid rgb(181, 181, 181); padding: 10px; width: 730px; margin-left: 20px;" ></fm-textarea>
					</div>
				</div>
				<div class="col col-sm-12 ">
					<div  style="display: inline-block;">
						<fm-textarea id="ETC" name="ETC" title="<spring:message code="NC_VRA_CATALOG_CMT" text="설명"/>" label_style="display: block; min-height: 42px; text-align: left !important;  padding-top: 6px; padding-left: 20px;" sty="height: 100px; border: 1px solid rgb(181, 181, 181); padding: 10px; width: 730px; margin-left: 20px;"></fm-textarea>
					</div>
				</div>
			</div>
		</div>
		
	</form>
	<script>
		var appr = null;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {CONN_NM : '', CONN_TYPE : '', CONN_URL : '' , databaseKinds : '', database : '' , CONN_USERID : '' , CONN_PWD : '' , CONN_PROP : '', ETC : '' };
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		
		function ${popupid}_click(vue, param, callback){
			 
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			${popupid}_vue.form_data = ${popupid}_param;
			${popupid}_vue.form_data.callback=callback;
			console.log(param)
			setTimeout(function() {
				if(param){
					if(param.hasOwnProperty("CONN_URL")){
						urlOutput(param.CONN_URL);
					}
				}
				if('${conn}' == 'DB'){
					${popupid}_param.CONN_TYPE = 'DB';
					$("#CONN_TYPE").val('DB');
					$('#CONN_TYPE').attr("disabled",true);
					typeChange($("#CONN_TYPE"));
				}
				}
			,100);
			return true;
		}
		function saveEtc(){
			if(validate('${popupid}_popup_form')){
				var param = new Object();
				if(${popupid}_vue.form_data.CONN_ID)
					param.CONN_ID =${popupid}_vue.form_data.CONN_ID;
					
				param.CONN_NM = $("#CONN_NM").val();
				param.CONN_TYPE = $("#CONN_TYPE").val();
				
				if(param.CONN_TYPE == "DB"){
					param.CONN_URL = "jdbc:"+$("#databaseKinds").val()+"://"+$("#CONN_URL").val()+"/"+$("#database").val();
				} else{
					param.CONN_URL = $("#CONN_URL").val();
				}
				
				param.CONN_USERID = $("#CONN_USERID").val();
				param.CONN_PWD = $("#CONN_PWD").val();
				param.CONN_PROP = $("#CONN_PROP").val();
				param.ETC = $("#ETC").val();
				console.log(param);
				post("/api/etcConnMng/save",param, function(data){
					var callback = ${popupid}_vue.form_data.callback;
					${popupid}_vue.form_data.CONN_ID = data.CONN_ID;
					if(callback != null){
						eval(callback+ '(${popupid}_vue.form_data);');
					}
					$("#${popupid}").modal('hide');
				})
			}
		}
		
		function typeChange(that){
			var val = $(that).val();
			console.log(val);
			if(val == 'DB'){
				$("#databaseKinds_DIV").removeClass("hide");
				$("#database_DIV").removeClass("hide");
			} else{
				$("#databaseKinds_DIV").addClass("hide");
				$("#database_DIV").addClass("hide");
			}
		}
		
		function urlOutput(data){
			if(${popupid}_vue.form_data.CONN_TYPE == 'DB'){
				var s1 = data.split(":");
				var databaseKinds = s1[1];
				var url;
				var database;
				if(databaseKinds == 'oracle'){
					url = s1[3].substring(1);
					database = st1[4];
				} else{
					url = s1[2].substring(2)+":"+ s1[3].split("/")[0];
					database =  s1[3].split("/")[1];
				}
				${popupid}_vue.form_data.CONN_URL = url;
				$("#CONN_URL").val(url);
				${popupid}_vue.form_data.database=database;
				${popupid}_vue.form_data.databaseKinds=databaseKinds;
				$("#database").val(database);
				$("#databaseKinds").val(databaseKinds);
			}else{
				$("#CONN_URL").val(data);
			}
		}
		
	</script>
	<style>

	</style>
</fm-modal>