<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
%>
<style>

#shell_popup .modal-dialog{
	width : 1000px;
}
#shell_popup .modal-dialog .form-panel.detail-panel{
	height : 500px;
}

</style>
<link rel="stylesheet" href="/res/css/jquery.minicolors.css">
<fm-modal id="${popupid}" title='<spring:message code="" text="아이콘 생성"/>' cmd="header-title">
	<span slot="footer">
		
		<button type="button" class="contentsBtn tabBtnImg save top5" onclick="makeIconPng()"><spring:message code="btn_save" text="저장"/></button>
	</span>
	<div class="form-panel detail-panel" style="height: 260px;">
		<div class="col col-sm-6">
			<label for="icon_text" style="min-width: 70px; background-color: white;" class="control-label grid-title value-title">첫번째 text</label>
			<input name="icon_text" id="icon_text_one" style="width: 290px;" onfocusout="makeIconSVG()" class="form-control input hastitle"/>
		</div>
		<div class="col col-sm-6">
			<label for="icon_text" style="min-width: 70px; background-color: white;" class="control-label grid-title value-title">두번째 text</label>
			<input name="icon_text" id="icon_text_two" style="width: 290px;" onfocusout="makeIconSVG()" class="form-control input hastitle"/>
		</div>
		<div class="col col-sm-6">
			<label for="icon_size" style="min-width: 70px; background-color: white;" class="control-label grid-title value-title">첫번쨰 Size</label>
			<input name="icon_size" id="icon_size_one" style="width: 290px;" onfocusout="makeIconSVG()" value="15" class="form-control input hastitle"/>
		</div>
		<div class="col col-sm-6">
			<label for="icon_size" style="min-width: 70px; background-color: white;" class="control-label grid-title value-title">두번째 Size</label>
			<input name="icon_size" id="icon_size_two" style="width: 290px;" onfocusout="makeIconSVG()" value="25" class="form-control input hastitle"/>
		</div>
		<div class="col col-sm-6">
			<label for="icon_bgcolor" style="min-width: 70px; background-color: white;" class="control-label grid-title value-title">바탕색</label>
			<input name="icon_bgcolor" id="icon_bgcolor" autocomplete="off" class="colorBox" onchange="makeIconSVG()" style="width: 290px;" data-control="Wheel" value="#f7bc60" class="form-control input hastitle"/>
		</div>
		<div class="col col-sm-6">
			<label for="icon_color" style="min-width: 70px; background-color: white;" class="control-label grid-title value-title">Font Color</label>
			<input name="icon_color" id="icon_color" autocomplete="off" class="colorBox" onchange="makeIconSVG()" style="width: 290px;" data-control="Wheel" value="#ffffff" class="form-control input hastitle"/>
		</div>
		<div class="col col-sm-12" style="font-family: none;">
			<label for="drawing" style="min-width: 70px; background-color: white;" class="control-label grid-title value-title">ICON 미리보기</label>
			<div id="drawing" style="height: 70px; width: 70px; margin: 15px;  border: 1px solid #ddd;"></div>
			<canvas id="canvas11" width="70" height="70" style="display:none;font-family: none;"></canvas>
		</div>
	</div>
    <script type="text/javascript" src="/res/js/jquery.minicolors.min.js"></script>
	<script>
		var appr = null;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		$(document).ready( function() {
	      $('.colorBox').each( function() {
	    	   
	          $(this).minicolors({
	            control: $(this).attr('data-control') || 'hue',
	            defaultValue: $(this).attr('data-defaultValue') || '',
	            format: $(this).attr('data-format') || 'hex',
	            keywords: $(this).attr('data-keywords') || '',
	            inline: $(this).attr('data-inline') === 'true',
	            letterCase: $(this).attr('data-letterCase') || 'lowercase',
	            opacity: $(this).attr('data-opacity'),
	            position: $(this).attr('data-position') || 'bottom',
	            swatches: $(this).attr('data-swatches') ? $(this).attr('data-swatches').split('|') : [],
	            change: function(value, opacity) {
	              if( !value ) return;
	              if( opacity ) value += ', ' + opacity;
	              if( typeof console === 'object' ) {
	                
	              }
	            },
	            theme: 'bootstrap'
	          });

	        });
		});
		function ${popupid}_click(vue, param, callback){
			 
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			${popupid}_vue.form_data = ${popupid}_param;
			${popupid}_vue.form_data.callback=callback;
			dateInit();
			
			return true;
		}
		
		function dateInit(){
			if(${popupid}_param.hasOwnProperty("CATEGORY_IDS") && ${popupid}_param.CATEGORY_IDS != null){
				CATEGORY_IDS = ${popupid}_param.CATEGORY_IDS[0];
				post('/api//popup/category/info',{"CATEGORY_ID": CATEGORY_IDS},function(data){
					$("#icon_text_two").val(String(data.CATEGORY_CODE).toUpperCase());
					makeIconSVG();
				});
				
			}			
		}
		
		
		function makeIconSVG(){
			
			var size = $("#icon_size").val(); 
			var bgcolor = $("#icon_bgcolor").val(); 
			var color = $("#icon_color").val(); 
			var font = $("#icon_font").val(); 
			var textContext = $("#icon_text").val(); 
			var textArr = $("input[name=icon_text]");
			var sizeArr = $("input[name=icon_size]");
			if(!size){
				size = '80%';
			}
			if(!font){
				font = 'Helvetica, Arial, sans-serif';
			}
			if(!color){
				color = '#000000';
			}
			if(!bgcolor){
				bgcolor = 'none';
			}
			
			$("#drawing").empty();
			var ns = 'http://www.w3.org/2000/svg'
			var div = document.getElementById('drawing') 
			var svg = document.createElementNS(ns, 'svg')
			svg.setAttributeNS(null, 'width', '70')
			svg.setAttributeNS(null, 'height', '70')
			div.appendChild(svg)
			var rect = document.createElementNS(ns, 'rect')

			rect.setAttributeNS(null, 'width', 70)
			rect.setAttributeNS(null, 'height', 70)
			rect.setAttributeNS(null, 'fill', bgcolor)

			svg.appendChild(rect);
			
			for(var i = 0 ; i < textArr.length ; i++){
				var y = 0;
				var text = document.createElementNS(ns, 'text')
				if($("input[name=icon_text]").eq(1).val() == '' || $("input[name=icon_text]").eq(0).val() == '')
					var y = 50;
				else
					var y =  ((100 / (textArr.length+1)) * (i+1));
				var ytext = y + "%";
				text.setAttributeNS(null ,"x", '50%');
				text.setAttributeNS(null ,"y", ytext);
				text.setAttributeNS(null ,"text-anchor", 'middle');
				text.setAttributeNS(null ,"alignment-baseline", 'middle');
				text.setAttributeNS(null ,"font-size", sizeArr.eq(i).val());
				text.setAttributeNS(null ,"font-weight", 'bold');
				text.setAttributeNS(null ,"fill", color);
				text.setAttributeNS(null ,"style", "font-family:"+font);
				var textNode = document.createTextNode(textArr.eq(i).val());
				text.appendChild(textNode);
				svg.appendChild(text)
			}
		}
		
		
		function makeIconPng(){
			
			if($("#drawing").children().length > 0){
				var svgString = new XMLSerializer().serializeToString(document.querySelector('svg'));
				var png = '';
				var canvas = document.getElementById("canvas11");
				var ctx = canvas.getContext("2d");
				var DOMURL = self.URL || self.webkitURL || self;
				var img = new Image();
				var svg = new Blob([svgString], {type: "image/svg+xml;charset=utf-8"});
				var url = DOMURL.createObjectURL(svg);
				img.onload = function() {
				    ctx.drawImage(img, 0, 0);
				     png = canvas.toDataURL("image/png");
				    DOMURL.revokeObjectURL(png);
				    
				    if(${popupid}_param.action == 'PRESET'){
					    var id = $(${popupid}_param.ID).attr("id");
					    $("#"+id+"_ICON_upload").attr("src", png);
					    $("#"+id+"_ICON").val(png);
					    $("#"+id+"_ICON_p").attr("src",png);
					    post('/api/vra_drv/save',{"DRV_ID": ${popupid}_param.DRV_ID , "ICON" : png, "ICON_YN": "Y"},function(data){
					    	$('#${popupid}').modal('hide');
					    	$('#'+id).modal('hide');
					    	
						});
				    } else if(${popupid}_param.action == 'CATALOG'){
					    post('/api/vra_catalog_mng/save',{"CATALOG_ID": ${popupid}_param.CATALOG_ID , "ICON" : png, "ICON_YN": "Y"},function(data){
					    	$('#${popupid}').modal('hide');
					    	search();
						});
				    } else if(${popupid}_param.action == 'OSTYPE'){
					    post('/api/ostype/save',{"OS_ID": ${popupid}_param.OS_ID , "ICON" : png, "ICON_YN": "Y"},function(data){
					    	$('#${popupid}').modal('hide');
							ostypeSearch();
						});
				    }
				};
				img.src = url;
				
				
			} else{
				alert("설정값을 한번 더 확인해주세요");
			}
		}
	</script>
	<style>

	</style>
</fm-modal>