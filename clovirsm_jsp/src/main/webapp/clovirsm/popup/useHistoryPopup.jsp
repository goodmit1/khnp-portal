<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
%>
<style>

#useHistory_popup .modal-dialog{
	width : 1000px;
}
#useHistory_popup .modal-dialog .form-panel.detail-panel{
	height : 500px;
}
#useHistory_popup .search-panel {
    box-shadow: none;
    border: none;
}
#useHistory_popup .input-spinner{
	width:100px;
}
</style>

<fm-modal id="${popupid}" title='<spring:message code="month_cost_detail" text="비용 상세"/>' cmd="header-title">
		<div class="panel-body">
			<div class="col col-sm">
					<table><tr>
						<td>조회 기간</td>
						<td>
							<fm-spin id="YYYY"
								name="YYYY" title=""   ></fm-spin>
						</td>
						<td>
							<label for="YYYY" class="control-label grid-title"><spring:message code="year" text="년"/></label>
						</td>
						<td>
							<fm-spin id="MM"
								name="MM" title="" min="1" max="12"  ></fm-spin>
						</td>
						<td>
							<label for="MM" class="control-label grid-title"><spring:message code="month" text="월"/></label>
						</td>
					</tr></table>
				</div>
	
	
				<div class="col btn_group nomargin" style="margin-top: -13px;">
				<div style="float:left; margin-top: 14px; margin-right: 9px; text-align: left; width: 170px;" id="FEE"></div>
				<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>
	
	
			</div>
		</div>
	<div class="form-panel detail-panel">
		<div id="useHistoryTable" class="ag-theme-fresh" style="height: 100%;" ></div>
	</div>
	<script>
		var appr = null;
		
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {YYYY: 0 , MM : 0};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		
		var useHistoryTable;


		$(function() {


			var useHistoryColumnDefs = [
				{headerName : "<spring:message code="NC_REQ_SVC_CD_NM" text="" />",field : "OBJ_TYPE_NM", minWidth:120},
				{headerName : "<spring:message code="NC_REQ_SVC_NM" text="" />",field : "VM_NM",minWidth:120,tooltipField:'VM_NM',},
				{headerName : "<spring:message code="NC_VM_GUEST_NM" text="" />",field : "GUEST_NM",minWidth:120,tooltipField:'GUEST_NM',},
				{headerName : "<spring:message code="NC_VM_CPU_CNT" text="" />",field : "CPU_CNT", cellStyle:{'text-align':'right'} , minWidth:100},
				{headerName : "<spring:message code="NC_VM_RAM_SIZE" text="" />(GB)",field : "RAM_SIZE", cellStyle:{'text-align':'right'}, minWidth:100},
				{headerName : "<spring:message code="NC_VM_DISK_SIZE" text="" />(GB)",field : "DISK_SIZE", cellStyle:{'text-align':'right'}, minWidth:100},
				{headerName : "<spring:message code="label_use_fee" text="" />", minWidth:120, field : "FEE", cellStyle:{'text-align':'right'},valueFormatter:function(params){
					return formatNumber(params.value);
				}},
				{headerName : "<spring:message code="USE_MM" text="사용기간" />",field : "USE_AMT", cellStyle:{'text-align':'right'}, minWidth:100},
				
				];
			var
			gridOptions = {
				hasNo : true,
				columnDefs : useHistoryColumnDefs,
				//rowModelType: 'infinite',
				//rowSelection : 'single',
				sizeColumnsToFit: true,
				cacheBlockSize: 100,
				rowData : [],
				enableSorting : true,
				enableColResize : true,
				enableServerSideSorting: false 
			}
			useHistoryTable = newGrid("useHistoryTable", gridOptions);
			 
		});
		
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			initFormData();
			search();
			${popupid}_vue.form_data = ${popupid}_param;
			return true;
		}
		
		function initFormData()
		{
			var date = new Date();
			${popupid}_param.YYYY = date.getFullYear();
			${popupid}_param.MM = date.getMonth()+1;
		}
		function search() {
			${popupid}_param.YYYYMM=${popupid}_param.YYYY + ("" + ${popupid}_param.MM).lpad(2,'0')
			list(${popupid}_param);
		}
		function list(search_param)
		{
			var FEE = 0 ;
			search_param.OBJ_TYPE='V';
			post('/api/use_history/list' ,search_param, function(data) {
				 
				for(var i = 0 ; i < data.list.length ; i++){
					FEE += data.list[i].FEE;
					 
				}
				
				$("#FEE").html("<spring:message code="COL_TOTAL_FEE" text="" /> : "+ formatNumber(FEE));
				useHistoryTable.setData(data);
				//drawVMMonitorChart(data.list) ;
			});
		}
	</script>
	<style>

	</style>
</fm-modal>