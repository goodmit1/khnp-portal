<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	String url = request.getParameter("url");
	request.setAttribute("url", url);
%>
<style>

#shell_popup .modal-dialog{
	width : 1000px;
}
#shell_popup .modal-dialog .form-panel.detail-panel{
	height : 500px;
}

</style>

<fm-modal id="${popupid}" title='<spring:message code="COL_FEE" text="금액"/>' cmd="header-title">
	<span slot="footer">
		<div class="col col-sm-12" style="text-align: left;">
			<div id="vmName" style="font-family: NanumSquareRoundEB; font-size: 26px; margin-bottom: 40px; margin-left: 10px; margin-top: 10px;"> <span></span></div>
			<label for="pay" style="min-width: 56px;" class="control-label grid-title value-title"><spring:message code="COL_FEE" text="pay"/></label>;
			<input name="pay" id="pay" style="width: 190px;" class="form-control input hastitle"/>;
		</div>
		<button type="button" class="contentsBtn tabBtnImg save top5" onclick="paySave()"><spring:message code="btn_save" text="저장"/></button>
	</span>
	<div class="form-panel detail-panel">
			<div id="editor" style="width:100%; height:100%;"></div>
	</div>
	<script>
		var editor;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		$(document).ready(function(){
		})

		
		
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			if(param != null){
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
			}
			${popupid}_vue.form_data = ${popupid}_param;
			 $("#vmName").html(param.SPEC_NM);
			 $("#vmName").append("<span style='font-size: 15px; font-family: NanumSquareRoundEB;'>( CPU : " + param.CPU_CNT + " MEM : " + param.RAM_SIZE + " )</span>");
			 $("#pay").val(param.DD_FEE);
			return true;
		}
		function paySave(){
			post('/api/vm_spec_mng/update/'+${popupid}_param.SPEC_ID+"/"+$("#pay").val()+"/",null,function(data){
				if(data){
					$('#${popupid}').modal('hide');
					search();
				} else{
					alert(msg_jsp_error);
				}
			});
		}
	</script>
	<style>

	</style>
</fm-modal>