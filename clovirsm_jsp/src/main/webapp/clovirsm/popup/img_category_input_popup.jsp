<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<fm-modal id="category_edit_popup" title="<spring:message code="label_category_edit" text="서비스 정보"/>" cmd="header-title">
	<span slot="footer">
		<input type="button" class="btn exeBtn" value="<spring:message code="label_confirm" text=""/>" onclick="category_edit_fin()" />
		<input type="button" class="btn delBtn" value="<spring:message code="label_cancel" text=""/>" onclick="category_edit_fin(true)" />
	</span>
	<div class="form-panel detail-panel">
		<form id="category_edit_popup_form" action="none">
			<div class="panel-body">
				<div class="col col-sm-12">
					<fm-input id="IMG_CATEGORY_NAME" name="NAME" title="<spring:message code="NC_FW_FW_USER_ID_NM" text=""/>"></fm-input>
				</div>
				<div class="col col-sm-12">
					<fm-input id="IMG_CATEGORY_CODE" name="CODE" title="<spring:message code="IMG_CATEGORY_CODE" text="코드"/>"></fm-input>
				</div>
			</div>
		</form>
	</div>
	<script>
		var popup_category_edit_form = newPopup("category_edit_popup_button", "category_edit_popup");
		var category_edit_popup_vue = new Vue({
			el: '#category_edit_popup',
			data: {
				form_data: {}
			}
		});
		function category_edit_popup_click(vue, param){
			 
			category_edit_popup_vue.callback = param.callback;
			category_edit_popup_vue.form_data.NAME=param.CATEGORY_NM;
			category_edit_popup_vue.form_data.CODE=param.CATEGORY_CODE;
			category_edit_popup_vue.form_data.CATEGORY=param.CATEGORY_ID;
			$("#IMG_CATEGORY_NAME").val(param.CATEGORY_NM);
			$("#IMG_CATEGORY_CODE").val(param.CATEGORY_CODE);
			return true;
		}
		function category_edit_fin(flag){
			var callback = category_edit_popup_vue.callback;
			 
			if(callback != null){
				if(flag == true){
					eval( callback + '({});');
				}else {
					eval( callback + '(category_edit_popup_vue.form_data);');
				}
			}
			$('#category_edit_popup').modal('hide');
		}
	</script>
	<style>
	</style>
</fm-modal>