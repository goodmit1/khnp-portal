<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String action = request.getParameter("action");
	request.setAttribute("action", action);

	String direct = request.getParameter("direct");
	request.setAttribute("direct", direct);

	String callback = request.getParameter("callback");
	request.setAttribute("callback", callback);

	String isAppr = request.getParameter("isAppr");
	request.setAttribute("isAppr", isAppr == null ? "N" : isAppr);


%>

<fm-modal id="${popupid}" title="<spring:message code="label_fw_req" text="" />" cmd="header-title">
	<span slot="footer">
		<!-- <span v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'owner')"><fm-popup-button popupid="vm_fw_owner_change_form_popup" popup="/popup/owner_change_form_popup.jsp?svc=fw&key=FW_ID&callback=onChangedOwner" cmd="update" param="${popupid}_vue.form_data"><spring:message code="btn_change_owner" text="담당자변경"/></fm-popup-button></span>
		 -->
		<input v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'work')" type="button" class="btn" value="<spring:message code="btn_work" text=""/>" onclick="${popupid}_to_work()" />
		<input v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'work')" type="button" class="btn" value="<spring:message code="btn_request" text="바로 요청"/>" onclick="${popupid}_request()" />
		<input v-if="isShowReqBtn('${isAppr}','${direct}' ,'${action}', form_data,'save')" type="button" class="btn saveBtn" value="<spring:message code="btn_save" text="" />" onclick="${popupid}_save()" />


	</span>
	<div class="form-panel detail-panel">
		<form id="${popupid}_form" action="none">
			<div class="panel-body">
				<div class="col col-sm-12">
					<fm-select id="${popupid}_DC_ID" name="DC_ID" url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC"
						keyfield="DC_ID" titlefield="DC_NM"
						class='insertReq'
						onchange="${popupid}_onchangeDC(this)"
						title="<spring:message code="NC_DC_DC_NM" text="데이터 센터"/>">
					</fm-select>
				</div>
				<div class="col col-sm-6">
					<fm-ibutton id="${popupid}_VM_NM" name="VM_NM" required="required" title="<spring:message code="NC_VM_VM_NM" text="서버명"/>" class='insertReq'>
						<fm-popup-button popupid="${popupid}_vm_search" popup="../../popup/vm_search_form_popup.jsp?isActiveOnly=Y" cmd="update" param="${popupid}_getVMSearchParam()" callback="select_${popupid}_vm" class='insertReq'><spring:message code="btn_vm_search" text="서버검색"/></fm-popup-button>
					</fm-ibutton>
				</div>
				<div class="col col-sm-6">
					<fm-output id="${popupid}_PUBLIC_IP" name="SERVER_IP"  title="<spring:message code="NC_FW_SERVER_IP" text="서버IP"/>"></fm-output>
				</div>

				<div class="col col-sm-6">
					<fm-ibutton id="${popupid}_USER_NAME" name="USER_NAME" required="required" title="<spring:message code="FM_USER_USER_NAME" text="사용자"/>" class='insertReq'>
						<fm-popup-button popupid="${popupid}_user_search" popup="/popup/user_search_form_popup.jsp" cmd="update" param="$('#${popupid}_USER_NAME').val()" callback="select_${popupid}_user" :disabled="${isAppr != 'N' || action != 'insert'}"><spring:message code="btn_change_user" text="사용자변경"/></fm-popup-button>
					</fm-ibutton>
				</div>
				<div class="col col-sm-6" >
					<fm-output id="${popupid}_TEAM_NM" name="TEAM_NM"  title="<spring:message code="NC_VM_TEAM_NM" text="사용자 팀"/>"></fm-output>
				</div>
				<c:choose>
					<c:when test="${isAppr eq 'Y'}"> <!--  결재시 -->
						<div class="col col-sm-12 ">
						<div class="fm-output"><label class="control-label grid-title value-title " :class="(form_data.CUD_CD !='C' &&  form_data.CIDR != form_data.CIDR_OLD) ? 'chg':''"><spring:message code="NC_VM_CIDR" text="대상IP"/></label> 
						 <div name="CIDR" class="output notInsert hastitle">
						 	<span v-if="form_data.CUD_CD !='C' && form_data.CIDR != form_data.CIDR_OLD">
						 		<span v-html="chgHighlight(form_data.CIDR,form_data.CIDR_OLD,'chg1' )" /> 
						 	 	<i   class="fa fa-arrow-circle-right"></i>
						 	</span>
						 	<span v-html="chgHighlight(form_data.CIDR_OLD,form_data.CIDR,'chg' )" /> 
							 
						 	
						 				 
					 
						 </div>
						 </div>
						 
					</div>
					
					 <div class="col col-sm-12">
						<div class="fm-output"><label class="control-label grid-title value-title"  :class="(form_data.CUD_CD !='C' && form_data.PORT != form_data.PORT_OLD) ? 'chg':''"><spring:message code="allowedPort" text="허용 Port"/></label> 
						 <div name="PORT" class="output  notInsert hastitle"> 
							 <span v-if="form_data.CUD_CD !='C' && form_data.PORT != form_data.PORT_OLD">
							 	<span v-html="chgHighlight(form_data.PORT,form_data.PORT_OLD,'chg1' )" />  
							 	 <i   class="fa fa-arrow-circle-right"></i>
							 </span>		 
								<span v-html="chgHighlight(form_data.PORT_OLD,form_data.PORT,'chg' )" />
							 
						</div>	 
						</div> 
					</div>
				</c:when>
				<c:otherwise>	<!-- 결재가 아닌 경우 -->
					<div class="col col-sm-12">
						<fm-input id="${popupid}_CIDR" name="CIDR" required="required" title="<spring:message code="NC_VM_CIDR" text="대상IP"/>" class='updateReq'></fm-input>
					</div>
					<div class="col col-sm-12">
						<fm-input id="${popupid}_PORT" name="PORT"  required="required" title="<spring:message code="allowedPort" text="허용 Port"/>" class='updateReq'></fm-input>
					</div>
				
				</c:otherwise>
				</c:choose>
			 <c:if test="${'Y'.equals(sessionScope.VM_USER_YN)}">
				<div class="col col-sm-6">
					<fm-select url="/api/code_list?grp=sys.yn" id="${popupid}_VM_USER_YN" name="VM_USER_YN" title="OS <spring:message code="NC_FW_VM_USER_YN" text="계정생성여부"/>" class='updateReq'></fm-select>
				</div>
			</c:if>	
				<div class="col col-sm-6">
					<fm-spin id="${popupid}_USE_MM"  name="USE_MM" title="<spring:message code="NC_FW_USE_MM_M" text="사용기간(개월)" />" min="6" max="120" class='save'></fm-spin>
				</div>
				<div class="col col-sm-12" style="height: 140px;">
					<fm-textarea id="${popupid}_CMT" name="CMT" style="height: 95%;" title="<spring:message code="NC_VM_CMT" text="설명"/>"  class='save'  ></fm-textarea>
				</div>
			</div>
		</form>
	</div>
	<script>
		var${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		var ${popupid}_callback = '${callback}';

		function ${popupid}_click(vue, param){
			if('${action}'=='insert'){
				param.CUD_CD='C';
			}
			if(isReqInputDisabled('${action}', form_data,'insertReq'))
			{
				$("#${popupid} .insertReq :input").prop("disabled", true)
			}
			if(isReqInputDisabled('${action}', form_data,'updateReq'))
			{
				$("#${popupid} .updateReq :input").prop("disabled", true)
			}
			if(isReqInputDisabled('${action}', form_data,'save'))
			{
				$("#${popupid} .save :input").prop("disabled", true)
			}
			if(param && param.VM_ID){
				 
				if(param.CUD_CD == undefined && param.TASK_STATUS_CD == undefined) delete param.TEAM_NM;
				${popupid}_param = $.extend({}, param);
				if(${popupid}_param.FW_ID == undefined) delete ${popupid}_param.CMT;
				${popupid}_vue.form_data =  ${popupid}_param;
				delete ${popupid}_vue.form_data.INS_DT;
				if(!param.SERVER_IP) ${popupid}_vue.form_data.SERVER_IP = param.PRIVATE_IP;
				if(param.VM_ID) {
					${popupid}_vue.form_data.FROM_ID = param.VM_ID;
					${popupid}_vue.form_data.FROM_NM = param.VM_NM;
				}
	
				${popupid}_vue.form_data.VM_USER_YN=param.VM_USER_YN;
				 
				$('#${popupid}_DC_ID').prop('disabled', true);
				 
				$('#${popupid}_VM_NM').parent().find(':input').prop('disabled', true);
				
				 
			} else {
				${popupid}_param = {
					DC_ID: null,
					DC_NM: null,
					VM_ID: null,
					VM_NM: null,
					SERVER_IP: null,
					PRIVATE_IP: null,
					CIDR : null,
					TEAM_NM: _TEAM_NM_,
					TEAM_CD: _TEAM_CD_,
					USER_NAME: _USER_NM_,
					USE_MM : 6,
					VM_USER_YN:'N',
					FW_USER_ID : _USER_ID_,

				};
				${popupid}_vue.form_data = ${popupid}_param;
				${popupid}_param.DC_ID = $('#${popupid}_DC_ID option:first').val();

				$('#${popupid}_VM_NM').parent().find(':input').prop('disabled', false);
				if(${action == 'save' || action == 'update'}) {
					alert(msg_select_first);
					return false;
				}
			}
			
			return true;
		}

		function ${popupid}_onAfterOpen(){
			/*var headerTitle = '<spring:message code="label_fw_req" text="" />';
			var status = '';
			if(${popupid}_vue.form_data.CUD_CD) status += ${popupid}_vue.form_data.CUD_CD_NM;
			
			if(${popupid}_vue.form_data.APPR_STATUS_CD) status += ' ' + ${popupid}_vue.form_data.APPR_STATUS_CD_NM;
			if(status != '') status = '(' + status + ')';
			headerTitle += status;
			$('#${popupid}_title').text(headerTitle);*/
			 
		}
		
		function ${popupid}_validate_port(port){
			//var reg = new RegExp('^[0-9]+[,]{0,1}$','g') // 숫자 , ',' 들어가 있는지  체크 
			var reg = new RegExp('^[0-9-]+$') // 숫자 , ',' 들어가 있는지  체크
			 
			var pattern = ',';
			var standard = port.indexOf(',');
			
			if(standard === -1)
			{
				return reg.test(port);
			}else{
				return validateRange(port, reg);					
			}
			
		}
		
		function ${popupid}_validate_ip(ip){
			var reg = new RegExp('^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$') //ipv4 255.255.255.255
			var standard = ip.indexOf(',');
			
			if(standard === -1)
			{
				return reg.test(ip);
			}else{
				return validateRange(ip, reg);					
			}
			
		}
		
		function validateRange(el, reg){
		    var arr;
		    var trueFalse = [];
		    var pattern = ',';

		    arr = el.replace(/[\t\s]/g,'').split(','); // 공백 문자 제거 후  ',' 기준으로 문자 잘라서 배열 반환 
		     
		    arr.forEach(function(el) {
		        var val = discriminant(el,pattern,reg)
		        trueFalse.push(val)
		    })

			//배열안의 값 비교
			return trueFalse.every(function (x) {
				return x === true;
			});
		}

		
		function discriminant(port,pattern,reg)
		{
			var start = port.indexOf(pattern);
			var end = port.length;				
			var cidr = port.substring(start,end)

			return reg.test(cidr);
		}
		
		function vmFw_input_validate(){
			var cidr = $('#${popupid}_CIDR').val()
			var port = $('#${popupid}_PORT').val()
			
			if(port != ''){// 포트 입력 있는지 확인 
				if( !${popupid}_validate_port(port) ){
					alert('<spring:message code="MSG_ALERT_FW_PORT" text="PORT번호 입력값이 잘못되었습니다." />: ' + port);
					return false;
				}						
			}
			if(!${popupid}_validate_ip(cidr)){
				alert('<spring:message code="tab_firewall" text="접근제어" /> <spring:message code="msg_ip_invalid" text="IP입력값이 잘못되었습니다." />')
				return false;
			}
			return true;
			
		}

		function ${popupid}_beforeSave()
		{
			var isok= validate("${popupid}_form");

			return isok;
		}
		function ${popupid}_to_work(){
			if(! ${popupid}_beforeSave()){
				return;
			}
			if(!vmFw_input_validate()) return;
			var action = 'insertReq';
			if(${action == 'update'}) action = 'updateReq';
			post('/api/fw/' + action, ${popupid}_param,  function(data){
				if(confirm(msg_complete_work_move)){
					location.href="/clovirsm/workflow/work/index.jsp";
					return;
				}
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '();');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_request(){
			if(! ${popupid}_beforeSave()){
				return;
			}
			if(!vmFw_input_validate()) return;
			${popupid}_param.DEPLOY_REQ_YN = 'Y';
			var action = 'insertReq';
			if(${action == 'update'}) action = 'updateReq';
			post('/api/fw/' + action, ${popupid}_param,  function(data){
				alert(msg_complete_work);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '();');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_save(){
			if(! ${popupid}_beforeSave()){
				return;
			}
			var action = 'insertReq';
			if(${action == 'insert'}) action = 'insertReq';
			else if(${action == 'update'}) action = 'updateReq';
			else if(${action == 'save'}) action = 'save';
			post('/api/fw/' + action, ${popupid}_param,  function(data){
				alert(msg_complete_work);
				if(${popupid}_callback != ''){
					eval(${popupid}_callback + '();');
				}
				$('#${popupid}').modal('hide');
			});
		}

		function ${popupid}_onchangeDC(select){
			if($('#${popupid}_DC_ID').prop("disabled")) return;
			${popupid}_clearVMInput();
			if($(select).val().trim() != ''){
				$('#${popupid}_VM_NM').parent().find(':input').prop('disabled', false);
			} else {
				$('#${popupid}_VM_NM').parent().find(':input').prop('disabled', true);
			}
		}

		function ${popupid}_clearVMInput(){
			${popupid}_vue.form_data.VM_ID = null;
			${popupid}_vue.form_data.VM_NM = null;
		}

		function ${popupid}_getVMSearchParam(){
			return {
				DC_ID: $('#${popupid}_DC_ID').val(),
				VM_NM:$('#${popupid}_VM_NM').val()
			}
		}

		function select_${popupid}_vm(vm, callback){

			delete vm.TEAM_NM;
			${popupid}_vue.form_data.DC_ID = vm.DC_ID;
			${popupid}_vue.form_data.DC_NM = vm.DC_NM;
			${popupid}_vue.form_data.VM_ID = vm.VM_ID;
			${popupid}_vue.form_data.VM_NM = vm.VM_NM;

			${popupid}_vue.form_data.SERVER_IP = vm.PRIVATE_IP;
 			if(callback) callback();
		}

		function select_${popupid}_user(user, callback){
			${popupid}_vue.form_data.TEAM_CD = user.TEAM_CD;
			${popupid}_vue.form_data.TEAM_NM = user.TEAM_NM;
			${popupid}_vue.form_data.FW_USER_ID = user.USER_ID;
			${popupid}_vue.form_data.USER_NAME = user.USER_NAME;
			${popupid}_vue.form_data.CIDR = user.LAST_LOGIN_IP;
			$("#${popupid}_USER_NAME").val(user.USER_NAME);
			$("#${popupid}_CIDR").val(user.LAST_LOGIN_IP);
 			if(callback) callback();
		}
		
		function inputDisabled(action, obj, kubun)
		{
			if(!isSavable(obj)) return true;
			if(kubun=="updateReq" && (action=='update'  || obj.CUD_CD=='U' )){
				 
				if(obj.VM_USER_YN == "Y"){
					 
					return true;
				}
				return false; //추가
			}
			if(action=='insert'  || obj.CUD_CD=='C') return false;
			if(kubun=='save' && (  !obj.CUD_CD) && action !='update') return false;
			 
			return true;
		}

	</script>
</fm-modal>