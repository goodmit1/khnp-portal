<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);
	request.setAttribute("popupid", popupid);
	String newItemUrl = "/res/comp/apprCdCmt.jsp?prefix=" + popupid ;
%>
<style>

#reserve_popup .modal-dialog{
	width : 1000px;
}
#reserve_popup .modal-dialog .form-panel.detail-panel{
	height : 500px;
}
/* #reserve_popup .value-title{ */
/*     background-color: white; */
/* } */
#S_RSV_DT{
    width: 80px;
}
</style>

<fm-modal id="${popupid}" title='<spring:message code="btn_reserve_deploy" text="배포예약"/>' cmd="header-title">
	<span slot="footer">
		
		<button type="button" class="popupBtn next top5" onclick="deployReservation()"><spring:message code="btn_reserve_deploy" text="배포 예약"/></button>
	</span>
	<div class="form-panel detail-panel" style="height: 200px;">
		<div class="col col-sm-6" style="text-align: left;">
			<fm-date id="S_RSV_DT" name="RSV_DT"  title="<spring:message code="RSV_DT" text="예약일" />"></fm-date>
			<fm-select :options="hh" id="START_DATE_HH" name="START_DATE_HH" div_style="display:inline-block;"></fm-select>
		</div>
		<div class="col col-sm-6" style="text-align: left;">
			<label for="item" style="min-width: 140px; display: inline-block;" class="control-label grid-title value-title"><spring:message code="mail_send_yn" text="메일전송여부"/></label>
			<div style="display:inline-block;position: relative; ">
				<input type="checkbox" id="MAIL_SEND_YN" name="MAIL_SEND_YN" style="margin-top: 10px; position: relative; top: 4px;">
			</div>
		</div>
		<div class="col col-sm-12" style="text-align: left;">
			<span><label for="S_RSV_DT" class="control-label value-title grid-title" style="height: 123px;"><spring:message code="reason" text="사유"/></label> <div class="input-group hastitle" style="width: 800px;">
			<jsp:include page="<%=newItemUrl%>"></jsp:include>
			  </div></span>
			
		</div>
	 
	</div>
	<script>
		var appr = null;
		var ${popupid}_button = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		
		function ${popupid}_click(vue, param){
			 
			${popupid}_parent_vue = vue;
			if(param != null){
				 
				${popupid}_param = param;
			} else{
				${popupid}_param = {};
				alert(msg_select_first);
				return;
			}
			${popupid}_vue.form_data = ${popupid}_param;
			 
			return true;
		}
		
		  
		function getTime(){
			var str =  $("#S_RSV_DT").val().split("-")
			return str[0]+""+str[1]+""+str[2]+""+$("#START_DATE_HH").val();
		}
		function deployReservation(){
			var param = new Object();
			param.SVC_ID = ${popupid}_param.SVC_ID;
			param.INS_DT = ${popupid}_param.INS_DT;
			param.SVC_CD = ${popupid}_param.SVC_CD;
			param.MAIL_SEND_YN = $("#MAIL_SEND_YN:checked").length == 1 ? "Y" : "N";
		 
			param.ADMIN_CMT = $("#${popupid}APPR_CMT").val();
			param.ADMIN_CMT_CD = $("#${popupid}APPR_CMT_CD").val();
			if($("#S_RSV_DT").val() != null && $("#START_DATE_HH").val() != null)
				param.RSV_DT = getTime();
			else{
				alert('<spring:message code="RSV_DT_err" text="" />');
				return;
			}
			
			post('/api/deploy_fail/updateReserve',param,function(data){
				if(data){
						alert(msg_complete);
						
						$('#${popupid}').modal('hide');
						search();
				} else{
					alert(msg_jsp_error);
				}
			});
		}
	</script>
	<style>

	</style>
</fm-modal>