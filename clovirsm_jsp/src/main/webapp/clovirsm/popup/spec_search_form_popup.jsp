<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%
	String popupid = request.getParameter("popupid");
	request.setAttribute("popupid", popupid);

	String DC_ID = request.getParameter("DC_ID");
	if(DC_ID != null) request.setAttribute("DC_ID", DC_ID);
%>
<fm-modal id="${popupid}" title="<spring:message code="btn_spec_search"/>" cmd="header-title">
	<form id="${popupid}_form" action="none">
		<div class="panel-body">
			<div class="row search_area">
				<div class="col col-sm-8">
					<fm-select id="${popupid}_spec_nm" emptystr="" onchange="${popupid}_chgSpec()"  ></fm-select>
				</div>

				<div class="col btn_group">
					<!-- <input type="button" onclick="${popupid}_spec_search()" class="btn" value="<spring:message code="btn_search" text="검색"/>" />
					 -->
				</div>
			</div>
		</div>
		<div id="${popupid}_specSearchGrid" style="height:250px" class="ag-theme-fresh"></div>
	</form>

	<script>
		var ${popupid} = newPopup("${popupid}_button", "${popupid}");
		var ${popupid}_param = {};
		var ${popupid}_parent_vue = null;
		var ${popupid}_vue = new Vue({
			el: '#${popupid}',
			data: {
				form_data: ${popupid}_param
			}
		});
		function ${popupid}_chgSpec()
		{
			var filter = ${popupid}_specSearchGrid.gridOptions.api.getFilterInstance('SPEC_NM');
			 filter.setFilter($("#${popupid}_spec_nm option:selected").text());

			 ${popupid}_specSearchGrid.gridOptions.api.onFilterChanged();
		}
		function ${popupid}_click(vue, param){
			${popupid}_parent_vue = vue;
			${popupid}_spec_search(param);
			return true;
		}

		function ${popupid}_spec_search(param){
			${popupid}_param = {
				keyword_field: $('#${popupid}_keyword_field').val()
			}

			if(typeof param == 'object'){
				${popupid}_param.DC_ID = param.DC_ID;
				${popupid}_param.keyword = param.SPEC_NM;
			} else {
				${popupid}_param.keyword = param ? param: $('#${popupid}_keyword').val()
			}
			${popupid}_vue.form_data = ${popupid}_param;
			${popupid}_specSearchReq.searchSub('/api/popup/specSearch/list', ${popupid}_param, function(data) {

				var nm_arr = []
				for(var i=0; i < data.list.length; i++)
				{
					nm_arr.push(data.list[i].SPEC_NM);
				}

				nm_arr = arr_unique(nm_arr);
				 
				fillOptionByData('${popupid}_spec_nm', nm_arr,'== <spring:message code="label_spec" text=""/> ==');
				${popupid}_specSearchGrid.setData(data);
			});
		}

		var ${popupid}_specSearchReq = new Req();
		var ${popupid}_specSearchGrid;
		$(document).ready(function(){
			var ${popupid}_specSearchGridColumnDefs =[
				{headerName: "<spring:message code="label_spec" text=""/>", field: "SPEC_NM", width: 140},

				{headerName: "CPU", field: "CPU_CNT", width: 80,cellStyle:{'text-align':'right'},valueFormatter:function(params){
					return formatNumber(params.value)
				}},
				{headerName: "<spring:message code="NC_VM_RAM_SIZE" text="메모리"/>(GB)", field: "RAM_SIZE", width: 110,cellStyle:{'text-align':'right'},valueFormatter:function(params){
					return formatNumber(params.value)
				}},
				<%--{headerName: "<spring:message code="tab_disk"/>(GB)", field: "DISK_SIZE", width: 110,cellStyle:{'text-align':'right'},valueFormatter:function(params){--%>
				<%--	return formatNumber(params.value)--%>
				<%--}},--%>
				<%--{headerName: "<spring:message code="label_disk_kubun"/>", field: "DISK_TYPE_NM", width: 110},--%>

				{headerName: "<spring:message code="NC_DISK_TYPE_DD_FEE"/>", field: "DD_FEE", width: 100, cellStyle:{'text-align':'right'},valueFormatter:function(params){
					return formatNumber(params.value)
				}}
			];
			var ${popupid}_specSearchGridOptions = {
				columnDefs: ${popupid}_specSearchGridColumnDefs,
				rowData: [],
				sizeColumnsToFit:true,
				enableSorting: true,
				rowSelection:'single',
			    enableColResize: true,
			    onRowDoubleClicked: function(){
					var arr = ${popupid}_specSearchGrid.getSelectedRows();
					var callback = ${popupid}_parent_vue ? ${popupid}_parent_vue.callback: null;
					if(callback != null) eval( callback + '(arr[0], function(){ $(\'#${popupid}\').modal(\'hide\'); });');
			    }
			};
			${popupid}_specSearchGrid = newGrid("${popupid}_specSearchGrid", ${popupid}_specSearchGridOptions);
		});
	</script>
</fm-modal>