<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

 
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
#S_NAME{ width:180px;}
#S_START_PER,#S_END_PER { width:50px }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="DC_ID"
				keyfield="DC_ID" titlefield="DC_NM" 
				name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>"></fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=OVERALLSTATUS" id="S_OVERALLSTATUS"
				emptystr=" "
				name="OVERALLSTATUS" title="<spring:message code="monitor_CATEGORY" text="사용용도"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_NAME" name="NAME" title="<spring:message code="label_userName" text="이름"/>"></fm-input>
		</div>
		<div class="col col-sm">
			<table class="hasTiltTbl"><tr><td><fm-input id="S_START_PER" name="START_PER" title="<spring:message code="monitor_usage" text="" />"></fm-input>
			</td><td class="tilt">~</td><td >
			<fm-input id="S_END_PER" name="END_PER"  ></fm-input>
			</td></tr></table>
		</div>	
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>
		</div>
	</div>
</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 700px" ></div>
	</div>
	</div>
</div>
<script>
	var req = new Req();
	mainTable;
	$(function() {
		var columnDefs = [{headerName : "<spring:message code="monitor_NAME" text="" />",field : "NAME", width:400},
			{headerName : "<spring:message code="monitor_CAPACITY" text="" />",width:130, cellStyle:{'text-align':'right'}, field : "CAPACITY",
				valueGetter: function(params) {
		    	return  getDiskSize(params.data.CAPACITY);
			}},
			{headerName : "<spring:message code="monitor_PROVISIONED" text="프로비저닝됨" />",width:140, cellStyle:{'text-align':'right'},field : "UNCMTD",
				valueGetter: function(params) {
					 
			    	return  getDiskSize(params.data.UNCMTD + (params.data.CAPACITY - params.data.FREESPACE));
				}},
			{headerName : "<spring:message code="monitor_FREESPACE" text="" />",width:140, cellStyle:{'text-align':'right'},field : "FREESPACE",
					valueGetter: function(params) {
			    	return  getDiskSize(params.data.FREESPACE);;
				}},
			{headerName : "<spring:message code="monitor_usage" text="" />",width:120, format:'number',field : "PER" },
			
			{headerName : "<spring:message code="monitor_VM_CNT" text="" />",width:100, cellStyle:{'text-align':'right'},field : "VM_CNT"},
			{headerName : "<spring:message code="monitor_OVERALLSTATUS" text="" />",width:100, cellStyle:{'text-align':'center'},field : "OVERALLSTATUS",
  				cellRenderer :function(params){
	  				return '<img src="/res/img/' + params.data.OVERALLSTATUS + '-icon.png">'
	  			}},
			]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			 
			cacheBlockSize: 100,
			rowData : [],
			enableSorting : true,
			enableColResize : true,
			enableServerSideSorting: false,
			onSelectionChanged : function() {
				var arr = mainTable.getSelectedRows();
				arr[0].DC_ID = search_data.DC_ID
				arr[0].name=arr[0].NAME;
				//goTabContent(arr[0])
			}
		}
		mainTable = newGrid("mainTable", gridOptions);
		setTimeout(function(){ search(); }, 100);

	});
	function getDiskSize(DISK_SIZE)
	{
		var gb = 1.0 * DISK_SIZE/1024/1024/1024;
		 
		if(gb<1024)
		{
			return  (gb).toFixed(2)   + ' GB'
		}	
		else
		{
			return  (gb/1024).toFixed(2)   + ' TB'
		}	
	}
	// 조회
	function search() {
		req.search('/api/admin_monitor/list/DS' , function(data) {
			mainTable.setData(data);
		});
	}

	// 엑셀 내보내기
	function exportExcel() {
		  mainTable.exportCSV({fileName:'HOST'})
	}
	
</script>