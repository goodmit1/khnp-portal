<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="table_title layout name" style="float: left;">
				<spring:message code="count" text="건수"/> : <span id="vmTable_total">0</span>
</div>
<div id="vmChartArea">
					<div class="termArea table_title">
						<dl>
							<dt style="background:none;" class="reload layoutPostion"><input type="text" class="form-control input hastitle" style="width:30px; min-width:30px;" id="vm_reloadTime"><span><spring:message code="minute" text="" /></span> <img id="vm_isMonitor" onclick="vmTable.monitorStartAndStop();" src="/res/img/mobis/start_icon.png"> </dt>
							<dt style="background:none;" class="layoutPostion">
							<input type="checkbox" value="true" id="INNER_HOUR" style="margin-right:12px;"/>
							<label for="INNER_HOUR"></label>
							<spring:message code="only_time" text="" /></dt>
							<span id="hideArea" class="layoutPostion" style="display:inline-block" >
								<table><tr><td>
									<fm-date name="START_DT" id="vm_START_DT" ></fm-date>
								</td>
								<td>
									<select id="vm_START_DT_S">
									<option value="00:00">&nbsp</option>
										<option>00:00</option>
										<option>01:00</option>
										<option>02:00</option>
										<option>03:00</option>
										<option>04:00</option>
										<option>05:00</option>
										<option>06:00</option>
										<option>07:00</option>
										<option>08:00</option>
										<option>09:00</option>
										<option>10:00</option>
										<option>11:00</option>
										<option>12:00</option>
										<option>13:00</option>
										<option>14:00</option>
										<option>15:00</option>
										<option>16:00</option>
										<option>17:00</option>
										<option>18:00</option>
										<option>19:00</option>
										<option>20:00</option>
										<option>21:00</option>
										<option>22:00</option>
										<option>23:00</option>
									</select>
								</td>
								<td>
									&nbsp~&nbsp
								</td>
								<td>
									<fm-date id="vm_FINISH_DT" name="FINISH_DT" ></fm-date>
								</td>
								<td>
									<select id="vm_FINISH_DT_S">
										<option value="23:59">&nbsp</option>
										<option>00:00</option>
										<option>01:00</option>
										<option>02:00</option>
										<option>03:00</option>
										<option>04:00</option>
										<option>05:00</option>
										<option>06:00</option>
										<option>07:00</option>
										<option>08:00</option>
										<option>09:00</option>
										<option>10:00</option>
										<option>11:00</option>
										<option>12:00</option>
										<option>13:00</option>
										<option>14:00</option>
										<option>15:00</option>
										<option>16:00</option>
										<option>17:00</option>
										<option>18:00</option>
										<option>19:00</option>
										<option>20:00</option>
										<option>21:00</option>
										<option>22:00</option>
										<option>23:00</option>
									</select>
								</td>
								<td>
									<button type="button" title="reload" onclick="reloadVMInCluster()" class="btn "><i class="fa fa-search"></i></button>
								</td>
								</tr>
								</table>
							</span>
							<button type="button" style=" float: none;" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="vmTable.mainTable.exportCSV({fileName:'alarm'})" class="layout excelBtn top0 right" id="excelBtn"></button>
						</dl>
					</div>
				</div>
<div id="vmTable" class="ag-theme-fresh" style="height: 500px; clear: both;" ></div>
<script>
				var vmTable = new Vue({
				  el: '#vmTable',
				  data: {
					  vmTable: [],
					  isMonitor:false,
					  interval:null
				  },
				  methods: {
					  
				  	  init:function()
				  	  {
				  		var columnDefs = [{headerName : "<spring:message code="NC_VM_VM_NM" text="" />",field : "NAME", width:250,
				  			cellRenderer:function(params)
								{
									if(params.data.TEMPLATE)
									{
										return '<span style="color:blue">' + params.data.NAME + '</span>';	
										
									}
									else
									{
										return '<span class="vm">' + params.data.NAME + '</span>';
									}
								}
				  			
				  			},
				  		
				  			{headerName : "OS",field : "GUESTFULLNAME", width:250 },
				  			{headerName : "CPU",field : "NUMCPU" , cellStyle:{'text-align':'right'} , width:100},
				  			{headerName : "Mem(GB)",field : "MEMORYSIZEMB", width:150,  cellStyle:{'text-align':'right'} , valueFormatter: function(params) {
						    	return  params.value/1024;
							}},
				  			{headerName : "Mem(%)",field : "mem_usage", width:150, cellStyle:{'text-align':'right'} , valueFormatter: function(params) {
						    	return  formatNumber(params.value,2);
							}},
				  			{headerName : "CPU(%) ",field : "cpu_usage", width:150, cellStyle:{'text-align':'right'} , valueFormatter: function(params) {
						    	return  formatNumber(params.value,2);
							}},
				  			{headerName : "Disk(GB)",field : "disk_totalgb", width:150, format:'number' },
				  			{headerName : "Used Disk(GB)",field : "disk_usedgb", width:200, format:'number' },
				  			{headerName : "Disk(%)",field : "disk_usage_per", width:150, cellStyle:{'text-align':'right'} , valueFormatter: function(params) {
						    	return  formatNumber(params.value,2);
							}},
				  			];
				  		var gridOptions = {
							hasNo : true,	
							columnDefs : columnDefs,
							//rowModelType: 'infinite',
							//rowSelection : 'single',
							sizeColumnsToFit: false,
							cacheBlockSize: 100,
							rowData : [],
							enableSorting : true,
							enableColResize : true,
							enableServerSideSorting: false,
							
						}
						this.mainTable = newGrid("vmTable", gridOptions);
						 
				  	  },
				  	  monitorStartAndStop : function(){
				  		
							if(this.isMonitor){
								this.isMonitor = false;
								$("#vm_isMonitor").attr("src","/res/img/mobis/start_icon.png");
								clearInterval(this.interval);
							}else{
								this.isMonitor = true;
								$("#vm_isMonitor").attr("src","/res/img/mobis/stop_icon.png");
								var time = $("#reloadTime").val() * 60000;
								this.interval = setInterval(function() { this.getReqInfo();}, time);
							}
						},
					  getReqInfo: function (selRow) {
						 
						if(selRow && selRow != null) {
								this.selRow = selRow;
						}
						var param = this.selRow;
						var that = this;
						if($("#vm_START_DT").val() != ""){
							param.START_DT=$("#vm_START_DT").val()+" "+$("#vm_START_DT_S").val();
							
						}
						else
						{
							delete param.START_DT;	
						}
						if($("#vm_FINISH_DT").val() != ""){
							param.FINISH_DT=$("#vm_FINISH_DT").val()+" "+$("#vm_FINISH_DT_S").val();
						}
						else
						{
							delete param.FINISH_DT;
						}
						param.INNER_HOUR = $("#INNER_HOUR").prop("checked");
						if($("#INNER_HOUR").prop("checked")){
							param.interval="7200";
						}
						else{
							delete param.interval;
						}
						post('/api/admin_monitor/list/VM', param, function(data)
							{
								that.mainTable.setData(data);
							}
						)
				    }
				  }
				});
				$(document).ready(function(){
					vmTable.init();
				})
				
				function reloadVMInCluster()
				{
					vmTable.getReqInfo();
				}
			 
</script>