<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!--  서버 목록 조회  -->
<script type="text/javascript" src="/res/js/heatmap.js"></script>
<style>
 .select2-container {
	width: 300px !important;
	margin-left: 5px;
}
 
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">
		<!-- 조회의 id는 S_를 붙인다. -->
		 
			 
			<div class="col col-sm">
				<fm-select
					url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC"
					id="S_DC_ID" onchange="getSVCHostMonitorInfo()" keyfield="DC_ID" titlefield="DC_NM"
					name="DC_ID"
					title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>">
				</fm-select>
			</div>
			<div class="col col-sm">
				<fm-select id="S_CNT" :options="cnt_option" onchange="drawChart( )" title="<spring:message code="label_svc_cnt_min" text="중복서버 개수(이상)"/>">
				</fm-select>	
			</div>
			<div class="col col-sm">
				<fm-select2
					url="/api/category/map/list/list_NC_CATEGORY_MAP_SELECT_TASK/?ALL_USE_YN=Y"
					id="S_CATEGORY" onchange="drawChart()" keyfield="CATEGORY_ID" :titlefield=["CATEGORY_NM","CATEGORY_CODE"]
					name="CATEGORY" emptyStr=""
					title="<spring:message code="TASK"/>">
				</fm-select2>	
			</div>
			<div class="  col col-sm ">
					<fm-select url="/api/code_list?grp=PURPOSE" id="S_PURPOSE"
						name="PURPOSE"  emptyStr=""  onchange="drawChart()"  
						title="<spring:message code="PURPOSE" text="업무"/>"   > </fm-select>
				</div>
			<div class="col col-sm">
				<fm-select
					url="/api/code_list?grp=P_KUBUN"
					id="S_P_KUBUN" onchange="drawChart()"  
					name="P_KUBUN"  emptyStr=""
					title="<spring:message code="NC_VM_P_KUBUN" text="구분"/>">
				</fm-select>
			</div>
			 
	</div>
</div>

<div class="fullGrid" id="input_area" style="height:640px">
<div class="panel-body">
<div id="monitor_svc_host_count"  class="chart-width-subtitle" style="height:630px"></div>
</div>
</div>

<script>
	 
			var svc_host_xaxis = {}; 
			<jsp:include page="../svchostcnt2/_common.jsp"></jsp:include>
			function drawChart( ){

				if(!data) return;
				var yaxis = {};
				var chart_data= [];
				for(var i=0; i< data.length; i++){
					try{
						 
							svc_host_xaxis[getTagKey(data[i])]= data[i] ;
						 
							yaxis[data[i].HOST_NM]='';
						 
					} 
					catch(e){}
				 }
				for(var i=0; i< hostList.length; i++){
					yaxis[hostList[i].OBJ_NM]='';
				}
				 var xarr = Object.keys(svc_host_xaxis);
				 var yarr = Object.keys(yaxis);
				 var xy={};
				 for(var i=0; i< data.length; i++){
					 try{
						 var x = xarr.indexOf(getTagKey(data[i]));
						 var y = yarr.indexOf(data[i].HOST_NM);
						 if(x>=0 && y>=0 && isTarget(data[i])){ 
							 data[i].x = x;
							 data[i].y = y;
							 data[i].value = data[i].CNT
							 data[i].color =  getAreaColor( data[i].CNT);
							 chart_data.push(data[i])
							  
						 }
					 }
					 catch(e){}
					
				 }
				  
				 Highcharts.chart('monitor_svc_host_count', {

					    chart: {
					        type: 'heatmap',
					        marginTop: 40,
					        marginBottom: 80,
					        plotBorderWidth: 1
					    },


					    title: {
					        text: ''
					    },

					    xAxis: {
					        categories: xarr
					    },

					    yAxis: {
					        categories:yarr,
					        title: null,
					        reversed: true
					    },
					    
					    

					    legend: {
					        enabled: false 
					    },

					    tooltip: {
					        formatter: function () {
					            return '<spring:message code="TASK"/> : ' + getTagTaskNm(this.point ) + ' <br>' +
					            'Host : ' + getPointCategoryName(this.point, 'y') + ' <br>' +
					            '<spring:message code="count"/>: '+   this.point.value ;
					        }
					    },

					    series: [{
					        name: 'VM Count',
					        borderWidth: 1,
					        data:chart_data,
					        dataLabels: {
					            enabled: true,
					            color: '#000000',
					            style: {
				                    textOutline: false 
				                }
					        }
					    }],
					    plotOptions: {
					    	series: {
			                	events: {
			                    	click: function (e) {
			                    		 
			                    		var svccd  = getPointCategoryName(e.point, 'x');
			                    		var taskNm = getTagTaskNm(e.point );
			                    		var host = getPointCategoryName(e.point, 'y');
			                    		var url = '/api/monitor/list/list_vm_by_host_tag/?' + getTagUrlParam(svc_host_xaxis[svccd]) + 'DC_ID=' + $("#S_DC_ID").val() + '&HOST_NM=' + host   
			                    		$.getJSON(url, function(data){
			                    			var html = '<div style="font-size:14px;text-align:left">Host : ' + host + "<br>";
			                    			html += '<spring:message code="TASK"/> : ' + taskNm + '<br><br>'
			                    			for(var i=0; i<data.length; i++){
			                    				html += '<li><a  tabindex="-1" href="/clovirsm/monitor/admin_vm/index.jsp?DC_ID='+ e.point.DC_ID + '&keyword=' + data[i].VM_NM + '">' + data[i].VM_NM + '</a></li>'
			                    			}
			                    			html += '</div>'
			                    			//message, title, oklabel, okAction , width, height
			                    			FMAlert(html,'VM',false,false,300,250);
			                    		})
			                    	}
			                	}
					    	}
					    },
					    responsive: {
					        rules: [{
					            condition: {
					                maxWidth: 500
					            },
					            chartOptions: {
					                yAxis: {
					                    labels: {
					                        formatter: function () {
					                            return this.value.charAt(0);
					                        }
					                    }
					                }
					            }
					        }]
					    }

					}); 
			
			}
			var data;
			var hostList ; 
			function getSVCHostMonitorInfo()
			{
				var DC_ID=$("#S_DC_ID").val();
				$.get('/api/monitor/list/list_NC_HV_OBJ/?OBJ_TYPE_NM=HostSystem&DC_ID=' + DC_ID, function(data1){
					hostList = data1;
					$.get('/api/monitor/list/list_vm_host_count_by_tag/?' + getTagUrlParam() + 'DC_ID=' + DC_ID, function(data1)
					{
						data = data1;
						drawChart( )
					});
				});
				
			}
			 

		 
			$(document).ready(function(){
			})
			 

			</script>