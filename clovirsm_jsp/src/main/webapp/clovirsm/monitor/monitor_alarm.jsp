<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div class="table_title layout name">
				<spring:message code="count" text="건수"/> : <span id="alarmTable_total">0</span>
				<div class="btn_group">
				 		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="alarmTable.mainTable.exportCSV({fileName:'alarm'})" class="layout excelBtn right" id="excelBtn"></button>
				</div>

</div>    
<div id="alarmTable" class="ag-theme-fresh" style="height: 600px" ></div>
<script>
				var alarmTable = new Vue({
				  el: '#alarmTable',
				  data: {
					  alarmTable: []
				  },
				  methods: {
					  
				  	  init:function()
				  	  {
				  		var columnDefs = [{headerName : "Type",field : "ALARM_NM" , width:300},
				  			{headerName : "<spring:message code="monitor_TARGET" text="" />",field : "TARGET"},
				  			{headerName : "<spring:message code="monitor_TARGET_TYPE" text="" />",field : "TARGET_TYPE"},
				  			{headerName : "<spring:message code="monitor_CATEGORY" text="" />",field : "CATEGORY", cellStyle:{'text-align':'center'}, 
				  				cellRenderer :function(params){
				  				return '<img src="/res/img/' + params.data.CATEGORY + '-icon.png">'
				  			}},
				  			{headerName : "<spring:message code="monitor_CRE_TIME" text="" />",field : "CRE_TIME",
				  				valueFormatter: function(params) {
							    	return formatDate(params.value,'datetime')
								}},
				  			];
				  		var gridOptions = {
							hasNo : true,	
							columnDefs : columnDefs,
							//rowModelType: 'infinite',
							//rowSelection : 'single',
							sizeColumnsToFit: true,
							cacheBlockSize: 100,
							rowData : [],
							enableSorting : true,
							enableColResize : true,
							enableServerSideSorting: false,
							
						}
						this.mainTable = newGrid("alarmTable", gridOptions);
						 
				  	  },
					  getReqInfo: function (param) {
						var that = this;
						param.REAL_YN='Y';
						post('/api/monitor/alarm/' + param.DC_ID + '/${param.key}/' + param.name + '/',  param, function(data)
							{
								data.list=data.LIST;
								that.mainTable.setData(data);
							}
						)
				    }
				  }
				});
				 
				$(document).ready(function(){
					alarmTable.init();
				})
			 
</script>