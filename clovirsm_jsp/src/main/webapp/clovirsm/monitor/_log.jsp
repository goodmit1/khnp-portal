 <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<layout:extends name="base/popup_main">
<layout:put block="content">
 
<style>
.log-view-output {
	padding: 40px 15px 40px 0;
	font-size: 12px;
	background: #000;
	min-height: 549px;
	height: 100%;
	overflow: auto;
	font-family: Menlo,Monaco,Consolas,monospace;
}
.log-view-output table {
	table-layout: fixed;
	width: 100%;
}
.log-line-text ::selection, .log-line-text::selection {
	background: #f7f7f7;
	color: #101214;
}
.log-line {
	color: #d1d1d1;
}
.log-line:hover {
	background-color: #22262b;
	color: #ededed;
}
.log-line-number {
	border-right: 1px #272b30 solid;
	padding-right: 10px;
	vertical-align: top;
	white-space: nowrap;
	width: 60px;
	color: #72767b;
	 text-align: right;
}
.log-line-number:before {
	 content: attr(data-line-number);
}
.log-line-text {
	padding: 0 10px;
	white-space: pre-wrap;
	width: 100%;
}
.log-line-text, .text-prepended-icon {
	word-break: break-word;
	overflow-wrap: break-word;
	word-wrap: break-word;
	min-width: 0;
}
#autoScrollToggle {
	right: 25px;
	top: 10px;
	position: absolute;
	display: inline-block;
	background-color: #545e69;
	border-radius: 1px;
	box-shadow: 0 0 5px 7px rgba(0,0,0,.9);
	color: #fff;
	font-size: 12px;
	padding: 2px 6px;
	font-family: Menlo,Monaco,Consolas,monospace;
}
</style>
<script src="/res/js/sockjs.js"></script>
<div style="position: relative; height: 100%; text-align: center;">
	<div id="autoScrollToggle" onclick="toggleAutoScroll()">
		<span>Stop Follow</span>
	</div>
	<div class="log-view-output" id="log-viewer4-fixed-scrollable">
		<table>
			<tbody id="log-viewer4-logContent">
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
var index = 0;
var autoScroll = true;

$(document).ready(function() {
	 
});

function addRow(log) {
	var html = '<tr class="log-line"><td class="log-line-number" data-line-number="' + ++index + '"></td><td class="log-line-text">'
	html += log;
	html += '</td>';
	$("#log-viewer4-logContent").append(html);
	afterAddRow();
}
var timeoutIsRun = false;


let sock = new SockJS("/api/ws/vm/log");

sock.onmessage = onMessage;

sock.onclose = onClose;

sock.onopen = function(){
	var obj = {target:"${param.target}",field:"${param.field}"};
	sendMessage(JSON.stringify(obj))
} 

// 메시지 전송

function sendMessage(str) {
	sock.send( str );
}


// 서버로부터 메시지를 받았을 때
var idx = 1;
function onMessage(msg) {
	var data = msg.data;
	addRow(data);
}

// 서버와 연결을 끊었을 때

function onClose(evt) {
 
	$("#data").append("연결 끊김");
}

function afterAddRow() {
	if(autoScroll) {
		if(!timeoutIsRun) {
			timeoutIsRun = true;
			setTimeout(function() {
				$(".log-view-output").scrollTop(99999999);
				timeoutIsRun = false;
			}, 100)
		}
	}
}

function toggleAutoScroll() {
	autoScroll = !autoScroll;
	if(autoScroll) {
		$(".log-view-output").scrollTop(99999999);
		$("#autoScrollToggle span").text("Stop Following");
	}else {
		$("#autoScrollToggle span").text("Follow");
	}
}
</script>
</layout:put>
</layout:extends>
