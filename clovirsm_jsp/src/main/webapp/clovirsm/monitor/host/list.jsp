<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%
String DC_ID = request.getParameter("DC_ID");
String OVERALLSTATUS = request.getParameter("OVERALLSTATUS");
String NAME = request.getParameter("NAME");
String HOST = request.getParameter("HOST");
if(HOST == null) HOST="";
request.setAttribute("DC_ID", DC_ID == null ? "null" : "'" + DC_ID + "'");
request.setAttribute("OVERALLSTATUS", OVERALLSTATUS == null ? "null" : "'" + OVERALLSTATUS + "'");
request.setAttribute("NAME", NAME == null ? "'" + HOST + "'" : "'" + NAME + "'");
%>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
#S_NAME{ width:180px;}
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->
		
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="DC_ID"
				keyfield="DC_ID" titlefield="DC_NM" 
				name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>"></fm-select>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=OVERALLSTATUS" id="S_OVERALLSTATUS"
				emptystr=" "
				name="OVERALLSTATUS" title="<spring:message code="STATUS" text="상태"/>">
			</fm-select>
		</div>
		<div class="col col-sm">
			<fm-input id="S_NAME" name="NAME" title="<spring:message code="label_userName" text="이름"/>"></fm-input>
		</div>
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>
		</div>
	</div>

</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		<%-- <c:if test="${sessionScope.LOG_SHOW eq 'Y' }">
			<fm-popup-button popupid="category1" class="contentsBtn tabBtnImg collection"
					popup="/clovirsm/monitor/log.jsp"
					cmd="update" param="form_data" >LOG</fm-popup-button>
		</c:if> --%>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 250px" ></div>
	</div>
	</div>
	<jsp:include page="detail.jsp"></jsp:include>

</div>
<script>
	var req = new Req();
	mainTable;

	$(function() {
		var columnDefs = [{headerName : "<spring:message code="monitor_NAME" text="" />",field : "NAME", width:250},
			

			{headerName : "<spring:message code="monitor_CPUMODEL" text="" />",field : "CPUMODEL", width:400},
			{headerName : "<spring:message code="monitor_NUMCPUCORES" text="" />",field : "NUMCPUCORES",minWidth:120, cellStyle:{'text-align':'right'}},
			{headerName : "<spring:message code="monitor_MEMORYSIZE" text="" />(GB)",minWidth:120,field : "MEMORYSIZE", cellStyle:{'text-align':'right'},
				valueFormatter: function(params) {

		    	return  (params.value/1024/1024/1024).toFixed(0);
			}},
			{headerName : "GPU(Q)", field : "GPU_SIZE", format:'number'},
			{headerName : "CPU(%)",field : "CPU_USAGE",minWidth:120,cellStyle:{'text-align':'right'}},
			{headerName : "<spring:message code="NC_VM_RAM_SIZE" text="메모리"/>(%)",field : "MEM_USAGE",minWidth:120,cellStyle:{'text-align':'right'}},
			{headerName : "<spring:message code="monitor_VM_CNT" text="" />",minWidth:120,field : "VM_CNT",cellStyle:{'text-align':'right'}},
			{headerName : "<spring:message code="monitor_OVERALLSTATUS" text="" />",minWidth:100,field : "OVERALLSTATUS",cellStyle:{'text-align':'center'},tooltipField:"ALARM_NM", cellRenderer :function(params){
  				 
  				return '<img src="/res/img/' + params.data.OVERALLSTATUS + '-icon.png">'
  			}},
			]
		var
		gridOptions = {
			hasNo : true,
			columnDefs : columnDefs,
			//rowModelType: 'infinite',
			rowSelection : 'single',
			cacheBlockSize: 100,
			rowData : [],
			getRowNodeId : function(data) {
			    return data.NAME;
			},
			enableSorting : true,
			enableColResize : true,
			 enableServerSideSorting: false,
			 onSelectionChanged : function() {
					var arr = mainTable.getSelectedRows();
					arr[0].DC_ID = search_data.DC_ID
					arr[0].name=arr[0].NAME;
					form_data = arr[0]
					goTabContent(arr[0])
					

				},
			/* onCellMouseOver : function(params) {
				var arr = params.data;
				if(arr.ALARM_NM != undefined)
					{
						 
					}
			} */
		}
		
		//gridOptions.onCellMouseOver();
		mainTable = newGrid("mainTable", gridOptions);

		setTimeout(function() {
			if(${DC_ID}){
				$("#DC_ID").val(${DC_ID});
				searchvue.form_data.DC_ID = ${DC_ID};
			}
			if(${NAME}){
				$("#S_NAME").val(${NAME});
				searchvue.form_data.NAME = ${NAME};
			}
			if(${OVERALLSTATUS}){
				$("#S_OVERALLSTATUS").val(${OVERALLSTATUS});
				searchvue.form_data.OVERALLSTATUS = ${OVERALLSTATUS};
			}
			search();
		}, 1000);
	});
	var hostNm = "${param.HOST}";

	// 조회
	function search() {


		req.search('/api/admin_monitor/list/HOST' , function(data) {
			mainTable.setData(data);
			if(hostNm != '')
			{
				mainTable.setSelectedById(hostNm,true)
			}
		});
	}

	// 엑셀 내보내기
	function exportExcel()
	{
		  mainTable.exportCSV({fileName:'HOST'})
	}

</script>


