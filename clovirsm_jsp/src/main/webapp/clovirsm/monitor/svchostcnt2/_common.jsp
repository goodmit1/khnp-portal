var cnt_option=[ 1,2,3]
		 
			var tags= ["svc","purpose","p_kubun"];
			function getTagUrlParam(obj){
				var result = '';
				for(var i=0; i < tags.length; i++) {
					 
					result +='TAGS=' + tags[i] + "&"
				}
				if(obj){
					for(var i=0; i < tags.length; i++) {
						 
						result += tags[i] + '=' + obj[tags[i].toUpperCase()] + "&"
					} 
				}
				return result;
			}
			function getTagKey(data1){
				
				var result = '';
				for(var i=0; i < tags.length; i++) {
					result += nvl(data1[tags[i].toUpperCase()],'') 
				}
				return result;
			}
			function getTagTaskNm( obj ){
				var result = '';
				var resultNm = '';
				for(var i=0; i < tags.length; i++) {
					result += obj[tags[i].toUpperCase()]
					resultNm += obj[tags[i].toUpperCase() + "_NM"] + " "
				}
				return resultNm + '(' + result + ')';
			 
				
			}
			function isTarget(data1){
				 
				
				 if($("#S_CNT").length>0 && data1.CNT <  $("#S_CNT").val()){
					return false;
				}
				var categorys  = $("#S_CATEGORY option:selected");
				if($("#S_CATEGORY option:selected")[0].data && categorys.length>0 && data1.SVC != categorys[0].data.CATEGORY_CODE){
					 return false;
				}
				var pkubun = $("#S_P_KUBUN").val();
				if(pkubun && pkubun != data1.P_KUBUN){
					return false;
				}
				var purpose = $("#S_PURPOSE").val();
				if(purpose && purpose != data1.PURPOSE){
					return false;
				}
				return true;
			}
			
			function getPointCategoryName(point, dimension) {
			    var series = point.series,
			        isY = dimension === 'y',
			        axis = series[isY ? 'yAxis' : 'xAxis'];
			    
			    return axis.categories[point[isY ? 'y' : 'x']];
			}
			function getAreaColor(cnt){
				if(cnt==0){
					return 'white';
				}
				if(cnt==1){
					return '#48bdb6';
				}
				else if(cnt==2){
					return '#f7bc60';
				}
				else if(cnt>2){
					return '#fd5d55';
				}
			}