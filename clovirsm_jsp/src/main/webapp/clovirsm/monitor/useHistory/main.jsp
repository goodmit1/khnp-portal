<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://fliconz.kr/jsp/tlds/fmtags" prefix="fmtags"%>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
.spinner, .spinner input{
  width: 60px !important;
}
.search-panel .grid-title{
	min-width:0px;
}
#search_area > div > div:nth-child(1) > div > span{
	width: 150px;
}

</style>
<link rel="stylesheet" href="/res/css/usehistory.css">


<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<!-- 조회의 id는 S_를 붙인다. -->


			<c:if test="${sessionScope.ADMIN_YN == 'Y'}">
				<div class="col col-sm">

						<fm-select2 url="/api/etc/team_list2" id="S_TEAM_CD" emptystr=" "  select_style="width: 175px"
								name="TEAM_CD" title="<spring:message code="FM_TEAM_TEAM_CD" text="팀"/>"></fm-select2>
				</div>
			</c:if>
			<div class="col col-sm">
				<table><tr>
					<td>조회 기간</td>
					<td>
						<fm-spin id="YYYY"
							name="YYYY" title=""   ></fm-spin>
					</td>
					<td>
						<label for="YYYY" class="control-label grid-title"><spring:message code="year" text="년"/></label>
					</td>
					<td>
						<fm-spin id="MM"
							name="MM" title="" min="1" max="12"  ></fm-spin>
					</td>
					<td>
						<label for="MM" class="control-label grid-title"><spring:message code="month" text="월"/></label>
					</td>
				</tr></table>
			</div>


			<div class="col btn_group nomargin">

			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"  ><spring:message code="btn_search" text="조회"/></fm-sbutton>


		</div>
	</div>


</div>
<div class="fullGrid" id="input_area">

	<fmtags:include page="detail.jsp"/>
	<%-- <jsp:include page="detail.jsp"></jsp:include> --%>
	<jsp:include page="list.jsp"></jsp:include>
	<div class="chart col-sm-6 LPadding" >
		<jsp:include page="history_fee.jsp"></jsp:include>
	</div>
	<div class="chart col-sm-6 RPadding">
		<jsp:include page="history_bar.jsp"></jsp:include>
	</div>
</div>
<script>

					var req = new Req();
					function initFormData()
					{
						var date = new Date();
						search_data.YYYY=date.getFullYear();
						search_data.MM=date.getMonth()+1;
						form_data.FEE=0;
						form_data.VM_CNT=0;
						form_data.DISK_SIZE=0;
						form_data.RAM_SIZE=0;
						form_data.IMG_CNT=0;
					}
					$(function() {
						setTimeout(function() {
							search();
						}, 1000);
					});

					// 조회
					function search() {
						search_data.YYYYMM=search_data.YYYY + ("" + search_data.MM).lpad(2,'0')
						req.search('/api/use_history/info' , function(data) {
							req.setData(data);
						});
						list(search_data);
						getHistoryInfo(search_data);
					}

					// 엑셀 내보내기
					function exportExcel()
					{
						  mainTable.exportCSV({fileName:'HOST'})
					}

				</script>


