<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<div id="history_bar" class="chart_area"></div>
<script>

			function getHistoryInfo(search_param)
			{

				var date = new Date(search_param.YYYYMM.substring(0,4), 1*search_param.YYYYMM.substring(4)-13)
				search_param.START_YYYYMM=formatDatePattern(date, 'yyyyMM');

				post('/api/use_history/history',search_param, function(data)
					{
						drawHistoryMonitorChart(data)
					});
			}
			function drawHistoryMonitorChart(  list) {

				var fee_data = [];
				var csv_data = "YYYYMM, VM<spring:message code="cnt" />, DISK(TB), <spring:message code="NC_VM_RAM_SIZE" text="메모리"/>(GB), CPU<spring:message code="cnt" />\n";
				for(var i=0; i < list.length; i++)
				{
					var date = list[i].YYYYMM.substring(0,4) + "-" + list[i].YYYYMM.substring(4);
				
					csv_data += "\"" + date + "\", " + list[i].VM_CNT + ", " + (list[i].DISK_SIZE/1024 ) + ", " + (list[i].RAM_SIZE  ) + ", " + nvl(list[i].CPU_CNT,0) + "\n";
					fee_data.push({name:date, y:list[i].FEE})
				}
				drawFeeHistoryChart(fee_data);
				 
				var config = {
						  chart: {
							    type: 'line'
						  },
						  data : {
							csv : csv_data
						  }	,
						  title: {
						    text: '<spring:message code="month_rsc_history" text="월별 리소스 추이"/>'
						  },
						  xAxis: {
							  	
							    type: 'category'
						    },
						  yAxis: [
							  {
							    title: {
							      text: '<spring:message code="label_size" text="크기"/>'
							    },
							    min :0
							

							  }

							  , { // Secondary yAxis
							        title: {
							            text: '<spring:message code="cnt" />',
							            style: {
							                color: Highcharts.getOptions().colors[0]
							            }
							        },
							        labels: {
							            format: '{value} <spring:message code="label_amount"/>',
							            style: {
							                color: Highcharts.getOptions().colors[0]
							            }
							        },
							        opposite: true,
							        min :0
							        
							    }
							],
						  legend: {
						    enabled: true
						  },

						  series:[
								{
									type: 'column'
							    }, {
							    	name:'DISK(TB)'
							    },{
							    	name:'<spring:message code="NC_VM_RAM_SIZE" text="메모리"/>(GB)'
							    },{
							    	name:'CPU<spring:message code="cnt" />'
							    }
						  ]
					};




				var chart2 = Highcharts.chart('history_bar', config);

			}


			</script>
