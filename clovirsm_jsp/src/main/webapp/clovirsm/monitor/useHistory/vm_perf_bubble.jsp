<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page contentType="text/html; charset=UTF-8"%>
<script src="/res/js/perf-chart.js"></script>
		<!-- VM 상태 -->

			 <div id="monitor_vm" class="chart_area"></div>


			<script>


			function drawVMMonitorChart(list) {
				var data = [];
				var green_cnt = 0;
				var yellow_cnt = 0;
				var red_cnt = 0;
				for(var i=0; list != null && i < list.length; i++)
				{
					if(list[i].CATEGORY == "RED")
					{
						red_cnt++;
					}
					else if(list[i].CATEGORY == "YELLOW")
					{
						yellow_cnt++;
					}
					else
					{
						green_cnt++;
					}
					data.push(  { id: list[i].OBJ_ID, x: list[i].CPU_USAGE, y: list[i].MEM_USAGE, z: list[i].DISK_USAGE, name: list[i].VM_NM, color:   convertColor(list[i].CATEGORY )} )	;
				}
				 
				var tooltip={
					    useHTML: true,
					    headerFormat: '<table>',
					    pointFormat: '<tr><th colspan="2"><h3>{point.name}</h3></th></tr>' +
					      '<tr><th>CPU:</th><td>{point.x}%</td></tr>' +
					      '<tr><th>MEMORY:</th><td>{point.y}%</td></tr>' +
					      '<tr><th>DISK:</th><td>{point.z}%</td></tr>',
					    footerFormat: '</table>',
					    followPointer: true
					  } ;
				var config = bubbleChartConfig( 'VM<spring:message code="monitor_perf" text="성능"/>', 'cpu(%)', 60, 'mem(%)', 60, data,'xy')
				/*config.subtitle = { useHTML : true, text :   '<span class="vm_num red">' + red_cnt + '</span><span class="vm_num yellow">' + yellow_cnt + '</span><span class="vm_num green">' + green_cnt + '</span>' };
				config.plotOptions.bubble={
						cursor:"pointer",
						events:{
							click:function(e)
							{

								location.href="/clovirsm/monitor/monitor.xhtml?VM_ID=" + e.point.id
							}

						}
					}
				*/
				config.tooltip=tooltip;
				config.yAxis.min=0;
				config.xAxis.min=0;

				Highcharts.chart('monitor_vm', config);
			}


			</script>

