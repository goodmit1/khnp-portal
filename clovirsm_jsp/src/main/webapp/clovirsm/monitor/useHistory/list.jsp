<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>
	<div class="box_s">
 	<div class="table_title layout name">
				<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
				<div class="btn_group">
					<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
				</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 250px" ></div>
	</div>
	</div>
	<script>
					var mainTable;


					$(function() {


						var columnDefs = [
							{headerName : "<spring:message code="NC_REQ_SVC_CD_NM" text="" />",field : "OBJ_TYPE_NM", minWidth:120},
							{headerName : "<spring:message code="NC_REQ_SVC_NM" text="" />",field : "VM_NM",minWidth:120,tooltipField:'VM_NM',},
							{headerName : "<spring:message code="NC_VM_GUEST_NM" text="" />",field : "GUEST_NM",minWidth:120,tooltipField:'GUEST_NM',},
							{headerName : "<spring:message code="NC_VM_CPU_CNT" text="" />",field : "CPU_CNT", cellStyle:{'text-align':'right'} , minWidth:120},
							{headerName : "<spring:message code="NC_VM_RAM_SIZE" text="" />(GB)",field : "RAM_SIZE", cellStyle:{'text-align':'right'}, minWidth:120},
							{headerName : "<spring:message code="NC_VM_DISK_SIZE" text="" />(GB)",field : "DISK_SIZE", cellStyle:{'text-align':'right'}, minWidth:120},
							{headerName : "<spring:message code="label_use_fee" text="" />", minWidth:120, field : "FEE", cellStyle:{'text-align':'right'},valueFormatter:function(params){
								return formatNumber(params.value);
							}},
							{headerName : "<spring:message code="monitor_mem_usage" text="" />",field : "MEM_USAGE", cellStyle:{'text-align':'right'}, minWidth:160},
							{headerName : "<spring:message code="monitor_cpu_usage" text="" />",field : "CPU_USAGE", cellStyle:{'text-align':'right'}, minWidth:140},
							
							{headerName : "<spring:message code="monitor_disk_used" text="" />(GB)",field : "DISK_USAGE", cellStyle:{'text-align':'right'}, minWidth:170, 
								headerTooltip:'<spring:message code="monitor_disk_used" text="" />(GB)',valueGetter:function(params){
								return params.data.DISK_USAGE? (params.data.DISK_SIZE * params.data.DISK_USAGE / 100).toFixed(1) : '';
							}},
							{headerName : "Disk Usage(%)",field : "DISK_USAGE", cellStyle:{'text-align':'right'}, minWidth:140, valueFormatter:function(params){
								try
								{
									return params.value.toFixed(2);
								}
								catch(e)
								{
									return params.value;
								}
							}},
							];
						var
						gridOptions = {
							hasNo : true,
							columnDefs : columnDefs,
							//rowModelType: 'infinite',
							//rowSelection : 'single',
							sizeColumnsToFit: true,
							cacheBlockSize: 100,
							rowData : [],
							enableSorting : true,
							enableColResize : true,
							enableServerSideSorting: false 
						}
						mainTable = newGrid("mainTable", gridOptions);
						 
					});
					function list(search_param)
					{
						search_param.OBJ_TYPE='V';
						post('/api/use_history/list' ,search_param, function(data) {
							mainTable.setData(data);
							//drawVMMonitorChart(data.list) ;
						});
					}
				</script>	