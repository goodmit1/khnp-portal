<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri ="http://java.sun.com/jsp/jstl/core" %>

<%
String DC_ID = request.getParameter("DC_ID");
String OVERALLSTATUS = request.getParameter("OVERALLSTATUS");
String TARGET_TYPE = request.getParameter("TARGET_TYPE");
request.setAttribute("DC_ID", DC_ID == null ? "null" : "'" + DC_ID + "'");
request.setAttribute("OVERALLSTATUS", OVERALLSTATUS == null ? "null" : "'" + OVERALLSTATUS + "'");
request.setAttribute("TARGET_TYPE", TARGET_TYPE == null ? "null" : "'" + TARGET_TYPE + "'");
%>
<!--  접근 목록 조회 -->
<style>
#mainTable { height:calc(100vh - 220px); }
</style>
<div id="search_area" class="search-panel panel panel-default">
	<div class="panel-body">

		<c:if test="${sessionScope.ADMIN_YN == 'Y'}">
			<div class="col col-sm">
				<fm-select url="/api/code_list?grp=dblist.com.clovirsm.common.Component.list_NC_DC" id="DC_ID"
					keyfield="DC_ID" titlefield="DC_NM"
					name="DC_ID" title="<spring:message code="NC_DC_DC_ID" text="데이터센터"/>"></fm-select>
			</div>
				<div class="col col-sm">
			<fm-select url="/api/code_list?grp=TARGET_TYPE" id="S_TARGET_TYPE"
				emptystr=" "
				name="TARGET_TYPE" title="<spring:message code="monitor_TARGET_TYPE" text="종류"/>">
			</fm-select>
		</div>
		</c:if>
	
		<div class="col col-sm">
			<fm-input id="S_ALARM_NM" name="ALARM_NM" title="<spring:message code="monitor_DETAIL" text="상세내용" />"></fm-input>
		</div>
		<div class="col col-sm">
			<fm-select url="/api/code_list?grp=OVERALLSTATUS" id="S_OVERALLSTATUS"
				emptystr=" "
				name="OVERALLSTATUS" title="<spring:message code="monitor_CATEGORY" text="상태"/>">
			</fm-select>
		</div>
		
		<!-- 조회의 id는 S_를 붙인다. -->
		<div class="col btn_group nomargin">
			<fm-sbutton cmd="search" class="searchBtn" onclick="search()"><spring:message code="btn_search" text="조회"/></fm-sbutton>
		</div>
		<c:if test="${ sessionScope.ADMIN_YN eq 'Y'}">
<!-- 			<div class="col btn_group_under"> -->
<%-- 				<fm-sbutton cmd="update" class="exeBtn contentsBtn tabBtnImg mark" onclick="alarmRead()"><spring:message code="btn_read" text="읽음으로 표시"/></fm-sbutton> --%>
<!-- 			</div> -->
		</c:if>
	</div>


</div>
<div class="fullGrid" id="input_area">
	<div class="box_s">
	<div class="table_title layout name">
		<%-- <spring:message code="mainTable_Search_information" text="검색정보" />&nbsp(&nbsp<spring:message code="count" text="건수"/> : <span id="mainTable_total">0</span>&nbsp)
		<div class="btn_group">
			<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="btn" id="excelBtn"></button>
		</div> --%>
		<div class="search_info">
		<spring:message code="mainTable_Search_information" text="검색정보" />
		<span class="search_count">&nbsp(&nbsp<spring:message code="count" text="건수"/>&nbsp:&nbsp<span id="mainTable_total">0</span>&nbsp)</span>
		</div>
		<div class="btn_group">
		<button type="button" title="<spring:message code="btn_excel_download" text="엑셀다운로드"/>" onclick="exportExcel()" class="layout excelBtn"></button>
		</div>
	</div>
	<div class="layout background mid">
		<div id="mainTable" class="ag-theme-fresh" style="height: 700px" ></div>
	</div>
	</div>
</div>
<script>

<c:if test="${ sessionScope.ADMIN_YN eq 'Y'}">
	function alarmRead(){
		var arr = mainTable.getSelectedRows();
		if(arr[0] == null){
			alert(msg_select_first);
			return;
		}
		post("/api/monitor/alarm/read", {list: JSON.stringify(arr)}, function(data){
			search();
		})
	}
</c:if>
var req = new Req();
mainTable;
$(function() {
	var columnDefs = [ 
		{		headerName : "<spring:message code="monitor_TARGET_TYPE" text="대상종류" />",
				field : "TARGET_TYPE",
				width: 100
		},{
			headerName : "<spring:message code="monitor_TARGET" text="대상" />",
			field : "TARGET",
			cellRenderer: function(params) {
				var dc = params.data.DC_ID;
				var type=params.data.TARGET_TYPE;
				 
				var target = params.data.TARGET;
				var href='';
				switch (type) {	
					case "DS":
						href = "/clovirsm/monitor/ds/index.jsp?NAME=" + target + "&DC_ID=" + dc;
						break;
					case "VM":
						href = "/clovirsm/monitor/vm/index.jsp?VM_NM=" + target+ "&DC_ID=" + dc;
						break;
					case "HOST":
						href = "/clovirsm/monitor/host/index.jsp?NAME=" + target + "&DC_ID=" + dc;
						break;
					case "CLUSTER":
						href = "/clovirsm/monitor/cluster/index.jsp?NAME=" + target + "&DC_ID=" + dc;
						break;
				}
				return '<a href="' + href + '"  >'+ params.value+'</a>'
			}
		}, {
			headerName : "<spring:message code="monitor_DETAIL" text="상세내용" />",
			field : "ALARM_NM"
		}, {
			headerName : "",
			field : "DC_ID",
			hide : true
		},{
			headerName : "<spring:message code="monitor_CATEGORY" text="" />",
			field : "CATEGORY",
			cellRenderer: function(params){
				return "<img src='/res/img/" + params.value + "-icon.png'>"
			},
			width: 50
		},  {
			headerName : "<spring:message code="monitor_CRE_TIME" text="" />",
			field : "CRE_TIME",
			format : "datetime"
		} ];
	var
	gridOptions = {
		hasNo : true,
		columnDefs : columnDefs,
		//rowModelType: 'infinite',
		rowSelection : 'single',
		sizeColumnsToFit: true,
		cacheBlockSize: 100,
		rowData : [],
		enableSorting : true,
		enableColResize : true,
		enableServerSideSorting: false,
	 
	}
	<c:if test="${ sessionScope.ADMIN_YN eq 'Y'}">
		gridOptions.rowSelection = 'multiple';
		gridOptions.editable = true;
	</c:if>
	mainTable = newGrid("mainTable", gridOptions);
	var today = formatDate(new Date(), 'date');
	$("#S_READ_YN").val("N");
	searchvue.form_data.READ_YN = "N";
	setTimeout(function() {
		if(${DC_ID}){
			$("#DC_ID").val(${DC_ID});
			searchvue.form_data.DC_ID = ${DC_ID};
		}
		if(${TARGET_TYPE}){
			$("#S_TARGET_TYPE").val(${TARGET_TYPE});
			searchvue.form_data.TARGET_TYPE = ${TARGET_TYPE};
		}
		if(${OVERALLSTATUS}){
			$("#S_OVERALLSTATUS").val(${OVERALLSTATUS});
			searchvue.form_data.OVERALLSTATUS = ${OVERALLSTATUS};
		}
		search();
	}, 1000);
});

// 조회
function search() {
	req.search('/api/monitor/list/list_ALARM/', function(data) {
		mainTable.setData(data);
	});
}
// 엑셀 내보내기
function exportExcel()
{
	mainTable.exportCSV({fileName:'ALARM'})		
	//exportExcelServer("mainForm", '/api/monitor/list_excel', 'ALARM', mainTable.gridOptions.columnDefs, req.getRunSearchData())
}

</script>


