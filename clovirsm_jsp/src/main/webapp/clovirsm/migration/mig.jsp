<%@page import="org.json.JSONObject"%><%@page
import="org.json.JSONArray"%><%@page
import="com.fliconz.fm.mvc.util.ExcelExportUtil"%><%@page
import="java.util.LinkedHashMap"%><%@page
import="java.util.ArrayList"%><%@page
import="java.util.HashMap"%><%@page
import="org.springframework.web.multipart.MultipartFile"%><%@page
import="com.clovirsm.service.resource.VMService"%><%@page
import="com.clovirsm.service.site.GroupService"%><%@page
import="com.fliconz.fm.admin.service.UserService"%><%@page
import="com.clovirsm.hv.IBeforeAfter"%><%@page
import="com.clovirsm.sys.hv.DC"%><%@page
import="com.fliconz.fw.runtime.util.SpringBeanUtil"%><%@page
import="com.clovirsm.sys.hv.DCService"%><%@page
import="com.fliconz.fm.security.util.DBParamMap"%><%@page
import="com.fliconz.fm.mvc.DefaultService"%><%@page
import="org.springframework.web.multipart.MultipartHttpServletRequest"%><%@page
import="org.springframework.web.multipart.support.StandardMultipartHttpServletRequest"%><%@page
import="com.fliconz.fm.mvc.util.ExcelImportUtil"%><%@page
import="java.util.Map"%><%@page
import="java.util.List"%><%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%><%!

List<Map> getExcelData(MultipartFile file, String[] fieldArr) throws Exception
{

    System.out.println("one" + fieldArr);
	List fields = new ArrayList();
	for(String f : fieldArr)
	{
		fields.add(f);
	}
	ExcelImportUtil util = new ExcelImportUtil(1, fields);
	System.out.println(file);
	return util.importExcel(file.getName(), file.getInputStream());


}
List insertDB(HttpServletRequest _request, String beanName, String tablename, StringBuffer sb) throws Exception
{
		int line = 2;

		MultipartHttpServletRequest  request = new StandardMultipartHttpServletRequest (_request );

		List<Map> list = getExcelData(request.getFile("org_file"),request.getParameterValues("field"));
		List result = new ArrayList();
		DefaultService service=(DefaultService)SpringBeanUtil.getBean(beanName);
		for(Map _m : list)
		{
			DBParamMap m = new DBParamMap();
			m.putAll(_m);
			try
			{
				int row  = service.updateByQueryKey("update_" + tablename, m);
				if(row==0)
					row  = service.insertByQueryKey("insert_" + tablename, m);
				result.add(m);
			}
			catch(Exception e)
			{
				sb.append("[" + line + "번째]" + e.getMessage() + "\n");
			}
			line++;

		}
		if(sb.length()==0)
		{
			sb.append((line-2) + "건 입력 성공");
		}
		return result;

}
%><%
String kubun  = request.getParameter("kubun");
StringBuffer sb = new StringBuffer();
try
{
	if(kubun.equals("org")){
		insertDB(request, "companySearchService", "FM_COMPANY", sb);
	}
	else if(kubun.equals("dept")){

		List<Map> list  = insertDB(request, "groupService", "FM_TEAM", sb);
		DCService service = (DCService)SpringBeanUtil.getBean("DCService");
		Map param = new HashMap();
		param.put("DC_ID", request.getParameter("DC_ID"));
		DC dc = service.getDC(request.getParameter("DC_ID"));
		param.putAll(dc.getProp());
		IBeforeAfter before  = dc.getBefore( service );
		/*ICreateInfo4Org createInfo = (ICreateInfo4Org)SpringBeanUtil.getBean("createInfo4Org");
		for(Map m : list)
		{
			m.putAll(param);
			boolean isNew = createInfo.getOrgDCInfo(service, request.getParameter("DC_ID"), m);
			if(isNew)
			{
				before.onBeforeCreateVM(true, m);
				dc.getAPI().onFirstVM(m);
			}
		}*/

	}
	else if(kubun.equals("user")){
		List<Map> list  = insertDB(request, "userSearchService", "FM_USER", sb);
		System.out.println(list);
		UserService service=(UserService)SpringBeanUtil.getBean("userService");
		GroupService groupService =(GroupService)SpringBeanUtil.getBean("groupService");

		for(Map m : list)
		{
			m.put("PASSWORD", m.get("LOGIN_ID"));
			service.chgPassword(m);
			if("Y".equals(m.get("TEAM_ADMIN_YN")))
			{
				m.put("TEAM_OWNER", m.get("USER_ID"));
				groupService.updateByQueryKey("update_FM_TEAM",m);
			}
			/*if("Y".equals(m.get("COMPANY_ADMIN_YN")))
			{
				m.put("ADMIN_ID", m.get("USER_ID"));
				companySearchService.updateByQueryKey("update_ADMIN_byTeam",m);
			}*/
		}


	}
	else if(kubun.equals("vm_sample")){
	
		DCService service = (DCService)SpringBeanUtil.getBean("DCService");
		Map param = new HashMap();
		param.put("DC_ID", request.getParameter("DC_ID"));
		DC dc = service.getDC(request.getParameter("DC_ID"));
		System.out.println("DC=" + dc);
		
		param.putAll(dc.getProp());
		Map result = dc.getAPI().getVMPerfListInDC(param);
		List<Map> list = (List)result.get("LIST");
		JSONArray fieldInfo = new JSONArray();
		JSONObject o = new JSONObject();
		o.put("headerName", "VM_ID");
		o.put("field", "OBJ_ID");
		//fieldInfo.put(o);
		JSONObject o1 = new JSONObject();
		o1.put("headerName", "VM명");
		o1.put("field", "NAME");
		fieldInfo.put(o1);

		JSONObject o2 = new JSONObject();
		o2.put("headerName", "로그인ID");
		o2.put("field", "LOGIN_ID");
		fieldInfo.put(o2);


		ExcelExportUtil util = new ExcelExportUtil("vm_sample", fieldInfo);
		util.export(list, request, response);
		return;
	}
	else if(kubun.equals("vm")){
		VMService service = (VMService)SpringBeanUtil.getBean("VMService");
		MultipartHttpServletRequest  request1 = new StandardMultipartHttpServletRequest (request );
		System.out.println(request.getParameter("DC_ID"));
		List<Map> list = getExcelData(request1.getFile("org_file"),request1.getParameterValues("field"));
		System.out.println(list);
		Map osMap = new HashMap();
		//osMap.put("CentOS 7 (64-bit)","2");
		try
		{
			service.updateExcelImport(list, request1.getParameter("DC_ID"), osMap);
			sb.append(list.size()+"건 성공");
		}
		catch(Exception e)
		{
			sb.append(e.getMessage());
		}
	}
	if(sb.length()>0)
	{
		out.println(sb.toString());
	}
}
catch(Exception e)
{
	e.printStackTrace();
	out.println(e.getMessage());
}
%>
</body>
</html>
