package fliconz_admin_jsp_sample;

import java.text.ParseException;
import java.util.Date;

import org.apache.poi.util.StringUtil;
import org.quartz.CronExpression;

import com.fliconz.fm.common.util.TextHelper;
import com.fliconz.fw.runtime.util.PropertyManager;
 

public class Test {
	@org.junit.Test
	public void cron() throws ParseException {
		CronExpression cronTrigger = new CronExpression("0 0 10 12 3 ? 2020");
	    Date next = cronTrigger.getNextValidTimeAfter(new Date());
	    System.out.println(next);
	}
	
	@org.junit.Test
	public void test1() {
		System.out.println(TextHelper.lpad("", 3, '0'));
	}
	
	
	@org.junit.Test
	public void crypt() throws Exception {
		com.fliconz.fm.security.util.DefaultCrypt crypt = new com.fliconz.fm.security.util.DefaultCrypt() {
			public String encrypt(String field) throws Exception {

				return encrypt(field,  "OjK6mHE1ajPt5XUuoTvOvw==" ,
						"rapper.hidden-ch");

			}
			public String decrypt(String field) throws Exception {

				return decrypt(field,  "OjK6mHE1ajPt5XUuoTvOvw==" ,
						"rapper.hidden-ch");

			}
		};
		crypt.setAlgorithm("AES");
		crypt.setTransformation("AES/CBC/PKCS5Padding");
		String a = "3sQ3VOksN9Udjq/oaCB+ig==\n" ; //crypt.encrypt("aabbvccc" );
		System.out.println(crypt.decrypt(a ));
		
	}
 
}
