
3. 중복로그인 방지
fliconz.properties에 login.single=true

web.xml에
<listener>
  	<listener-class>com.fliconz.fm.security.SecuritySessionListener</listener-class>
  </listener>
  

4. 주기적으로 패스워드 변경
web.xml에
 <error-page>
    <error-code>700</error-code>
    <location>/login/chgPwd.jsp</location>
  </error-page>