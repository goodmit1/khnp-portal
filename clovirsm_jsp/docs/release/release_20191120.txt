--- applicationContext.xml에 추가
<tx:method name="save*" propagation="REQUIRED" rollback-for="Exception"/>

ALTER table NC_MONITOR_VM  ADD PREV_CPU_MAX decimal(5,1) NULL COMMENT '기간내 최대 CPU%' ;
ALTER table NC_MONITOR_VM  ADD PREV_MEM_MAX decimal(5,1) NULL COMMENT '기간내 최대 메모리%' ;

INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'oversize_term_day', '30', '오버사이즈서버:기준(일)', 10, 'Y', NULL, '2019-06-03 16:19:43.000', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43.000', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'oversize_cpu', '6', '오버사이즈서버:최대CPU(%)', 11, 'Y', NULL, '2019-06-03 16:19:43.000', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43.000', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'oversize_mem', '6', '오버사이즈서버:최대Memory(%)', 12, 'Y', NULL, '2019-06-03 16:19:43.000', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43.000', NULL, NULL, NULL);
INSERT INTO fm_ddic
(DD_ID, DD_NM, DD_VALUE, KO_DD_DESC, EN_DD_DESC, SORT, USE_YN, EXT_DD_ID, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES('ENV', '환경셋팅', 'oversize_disk', '6', '오버사이즈서버:최대Disk(%)', 13, 'Y', NULL, '2019-06-03 16:19:43.000', '66', '0', '0:0:0:0:0:0:0:1', '2019-06-03 16:19:43.000', NULL, NULL, NULL);


-- IP-Network 맵핑
INSERT INTO fm_prgm
(PRGM_ID, SYSTEM_CODE, PRGM_NM, PRGM_DESC, USE_YN, PRGM_PATH, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, PKG_NM, QUERY_GEN_KUBUN, PAGE_ID, TMPL_ID, INTERNAL_ONLY, MOBILE_YN, COMMON_YN, MANUAL_PATH, PROCEDURE_SCHEMA)
VALUES(165, 0, 'IP-네트웍 맵핑', NULL, 'Y', '/clovirsm/admin/portgroupAll/index.jsp', '2019-07-12 12:38:11.000', '10', '0', '0:0:0:0:0:0:0:1', '2019-07-12 12:38:35.000', '10', '0', '0:0:0:0:0:0:0:1', '/api/portgroup/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO fm_menu
(ID, PRGM_ID, MENU_NM, PARENT_ID, THEME, MENU_DESC, MENU_ORDER, USE_YN, ICON, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP, IS_POPUP, MENU_NM_EN)
VALUES(161, 165, 'IP-네트웍 맵핑', 88, NULL, 'IP대역에 따른 VM Network 맵핑', 17, 'Y', NULL, '2019-07-12 03:38:11.000', '10', '0', '0:0:0:0:0:0:0:1', '2019-07-12 12:38:35.000', '10', '0', '0:0:0:0:0:0:0:1', 'N', NULL);


INSERT INTO fm_role_prgm
(PRGM_ID, ROLE_ID, SELECT_YN, UPDATE_YN, DELETE_YN, INSERT_YN, PRINT_YN, INS_TMS, INS_PGM, INS_ID, INS_IP, UPD_TMS, UPD_PGM, UPD_ID, UPD_IP)
VALUES(165, '19', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-07-12 12:39:08.000', '43', '0', '0:0:0:0:0:0:0:1', '2019-07-12 12:39:08.000', NULL, NULL, NULL);


INSERT INTO fm_nl
(TABLE_NM, FIELD_NM, ID, LANG_TYPE, NM)
VALUES('FM_MENU', 'ID', '161', 'en', 'IP-Network Map');


 	ALTER TABLE NC_MONITOR_DS ADD UNCMTD decimal(15) NULL 

 
 
 create or replace view v_nc_work as select
    nc_vm_req.VM_ID as SVC_ID,
    'S' as SVC_CD,
    nc_vm_req.VM_NM as SVC_NM,
    nc_vm_req.CUD_CD as CUD_CD,
    nc_vm_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_vm_req.INS_ID as INS_ID,
    nc_vm_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_vm_req.CUD_CD = 'D'
            ) then 0
            else GET_VM_DD_FEE(
                nc_vm_req.VM_ID,
                nc_vm_req.DC_ID,
                nc_vm_req.CPU_CNT,
                nc_vm_req.RAM_SIZE,
                nc_vm_req.DISK_TYPE_ID,
                nc_vm_req.DISK_SIZE,
                nc_vm_req.SPEC_ID
            )
        end
    ) as FEE,
    nc_vm_req.FEE_TYPE_CD as FEE_TYPE_CD
from
    nc_vm_req where   APPR_STATUS_CD = 'W'
union all select
    nc_img_req.IMG_ID as SVC_ID,
    'G' as SVC_CD,
    nc_img_req.IMG_NM as SVC_NM,
    nc_img_req.CUD_CD as CUD_CD,
    nc_img_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_img_req.INS_ID as INS_ID,
    nc_img_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_img_req.CUD_CD = 'D'
            ) then 0
            else GET_VM_DD_FEE(
                nc_img_req.IMG_ID,
                nc_img_req.DC_ID,
                0,
                0,
                (
                    select
                        b.IMG_DISK_TYPE_ID
                    from
                        nc_dc b
                    where
                        (
                            b.DC_ID = nc_img_req.DC_ID
                        )
                ),
                nc_img_req.DISK_SIZE,
                null
            )
        end
    ) as FEE,
    nc_img_req.FEE_TYPE_CD as FEE_TYPE_CD
from
    nc_img_req where   APPR_STATUS_CD = 'W'
union all select
    nc_fw_req.FW_ID as SVC_ID,
    'F' as SVC_CD,
    concat( concat( concat( concat( CIDR, '->' ),  VM_NM), ':' ), ifnull( nc_fw_req.PORT, '' )) as SVC_NM,
    nc_fw_req.CUD_CD as CUD_CD,
    nc_fw_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_fw_req.INS_ID as INS_ID,
    nc_fw_req.INS_DT as INS_DT,
    0 as FEE,
    'F' as FEE_TYPE_CD
from
    nc_fw_req,nc_vm
    	 
where
   nc_fw_req.VM_ID=nc_vm.VM_ID and    APPR_STATUS_CD = 'W'
union all select
    nc_vra_catalogreq_req.CATALOGREQ_ID as SVC_ID,
    'C' as SVC_CD,
    concat( concat( r.CATALOG_NM, '/' ), nc_vra_catalogreq_req.PURPOSE ) as SVC_NM,
    nc_vra_catalogreq_req.CUD_CD as CUD_CD,
    nc_vra_catalogreq_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_vra_catalogreq_req.INS_ID as INS_ID,
    nc_vra_catalogreq_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_vra_catalogreq_req.CUD_CD = 'D'
            ) then 0
            else nc_vra_catalogreq_req.PREDICT_DD_FEE
        end
    ) as FEE,
    'D' as D
from
    (
        nc_vra_catalogreq_req
    join nc_vra_catalog r
    )
where
    (
        r.CATALOG_ID = nc_vra_catalogreq_req.CATALOG_ID
    ) and     APPR_STATUS_CD = 'W';
    
    
    ------------------------------- 
    create or replace view v_nc_fw as 
select
    fw.APPR_STATUS_CD as APPR_STATUS_CD,
    fw.TASK_STATUS_CD as TASK_STATUS_CD,
    fw.CIDR as CIDR,
    fw.PORT as PORT,
    fw.VM_USER_YN as VM_USER_YN,
    fw.OLD_PORT as OLD_PORT,
    fw.OLD_VM_USER_YN as OLD_VM_USER_YN,
    fw.FW_USER_ID as FW_USER_ID,
    fw.VM_ID as VM_ID,
    fw.FW_ID as FW_ID,
    fw.CUD_CD as CUD_CD,
    fw.INS_DT as INS_DT,
    fw.INS_ID as INS_ID,
    fw.FAIL_MSG as FAIL_MSG,
    fw.SERVER_IP as SERVER_IP,
    fw.CMT as CMT,
    fw.USE_MM as USE_MM,
    fw.EXPIRE_DT as EXPIRE_DT,
    t.TEAM_CD as TEAM_CD,
    t.TEAM_NM as TEAM_NM,
    u.POSITION as POSITION,
    u.USER_NAME as USER_NAME,
    u.LOGIN_ID as LOGIN_ID,
    t2.TEAM_CD as INS_TEAM_CD,
    t2.TEAM_NM as INS_TEAM_NM,
    u2.POSITION as INS_POSITION,
    u2.USER_NAME as INS_NAME,
    u2.LOGIN_ID as INS_LOGIN_ID
from
    (
        (
            (
                (
                    (
                        (
                            select
                                '' as APPR_STATUS_CD,
                               nc_fw.TASK_STATUS_CD as TASK_STATUS_CD,
                               nc_fw.CIDR as CIDR,
                               nc_fw.PORT as PORT,
                               nc_fw.VM_USER_YN as VM_USER_YN,
                               nc_fw.PORT as OLD_PORT,
                               nc_fw.VM_USER_YN as OLD_VM_USER_YN,
                               nc_fw.FW_USER_ID as FW_USER_ID,
                               nc_fw.VM_ID as VM_ID,
                               nc_fw.FW_ID as FW_ID,
                                '' as CUD_CD,
                                INS_DT,
                               (select INS_ID from NC_VM v where v.VM_ID=nc_fw.VM_ID)  as INS_ID,
                               nc_fw.FAIL_MSG as FAIL_MSG,
                                get_vm_ip(
                                    null,
                                    null,
                                   nc_fw.VM_ID
                                ) as SERVER_IP,
                               nc_fw.CMT as CMT,
                               nc_fw.USE_MM as USE_MM,
                                (
                                    case
                                        when isnull(
                                           nc_fw.USE_MM
                                        ) then null
                                        else(
                                           nc_fw.INS_TMS + interval nc_fw.USE_MM month
                                        )
                                    end
                                ) as EXPIRE_DT
                            from
                               nc_fw
                            where
                                (
                                    (
                                       nc_fw.DEL_YN = 'N'
                                    )
                                    
                                )
                        )
                
                    ) fw
                left join fm_user u on
                    (
                        (
                            u.USER_ID = fw.FW_USER_ID
                        )
                    )
                )
            left join fm_user u2 on
                (
                    (
                        u2.USER_ID = fw.INS_ID
                    )
                )
            )
        left join fm_team t on
            (
                (
                    t.TEAM_CD = u.TEAM_CD
                )
            )
        )
    left join fm_team t2 on
        (
            (
                t2.TEAM_CD = u2.TEAM_CD
            )
        )
    );
  -----------------------------
  
  create or replace view v_nc_req_detail as  
select
    nc_vm_req.VM_ID as SVC_ID,
    'S' as SVC_CD,
    nc_vm_req.VM_NM as SVC_NM,
    nc_vm_req.CUD_CD as CUD_CD,
    nc_vm_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_vm_req.INS_ID as INS_ID,
    nc_vm_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_vm_req.CUD_CD = 'D'
            ) then 0
            else GET_VM_DD_FEE(
                nc_vm_req.VM_ID,
                nc_vm_req.DC_ID,
                nc_vm_req.CPU_CNT,
                nc_vm_req.RAM_SIZE,
                nc_vm_req.DISK_TYPE_ID,
                nc_vm_req.DISK_SIZE,
                nc_vm_req.SPEC_ID
            )
        end
    ) as FEE,
    nc_vm_req.FEE_TYPE_CD as FEE_TYPE_CD
from
    nc_vm_req
union all select
    nc_img_req.IMG_ID as SVC_ID,
    'G' as SVC_CD,
    nc_img_req.IMG_NM as SVC_NM,
    nc_img_req.CUD_CD as CUD_CD,
    nc_img_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_img_req.INS_ID as INS_ID,
    nc_img_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_img_req.CUD_CD = 'D'
            ) then 0
            else GET_VM_DD_FEE(
                nc_img_req.IMG_ID,
                nc_img_req.DC_ID,
                0,
                0,
                (
                    select
                        b.IMG_DISK_TYPE_ID
                    from
                        nc_dc b
                    where
                        (
                            b.DC_ID = nc_img_req.DC_ID
                        )
                ),
                nc_img_req.DISK_SIZE,
                null
            )
        end
    ) as FEE,
    nc_img_req.FEE_TYPE_CD as FEE_TYPE_CD
from
    nc_img_req
union all select
    nc_fw_req.FW_ID as SVC_ID,
    'F' as SVC_CD,
    concat(
        concat(
            concat(
                concat(
                    nc_fw_req.CIDR,
                    '->'
                ),
                VM_NM 
            ),
            ':'
        ),
        ifnull(
            nc_fw_req.PORT,
            ''
        )
    ) as SVC_NM,
    nc_fw_req.CUD_CD as CUD_CD,
    nc_fw_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_fw_req.INS_ID as INS_ID,
    nc_fw_req.INS_DT as INS_DT,
    0 as FEE,
    'F' as FEE_TYPE_CD
from
    nc_fw_req, nc_vm
where 	nc_vm.VM_ID = nc_fw_req.VM_ID
        
union all select
    nc_vra_catalogreq_req.CATALOGREQ_ID as SVC_ID,
    'C' as SVC_CD,
    concat(
        concat(
            r.CATALOG_NM,
            '/'
        ),
        nc_vra_catalogreq_req.PURPOSE
    ) as SVC_NM,
    nc_vra_catalogreq_req.CUD_CD as CUD_CD,
    nc_vra_catalogreq_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_vra_catalogreq_req.INS_ID as INS_ID,
    nc_vra_catalogreq_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_vra_catalogreq_req.CUD_CD = 'D'
            ) then 0
            else nc_vra_catalogreq_req.PREDICT_DD_FEE
        end
    ) as FEE,
    'D' as D
from
    (
        nc_vra_catalogreq_req
    join nc_vra_catalog r
    )
where
    (
        r.CATALOG_ID = nc_vra_catalogreq_req.CATALOG_ID
    )
    
    