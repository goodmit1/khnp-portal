
-- Can't generate DDL: table editor not found for org.jkiss.dbeaver.ext.oracle.model.OracleView


  CREATE OR REPLACE VIEW "V_NC_IMG" ("IMG_ID", "IMG_NM", "CMT", "DISK_SIZE", "FEE_TYPE_CD", "HH_FEE", "DD_FEE", "MM_FEE", "FROM_ID", "DEL_YN", "TEAM_CD", "DC_ID", "OS_ID", "INS_ID", "CUD_CD", "APPR_STATUS_CD", "TASK_STATUS_CD", "FAIL_MSG", "USE_MM", "INS_DT", "CATEGORY", "P_KUBUN", "PURPOSE", "SPEC_ID") AS 
  SELECT IMG_ID, IMG_NM, CMT, DISK_SIZE, FEE_TYPE_CD, HH_FEE, DD_FEE, MM_FEE, FROM_ID, DEL_YN, TEAM_CD, DC_ID, OS_ID,  INS_ID , '' CUD_CD, '' APPR_STATUS_CD, 
  TASK_STATUS_CD, FAIL_MSG, USE_MM,INS_DT,CATEGORY,P_KUBUN,PURPOSE,SPEC_ID
FROM NC_IMG where not exists (select IMG_ID from NC_IMG_REQ R  WHERE R.APPR_STATUS_CD In ('W','R') and NC_IMG.IMG_ID = R.IMG_ID)
union all
SELECT R.IMG_ID, R.IMG_NM, CASE WHEN I.CMT IS NULL THEN R.CMT ELSE I.CMT END CMT, (SELECT DISK_SIZE FROM NC_VM v WHERE v.VM_ID=R.FROM_ID)   DISK_SIZE, R.FEE_TYPE_CD, R.HH_FEE, R.DD_FEE, R.MM_FEE, R.FROM_ID,'N' DEL_YN, R.TEAM_CD, R.DC_ID, R.OS_ID,
	R.INS_ID, R.CUD_CD, R.APPR_STATUS_CD, I.TASK_STATUS_CD, I.FAIL_MSG, R.USE_MM, R.INS_DT, R.CATEGORY,R.P_KUBUN,R.PURPOSE,I.SPEC_ID
FROM NC_IMG_REQ R
	LEFT OUTER JOIN NC_IMG I ON ( I.IMG_ID=R.IMG_ID )
 WHERE R.APPR_STATUS_CD In ('W','R')
;

  CREATE OR REPLACE VIEW "V_NC_REQ_DETAIL" ("SVC_ID", "SVC_CD", "SVC_NM", "CUD_CD", "APPR_STATUS_CD", "INS_ID", "INS_DT", "FEE", "FEE_TYPE_CD") AS 
  SELECT VM_ID AS SVC_ID, 'S' as SVC_CD, VM_NM AS SVC_NM, CUD_CD, APPR_STATUS_CD, INS_ID, INS_DT,
			CASE WHEN CUD_CD='D' THEN 0 ELSE GET_VM_DD_FEE(VM_ID, DC_ID, CPU_CNT, RAM_SIZE, DISK_TYPE_ID, DISK_SIZE, SPEC_ID) END FEE,
			FEE_TYPE_CD
			FROM NC_VM_REQ
			 
			UNION ALL
			SELECT IMG_ID AS SVC_ID, 'G' as SVC_CD, IMG_NM AS SVC_NM, CUD_CD, APPR_STATUS_CD, INS_ID, INS_DT,
			CASE WHEN CUD_CD='D' THEN 0 ELSE GET_VM_DD_FEE(IMG_ID, DC_ID, 0,0, (SELECT IMG_DISK_TYPE_ID FROM NC_DC b WHERE b.DC_ID=NC_IMG_REQ.DC_ID), DISK_SIZE, null) END FEE, FEE_TYPE_CD
			FROM NC_IMG_REQ
			UNION ALL
			SELECT FW_ID AS SVC_ID, 'F' as SVC_CD, concat( concat( concat (concat(CIDR, '->'), get_vm_ip(null, null, VM_ID) ),':'), PORT) AS SVC_NM, CUD_CD, APPR_STATUS_CD, INS_ID, INS_DT,
			0 AS FEE, 'F' AS FEE_TYPE_CD
			FROM NC_FW_REQ WHERE EXISTS (SELECT VM_ID FROM NC_VM WHERE NC_VM.VM_ID = NC_FW_REQ.VM_ID)
  			UNION ALL
			SELECT CATALOGREQ_ID AS SVC_ID, 'C' as SVC_CD, concat(concat(CATALOG_NM,'/'), REQ_TITLE) AS SVC_NM, CUD_CD, APPR_STATUS_CD, NC_VRA_CATALOGREQ_REQ.INS_ID, NC_VRA_CATALOGREQ_REQ.INS_DT,
			CASE WHEN CUD_CD='D' THEN 0 ELSE NC_VRA_CATALOGREQ_REQ.PREDICT_DD_FEE END FEE, 'D'
			FROM NC_VRA_CATALOGREQ_REQ , NC_VRA_CATALOG r
			WHERE r.CATALOG_ID=NC_VRA_CATALOGREQ_REQ.CATALOG_ID;

  CREATE OR REPLACE VIEW "V_NC_RSC" ("ID", "FEE_TYPE_CD", "HH_FEE", "DD_FEE", "MM_FEE", "STOP_HH_FEE", "STOP_DD_FEE", "STOP_MM_FEE", "TITLE", "TASK_STATUS_CD", "DC_ID", "TEAM_CD", "DEL_YN", "INS_ID") AS 
  SELECT
		DISK_ID ID,
		FEE_TYPE_CD,
		DISK_SIZE * HH_FEE HH_FEE,
		DISK_SIZE * DD_FEE DD_FEE,
		DISK_SIZE * MM_FEE MM_FEE,
		0 STOP_HH_FEE,
		0 STOP_DD_FEE,
		0 STOP_MM_FEE,
	    NC_DISK.DISK_NM TITLE,
	    NC_DISK.TASK_STATUS_CD TASK_STATUS_CD,
	    NC_DISK.DC_ID DC_ID,
	    NC_DISK.TEAM_CD TEAM_CD,
		DEL_YN,INS_ID
	FROM
		NC_DISK
UNION ALL SELECT
		VM_ID ID,
		FEE_TYPE_CD,
		OS_HH_FEE + SPEC_HH_FEE,
		OS_DD_FEE + SPEC_DD_FEE,
		OS_MM_FEE + SPEC_MM_FEE,
		OS_HH_FEE + DISK_SIZE * STOP_HH_FEE STOP_HH_FEE,
		OS_DD_FEE + DISK_SIZE * STOP_DD_FEE STOP_DD_FEE,
		OS_MM_FEE + DISK_SIZE * STOP_MM_FEE STOP_MM_FEE,
	    NC_VM.VM_NM AS TITLE,
	    NC_VM.RUN_CD AS TASK_STATUS_CD,
	    NC_VM.DC_ID AS DC_ID,
	    NC_VM.TEAM_CD AS TEAM_CD,
		DEL_YN, INS_ID
	FROM
		NC_VM
UNION ALL SELECT
		IMG_ID ID,
		FEE_TYPE_CD,
		DISK_SIZE * HH_FEE,
		DISK_SIZE * DD_FEE,
		DISK_SIZE * MM_FEE,
		0 STOP_HH_FEE,
		0 STOP_DD_FEE,
		0 STOP_MM_FEE,
	    NC_IMG.IMG_NM AS TITLE,
	    NC_IMG.TASK_STATUS_CD AS TASK_STATUS_CD,
	    NC_IMG.DC_ID AS DC_ID,
	    NC_IMG.TEAM_CD AS TEAM_CD,
		DEL_YN, INS_ID 
	FROM
		NC_IMG
UNION ALL SELECT
	NC_VRA_CATALOGREQ.CATALOGREQ_ID,
    '' AS FEE_TYPE_CD,
    0 AS HH_FEE,
    0 AS DD_FEE,
    0 AS MM_FEE,
    0 AS STOP_HH_FEE,
    0 AS STOP_DD_FEE,
    0 AS STOP_MM_FEE,
    NC_VRA_CATALOGREQ.REQ_TITLE AS TITLE,
    NC_VRA_CATALOGREQ.TASK_STATUS_CD AS TASK_STATUS_CD,
    '' AS DC_ID,
    NC_VRA_CATALOGREQ.TEAM_CD AS TEAM_CD,
    NC_VRA_CATALOGREQ.DEL_YN,INS_ID
FROM
    NC_VRA_CATALOGREQ
UNION ALL SELECT
	FW_ID,
    '' AS FEE_TYPE_CD,
    0 AS HH_FEE,
    0 AS DD_FEE,
    0 AS MM_FEE,
    0 AS STOP_HH_FEE,
    0 AS STOP_DD_FEE,
    0 AS STOP_MM_FEE,
    concat( concat( concat ( concat(a.CIDR, '->'), get_vm_ip(null, null, a.VM_ID) ),':'), a.PORT)  AS TITLE,
    a.TASK_STATUS_CD AS TASK_STATUS_CD,
    b.DC_ID AS DC_ID,
    b.TEAM_CD AS TEAM_CD,
    a.DEL_YN, a.INS_ID
FROM
    NC_FW    a 
    LEFT OUTER JOIN NC_VM b ON (a.VM_ID=b.VM_ID)
;

  CREATE OR REPLACE VIEW "V_NC_VM" ("VM_ID", "VM_NM", "OLD_VM_NM", "DC_ID", "TEAM_CD", "CMT", "RUN_CD", "CUD_CD", "APPR_STATUS_CD", "INS_PGM", "INS_TMS", "INS_IP", "INS_ID", "FEE_TYPE_CD", "OS_HH_FEE", "SPEC_HH_FEE", "OS_DD_FEE", "SPEC_DD_FEE", "SPEC_ID", "OLD_SPEC_ID", "OS_ID", "OS_MM_FEE", "SPEC_MM_FEE", "FROM_ID", "DISK_TYPE_ID", "OLD_DISK_TYPE_ID", "TMPL_NM", "OLD_CPU_CNT", "OLD_RAM_SIZE", "OLD_DISK_SIZE", "OLD_DISK_UNIT", "CPU_CNT", "RAM_SIZE", "DISK_SIZE", "DISK_UNIT", "STOP_DD_FEE", "STOP_HH_FEE", "STOP_MM_FEE", "FAIL_MSG", "INS_DT", "VM_HV_ID", "LAST_USE_TMS", "PURPOSE", "GUEST_NM", "CATEGORY", "P_KUBUN", "USE_MM", "NAS", "DEL_YN") AS 
  select VM_ID, VM_NM, VM_NM AS OLD_VM_NM, DC_ID, TEAM_CD, CMT, RUN_CD,  '' CUD_CD,  '' APPR_STATUS_CD , INS_PGM,  INS_TMS,INS_IP,INS_ID, FEE_TYPE_CD, OS_HH_FEE, SPEC_HH_FEE, OS_DD_FEE, SPEC_DD_FEE,
   SPEC_ID, SPEC_ID AS OLD_SPEC_ID, OS_ID, OS_MM_FEE, SPEC_MM_FEE, FROM_ID, 
   DISK_TYPE_ID, DISK_TYPE_ID AS OLD_DISK_TYPE_ID,
   get_template_nm(NC_VM.OS_ID, NC_VM.FROM_ID) AS TMPL_NM,
   CPU_CNT AS OLD_CPU_CNT, RAM_SIZE AS OLD_RAM_SIZE,
   DISK_SIZE AS OLD_DISK_SIZE,
   'G' AS OLD_DISK_UNIT,
   CPU_CNT, RAM_SIZE, 
   DISK_SIZE,
   'G' DISK_UNIT, STOP_DD_FEE, STOP_HH_FEE, STOP_MM_FEE,
   FAIL_MSG, INS_DT, VM_HV_ID, LAST_USE_TMS, PURPOSE, GUEST_NM, CATEGORY, P_KUBUN, USE_MM, NAS, DEL_YN
from NC_VM  where not exists (select VM_ID from NC_VM_REQ where APPR_STATUS_CD in ('W','R') and NC_VM.VM_ID = NC_VM_REQ.VM_ID)
union all
select r.VM_ID, r.VM_NM, v.VM_NM AS OLD_VM_NM, r.DC_ID, r.TEAM_CD, r.CMT, 
   case when r.CUD_CD='C' then 'C' else v.RUN_CD end RUN_CD, 
   r.CUD_CD,  r.APPR_STATUS_CD,  r.INS_PGM, null INS_TMS,r.INS_IP,r.INS_ID, r.FEE_TYPE_CD, r.OS_HH_FEE, r.SPEC_HH_FEE,
   r.OS_DD_FEE, r.SPEC_DD_FEE, r.SPEC_ID, v.SPEC_ID AS OLD_SPEC_ID, NVL(v.OS_ID, r.OS_ID), r.OS_MM_FEE, r.SPEC_MM_FEE, r.FROM_ID, 
   r.DISK_TYPE_ID, v.DISK_TYPE_ID AS OLD_DISK_TYPE_ID,
   get_template_nm(r.OS_ID, r.FROM_ID) AS TMPL_NM,
   v.CPU_CNT AS OLD_CPU_CNT, v.RAM_SIZE AS OLD_RAM_SIZE, 
   v.DISK_SIZE AS OLD_DISK_SIZE,
   'G' AS OLD_DISK_UNIT, 
   r.CPU_CNT, r.RAM_SIZE, 
   r.DISK_SIZE,
   'G' AS DISK_UNIT, r.STOP_DD_FEE, r.STOP_HH_FEE, r.STOP_MM_FEE,
   '' FAIL_MSG, r.INS_DT, '' VM_HV_ID, null LAST_USE_TMS, NVL(v.PURPOSE, r.PURPOSE), v.GUEST_NM, 
   NVL(v.CATEGORY, r.CATEGORY), NVL(v.P_KUBUN, r.P_KUBUN), NVL(v.USE_MM, r.USE_MM), r.NAS, 'N' DEL_YN
from NC_VM_REQ r 
LEFT OUTER JOIN NC_VM v ON (r.VM_ID=v.VM_ID)
where  r.APPR_STATUS_CD in ('W','R')
;

  CREATE OR REPLACE VIEW "V_NC_VRA_CATALOGREQ" ("CATALOGREQ_ID", "CATALOG_ID", "REQ_TITLE", "P_KUBUN", "USE_MM", "CUD_CD", "APPR_STATUS_CD", "TASK_STATUS_CD", "FAIL_MSG", "TEAM_CD", "INS_TMS", "INS_ID", "DEL_YN") AS 
  select
    req.CATALOGREQ_ID as CATALOGREQ_ID,
    req.CATALOG_ID as CATALOG_ID,
    req.REQ_TITLE as REQ_TITLE,
    req.P_KUBUN as P_KUBUN,
    req.USE_MM as USE_MM,
    '' as CUD_CD,
    '' as APPR_STATUS_CD,
    req.TASK_STATUS_CD as TASK_STATUS_CD,
    req.FAIL_MSG as FAIL_MSG,
    req.TEAM_CD as TEAM_CD,
    req.INS_TMS as INS_TMS,
    req.INS_ID as INS_ID,
    req.DEL_YN as DEL_YN
from
    nc_vra_catalogreq req
	left outer join nc_vra_catalogreq_req reqreq 
	on (reqreq.CATALOGREQ_ID=req.CATALOGREQ_ID and reqreq.APPR_STATUS_CD in ('R',  'W'))
where
   reqreq.CATALOGREQ_ID is null 
union all
select
    reqreq.CATALOGREQ_ID as CATALOGREQ_ID,
    reqreq.CATALOG_ID as CATALOG_ID,
    reqreq.REQ_TITLE as REQ_TITLE,
    reqreq.P_KUBUN as P_KUBUN,
    reqreq.USE_MM as USE_MM,
    reqreq.CUD_CD as CUD_CD,
    reqreq.APPR_STATUS_CD as APPR_STATUS_CD,
    '' as TASK_STATUS_CD,
    '' as FAIL_MSG,
    reqreq.TEAM_CD as TEAM_CD,
    reqreq.INS_TMS as INS_TMS,
    reqreq.INS_ID as INS_ID,
    'N' as DEL_YN
from
    nc_vra_catalogreq_req reqreq
    left outer join  nc_vra_catalogreq req
    on (reqreq.CATALOGREQ_ID=req.CATALOGREQ_ID)
where
    (reqreq.APPR_STATUS_CD in ('R',
    'W'));

  CREATE OR REPLACE VIEW "V_NC_WORK" ("SVC_ID", "SVC_CD", "SVC_NM", "CUD_CD", "APPR_STATUS_CD", "INS_ID", "INS_DT", "FEE", "FEE_TYPE_CD") AS 
  select
    nc_vm_req.VM_ID as SVC_ID,
    'S' as SVC_CD,
    nc_vm_req.VM_NM as SVC_NM,
    nc_vm_req.CUD_CD as CUD_CD,
    nc_vm_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_vm_req.INS_ID as INS_ID,
    nc_vm_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_vm_req.CUD_CD = 'D'
            ) then 0
            else GET_VM_DD_FEE(
                nc_vm_req.VM_ID,
                nc_vm_req.DC_ID,
                nc_vm_req.CPU_CNT,
                nc_vm_req.RAM_SIZE,
                nc_vm_req.DISK_TYPE_ID,
                nc_vm_req.DISK_SIZE,
                nc_vm_req.SPEC_ID
            )
        end
    ) as FEE,
    nc_vm_req.FEE_TYPE_CD as FEE_TYPE_CD
from
    nc_vm_req where   APPR_STATUS_CD = 'W'
union all select
    nc_img_req.IMG_ID as SVC_ID,
    'G' as SVC_CD,
    nc_img_req.IMG_NM as SVC_NM,
    nc_img_req.CUD_CD as CUD_CD,
    nc_img_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_img_req.INS_ID as INS_ID,
    nc_img_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_img_req.CUD_CD = 'D'
            ) then 0
            else GET_VM_DD_FEE(
                nc_img_req.IMG_ID,
                nc_img_req.DC_ID,
                0,
                0,
                (
                    select
                        b.IMG_DISK_TYPE_ID
                    from
                        nc_dc b
                    where
                        (
                            b.DC_ID = nc_img_req.DC_ID
                        )
                ),
                nc_img_req.DISK_SIZE,
                null
            )
        end
    ) as FEE,
    nc_img_req.FEE_TYPE_CD as FEE_TYPE_CD
from
    nc_img_req where   APPR_STATUS_CD = 'W'
union all select
    nc_fw_req.FW_ID as SVC_ID,
    'F' as SVC_CD,
    concat( concat( concat( concat( CIDR, '->' ),  VM_NM), ':' ), nvl( nc_fw_req.PORT, '' )) as SVC_NM,
    nc_fw_req.CUD_CD as CUD_CD,
    nc_fw_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_fw_req.INS_ID as INS_ID,
    nc_fw_req.INS_DT as INS_DT,
    0 as FEE,
    'F' as FEE_TYPE_CD
from
    nc_fw_req,nc_vm
    	 
where
   nc_fw_req.VM_ID=nc_vm.VM_ID and    APPR_STATUS_CD = 'W'
union all select
    nc_vra_catalogreq_req.CATALOGREQ_ID as SVC_ID,
    'C' as SVC_CD,
    concat( concat( r.CATALOG_NM, '/' ), nc_vra_catalogreq_req.REQ_TITLE ) as SVC_NM,
    nc_vra_catalogreq_req.CUD_CD as CUD_CD,
    nc_vra_catalogreq_req.APPR_STATUS_CD as APPR_STATUS_CD,
    nc_vra_catalogreq_req.INS_ID as INS_ID,
    nc_vra_catalogreq_req.INS_DT as INS_DT,
    (
        case
            when(
                nc_vra_catalogreq_req.CUD_CD = 'D'
            ) then 0
            else nc_vra_catalogreq_req.PREDICT_DD_FEE
        end
    ) as FEE,
    'D' as D
from
      nc_vra_catalogreq_req, nc_vra_catalog r
     
where
    (
        r.CATALOG_ID = nc_vra_catalogreq_req.CATALOG_ID
    ) and     APPR_STATUS_CD = 'W'
;