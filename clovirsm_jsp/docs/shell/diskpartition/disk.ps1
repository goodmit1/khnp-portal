#diskpartition �Ŀ���

## diskinfos parameters must be contain the mountpoint and disk size information
## ex) q=100,m=200(driverletter=disksize,...)
## Do not accept defined (A, B, Z) drive from portal

Function DiskFormat {
 Write-Output "Initializing Disk"
 Initialize-Disk $Disk.Number -PartitionStyle GPT -confirm:$false
 Write-Output "Partitioning Disk"
 New-Partition -DiskNumber $Disk.Number -AssignDriveLetter -UseMaximumSize | Format-Volume -FileSystem NTFS -NewFileSystemLabel "Local Disk" -confirm:$False
 }
Function DiskFormat_DriveLetter {
 Write-Output "Initializing Disk: $DriveLetter - $DiskSize"
 Initialize-Disk $Disk.Number -PartitionStyle GPT -confirm:$false
 Write-Output "Partitioning Disk: $DriveLetter - $DiskSize"
 New-Partition -DiskNumber $Disk.Number -DriveLetter $DriveLetter -UseMaximumSize | Format-Volume -FileSystem NTFS -NewFileSystemLabel "Local Disk" -confirm:$False
 }
$DiskInfos = $args[0]
If($DiskInfos -eq "") {
 $Disks = Get-Disk | Where-Object {$_.PartitionStyle -eq "RAW"}
 Foreach($Disk in $Disks) {
  DiskFormat
 }
}
Else {
 $Array_DiskInfo = $DiskInfos.split(",")
 Foreach($DiskInfo in $Array_DiskInfo) {
  $DriveLetter = ($DiskInfo.Split("="))[0]
  $DiskSize = ($DiskInfo.Split("="))[1]
  $DiskSizeinGB = $DiskSize + "GB"
  $Disks = Get-Disk | Where-Object {$_.PartitionStyle -eq "RAW" -and $_.Size -eq $DiskSizeinGB}
  $Disk = $Disks[0]
  If($DriveLetter -ne "") {
     DiskFormat_DriveLetter
  }
  Else {
   DiskFormat
  }
}
}