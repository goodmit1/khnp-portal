@echo off


 
:adduser
 
@echo Set objShell = CreateObject("Shell.Application") > %temp%\sudo.tmp.vbs
@echo args = "user %2 P@ssw0rd /ADD /logonpasswordchg:Yes" >> %temp%\sudo.tmp.vbs
@echo objShell.ShellExecute "net", args, "", "runas", 0 >> %temp%\sudo.tmp.vbs
@echo args = "localgroup ""Remote Desktop Users"" %2/ADD" >> %temp%\sudo.tmp.vbs
@echo objShell.ShellExecute "net", args, "", "runas", 0 >> %temp%\sudo.tmp.vbs
@cscript %temp%\sudo.tmp.vbs


goto end

:deluser

if %result% == 1 (
	goto end
)
@echo Set objShell = CreateObject("Shell.Application") > %temp%\sudo.tmp.vbs
@echo args = "user %2 /delete" >> %temp%\sudo.tmp.vbs
@echo objShell.ShellExecute "net", args, "", "runas", 0 >> %temp%\sudo.tmp.vbs
@cscript %temp%\sudo.tmp.vbs


:end
