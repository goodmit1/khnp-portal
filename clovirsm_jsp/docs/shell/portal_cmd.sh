#!/bin/sh


if [ "$1" = "adduser" ] ; then
	linecnt=$(id $2 | grep uid | wc -l)
	if [ "$linecnt" = "1" ]; then
		exit 0
	fi
	sudo useradd -d /home/$2 -m -s /bin/bash -k /etc/skel $2
	echo "$2:P@ssw0rd" | sudo chpasswd
	sudo chage -d 0 $2
else
	if [ "$1" = "deluser" ] ; then
		if [ "$linecnt" = "0" ]; then
			exit 0
		fi
		sudo userdel -r  $2
	fi
fi
